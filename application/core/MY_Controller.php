<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends MX_Controller{

  var $sess;

  public function __construct(){
    parent::__construct();
    if(@$this->session->userdata('sess_login') == false) redirect('app/auth/login');
    $this->sess = $this->session->userdata();

    // Pagination config
    $config_pagination['full_tag_open'] = '<ul class="pagination pagination-sm float-right">';
    $config_pagination['full_tag_close'] = '</ul>';
    $config_pagination['attributes'] = ['class' => 'page-link'];
    $config_pagination["first_link"] = "&Lang;";
    $config_pagination["last_link"] = "&Rang;";
    $config_pagination['first_tag_open'] = '<li class="page-item">';
    $config_pagination['first_tag_close'] = '</li>';
    $config_pagination['prev_link'] = '&lang;';
    $config_pagination['prev_tag_open'] = '<li class="page-item">';
    $config_pagination['prev_tag_close'] = '</li>';
    $config_pagination['next_link'] = '&rang;';
    $config_pagination['next_tag_open'] = '<li class="page-item">';
    $config_pagination['next_tag_close'] = '</li>';
    $config_pagination['last_tag_open'] = '<li class="page-item">';
    $config_pagination['last_tag_close'] = '</li>';
    $config_pagination['cur_tag_open'] = '<li class="page-item active"><a href="#" class="page-link">';
    $config_pagination['cur_tag_close'] = '<span class="sr-only">(current)</span></a></li>';
    $config_pagination['num_tag_open'] = '<li class="page-item">';
    $config_pagination['num_tag_close'] = '</li>';
    $config_pagination['num_links'] = 3;
    $this->pagination->initialize($config_pagination);
  }

  public function authorize($nav, $field)
  {
    if ($nav[$field] == false) redirect(site_url().'/error/403');
  }

  public function render($content, $data = array())
  {
    $data['config']   = $this->m_app->_get_config();
    $data['identitas']   = $this->m_app->_get_identitas();
    $data['profile']  = $this->m_app->_get_profile_user_login();
    $data['module_nav']  = $this->m_app->_get_nav_by_module($data['nav']['nav_module']);
    $data['group_nav']   = $this->m_app->_list_nav(@$data['module_nav']['nav_id']);
    if ($data['nav']['nav_module'] == 'overview') {
      $data['running_text'] = $this->m_app->_get_running_text();
    }
    $data['template_top_menu'] = ($data['nav']['nav_module'] == 'overview' ? 'app/template/top-menu-dashboard' : 'app/template/top-menu');
    $data['module_title'] = (@$data['module_nav']['nav_id'] != '1') ? @$data['module_nav']['nav_desc'] : @$data['module_nav']['nav_desc'];

    $this->load->view('app/template/header', $data);
    $this->load->view($data['template_top_menu'], $data);
    $this->load->view($content, $data);
    $this->load->view('app/template/footer');
  }

}
