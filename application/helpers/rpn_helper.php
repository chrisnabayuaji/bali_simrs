<?php
defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('hash_password')) {
  function hash_password($str)
  {
    if ($str == "") $str = date('Y-m-d H:i:s');
    $result = password_hash($str, PASSWORD_BCRYPT);
    return $result;
  }
}

if (!function_exists('active_menu')) {
  function active_menu($data = null, $uri1 = null, $uri2 = null, $uri3 = null)
  {
    $uri = $uri1 . '/' . $uri2;
    if ($uri3 != '') {
      $uri .= '/' . $uri3;
    }
    $result = false;
    foreach ($data as $key => $val) {
      if ($val['nav_url'] == $uri) $result = true;
    }
    return $result;
  }
}

if (!function_exists('active_submenu')) {
  function active_submenu($nav_url = null, $uri1 = null, $uri2 = null, $uri3 = null)
  {
    $uri = $uri1 . '/' . $uri2;
    if ($uri3 != '') {
      $uri .= '/' . $uri3;
    }
    $result = false;
    if ($nav_url == $uri) $result = true;
    return $result;
  }
}

if (!function_exists('session_destroy')) {
  function session_destroy()
  {
    $CI = get_instance();
    $CI->session->sess_destroy();

    $menu = $CI->db->query("SELECT * FROM app_nav")->result_array();
    foreach ($menu as $r) {
      delete_cookie($r['nav_url']);
    }
  }
}

if (!function_exists('create_log')) {
  function create_log($access = 1, $module = "")
  {
    // $CI = get_instance();

    // $acc = $CI->db->where('id', $access)->get('_access')->row_array();

    // if ($CI->agent->is_browser()){
    //   $agent = $CI->agent->browser().' '.$CI->agent->version();
    // }elseif ($CI->agent->is_robot()){
    //   $agent = $CI->agent->robot();
    // }elseif ($CI->agent->is_mobile()){
    //   $agent = $CI->agent->mobile();
    // }else{
    //   $agent = 'Unidentified';
    // }

    // $data = array(
    //   'user_id' => @$CI->session->userdata('user_id'),
    //   'session_id' => @$CI->session->session_id,
    //   'fullname' => @$CI->session->userdata('fullname'),
    //   'access' => @$acc['access'],
    //   'ip_address' => @$CI->input->ip_address(),
    //   'user_agent' => @$agent,
    //   'platform' => @$CI->agent->platform(),
    //   'module' => @$module,
    //   'url' => @current_url(),
    //   'description' => @$acc['description'],
    //   'created' => @date('Y-m-d H:i:s')
    // );

    // $file = APPPATH.'logs/access-'.date('Y-m-d').'.json';
    // $log = json_decode(read_file($file));
    // if ($log === null) {$log = array();};
    // array_push($log,$data);
    // write_file($file, json_encode($log), 'w+');
  }
}

if (!function_exists('get_cookie_nav')) {
  function get_cookie_nav($nav_id)
  {
    $nav_id = str_replace('.', '_', $nav_id);
    $CI = get_instance();
    if (is_null(get_cookie($nav_id))) {
      $val = array(
        'search' => null,
        'per_page' => null,
        'cur_page' => null,
        'total_rows' => null,
        'order' => null
      );
      $cookie = array(
        'name'   => $nav_id,
        'value'  => json_encode($val),
        'expire' => '120'
      );
      $CI->input->set_cookie($cookie);
      return $val;
    } else {
      return json_decode(get_cookie($nav_id), TRUE);
    }
  }
}

if (!function_exists('set_cookie_nav')) {
  function set_cookie_nav($nav_id, $cookie_val)
  {
    $nav_id = str_replace('.', '_', $nav_id);
    $CI = get_instance();
    $cookie = array(
      'name'   => $nav_id,
      'value'  => json_encode($cookie_val),
      'expire' => '120'
    );
    $CI->input->set_cookie($cookie);
  }
}

if (!function_exists('del_cookie_nav')) {
  function del_cookie_nav($nav_id)
  {
    $nav_id = str_replace('.', '_', $nav_id);
    $CI = get_instance();
    delete_cookie($nav_id);
  }
}

if (!function_exists('set_pagination')) {
  function set_pagination($nav, $data, $id = null)
  {
    $CI = get_instance();
    $config['per_page'] = $data['per_page'];
    $config['base_url'] = site_url() . '/' . $nav['nav_url'] . '/index/' . $id;
    $config['total_rows'] = $data['total_rows'];
    $CI->pagination->initialize($config);
  }
}

if (!function_exists('pagination_info')) {
  function pagination_info($list_rows, $data)
  {
    $str = '<i class="fas fa-eye"></i> Tampil ';
    if ($list_rows == 0) {
      $str .= '0 - 0 dari 0';
    } else {
      if ($list_rows > 0) {
        $str .= ($data['cur_page'] + 1);
      } else {
        $str .= ($data['cur_page']);
      }
      $str .= " - " . ($data['cur_page'] + $list_rows) . " dari " . $data['total_rows'];
    }
    $str .= " data";
    return $str;
  }
}

function table_sort($nav_id, $title, $field, $order)
{
  $url = ($order['type'] == 'asc') ? 'desc' : 'asc';
  $icon = ($order['type'] == 'asc') ? 'sort-up' : 'sort-down';
  if ($order['field'] == $field) :
    return '<a class="text-gray" href="' . site_url() . '/app/order/' . $nav_id . '/' . $field . '/' . $url . '">' . $title . ' <i class="fa fa-' . $icon . '"></i></a>';
  else :
    return '<a class="text-gray" href="' . site_url() . '/app/order/' . $nav_id . '/' . $field . '/asc">' . $title . ' <i class="fa fa-sort"></i></a>';
  endif;
}

if (!function_exists('zerofill')) {
  function zerofill($id = null, $num = null)
  {
    $len = strlen($id);
    $r = '';
    if ($num == 2) {
      if ($len == '1') $r = '0' . $id;
      elseif ($len == '2') $r = $id;
      else $r = $id;
    } elseif ($num == 4) {
      if ($len == '1') $r = '000' . $id;
      elseif ($len == '2') $r = '00' . $id;
      elseif ($len == '3') $r = '0' . $id;
      elseif ($len == '4') $r = $id;
      else $r = $id;
    } elseif ($num == 5) {
      if ($len == '1') $r = '0000' . $id;
      elseif ($len == '2') $r = '000' . $id;
      elseif ($len == '3') $r = '00' . $id;
      elseif ($len == '4') $r = '0' . $id;
      elseif ($len == '5') $r = $id;
      else $r = $id;
    } elseif ($num == 6) {
      if ($len == '1') $r = '00000' . $id;
      elseif ($len == '2') $r = '0000' . $id;
      elseif ($len == '3') $r = '000' . $id;
      elseif ($len == '4') $r = '00' . $id;
      elseif ($len == '5') $r = '0' . $id;
      elseif ($len == '6') $r = $id;
      else $r = $id;
    } else {
      if ($len == '1') $r = '00' . $id;
      elseif ($len == '2') $r = '0' . $id;
      elseif ($len == '3') $r = $id;
      else $r = $id;
    }
    return $r;
  }
}

if (!function_exists('clear_kab_kota')) {
  function clear_kab_kota($str = null)
  {
    $str = str_replace("KAB. ", "", $str);
    $str = str_replace("KOTA ", "", $str);
    $str = strip_tags($str);
    $str = htmlentities($str);
    return $str;
  }
}

if (!function_exists('clear_numeric')) {
  function clear_numeric($val = null)
  {
    $result = str_replace('.', '', $val);
    $result = str_replace(',', '.', $result);
    return $result;
  }
}

if (!function_exists('point_to_under')) {
  function point_to_under($id = null)
  {
    $result = str_replace('.', '_', $id);
    return $result;
  }
}

if (!function_exists('under_to_point')) {
  function under_to_point($id = null)
  {
    $result = str_replace('_', '.', $id);
    return $result;
  }
}

if (!function_exists('space_to_dbl_hashtage')) {
  function space_to_dbl_hashtage($id = null)
  {
    $result = str_replace(' ', '##', $id);
    return $result;
  }
}

if (!function_exists('dbl_hashtage_to_space')) {
  function dbl_hashtage_to_space($id = null)
  {
    $result = str_replace('##', ' ', $id);
    return $result;
  }
}

if (!function_exists('num_id')) {
  function num_id($v, $s = null)
  {
    if ($v != '') {
      if (is_numeric($v)) {
        $res = number_format($v, 0, ",", ".");
        if ($s != null && $v == 0) return $s;
        else return $res;
      } else {
        return $s;
      }
    } else {
      return 0;
    }
  }
}

if (!function_exists('float_id')) {
  function float_id($v, $s = null)
  {
    $raw = explode('.', $v);
    $fraction = "";
    $fraction = (count($raw) == 2) ? "," . $raw[1] : "";
    if ($v != '') {
      if (is_numeric($v)) {
        $res = number_format($raw[0], 0, ",", ".");
        if ($s != null && $raw[0] == 0) return $s;
        else return $res . $fraction;
      } else {
        return $s;
      }
    } else {
      return 0;
    }
  }
}

if (!function_exists('js_chosen')) {
  function js_chosen()
  {
    $html = '<script>
               $(function() {
                  $(".chosen-select").select2();
               });
               </script>';
    return $html;
  }
}

if (!function_exists('month')) {
  function month($bln)
  {
    switch ($bln) {
      case 1:
        return "Januari";
        break;
      case 2:
        return "Februari";
        break;
      case 3:
        return "Maret";
        break;
      case 4:
        return "April";
        break;
      case 5:
        return "Mei";
        break;
      case 6:
        return "Juni";
        break;
      case 7:
        return "Juli";
        break;
      case 8:
        return "Agustus";
        break;
      case 9:
        return "September";
        break;
      case 10:
        return "Oktober";
        break;
      case 11:
        return "November";
        break;
      case 12:
        return "Desember";
        break;
    }
  }
}

if (!function_exists('to_date')) {
  function to_date($date = null, $sp = null, $tp = null, $sp2 = null)
  {
    if ($date != '') {
      if ($tp == 'date') {
        $arr_date = explode(' ', $date);
        $date = $arr_date[0];
      } elseif ($tp == 'full_date') {
        $arr_date = explode(' ', $date);
        $date = $arr_date[0];
        $time = $arr_date[1];
      }
      $arr = explode('-', $date);
      if ($sp != '') {
        $result = $arr[2] . $sp . $arr[1] . $sp . $arr[0];
      } else {
        $result = $arr[2] . '-' . $arr[1] . '-' . $arr[0];
      }
      if ($tp == 'full_date') {
        if ($sp2 != '') {
          $result .= $sp2 . $time;
        } else {
          $result .= ' ' . $time;
        }
      }
    } else {
      $result = '';
    }
    return $result;
  }
}

if (!function_exists('to_date_indo')) {
  function to_date_indo($tgl = '', $type = '')
  {
    if ($tgl != '') {
      $tanggal = substr($tgl, 8, 2);
      $jam = substr($tgl, 11, 8);
      $bulan = month(substr($tgl, 5, 2));
      $tahun = substr($tgl, 0, 4);
      if ($type == 'date') {
        return $tanggal . ' ' . $bulan . ' ' . $tahun;
      } else {
        if ($jam != '') {
          return $tanggal . ' ' . $bulan . ' ' . $tahun . ' ' . $jam . ' WIB';
        } else {
          return $tanggal . ' ' . $bulan . ' ' . $tahun;
        }
      }
    }
  }
}

if (!function_exists('clear_font_icon')) {
  function clear_font_icon($str = null)
  {
    $str = str_replace("fas fa-", "", $str);
    $str = str_replace("mdi mdi-", "", $str);
    $str = strip_tags($str);
    $str = htmlentities($str);
    return $str;
  }
}

if (!function_exists('get_file_type')) {
  function get_file_type($file_name = null)
  {
    $arr = explode('.', $file_name);
    $len = count($arr) - 1;
    $file_type = $arr[$len];
    return $file_type;
  }
}

if (!function_exists('create_title_img')) {
  function create_title_img($path_dir, $tmp_name, $fupload_name = '', $old_file = null, $name_file = '')
  {
    if ($old_file != "") {
      unlink($path_dir . $old_file);
    }
    //
    $file_type = get_file_type($fupload_name);
    if ($name_file != '') {
      $file_name = 'simrs-' . $name_file . '.' . $file_type;
    } else {
      $file_name = 'simrs-' . md5(md5(date('Y-m-d H:i:s') . microtime() . $fupload_name)) . '.' . $file_type;
    }
    $vfile_upload = $path_dir . $file_name;
    //
    return @$file_name;
  }
}

function upload_image($path_dir, $tmp_name, $fupload_name = '', $old_file = null, $name_file = '')
{
  if ($old_file != "") {
    unlink($path_dir . $old_file);
  }
  //
  $file_type = get_file_type($fupload_name);
  if ($name_file != '') {
    $file_name = 'simrs-' . $name_file . '.' . $file_type;
  } else {
    $file_name = 'simrs-' . md5(md5(date('Y-m-d H:i:s') . microtime() . $fupload_name)) . '.' . $file_type;
  }
  $vfile_upload = $path_dir . $file_name;
  move_uploaded_file($tmp_name, $vfile_upload);
  //
  return @$file_name;
}

if (!function_exists('upload_move_video')) {
  function upload_move_video($path_dir, $tmp_name, $fupload_name = '', $old_file = null)
  {
    if ($old_file != "") {
      unlink($path_dir . $old_file);
    }
    //
    $file_type = get_file_type($fupload_name);
    $file_name = 'simrs-' . md5(md5(date('Y-m-d H:i:s') . microtime() . $fupload_name)) . '.' . $file_type;
    $vfile_upload = $path_dir . $file_name;
    //
    if (!$tmp_name) { // if file not chosen
      echo "ERROR: Please browse for a file before clicking the upload button.";
      exit();
    }
    if (move_uploaded_file($tmp_name, $vfile_upload)) {
      echo "Video berhasil di upload";
    } else {
      echo "move_uploaded_file function failed";
    }

    //
    return @$file_name;
  }
}

if (!function_exists('get_parameter')) {
  function get_parameter($value, $type = 'parameter_field', $where = null)
  {

    if ($where != '') {
      $where_res = "AND " . $where;
    } else {
      $where_res = "";
    }

    $CI = get_instance();

    $res = $CI->db->query("SELECT * FROM mst_parameter WHERE $type = '$value' $where_res")->result_array();
    return $res;
  }
}

if (!function_exists('get_id')) {
  function get_id($modul = '')
  {
    $CI = get_instance();

    $date_now = date('Y-m-d');
    $id = $CI->db->query("SELECT * FROM tmp_id WHERE modul = '$modul' AND tgl_id = '$date_now'")->row_array();
    if (@$id['no_id'] == '' || @$id['modul'] == '') {
      $result = date('ymd') . '0001';
    } else {
      $result = $id['no_id'] + 1;
    }
    return $result;
  }
}

if (!function_exists('update_id')) {
  function update_id($modul = '', $no_id = '')
  {
    $CI = get_instance();

    $date_now = date('Y-m-d');
    $check = $CI->db->query("SELECT * FROM tmp_id WHERE modul = '$modul'")->row_array();

    if (@$check['no_id'] == '') {
      $result = $CI->db->query("INSERT INTO tmp_id (modul, tgl_id, no_id) VALUES ('$modul', '$date_now', '$no_id')");
    } else {
      $result = $CI->db->query("UPDATE tmp_id SET tgl_id = '$date_now', no_id = '$no_id' WHERE modul = '$modul'");
    }

    return $result;
  }
}

// if (!function_exists('update_id')){
//   function update_id($modul='', $no_id='')
//   {
//     $CI = get_instance();

//     $date_now = date('Y-m-d');
//     $check = $CI->db->query("SELECT * FROM tmp_id WHERE modul = '$modul'")->row_array();

//     if ($modul == '') {
//       $result = $CI->db->query("INSERT INTO tmp_id (modul, tgl_id, no_id) VALUES ('$modul', '$date_now', '$no_id')");
//     }else{
//       $result = $CI->db->query("UPDATE tmp_id SET tgl_id = '$date_now', no_id = '$no_id' WHERE modul = '$modul'");
//     }

//     return $result;
//   }
// }

if (!function_exists('get_kelompokumur')) {
  function get_kelompokumur($val)
  {
    $CI = get_instance();

    $tgl = date_create($val);
    $now = date_create(date('Y-m-d'));

    $diff = date_diff($tgl, $now);

    $a = intval($diff->format('%a'));
    $y = intval($diff->format('%y'));
    if ($a <= 365) {
      $kode = $CI->db->query("SELECT * FROM mst_kelompok_umur WHERE rentang_bawah <= $a AND rentang_atas >= $a AND rentang_satuan='hari'")->row_array();
    } else {
      $kode = $CI->db->query("SELECT * FROM mst_kelompok_umur WHERE rentang_bawah <= $y AND rentang_atas >= $y AND rentang_satuan='tahun'")->row_array();
    }

    return $kode['kelompokumur_id'];
  }
}

if (!function_exists('get_wilayah')) {
  function get_wilayah($wilayah = null, $type = null)
  {
    $result = explode("#", $wilayah);
    if ($type == 'id') {
      return @$result[0];
    } else {
      return @$result[1];
    }
  }
}

if (!function_exists('get_wilayah_id_bpjs')) {
  function get_wilayah_id_bpjs($wilayah = '')
  {
    if (@$wilayah != '') {
      $result = explode("#", $wilayah);
      return @$result[2];
    } else {
      return '';
    }
  }
}

if (!function_exists('get_parameter_value')) {
  function get_parameter_value($parameter_field = '', $parameter_cd = '')
  {
    $CI = get_instance();

    $result = $CI->db->query("SELECT * FROM mst_parameter WHERE parameter_field = '$parameter_field' AND parameter_cd = '$parameter_cd'")->row_array();
    return $result['parameter_val'];
  }
}

if (!function_exists('get_last_pasien_id')) {
  function get_last_pasien_id()
  {
    $CI = get_instance();

    $result = $CI->db->query("SELECT MAX(pasien_id) AS max_pasien_id FROM mst_pasien")->row_array();
    return sprintf('%06s', $result['max_pasien_id'] + 1);
  }
}

if (!function_exists('get_wilayah_id')) {
  function get_wilayah_id($wilayah_id = null, $type = null)
  {
    $result = explode(".", $wilayah_id);
    if ($type == 'provinsi') {
      return @$result[0];
    } else if ($type == 'kabupaten') {
      return @$result[0] . '.' . @$result[1];
    } else if ($type == 'kecamatan') {
      return @$result[0] . '.' . @$result[1] . '.' . @$result[2];
    } else if ($type == 'kelurahan') {
      return @$wilayah_id;
    } else {
      return @$wilayah_id;
    }
  }
}

if (!function_exists('trash')) {
  function trash($table = null, $field = null)
  {
    $CI = get_instance();
    $trash = $CI->load->database('trash', TRUE);

    if ($field != null) {
      foreach ($field as $k => $v) {
        $CI->db->where($k, $v);
      };
      $data = $CI->db->get($table)->row_array();

      try {
        $trash->insert($table, $data);
      } catch (Exception $e) {
        log_message('error', $e->getMessage());
        return;
      }
    }
  }
}

if (!function_exists('selisih_hari')) {
  function selisih_hari($tgl_awal = '', $tgl_akhir = '')
  {
    $diff = strtotime($tgl_awal) - strtotime($tgl_akhir);
    $result = floor($diff / (60 * 60 * 24));
    return $result + 1;
  }
}

if (!function_exists('list_bulan')) {
  function list_bulan()
  {
    $data = array(
      '01' => 'Januari',
      '02' => 'Februari',
      '03' => 'Maret',
      '04' => 'April',
      '05' => 'Mei',
      '06' => 'Juni',
      '07' => 'Juli',
      '08' => 'Agustus',
      '09' => 'September',
      '10' => 'Oktober',
      '11' => 'November',
      '12' => 'Desember',
    );
    return $data;
  }
}

if (!function_exists('list_tahun')) {
  function list_tahun()
  {
    $data = array(
      '2019' => '2019',
      '2020' => '2020',
      '2021' => '2021',
      '2022' => '2022',
    );
    return $data;
  }
}

if (!function_exists('list_bulan_singkat')) {
  function list_bulan_singkat()
  {
    $data = array(
      '01' => 'Jan',
      '02' => 'Feb',
      '03' => 'Mar',
      '04' => 'Apr',
      '05' => 'Mei',
      '06' => 'Jun',
      '07' => 'Jul',
      '08' => 'Agu',
      '09' => 'Sep',
      '10' => 'Okt',
      '11' => 'Nov',
      '12' => 'Des',
    );
    return $data;
  }
}

if (!function_exists('get_bulan')) {
  function get_bulan($bln)
  {
    switch ($bln) {
      case 1:
        return "Januari";
        break;
      case 2:
        return "Februari";
        break;
      case 3:
        return "Maret";
        break;
      case 4:
        return "April";
        break;
      case 5:
        return "Mei";
        break;
      case 6:
        return "Juni";
        break;
      case 7:
        return "Juli";
        break;
      case 8:
        return "Agustus";
        break;
      case 9:
        return "September";
        break;
      case 10:
        return "Oktober";
        break;
      case 11:
        return "November";
        break;
      case 12:
        return "Desember";
        break;
    }
  }
}

if (!function_exists('terbilang')) {
  function terbilang($x)
  {
    $x = abs((int) $x);
    if ($x >= 0) {

      $abil = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
      if ($x < 12) {
        return " " . $abil[$x];
      } elseif ($x < 20) {
        return terbilang($x - 10) . " Belas";
      } elseif ($x < 100) {
        return terbilang($x / 10) . " Puluh" . terbilang($x % 10);
      } elseif ($x < 200) {
        return " Seratus" . terbilang($x - 100);
      } elseif ($x < 1000) {
        return terbilang($x / 100) . " Ratus" . terbilang($x % 100);
      } elseif ($x < 2000) {
        return " Seribu" . terbilang($x - 1000);
      } elseif ($x < 1000000) {
        return terbilang($x / 1000) . " Ribu" . terbilang($x % 1000);
      } elseif ($x < 1000000000) {
        return terbilang($x / 1000000) . " Juta" . terbilang($x % 1000000);
      } else {
        return '';
      }
    }
  }
}

if (!function_exists('bpjs_service')) {
  function bpjs_service($type = null, $method = null, $url = null, $data_str = null)
  {
    $CI = get_instance();
    $conf = $CI->db->get('bpjs_config')->row_array();

    $data = $conf['cons_id'];
    $secretKey = $conf['secret_key'];
    // Computes the timestamp
    date_default_timezone_set('UTC');
    $tStamp = strval(time() - strtotime('1970-01-01 00:00:00'));
    // Computes the signature by hashing the salt with the secret key as the key
    $signature = hash_hmac('sha256', $data . "&" . $tStamp, $secretKey, true);

    // base64 encode…
    $encodedSignature = base64_encode($signature);

    // urlencode…
    // $encodedSignature = urlencode($encodedSignature);

    // echo "X-cons-id: " .$data ." ";
    // echo "X-timestamp:" .$tStamp ." ";
    // echo "X-signature: " .$encodedSignature;

    switch ($type) {
      case 'aplicare':
        $base_url = $conf['aplicare_base_url'];
        $service_name = $conf['aplicare_service_name'];
        break;

      default:
        # code...
        break;
    }

    $url_str = $base_url . $service_name . '/' . $url;

    $curl = curl_init();

    $curl_conf = array(
      CURLOPT_URL => "$url_str",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "$method",
      CURLOPT_HTTPHEADER => array(
        "X-cons-id: $data",
        "X-timestamp: $tStamp",
        "X-signature: $encodedSignature"
      )
    );

    if ($method == 'POST') {
      $curl_conf[CURLOPT_POSTFIELDS] = "$data_str";
      $curl_conf[CURLOPT_HTTPHEADER] = array(
        "X-cons-id: $data",
        "X-timestamp: $tStamp",
        "X-signature: $encodedSignature",
        "Content-Type: application/json"
      );
    }
    // echo $data_str;
    // var_dump($curl_conf);
    // die();

    curl_setopt_array($curl, $curl_conf);

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
  }
}

if (!function_exists('to_rome')) {
  function to_rome($number)
  {
    $map = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
    $returnValue = '';
    while ($number > 0) {
      foreach ($map as $roman => $int) {
        if ($number >= $int) {
          $number -= $int;
          $returnValue .= $roman;
          break;
        }
      }
    }
    return $returnValue;
  }
}

if (!function_exists('clean_url_news')) {
  function clean_url_news($url = null)
  {
    $url = strtolower($url);
    $url = anti_injection_news($url);
    return $url;
  }
}

if (!function_exists('anti_injection_news')) {
  function anti_injection_news($str = null)
  {
    $str = str_replace('"', "", $str);
    $str = str_replace("'", '', $str);
    $str = str_replace("`", '', $str);
    $str = str_replace(" ", '-', $str);
    $str = str_replace("/", '', $str);
    $str = str_replace(",", '', $str);
    $str = str_replace("&", '', $str);
    $str = str_replace('“', '', $str);
    $str = str_replace('”', '', $str);
    $str = str_replace('(', '', $str);
    $str = str_replace(')', '', $str);
    $str = str_replace('{', '', $str);
    $str = str_replace('}', '', $str);
    $str = str_replace('.', '', $str);
    $str = str_replace(':', '', $str);
    $str = str_replace(';', '', $str);
    $str = str_replace('<', '', $str);
    $str = str_replace('>', '', $str);
    $str = str_replace('[', '', $str);
    $str = str_replace(']', '', $str);
    $str = str_replace('%', '', $str);
    $str = str_replace('$', '', $str);
    $str = str_replace('!', '', $str);
    $str = str_replace('`', '', $str);
    $str = str_replace('~', '', $str);
    $str = str_replace('=', '', $str);
    $str = str_replace('+', '', $str);
    $str = str_replace('_', '', $str);
    $str = str_replace('*', '', $str);
    $str = str_replace('^', '', $str);
    $str = str_replace('#', '', $str);
    $str = str_replace('@', '', $str);
    $str = str_replace('script', '', $str);
    //
    $str = strip_tags($str);
    $str = htmlentities($str);
    return $str;
  }
}

if (!function_exists('anti_injection')) {
  function anti_injection($str = null)
  {
    $str = str_replace('"', "", $str);
    $str = str_replace("'", '', $str);
    $str = str_replace("`", '', $str);
    $str = str_replace(" ", '', $str);
    $str = str_replace("/", '', $str);
    $str = str_replace(",", '', $str);
    $str = str_replace("&", '', $str);
    $str = str_replace('“', '', $str);
    $str = str_replace('”', '', $str);
    $str = str_replace('(', '', $str);
    $str = str_replace(')', '', $str);
    $str = str_replace('{', '', $str);
    $str = str_replace('}', '', $str);
    $str = str_replace('.', '', $str);
    $str = str_replace(':', '', $str);
    $str = str_replace(';', '', $str);
    $str = str_replace('<', '', $str);
    $str = str_replace('>', '', $str);
    $str = str_replace('[', '', $str);
    $str = str_replace(']', '', $str);
    $str = str_replace('%', '', $str);
    $str = str_replace('$', '', $str);
    $str = str_replace('!', '', $str);
    $str = str_replace('`', '', $str);
    $str = str_replace('~', '', $str);
    $str = str_replace('=', '', $str);
    $str = str_replace('+', '', $str);
    $str = str_replace('_', '', $str);
    $str = str_replace('*', '', $str);
    $str = str_replace('^', '', $str);
    $str = str_replace('#', '', $str);
    $str = str_replace('@', '', $str);
    $str = str_replace('script', '', $str);
    //
    $str = strip_tags($str);
    $str = htmlentities($str);
    return $str;
  }
}

if (!function_exists('jecho')) {
  function jecho($a, $b, $str)
  {
    if ($a == $b) {
      echo $str;
    }
  }
}

if (!function_exists('color_chart')) {
  function color_chart($no)
  {
    if ($no == 1) {
      $color = 'rgb(255, 99, 132)';
    } elseif ($no == 2) {
      $color = 'rgb(255, 159, 64)';
    } elseif ($no == 3) {
      $color = 'rgb(255, 205, 86)';
    } elseif ($no == 4) {
      $color = 'rgb(75, 192, 192)';
    } elseif ($no == 5) {
      $color = 'rgb(54, 162, 235)';
    } elseif ($no == 6) {
      $color = 'rgb(153, 102, 255)';
    } elseif ($no == 7) {
      $color = 'rgb(201, 203, 207)';
    } elseif ($no == 8) {
      $color = 'rgb(255, 99, 132)';
    } elseif ($no == 9) {
      $color = 'rgb(255, 159, 64)';
    } elseif ($no == 10) {
      $color = 'rgb(255, 205, 86)';
    } elseif ($no == 11) {
      $color = 'rgb(75, 192, 192)';
    } elseif ($no == 12) {
      $color = 'rgb(54, 162, 235)';
    } elseif ($no == 13) {
      $color = 'rgb(153, 102, 255)';
    } elseif ($no == 14) {
      $color = 'rgb(201, 203, 207)';
    } elseif ($no == 15) {
      $color = 'rgb(255, 99, 132)';
    } elseif ($no == 16) {
      $color = 'rgb(255, 159, 64)';
    } elseif ($no == 17) {
      $color = 'rgb(255, 205, 86)';
    } elseif ($no == 18) {
      $color = 'rgb(75, 192, 192)';
    } elseif ($no == 19) {
      $color = 'rgb(54, 162, 235)';
    } elseif ($no == 20) {
      $color = 'rgb(153, 102, 255)';
    } else {
      $color = 'rgb(255, 99, 132)';
    }
    //
    return $color;
  }
}

if (!function_exists('for_minus')) {
  function for_minus($x)
  {
    for ($i = 0; $i <= $x; $i++) {
      echo '-';
    }
  }
}

if (!function_exists('get_username')) {
  function get_username($pegawai_nm)
  {
    $arr = explode(' ', $pegawai_nm);
    return strtolower($arr[0]);
  }
}

if (!function_exists('get_kelas_nm')) {
  function get_kelas_nm($kelas_nm)
  {
    $arr = explode(' ', $kelas_nm);
    if ($arr[0] == 'kelas' || $arr[0] == 'KELAS' || $arr[0] == 'Kelas') {
      $result = $kelas_nm;
    } else {
      $result = 'Kelas ' . $kelas_nm;
    }
    return $result;
  }
}

if (!function_exists('get_role_id')) {
  function get_role_id($jenispegawai_cd)
  {
    if ($jenispegawai_cd == '01') {
      // DIREKTUR            
      $result = 8;
    } elseif ($jenispegawai_cd == '02') {
      // DOKTER              
      $result = 20;
    } elseif ($jenispegawai_cd == '03') {
      // PERAWAT             
      $result = 21;
    } elseif ($jenispegawai_cd == '04') {
      // ADMINISTRASI        
      $result = 22;
    } elseif ($jenispegawai_cd == '05') {
      // APOTEKER            
      $result = 7;
    } elseif ($jenispegawai_cd == '06') {
      // ASISTEN APOTEKER    
      $result = 7;
    } elseif ($jenispegawai_cd == '07') {
      // AS. PERAWAT         
      $result = 21;
    } elseif ($jenispegawai_cd == '14') {
      // RADIOLOG            
      $result = 23;
    } elseif ($jenispegawai_cd == '17') {
      // REKAM MEDIK / FO
      $result = 24;
    } elseif ($jenispegawai_cd == '18') {
      // KEUANGAN
      $result = 19;
    } elseif ($jenispegawai_cd == '19') {
      // KEPEGAWAIAN
      $result = 25;
    } elseif ($jenispegawai_cd == '20') {
      // IT
      $result = 1;
    } elseif ($jenispegawai_cd == '21') {
      // AHLI GIZI
      $result = 20;
    } elseif ($jenispegawai_cd == '24') {
      // BIDAN
      $result = 27;
    } elseif ($jenispegawai_cd == '25') {
      // ANALIS KESEHATAN
      $result = 20;
    }
    return $result;
  }
}

function post_data($url, $postVars = array())
{
  //Transform our POST array into a URL-encoded query string.
  $postStr = http_build_query($postVars);
  //Create an $options array that can be passed into stream_context_create.
  $options = array(
    'http' =>
    array(
      'method'  => 'POST', //We are using the POST HTTP method.
      'header'  => 'Content-type: application/x-www-form-urlencoded',
      'content' => $postStr //Our URL-encoded query string.
    )
  );
  //Pass our $options array into stream_context_create.
  //This will return a stream context resource.
  $streamContext  = stream_context_create($options);
  //Use PHP's file_get_contents function to carry out the request.
  //We pass the $streamContext variable in as a third parameter.
  $result = file_get_contents($url, false, $streamContext);
  //If $result is FALSE, then the request has failed.
  if ($result === false) {
    //If the request failed, throw an Exception containing
    //the error.
    $error = error_get_last();
    throw new Exception('POST request failed: ' . $error['message']);
  }
  //If everything went OK, return the response.
  return $result;
}

if (!function_exists('convert_suhu')) {
  function convert_suhu($str = null)
  {
    $str = str_replace(',', '.', $str);
    //
    $str = strip_tags($str);
    $str = htmlentities($str);
    return $str;
  }
}

if (!function_exists('pasaran_jawa')) {
  function pasaran_jawa($tgl_src = null)
  {
    // dipilih tanggal 03 Januari 2000 sebagai acuan
    // hari pasaran tanggal 03 Januari 2000 adalah 'Pon'
    $tgl1 = "2000-01-03";
    $tgl2 = $tgl_src;
    // array urutan nama hari pasaran dimulai dari 'Pon' 
    $pasaran = array('pon', 'wage', 'kliwon', 'legi', 'pahing');
    // proses mencari selisih hari antara kedua tanggal 
    $pecah1 = explode("-", $tgl1);
    $date1 = $pecah1[2];
    $month1 = $pecah1[1];
    $year1 = $pecah1[0];

    $pecah2 = explode("-", $tgl2);
    $date2 = $pecah2[2];
    $month2 = $pecah2[1];
    $year2 =  $pecah2[0];

    $jd1 = GregorianToJD($month1, $date1, $year1);
    $jd2 = GregorianToJD($month2, $date2, $year2);

    $selisih = $jd2 - $jd1;
    $mod = $selisih % 5;
    //
    return $pasaran[$mod];
  }
}

if (!function_exists('date_dayname')) {
  function date_dayname($id = null)
  {
    $arr = array(
      'Sun' => 'Minggu',
      'Mon' => 'Senin',
      'Tue' => 'Selasa',
      'Wed' => 'Rabu',
      'Thu' => 'Kamis',
      'Fri' => 'Jumat',
      'Sat' => 'Sabtu',
    );
    if ($id != '') return $arr[$id];
    else return $arr;
  }
}

if (!function_exists('to_dayname')) {
  function to_dayname($date = null)
  {
    if ($date == '') $date = date('Y-m-d');
    $datetime = DateTime::createFromFormat('Y-m-d', $date);
    $dayindex = $datetime->format('D');
    $dayname  = @date_dayname($dayindex);
    //
    $result = $dayname;
    return $result;
  }
}
