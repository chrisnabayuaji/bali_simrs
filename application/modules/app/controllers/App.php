<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class App extends MX_Controller {

  public function search($nav_id)
  {
    $nav = $this->m_app->_get_nav($nav_id);
    $data = $this->input->post(null,true);
    if ($data == null) redirect(site_url().'/message/error_403');
    $cookie = get_cookie_nav($nav_id);
    $cookie['search'] = $data;
    set_cookie_nav($nav_id,$cookie);
    redirect(site_url().'/'.$nav['nav_url']);
  }

  public function search_redirect($id)
  {
    $data = $this->input->post(null,true);
    if ($data == null) redirect(site_url().'/message/error_403');
    $cookie = get_cookie_nav($id);
    $cookie['search'] = $data;
    set_cookie_nav($id,$cookie);
    redirect(site_url().'/'.$data['redirect']);
  }

  public function reset($nav_id)
  {
    $nav = $this->m_app->_get_nav($nav_id);
    del_cookie_nav($nav_id);
    redirect(site_url().'/'.$nav['nav_url']);
  }

  public function reset_redirect($nav_id, $redirect)
  {
    del_cookie_nav($nav_id);
    $redirect = str_replace('-','/',$redirect);
    redirect(site_url().'/'.$redirect);
  }

  public function per_page($nav_id)
  {
    $nav = $this->m_app->_get_nav($nav_id);
    $data = $this->input->post(null,true);
    if ($data == null) redirect(site_url().'/message/error_403');
    $cookie = get_cookie_nav($nav_id);
    $cookie['per_page'] = $data['per_page'];
    set_cookie_nav($nav_id,$cookie);
    redirect(site_url().'/'.$nav['nav_url']);
  }

  public function order($nav_id, $field, $type)
  {
    $nav = $this->m_app->_get_nav($nav_id);
    $cookie = get_cookie_nav($nav_id);
    $cookie['order'] = array(
      'field' => $field,
      'type' => $type
    );
    set_cookie_nav($nav_id,$cookie);
    redirect(site_url().'/'.$nav['nav_url']);
  }

  public function form_modal() {
    $data_url = str_replace('/', '__', $this->input->post('data_url'));
    $data['main'] = $this->m_app->_get_profile_user_login();
    $data['form_action'] = site_url().'/app/app/save/'.$data['main']['user_id'].'/'.$data_url;
      
    echo json_encode(array(
      'html' => $this->load->view('app/app/form_modal', $data, true)
    ));
  }

  public function save($id = null, $data_url = null)
  {
    $this->m_app->update_user($id);
    $data_url = str_replace('__', '/', $data_url);
    redirect($data_url);
  }

}