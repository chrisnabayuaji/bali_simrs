<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MX_Controller {

  function __construct(){
    parent::__construct();
		$this->load->model(
      'app/m_app'
    );
  }

  public function index() {
    redirect(site_url().'/app/auth/login');
  }

  public function login()
  {
    session_destroy();
    $data['config'] = $this->m_app->_get_config();
    $data['identitas'] = $this->m_app->_get_identitas();
    $this->load->view('login/index',$data);
  }

  public function login_action()
  {
    $res = $this->m_app->_login();
    switch ($res) {
      case 0:
        $this->session->set_flashdata('flash_error', 'Nama Pengguna dan Kata Sandi tidak cocok.');
        redirect(site_url().'/app/auth/login');
        break;
      
      case 1:
        redirect(site_url().'/app/dashboard');
        break;

      case -1:
        $this->session->set_flashdata('flash_error', 'Akun tidak aktif.');
        redirect(site_url().'/app/auth/login');
        break;

      case -2:
        $this->session->set_flashdata('flash_error', 'Akun tidak ditemukan.');
        redirect(site_url().'/app/auth/login');
        break;
    }
  }

  public function logout_action()
	{
		session_destroy();
		redirect(site_url().'/app/auth/login');
	}

}