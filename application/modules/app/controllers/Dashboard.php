<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

	var $nav_id = '01', $nav;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'm_dashboard',
			'm_nav'
		));
		$this->nav = $this->m_app->_get_nav($this->nav_id);
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		$data['nav'] = $this->nav;

		$data['list_nav'] = $this->m_nav->list_nav_all();
		$data['list_nav_module'] = $this->m_nav->list_nav_module();

		$this->render('app/dashboard/dashboard', $data);
	}

	function profil_rumah_sakit()
	{
		$data['identitas'] = $this->m_dashboard->identitas();
		echo json_encode(array(
			'html' => $this->load->view('app/dashboard/profil_rumah_sakit', $data, true)
		));
	}

	function statistik_rumah_sakit()
	{
		$data['rawat_jalan'] = $this->m_dashboard->rawat_jalan();
		$data['rawat_inap_atas'] = $this->m_dashboard->rawat_inap_atas();
		$data['rawat_inap_bawah'] = $this->m_dashboard->rawat_inap_bawah();
		$data['rawat_darurat'] = $this->m_dashboard->rawat_darurat();
		$data['pasien_pulang_hari_ini'] = $this->m_dashboard->pasien_pulang_hari_ini();
		$data['pasien_masih_inap'] = $this->m_dashboard->pasien_masih_inap();
		echo json_encode(array(
			'html' => $this->load->view('app/dashboard/statistik_rumah_sakit', $data, true)
		));
	}

	function grafik_kunjungan_pasien()
	{
		$data['list_kunjungan_harian'] = $this->m_dashboard->list_kunjungan_harian();
		echo json_encode(array(
			'html' => $this->load->view('app/dashboard/grafik_kunjungan_pasien', $data, true)
		));
	}

	function list_berita($p = 1, $o = 0)
	{
		$data['p'] = $p;
		$data['o'] = $o;
		$data['nav'] = $this->nav;
		$data['paging'] = $this->m_dashboard->paging_berita($p, $o);
		$data['main'] = $this->m_dashboard->list_berita($o, $data['paging']->offset, $data['paging']->per_page);
		echo json_encode(array(
			'html' => $this->load->view('app/dashboard/list_berita', $data, true)
		));
	}

	public function baca_berita($id = null)
	{
		$this->m_dashboard->update_hit($id);
		$data['id'] = $id;
		$data['main'] = $this->m_dashboard->get_berita($id);
		$data['first_img'] = $this->m_dashboard->get_image_first($id);
		$data['list_img'] = $this->m_dashboard->list_img_berita($id);

		echo json_encode(array(
			'html' => $this->load->view('app/dashboard/baca_berita', $data, true)
		));
	}
}
