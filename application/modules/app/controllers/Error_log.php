<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Error_log extends MY_Controller
{

  var $nav_id = '02.05.09', $nav, $cookie;

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'm_error_log',
    ));
    $this->nav = $this->m_app->_get_nav($this->nav_id);

    //cookie
    $this->cookie = get_cookie_nav($this->nav_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '', 'role_id' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'user_id', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
  }

  public function index()
  {
    $this->authorize($this->nav, '_view');
    //cookie
    $this->cookie['cur_page'] = $this->uri->segment(4, 0);
    // $this->cookie['total_rows'] = $this->m_user->all_rows($this->cookie);
    set_cookie_nav($this->nav_id, $this->cookie);
    //main data
    $data['nav'] = $this->nav;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_error_log->dir_array();
    $this->render('app/error_log/index', $data);
  }

  public function form_modal($file_name = null)
  {
    $this->authorize($this->nav, ($file_name != '') ? '_update' : '_add');

    $data['file_name'] = $file_name;
    $data['nav'] = $this->nav;
    $filename = "application/logs/" . $file_name;
    $data['main'] = file($filename, FILE_IGNORE_NEW_LINES);

    echo json_encode(array(
      'html' => $this->load->view('app/error_log/form_modal', $data, true)
    ));
  }
}
