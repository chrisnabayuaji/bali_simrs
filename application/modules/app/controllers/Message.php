<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Message extends CI_Controller{

	function __construct() {
		parent::__construct();
	}

	function index() {			
		$data['config'] = $this->App->_get_config();
		$this->load->view('app/template/message/error',$data);		
	}

	function error_403() {			
		$data['config'] = $this->m_app->_get_config();
		$this->load->view('app/template/message/error', $data);		
	}
	
}