<?php 
if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Nav extends MY_Controller{

	var $nav_id = '02.05.03', $nav, $cookie;
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array(
			'm_nav'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
    $this->cookie = get_cookie_nav($this->nav_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '', 'nav_nm' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'nav_id','type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}
	
	public function index() {	
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(4, 0);
    $this->cookie['total_rows'] = $this->m_nav->all_rows($this->cookie);
    set_cookie_nav($this->nav_id, $this->cookie);
    //main data
    $data['nav'] = $this->nav;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_nav->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
    //set pagination
    set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('app/nav/index',$data);
	}

	public function form_modal($id=null) {
    $this->authorize($this->nav, ($id != '' ) ? '_update' : '_add');
		
		if($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_nav->get_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['list_nav'] = $this->m_nav->list_nav_all();
		$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/save/'.$id;
			
		echo json_encode(array(
			'html' => $this->load->view('app/nav/form_modal', $data, true)
		));
	}
	
	public function save($id = null)
	{
		$this->m_nav->save($id);
		create_log(($id != '' ) ? '_update' : '_add', $this->nav_id);
		redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	public function delete($id = null)
	{
		$this->m_nav->delete($id, true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log('_delete', $this->nav_id);
		redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	public function multiple($type = null)
	{
		$data = $this->input->post();
    if(isset($data['checkitem'])){
      foreach ($data['checkitem'] as $key) {
        switch ($type) {					
					case 'delete':
						$this->authorize($this->nav, '_delete');
            $this->m_nav->delete($key, true);
						$flash = 'Data berhasil dihapus.';
						create_log('_delete', $this->nav_id);
            break;

					case 'enable':
						$this->authorize($this->nav, '_update');
            $this->m_nav->update($key, array('nav_st' => 1));
						$flash = 'Data berhasil diaktifkan.';
						create_log('_update', $this->nav_id);
            break;

					case 'disable':
						$this->authorize($this->nav, '_update');
            $this->m_nav->update($key, array('nav_st' => 0));
						$flash = 'Data berhasil dinonaktifkan.';
						create_log('_delete', $this->nav_id);
            break;
        }
      }
    }
    $this->session->set_flashdata('flash_success', $flash);
    redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	function ajax($type=null, $id=null) {
		if($type == 'get_nav_order') {
			$parent_id = $this->input->get('parent_id');
			$result = $this->m_nav->get_nav_order($parent_id);
			echo json_encode(array(
				'result' => $result
			));
		}elseif($type == 'delete_photo') {
			$nav_id = html_escape($this->input->get('nav_id',true));
			$nav_icon = html_escape($this->input->get('nav_icon',true));
			//
			$result = $this->m_nav->delete_photo($nav_id, $nav_icon);
			$callback = 'false';
			if($result) $callback = 'true';
			//
			echo json_encode(array(
				'callback' => $callback
			));
		}elseif ($type = 'cek_id') {
			$data = $this->input->post();
			$cek = $this->m_nav->get_data($data['nav_id']);
			if ($id == null) {
				if ($cek == null) {
					echo 'true';
				}else{
					echo 'false';
				}
			}else{
				if ($id != $data['nav_id'] && $cek != null) {
					echo 'false';
				}else{
					echo 'true';
				}
			}
		}
	}
	
}