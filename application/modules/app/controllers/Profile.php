<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends MY_Controller
{

	var $nav_id = '02.05.05', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'm_profile',
			'master/m_wilayah'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id);
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'role_id', 'type' => 'asc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form');
	}

	public function form()
	{
		$this->authorize($this->nav, '_update');

		$data['nav'] = $this->nav;
		$data['main'] = $this->m_profile->get_first();
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save';
		$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_parent('');
		$this->render('app/profile/form', $data);
	}

	public function save($type = '', $val = '')
	{
		$this->m_profile->update($type, $val);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log('_update', $this->nav_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	public function auth_reset()
	{
		$this->authorize($this->nav, '_update');

		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/form';

		echo json_encode(array(
			'html' => $this->load->view('app/profile/auth_reset', $data, true)
		));
	}

	public function reset()
	{
		$this->db->query("TRUNCATE reg_pasien");
		$this->db->query("TRUNCATE reg_pasien_kamar");
		$this->db->query("TRUNCATE reg_pasien_online");
		$this->db->query("TRUNCATE reg_pasien_pulang");
		$this->db->query("TRUNCATE reg_pasien_pulang_rinc");
		$this->db->query("TRUNCATE reg_pasien_tindaklanjut");

		$this->db->query("TRUNCATE lkt_antrian");

		$this->db->query("TRUNCATE lab_pemeriksaan");
		$this->db->query("TRUNCATE lab_pemeriksaan_rinc");

		$this->db->query("TRUNCATE rad_pemeriksaan");
		$this->db->query("TRUNCATE rad_pemeriksaan_rinc");

		$this->db->query("TRUNCATE ibs_pemeriksaan");
		$this->db->query("TRUNCATE ibs_pemeriksaan_rinc");
		$this->db->query("TRUNCATE icu_pemeriksaan");
		$this->db->query("TRUNCATE hd_pemeriksaan");
		$this->db->query("TRUNCATE vk_pemeriksaan");


		$this->db->query("TRUNCATE dat_alkes");
		$this->db->query("TRUNCATE dat_anamnesis");
		$this->db->query("TRUNCATE dat_bhp");
		$this->db->query("TRUNCATE dat_billing");
		$this->db->query("TRUNCATE dat_billing_group");
		$this->db->query("TRUNCATE dat_billing_rinc");
		$this->db->query("TRUNCATE dat_catatanmedis");
		$this->db->query("TRUNCATE dat_diagnosis");
		$this->db->query("TRUNCATE dat_resep");
		$this->db->query("TRUNCATE dat_resep_group");
		$this->db->query("TRUNCATE dat_resep_racik");
		$this->db->query("TRUNCATE dat_resep_rinc");
		$this->db->query("TRUNCATE dat_farmasi");
		$this->db->query("TRUNCATE dat_tindakan");

		$this->db->query("TRUNCATE trx_jurnal");
		$this->db->query("TRUNCATE trx_jurnal_debet");
		$this->db->query("TRUNCATE trx_jurnal_kredit");

		// $this->db->query("TRUNCATE far_distribusi");
		// $this->db->query("TRUNCATE far_order");
		// $this->db->query("TRUNCATE far_order_rinc");
		// $this->db->query("TRUNCATE far_penerimaan");
		// $this->db->query("TRUNCATE far_penerimaan_rinc");
		// $this->db->query("TRUNCATE far_retur_penerimaan");
		// $this->db->query("TRUNCATE far_retur_penerimaan_rinc");
		// $this->db->query("TRUNCATE far_stok");
		// $this->db->query("TRUNCATE far_stok_depo");
		// $this->db->query("TRUNCATE far_stok_history");
		// $this->db->query("TRUNCATE far_stok_history_depo");

		$this->db->query("TRUNCATE tmp_id");

		//reset kamar
		$this->db->query("UPDATE mst_kamar SET jml_bed_terpakai = 0, jml_bed_kosong=jml_bed");

		echo json_encode('1');
		// redirect(site_url() . '/' . $this->nav['nav_url'] . '/index');
	}

	public function migration($type = null)
	{
		$this->m_profile->migration($type);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index');
	}

	public function migrasi_tarif()
	{
		ini_set('memory_limit', '-1');

		//Configuration -------------------------------------------------------------------------
		$this->load->file(APPPATH . 'libraries/PHPExcel.php');
		$tarif_tindakan = FCPATH . 'assets/daftarharga.xlsx';
		$PHPExcel = PHPExcel_IOFactory::load($tarif_tindakan);

		$sheet = $PHPExcel->getActiveSheet()->toArray(null, true, true, true);

		$i = 1;
		//truncate mst_tarif
		$this->db->query("TRUNCATE mst_tarif");
		$this->db->query("TRUNCATE mst_tarif_kelas");
		foreach ($sheet as $row) {
			$tarif_tp = (strlen($row['A']) == 9) ? 'D' : 'G';
			if ($i > 1 && $row['A'] != '') {
				$d = array(
					"tarif_id" => $row['A'],
					"parent_id" => $row['B'],
					"tarif_nm" => strtoupper($row['C']),
					"tarif_tp" => $tarif_tp,
					"tarif_st" => 1,
					"is_kwitansi" => 1,
					"created_by" => 'import 2020-06-02',
					"created_at" => date('Y-m-d H:i:s')
				);
				//insert to mst_tarif
				$this->db->insert('mst_tarif', $d);
				//kelas3 06 e
				$tk3 = array(
					"tarifkelas_id" => $d['tarif_id'] . '.06',
					"tarif_id" => $d['tarif_id'],
					"kelas_id" => "06",
					"nominal" => $row['D']
				);
				$this->db->insert('mst_tarif_kelas', $tk3);
				//kelas2 05 f
				$tk2 = array(
					"tarifkelas_id" => $d['tarif_id'] . '.05',
					"tarif_id" => $d['tarif_id'],
					"kelas_id" => "05",
					"nominal" => $row['E']
				);
				$this->db->insert('mst_tarif_kelas', $tk2);
				//kelas1 04 g
				$tk1 = array(
					"tarifkelas_id" => $d['tarif_id'] . '.04',
					"tarif_id" => $d['tarif_id'],
					"kelas_id" => "04",
					"nominal" => $row['F']
				);
				$this->db->insert('mst_tarif_kelas', $tk1);
				//vip 02 h
				$tkv = array(
					"tarifkelas_id" => $d['tarif_id'] . '.02',
					"tarif_id" => $d['tarif_id'],
					"kelas_id" => "02",
					"nominal" => $row['G']
				);
				$this->db->insert('mst_tarif_kelas', $tkv);
			}
			$i++;
		}
	}

	public function migrasi_obat()
	{
		//kosongkan data master obat
		$this->db->query("TRUNCATE mst_obat");
		//import excel
		ini_set('memory_limit', '-1');

		//Configuration -------------------------------------------------------------------------
		$this->load->file(APPPATH . 'libraries/PHPExcel.php');
		$excel = FCPATH . 'assets/daftarharga.xlsx';
		$PHPExcel = PHPExcel_IOFactory::load($excel);
		$PHPExcel->setActiveSheetIndex(1);
		$sheet = $PHPExcel->getActiveSheet()->toArray(null, true, true, true);

		$id = 1;
		$k = 1;
		foreach ($sheet as $row) {
			if ($k > 1) {
				$obat = array(
					'obat_id' => '01.' . ($id++),
					'obat_nm' => $row['A'],
					'no_batch' => '-',
					'harga_beli' => $row['B'],
					'harga_jual' => $row['C'],
					'harga_akhir' => $row['C'],
					'stok_minimal' => 100,
					'jenisbarang_cd' => '01',
					'tgl_catat' => date('Y-m-d'),
					'created_at' => date('Y-m-d H:i:s'),
					'created_by' => 'System'
				);
				$this->db->insert('mst_obat', $obat);
			}
			$k++;
		}
		// looping 
		// isi data berdasarkan excel
		// //kosongkan semua data stok
		$this->db->query("TRUNCATE far_stok");
		$this->db->query("TRUNCATE far_stok_depo");
		// //ambil semua data obat
		// $this->db->query("UPDATE mst_obat SET harga_akhir = harga_jual");
		$obat = $this->db->where('jenisbarang_cd', '01')->get('mst_obat')->result_array();
		$id = date('ymd') . '0001';
		$i = 1;
		foreach ($obat as $row) {
			//masukkan obat ke far_stok
			$ds = array(
				'stok_id' => $id,
				'obat_id' => $row['obat_id'],
				'obat_nm' => $row['obat_nm'],
				'no_batch' => $row['no_batch'],
				'satuan_cd' => $row['satuan_cd'],
				'harga_beli' => $row['harga_beli'],
				'harga_jual' => $row['harga_jual'],
				'harga_akhir' => $row['harga_akhir'],
				'jenisbarang_cd' => $row['jenisbarang_cd'],
				'keterangan_obat' => $row['keterangan_obat'],
				'is_obat_indikator' => $row['is_obat_indikator'],
				'is_napza' => $row['is_napza'],
				'is_generik' => $row['is_generik'],
				'is_antibiotik' => $row['is_antibiotik'],
				'is_injeksi' => $row['is_injeksi'],
				'stok_awal' => 0,
				'stok_penerimaan' => 9999,
				'stok_distribusi' => 0,
				'stok_retur_masuk' => 0,
				'stok_retur_keluar' => 0,
				'stok_musnah' => 0,
				'stok_akhir' => 9999,
				'stok_opname' => 0,
				'tgl_catat' => date('Y-m-d H:i:s'),
				'stok_st' => 1
			);
			$this->db->insert('far_stok', $ds);

			// 	//masukkan obat ke far_stok_depo
			$dd = array(
				'stokdepo_id' => $id,
				'distribusi_id' => $id,
				'stok_id' => $id,
				'lokasi_id' => '06.01.01',
				'obat_id' => $row['obat_id'],
				'obat_nm' => $row['obat_nm'],
				'no_batch' => $row['no_batch'],
				'satuan_cd' => $row['satuan_cd'],
				'harga_beli' => $row['harga_beli'],
				'harga_jual' => $row['harga_jual'],
				'harga_akhir' => $row['harga_akhir'],
				'jenisbarang_cd' => $row['jenisbarang_cd'],
				'keterangan_obat' => $row['keterangan_obat'],
				'is_obat_indikator' => $row['is_obat_indikator'],
				'is_napza' => $row['is_napza'],
				'is_generik' => $row['is_generik'],
				'is_antibiotik' => $row['is_antibiotik'],
				'is_injeksi' => $row['is_injeksi'],
				'stok_awal' => 0,
				'stok_penerimaan' => 9999,
				'stok_terpakai' => 0,
				'stok_retur_keluar' => 0,
				'stok_akhir' => 9999,
				'stok_opname' => 0,
				'tgl_catat' => date('Y-m-d H:i:s'),
				'stok_st' => 1,
			);
			$this->db->insert('far_stok_depo', $dd);

			$id++;
		}

		// //import excel
		// ini_set('memory_limit', '-1');

		// //Configuration -------------------------------------------------------------------------
		// $this->load->file(APPPATH . 'libraries/PHPExcel.php');
		// $tarif_tindakan = FCPATH . 'assets/stok_obat.xlsx';
		// $PHPExcel = PHPExcel_IOFactory::load($tarif_tindakan);
		// $PHPExcel->setActiveSheetIndex(4);
		// $sheet = $PHPExcel->getActiveSheet()->toArray(null, true, true, true);

		// $i = 1;
		// foreach ($sheet as $row) {
		// 	$obt = $this->db->query("SELECT * FROM mst_obat WHERE obat_nm LIKE '%".$row['B']."%'")->row_array();

		// 	//update far_stok
		// 	$fs = array(
		// 		'stok_awal' => 0,
		// 		'stok_penerimaan' => $row['C'],
		// 		'stok_distribusi' => $row['C'],
		// 		'stok_retur_masuk' => 0,
		// 		'stok_retur_keluar' => 0,
		// 		'stok_musnah' => 0,
		// 		'stok_akhir' => 0,
		// 		'stok_opname' => 0,
		// 	);
		// 	$this->db->where('obat_id', $obt['obat_id'])->update('far_stok', $fs);

		// 	//update far_stok_depo
		// 	$fsd = array(
		// 		'stok_penerimaan' => $row['C'],
		// 		'stok_akhir' => $row['C']
		// 	);
		// 	$this->db->where('obat_id', $obt['obat_id'])->update('far_stok_depo', $fsd);
		// };
		// $this->migrasi_bmhp();
		// $this->migrasi_alkes();
	}

	public function migrasi_bmhp()
	{
		//kosongkan data master obat
		// $this->db->query("TRUNCATE mst_obat");
		//import excel
		ini_set('memory_limit', '-1');

		//Configuration -------------------------------------------------------------------------
		$this->load->file(APPPATH . 'libraries/PHPExcel.php');
		$excel = FCPATH . 'assets/daftarharga.xlsx';
		$PHPExcel = PHPExcel_IOFactory::load($excel);
		$PHPExcel->setActiveSheetIndex(2);
		$sheet = $PHPExcel->getActiveSheet()->toArray(null, true, true, true);

		$id = 1;
		foreach ($sheet as $row) {
			$obat = array(
				'obat_id' => '02.' . ($id++),
				'obat_nm' => $row['A'],
				'no_batch' => '-',
				'harga_beli' => 0,
				'harga_jual' => $row['B'],
				'harga_akhir' => $row['B'],
				'stok_minimal' => 100,
				'jenisbarang_cd' => '02',
				'tgl_catat' => date('Y-m-d'),
				'created_at' => date('Y-m-d H:i:s'),
				'created_by' => 'System'
			);
			$this->db->insert('mst_obat', $obat);
		}
		// looping 
		// isi data berdasarkan excel
		// //kosongkan semua data stok
		// $this->db->query("TRUNCATE far_stok");
		// $this->db->query("TRUNCATE far_stok_depo");
		// //ambil semua data obat
		// $this->db->query("UPDATE mst_obat SET harga_akhir = harga_jual");
		$obat = $this->db->where('jenisbarang_cd', '02')->get('mst_obat')->result_array();
		$far_stok = $this->db->query("SELECT * FROM far_stok ORDER BY stok_id DESC")->row_array();
		$id = $far_stok['stok_id'] + 1;
		$i = 1;
		foreach ($obat as $row) {
			//masukkan obat ke far_stok
			$ds = array(
				'stok_id' => $id,
				'obat_id' => $row['obat_id'],
				'obat_nm' => $row['obat_nm'],
				'no_batch' => $row['no_batch'],
				'satuan_cd' => $row['satuan_cd'],
				'harga_beli' => $row['harga_beli'],
				'harga_jual' => $row['harga_jual'],
				'harga_akhir' => $row['harga_akhir'],
				'jenisbarang_cd' => $row['jenisbarang_cd'],
				'keterangan_obat' => $row['keterangan_obat'],
				'is_obat_indikator' => $row['is_obat_indikator'],
				'is_napza' => $row['is_napza'],
				'is_generik' => $row['is_generik'],
				'is_antibiotik' => $row['is_antibiotik'],
				'is_injeksi' => $row['is_injeksi'],
				'stok_awal' => 0,
				'stok_penerimaan' => 9999,
				'stok_distribusi' => 0,
				'stok_retur_masuk' => 0,
				'stok_retur_keluar' => 0,
				'stok_musnah' => 0,
				'stok_akhir' => 9999,
				'stok_opname' => 0,
				'tgl_catat' => date('Y-m-d H:i:s'),
				'stok_st' => 1
			);
			$this->db->insert('far_stok', $ds);

			// 	//masukkan obat ke far_stok_depo
			$dd = array(
				'stokdepo_id' => $id,
				'distribusi_id' => $id,
				'stok_id' => $id,
				'lokasi_id' => '06.01.01',
				'obat_id' => $row['obat_id'],
				'obat_nm' => $row['obat_nm'],
				'no_batch' => $row['no_batch'],
				'satuan_cd' => $row['satuan_cd'],
				'harga_beli' => $row['harga_beli'],
				'harga_jual' => $row['harga_jual'],
				'harga_akhir' => $row['harga_akhir'],
				'jenisbarang_cd' => $row['jenisbarang_cd'],
				'keterangan_obat' => $row['keterangan_obat'],
				'is_obat_indikator' => $row['is_obat_indikator'],
				'is_napza' => $row['is_napza'],
				'is_generik' => $row['is_generik'],
				'is_antibiotik' => $row['is_antibiotik'],
				'is_injeksi' => $row['is_injeksi'],
				'stok_awal' => 0,
				'stok_penerimaan' => 9999,
				'stok_terpakai' => 0,
				'stok_retur_keluar' => 0,
				'stok_akhir' => 9999,
				'stok_opname' => 0,
				'tgl_catat' => date('Y-m-d H:i:s'),
				'stok_st' => 1,
			);
			$this->db->insert('far_stok_depo', $dd);

			$id++;
		}
	}

	public function migrasi_alkes()
	{
		//kosongkan data master obat
		// $this->db->query("TRUNCATE mst_obat");
		//import excel
		ini_set('memory_limit', '-1');

		//Configuration -------------------------------------------------------------------------
		$this->load->file(APPPATH . 'libraries/PHPExcel.php');
		$excel = FCPATH . 'assets/daftarharga.xlsx';
		$PHPExcel = PHPExcel_IOFactory::load($excel);
		$PHPExcel->setActiveSheetIndex(3);
		$sheet = $PHPExcel->getActiveSheet()->toArray(null, true, true, true);

		$id = 1;
		foreach ($sheet as $row) {
			$obat = array(
				'obat_id' => '03.' . ($id++),
				'obat_nm' => $row['A'],
				'no_batch' => '-',
				'harga_beli' => 0,
				'harga_jual' => $row['B'],
				'harga_akhir' => $row['B'],
				'stok_minimal' => 100,
				'jenisbarang_cd' => '03',
				'tgl_catat' => date('Y-m-d'),
				'created_at' => date('Y-m-d H:i:s'),
				'created_by' => 'System'
			);
			$this->db->insert('mst_obat', $obat);
		}
		// looping 
		// isi data berdasarkan excel
		// //kosongkan semua data stok
		// $this->db->query("TRUNCATE far_stok");
		// $this->db->query("TRUNCATE far_stok_depo");
		// //ambil semua data obat
		// $this->db->query("UPDATE mst_obat SET harga_akhir = harga_jual");
		$obat = $this->db->where('jenisbarang_cd', '03')->get('mst_obat')->result_array();
		$far_stok = $this->db->query("SELECT * FROM far_stok ORDER BY stok_id DESC")->row_array();
		$id = $far_stok['stok_id'] + 1;
		$i = 1;
		foreach ($obat as $row) {
			//masukkan obat ke far_stok
			$ds = array(
				'stok_id' => $id,
				'obat_id' => $row['obat_id'],
				'obat_nm' => $row['obat_nm'],
				'no_batch' => $row['no_batch'],
				'satuan_cd' => $row['satuan_cd'],
				'harga_beli' => $row['harga_beli'],
				'harga_jual' => $row['harga_jual'],
				'harga_akhir' => $row['harga_akhir'],
				'jenisbarang_cd' => $row['jenisbarang_cd'],
				'keterangan_obat' => $row['keterangan_obat'],
				'is_obat_indikator' => $row['is_obat_indikator'],
				'is_napza' => $row['is_napza'],
				'is_generik' => $row['is_generik'],
				'is_antibiotik' => $row['is_antibiotik'],
				'is_injeksi' => $row['is_injeksi'],
				'stok_awal' => 0,
				'stok_penerimaan' => 9999,
				'stok_distribusi' => 0,
				'stok_retur_masuk' => 0,
				'stok_retur_keluar' => 0,
				'stok_musnah' => 0,
				'stok_akhir' => 9999,
				'stok_opname' => 0,
				'tgl_catat' => date('Y-m-d H:i:s'),
				'stok_st' => 1
			);
			$this->db->insert('far_stok', $ds);

			// 	//masukkan obat ke far_stok_depo
			$dd = array(
				'stokdepo_id' => $id,
				'distribusi_id' => $id,
				'stok_id' => $id,
				'lokasi_id' => '06.01.01',
				'obat_id' => $row['obat_id'],
				'obat_nm' => $row['obat_nm'],
				'no_batch' => $row['no_batch'],
				'satuan_cd' => $row['satuan_cd'],
				'harga_beli' => $row['harga_beli'],
				'harga_jual' => $row['harga_jual'],
				'harga_akhir' => $row['harga_akhir'],
				'jenisbarang_cd' => $row['jenisbarang_cd'],
				'keterangan_obat' => $row['keterangan_obat'],
				'is_obat_indikator' => $row['is_obat_indikator'],
				'is_napza' => $row['is_napza'],
				'is_generik' => $row['is_generik'],
				'is_antibiotik' => $row['is_antibiotik'],
				'is_injeksi' => $row['is_injeksi'],
				'stok_awal' => 0,
				'stok_penerimaan' => 9999,
				'stok_terpakai' => 0,
				'stok_retur_keluar' => 0,
				'stok_akhir' => 9999,
				'stok_opname' => 0,
				'tgl_catat' => date('Y-m-d H:i:s'),
				'stok_st' => 1,
			);
			$this->db->insert('far_stok_depo', $dd);

			$id++;
		}
	}

	public function migrasi_laborat()
	{
		//hapus semua data lab
		$this->db->query("TRUNCATE mst_item_lab");
		//mapping
		$tarif = $this->db->query("SELECT * FROM mst_tarif WHERE tarif_id LIKE '06%' ")->result_array();
		foreach ($tarif as $row) {
			$itemlab_id = substr($row['tarif_id'], 3, 10);
			if ($itemlab_id != false) {
				$d = array(
					'itemlab_id' => substr($row['tarif_id'], 3, 10),
					'parent_id' => (substr($row['parent_id'], 3, 10) != false) ? substr($row['parent_id'], 3, 10) : null,
					'itemlab_nm' => $row['tarif_nm'],
					'nilai_normal' => 0,
					'paket' => 0,
					'tarif_id' => $row['tarif_id'],
					'created_by' => 'system',
					'created_at' => date('Y-m-d H:i:s')
				);
				$this->db->insert('mst_item_lab', $d);
			}
		}
	}

	public function migrasi_radiologi()
	{
		//hapus semua data rad
		$this->db->query("TRUNCATE mst_item_rad");
		//mapping
		$tarif = $this->db->query("SELECT * FROM mst_tarif WHERE tarif_id LIKE '07%' ")->result_array();
		foreach ($tarif as $row) {
			$itemlab_id = substr($row['tarif_id'], 3, 10);
			if ($itemlab_id != false) {
				$d = array(
					'itemrad_id' => substr($row['tarif_id'], 3, 10),
					'parent_id' => (substr($row['parent_id'], 3, 10) != false) ? substr($row['parent_id'], 3, 10) : null,
					'itemrad_nm' => $row['tarif_nm'],
					'tarif_id' => $row['tarif_id'],
					'created_by' => 'system',
					'created_at' => date('Y-m-d H:i:s')
				);
				$this->db->insert('mst_item_rad', $d);
			}
		}
	}

	public function ajax($type = null, $id = null)
	{
		if ($type == 'auth_reset') {
			$auth = $this->m_profile->auth_reset();
			echo json_encode($auth);
		}
	}

	public function bpjs()
	{
		$data = "testtesttest";
		$secretKey = "secretkey";
		// Computes the timestamp
		date_default_timezone_set('UTC');
		$tStamp = strval(time() - strtotime('1970-01-01 00:00:00'));
		// Computes the signature by hashing the salt with the secret key as the key
		$signature = hash_hmac('sha256', $data . "&" . $tStamp, $secretKey, true);

		// base64 encode…
		$encodedSignature = base64_encode($signature);

		// urlencode…
		// $encodedSignature = urlencode($encodedSignature);

		echo "X-cons-id: " . $data . " ";
		echo "X-timestamp:" . $tStamp . " ";
		echo "X-signature: " . $encodedSignature;
	}

	public function mapping_radiologi()
	{
		$itemrad = $this->db->query("SELECT * FROM mst_item_rad WHERE is_tagihan = 1")->result_array();
		$kelas = $this->db->query("SELECT * FROM mst_kelas")->result_array();
		foreach ($itemrad as $row) {
			foreach ($kelas as $row2) {
				$this->db->where('tarif_id', $row['tarif_id'])->where('kelas_id', $row2['kelas_id'])->delete('mst_tarif_kelas');
				//insert data
				$d = array(
					'tarifkelas_id' => $row['tarif_id'] . '.' . $row2['kelas_id'],
					'tarif_id' => $row['tarif_id'],
					'kelas_id' => $row2['kelas_id'],
					'nominal' => 0,
					'created_by' => 'System',
					'created_at' => date('Y-m-d H:i:s'),
				);
				$this->db->insert('mst_tarif_kelas', $d);
			}
		}
	}
}
