<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class User extends MY_Controller
{

	var $nav_id = '02.05.01', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'm_role',
			'm_user',
			'master/m_pegawai'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id);
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '', 'role_id' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'user_id', 'type' => 'asc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(4, 0);
		$this->cookie['total_rows'] = $this->m_user->all_rows($this->cookie);
		set_cookie_nav($this->nav_id, $this->cookie);
		//main data
		$data['nav'] = $this->nav;
		$data['cookie'] = $this->cookie;
		$data['main'] = $this->m_user->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
		$data['list_role'] = $this->m_role->all_data();
		//set pagination
		set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('app/user/index', $data);
	}

	public function form_modal($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_user->get_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save/' . $id;
		$data['list_role'] = $this->m_role->all_data();
		$data['list_lokasi'] = $this->m_user->list_lokasi($id);
		$data['list_pegawai'] = $this->m_pegawai->all_data();

		echo json_encode(array(
			'html' => $this->load->view('app/user/form_modal', $data, true)
		));
	}

	public function save($id = null)
	{
		$this->m_user->save($id);
		create_log(($id != '') ? '_update' : '_add', $this->nav_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	public function create_user()
	{
		$insert = $this->m_user->create_user();
		echo $insert;
	}

	public function delete($id = null)
	{
		$this->m_user->delete($id, true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log('_delete', $this->nav_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	public function multiple($type = null)
	{
		$data = $this->input->post();
		if (isset($data['checkitem'])) {
			foreach ($data['checkitem'] as $key) {
				switch ($type) {
					case 'delete':
						$this->authorize($this->nav, '_delete');
						$this->m_user->delete($key, true);
						$flash = 'Data berhasil dihapus.';
						create_log('_delete', $this->nav_id);
						break;

					case 'enable':
						$this->authorize($this->nav, '_update');
						$this->m_user->update($key, array('is_active' => 1));
						$flash = 'Data berhasil diaktifkan.';
						create_log('_update', $this->nav_id);
						break;

					case 'disable':
						$this->authorize($this->nav, '_update');
						$this->m_user->update($key, array('is_active' => 0));
						$flash = 'Data berhasil dinonaktifkan.';
						create_log('_delete', $this->nav_id);
						break;
				}
			}
		}
		$this->session->set_flashdata('flash_success', $flash);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	function ajax($type = null)
	{
		if ($type == 'delete_photo') {
			$user_id = html_escape($this->input->get('user_id', true));
			$user_photo = html_escape($this->input->get('user_photo', true));
			//
			$result = $this->m_user->delete_photo($user_id, $user_photo);
			$callback = 'false';
			if ($result) $callback = 'true';
			//
			echo json_encode(array(
				'callback' => $callback
			));
		}

		if ($type = 'search_lokasi') {
			$list = $this->m_dt_lokasi->get_datatables();
			$no = 0;
			$data = array();
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $field['lokasi_id'];
				if ($field['parent_id'] != '') {
					$dash = '<span class="ml-2"></span> -- ';
					$suf = '';
				} else {
					$dash = '<b>';
					$suf = '</b>';
				};
				if ($field['parent_id'] != '') {
					// if (@$field['pemeriksaanrinc_id'] != null) {
					// 	// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemlab_id'].'" checked="true">';
					// 	$row[] = '<div class="d-flex justify-content-center">
					// 							<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
					// 								<label class="form-check-label">
					// 									<input type="checkbox" class="form-check-input" name="checkitem[]" value="'.$field['itemlab_id'].'" checked="true">
					// 								<i class="input-helper"></i></label>
					// 							</div>
					// 						</div>';
					// }else{
					// }
					// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemlab_id'].'">';
					$row[] = '<div class="d-flex justify-content-center">
											<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
												<label class="form-check-label">
													<input type="checkbox" class="form-check-input cb-item" name="checkitem[]" value="' . $field['lokasi_id'] . '">
												<i class="input-helper"></i></label>
											</div>
										</div>';
				} else {
					$row[] = '';
				}
				$row[] = $dash . $field['lokasi_nm'] . $suf;

				$data[] = $row;
			}

			$output = array(
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}
	}
}
