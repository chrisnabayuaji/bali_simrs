<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller{

	var $nav_id = '01', $nav;
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array(
			'm_nav'
		));
		$this->nav = $this->m_app->_get_nav($this->nav_id);
	}

	public function index() {
		$this->authorize($this->nav, '_view');
		$data['nav'] = $this->nav;
		
		$data['list_nav'] = $this->m_nav->list_nav_all();
		$data['list_nav_module'] = $this->m_nav->list_nav_module();

		$this->render('app/template/dashboard', $data);
	}
	
}