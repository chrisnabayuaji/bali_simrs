<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_app extends CI_Model {
  
  var $sess;

  function __construct(){
    parent::__construct();
		$this->sess = $this->session->userdata();
  }
  
  function _get_config() {
    $sql = "SELECT * FROM app_config";
    $query = $this->db->query($sql);
    $row = $query->row_array();
    return $row;
  }

  function _get_identitas() {
    $sql = "SELECT * FROM mst_identitas";
    $query = $this->db->query($sql);
    $row = $query->row_array();
    return $row;
  }

  function _get_running_text() {
    $sql = "SELECT * FROM app_info WHERE is_deleted = 0 AND is_active = 1";
    $query = $this->db->query($sql);
    $row = $query->result_array();
    return $row;
  }

  function _login() {
    $d = html_escape($this->input->post());
    $sql = "SELECT * FROM app_user WHERE user_name=?";
    $user = $this->db->query($sql, array($d['user_name']))->row_array();

    if($user != null) {
      if($user['is_active'] == 0) {
        return -1;
      } else {
        if (password_verify($d['user_password'], $user['user_password'])) {
          $lokasi = $this->db->where('user_id', $user['user_id'])->get('app_user_lokasi')->result_array();
          $role = $this->db->where('role_id', $user['role_id'])->get('app_role')->row_array();
          $arr_lokasi = array();
          foreach ($lokasi as $row) {
            $arr_lokasi[] = $row['lokasi_id'];
          };
          $sess_data = array(
            'sess_login'          => true,
            'sess_user_id'        => $user['user_id'],
            'sess_pegawai_id'     => $user['pegawai_id'],
            'sess_user_name'      => $user['user_name'],
            'sess_user_realname'  => $user['user_realname'],
            'sess_user_cd'        => $user['user_cd'],
            'sess_role_id'        => $user['role_id'],
            'sess_role_nm'        => $role['role_nm'],
            'sess_last_login'     => $user['last_login'],
            'sess_is_active'      => $user['is_active'],
            'sess_lokasi'         => $arr_lokasi
          );
          $this->session->set_userdata($sess_data);
          return 1;
        } else {
          return 0;
        }
      }            
    } else {
      return -2;
    }
  }
  
  function _get_profile_user_login() {
    $sql = "SELECT 
              a.*, b.role_nm 
            FROM 
              app_user a 
            LEFT JOIN app_role b ON a.role_id = b.role_id
            WHERE a.user_id=?";
    $query = $this->db->query($sql, array($this->sess['sess_user_id']));
    $row = $query->row_array();
    return $row;
  }

  // NAVIGATION ZONE ==================================================
  public function _get_nav($nav_id)
  {
    $sql = "SELECT a.*, b._view, b._add, b._update, b._delete 
            FROM app_nav a 
            JOIN app_role_nav b ON b.nav_id = a.nav_id
            WHERE b.nav_id = ? AND b.role_id = ?";
    $query = $this->db->query($sql, array($nav_id, $this->sess['sess_role_id']));
    return $query->row_array();
  }

  function _get_nav_by_module($module=null) {
    $sql = "SELECT * FROM app_nav WHERE nav_module=?";
    $query = $this->db->query($sql, $module);
    return $query->row_array();
  }

  function _list_nav($parent_id=null, $nav_top_menu_type=null, $group_by=null) {
    $sql_where = "";
    $sql_where.= ($parent_id != "") ? " AND a.parent_id = '$parent_id'" : " AND a.parent_id IS NULL ";
    $sql_where.= ($nav_top_menu_type != "") ? " AND a.nav_top_menu_type = '$nav_top_menu_type'" : "";
    $sql_group_by = ($group_by != "") ? " GROUP BY a.nav_sub_label" : "";
    //
    $sql = "SELECT a.* 
            FROM app_nav a 
            INNER JOIN app_role_nav b ON a.nav_id=b.nav_id 
            WHERE a.nav_st='1' AND b._view='1' AND b.role_id=? 
                $sql_where 
            $sql_group_by
            ORDER BY a.nav_id ASC ";
    $query = $this->db->query($sql, $this->sess['sess_role_id']);
    if($query->num_rows() > 0) {
      $result = $query->result_array();
      $no=1;
      foreach($result as $key => $val) {
        $result[$key]['no'] = $no;
        //
        $result[$key]['item_nav'] = $this->_list_nav($result[$key]['nav_id']);
        $result[$key]['sub_label'] = $this->_list_nav($result[$key]['nav_id'], 'L', 'a.nav_sub_label');
        $result[$key]['large_menu'] = $this->_list_large_nav($result[$key]['parent_id'], 'L', $result[$key]['nav_sub_label']);
        //
        if ($val['nav_category'] == 'E') {
            $result[$key]['url_nav'] = $val['nav_url'];
        }else{
            $result[$key]['url_nav'] = site_url($val['nav_url']);
        }
        //
        $no++;
      }
      return $result;
    } else {
      return array();
    }
  }

  function _list_large_nav($parent_id=null, $nav_top_menu_type=null, $nav_sub_label=null) {
    $sql_where = "";
    $sql_where.= ($parent_id != "") ? " AND a.parent_id = '$parent_id'" : " AND a.parent_id IS NULL ";
    $sql_where.= ($nav_top_menu_type != "") ? " AND a.nav_top_menu_type = '$nav_top_menu_type'" : "";
    $sql_where.= ($nav_sub_label != "") ? " AND a.nav_sub_label = '$nav_sub_label'" : "";
    
    $sql = "SELECT a.* 
            FROM app_nav a 
            INNER JOIN app_role_nav b ON a.nav_id=b.nav_id 
            WHERE a.nav_st='1' AND b._view='1' AND b.role_id=? 
                $sql_where 
            GROUP BY a.nav_id
            ORDER BY a.nav_id ASC ";
    $query = $this->db->query($sql, $this->sess['sess_role_id']);
    if($query->num_rows() > 0) {
        $result = $query->result_array();
        $no=1;
        foreach($result as $key => $val) {
            $result[$key]['no'] = $no;
            $no++;
        }
        return $result;
    } else {
        return array();
    }
  }

  public function update_user($id = null)
  {
    $data = html_escape($this->input->post());
    $get_user = $this->_get_profile_user_login();
    // upload img
    $user_photo = $this->upload_file_process($data, $id);
    if($user_photo != '') {
      $data['user_photo'] = $user_photo;    
    }
    //
    if(@$data['user_password'] != '') {
      $data['user_password'] = hash_password($data['user_password']);
    }else{
      $data['user_password'] = $get_user['user_password'];
    }
    // unset data
    unset($data['cb_password']);
    unset($data['confirm_user_password']);
    unset($data['set_user_password']);
    //
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('sess_user_realname');
    if ($data['user_photo'] == 'error_img') {
      $this->session->set_flashdata('flash_error', 'Data gagal disimpan <br> Format gambar salah');
      //update img to empty
      $data_img['user_photo'] = '';
      $this->db->where('user_id', $id)->update('app_user', $data_img);
    }else{
      $this->db->where('user_id', $id)->update('app_user', $data);
      $this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
    }
  }

  function upload_file_process($data=null,$user_id=null) {
    $result   = '';
    if(@$_FILES['user_photo']['tmp_name'] != '') {
        $path_dir       = "assets/images/user/";
        $tmp_name       = @$_FILES['user_photo']['tmp_name'];
        $fupload_name   = @$_FILES['user_photo']['name'];
        //
        if($user_id != '') {
            $user = $this->_get_profile_user_login();
            $result = $this->_upload_img($path_dir, $tmp_name, $fupload_name, $user['user_photo']);
        } else {
            $result = $this->_upload_img($path_dir, $tmp_name, $fupload_name);
        }         
    }        
    return $result;
  }

  function _upload_img($path_dir=null, $tmp_name=null, $fupload_name=null, $old_file=null) {
    //
    $this->load->library('upload');
    $this->load->library('image_lib');
    //
    $name_img = create_title_img($path_dir, $tmp_name, $fupload_name, $old_file);
    $src_file_name = 'user_photo';
    //
    $config['file_name'] = $name_img;
    $config['upload_path'] = $path_dir; //path folder
    $config['allowed_types'] = 'jpg|png|jpeg|gif'; //type yang dapat diakses bisa anda sesuaikan

    $this->upload->initialize($config);
    if(!empty($fupload_name)){
      if ($this->upload->do_upload($src_file_name)){
        $gbr = array('upload_data' => $this->upload->data()); 
        // cek resolusi gambar
        $nama_gambar = $path_dir.$gbr['upload_data']['file_name'];
        $data = getimagesize($nama_gambar);
        $width = $data[0];
        $height = $data[1];
        // pembagian
        $bagi_width = $width / 4;
        $bagi_height = $height / 4;
        // Compress Image
        $config['image_library']='gd2';
        $config['source_image'] = $gbr['upload_data']['full_path'];
        $config['create_thumb']= FALSE;
        $config['maintain_ratio']= FALSE;
        $config['quality']= '20%';
        $config['width']= round($bagi_width);
        $config['height']= round($bagi_height);
        $config['new_image']= $path_dir.$gbr['upload_data']['file_name'];
        $this->image_lib->initialize($config);
        // $this->load->library('image_lib', $config);
        $this->image_lib->resize();
        $this->image_lib->clear();
        //
        // $gambar_1 = $gbr['upload_data']['file_name'];
        $result = $name_img;
      }else{
        $result = 'error_img';
      }
      //
      return $result;          
    }   
  }

}