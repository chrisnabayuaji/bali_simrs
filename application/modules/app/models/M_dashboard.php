<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_dashboard extends CI_Model
{

  public function identitas()
  {
    $sql = "SELECT * FROM mst_identitas";
    $query = $this->db->query($sql);
    $row = $query->row_array();
    return $row;
  }

  public function rawat_jalan()
  {
    $sql = "SELECT COUNT(*) AS jml
            FROM reg_pasien a 
            INNER JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id 
            WHERE b.jenisreg_st='1' AND DATE(a.tgl_registrasi)=DATE(NOW())";
    $query = $this->db->query($sql);
    $row = $query->row_array();
    return $row['jml'];
  }

  public function rawat_inap_atas()
  {
    $sql = "SELECT COUNT(*) AS jml
            FROM reg_pasien a 
            INNER JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id 
            WHERE b.jenisreg_st='2' AND DATE(a.tgl_registrasi)<=DATE(NOW()) AND b.lokasi_ruang='atas' AND a.pulang_st='0'";
    $query = $this->db->query($sql);
    $row = $query->row_array();
    return $row['jml'];
  }

  public function rawat_inap_bawah()
  {
    $sql = "SELECT COUNT(*) AS jml
            FROM reg_pasien a 
            INNER JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id 
            WHERE b.jenisreg_st='2' AND DATE(a.tgl_registrasi)<=DATE(NOW()) AND b.lokasi_ruang='bawah' AND a.pulang_st='0'";
    $query = $this->db->query($sql);
    $row = $query->row_array();
    return $row['jml'];
  }

  public function rawat_darurat()
  {
    $sql = "SELECT COUNT(*) AS jml
            FROM reg_pasien a 
            INNER JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id 
            WHERE b.jenisreg_st='3' AND DATE(a.tgl_registrasi)=DATE(NOW())";
    $query = $this->db->query($sql);
    $row = $query->row_array();
    return $row['jml'];
  }

  public function pasien_pulang_hari_ini()
  {
    $sql = "SELECT COUNT(*) AS jml
            FROM reg_pasien a 
            INNER JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id 
            WHERE b.jenisreg_st='2' AND DATE(a.tgl_pulang)=DATE(NOW())";
    $query = $this->db->query($sql);
    $row = $query->row_array();
    return $row['jml'];
  }

  public function pasien_masih_inap()
  {
    $sql = "SELECT COUNT(*) AS jml
            FROM reg_pasien a 
            INNER JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id 
            WHERE b.jenisreg_st='2' AND a.pulang_st='0' AND lokasi_ruang IN ('atas','bawah')";
    $query = $this->db->query($sql);
    $row = $query->row_array();
    return $row['jml'];
  }

  public function list_news()
  {
    $sql = "SELECT * FROM web_news WHERE news_st = 1 AND is_deleted = 0 AND is_active = 1";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    //
    foreach ($result as $key => $val) {
      $result[$key]['first_image'] = $this->get_image_first($val['news_id']);
    }
    return $result;
  }

  public function get_image_first($news_id = null)
  {
    $sql = "SELECT * FROM web_news_image WHERE news_id=? ORDER BY newsimage_id ASC";
    $query = $this->db->query($sql, $news_id);
    if ($query->num_rows() > 0) {
      return $query->row_array();
    } else {
      return false;
    }
  }

  function paging_berita($p = 1, $o = 0)
  {
    $sql = "SELECT 
              COUNT(news_id) AS count_data 
            FROM web_news a 
            WHERE news_st = 1 AND is_deleted = 0 AND is_active = 1";
    $query = $this->db->query($sql);
    $row = $query->row_array();
    $count_data = $row['count_data'];
    //
    $this->load->library('paging_mini');
    $cfg['page'] = $p;
    $cfg['per_page'] = 2;
    $cfg['num_rows'] = $count_data;
    $this->paging_mini->init($cfg);
    return $this->paging_mini;
  }

  function list_berita($o = 0, $offset = 0, $limit = 100)
  {
    $sql_paging = " LIMIT " . $offset . "," . $limit;
    //
    $sql = "SELECT 
              a.* 
            FROM web_news a 
            WHERE news_st = 1 AND is_deleted = 0 AND is_active = 1
            ORDER BY a.news_id DESC
              $sql_paging";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    // 
    $no = 1;
    foreach ($result as $key => $val) {
      $result[$key]['no'] = $no + $offset;
      $result[$key]['first_image'] = $this->get_image_first($val['news_id']);
      $no++;
    }
    return $result;
  }

  public function get_berita($news_id = null)
  {
    $sql = "SELECT * FROM web_news WHERE news_id=?";
    $query = $this->db->query($sql, $news_id);
    return $query->row_array();
  }

  public function list_img_berita($news_id = null)
  {
    $sql = "SELECT 
              image_path, image_no, image_name, image_description  
            FROM web_news_image 
            WHERE news_id = ? 
            ORDER BY image_no ASC";
    $query = $this->db->query($sql, $news_id);
    $result = $query->result_array();
    // 
    return $result;
  }

  public function update_hit($news_id = null)
  {
    $sql = "UPDATE web_news SET news_hit = news_hit+1 WHERE news_id=?";
    $query = $this->db->query($sql, $news_id);
    return $query;
  }

  public function list_kunjungan_harian()
  {
    $bln = date('m');
    $thn = date('Y');
    $sql = "SELECT 
            a.*,
            (jml_tgl_01+jml_tgl_02+jml_tgl_03+jml_tgl_04+jml_tgl_05+jml_tgl_06+jml_tgl_07+jml_tgl_08+jml_tgl_09+jml_tgl_10+jml_tgl_11+jml_tgl_12+jml_tgl_13+jml_tgl_14+jml_tgl_15+jml_tgl_16+jml_tgl_17+jml_tgl_18+jml_tgl_19+jml_tgl_20+jml_tgl_21+jml_tgl_22+jml_tgl_23+jml_tgl_24+jml_tgl_25+jml_tgl_26+jml_tgl_27+jml_tgl_28+jml_tgl_29+jml_tgl_30+jml_tgl_31) as jml_tgl_total
            FROM 
            (
             SELECT 
              a.lokasi_id,a.lokasi_nm,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='01') as jml_tgl_01,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='02') as jml_tgl_02,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='03') as jml_tgl_03,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='04') as jml_tgl_04,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='05') as jml_tgl_05,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='06') as jml_tgl_06,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='07') as jml_tgl_07,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='08') as jml_tgl_08,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='09') as jml_tgl_09,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='10') as jml_tgl_10,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='11') as jml_tgl_11,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='12') as jml_tgl_12,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='13') as jml_tgl_13,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='14') as jml_tgl_14,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='15') as jml_tgl_15,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='16') as jml_tgl_16,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='17') as jml_tgl_17,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='18') as jml_tgl_18,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='19') as jml_tgl_19,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='20') as jml_tgl_20,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='21') as jml_tgl_21,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='22') as jml_tgl_22,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='23') as jml_tgl_23,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='24') as jml_tgl_24,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='25') as jml_tgl_25,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='26') as jml_tgl_26,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='27') as jml_tgl_27,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='28') as jml_tgl_28,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='29') as jml_tgl_29,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='30') as jml_tgl_30,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.lokasi_id=a.lokasi_id AND YEAR(b.tgl_registrasi) = '$thn' AND MONTH(b.tgl_registrasi) = '$bln' AND DAY(b.tgl_registrasi)='31') as jml_tgl_31 
             FROM mst_lokasi a 
             WHERE a.jenisreg_st='1' 
             ORDER BY a.lokasi_id ASC
            ) a";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
}
