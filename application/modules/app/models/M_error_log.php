<?php

class M_error_log extends CI_Model
{

  function dir_array()
  {
    $dir = FCPATH . 'application/logs';
    $result = array();

    $cdir = scandir($dir);
    // foreach ($cdir as $key => $value) {
    //   if (!in_array($value, array(".", ".."))) {
    //     if (is_dir($dir . DIRECTORY_SEPARATOR . $value)) {
    //       $result[$value] = $this->dir_array($dir . DIRECTORY_SEPARATOR . $value);
    //     } else {
    //       $result[] = $value;
    //     }
    //   }
    // }

    foreach ($cdir as $key => $value) {
      if (!in_array($value, array(".", "..", "index.html"))) {
        $result[] = $value;
      }
    }
    $res = array();

    foreach ($result as $key => $value) {
      $file = "application/logs/" . $value;
      $res[] = array(
        'file_name' => $value,
        'file_size' => filesize($file),
        'modified' => date("d M Y H:i:s", filemtime($file))
      );
    }

    return array_reverse($res);
  }
}
