<?php
class M_nav extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function where($cookie)
	{
		$where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND (a.nav_nm LIKE '%".$this->db->escape_like_str($cookie['search']['term'])."%' OR a.nav_id LIKE '%".$this->db->escape_like_str($cookie['search']['term'])."%') ";
		}
		return $where;
	}

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT *, CHAR_LENGTH(a.nav_id) as count_order FROM app_nav a 
      $where
      ORDER BY "
        .$cookie['order']['field']." ".$cookie['order']['type'].
      " LIMIT ".$cookie['cur_page'].",".$cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM app_nav a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM app_nav a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id) {
    $sql = "SELECT * FROM app_nav WHERE nav_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  function get_nav_order($parent_id=null) {
      $arr_nav = $this->get_nav($parent_id);
      //
      $len = strlen($arr_nav['nav_order']);
      $len_next = $len+3;
      $sql = "SELECT MAX(RIGHT(nav_order,2)) as nav_order FROM app_nav WHERE LEFT(nav_order,$len)=? AND LENGTH(nav_order)=?";
      $query = $this->db->query($sql, array($arr_nav['nav_order'], $len_next));
      if($query->num_rows() > 0) {
          $row = $query->row_array();
          $nav_order = abs($row['nav_order'])+1;
          $nav_order = zerofill($nav_order,2);
          $result = $arr_nav['nav_order'].'.'.$nav_order;
      } else {
          $result = '01';
      }
      return $result;
  }

  public function save($id = null)
  {
		$data = html_escape($this->input->post());
		if ($id == null) {
			// upload img
      $data['nav_icon'] = $this->upload_file_process($data);    
      // font icon
      $nav_font_icon = $this->font_icon_setting($data);
      $data['nav_font_icon'] = @$nav_font_icon['nav_font_icon'];    
			$data['created_at'] = date('Y-m-d H:i:s');
			$data['created_by'] = $this->session->userdata('sess_user_realname');
			if ($data['nav_icon'] == 'error_img') {
				$this->session->set_flashdata('flash_error', 'Data gagal disimpan <br> Format gambar salah');
			}else{
				$this->db->insert('app_nav', $data);
				$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
			}
		}else{
			// upload img
      $nav_icon = $this->upload_file_process($data, $id);
      if($nav_icon != '') {
          $data['nav_icon'] = $nav_icon;    
      }
      // font icon
      $nav_font_icon = $this->font_icon_setting($data, $id);
      $data['nav_font_icon'] = @$nav_font_icon['nav_font_icon'];
      if ($nav_font_icon['nav_icon'] == 'empty') {
          $data['nav_icon'] = '';
      }
			$data['updated_at'] = date('Y-m-d H:i:s');
			$data['updated_by'] = $this->session->userdata('sess_user_realname');
			if ($nav_icon == 'error_img') {
				$this->session->set_flashdata('flash_error', 'Data gagal disimpan <br> Format gambar salah');
				//update img to empty
        $data_img['nav_icon'] = '';
        $this->db->where('nav_id', $id)->update('app_nav', $data_img);
			}else{
				$this->db->where('nav_id', $id)->update('app_nav', $data);
				$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
			}
		}
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('nav_id',$id)->update('app_nav', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('nav_id', $id)->delete('app_nav');
    }else{
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('nav_id', $id)->update('app_nav', $data);
    }
  }

  function delete_photo($id=null, $nav_icon=null) {
    $this->delete_file_process($nav_icon);
    //
    $data['nav_icon'] = '';
    $this->db->where('nav_id', $id);
    $result = $this->db->update('app_nav', $data);
    return $result;
  }

	function list_nav_all() {
    $sql = "SELECT * FROM app_nav WHERE nav_st = 1 ORDER BY nav_id ASC";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    return $result;
	}

	function list_nav_module() {
    $role_id = $this->session->userdata('sess_role_id');
		$sql = "SELECT b.* FROM app_role_nav a 
      JOIN app_nav b ON a.nav_id = b.nav_id
      WHERE a.role_id = '$role_id' AND a._view = 1 AND b.is_module = 1 AND b.nav_st = 1 ORDER BY b.nav_id ASC";
		$query = $this->db->query($sql);
		$result = $query->result_array();
    return $result;
    // return null;
	}

	function upload_file_process($data=null, $nav_id=null) {
		$result   = '';
		if(@$_FILES['nav_icon']['tmp_name'] != '') {
			$path_dir       = FCPATH."assets/images/module/";
			$tmp_name       = @$_FILES['nav_icon']['tmp_name'];
			$fupload_name   = @$_FILES['nav_icon']['name'];
			
			if($nav_id != '') {
				$nav = $this->get_data($nav_id);
				$result = $this->_upload_img($path_dir, $tmp_name, $fupload_name, $nav['nav_icon']);
			} else {
				$result = $this->_upload_img($path_dir, $tmp_name, $fupload_name);
			}         
		}        
		return $result;
	}

	function _upload_img($path_dir=null, $tmp_name=null, $fupload_name=null, $old_file=null) {
    //
		$this->load->library('upload');
		$this->load->library('image_lib');
		//
		$name_img = create_title_img($path_dir, $tmp_name, $fupload_name, $old_file);
		$src_file_name = 'nav_icon';
		//
		$config['file_name'] = $name_img;
		$config['upload_path'] = $path_dir; //path folder
		$config['allowed_types'] = 'jpg|png|jpeg|gif'; //type yang dapat diakses bisa anda sesuaikan

		$this->upload->initialize($config);
		if(!empty($fupload_name)){
			if ($this->upload->do_upload($src_file_name)){
				$gbr = array('upload_data' => $this->upload->data()); 
				// cek resolusi gambar
				$nama_gambar = $path_dir.$gbr['upload_data']['file_name'];
				//Compress Image
				$config['image_library']='gd2';
				$config['source_image'] = $gbr['upload_data']['full_path'];
				$config['create_thumb']= FALSE;
				$config['maintain_ratio']= FALSE;
				$config['quality']= '100%';
				$config['width']= '50';
				$config['height']= '50';
				$config['new_image']= $path_dir.$gbr['upload_data']['file_name'];
				$this->image_lib->initialize($config);
				// $this->load->library('image_lib', $config);
        $this->image_lib->resize();
        $this->image_lib->clear();
        //
        // $gambar_1 = $gbr['upload_data']['file_name'];
        $result = $name_img;
			}else{
        $result = $this->upload->display_errors();
        var_dump($result);
        die;
			}
			//
			return $result;          
		}
	}

	function font_icon_setting($data=null, $id=null) {
		$result = '';
		if ($id !='') {
			$nav = $this->get_data($id);
			if ($nav['nav_icon'] !='' && $data['nav_icon_type'] !='1') {
				$this->delete_file_process($nav['nav_icon']);
				$result['nav_icon'] = 'empty';
			}
		}
		
		if ($data['nav_icon_type'] == '2') {
			$result['nav_font_icon'] = 'fas fa-'.clear_font_icon($data['nav_font_icon']);
		}elseif ($data['nav_icon_type'] == '3') {
			$result['nav_font_icon'] = 'mdi mdi-'.clear_font_icon($data['nav_font_icon']);
		}
		return $result;
	}

	function delete_file_process($nav_icon=null) {
    $path_dir = "assets/images/module/";
    $result = unlink($path_dir . $nav_icon);
    return $result;
  }
    
}
