<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_parameter extends CI_Model {

	public function where($cookie)
	{
		$where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND a.parameter_nm LIKE '%".$this->db->escape_like_str($cookie['search']['term'])."%' ";
		}
    if (@$cookie['search']['parameter_group'] != '') {
      $where .= "AND a.parameter_group LIKE '%".$this->db->escape_like_str($cookie['search']['parameter_group'])."%' ";
		}
    if (@$cookie['search']['parameter_nm'] != '') {
      $where .= "AND a.parameter_nm LIKE '%".$this->db->escape_like_str($cookie['search']['parameter_nm'])."%' ";
		}
		return $where;
	}

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT * FROM mst_parameter a 
      $where
      ORDER BY "
        .$cookie['order']['field']." ".$cookie['order']['type'].
      " LIMIT ".$cookie['cur_page'].",".$cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM mst_parameter a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM mst_parameter a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function list_parameter_group() {
    $sql = "SELECT parameter_group FROM mst_parameter GROUP BY parameter_group ORDER BY parameter_group ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function list_parameter_name($cookie) {
    $sql_where = "";
    if($cookie['search']['parameter_group'] != '')  $sql_where .= " AND parameter_group = '".$cookie['search']['parameter_group']."'";
    
    $sql = "SELECT parameter_nm FROM mst_parameter WHERE 1=1 $sql_where GROUP BY parameter_nm ORDER BY parameter_nm ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function list_parameter_by_field($parameter_field='ALL') {
    $sql = "SELECT * FROM mst_parameter WHERE parameter_field=? ORDER BY parameter_cd ASC";
    $query = $this->db->query($sql, $parameter_field);
    return $query->result_array();
  }

  function get_data($id) {
    $sql = "SELECT * FROM mst_parameter WHERE parameter_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    if ($id == null) {
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('mst_parameter', $data);
    }else{
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('parameter_id', $id)->update('mst_parameter', $data);
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('parameter_id',$id)->update('mst_parameter', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('parameter_id', $id)->delete('mst_parameter');
    }else{
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('parameter_id', $id)->update('mst_parameter', $data);
    }
  }
  
}