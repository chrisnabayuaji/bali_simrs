<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_permission extends CI_Model {

  public function list_role_nav($role_id=null) {
    $sql = "SELECT
          a.*,
          b.role_id as set_role_id,
          b.nav_id as set_nav_id,
          b._view,
          b._add,
          b._update,
          b._delete,
          (b._view+b._add+b._update+b._delete) as count_rules
        FROM app_nav a
        LEFT JOIN app_role_nav b ON a.nav_id=b.nav_id AND b.role_id=?
        ORDER BY a.nav_id ASC ";
    $query = $this->db->query($sql, $role_id);
    $result = $query->result_array();
    //
    $no = 1;
    foreach($result as $key => $val) {
      $result[$key]['no'] = $no;
      $result[$key]['cb_all'] = ($result[$key]['count_rules'] == '4' ? 'yes' : 'no');
      $no++;
    }
    return $result;
  }

  public function save($role_id=null) {
    $data = html_escape($this->input->post());
    // delete all
    $this->db->where('role_id', $role_id);
    $this->db->delete('app_role_nav');
    //
    foreach($data['nav_id'] as $key => $val) {
      $nav_id = $val;
      //
      $set_nav_id = @$data['set_nav_id'][$key];
      $set_role_id = @$data['set_role_id'][$key];
      //
      $data_role_nav = array(
        'role_id' => $role_id,
        'nav_id'  => under_to_point($val),
        '_view'   => isset($data['cb_view'][$nav_id]) && $data['cb_view'][$nav_id] != '' ? '1' : '0',
        '_add'    => isset($data['cb_add'][$nav_id]) && $data['cb_add'][$nav_id] != '' ? '1' : '0',
        '_update' => isset($data['cb_update'][$nav_id]) && $data['cb_update'][$nav_id] != '' ? '1' : '0',
        '_delete' => isset($data['cb_delete'][$nav_id]) && $data['cb_delete'][$nav_id] != '' ? '1' : '0',
      );
      //
      $this->db->insert('app_role_nav', $data_role_nav);
    }
  }

}