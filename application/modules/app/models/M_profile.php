<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_profile extends CI_Model {

  public function get_first()
  {
    return $this->db->get('app_config')->row_array();
  }

  public function update($type, $val)
  {
  	$data = html_escape($this->input->post());
  	if ($type !='' && $val !='') {
  		$data[$type] = urldecode($val);
  	}
    $this->db->update('app_config', $data);
  }

  public function migration($type = null)
  {
    $this->load->database('rspermata', TRUE);
    if ($type == 'pasien') {
      $this->db->query("TRUNCATE mst_pasien");
      $this->db->query(
        "INSERT INTO mst_pasien (
            pasien_id, pasien_nm, sebutan_cd, nik, alamat, wilayah_st, provinsi, kabupaten, kecamatan, kelurahan, 
            wilayah_id,
            tgl_lahir, sex_cd, telp, tgl_catat, no_kartu, pendidikan_cd, pekerjaan_cd, agama_cd, jenispasien_id
          )
          SELECT 
            nocm, UPPER(TRIM(REPLACE(REPLACE(REPLACE(nama,',',' '),'.',' '),'  ',' '))), RIGHT(UPPER(TRIM(REPLACE(REPLACE(nama,',',' '),'.',' '))), 2), nik, UPPER(alamat), 'D', b.wilayah_nm, c.wilayah_nm, d.wilayah_nm, e.wilayah_nm, 
            CONCAT(SUBSTRING(kkelurahan,1,2),'.',SUBSTRING(kkelurahan,3,2),'.',SUBSTRING(kkelurahan,6,2),'.2',LPAD(SUBSTRING(kkelurahan,8,4),3,'0')),
            tgllahir, jeniskel, telp, CONCAT(tgldaftar,' ',jamdaftar), noasuransi, '00', '00', '00', '01'
          FROM rspermata.tpasien a
          LEFT JOIN mst_wilayah b ON b.wilayah_id = SUBSTRING(kkelurahan,1,2)
          LEFT JOIN mst_wilayah c ON c.wilayah_id = CONCAT(SUBSTRING(kkelurahan,1,2),'.',SUBSTRING(kkelurahan,3,2))
          LEFT JOIN mst_wilayah d ON d.wilayah_id = CONCAT(SUBSTRING(kkelurahan,1,2),'.',SUBSTRING(kkelurahan,3,2),'.',SUBSTRING(kkelurahan,6,2))
          LEFT JOIN mst_wilayah e ON e.wilayah_id = CONCAT(SUBSTRING(kkelurahan,1,2),'.',SUBSTRING(kkelurahan,3,2),'.',SUBSTRING(kkelurahan,6,2),'.2',LPAD(SUBSTRING(kkelurahan,8,4),3,'0'))
          ORDER BY nocm
        ");

      //delete and insert sebutan;
      $this->db->where('parameter_field','sebutan_cd')->delete('mst_parameter');
      $sebutan = array('parameter_group' => 'PASIEN', 'parameter_nm' => 'SEBUTAN', 'parameter_field' => 'sebutan_cd', 'parameter_val' => 'Tuan', 'parameter_cd' => 'TN');
      $this->db->insert('mst_parameter',$sebutan);
      $sebutan = array('parameter_group' => 'PASIEN', 'parameter_nm' => 'SEBUTAN', 'parameter_field' => 'sebutan_cd', 'parameter_val' => 'Nyonya', 'parameter_cd' => 'NY');
      $this->db->insert('mst_parameter',$sebutan);
      $sebutan = array('parameter_group' => 'PASIEN', 'parameter_nm' => 'SEBUTAN', 'parameter_field' => 'sebutan_cd', 'parameter_val' => 'Nona', 'parameter_cd' => 'NN');
      $this->db->insert('mst_parameter',$sebutan);
      $sebutan = array('parameter_group' => 'PASIEN', 'parameter_nm' => 'SEBUTAN', 'parameter_field' => 'sebutan_cd', 'parameter_val' => 'Saudara', 'parameter_cd' => 'SDR');
      $this->db->insert('mst_parameter',$sebutan);
      $sebutan = array('parameter_group' => 'PASIEN', 'parameter_nm' => 'SEBUTAN', 'parameter_field' => 'sebutan_cd', 'parameter_val' => 'Anak', 'parameter_cd' => 'AN');
      $this->db->insert('mst_parameter',$sebutan);
      $sebutan = array('parameter_group' => 'PASIEN', 'parameter_nm' => 'SEBUTAN', 'parameter_field' => 'sebutan_cd', 'parameter_val' => 'Bayi', 'parameter_cd' => 'BY');
      $this->db->insert('mst_parameter',$sebutan);
      $sebutan = array('parameter_group' => 'PASIEN', 'parameter_nm' => 'SEBUTAN', 'parameter_field' => 'sebutan_cd', 'parameter_val' => 'Bayi Nyonya', 'parameter_cd' => 'BY NY');
      $this->db->insert('mst_parameter',$sebutan);

      //update sebutan menurut parameter cd
      $this->db->where('sebutan_cd','DR')->update('mst_pasien', array('sebutan_cd' => 'SDR'));
      $this->db->query("UPDATE mst_pasien a
        LEFT JOIN mst_parameter b ON a.sebutan_cd = b.parameter_cd AND b.parameter_field = 'sebutan_cd' 
      SET a.sebutan_cd = IF(b.parameter_cd is null, NULL, b.parameter_cd)");
      //update sebutan by ny
      $this->db->query("UPDATE mst_pasien SET sebutan_cd = 'BY NY' WHERE LEFT(pasien_nm,5) = 'BY NY' OR RIGHT(pasien_nm,5) = 'BY NY'");
      // remove sebutan dari pasien_nm
      $this->db->query("UPDATE mst_pasien SET pasien_nm = replace(pasien_nm,sebutan_cd,'') WHERE sebutan_cd IS NOT NULL");
      $this->db->query("UPDATE mst_pasien SET pasien_nm = TRIM(pasien_nm)");
      // sebutan yang ada di depan
      $this->db->query("UPDATE mst_pasien a
          SET sebutan_cd=(SELECT parameter_cd FROM mst_parameter b 
                    WHERE SUBSTRING_INDEX(a.pasien_nm, ' ', 1) = b.parameter_cd AND b.parameter_field='sebutan_cd')
      WHERE a.sebutan_cd IS NULL");
      // remove sebutan dari pasien_nm
      $this->db->query("UPDATE mst_pasien SET pasien_nm = replace(pasien_nm,sebutan_cd,'') WHERE sebutan_cd IS NOT NULL");
      $this->db->query("UPDATE mst_pasien SET pasien_nm = TRIM(pasien_nm)");
    }
    
    if ($type == 'pegawai') {
      //migrasi jenis pegawai
      $this->db->query("DELETE FROM mst_parameter WHERE parameter_field = 'jenispegawai_cd'");
      $this->db->query(
        "INSERT INTO mst_parameter (
          parameter_group, parameter_nm, parameter_field, parameter_val, parameter_cd
        ) 
        SELECT 
          'PEGAWAI', 'JENIS PEGAWAI', 'jenispegawai_cd', NMJABATAN, JABATAN 
        FROM rspermata.tmasterjabatan ORDER BY JABATAN"
      );
      //migrasi pegawai
      $this->db->query("TRUNCATE mst_pegawai");
      $this->db->query(
        "INSERT INTO mst_pegawai (
            pegawai_id, pegawai_nm, pegawai_nip, jenispegawai_cd, tmp_lahir, tgl_lahir, pendidikan_cd, alamat_lengkap, pegawai_st, created_at
          )
          SELECT 
            nourut, nama, nip, jabatan, tmplahir, tgllahir, kpendidikan, alamat, '1', tglmasuk
          FROM rspermata.tpegawai a
          ORDER BY nourut
        ");
    }

    if ($type == 'obat') {
      //migrasi obat
      $this->db->query("TRUNCATE mst_obat");
      $this->db->query(
        "INSERT INTO mst_obat (
            obat_id, obat_nm, harga_beli, harga_jual
          )
          SELECT 
            RIGHT(kobat,8), obat, hbeli, hjual
          FROM rspermata.tobat a
          ORDER BY kobat
        ");
    }

    if ($type == 'tarif') {
      //Migrasi Kelas
      $this->db->query("TRUNCATE mst_kelas");
      $kelasbpjs = $this->db->query("SELECT * FROM rspermata.tkelasbpjs")->result_array();
      $i = 1;
      foreach ($kelasbpjs as $row) {
        $dk = array(
          'kelas_id' => str_pad($i++, 2, "0", STR_PAD_LEFT),
          'kelas_nm' => $row['namakelas'],
          'kelas_singkatan' => $row['kodekelas']
        );
        $this->db->insert('mst_kelas', $dk);
      }
      //Migrasi Tarif
      $this->db->query("TRUNCATE mst_tarif");
      $this->db->query("TRUNCATE mst_tarif_kelas");
      $this->db->query("TRUNCATE mst_tarif_admisi");
      // golongan tindakan
      $this->db->query(
        "INSERT INTO mst_tarif (
            tarif_id, old_id, tarif_nm, tarif_tp, akun_id, created_by, is_kwitansi, tarif_st
          )
          SELECT 
            kgoltindakan, kgoltindakan, goltindakan, 'G', '04.01.01', 'System', 1, 1
          FROM rspermata.tgoltindakan
          ORDER BY kgoltindakan
        "); 
      // jenis tindakan
      $this->db->query(
        "INSERT INTO mst_tarif (
            tarif_id, parent_id, old_id, tarif_nm, tarif_tp, akun_id, created_by, is_kwitansi, tarif_st
          )
          SELECT 
            CONCAT(kgoltindakan,'.',RIGHT(kjenistindakan,3)),kgoltindakan, RIGHT(kjenistindakan,3), jenistindakan, 'G', '04.01.01', 'System', 0, 1
          FROM rspermata.tjenistindakan
          ORDER BY kgoltindakan
        "); 

      $this->db->query(
        "INSERT INTO mst_tarif (
            tarif_id, old_id,
            parent_id, tarif_nm, tarif_tp, akun_id, created_by, is_kwitansi, tarif_st
          )
          SELECT 
            IF(kjenistindakan != '', CONCAT(kgoltindakan,'.',RIGHT(kjenistindakan, 3),'.',RIGHT(ktindakan, 3)),CONCAT(kgoltindakan,'.000.',RIGHT(ktindakan,3))), ktindakan,
            IF(kjenistindakan != '', CONCAT(kgoltindakan,'.',RIGHT(kjenistindakan, 3)), CONCAT(kgoltindakan,'.000')),
            tindakan, 'D', '04.01.01', 'System', 0, 1
          FROM rspermata.ttindakan a GROUP BY tindakan
          ORDER BY ktindakan
        ");
      //update dot
      $tarif = $this->db->get("mst_tarif")->result_array();
      foreach ($tarif as $row) {
        $row['tarif_nm'] = strtoupper($row['tarif_nm']);
        $this->db->where('tarif_id', $row['tarif_id'])->update('mst_tarif', $row);
      }
      
      // mapping tarif kelas
      $this->db->query("TRUNCATE mst_tarif_kelas");
      $tarif = $this->db->query("SELECT a.*, b.tarif_id FROM rspermata.ttindakan a JOIN mst_tarif b ON a.ktindakan = b.old_id")->result_array();
      // var_dump($tarif);
      foreach ($tarif as $row) {
        $data = array(
          'tarifkelas_id' => $row['tarif_id'].'.02',
          'tarif_id' => $row['tarif_id'],
          'kelas_id' => '02',
          'nominal' => $row['TARIFVIP']
        );
        $this->db->insert('mst_tarif_kelas', $data);
        $data = array(
          'tarifkelas_id' => $row['tarif_id'].'.04',
          'tarif_id' => $row['tarif_id'],
          'kelas_id' => '04',
          'nominal' => $row['TARIFKELAS1']
        );
        $this->db->insert('mst_tarif_kelas', $data);
        $data = array(
          'tarifkelas_id' => $row['tarif_id'].'.05',
          'tarif_id' => $row['tarif_id'],
          'kelas_id' => '05',
          'nominal' => $row['TARIFKELAS2']
        );
        $this->db->insert('mst_tarif_kelas', $data);
        $data = array(
          'tarifkelas_id' => $row['tarif_id'].'.06',
          'tarif_id' => $row['tarif_id'],
          'kelas_id' => '06',
          'nominal' => $row['TARIFKELAS3']
        );
        $this->db->insert('mst_tarif_kelas', $data);
      }
    }
    
  }

  function auth_reset() {
    $d = html_escape($this->input->post());
    $sql = "SELECT * FROM app_config";
    $user = $this->db->query($sql)->row_array();

    if ($d['pass_reset'] == $user['pass_reset_data']) {
      return 1;
    } else {
      return 0;
    }
  }

}
