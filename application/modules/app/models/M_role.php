<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_role extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND a.role_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' ";
    }
    if (@$cookie['search']['role_group'] != '') {
      $where .= "AND a.role_group LIKE '%" . $this->db->escape_like_str($cookie['search']['role_group']) . "%' ";
    }
    if (@$cookie['search']['role_nm'] != '') {
      $where .= "AND a.role_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['role_nm']) . "%' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT * FROM app_role a 
      $where
      ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM app_role a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM app_role a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function list_role_by_field($role_field = 'ALL')
  {
    $sql = "SELECT * FROM app_role WHERE role_field=? ORDER BY role_cd ASC";
    $query = $this->db->query($sql, $role_field);
    return $query->result_array();
  }

  function get_data($id)
  {
    $sql = "SELECT * FROM app_role WHERE role_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    if ($id == null) {
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('app_role', $data);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('role_id', $id)->update('app_role', $data);
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('role_id', $id)->update('app_role', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('role_id', $id)->delete('app_role');
    } else {
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('role_id', $id)->update('app_role', $data);
    }
  }
}
