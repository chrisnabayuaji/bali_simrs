<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_user extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND (a.user_name LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' OR a.user_realname LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' ) ";
    }
    if (@$cookie['search']['role_id'] != '') {
      $where .= "AND a.role_id = '" . $this->db->escape_like_str($cookie['search']['role_id']) . "' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT a.*,role_nm FROM app_user a 
            JOIN app_role b ON a.role_id = b.role_id
            $where
            ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM app_user a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM app_user a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function list_user_by_field($user_field = 'ALL')
  {
    $sql = "SELECT * FROM app_user WHERE user_field=? ORDER BY user_cd ASC";
    $query = $this->db->query($sql, $user_field);
    return $query->result_array();
  }

  function get_data($id)
  {
    $sql = "SELECT * FROM app_user WHERE user_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    $lokasi = null;
    if (isset($data['lokasi_id'])) {
      $lokasi = $data['lokasi_id'];
    }
    unset($data['lokasi_id']);
    if ($id == null) {
      // upload img
      $data['user_photo'] = $this->upload_file_process($data);
      //
      if (@$data['user_password'] != '') {
        $data['user_password'] = hash_password($data['user_password']);
      }
      // unset data
      unset($data['cb_password']);
      unset($data['confirm_user_password']);
      unset($data['set_user_password']);
      //
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      if ($data['user_photo'] == 'error_img') {
        $this->session->set_flashdata('flash_error', 'Data gagal disimpan <br> Format gambar salah');
      } else {
        $this->db->insert('app_user', $data);
        if ($lokasi != null) {
          $this->save_lokasi($this->db->insert_id(), $lokasi);
        }
        $this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
      }
    } else {
      $get_user = $this->get_data($id);
      // upload img
      $user_photo = $this->upload_file_process($data, $id);
      if ($user_photo != '') {
        $data['user_photo'] = $user_photo;
      }
      //
      if (@$data['user_password'] != '') {
        $data['user_password'] = hash_password($data['user_password']);
      } else {
        $data['user_password'] = $get_user['user_password'];
      }
      // unset data
      unset($data['cb_password']);
      unset($data['confirm_user_password']);
      unset($data['set_user_password']);
      //
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      if (isset($data['user_photo']) == 'error_img') {
        $this->session->set_flashdata('flash_error', 'Data gagal disimpan <br> Format gambar salah');
        //update img to empty
        $data_img['user_photo'] = '';
        $this->db->where('user_id', $id)->update('app_user', $data_img);
      } else {
        $this->db->where('user_id', $id)->update('app_user', $data);
        $this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
      }
      if ($lokasi != null) {
        $this->save_lokasi($id, $lokasi);
      }
    }
  }

  public function save_lokasi($id = null, $data = null)
  {
    if ($id != null && $data != null) {
      $this->db->where('user_id', $id)->delete('app_user_lokasi');
      foreach ($data as $k => $v) {
        $d = array(
          'user_id' => $id,
          'lokasi_id' => $v,
          'created_by' => $this->session->userdata('sess_user_realname'),
          'created_at' => date('Y-m-d H:i:s')
        );
        $this->db->insert('app_user_lokasi', $d);
      }
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('sess_user_realname');
    $this->db->where('user_id', $id)->update('app_user', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('user_id', $id)->delete('app_user');
      $this->db->where('user_id', $id)->delete('app_user_lokasi');
    } else {
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('user_id', $id)->update('app_user', $data);
    }
  }

  function delete_photo($id = null, $user_photo = null)
  {
    $this->delete_file_process($user_photo);
    //
    $data['user_photo'] = '';
    $result = $this->db->where('user_id', $id)->update('app_user', $data);
    return $result;
  }

  function upload_file_process($data = null, $user_id = null)
  {
    $result   = '';
    if (@$_FILES['user_photo']['tmp_name'] != '') {
      $path_dir       = "assets/images/user/";
      $tmp_name       = @$_FILES['user_photo']['tmp_name'];
      $fupload_name   = @$_FILES['user_photo']['name'];
      //
      if ($user_id != '') {
        $user = $this->get_data($user_id);
        $result = $this->_upload_img($path_dir, $tmp_name, $fupload_name, $user['user_photo']);
      } else {
        $result = $this->_upload_img($path_dir, $tmp_name, $fupload_name);
      }
    }
    return $result;
  }

  function _upload_img($path_dir = null, $tmp_name = null, $fupload_name = null, $old_file = null)
  {
    //
    $this->load->library('upload');
    $this->load->library('image_lib');
    //
    $name_img = create_title_img($path_dir, $tmp_name, $fupload_name, $old_file);
    $src_file_name = 'user_photo';
    //
    $config['file_name'] = $name_img;
    $config['upload_path'] = $path_dir; //path folder
    $config['allowed_types'] = 'jpg|png|jpeg|gif'; //type yang dapat diakses bisa anda sesuaikan

    $this->upload->initialize($config);
    if (!empty($fupload_name)) {
      if ($this->upload->do_upload($src_file_name)) {
        $gbr = array('upload_data' => $this->upload->data());
        // cek resolusi gambar
        $nama_gambar = $path_dir . $gbr['upload_data']['file_name'];
        $data = getimagesize($nama_gambar);
        $width = $data[0];
        $height = $data[1];
        // pembagian
        $bagi_width = $width / 4;
        $bagi_height = $height / 4;
        // Compress Image
        $config['image_library'] = 'gd2';
        $config['source_image'] = $gbr['upload_data']['full_path'];
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['quality'] = '20%';
        $config['width'] = round($bagi_width);
        $config['height'] = round($bagi_height);
        $config['new_image'] = $path_dir . $gbr['upload_data']['file_name'];
        $this->image_lib->initialize($config);
        // $this->load->library('image_lib', $config);
        $this->image_lib->resize();
        $this->image_lib->clear();
        //
        // $gambar_1 = $gbr['upload_data']['file_name'];
        $result = $name_img;
      } else {
        $result = 'error_img';
      }
      //
      return $result;
    }
  }

  function delete_file_process($user_photo = null)
  {
    $path_dir = "assets/images/user/";
    $result = unlink($path_dir . $user_photo);
    return $result;
  }

  public function list_lokasi($id = null)
  {
    $where = '';
    if ($id != null) {
      $sql = $this->db->query(
        "SELECT a.*, b.lokasi_id AS user_lokasi
        FROM mst_lokasi a
        LEFT JOIN app_user_lokasi b ON b.lokasi_id = a.lokasi_id AND b.user_id = '$id'
        ORDER BY a.lokasi_id"
      );
    } else {
      $sql = $this->db->query(
        "SELECT a.*
        FROM mst_lokasi a
        ORDER BY a.lokasi_id"
      );
    }
    return $sql->result_array();
  }


  public function list_pegawai()
  {
    $sql = "SELECT * FROM mst_pegawai a WHERE a.is_deleted = 0 AND a.jenispegawai_cd IN (01, 02, 03, 04, 05, 06, 07, 14, 17, 18, 19, 20, 21, 24, 25)";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function create_user()
  {
    $list_pegawai = $this->list_pegawai();
    foreach ($list_pegawai as $pegawai) {
      $data['role_id'] = get_role_id($pegawai['jenispegawai_cd']);
      $data['pegawai_id'] = $pegawai['pegawai_id'];
      $data['user_name'] = get_username($pegawai['pegawai_nm']);
      $data['user_password'] = hash_password('rsiapermata');
      $data['user_realname'] = $pegawai['pegawai_nm'];
      $data['user_cd'] = $pegawai['pegawai_id'];
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('app_user', $data);
      $user_id = $this->db->insert_id();
      //
      $list_lokasi = $this->list_lokasi();
      $this->db->where('user_id', $user_id)->delete('app_user_lokasi');
      foreach ($list_lokasi as $v) {
        $d = array(
          'user_id' => $user_id,
          'lokasi_id' => $v['lokasi_id'],
          'created_by' => $this->session->userdata('sess_user_realname'),
          'created_at' => date('Y-m-d H:i:s')
        );
        $this->db->insert('app_user_lokasi', $d);
      }
    }
  }
}
