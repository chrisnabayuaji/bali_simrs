<!-- js -->
<?php $this->load->view('app/app/_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-lg-12">
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Username <span class="text-danger">*<span></label>
        <div class="col-lg-5 col-md-5">
          <input type="text" class="form-control" name="" value="<?=@$main['user_name']?>" disabled>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Real Name <span class="text-danger">*<span></label>
        <div class="col-lg-8 col-md-8">
          <input type="text" class="form-control" name="user_realname" value="<?=@$main['user_realname']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Photo</label>
        <div class="col-lg-6 col-md-8">
          <input type="file" name="user_photo" class="form-control" id="user_photo">
          <?php if (@$main['user_photo'] !=''): ?>
          <div id="box_img">
            <div class="text-danger small-text font-weight-semibold">* Upload gambar untuk mengubah</div>
            <div class="card mb-2 mt-1">
              <div class="card-body">
                <div class="text-center">
                  <img src="<?=base_url()?>assets/images/user/<?=@$main['user_photo']?>" class="img-lg rounded">
                  <button type="button" class="btn btn-xs btn-danger mt-2 delete_photo" data-id="<?=@$main['user_id']?>" data-file="<?=@$main['user_photo']?>"><i class="far fa-trash-alt"></i> Hapus Gambar</button>
                </div>
              </div>
            </div>
          </div>
          <?php endif; ?>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4"></label>
        <div class="col-lg-8">
          <span class="text-danger small-text font-weight-semibold">Untuk mengganti password silahkan isi form dibawah<span>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Password <span class="text-danger">*<span></label>
        <div class="col-lg-5 col-md-5">
          <input type="text" class="form-control" name="user_password" id="user_password" autocomplete="off">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Konfirmasi Password <span class="text-danger">*<span></label>
        <div class="col-lg-5 col-md-5">
          <input type="text" class="form-control" name="confirm_user_password" id="confirm_user_password" autocomplete="off">
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>