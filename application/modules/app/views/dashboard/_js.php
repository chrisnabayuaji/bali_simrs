<script type="text/javascript">
  $(document).ready(function() {
    ProfilRumahSakit();
    StatistikRumahSakit();
    GrafikKunjunganPasien();
    ListBerita();
  });

  function ProfilRumahSakit() {
    $('#profil-rumah-sakit').html('<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i></div><div class="text-center mt-2">Memuat...</div>');
    $.get('<?= site_url($nav['nav_url']) ?>/profil_rumah_sakit', null, function(data) {
      $('#profil-rumah-sakit').html(data.html);
    }, 'json');
  }

  function StatistikRumahSakit() {
    $('#statistik-rumah-sakit').html('<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i></div><div class="text-center mt-2">Memuat...</div>');
    $.get('<?= site_url($nav['nav_url']) ?>/statistik_rumah_sakit', null, function(data) {
      $('#statistik-rumah-sakit').html(data.html);
    }, 'json');
  }

  function GrafikKunjunganPasien() {
    $('#grafik-kunjungan-pasien').html('<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i></div><div class="text-center mt-2">Memuat...</div>');
    $.get('<?= site_url($nav['nav_url']) ?>/grafik_kunjungan_pasien', null, function(data) {
      $('#grafik-kunjungan-pasien').html(data.html);
    }, 'json');
  }

  function ListBerita() {
    $('#list-berita').html('<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i></div><div class="text-center mt-2">Memuat...</div>');
    $.get('<?= site_url($nav['nav_url']) ?>/list_berita', null, function(data) {
      $('#list-berita').html(data.html);
    }, 'json');
  }
</script>