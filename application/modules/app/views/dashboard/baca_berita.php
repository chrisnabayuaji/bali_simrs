<?php if (count($list_img) > 0) : ?>
  <?php if (count($list_img) > 1) : ?>
    <div id="carouselIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <?php for ($i = 0; $i < count($list_img); $i++) : ?>
          <li data-target="#carouselIndicators" data-slide-to="<?= $i ?>" <?= ($i == 0) ? 'class="active"' : '' ?>></li>
        <?php endfor; ?>
      </ol>
      <div class="carousel-inner">
        <?php
        $no = 1;
        foreach ($list_img as $row) :
        ?>
          <div class="carousel-item <?= ($no == 1) ? 'active' : '' ?>">
            <img class="d-block w-100" src="<?= base_url(@$row['image_path'] . @$row['image_name']) ?>">
            <div class="carousel-caption d-none d-md-block">
              <p><?= $row['image_description'] ?></p>
            </div>
          </div>
        <?php $no++;
        endforeach;
        ?>
      </div>
      <a class="carousel-control-prev" href="#carouselIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  <?php else : ?>
    <img src="<?= base_url(@$first_img['image_path'] . @$first_img['image_name']) ?>" style="width: 100%;">
  <?php endif; ?>
<?php endif; ?>
<h3 class="font-weight-bold text-primary mt-3 mb-3"><?= @$main['news_title'] ?></h3>
<ul class="list-inline mb-2">
  <li class="list-inline-item pr-4">
    <div class="text-muted"><i class="fas fa-calendar-alt mr-1 text-primary"></i> <?= to_date_indo(@$main['created_at']) ?></div>
  </li>
  <li class="list-inline-item pr-4">
    <div class="text-muted"><i class="fas fa-user-circle mr-1 text-primary"></i> <?= @$main['author_nm'] ?></div>
  </li>
  <li class="list-inline-item pr-4">
    <div class="text-muted"><i class="fas fa-book-reader mr-1 text-primary"></i> dibaca <?= @$main['news_hit'] ?> kali</div>
  </li>
</ul>
<p><?= nl2br(@$main['news_content']) ?></p>
</div>