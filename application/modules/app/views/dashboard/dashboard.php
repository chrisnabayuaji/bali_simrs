<!-- JS -->
<?php $this->load->view('_js'); ?>
<!-- End JS -->
<div class="content-wrapper mw-100">
  <div class="row d-flex justify-content-center">
    <div class="col-md-12 col-xl-12 grid-margin stretch-card mt-n3 mb-0">
      <div class="card card-shadow">
        <div class="tab-content">
          <div class="tab-pane fade show active">
            <div class="tab-pane-body">
              <div class="row">
                <div class="col-md-10 col-xl-10 pb-2">
                  <form>
                    <div class="form-group row">
                      <label class="col-lg-2 col-md-2 pr-0 col-form-label font-weight-bold text-left" style="margin-right: -100px;"><i class="fas fa-tags"></i> NAVIGASI : </label>
                      <div class="col-lg-9 col-md-9">
                        <select name="ses_user_st" id="menu_module" class="select-search-menu w-100">
                          <option value="">- Pilih Menu -</option>
                          <?php foreach ($list_nav as $nav) : ?>
                            <?php if ($nav['parent_id'] == 0) : ?>
                              <option value="<?= site_url($nav['nav_url']) ?>"><?= $nav['nav_nm'] ?></option>
                            <?php elseif ($nav['parent_id'] != 0 && $nav['nav_url'] == '#') : ?>
                              <optgroup label="&nbsp;&nbsp;&nbsp; <?= $nav['nav_nm'] ?>"></optgroup>
                            <?php else : ?>
                              <option value="<?= site_url($nav['nav_url']) ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?= $nav['nav_nm'] ?></option>
                            <?php endif; ?>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div class="border-bottom border-2 mt-n1 mb-2"></div>
              <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8">
                  <div style="position: relative; height: 43vh; width: 100%; overflow: hidden; border-radius: 4px;">
                    <?php if (@$identitas['img_rumah_sakit'] != '') : ?>
                      <img src="<?= base_url() ?>assets/images/icon/<?= @$identitas['img_rumah_sakit'] ?>" alt="image" style="position: absolute; top: -9999px; left: -9999px; right: -9999px; bottom: -9999px; margin: auto; width: 100%;" />
                    <?php endif; ?>
                  </div>
                </div>
                <div class="col-md-4 col-lg-4 col-xl-4">
                  <h4 class="card-title border-bottom border-2 pb-2 mb-3 font-weight-bold"><i class="fas fa-clinic-medical"></i> Profil Rumah Sakit</h4>
                  <div class="mt-n2 ml-1" id="profil-rumah-sakit"></div>
                  <h4 class="card-title border-bottom border-2 pb-2 mb-3 font-weight-bold"><i class="fas fa-chart-line"></i> Statistik Rumah Sakit</h4>
                  <div class="mt-n2 ml-1" id="statistik-rumah-sakit"></div>
                </div>
              </div>
            </div>
            <div class="tab-pane-body d-flex justify-content-start mt-4 ml-n3 mr-n3">
              <?php foreach ($list_nav_module as $nav) : ?>
                <?php if ($nav['nav_id'] != '01') : ?>
                  <div class="col-menu-icon col-menu-icon-dashboard">
                    <div onclick="window.location.href='<?= site_url($nav['nav_url']) ?>'" class="card card-menu-icon-dashboard card-shadow">
                      <div class="card-body text-center p-2">
                        <img class="img-menu-icon img-menu-icon-dashboard lazy-load" data-src="<?= base_url() ?>assets/images/module/<?= $nav['nav_icon'] ?>">
                        <div class="title-menu-icon title-menu-icon-dashboard font-weight-bold"><?= $nav['nav_nm'] ?></div>
                      </div>
                    </div>
                  </div>
                <?php endif; ?>
              <?php endforeach; ?>
            </div>
            <div class="tab-pane-body pt-5">
              <div class="row">
                <div class="col-md-7 col-xl-7">
                  <h4 class="card-title border-bottom border-2 pb-2 mb-3 font-weight-bold"><i class="fas fa-chart-line"></i> Grafik Kunjungan Pasien Harian <?= month(date('m')) ?> <?= date('Y') ?></h4>
                  <div id="grafik-kunjungan-pasien"></div>
                  <div id="container" style="width: 100%;">
                    <canvas id="canvas"></canvas>
                  </div>
                </div>
                <div class="col-md-5 col-xl-5">
                  <h4 class="card-title border-bottom border-2 pb-2 mb-3 font-weight-bold"><i class="far fa-newspaper"></i> Berita Terbaru</h4>
                  <div id="list-berita"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#menu_module').on('change', function() {
      var link = $(this).val();
      window.location = link;
    })
  })
</script>