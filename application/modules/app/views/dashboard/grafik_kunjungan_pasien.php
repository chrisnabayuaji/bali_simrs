<script>
  var color = Chart.helpers.color;
  var ctx = document.getElementById('canvas').getContext('2d');
  window.myBar = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: [
        <?php
        $count_day = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
        for ($i = 1; $i <= $count_day; $i++) :
          $num = sprintf("%02d", $i);
        ?> '<?= $i ?>',
        <?php endfor; ?>
      ],
      datasets: [
        <?php
        $no = 1;
        foreach ($list_kunjungan_harian as $row) :
        ?> {
            label: '<?= $row['lokasi_nm'] ?>',
            backgroundColor: color('<?= color_chart($no) ?>').alpha(0.5).rgbString(),
            borderColor: '<?= color_chart($no) ?>',
            borderWidth: 1,
            data: [
              <?php
              $count_day = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));
              for ($n = 1; $n <= $count_day; $n++) :
                $num = sprintf("%02d", $n);
              ?> '<?= $row['jml_tgl_' . $num] ?>',
              <?php endfor; ?>
            ]
          },
        <?php
          $no++;
        endforeach;
        ?>
      ]
    },
    options: {
      responsive: true,
      legend: {
        position: 'bottom',
      },
      title: {
        display: false,
        text: ''
      },
      tooltips: {
        mode: 'label',
        callbacks: {
          label: function(t, d) {
            var dstLabel = d.datasets[t.datasetIndex].label;
            var yLabel = t.yLabel;
            return dstLabel + ' : ' + yLabel;
          }
        }
      },
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });
</script>