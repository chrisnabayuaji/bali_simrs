<?php if (count($main) > 0) : ?>
  <?php foreach ($main as $news) : ?>
    <div class="bd-callout bd-callout-warning">
      <?php if ($news['first_image']['image_name'] != '') : ?>
        <div class="row">
          <div class="col-lg-5 mr-n3">
            <div class="images-normal change-ratio">
              <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/baca_berita/' . $news['news_id'] ?>" modal-header="hidden" modal-title="" modal-size="lg" class="modal-href">
                <img class="card-img-top rounded w-100" src="<?= base_url() . $news['first_image']['image_path'] . $news['first_image']['image_name'] ?>" alt="Image">
              </a>
            </div>
          </div>
          <div class="col-lg-7">
            <div style="font-size: 15px;" class="text-primary font-weight-bold mb-2">
              <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/baca_berita/' . $news['news_id'] ?>" modal-header="hidden" modal-title="" modal-size="lg" class="modal-href">
                <?= $news['news_title'] ?>
              </a>
            </div>
            <div style="font-size: 13px;"><?= word_limiter(strip_tags($news['news_content']), 17) ?></div>
          </div>
        </div>
      <?php else : ?>
        <div style="font-size: 15px;" class="text-primary font-weight-bold mb-2">
          <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/baca_berita/' . $news['news_id'] ?>" modal-header="hidden" modal-title="" modal-size="lg" class="modal-href">
            <?= $news['news_title'] ?>
          </a>
        </div>
        <div style="font-size: 13px;"><?= word_limiter(strip_tags($news['news_content']), 17) ?></div>
      <?php endif; ?>
    </div>
  <?php endforeach; ?>

  <?php
  $paging_url_end = '';
  if (@$paging->url_end != '') $paging_url_end = '/' . $paging->url_end;
  $url = 'app/dashboard/list_berita';
  ?>
  <div class="col-pagination pb-2">
    <ul class="pagination d-flex flex-wrap justify-content-end pagination-primary">
      <?php if ($paging->start_link) : ?>
        <li class="page-item">
          <a class="page-link link-pagination" href="<?= site_url("$url/$paging->start_link/$o" . $paging_url_end) ?>">First</a>
        </li>
      <?php endif; ?>
      <?php if ($paging->prev) : ?>
        <li class="page-item">
          <a class="page-link link-pagination" href="<?= site_url("$url/$paging->prev/$o" . $paging_url_end) ?>">Prev</a>
        </li>
      <?php endif; ?>

      <?php for ($i = $paging->c_start_link; $i <= $paging->c_end_link; $i++) : ?>
        <li class="page-item <?php jecho($p, $i, "active") ?>">
          <a class="page-link link-pagination" href="<?= site_url("$url/$i/$o" . $paging_url_end) ?>"><?= $i ?></a>
        </li>
      <?php endfor; ?>
      <?php if ($paging->next) : ?>
        <li class="page-item">
          <a class="page-link link-pagination" href="<?= site_url("$url/$paging->next/$o" . $paging_url_end) ?>">Next</a>
        </li>
      <?php endif; ?>
      <?php if ($paging->end_link) : ?>
        <li class="page-item">
          <a class="page-link link-pagination" href="<?= site_url("$url/$paging->end_link/$o" . $paging_url_end) ?>">Last</a>
        </li>
      <?php endif; ?>
    </ul>
  </div>

  <script>
    $(function() {
      $('.link-pagination').bind('click', function(e) {
        e.preventDefault();
        $(this).each(function() {
          var i = $(this).attr('href');
          $('#list-berita').html('<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i></div><div class="text-center mt-2">Memuat...</div>');
          $.get(i, null, function(data) {
            $('#list-berita').html(data.html);
          }, 'json');
        })
      });

      $('.modal-href').click(function(e) {
        e.preventDefault();
        var modal_title = $(this).attr("modal-title");
        var modal_size = $(this).attr("modal-size");
        var modal_custom_size = $(this).attr("modal-custom-size");
        var modal_header = $(this).attr("modal-header");
        var modal_content_top = $(this).attr("modal-content-top");

        $("#modal-size").removeClass('modal-lg').removeClass('modal-md').removeClass('modal-sm');

        $("#modal-title").html(modal_title);
        $("#modal-size").addClass('modal-' + modal_size);
        if (modal_custom_size) {
          $("#modal-size").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
        }
        if (modal_content_top) {
          $(".modal-content-top").attr('style', 'margin-top: ' + modal_content_top + ' !important');
        }
        if (modal_header == 'hidden') {
          $("#modal-header").addClass('d-none');
        } else {
          $("#modal-header").removeClass('d-none');
        }
        $("#myModal").modal('show');
        $("#modal-body").html('<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i><br>Loading</div>');
        $.post($(this).data('href'), function(data) {
          $("#modal-body").html(data.html);
        }, 'json');
      });
    });
  </script>
<?php else : ?>
  <div>Tidak ada berita</div>
<?php endif; ?>