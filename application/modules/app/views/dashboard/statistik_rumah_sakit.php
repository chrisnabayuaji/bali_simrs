<ul class="bullet-line-list">
  <li>
    <h6>Pasien Terdaftar Hari Ini</h6>
    <ul class="bullet-line-list">
      <li>
        <h6>Rawat Jalan : <?= @$rawat_jalan ?></h6>
      </li>
      <li>
        <h6>Rawat Inap Atas : <?= @$rawat_inap_atas ?></h6>
      </li>
      <li>
        <h6>Rawat Inap Bawah : <?= @$rawat_inap_bawah ?></h6>
      </li>
      <li>
        <h6>Rawat Darurat : <?= @$rawat_darurat ?></h6>
      </li>
    </ul>
  </li>
  <li>
    <h6>Pasien Pulang Hari Ini : <?= @$pasien_pulang_hari_ini ?></h6>
  </li>
  <li>
    <h6>Pasien Masih Inap : <?= @$pasien_masih_inap ?></h6>
  </li>
</ul>