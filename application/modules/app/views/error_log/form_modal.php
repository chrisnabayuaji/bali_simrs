<div class="row">
  <div class="col-12">
    <table class="table table-hover table-striped table-bordered table-fixed">
      <thead>
        <tr>
          <th class="text-center" width="36">No</th>
          <th class="text-center" width="100">Jenis Log</th>
          <th class="text-center" width="150">Waktu</th>
          <th class="text-center">Pesan</th>
        </tr>
      </thead>
      <?php if (@$main == null) : ?>
        <tbody>
          <tr>
            <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
          </tr>
        </tbody>
      <?php else : ?>
        <tbody>
          <?php
          $i = 1;
          foreach ($main as $row) :
            $error = explode('-->', $row);
            $pesan = '';
            $jam = '';
            $jenis = '';
            if (count($error) > 1) {
              // ERROR - 2020-10-27 15:51:23
              // $pesan = $error[1];
              foreach ($error as $key => $value) {
                if ($key > 0) {
                  $pesan .= $value;
                }
              }
              $raw = explode(' - ', $error[0]);
              $jenis = $raw[0];
              $jam = $raw[1];
            } else {
              $pesan = $row;
            }
          ?>
            <tr>
              <td class="text-center" width="36">
                <?= (count($error) > 1) ? $i++ : '' ?>
              </td>
              <td class="text-center" width="100">
                <?= (count($error) > 1) ? $jenis : '' ?>
              </td>
              <td class="text-center" width="150">
                <?= (count($error) > 1) ? $jam : '' ?>
              </td>
              <td class="text-left">
                <?= $pesan ?>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      <?php endif; ?>
    </table>
  </div>
</div>