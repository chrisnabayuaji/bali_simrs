<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline col-title">
        <div class="col-back">
          <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
        </div>
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          Konfigurasi <?= $nav['nav_nm'] ?>
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item"><a href="#"><i class="fas fa-cog"></i> Konfigurasi</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><?= $nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item active"><span>Index</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>
  <!-- flash message -->
  <div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
  <!-- /flash message -->
  <div class="row full-page mt-4 mb-n2">
    <div class="col-lg-12 grid-margin-xl-0 stretch-card">
      <div class="card border-none">
        <div class="card-body">
          <div class="row mt-2">
            <div class="col-md-12">
              <div class="table-responsive">
                <form id="form-multiple" action="<?= site_url() . '/' . $nav['nav_url'] . '/multiple/enable' ?>" method="post">
                  <table class="table table-hover table-striped table-bordered table-fixed">
                    <thead>
                      <tr>
                        <th class="text-center" width="36">No</th>
                        <th class="text-center" width="50">Aksi</th>
                        <th class="text-center" width="100">Ukuran</th>
                        <th class="text-center" width="200">Terakhir di modifikasi</th>
                        <th class="text-center">Nama Berkas</th>
                      </tr>
                    </thead>
                    <?php if (@$main == null) : ?>
                      <tbody>
                        <tr>
                          <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                        </tr>
                      </tbody>
                    <?php else : ?>
                      <tbody>
                        <?php $i = 1;
                        foreach ($main as $row) : ?>
                          <tr>
                            <td class="text-center" width="36"><?= ($i++) ?></td>
                            <td class="text-center" width="50">
                              <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/form_modal/' . $row['file_name'] ?>" modal-title="Lihat Data" modal-size="lg" class="btn btn-primary btn-table modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Lihat Data"><i class="fas fa-eye"></i></a>
                            </td>
                            <td class="text-right" width="100"><?= $row['file_size'] ?></td>
                            <td class="text-center" width="200"><?= $row['modified'] ?></td>
                            <td class="text-left"><?= $row['file_name'] ?></td>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                    <?php endif; ?>
                  </table>
                </form>
              </div>
            </div>
          </div><!-- /.row -->
          <div class="row mt-2">
            <div class="col-6 text-middle">
              <div class="row">
                <div class="col">
                  <div class="row pagination-info">
                    <div class="col-2">
                      <?php if ($nav['_update'] == 1 || $nav['_delete'] == 1) : ?>
                        <div class="input-group-prepend">
                          <button type="button" class="btn btn-block btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            Aksi Multi
                          </button>
                          <div class="dropdown-menu">
                            <?php if ($nav['_update'] == 1) : ?>
                              <a class="dropdown-item" href="javascript:multipleAction('enable')">Aktif</a>
                              <a class="dropdown-item" href="javascript:multipleAction('disable')">Non Aktif</a>
                            <?php endif; ?>
                            <?php if ($nav['_delete'] == 1) : ?>
                              <a class="dropdown-item" href="javascript:multipleAction('delete')">Hapus</a>
                            <?php endif; ?>
                          </div>
                        </div>
                      <?php endif; ?>
                    </div>
                    <div class="col-md-8 p-0" style="padding-top:3px !important">
                      <form id="form-paging" action="<?= site_url() . '/app/per_page/' . $nav['nav_id'] ?>" method="post">
                        <label><i class="fas fa-bookmark"></i> Perhalaman</label>
                        <select name="per_page" class="select-pagination" onchange="$('#form-paging').submit()">
                          <option value="10" <?php if ($cookie['per_page'] == 10) {
                                                echo 'selected';
                                              } ?>>10</option>
                          <option value="50" <?php if ($cookie['per_page'] == 50) {
                                                echo 'selected';
                                              } ?>>50</option>
                          <option value="100" <?php if ($cookie['per_page'] == 100) {
                                                echo 'selected';
                                              } ?>>100</option>
                        </select>
                        <label class="ml-2"><?= @$pagination_info ?></label>
                      </form>
                    </div>
                  </div><!-- /.row -->
                </div>
              </div>
            </div>
            <div class="col-6 text-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>