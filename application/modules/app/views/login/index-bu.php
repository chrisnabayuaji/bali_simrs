<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?= $config['app_shorttitle'] ?></title>
	<!-- css -->
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/theme-color/<?= $config['primary_color'] ?>.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/login.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/root.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/bootstrap/bootstrap.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/components/components.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/menu.css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/plugins/font-awesome/font-awesome.css">
	<!-- javascript -->
	<script src="<?= base_url() ?>assets/js/jquery.js"></script>
	<script src="<?= base_url() ?>assets/js/bootstrap.js"></script>
	<script src="<?= base_url() ?>assets/plugins/jquery-validation/jquery.validate.min.js"></script>
	<script src="<?= base_url() ?>assets/plugins/jquery-validation/localization/messages_id.min.js"></script>
</head>

<body>

	<img src="<?= base_url() ?>assets/images/bg/bg-login.png" class="bg">
	<div class="container-scroller">
		<div class="container-fluid page-body-wrapper full-page-wrapper">
			<div class="row w-100 mx-0">
				<div class="col-lg-8 mx-auto">
					<div class="logo-login">
						<?php if (@$identitas['logo_rumah_sakit'] != '') : ?>
							<img src="<?= base_url() ?>assets/images/icon/<?= @$identitas['logo_rumah_sakit'] ?>" class="img-logo-login rounded-circle">
						<?php endif; ?>
						<div class="title-logo-login">
							<div class="text-dark"><?= @$config['title_logo_login'] ?></div>
							<div class="text-dark font-weight-bold"><?= @$config['sub_title_logo_login'] ?></div>
						</div>
					</div>
				</div>
			</div>
			<div class="content-wrapper d-flex align-items-center auth px-0">
				<div class="row w-100 mx-0">
					<div class="col-lg-8 mx-auto">
						<div class="card">
							<div class="card-body">
								<div class="row col-login">
									<div class="col-lg-8 col-sm-12 col-xs-12 col-login-left">
										<div class="font-weight-normal">Dengan Akun Ini Anda Bisa Mengakses Layanan : </div>
										<div class="col-icon-login">
											<div class="icon-login text-center">
												<a href="http://antrian.rsiapermata.com" class="link-icon" target="_blank">
													<img src="<?= base_url() ?>assets/images/icon/antrian-online-icon.png" class="img-icon-login">
													<div class="text-icon-login">Antrian Online</div>
												</a>
											</div>
											<!-- <div class="icon-login text-center">
												<a href="http://kebumenprimasarana.com/anjungan_rsia_permata" class="link-icon" target="_blank">
													<img src="<?= base_url() ?>assets/images/icon/anjungan-icon.png" class="img-icon-login">
													<div class="text-icon-login">Anjungan</div>
												</a>
											</div> -->
											<div class="icon-login text-center">
												<a href="http://spgdt.rsiapermata.com" class="link-icon" target="_blank">
													<img src="<?= base_url() ?>assets/images/icon/spgdt-icon.png" class="img-icon-login">
													<div class="text-icon-login">SPGDT</div>
												</a>
											</div>
										</div>
										<div class="desc-login">
											<div class="desc-login-text">Akun ini hanya untuk pihak rumah sakit yang membantu melayani pasien. Untuk membuat akun ini harap menghubungi pihak Admin <?= @$identitas['rumah_sakit'] ?> <?= ucwords(strtolower(clear_kab_kota($identitas['kabupaten']))) ?></div>
										</div>
									</div>
									<div class="col-lg-4 col-sm-12 col-xs-12 col-login-right">
										<div class="text-center">
											<img src="<?= base_url() ?>assets/images/icon/<?= @$identitas['logo_rumah_sakit'] ?>" class="img-logo-form rounded-circle">
											<div class="text-logo-form">
												<h5>LOGIN USER</h5>
											</div>
										</div>
										<form id="form" action="<?= site_url() . '/app/auth/login_action' ?>" method="post" autocomplete="off">
											<div class="form-group">
												<div class="input-group">
													<div class="input-group-prepend">
														<div class="input-group-text text-center" style="color: #3498db !important;"><i class="fas fa-user-lock"></i></div>
													</div>
													<input type="text" class="form-control-login" id="user_name" name="user_name" placeholder="Username" required="" autocomplete="off">
												</div>
											</div>
											<div class="form-group">
												<div class="input-group">
													<div class="input-group-prepend">
														<div class="input-group-text text-center" style="color: #3498db !important;"><i class="fas fa-lock"></i></div>
													</div>
													<input type="password" class="form-control-login" id="user_password" name="user_password" placeholder="Password" required="" autocomplete="off">
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<button type="submit" id="btn-login" class="btn btn-primary btn-submit float-right">Login</button>
												</div>
											</div>
											<?php if (@$this->session->flashdata('flash_error')) : ?>
												<div class="row mt-3 mb-n4" id="flash_error">
													<div class="col-12">
														<div class="alert alert-danger alert-dismissible">
															<?= $this->session->flashdata('flash_error') ?>
														</div>
													</div>
												</div>
											<?php endif; ?>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-footer">
		<div class="text-footer text-center" style="background-color: #3498db !important;"><?= @$config['copyright'] ?></div>
	</div>

	<script>
		$(document).ready(function() {
			$("#form").validate({
				rules: {

				},
				messages: {
					user_name: 'Nama Pengguna harap diisi!',
					user_password: 'Kata Sandi harap diisi!'
				},
				errorElement: "em",
				errorPlacement: function(error, element) {
					error.addClass("invalid-feedback");
					if (element.prop("type") === "checkbox") {
						error.insertAfter(element.next("label"));
					} else {
						error.insertAfter(element);
					}
				},
				highlight: function(element, errorClass, validClass) {
					$(element).addClass("is-invalid").removeClass("is-valid");
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).addClass("is-valid").removeClass("is-invalid");
				},
				submitHandler: function(form) {
					$(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
					$(".btn-submit").attr("disabled", "disabled");
					$(".btn-cancel").attr("disabled", "disabled");
					form.submit();
				}
			});
		})
	</script>

</body>

</html>