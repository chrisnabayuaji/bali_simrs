<script type="text/javascript">
  $(document).ready(function () {
    $("#form-data").validate( {
      rules: {
        nav_id:{
          remote: {
            url: '<?=site_url().'/'.$nav['nav_url']?>/ajax/cek_id/<?=@$main['nav_id']?>',
            type: 'post',
            data: {
              nav_id: function() {
                return $("#nav_id").val();
              }
            }
          }
        },
      },
      messages: {
        nav_id : {
          remote : 'Kode sudah digunakan!'
        }
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container"));
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    $('#parent_id').bind('change',function(e) {
      e.preventDefault();
      var i = $(this).val();
      $.get('<?=site_url("app/nav/ajax/get_nav_id")?>?parent_id='+i,null,function(data) {
        $('#nav_id').val(data.result);
      },'json');
    });
  
    if ($(".chosen-select").length) {
      $(".chosen-select").select2({
        width: '100%'
      });
      // addClass
      $('.filter-data').addClass('filter-data-chosen-select');
    }

    var act_nav_icon_type = '<?=@$main['nav_icon_type']?>';
    if (act_nav_icon_type == '2') {
      $("#type-img").addClass('d-none');
      $("#icon-color").removeClass('d-none');
      $("#type-font-icon").removeClass('d-none');
      $("#title-font-icon").html('Font Awesome 5');
      $("#label-font-icon").html('fas fa-');
    }else if (act_nav_icon_type == '3') {
      $("#type-img").addClass('d-none');
      $("#icon-color").removeClass('d-none');
      $("#type-font-icon").removeClass('d-none');
      $("#title-font-icon").html('Material Icon');
      $("#label-font-icon").html('mdi mdi-');
    }
	
    $("input[name$='nav_icon_type']").click(function() {
      var text = $(this).attr("data-text");
      $(".type-icon").addClass('d-none');
      $("#img-icon").val('');
      $("#font-icon").val('');
      $("#color-icon").val('');
      if (text == 'mdi') {
        $("#icon-color").removeClass('d-none');
        $("#type-font-icon").removeClass('d-none');
        $("#title-font-icon").html('Material Icon');
        $("#label-font-icon").html('mdi mdi-');
      }else if(text == 'fas') {
        $("#icon-color").removeClass('d-none');
        $("#type-font-icon").removeClass('d-none');
        $("#title-font-icon").html('Font Awesome 5');
        $("#label-font-icon").html('fas fa-');
      }else{
        $("#type-img").removeClass('d-none');
      }
    });
    
    var act_nav_top_menu_type = '<?=@$main['nav_top_menu_type']?>';
    if (act_nav_top_menu_type == 'L') {
      $("#type-label-sub-menu").removeClass('d-none');
      $("input[name='nav_sub_label']").prop('required',true);
    }
    
    $("input[name$='nav_top_menu_type']").click(function() {
      var text = $(this).val();
      $(".type-sub-menu").addClass('d-none');
      $("input[name='nav_sub_label']").val('');
      $("input[name='nav_sub_label']").removeAttr("required");
      if (text == 'L') {
        $("#type-label-sub-menu").removeClass('d-none');
        $("input[name='nav_sub_label']").prop('required',true);
      }
    });
    //delete photo
    $('.delete_photo').bind('click',function(e) {
      e.preventDefault();
      const i = $(this).attr('data-id');
      const f = $(this).attr('data-file');
      Swal.fire({
        title: 'Apakah Anda yakin menghapus gambar?',
        text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#eb3b5a',
        cancelButtonColor: '#b2bec3',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        customClass: 'swal-wide'
      }).then((result) => {
        if (result.value) {
          $.get('<?=site_url("app/nav/ajax/delete_photo")?>?nav_id='+i+'&nav_icon='+f,null,function(data) {
            if(data.callback == 'true') {
              $('#box_img').hide();
              $.toast({
                heading: 'Sukses',
                text: 'Gambar berhasil dihapus.',
                icon: 'success',
                position: 'top-right'
              })
            }else {
              $.toast({
                heading: 'Gagal',
                text: 'Gambar gagal dihapus.',
                icon: 'error',
                position: 'top-right'
              })
            }                    
          },'json');
        }
      })
    });
  })

</script>