<!-- js -->
<?php $this->load->view('app/nav/_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-lg-6">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Parent Navigation</label>
        <div class="col-lg-9 col-md-9">
          <select class="chosen-select custom-select w-100" name="parent_id" id="parent_id">
            <option value="">- None -</option>
            <?php foreach($list_nav as $ln):?>
            <option value="<?=$ln['nav_id']?>" <?php if(@$main['parent_id'] == $ln['nav_id']) echo 'selected'?>><?=$ln['nav_id']?> - <?=$ln['nav_nm']?></option>
            <?php endforeach;?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Nav Id <span class="text-danger">*<span></label>
        <div class="col-lg-4 col-md-9">
          <input type="text" class="form-control" name="nav_id" id="nav_id" value="<?=@$main['nav_id']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Navigation Name <span class="text-danger">*<span></label>
        <div class="col-lg-9 col-md-9">
          <input type="text" class="form-control" name="nav_nm" value="<?=@$main['nav_nm']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Description</label>
        <div class="col-lg-9 col-md-9">
          <input type="text" class="form-control" name="nav_desc" value="<?=@$main['nav_desc']?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">URL <span class="text-danger">*<span></label>
        <div class="col-lg-9 col-md-9">
          <input type="text" class="form-control" name="nav_url" value="<?=@$main['nav_url']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Tipe Icon</label>
        <div class="col-lg-3 col-md-3">
          <div class="form-check form-radio form-check-primary">
            <label class="form-check-label label-radio">
              <input type="radio" class="form-check-input" name="nav_icon_type" value="1" data-text="img" <?php if (@$main['nav_icon_type'] == '1') echo 'checked' ?> <?php if (@$main['nav_id'] == '') echo 'checked' ?>>
              Image
            <i class="input-helper"></i></label>
          </div>
        </div>
        <div class="col-lg-3 col-md-3">
          <div class="form-check form-radio form-check-primary">
            <label class="form-check-label label-radio">
              <input type="radio" class="form-check-input" value="2" data-text="fas" name="nav_icon_type" <?php if(@$main['nav_icon_type'] == '2') echo 'checked'?>>
              Font Awesome 5
            <i class="input-helper"></i></label>
          </div>
        </div>
        <div class="col-lg-3 col-md-3">
          <div class="form-check form-radio form-check-primary">
            <label class="form-check-label label-radio">
              <input type="radio" class="form-check-input" value="3" data-text="mdi" name="nav_icon_type" <?php if(@$main['nav_icon_type'] == '3') echo 'checked'?>>
              Material Icon
            <i class="input-helper"></i></label>
          </div>
        </div>
      </div>
      <div class="form-group row type-icon" id="type-img">
        <label class="col-lg-3 col-md-3 col-form-label">Image Icon</label>
        <div class="col-lg-4 col-md-9">
          <input type="file" name="nav_icon" class="form-control" id="img-icon">
          <?php if (@$main['nav_icon_type'] == '1' && @$main['nav_icon'] !=''): ?>
          <div id="box_img">
            <div class="text-danger small-text font-weight-semibold">* Upload gambar untuk mengubah</div>
            <div class="card mb-2 mt-1">
              <div class="card-body">
                <div class="text-center">
                  <img src="<?=base_url()?>assets/images/module/<?=@$main['nav_icon']?>" class="img-lg rounded">
                  <button type="button" class="btn btn-xs btn-danger mt-2 delete_photo" data-id="<?=@$main['nav_id']?>" data-file="<?=@$main['nav_icon']?>"><i class="far fa-trash-alt"></i> Hapus Gambar</button>
                </div>
              </div>
            </div>
          </div>
          <?php endif; ?>
        </div>
      </div>
      <div class="form-group row type-icon d-none" id="type-font-icon">
        <label class="col-lg-3 col-md-3 col-form-label" id="title-font-icon">Material Icon</label>
        <div class="col-lg-4 col-md-4">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text font-weight-normal normal-text" id="label-font-icon">mdi mdi-</span>
            </div>
            <input type="text" name="nav_font_icon" class="form-control" value="<?=clear_font_icon(@$main['nav_font_icon'])?>" id="font-icon">
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Tipe Top Sub Menu</label>
        <div class="col-lg-3 col-md-3">
          <div class="form-check form-radio form-check-primary">
            <label class="form-check-label label-radio">
              <input type="radio" class="form-check-input" name="nav_top_menu_type" value="S" <?php if (@$main['nav_top_menu_type'] == 'S') echo 'checked' ?> <?php if (@$main['nav_id'] == '') echo 'checked' ?>>
              Sub Menu Small
            <i class="input-helper"></i></label>
          </div>
        </div>
        <div class="col-lg-3 col-md-3">
          <div class="form-check form-radio form-check-primary">
            <label class="form-check-label label-radio">
              <input type="radio" class="form-check-input" value="L" name="nav_top_menu_type" <?php if (@$main['nav_top_menu_type'] == 'L') echo 'checked' ?>>
              Sub Menu Large
            <i class="input-helper"></i></label>
          </div>
        </div>
      </div>
      <div class="form-group row d-none type-sub-menu" id="type-label-sub-menu">
        <label class="col-lg-3 col-md-3 col-form-label">Label Sub Menu</label>
        <div class="col-lg-5 col-md-9">
          <input type="text" class="form-control" name="nav_sub_label" value="<?=@$main['nav_sub_label']?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Module Name</label>
        <div class="col-lg-5 col-md-9">
          <input type="text" class="form-control" name="nav_module" value="<?=@$main['nav_module']?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Modul ?</label>
        <div class="col-lg-4 col-md-9">
          <select class="chosen-select custom-select w-100" name="is_module">
            <option value="0" <?php if(@$main['is_module'] == '0') echo 'selected'?>>Tidak</option>
            <option value="1" <?php if(@$main['is_module'] == '1') echo 'selected'?>>Ya</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Kategori Link</label>
        <div class="col-lg-4 col-md-9">
          <select class="chosen-select custom-select w-100" name="nav_category">
            <option value="I" <?php if(@$main['nav_category'] == 'I') echo 'selected'?>>Internal</option>
            <option value="E" <?php if(@$main['nav_category'] == 'E') echo 'selected'?>>External</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Target Link</label>
        <div class="col-lg-4 col-md-9">
          <select class="chosen-select custom-select w-100" name="nav_target">
            <option value="S" <?php if(@$main['nav_target'] == 'S') echo 'selected'?>>Self</option>
            <option value="B" <?php if(@$main['nav_target'] == 'B') echo 'selected'?>>Blank</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Status</label>
        <div class="col-lg-4 col-md-9">
          <select class="chosen-select custom-select w-100" name="nav_st">
            <option value="1" <?php if(@$main['nav_st'] == '1') echo 'selected'?>>Aktif</option>
            <option value="0" <?php if(@$main['nav_st'] == '0') echo 'selected'?>>Tidak Aktif</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="col-2 offset-md-10 ">
        <div class="float-right">
          <button type="button" class="btn btn-xs btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
          <button type="submit" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>