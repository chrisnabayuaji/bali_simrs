<!-- js -->
<?php $this->load->view('app/parameter/_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <input type="hidden" name="parameter_id" value="<?=@$main['parameter_id']?>">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Group <span class="text-danger">*<span></label>
        <div class="col-lg-6 col-md-5">
          <input type="text" class="form-control" name="parameter_group" value="<?=@$main['parameter_group']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Nama <span class="text-danger">*<span></label>
        <div class="col-lg-6 col-md-5">
          <input type="text" class="form-control" name="parameter_nm" value="<?=@$main['parameter_nm']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Field <span class="text-danger">*<span></label>
        <div class="col-lg-6 col-md-5">
          <input type="text" class="form-control" name="parameter_field" value="<?=@$main['parameter_field']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Nilai <span class="text-danger">*<span></label>
        <div class="col-lg-9 col-md-5">
          <input type="text" class="form-control" name="parameter_val" value="<?=@$main['parameter_val']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Kode <span class="text-danger">*<span></label>
        <div class="col-lg-5 col-md-5">
          <input type="text" class="form-control" name="parameter_cd" value="<?=@$main['parameter_cd']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Locked</label>
        <div class="col-lg-5 col-md-9">
          <select class="chosen-select custom-select w-100" name="is_lock_edited">
            <option value="0" <?php if(@$main['is_lock_edited'] == '0') echo 'selected'?>>Tidak</option>
            <option value="1" <?php if(@$main['is_lock_edited'] == '1') echo 'selected'?>>Ya</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>