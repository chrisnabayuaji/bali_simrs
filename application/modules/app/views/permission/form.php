<!-- js -->
<?php $this->load->view('app/permission/_js')?>
<!-- / -->

<form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?=$form_action?>" class="needs-validation" novalidate>
<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline col-title">
        <div class="col-back">
          <a href="<?=site_url($nav['nav_module'].'/dashboard')?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
        </div>
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          Konfigurasi <?=$nav['nav_nm']?>
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?=site_url('app/dashboard')?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url($module_nav['nav_url'])?>"><i class="fas fa-folder-open"></i> <?=$module_nav['nav_nm']?></a></li>
          <li class="breadcrumb-item"><a href="#"><i class="fas fa-cog"></i> Konfigurasi</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url($nav['nav_url'])?>"><?=$nav['nav_nm']?></a></li>
          <li class="breadcrumb-item active"><span>Form</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>

  <div class="row full-page mt-4">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card border-none">
        <div class="card-body card-shadow">
          <div class="row">

            <div class="col-lg-8">
              <div class="form-group row">
                <label class="col-lg-2 col-md-3 col-form-label">Role Name</label>
                <div class="col-lg-4 col-md-5">
                  <input type="text" class="form-control" name="" value="<?=@$main['role_nm']?>" disabled>
                </div>
              </div>
              <div class="form-group mt-2 mb-3">
                <div class="table-responsive">
                  <table class="table table-hover table-striped table-bordered table-fixed">
                    <thead>
                      <tr>
                        <th class="text-center" width="38">&nbsp;</th>
                        <th class="text-center" width="38">No</th>
                        <th class="text-center" width="300">Navigation</th>
                        <th class="text-center" width="80">VIEW</th>
                        <th class="text-center" width="80">ADD</th>
                        <th class="text-center" width="80">UPDATE</th>
                        <th class="text-center" width="80">DELETE</th>
                      </tr>
                    </thead>
                    <tbody style="height: 51vh;">
                      <?php foreach($list_role_nav as $row):?>
                      <tr>
                        <td align="center" width="38">
                          <div class="form-check w-20px form-check-primary ml-1 pt-2">
                            <label class="form-check-label">
                              <input type="checkbox" class="form-check-input cb_all" name="cb_all[<?=point_to_under($row['nav_id'])?>]" data-id="<?=point_to_under($row['nav_id'])?>" <?php if($row['cb_all'] == 'yes') echo 'checked'?>>
                            </label>
                          </div>
                          <input type="hidden" name="nav_id[]" value="<?=point_to_under($row['nav_id'])?>">
                          <input type="hidden" name="set_nav_id[]" value="<?=$row['set_nav_id']?>">
                          <input type="hidden" name="set_role_id[]" value="<?=$row['set_role_id']?>">
                        </td>
                        <td class="text-center" width="38"><?=$row['no']?></td>
                        <td width="300"><?=$row['nav_id']?> - <?=$row['nav_nm']?></td>
                        <td align="center" width="80">
                          <div class="form-check w-20px form-check-primary ml-1 pt-2">
                            <label class="form-check-label">
                              <input type="checkbox" class="form-check-input cb_detail_<?=point_to_under($row['nav_id'])?>" name="cb_view[<?=point_to_under($row['nav_id'])?>]" value="1" <?php if($row['_view'] == '1') echo 'checked';?>>
                            </label>
                          </div>
                        </td>
                        <td align="center" width="80">
                          <div class="form-check w-20px form-check-primary ml-1 pt-2">
                            <label class="form-check-label">
                              <input type="checkbox" class="form-check-input cb_detail_<?=point_to_under($row['nav_id'])?>" name="cb_add[<?=point_to_under($row['nav_id'])?>]" value="1" <?php if($row['_add'] == '1') echo 'checked';?>>
                            </label>
                          </div>
                        </td>
                        <td align="center" width="80">
                          <div class="form-check w-20px form-check-primary ml-1 pt-2">
                            <label class="form-check-label">
                              <input type="checkbox" class="form-check-input cb_detail_<?=point_to_under($row['nav_id'])?>" name="cb_update[<?=point_to_under($row['nav_id'])?>]" value="1" <?php if($row['_update'] == '1') echo 'checked';?>>
                            </label>
                          </div>
                        </td>
                        <td align="center" width="80">
                          <div class="form-check w-20px form-check-primary ml-1 pt-2">
                            <label class="form-check-label">
                              <input type="checkbox" class="form-check-input cb_detail_<?=point_to_under($row['nav_id'])?>" name="cb_delete[<?=point_to_under($row['nav_id'])?>]" value="1" <?php if($row['_delete'] == '1') echo 'checked';?>>
                            </label>
                          </div>
                        </td>
                      </tr>                                              
                      <?php endforeach;?>
                  </tbody>
                  </table>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="btn-form">
  <div class="btn-form-action">
    <div class="btn-form-action-bottom w-100 small-text clearfix">
      <div class="col-lg-10 col-md-8 col-4">
        <a href="<?=site_url('app/permission')?>" class="btn btn-xs btn-secondary"><i class="mdi mdi-keyboard-backspace"></i> Kembali</a>
      </div>
      <div class="col-lg-1 col-md-2 col-4 btn-form-clear">
        <button type="reset" onClick="window.location.reload();" class="btn btn-xs btn-primary"><i class="mdi mdi-refresh"></i> Clear</button>
      </div>
      <div class="col-lg-1 col-md-2 col-4 btn-form-save">
        <button type="submit" class="btn btn-xs btn-primary"><i class="mdi mdi-file-check"></i> Simpan</button>
      </div>
    </div>
  </div>
</div>
</form>