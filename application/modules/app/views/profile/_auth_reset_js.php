<script type="text/javascript">
  var auth_reset;
  $(document).ready(function () {
    var auth_reset = $("#auth_reset").validate( {
      rules: {
        pass_reset: { valueNotEquals: "" }
      },
      messages: {
        pass_reset: { valueNotEquals: "Password masih kosong!" }
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('select2')){
          error.insertAfter(element.next(".select2-container"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container"));
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $.ajax({
          type : 'post',
          url : '<?=site_url($nav['nav_url'])?>/ajax/auth_reset',
          data : $(form).serialize(),
          success : function (data) {
            $("#btn-reset").html('<i class="fas fa-spin fa-spinner"></i> Proses');
            $("#btn-reset").attr("disabled", "disabled");
            $("#btn-cancel-reset").attr("disabled", "disabled");
            
            if (data == 1) {
              $("#messages").removeClass("d-none");
              $("#reset-loading").removeClass("d-none");
              $("#reset-failed").addClass("d-none");
              $("#reset-success").addClass("d-none");

              $.post('<?=site_url().'/'.$nav['nav_url'].'/reset'?>', function (data_reset) {
                $("#reset-success").removeClass("d-none");
                $("#reset-loading").addClass("d-none");
                $("#reset-failed").addClass("d-none");

                $("#btn-reset").html('<i class="fas fa-save"></i> Simpan');
                $("#btn-reset").attr("disabled", false);
                $("#btn-cancel-reset").attr("disabled", false);

                setTimeout(function() { 
                  $('#myModal').modal('toggle');
                }, 1500);
              }, 'json');
            }else{
              $("#messages").removeClass("d-none");
              $("#reset-failed").removeClass("d-none");
              $("#reset-loading").addClass("d-none");
              $("#reset-success").addClass("d-none");

              $("#btn-reset").html('<i class="fas fa-save"></i> Simpan');
              $("#btn-reset").attr("disabled", false);
              $("#btn-cancel-reset").attr("disabled", false);
            }
          }
        })
        return false;
      }
    });
  })
</script>