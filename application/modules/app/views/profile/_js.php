<script type="text/javascript">
  $(document).ready(function () {
  	$("#form-data").validate( {
	    rules: {
	      
	    },
	    messages: {
	      
	    },
	    errorElement: "em",
	    errorPlacement: function (error,element) {
	      error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container"));
        }else{
          error.insertAfter(element);
        }
	    },
	    highlight: function (element,errorClass,validClass) {
	      $(element).addClass("is-invalid").removeClass("is-valid");
	    },
	    unhighlight: function (element, errorClass, validClass) {
	      $(element).addClass("is-valid").removeClass("is-invalid");
	    },
	    submitHandler: function (form) {
	      $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
	      $(".btn-submit").attr("disabled", "disabled");
	      $(".btn-cancel").attr("disabled", "disabled");
	      form.submit();
	    }
	  });

	  $('#reset-data').on('click', function (e) {
	    e.preventDefault();

	    const href = $(this).attr('href');

	    Swal.fire({
	      title: 'Apakah Anda yakin?',
	      text: "Aksi ini tidak bisa dikembalikan. Data akan dihapus semua.",
	      type: 'warning',
	      showCancelButton: true,
	      confirmButtonColor: '#eb3b5a',
	      cancelButtonColor: '#b2bec3',
	      confirmButtonText: 'Reset',
	      cancelButtonText: 'Batal',
	      customClass: 'swal-wide'
	    }).then((result) => {
	      if (result.value) {
	        document.location.href = href;
	      }
	    })
	  })

	  $('.migration').on('click', function (e) {
	    e.preventDefault();

	    const href = $(this).attr('href');;

	    Swal.fire({
	      title: 'Apakah Anda yakin?',
	      text: "Aksi ini tidak bisa dikembalikan. Data akan dimigrasi ke database baru.",
	      type: 'warning',
	      showCancelButton: true,
	      confirmButtonColor: '#eb3b5a',
	      cancelButtonColor: '#b2bec3',
	      confirmButtonText: 'Migrasi',
	      cancelButtonText: 'Batal',
	      customClass: 'swal-wide'
	    }).then((result) => {
	      if (result.value) {
	        document.location.href = href;
	      }
	    })
	  })
  })
  function save(type, val) {
    window.location.href = "<?=site_url().'/'.$nav['nav_url'].'/save/'?>"+type+"/"+val;
  }
</script>