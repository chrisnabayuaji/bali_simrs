<!-- js -->
<?php $this->load->view('app/profile/_auth_reset_js')?>
<!-- / -->
<form id="auth_reset" action="" method="post" autocomplete="off">
  <div class="row">
    <div class="col-lg-12">
      <div class="form-group row">
        <div class="col-lg-12 col-md-12 mb-1">Password Reset Data <span class="text-danger">*<span></div>
        <div class="col-lg-12 col-md-12">
          <input type="text" class="form-control" name="pass_reset" id="pass_reset">
        </div>
        <div class="col-lg-12 col-md-12 mt-2 d-none" id="messages">
          <div class="text-danger text-small d-none" id="reset-failed">Password yang Anda masukkan Salah !</div>
          <div class="text-small d-none" style="color: #28a745;" id="reset-loading">
            Password yang Anda masukkan Benar. <br>
            Sedang melakukan Reset/Penghapusan data <br> 
            Silahkan Tunggu <i class="fas fa-spin fa-spinner"></i>
          </div>
          <div class="text-small" style="color: #28a745;" id="reset-success">Data berhasil di Reset <i class="fas fa-check"></i></div>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-6 offset-md-6">
        <button type="button" class="btn btn-xs btn-secondary" id="btn-cancel-reset" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-danger" id="btn-reset"><i class="fas fa-sync-alt"></i> Reset</button>
      </div>
    </div>
  </div>
</form>