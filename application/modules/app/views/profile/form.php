<!-- js -->
<?php $this->load->view('app/profile/_js')?>
<!-- / -->

<form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?=$form_action?>" class="needs-validation" novalidate>
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline col-title">
          <div class="col-back">
            <a href="<?=site_url($nav['nav_module'].'/dashboard')?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
          </div>
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            Konfigurasi <?=$nav['nav_nm']?>
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?=site_url('app/dashboard')?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?=site_url($module_nav['nav_url'])?>"><i class="fas fa-folder-open"></i> <?=$module_nav['nav_nm']?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-cog"></i> Konfigurasi</a></li>
            <li class="breadcrumb-item"><a href="<?=site_url($nav['nav_url'])?>"><?=$nav['nav_nm']?></a></li>
            <li class="breadcrumb-item active"><span>Form</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>

    <div class="row full-page mt-4">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <div class="row mt-1">
              <div class="col-md-6">
                <h4 class="card-title border-bottom border-2 pb-2 mb-3">Konfigurasi Aplikasi</h4>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Nama Aplikasi <span class="text-danger">*<span></label>
                  <div class="col-lg-8 col-md-8">
                    <input type="text" class="form-control" name="app_title" value="<?=@$main['app_title']?>" required="">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Singkatan <span class="text-danger">*<span></label>
                  <div class="col-lg-5 col-md-5">
                    <input type="text" class="form-control" name="app_shorttitle" value="<?=@$main['app_shorttitle']?>" required="">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Copyright <span class="text-danger">*<span></label>
                  <div class="col-lg-7 col-md-7">
                    <input type="text" class="form-control" name="copyright" value="<?=@$main['copyright']?>" required="">
                  </div>
                </div>
                <h4 class="card-title border-bottom border-2 pb-2 mb-3">Konfigurasi Login</h4>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Title Logo <span class="text-danger">*<span></label>
                  <div class="col-lg-7 col-md-7">
                    <input type="text" class="form-control" name="title_logo_login" value="<?=@$main['title_logo_login']?>" required="">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Sub Title Logo <span class="text-danger">*<span></label>
                  <div class="col-lg-7 col-md-7">
                    <input type="text" class="form-control" name="sub_title_logo_login" value="<?=@$main['sub_title_logo_login']?>" required="">
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <h4 class="card-title border-bottom border-2 pb-2 mb-3">Konfigurasi Tema</h4>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label" style="margin-top: -7px;">Primary Color</label>
                  <div class="col-lg-9 col-md-9">
                    <div class="d-flex flex-wrap mb-3">
                      <button type="button" onclick="save('primary_color','primary-green')" class="btn" style="background-color: #33b1a2; width: 50px; height: 35px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;">
                        <?php if(@$main['primary_color'] == "primary-green"):?>
                          <i class="fas fa-check" style="position: absolute; margin-top: -7px; margin-left: -6px; color: #fff;"></i>
                        <?php endif;?>
                      </button>
                      <button type="button" onclick="save('primary_color','primary-blue')" class="btn" style="background-color: #3498db; width: 50px; height: 35px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;">
                        <?php if(@$main['primary_color'] == "primary-blue"):?>
                          <i class="fas fa-check" style="position: absolute; margin-top: -7px; margin-left: -6px; color: #fff;"></i>
                        <?php endif;?>
                      </button>
                      <button type="button" onclick="save('primary_color','primary-red')" class="btn" style="background-color: #ff6b6b; width: 50px; height: 35px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;">
                        <?php if(@$main['primary_color'] == "primary-red"):?>
                          <i class="fas fa-check" style="position: absolute; margin-top: -7px; margin-left: -6px; color: #fff;"></i>
                        <?php endif;?>
                      </button>
                      <button type="button" onclick="save('primary_color','primary-orange')" class="btn" style="background-color: #f39c12; width: 50px; height: 35px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;">
                        <?php if(@$main['primary_color'] == "primary-orange"):?>
                          <i class="fas fa-check" style="position: absolute; margin-top: -7px; margin-left: -6px; color: #fff;"></i>
                        <?php endif;?>
                      </button>
                    </div>
                    <!-- <a class="btn btn-xs btn-danger float-right" id="reset-data" href="<?=site_url($nav['nav_url'].'/reset')?>"><i class="fas fa-sync-alt"></i> Reset Data</a> -->
                    <a href="#" data-href="<?=site_url().'/'.$nav['nav_url'].'/auth_reset'?>" modal-title="Autentikasi Reset Data" modal-size="sm" class="btn btn-xs btn-danger float-right modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-danger" title="" data-original-title="Reset Data"><i class="fas fa-sync-alt"></i> Reset Data</a>
                  </div>
                </div>
                <h4 class="card-title border-bottom border-2 pb-2 mb-3">Migrasi data RS Permata</h4>
                <a class="btn btn-xs btn-primary btn-migration migration" href="javascript:void(0)" data-href="<?=site_url($nav['nav_url'].'/migration/pasien')?>"><i class="fas fa-users"></i> Data Pasien</a>
                <a class="btn btn-xs btn-primary btn-migration migration" href="javascript:void(0)" data-href="<?=site_url($nav['nav_url'].'/migration/pegawai')?>"><i class="fas fa-users"></i> Data Pegawai</a>
                <a class="btn btn-xs btn-primary btn-migration migration" href="javascript:void(0)" data-href="<?=site_url($nav['nav_url'].'/migration/obat')?>"><i class="fas fa-capsules"></i> Data Obat</a>
                <a class="btn btn-xs btn-primary btn-migration migration" href="javascript:void(0)" data-href="<?=site_url($nav['nav_url'].'/migration/tarif')?>"><i class="fas fa-money-bill"></i> Data Tarif</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="btn-form">
    <div class="btn-form-action">
      <div class="btn-form-action-bottom w-100 small-text clearfix">
        <div class="col-lg-10 col-md-8 col-4">
        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-clear">
          <button type="reset" onClick="window.location.reload();" class="btn btn-xs btn-primary"><i class="mdi mdi-refresh"></i> Clear</button>
        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-save">
          <button type="submit" class="btn btn-xs btn-primary"><i class="mdi mdi-file-check"></i> Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>
