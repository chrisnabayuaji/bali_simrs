<!-- js -->
<?php $this->load->view('app/role/_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <input type="hidden" name="role_id" value="<?=@$main['role_id']?>">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Nama <span class="text-danger">*<span></label>
        <div class="col-lg-6 col-md-5">
          <input type="text" class="form-control" name="role_nm" value="<?=@$main['role_nm']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Description <span class="text-danger">*<span></label>
        <div class="col-lg-5 col-md-5">
          <input type="text" class="form-control" name="role_desc" value="<?=@$main['role_desc']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Status</label>
        <div class="col-lg-5 col-md-9">
          <select class="chosen-select custom-select w-100" name="is_active">
            <option value="1" <?php if(@$main['is_active'] == '1') echo 'selected'?>>Ya</option>
            <option value="0" <?php if(@$main['is_active'] == '0') echo 'selected'?>>Tidak</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>