<div class="content-wrapper mw-100">
  <div class="d-flex justify-content-center col-title-dashboard">
    <p class="ml-md-3 font-weight-semibold mb-0 mt-1 text-primary text-center">SELAMAT DATANG DI MODUL ADMIN SISTEM</p>
  </div>
  <div class="row d-flex justify-content-center">
    <div class="col-md-6 col-xl-6 pb-2">
      <form>
        <div class="form-group">
          <select name="ses_user_st" class="select-search-menu-module w-100" data-text="Modul Admin Sistem">
            <option value="">- Pilih Menu -</option>
            <optgroup label="Registrasi">
              <option value="volvo">Volvo</option>
              <option value="saab">Saab</option>
            </optgroup>
            <optgroup label="Tindakan">
              <option value="mercedes">Mercedes</option>
              <option value="audi">Audi</option>
            </optgroup>
          </select>
        </div>
      </form>
    </div>
    <div class="col-md-12 col-xl-12 grid-margin stretch-card">
      <div class="card card-shadow">
          <ul class="nav nav-tabs" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="item-tab-1" data-toggle="tab" href="#tab-1" role="tab" aria-controls="tab-1" aria-selected="true"> Data Dasar RS</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="item-tab-2" data-toggle="tab" href="#tab-2" role="tab" aria-controls="tab-2" aria-selected="false"><img src="<?=base_url()?>assets/images/menu/tindakan.png"> Tindakan</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="item-tab-3" data-toggle="tab" href="#tab-3" role="tab" aria-controls="tab-3" aria-selected="false"><img src="<?=base_url()?>assets/images/menu/manajemen.png"> Manajemen</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="item-tab-4" data-toggle="tab" href="#tab-4" role="tab" aria-controls="tab-4" aria-selected="false"><img src="<?=base_url()?>assets/images/menu/inventori-obat.png"> Inventori Obat</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="item-tab-5" data-toggle="tab" href="#tab-5" role="tab" aria-controls="tab-5" aria-selected="false"><img src="<?=base_url()?>assets/images/menu/inventori-non-medis.png"> Inventori Barang Non Medis</a>
            </li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane fade show active" id="tab-1" role="tabpanel" aria-labelledby="item-tab-1">
              <div class="tab-pane-body d-flex justify-content-center">
                <?php for ($i=0; $i < 8 ; $i++): ?>
                <div class="col-menu-icon">
                  <div class="card card-menu-icon card-shadow">
                    <div class="card-body text-center p-1">
                      <img class="img-menu-icon" src="<?=base_url()?>assets/images/menu/informasi-kamar.png">
                      <div class="title-menu-icon">Informasi Kamar</div>
                    </div>
                  </div>
                </div>
                <div class="col-menu-icon">
                  <div class="card card-menu-icon card-shadow">
                    <div class="card-body text-center p-1">
                      <img class="img-menu-icon" src="<?=base_url()?>assets/images/menu/jadwal-praktek.png">
                      <div class="title-menu-icon">Jadwal Prakter</div>
                    </div>
                  </div>
                </div>
                <div class="col-menu-icon">
                  <div class="card card-menu-icon card-shadow">
                    <div class="card-body text-center p-1">
                      <img class="img-menu-icon" src="<?=base_url()?>assets/images/menu/register.png">
                      <div class="title-menu-icon">Registrasi</div>
                    </div>
                  </div>
                </div>
                <div class="col-menu-icon">
                  <div class="card card-menu-icon card-shadow">
                    <div class="card-body text-center p-1">
                      <img class="img-menu-icon" src="<?=base_url()?>assets/images/menu/booking-registrasi.png">
                      <div class="title-menu-icon">Booking Registrasi</div>
                    </div>
                  </div>
                </div>
                <div class="col-menu-icon">
                  <div class="card card-menu-icon card-shadow">
                    <div class="card-body text-center p-1">
                      <img class="img-menu-icon" src="<?=base_url()?>assets/images/menu/igd-ugd.png">
                      <div class="title-menu-icon">IGD/UGD</div>
                    </div>
                  </div>
                </div>
                <?php endfor; ?>
              </div>
            </div>
            <div class="tab-pane fade" id="tab-2" role="tabpanel" aria-labelledby="item-tab-2">
              <div>2</div>
            </div>
            <div class="tab-pane fade" id="tab-3" role="tabpanel" aria-labelledby="item-tab-3">
              <div>3</div>
            </div>
            <div class="tab-pane fade" id="tab-4" role="tabpanel" aria-labelledby="item-tab-4">
              <div>4</div>
            </div>
            <div class="tab-pane fade" id="tab-5" role="tabpanel" aria-labelledby="item-tab-5">
              <div>5</div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>