<div class="content-wrapper mw-100">
  <div class="d-flex justify-content-center col-title-dashboard">
    <p class="ml-md-3 font-weight-semibold mb-0 mt-1 text-primary text-center">SILAHKAN PILIH MENU ATAU MODULE DI BAWAH INI</p>
  </div>
  <div class="row d-flex justify-content-center">
    <div class="col-md-6 col-xl-6 pb-2">
      <form>
        <div class="form-group">
          <select name="ses_user_st" id="menu_module" class="select-search-menu w-100">
            <option value="">- Pilih Menu -</option>
            <?php foreach ($list_nav as $nav): ?>
              <?php if ($nav['parent_id'] == 0): ?>
                <option value="<?=site_url($nav['nav_url'])?>"><?=$nav['nav_nm']?></option>
              <?php elseif ($nav['parent_id'] != 0 && $nav['nav_url'] == '#'): ?>
                <optgroup label="&nbsp;&nbsp;&nbsp; <?=$nav['nav_nm']?>"></optgroup>
              <?php else: ?>
                <option value="<?=site_url($nav['nav_url'])?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?=$nav['nav_nm']?></option>
              <?php endif; ?>
            <?php endforeach; ?>
          </select>
        </div>
      </form>
    </div>
    <div class="col-md-12 col-xl-12 grid-margin stretch-card">
      <div class="card card-shadow">
        <div class="tab-content">
          <div class="tab-pane fade show active">
            <div class="tab-pane-body d-flex justify-content-center">
              <?php foreach ($list_nav_module as $nav): ?>
              <div class="col-menu-icon col-menu-icon-dashboard">
                <div onclick="window.location.href='<?=site_url($nav['nav_url'])?>'" class="card card-menu-icon-dashboard card-shadow">
                  <div class="card-body text-center p-1">
                    <img class="img-menu-icon img-menu-icon-dashboard lazy-load" data-src="<?=base_url()?>assets/images/module/<?=$nav['nav_icon']?>">
                    <div class="title-menu-icon title-menu-icon-dashboard"><?=$nav['nav_nm']?></div>
                  </div>
                </div>
              </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function () {
    $('#menu_module').on('change', function() {
      var link = $(this).val();
      window.location = link;
    })
  })
</script>