        </div>
        </div>
        </div>
        <footer class="footer">
          <!-- For Desktop -->
          <div class="w-100 clearfix col-footer">
            <div class="col-lg-6 col-sm-6 d-xs-none"><?= to_date_indo(date('Y-m-d H:i:s')) ?> | User : <?= @$profile['user_realname'] ?> - <?= @$profile['role_nm'] ?></div>
            <div class="col-lg-2 col-md-6 d-tablet-none d-xs-none"></div>
            <div class="col-lg-4 col-sm-6 text-lg-right text-md-right text-xs-center"><?= $config['copyright'] ?> | Page rendered in <span class="text-danger font-weight-semibold">{elapsed_time}</span> second.</div>
          </div>
        </footer>

        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog" aria-hidden="true">
          <div id="modal-size" class="modal-dialog" role="document">
            <div class="modal-content modal-content-top">
              <div class="modal-header" id="modal-header">
                <div class="modal-title" id="modal-title"></div>
                <button type="button" class="close btn-no-border" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body" id="modal-body">
              </div>
            </div>
          </div>
        </div>
        <!-- End Modal -->

        <!-- Modal -->
        <div id="myModal-2" class="modal fade" role="dialog" aria-hidden="true">
          <div id="modal-size-2" class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header" id="modal-header-2">
                <div class="modal-title" id="modal-title-2"></div>
                <button type="button" class="close btn-no-border" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body" id="modal-body-2">
              </div>
            </div>
          </div>
        </div>
        <!-- End Modal -->

        <!-- Modal -->
        <div id="myModal-3" class="modal fade" role="dialog" aria-hidden="true">
          <div id="modal-size-3" class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header" id="modal-header-3">
                <div class="modal-title" id="modal-title-3"></div>
                <button type="button" class="close btn-no-border" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body" id="modal-body-3">
              </div>
            </div>
          </div>
        </div>
        <!-- End Modal -->

        <!-- Modal -->
        <div class="modal fade" id="loadingModal" tabindex="-1" role="dialog" aria-labelledby="loadingModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered" style="margin: 30px auto !important; max-width: 150px;" role="document">
            <div class="modal-content">
              <div class="modal-body">
                <div class="text-center">
                  <div class="mt-3">
                    <i class="fas fa-spin fa-spinner fa-3x"></i>
                  </div>
                  <div class="mt-3 h6">Proses Data...</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- End Modal -->

        <script src="<?= base_url() ?>assets/plugins/lazy-load/lazy-load.js"></script>
        <script>
          // lazy load image
          (function() {
            var ll = new LazyLoad({
              elements_selector: ".lazy-load",
            });
          })();
        </script>

        </body>

        </html>