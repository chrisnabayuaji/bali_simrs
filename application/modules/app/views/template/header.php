<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title><?= $config['app_shorttitle'] ?></title>
  <link rel="shortcut icon" href="<?= base_url() ?>assets/images/icon/<?= @$identitas['logo_rumah_sakit'] ?>" type="image/x-icon">
  <!-- stylesheet -->
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/theme-color/<?= $config['primary_color'] ?>.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/root.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/bootstrap/bootstrap.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/mixins/mixins.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/core/core.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/components/components.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/overrides/overrides.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/auth/auth.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/menu.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/custom.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/responsive.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/material-icon/material-icon.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/select2/select2.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/sweetalert2/sweetalert2.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/jquery-toast/jquery-toast.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/font-awesome/font-awesome.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/DataTables/datatables.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/DataTables/DataTables-1.10.20/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/Chart.js/Chart.min.css">

  <!-- javascript -->
  <script>
    var base_url = '<?= @base_url() ?>';
    var site_url = '<?= @site_url() ?>';
    var nav_url = '<?= @site_url() . '/' . @$nav['nav_url'] ?>';
  </script>
  <script src="<?= base_url() ?>assets/js/jquery.js"></script>
  <script src="<?= base_url() ?>assets/js/bootstrap.js"></script>
  <script src="<?= base_url() ?>assets/js/jquery.lazy.min.js"></script>
  <script src="<?= base_url() ?>assets/plugins/jquery-validation/jquery.validate.min.js"></script>
  <script src="<?= base_url() ?>assets/plugins/jquery-validation/additional-methods.js"></script>
  <script src="<?= base_url() ?>assets/plugins/jquery-validation/localization/messages_id.js"></script>
  <script src="<?= base_url() ?>assets/plugins/select2/select2.js"></script>
  <script src="<?= base_url() ?>assets/plugins/sweetalert2/sweetalert2.min.js"></script>
  <script src="<?= base_url() ?>assets/plugins/jquery-toast/jquery.toast.min.js"></script>
  <script src="<?= base_url() ?>assets/plugins/daterangepicker/moment.min.js"></script>
  <script src="<?= base_url() ?>assets/plugins/daterangepicker/daterangepicker.js"></script>
  <script src="<?= base_url() ?>assets/plugins/autonumeric/autonumeric.js"></script>
  <!-- <script src="<?= base_url() ?>assets/plugins/auto-numeric/AutoNumeric.js"></script> -->
  <script src="<?= base_url() ?>assets/plugins/overlayScrollbars/js/OverlayScrollbars.min.js"></script>
  <script src="<?= base_url() ?>assets/plugins/DataTables/datatables.min.js"></script>
  <script src="<?= base_url() ?>assets/plugins/DataTables/DataTables-1.10.20/js/dataTables.bootstrap4.min.js"></script>
  <script src="<?= base_url() ?>assets/js/autocomplete.js"></script>
  <script src="<?= base_url() ?>assets/js/template.js"></script>
  <script src="<?= base_url() ?>assets/js/custom.js"></script>
  <script src="<?= base_url() ?>assets/js/rpn_helper.js"></script>
  <script src="<?= base_url() ?>assets/plugins/Chart.js/Chart.min.js"></script>
</head>

<body>

  <div class="container-scroller">
    <!-- partial:partials/_horizontal-navbar.html -->
    <div class="horizontal-menu">
      <nav class="navbar top-navbar col-lg-12 col-12 p-0 mt-n3 mb-n3">
        <div class="container mw-100">
          <div class="navbar-menu-wrapper navbar-menu-left d-flex align-items-center col-lg-4">
            <img src="<?= base_url() ?>assets/images/icon/<?= @$identitas['logo_rumah_sakit'] ?>" style="width: 22px;" class="rounded-circle">
            <div class="ml-1"><?= $config['app_title'] ?></div>
          </div>
          <div class="navbar-menu-wrapper navbar-menu-center d-flex align-items-center justify-content-center">
            <?= @$identitas['rumah_sakit'] ?>
          </div>
          <div class="navbar-menu-wrapper navbar-menu-right d-flex align-items-center justify-content-end">
            <ul class="navbar-nav navbar-nav-right">
              <li class="nav-item d-flex nav-profile dropdown">
                <a class="nav-link dropdown-toggle pl-1 pr-1 button-user text-short" href="#" data-toggle="dropdown" id="profileDropdown" style="border-radius: 3px; padding-top: 2px; padding-bottom: 2px;">
                  <span class="nav-status-indicator"></span>
                  <img class="rounded-circle lazy-load" data-src="<?= base_url() ?>assets/images/user/<?= (@$profile['user_photo'] != '') ? @$profile['user_photo'] : 'no-image.png' ?>">
                  <span class="nav-profile-name"><?= @$profile['user_realname'] ?></span>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="profileDropdown">
                  <p class="mb-0 font-weight-normal text-center font-weight-semibold dropdown-header border-bottom">ANDA LOGIN SEBAGAI</p>
                  <a class="dropdown-item preview-item pt-2 pb-2">
                    <div class="preview-thumbnail">
                      <img class="rounded-circle lazy-load" style="width: 36px; height: 36px;" data-src="<?= base_url() ?>assets/images/user/<?= (@$profile['user_photo'] != '') ? @$profile['user_photo'] : 'no-image.png' ?>">
                    </div>
                    <div class="preview-item-content">
                      <div class="preview-subject font-weight-normal"><?= @$profile['user_realname'] ?></div>
                      <p class="font-weight-light small-text mb-0 text-muted"><?= @$profile['role_nm'] ?></p>
                    </div>
                  </a>
                  <a href="#" data-href="<?= site_url() . '/app/form_modal' ?>" modal-title="Pengaturan User" modal-size="md" data-url="<?= site_url() . '/' . $this->uri->uri_string() ?>" class="dropdown-item preview-item pt-2 pb-2 modal-config-user">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-warning">
                        <i class="mdi mdi-settings mx-0"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <div class="preview-subject font-weight-normal">Settings</div>
                      <p class="font-weight-light small-text mb-0 text-muted">
                        Pengaturan User
                      </p>
                    </div>
                  </a>
                  <a href="<?= site_url() . '/app/auth/logout_action' ?>" class="dropdown-item preview-item pt-2 pb-2">
                    <div class="preview-thumbnail">
                      <div class="preview-icon bg-danger">
                        <i class="mdi mdi-power mx-0"></i>
                      </div>
                    </div>
                    <div class="preview-item-content">
                      <div class="preview-subject font-weight-normal">Log out</div>
                    </div>
                  </a>
                </div>
              </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="horizontal-menu-toggle">
              <span class="mdi mdi-menu"></span>
            </button>
          </div>
        </div>
      </nav>