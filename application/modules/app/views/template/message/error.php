<!DOCTYPE html>
<html lang="en">  
<head>
    <meta charset="utf-8">
    <title><?=$config['app_title']?> - <?=$config['app_subtitle']?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Bootstrap core CSS -->
    <link href="<?=base_url()?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Font Awesome -->
    <link href="<?=base_url()?>assets/css/font-awesome.min.css" rel="stylesheet">

    <!-- Perfect -->
    <link href="<?=base_url()?>assets/css/app.min.css" rel="stylesheet">
    <link href="<?=base_url()?>assets/css/app-skin.css" rel="stylesheet">
    
  </head>

  <body>
    <div id="wrapper">
        <div class="padding-md" style="margin-top:50px;">
            <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 text-center">
                    <div class="h5"><?=$config['app_title']?><br><b><?=$config['app_subtitle']?></b></div>
                    <h4 class="m-top-none error-heading">Error!</h4>
                    
                    <h4><?=$this->session->flashdata('auth_message')?></h4>
                    <br>
                    <a class="btn btn-success m-bottom-sm" href="<?=site_url('app/dashboard')?>"><i class="fa fa-home"></i> Back to Dashboard</a>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.padding-md -->
    </div><!-- /wrapper -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    
    <!-- Jquery -->
    <script src="<?=base_url()?>assets/js/jquery-1.10.2.min.js"></script>
    
    <!-- Bootstrap -->
    <script src="<?=base_url()?>assets/bootstrap/js/bootstrap.min.js"></script>
    
    <!-- Modernizr -->
    <script src="<?=base_url()?>assets/js/modernizr.min.js"></script>
    
    <!-- Pace -->
    <script src="<?=base_url()?>assets/js/pace.min.js"></script>
    
    <!-- Popup Overlay -->
    <script src="<?=base_url()?>assets/js/jquery.popupoverlay.min.js"></script>
    
    <!-- Slimscroll -->
    <script src="<?=base_url()?>assets/js/jquery.slimscroll.min.js"></script>
    
    <!-- Cookie -->
    <script src="<?=base_url()?>assets/js/jquery.cookie.min.js"></script>

    <!-- Perfect -->
    <script src="<?=base_url()?>assets/js/app/app.js"></script>
    
  </body>

</html>
