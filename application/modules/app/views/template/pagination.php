<?php 
$paging_url_end = '';
if(@$paging->url_end != '') $paging_url_end = '/'.$paging->url_end;
?>
<div class="col-pagination">
  <ul class="showing-data">
    <li class="list-unstyled">
      <form id="paging" action="<?=site_url("$url/1/$o". $paging_url_end) ?>" method="post">
        <label><i class="mdi mdi-bookmark"></i> Tampilkan</label>
        <select name="per_page" class="select-pagination" onchange="$('#paging').submit()">
          <option <?php if($paging->per_page == '10') echo 'selected'?> value="10">10</option>
          <option <?php if($paging->per_page == '50') echo 'selected'?> value="50">50</option>
          <option <?php if($paging->per_page == '100') echo 'selected'?> value="100">100</option>
        </select>
        <label>Dari</label>
        <label><strong><?=($paging->num_rows)?></strong></label>
        <label>Total Data</label>
      </form>
    </li>
  </ul>
  <ul class="pagination d-flex flex-wrap justify-content-end pagination-primary">
    <?php if($paging->start_link): ?>
      <li class="page-item">
        <a class="page-link" href="<?=site_url("$url/$paging->start_link/$o". $paging_url_end) ?>">First</a>
      </li>
    <?php endif; ?>
    <?php if($paging->prev): ?>
      <li class="page-item">
        <a class="page-link" href="<?=site_url("$url/$paging->prev/$o". $paging_url_end) ?>">Prev</a>
      </li>
    <?php endif; ?>

    <?php for($i = $paging->c_start_link; $i <= $paging->c_end_link; $i++): ?>
      <li class="page-item <?php jecho($p, $i, "active") ?>">
        <a class="page-link" href="<?=site_url("$url/$i/$o". $paging_url_end) ?>"><?=$i ?></a>
      </li>
    <?php endfor; ?>
    <?php if($paging->next): ?>
      <li class="page-item">
        <a class="page-link" href="<?=site_url("$url/$paging->next/$o". $paging_url_end) ?>">Next</a>
      </li>
    <?php endif; ?>
    <?php if($paging->end_link): ?>
      <li class="page-item">
        <a class="page-link" href="<?=site_url("$url/$paging->end_link/$o". $paging_url_end) ?>">Last</a>
      </li>
    <?php endif; ?>
  </ul> 
</div>