                <div class="size-toggle">
                    <a class="btn btn-sm" id="sizeToggle">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="btn btn-sm pull-right logoutConfirm_open" href="#logoutConfirm" data-popup-ordinal="1" id="open_25014350">
                        <i class="fa fa-power-off"></i>
                    </a>
                </div>
                <div class="user-block clearfix">
                    <img src="<?=base_url()?>assets/images/user.png" alt="User Avatar">
                    <div class="detail">
                        <strong><?=$profile['user_realname']?></strong><span class="badge badge-danger bounceIn animation-delay4 m-left-xs">4</span>
                        <ul class="list-inline">
                            <li><a href="<?=site_url('manage/index/location/profile')?>" style="margin-left: 5px">Change Profile</a></li>
                        </ul>
                    </div>
                </div><!-- /user-block -->
                <!--
                <div class="search-block">
                    <div class="input-group">
                        <input type="text" class="form-control input-sm" placeholder="search here...">
                        <span class="input-group-btn">
                            <button class="btn btn-default btn-sm" type="button"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </div>
                -->
                <!-- /search-block -->
                <!-- /user-block -->