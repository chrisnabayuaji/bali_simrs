        <aside class="fixed skin-1">     
            <div class="sidebar-inner scrollable-sidebar">
                <!-- user info -->
                <?php $this->load->view('app/template/sidebar-info')?>
                <!-- /user-block -->
                <div class="main-menu">
                    <ul>
                        <?php foreach($module_list as $mod_key => $mod_val):?>                        
                            <?php if($mod_key == 0):?>
                            <li class="active">
                                <a href="javascript:void(0)" style="cursor: default; border-top:0px dotted #bd3f26; border-bottom:2px solid #bd3f26; color: #bd3f26; padding-left: 0px; padding-top:6px; padding-bottom:6px">
                                    <span class="text"><b><?=strtoupper($mod_val['nav_nm'])?></b></span>
                                </a>
                            </li>   
                            <?php else:?>
                            <li>
                                <a href="<?=site_url($mod_val['nav_url'])?>">
                                    <span class="menu-icon"><i class="fa <?=$mod_val['nav_icon']?>"></i> </span>
                                    <span class="text"><b><?=$mod_val['nav_nm']?></b></span>
                                </a>
                            </li>    
                            <?php endif;?>
                        <?php endforeach;?>   
                    </ul>                    
                </div><!-- /main-menu -->
            </div><!-- /sidebar-inner scrollable-sidebar -->
        </aside>