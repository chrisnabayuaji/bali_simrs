        <aside class="fixed skin-1">     
            <div class="sidebar-inner scrollable-sidebar">
                <!-- user info -->
                <?php $this->load->view('app/template/sidebar-info')?>
                <!-- /user-block -->
                <div class="main-menu">
                    <ul>
                        <li class="">
                            <a href="<?=site_url('app/dashboard')?>">
                                <span class="menu-icon"><i class="fa fa-home fa-lg"></i> </span>
                                <span class="text">Halaman Utama</span>
                                <span class="menu-hover"></span>
                            </a>
                        </li>    
                        <?php foreach($group_nav as $gnav):?>     
                        <li class="active">
                            <a href="javascript:void(0)" style="cursor: default; border-top:0px dotted #bd3f26; border-bottom:2px solid #bd3f26; color: #bd3f26; padding-left: 0px; padding-top:6px; padding-bottom:6px">
                                <span class="text"><b><?=strtoupper($gnav['nav_nm'])?></b></span>
                            </a>
                        </li>        
                            <?php foreach($gnav['item_nav'] as $inav):?>
                            <li class="openable <?php if(active_menu($inav['item_nav'], $this->uri->segment(1), $this->uri->segment(2)) == true) echo 'active'?>">
                                <a href="#">
                                    <span class="menu-icon"><i class="fa fa-list"></i> </span>
                                    <span class="text"><b><?=$inav['nav_nm']?></b></span>
                                    <span class="badge badge-danger bounceIn animation-delay1"><?=count($inav['item_nav'])?></span>
                                    <span class="menu-hover"></span>
                                </a>
                                <ul class="submenu">
                                    <?php foreach($inav['item_nav'] as $inav_sub):?>
                                    <li><a href="<?=site_url("route/location/index/" . $inav_sub['nav_url'])?>"><span class="submenu-label"><i class="fa fa-long-arrow-right"></i> <?=$inav_sub['nav_nm']?></span></a></li>
                                    <?php endforeach;?>
                                </ul>   
                            </li>
                            <?php endforeach;?>
                        <?php endforeach;?>
                    </ul>                    
                </div><!-- /main-menu -->
            </div><!-- /sidebar-inner scrollable-sidebar -->
        </aside>