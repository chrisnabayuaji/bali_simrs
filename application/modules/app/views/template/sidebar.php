  <body class="overflow-hidden">
    <!-- Overlay Div -->
    <div id="overlay" class="transparent"></div>
    
    <div id="wrapper" class="preload">
        <div id="top-nav" class="skin-2 fixed">
            <div class="brand">
                <!-- <span><b><?=$config['app_shorttitle']?></b></span> -->
                <span><img src="<?=base_url()?>assets/images/header-app.png"></span>
            </div><!-- /brand -->
            <button type="button" class="navbar-toggle pull-left" id="sidebarToggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <button type="button" class="navbar-toggle pull-left hide-menu" id="menuToggle">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <div class="brand">
                <span class=""><?=@$module_title?></span>
            </div>
            <ul class="nav-notification clearfix">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-bell fa-lg"></i>
                        <span class="notification-label bounceIn animation-delay6">5</span>
                    </a>
                    <ul class="dropdown-menu notification dropdown-3">
                        <li><a href="#">You have 5 new notifications</a></li>                     
                        <li>
                            <a href="#">
                                <span class="notification-icon bg-warning">
                                    <i class="fa fa-warning"></i>
                                </span>
                                <span class="m-left-xs">Server #2 not responding.</span>
                                <span class="time text-muted">Just now</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="notification-icon bg-success">
                                    <i class="fa fa-plus"></i>
                                </span>
                                <span class="m-left-xs">New user registration.</span>
                                <span class="time text-muted">2m ago</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="notification-icon bg-danger">
                                    <i class="fa fa-bolt"></i>
                                </span>
                                <span class="m-left-xs">Application error.</span>
                                <span class="time text-muted">5m ago</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="notification-icon bg-success">
                                    <i class="fa fa-usd"></i>
                                </span>
                                <span class="m-left-xs">2 items sold.</span>
                                <span class="time text-muted">1hr ago</span>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span class="notification-icon bg-success">
                                    <i class="fa fa-plus"></i>
                                </span>
                                <span class="m-left-xs">New user registration.</span>
                                <span class="time text-muted">1hr ago</span>
                            </a>
                        </li>
                        <li><a href="#">View all notifications</a></li>                   
                    </ul>
                </li>
                <li class="profile dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <strong><i class="fa fa-home fa-lg"></i> MODUL SIMRS</strong>
                        <span><i class="fa fa-chevron-down"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <?php foreach($module_list as $mod_key => $mod_val):?>
                        <li <?php if($mod_key == 0) echo 'style="margin-bottom: 0"'?>><a tabindex="-1" href="<?=site_url($mod_val['nav_url'])?>" class="main-link"><i class="<?=$mod_val['nav_icon']?> fa-lg"></i> sdf<?=$mod_val['nav_nm']?></a></li>
                        <?php endforeach;?>                        
                    </ul>
                </li>
                <li class="profile dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <strong><i class="fa fa-user fa-lg"></i> <?=$profile['user_realname']?></strong>
                        <span><i class="fa fa-chevron-down"></i></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a class="clearfix" href="#">
                                <img src="<?=base_url()?>assets/images/user.png" alt="User Avatar">
                                <div class="detail">
                                    <strong><?=$profile['user_realname']?></strong>
                                    <p class="grey"><?=$profile['user_name']?></p> 
                                </div>
                            </a>
                        </li>
                        <!-- <li><a tabindex="-1" href="profile.html" class="main-link"><i class="fa fa-edit fa-lg"></i> Edit profile</a></li>
                        <li><a tabindex="-1" href="gallery.html" class="main-link"><i class="fa fa-picture-o fa-lg"></i> Photo Gallery</a></li>
                        <li><a tabindex="-1" href="#" class="theme-setting"><i class="fa fa-cog fa-lg"></i> Setting</a></li> -->
                        <li class="divider"></li>
                        <li><a tabindex="-1" class="main-link logoutConfirm_open" href="#logoutConfirm"><i class="fa fa-lock fa-lg"></i> Log out</a></li>
                    </ul>
                </li>
            </ul>
        </div><!-- /top-nav-->
        