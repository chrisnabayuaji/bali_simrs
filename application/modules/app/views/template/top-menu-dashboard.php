<style type="text/css">
	.change-animation {
	  margin-top: -5px;
	}
	.slides {
		position: absolute;
		margin-left: 3px;
	}
</style>
<?php if (count($running_text) > 1): ?>
<script type="text/javascript">
$(document).ready(function(){
  $(function(){
    $('.change-animation .slides:gt(0)').hide();
    setInterval(function(){
      $('.change-animation :first-child').fadeOut(2000).next('.slides').fadeIn(2000)
      .end().appendTo('.change-animation');
  	}, 7000);
  });
});
</script>
<?php endif; ?>
<nav class="bottom-navbar">
  <div class="container mw-100" style="height: 36px; padding-top: 8px;">
  	<div class="float-left font-weight-semibold"><i class="fas fa-info-circle"></i> INFO : </div>
    <div class="running-text text-warning change-animation float-left">
      <?php foreach ($running_text as $row): ?>
    	<div class="slides"><?=$row['info_text']?></div>
      <?php endforeach; ?>
    </div>
  </div>
</nav>
</div>

<div class="container-fluid page-body-wrapper">
  <div class="main-panel bg-pb">