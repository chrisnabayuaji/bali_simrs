<nav class="bottom-navbar">
  <div class="container mw-100">
    <ul class="nav page-navigation justify-content-start">
      <li class="nav-item">
        <a class="nav-link" href="<?=site_url('app/dashboard')?>">
          <i class="mdi mdi-view-dashboard menu-icon"></i>
          <span class="menu-title active-menu">Dashboard</span>
        </a>
      </li>
      <?php foreach($group_nav as $gnav):?>
        <?php if (count($gnav['item_nav']) > 0): ?>
          <?php if (count($gnav['sub_label']) > 0): ?>
            <li class="nav-item mega-menu">
              <a href="#" class="nav-link">
                <i class="<?=$gnav['nav_font_icon']?> menu-icon"></i>
                <span class="menu-title <?php if(active_menu($gnav['item_nav'], $this->uri->segment(1), $this->uri->segment(2), $this->uri->segment(3)) == true) echo 'active-menu'?>"><?=$gnav['nav_nm']?></span>
                <i class="menu-arrow"></i>
              </a>
              <div class="submenu">
                <div class="col-group-wrapper row">
                  <?php foreach ($gnav['sub_label'] as $sblabel): ?>
                  <div class="col-group col-md-2">
                    <p class="category-heading"><?=$sblabel['nav_sub_label']?></p>
                    <ul class="submenu-item">
                      <?php foreach($sblabel['large_menu'] as $lmenu):?>
                      <li class="nav-item">
                        <a class="nav-link <?php if(active_submenu($lmenu['nav_url'], $this->uri->segment(1), $this->uri->segment(2), $this->uri->segment(3)) == true) echo 'font-weight-bold'?>" href="<?=$lmenu['url_nav']?>" <?=($lmenu['nav_target'] == 'B') ? 'target="_blank"' : ''?>><?=$lmenu['nav_nm']?></a>
                      </li>
                      <?php endforeach; ?>
                    </ul>
                  </div>
                  <?php endforeach; ?>
                </div>
              </div>
          </li>
          <?php else: ?>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="<?=$gnav['nav_font_icon']?> menu-icon"></i>
                <span class="menu-title <?php if(active_menu($gnav['item_nav'], $this->uri->segment(1), $this->uri->segment(2), $this->uri->segment(3)) == true) echo 'active-menu'?>"><?=$gnav['nav_nm']?></span>
                <i class="menu-arrow"></i>
              </a>
              <div class="submenu">
                <ul class="submenu-item">
                  <?php foreach($gnav['item_nav'] as $inav):?>
                  <li class="nav-item">
                    <a class="nav-link <?php if(active_submenu($inav['nav_url'], $this->uri->segment(1), $this->uri->segment(2), $this->uri->segment(3)) == true) echo 'font-weight-bold'?>" href="<?=$inav['url_nav']?>" <?=($inav['nav_target'] == 'B') ? 'target="_blank"' : ''?>><?=$inav['nav_nm']?></a>
                  </li>
                  <?php endforeach; ?>
                </ul>
              </div>
            </li>
          <?php endif; ?>
        <?php else: ?>
          <li class="nav-item">
            <a class="nav-link" href="<?=$gnav['url_nav']?>" <?=($gnav['nav_target'] == 'B') ? 'target="_blank"' : ''?>>
              <i class="<?=$gnav['nav_font_icon']?> menu-icon"></i>
              <span class="menu-title"><?=$gnav['nav_nm']?></span>
            </a>
          </li>
        <?php endif; ?>
      <?php endforeach; ?>
    </ul>
  </div>
</nav>
</div>

<div class="container-fluid page-body-wrapper">
  <div class="main-panel bg-pb">