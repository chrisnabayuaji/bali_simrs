<script type="text/javascript">
  $(document).ready(function () {
    $("#form-data").validate( {
      rules: {
        user_photo: {
          extension: "jpg|jpeg|gif|png"
        }
      },
      messages: {
         user_photo: {
          extension: "Hanya menerima file berupa gambar"
        }
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container"));
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });
    //
    $('#cb_password').bind('click',function() {
    var c = $(this).is(':checked');
    if(c == true) {
      $('#user_password').removeAttr('disabled').focus().val('');
    } else {
      location.reload(true);    
    }

  }); 
  // select2
  $(".chosen-select").select2();
  $('.select2-container').css('width', '100%');
  //
  $("#form-data").submit(function() {
    var user_password = $('#user_password').val();
    var confirm_user_password = $('#confirm_user_password').val();
    var id = '<?=$id?>';
    //
    if (user_password !='' && confirm_user_password !='') {
      if (user_password != confirm_user_password) {
        swal({
          text: "Kata Sandi Tidak Sama !",
          icon: "warning",
          button: "OK",
        });
        $('#confirm_user_password').removeClass('form-valid');
        $('#confirm_user_password').addClass('form-invalid');
        return false;
      }else{
        $('#confirm_user_password').removeClass('form-invalid');
        $('#confirm_user_password').addClass('form-valid');
      }
    }else if(confirm_user_password =='') {
       if (id !='' && user_password !='') {
        swal({
          text: "Konfirmasi Kata Sandi belum diisi !",
          icon: "warning",
          button: "OK",
        });
        $('#confirm_user_password').removeClass('form-valid');
        $('#confirm_user_password').addClass('form-invalid');
        return false;
       } 
    }
    });
    //
    $('.delete_photo').bind('click',function(e) {
      e.preventDefault();
      const i = $(this).attr('data-id');
      const f = $(this).attr('data-file');
      Swal.fire({
        title: 'Apakah Anda yakin menghapus gambar?',
        text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#eb3b5a',
        cancelButtonColor: '#b2bec3',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        customClass: 'swal-wide'
      }).then((result) => {
        if (result.value) {
          $.get('<?=site_url("app/user/ajax/delete_photo")?>?user_id='+i+'&user_photo='+f,null,function(data) {
            if(data.callback == 'true') {
              $('#box_img').hide();
              $.toast({
                heading: 'Sukses',
                text: 'Gambar berhasil dihapus.',
                icon: 'success',
                position: 'top-right'
              })
            }else {
              $.toast({
                heading: 'Gagal',
                text: 'Gambar gagal dihapus.',
                icon: 'error',
                position: 'top-right'
              })
            }                    
          },'json');
        }
      })
    });
  })
</script>