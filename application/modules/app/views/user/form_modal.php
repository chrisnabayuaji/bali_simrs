<!-- js -->
<?php $this->load->view('app/user/_js') ?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?= $form_action ?>" autocomplete="off">
  <div class="row">
    <div class="col-lg-6">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Pegawai</label>
        <div class="col-lg-5 col-md-5">
          <select class="chosen-select custom-select w-100" id="pegawai_id" name="pegawai_id">
            <option value="">- Pilih -</option>
            <?php foreach ($list_pegawai as $pegawai) : ?>
              <option value="<?= $pegawai['pegawai_id'] ?>" <?php if ($pegawai['pegawai_id'] == @$main['pegawai_id']) echo 'selected' ?>><?= $pegawai['pegawai_nm'] ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">User Role/Group</label>
        <div class="col-lg-5 col-md-5">
          <select class="chosen-select custom-select w-100" name="role_id" required="">
            <option value="">- Pilih -</option>
            <?php foreach ($list_role as $role) : ?>
              <option value="<?= $role['role_id'] ?>" <?php if ($role['role_id'] == @$main['role_id']) echo 'selected' ?>><?= $role['role_nm'] ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Username <span class="text-danger">*<span></label>
        <div class="col-lg-5 col-md-5">
          <input type="text" class="form-control" name="user_name" value="<?= @$main['user_name'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Real Name <span class="text-danger">*<span></label>
        <div class="col-lg-9 col-md-9">
          <input type="text" class="form-control" id="user_realname" name="user_realname" value="<?= @$main['user_realname'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Photo</label>
        <div class="col-lg-4 col-md-9">
          <input type="file" name="user_photo" class="form-control" id="user_photo">
          <?php if (@$main['user_photo'] != '') : ?>
            <div id="box_img">
              <div class="text-danger small-text font-weight-semibold">* Upload gambar untuk mengubah</div>
              <div class="card mb-2 mt-1">
                <div class="card-body">
                  <div class="text-center">
                    <img src="<?= base_url() ?>assets/images/user/<?= @$main['user_photo'] ?>" class="img-lg rounded">
                    <button type="button" class="btn btn-xs btn-danger mt-2 delete_photo" data-id="<?= @$main['user_id'] ?>" data-file="<?= @$main['user_photo'] ?>"><i class="far fa-trash-alt"></i> Hapus Gambar</button>
                  </div>
                </div>
              </div>
            </div>
          <?php endif; ?>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Status</label>
        <div class="col-lg-4 col-md-9">
          <select class="chosen-select custom-select w-100" name="is_active">
            <option value="1" <?php if (@$main['is_active'] == '1') echo 'selected' ?>>Aktif</option>
            <option value="0" <?php if (@$main['is_active'] == '0') echo 'selected' ?>>Tidak Aktif</option>
          </select>
        </div>
      </div>
    </div>
    <div class="col-lg-6">
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Password <span class="text-danger">*<span></label>
        <div class="col-lg-5 col-md-5">
          <input type="text" class="form-control" name="user_password" id="user_password" <?= (@$id == '') ? 'required=""' : '' ?> autocomplete="off">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Konfirmasi Password <span class="text-danger">*<span></label>
        <div class="col-lg-5 col-md-5">
          <input type="text" class="form-control" name="confirm_user_password" id="confirm_user_password" <?= (@$id == '') ? 'required=""' : '' ?> autocomplete="off">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Kode User <span class="text-danger">*<span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="user_cd" id="user_cd" value="<?= @$main['user_cd'] ?>" required="">
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="col-12">
    <div class="table-responsive" style="max-height:35vh">
      <table class="table table-hover table-striped table-bordered">
        <thead>
          <tr>
            <th class="text-center text-middle" width="36">No</th>
            <th class="text-center text-middle" width="36">
              <div class="form-check form-check-primary form-check-th">
                <label class="form-check-label">
                  <input type="checkbox" class="form-check-input cb-all-lokasi">
                  <i class="input-helper"></i></label>
              </div>
            </th>
            <th class="text-center text-middle" width="80">Kode</th>
            <th class="text-center text-middle">Nama Lokasi</th>
            <th class="text-center text-middle" width="90">Jenis Registrasi</th>
            <th class="text-center text-middle" width="90">Untuk Pendaftaran Online ?</th>
            <th class="text-center text-middle" width="70">Ada Bed?</th>
            <th class="text-center text-middle" width="70">Ada Antrian?</th>
            <th class="text-center text-middle" width="70">Ada Jadwal Dokter?</th>
            <th class="text-center text-middle" width="70">Sebagai Depo Farmasi?</th>
            <th class="text-center text-middle" width="55">Status</th>
          </tr>
        </thead>
        <?php if (@$list_lokasi == null) : ?>
          <tbody>
            <tr>
              <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
            </tr>
          </tbody>
        <?php else : ?>
          <tbody>
            <?php $j = 1;
            foreach ($list_lokasi as $row) : ?>
              <tr>
                <td class="text-center" width="36"><?= ($j++) ?></td>
                <td class="text-center" width="36">
                  <div class="d-flex justify-content-center">
                    <div class="form-check form-check-primary" style="margin-left: -10px !important;">
                      <label class="form-check-label">
                        <input type="checkbox" class="form-check-input cb-item-lokasi" id="lokasi_<?= str_replace('.', '_', $row['lokasi_id']) ?>" name="lokasi_id[]" value="<?= $row['lokasi_id'] ?>">
                        <i class="input-helper"></i></label>
                    </div>
                  </div>
                </td>
                <td width="80"><?= $row['lokasi_id'] ?></td>
                <td>
                  <?php
                  $pref = ($row['parent_id'] != '') ? '|-- ' : '<b>';
                  $suf = ($row['parent_id'] != '') ?  '' : '</b>';
                  $spc = 0;
                  for ($i = 0; $i < strlen($row['lokasi_id']); $i++) {
                    $spc += 3;
                  };
                  $space = '<span style="margin-left:' . $spc . 'px;"></span> ';
                  echo $space . $pref . $row['lokasi_nm'] . $suf;
                  ?>
                </td>
                <td class="text-center" width="90">
                  <?php if ($row['jenisreg_st'] == '1') : ?>
                    Rawat Jalan
                  <?php elseif ($row['jenisreg_st'] == '2') : ?>
                    Rawat Inap
                  <?php else : ?>
                    -
                  <?php endif; ?>
                </td>
                <td class="text-center" width="90">
                  <li class="fa <?= ($row['is_reg_online'] == '1' ? 'fa-check-circle text-success' : 'fa-minus-circle text-danger') ?>"></li>
                </td>
                <td class="text-center" width="70">
                  <li class="fa <?= ($row['is_bed'] == '1' ? 'fa-check-circle text-success' : 'fa-minus-circle text-danger') ?>"></li>
                </td>
                <td class="text-center" width="70">
                  <li class="fa <?= ($row['is_loket_antrian'] == '1' ? 'fa-check-circle text-success' : 'fa-minus-circle text-danger') ?>"></li>
                </td>
                <td class="text-center" width="70">
                  <li class="fa <?= ($row['is_jadwal_dokter'] == '1' ? 'fa-check-circle text-success' : 'fa-minus-circle text-danger') ?>"></li>
                </td>
                <td class="text-center" width="70">
                  <li class="fa <?= ($row['is_depo_farmasi'] == '1' ? 'fa-check-circle text-success' : 'fa-minus-circle text-danger') ?>"></li>
                </td>
                <td class="text-center" width="55">
                  <li class="fa <?= ($row['is_active'] == '1' ? 'fa-check-circle text-success' : 'fa-minus-circle text-danger') ?>"></li>
                </td>
              </tr>
            <?php endforeach; ?>
          </tbody>
        <?php endif; ?>
      </table>
    </div>
    <button id="semualokasi" class="btn btn-xs btn-primary mt-2" type="button"><i class="fas fa-check-square"></i> Pilih Semua Lokasi</button>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="col"></div>
      <div class="col-2 float-right">
        <button type="submit" class="btn btn-xs btn-primary btn-submit float-right"><i class="fas fa-save"></i> Simpan</button>
        <button type="button" class="btn btn-xs btn-secondary btn-cancel float-right mr-2" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
      </div>
    </div>
  </div>
</form>

<script>
  $(document).ready(function() {
    $('.cb-all-lokasi').click(function(e) {
      $('.cb-item-lokasi').prop('checked', this.checked);
    });

    $('#semualokasi').click(function(e) {
      $('.cb-item-lokasi').attr('checked', true);
      $('.cb-all-lokasi').attr('checked', true);
    });

    <?php if ($id != null) : ?>
      <?php foreach ($list_lokasi as $row) : ?>
        <?php if ($row['user_lokasi'] == $row['lokasi_id']) : ?>
          $("#lokasi_<?= str_replace('.', '_', $row['lokasi_id']) ?>").attr('checked', true);
        <?php endif; ?>
      <?php endforeach; ?>
    <?php endif; ?>

    $("#pegawai_id").on('change', function() {
      var pegawai_nm = $("#pegawai_id option:selected").text();
      $("#user_realname").val(pegawai_nm);
    });

  })
</script>