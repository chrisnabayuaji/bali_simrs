<?php 
if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Konfigurasi extends MY_Controller{

	var $nav_id = '09.01', $nav, $cookie;
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array(
			'm_konfigurasi'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
    $this->cookie = get_cookie_nav($this->nav_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'role_id','type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}
	
	public function index() {	
		redirect(site_url().'/'.$this->nav['nav_url'].'/form');
	}

	public function form() {
    $this->authorize($this->nav, '_update');
		
		$data['nav'] = $this->nav;
		$data['main'] = $this->m_konfigurasi->get_first();
		$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/save';
		$this->render('bridging/konfigurasi/form',$data);
	}
	
	public function save()
	{
		$this->m_konfigurasi->update();
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log('_update', $this->nav_id);
		redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}
	
}