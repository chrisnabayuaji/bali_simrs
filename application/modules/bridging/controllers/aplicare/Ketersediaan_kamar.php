<?php 
if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Ketersediaan_kamar extends MY_Controller{

	var $nav_id = '09.02.01', $nav, $cookie;
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array(
			'm_konfigurasi'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
    $this->cookie = get_cookie_nav($this->nav_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'role_id','type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}
	
	public function index() {	
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(4, 0);
    $this->cookie['total_rows'] = null;
    set_cookie_nav($this->nav_id, $this->cookie);
		//main data
		$conf = $this->m_konfigurasi->get_first();
    $data['nav'] = $this->nav;
    $data['cookie'] = $this->cookie;
    //set pagination
    set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('bridging/ketersediaan_kamar/index',$data);
	}

	public function referensi_kelas() {
    $this->authorize($this->nav, '_view');

    // $data['main'] = $this->m_upf->get_data($id);
		$data['nav'] = $this->nav;
			
		echo json_encode(array(
			'html' => $this->load->view('bridging/ketersediaan_kamar/referensi_kelas', $data, true)
		));
	}
	
	public function form_modal($id = null)
	{
		if ($id == null) {
			$data['main'] = null;
		}else{
			$conf = $this->m_konfigurasi->get_first();
			$res = json_decode(bpjs_service('aplicare', 'GET', "rest/bed/read/".$conf['kode_ppk']."/1/50"),true);
			
			if (isset($res['response']['list'])) {
				$data['main'] = $res['response']['list'][$id];
			}else{
				$data['main'] = null;
			}
		}

		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/save/'.$id;
			
		echo json_encode(array(
			'html' => $this->load->view('bridging/ketersediaan_kamar/form_modal', $data, true)
		));
	}

	public function save($id)
	{
		$data = $this->input->post();
		$data_str = json_encode($data);

		$conf = $this->m_konfigurasi->get_first();
		
		if ($id == null) {
			$this->session->set_flashdata('flash_success', 'Data berhasil ditambahkan');
			$response = bpjs_service('aplicare', 'POST', "rest/bed/create/".$conf['kode_ppk'], $data_str);
		}else{
			$this->session->set_flashdata('flash_success', 'Data berhasil diubah');
			$response = bpjs_service('aplicare', 'POST', "rest/bed/update/".$conf['kode_ppk'], $data_str);
		}

		redirect(site_url().'/'.$this->nav['nav_url'].'/index');
	}

	public function delete($kodekelas = null, $koderuang = null)
	{
		if ($kodekelas != null && $koderuang != null) {
			$conf = $this->m_konfigurasi->get_first();
			$data = array(
				'kodekelas' => $kodekelas,
				'koderuang' => $koderuang
			);
			$data_str = json_encode($data);
			$response = bpjs_service('aplicare', 'POST', "rest/bed/delete/".$conf['kode_ppk'], $data_str);
		}
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		redirect(site_url().'/'.$this->nav['nav_url'].'/index');
	}
  
  public function ajax($type)
  {
		$conf = $this->m_konfigurasi->get_first();

		if ($type == 'referensi_kelas') {
			$res = json_decode(bpjs_service('aplicare', 'GET', "rest/ref/kelas"),true);
			
			if (isset($res['response']['list'])) {
				foreach ($res['response']['list'] as $k => $v) {
					$l = array(
						'id' => $v['kodekelas'],
						'text' => '['.$v['kodekelas'].'] '.$v['namakelas']
					);
					$r[] = $l;
				}
				echo json_encode(array('result' => $r));
			}else{
				echo 'NULL';
			}
		}

		if ($type == 'ketersediaan_kamar') {
			$res = json_decode(bpjs_service('aplicare', 'GET', "rest/bed/read/".$conf['kode_ppk']."/1/50"),true);
			$r = array();
			if (isset($res['response']['list'])) {
				foreach ($res['response']['list'] as $k => $v) {
					$aksi = '<a href="javascript:void(0)" data-href="'.site_url().'/'.$this->nav['nav_url'].'/form_modal/'.$k.'" onclick="update(this)" modal-title="Ubah Data" modal-size="md" class="btn btn-primary btn-table" style="padding:4px" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Ubah Data"><i class="fas fa-pencil-alt"></i></a>';
					$aksi .= '<a href="javascript:void(0)" data-href="'.site_url().'/'.$this->nav['nav_url'].'/delete/'.$v['kodekelas'].'/'.$v['koderuang'].'" onclick="delete_row(this)" modal-title="Hapus Data" modal-size="md" class="btn btn-danger btn-table ml-1" style="padding:4px" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-danger" title="" data-original-title="Ubah Data"><i class="fas fa-trash-alt"></i></a>';
					$v['aksi'] = $aksi;
					$r[] = $v;
				}
				echo json_encode($r);
			}else{
				echo 'NULL';
			}
		}

		if ($type == 'detail_kamar') {
			$res = json_decode(bpjs_service('aplicare', 'GET', "rest/bed/read/".$conf['kode_ppk']."/1/50"),true);
			$r = array();
			if (isset($res['response']['list'])) {
				echo json_encode($res['response']['list']['']);
				echo json_encode($r);
			}else{
				echo 'NULL';
			}
		}

	}
	
}