
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_konfigurasi extends CI_Model {

  public function get_first()
  {
    $sql = "SELECT * FROM bpjs_config";
    $query = $this->db->query($sql);
    return $query->row_array();
  }

  function get_data($id) {
    $sql = "SELECT * FROM bpjs_config WHERE config_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function update($data)
  {
    $data = html_escape($this->input->post());
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->update('bpjs_config', $data);
  }
  
}