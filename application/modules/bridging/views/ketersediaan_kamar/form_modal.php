<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Kode Ruang <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="koderuang" id="koderuang" value="<?=@$main['koderuang']?>" required="" <?=($id != null) ? 'readonly':''?>>
        </div>
      </div>
      <?php if($id == null): ?>
        <div class="form-group row">
          <label class="col-lg-4 col-md-3 col-form-label">Kelas <span class="text-danger">*</span></label>
          <div class="col-lg-5 col-md-3">
            <select class="form-control" name="kodekelas" id="kodekelas">

            </select>
          </div>
        </div>
      <?php else: ?>
        <div class="form-group row">
          <label class="col-lg-4 col-md-3 col-form-label">Kelas <span class="text-danger">*</span></label>
          <div class="col-lg-5 col-md-3">
            <input type="text" class="form-control" id="namakelas" value="<?='['.@$main['kodekelas'].'] '.@$main['namakelas']?>" required="" <?=($id != null) ? 'readonly':''?>>
            <input type="hidden" class="form-control" name="kodekelas" id="kodekelas" value="<?=@$main['kodekelas']?>" required="" <?=($id != null) ? 'readonly':''?>>
          </div>
        </div>
      <?php endif; ?>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Nama Ruang <span class="text-danger">*</span></label>
        <div class="col-lg-6 col-md-3">
          <input type="text" class="form-control" name="namaruang" id="namaruang" value="<?=@$main['namaruang']?>" required="" <?=($id != null) ? 'readonly':''?>>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Kapasitas <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="number" class="form-control" name="kapasitas" id="kapasitas" value="<?=@$main['kapasitas']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Tersedia <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="number" class="form-control" name="tersedia" id="tersedia" value="<?=@$main['tersedia']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Tersedia Pria<span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="number" class="form-control" name="tersediapria" id="tersediapria" value="<?=@$main['tersediapria']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Tersedia Wanita<span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="number" class="form-control" name="tersediawanita" id="tersediawanita" value="<?=@$main['tersediawanita']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Tersedia Pria/Wanita<span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="number" class="form-control" name="tersediapriawanita" id="tersediapriawanita" value="<?=@$main['tersediapriawanita']?>" required="">
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>
<script>
  $(document).ready(function () {
    $.ajax({
      type : 'get',
      url : '<?=site_url($nav['nav_url'])?>/ajax/referensi_kelas',
      dataType : 'json',
      async : false,
      success : function (data) {
        kelas_data = data;
        $('#kodekelas').select2({
          data: data.result
        });
        <?php if($id != null):?>
          $('#kodekelas').val('<?=@$main['kodekelas']?>').trigger('change');
        <?php endif;?>
        $('.select2-container').css('width', '100%');
      }
    })
    
  })
</script>