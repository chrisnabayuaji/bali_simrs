<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline col-title">
        <div class="col-back">
          <a href="<?=site_url($nav['nav_module'].'/dashboard')?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
        </div>
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          <?=$nav['nav_nm']?>
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?=site_url('app/dashboard')?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url($module_nav['nav_url'])?>"><i class="fas fa-folder-open"></i> <?=$module_nav['nav_nm']?></a></li>
          <li class="breadcrumb-item"><a href="#"><i class="fas fa-hospital-alt"></i> Data Dasar RS</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url($nav['nav_url'])?>"><?=$nav['nav_nm']?></a></li>
          <li class="breadcrumb-item active"><span>Index</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>
  <!-- flash message -->
  <div class="flash-success" data-flashsuccess="<?=$this->session->flashdata('flash_success')?>"></div>
  <!-- /flash message -->
  <div class="row full-page mt-4 mb-n2">
    <div class="col-lg-12 grid-margin-xl-0 stretch-card">
      <div class="card border-none">
        <div class="card-body">
          <div class="row">
            <div class="col-2">
              <?php if ($nav['_add']):?>
                <a href="#" data-href="<?=site_url().'/'.$nav['nav_url'].'/form_modal'?>" modal-title="Tambah Data" modal-size="md" class="btn btn-xs btn-primary modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Tambah Data"><i class="fas fa-plus-circle"></i> Tambah</a>
              <?php endif;?>
            </div>
          </div><!-- /.row -->
          <div class="row mt-2">
            <div class="col-md-12">
              <div class="table-responsive">
                <table id="table_ketersediaan_kamar" class="table table-bordered table-striped table-sm">
                  <thead>
                    <tr>
                      <th class="text-center" width="30">No</th>
                      <th class="text-center" width="50">Aksi</th>
                      <th class="text-center" width="80">Kode Ruang</th>
                      <th class="text-center">Nama Ruang</th>
                      <th class="text-center" width="80">Kode Kelas</th>
                      <th class="text-center" width="120">Nama Kelas</th>
                      <th class="text-center" width="80">Kapasitas</th>
                      <th class="text-center" width="80">Tersedia</th>
                      <th class="text-center" width="80">Tersedia Pria</th>
                      <th class="text-center" width="100">Tersedia Wanita</th>
                      <th class="text-center" width="130">Tersedia Pria/Wanita</th>
                      <th class="text-center" width="150">Last Update</th>
                    </tr>
                  </thead>
                </table>
              </div>
            </div>
          </div><!-- /.row -->
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function () {
    var ketersediaanKamarTable = $('#table_ketersediaan_kamar').DataTable( {
      autoWidth : false,
      processing: true, 
      serverSide: false, 
      paging: false,
      retrive: false,
      info: false,
      order: [[0, "asc"]],
      ajax: {
        url: '<?=site_url($nav['nav_url'])?>/ajax/ketersediaan_kamar',
        dataSrc: ''
      },
      columns: [
        { data: 'rownumber' },
        { data: 'aksi' },
        { data: 'koderuang' },
        { data: 'namaruang' },
        { data: 'kodekelas' },
        { data: 'namakelas' },
        { data: 'kapasitas' },
        { data: 'tersedia' },
        { data: 'tersediapria' },
        { data: 'tersediawanita' },
        { data: 'tersediapriawanita' },
        { data: 'lastupdate' },
      ],
      columnDefs: [
        {"targets": 0, "className": 'text-center', "orderable" : true},
        {"targets": 1, "className": 'text-center', "orderable" : true},
        {"targets": 2, "className": 'text-center', "orderable" : true},
        {"targets": 3, "className": 'text-left', "orderable" : true},
        {"targets": 4, "className": 'text-center', "orderable" : true},
        {"targets": 5, "className": 'text-center', "orderable" : true},
        {"targets": 6, "className": 'text-center', "orderable" : true},
        {"targets": 7, "className": 'text-center', "orderable" : true},
        {"targets": 8, "className": 'text-center', "orderable" : true},
        {"targets": 9, "className": 'text-center', "orderable" : true},
        {"targets": 10, "className": 'text-center', "orderable" : true},
        {"targets": 10, "className": 'text-center', "orderable" : true},
      ],
    });
    ketersediaanKamarTable.columns.adjust().draw();
  })
</script>
<script>
  function update(obj) {
    var modal_title = $(obj).attr("modal-title");
    var modal_size = $(obj).attr("modal-size");
    var modal_custom_size = $(obj).attr("modal-custom-size");
    var modal_header = $(obj).attr("modal-header");
    
    $("#modal-size").removeClass('modal-lg').removeClass('modal-md').removeClass('modal-sm');

    $("#modal-title").html(modal_title);
    $("#modal-size").addClass('modal-' + modal_size);
    if(modal_custom_size){
      $("#modal-size").attr('style', 'max-width: '+modal_custom_size+'px !important');
    }
    if (modal_header == 'hidden') {
      $("#modal-header").addClass('d-none');
    }else{
      $("#modal-header").removeClass('d-none');
    }
    $("#myModal").modal('show');
    $.post($(obj).data('href'), function (data) {
      $("#modal-body").html(data.html);
    }, 'json');
  }

  function delete_row(obj) {
    const href = $(obj).data('href');

    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Aksi ini tidak bisa dikembalikan.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#eb3b5a',
      cancelButtonColor: '#b2bec3',
      confirmButtonText: 'Hapus',
      cancelButtonText: 'Batal',
      customClass: 'swal-wide'
    }).then((result) => {
      if (result.value) {
        document.location.href = href;
      }
    })
  }
</script>