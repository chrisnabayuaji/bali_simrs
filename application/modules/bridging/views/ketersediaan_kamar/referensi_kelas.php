<table id="table_referensi_kelas" class="table table-bordered table-striped table-sm">
  <thead>
    <tr>
      <th>Kode</th>
      <th>Nama Kelas</th>
    </tr>
  </thead>
</table>
<script>
  $(document).ready(function () {
    $('#table_referensi_kelas').DataTable( {
      ajax: {
        url: '<?=site_url($nav['nav_url'])?>/ajax/referensi_kelas',
        dataSrc: 'response.list'
      },
      columns: [
        { data: 'kodekelas' },
        { data: 'namakelas' },
      ]
    });
  })
</script>