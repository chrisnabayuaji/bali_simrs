<!-- js -->
<?php $this->load->view('app/profile/_js')?>
<!-- / -->

<form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?=$form_action?>" class="needs-validation" novalidate>
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline col-title">
          <div class="col-back">
            <a href="<?=site_url($nav['nav_module'].'/dashboard')?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
          </div>
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            Konfigurasi <?=$nav['nav_nm']?>
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?=site_url('app/dashboard')?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?=site_url($module_nav['nav_url'])?>"><i class="fas fa-folder-open"></i> <?=$module_nav['nav_nm']?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-cog"></i> Bridging</a></li>
            <li class="breadcrumb-item"><a href="<?=site_url($nav['nav_url'])?>"><?=$nav['nav_nm']?></a></li>
            <li class="breadcrumb-item active"><span>Form</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>

    <div class="row full-page mt-4">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <div class="row mt-1">
              <div class="col-md-6">
                <h4 class="card-title border-bottom border-2 pb-2 mb-3">Konfigurasi Aplikasi</h4>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Cons Id <span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-8">
                    <input type="text" class="form-control" name="cons_id" value="<?=@$main['cons_id']?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Secret Key<span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-8">
                    <input type="text" class="form-control" name="secret_key" value="<?=@$main['secret_key']?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Kode PPK<span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-8">
                    <input type="text" class="form-control" name="kode_ppk" value="<?=@$main['kode_ppk']?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Base Url Vclaim <span class="text-danger">*<span></label>
                  <div class="col-lg-6 col-md-5">
                    <input type="text" class="form-control" name="vclaim_base_url" value="<?=@$main['vclaim_base_url']?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Service Name Vclaim <span class="text-danger">*<span></label>
                  <div class="col-lg-3 col-md-7">
                    <input type="text" class="form-control" name="vclaim_service_name" value="<?=@$main['vclaim_service_name']?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Base Url Aplicare <span class="text-danger">*<span></label>
                  <div class="col-lg-6 col-md-5">
                    <input type="text" class="form-control" name="aplicare_base_url" value="<?=@$main['aplicare_base_url']?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Service Name Aplicare <span class="text-danger">*<span></label>
                  <div class="col-lg-3 col-md-7">
                    <input type="text" class="form-control" name="aplicare_service_name" value="<?=@$main['aplicare_service_name']?>">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="btn-form">
    <div class="btn-form-action">
      <div class="btn-form-action-bottom w-100 small-text clearfix">
        <div class="col-lg-10 col-md-8 col-4">
        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-clear">
          <button type="reset" onClick="window.location.reload();" class="btn btn-xs btn-primary"><i class="mdi mdi-refresh"></i> Clear</button>
        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-save">
          <button type="submit" class="btn btn-xs btn-primary"><i class="mdi mdi-file-check"></i> Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>
