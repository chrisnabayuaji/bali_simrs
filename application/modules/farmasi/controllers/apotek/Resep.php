<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

require './vendor/autoload.php';

use Spipu\Html2Pdf\Html2Pdf;

class Resep extends MY_Controller
{

	var $nav_id = '05.03.01', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'apotek/m_resep',
			'master/m_lokasi',
			'apotek/m_dt_obat',
			'apotek/m_dt_obat_master',
			'app/m_profile',
			'master/m_identitas',
			'master/m_pegawai',
			'apotek/m_dt_rajal',
			'apotek/m_dt_ranap',
			'apotek/m_dt_igd'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id);
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('tgl_registrasi_from' => '', 'tgl_registrasi_to' => '', 'no_rm_nm' => '', 'lokasi_id' => '', 'lokasi_depo' => '', 'resepgroup_st' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'resep_id', 'type' => 'desc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(5, 0);
		$this->cookie['total_rows'] = $this->m_resep->all_rows($this->cookie);
		set_cookie_nav($this->nav_id, $this->cookie);
		//main data
		$data['nav'] = $this->nav;
		$data['cookie'] = $this->cookie;
		$data['main'] = $this->m_resep->list_data($this->cookie);
		$data['lokasi'] = $this->m_lokasi->by_field('jenisreg_st', 1, 'result');
		$data['lokasi_depo'] = $this->m_lokasi->by_field('is_depo_farmasi', 1, 'result');
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
		//set pagination
		set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('farmasi/apotek/resep/index', $data);
	}

	public function form($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
			$data['alergi'] = array();
			$data['diagnosis'] = array();
			$data['rinc'] = null;
			$data['rinc_racik'] = null;
		} else {
			$data['main'] = $this->m_resep->get_data($id);
			$data['alergi'] = $this->m_resep->get_alergi($data['main']['pasien_id']);
			$data['diagnosis'] = $this->m_resep->get_diagnosis($data['main']['reg_id']);
			$data['rinc'] = $this->m_resep->rinc($id);
			$data['rinc_racik'] = $this->m_resep->rinc_racik($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['apoteker'] = $this->m_pegawai->by_field('jenispegawai_cd', '05', 'result');
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save/' . $id;

		$this->render('farmasi/apotek/resep/form', $data);
	}

	public function form_modal()
	{
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/new_resep/';

		echo json_encode(array(
			'html' => $this->load->view('farmasi/apotek/resep/form_modal', $data, true)
		));
	}

	public function new_resep()
	{
		$data = $this->input->post();
		$new = $this->m_resep->new_resep($data);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $new);
	}

	public function save($id = null)
	{
		$this->m_resep->save($id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	public function farmasi($resep_id, $id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['detail'] = array();
		} else {
			$data['detail'] = $this->m_resep->farmasi_get($id);
		}
		$data['nav'] = $this->nav;
		$data['resep'] = $this->m_resep->get_data($resep_id);
		$data['id'] = $id;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/farmasi_save/' . $id;

		echo json_encode(array(
			'html' => $this->load->view('farmasi/apotek/resep/farmasi', $data, true)
		));
	}

	// List Pasien
	public function ajax_list_pasien($type = null, $id = null)
	{
		if ($type == 'search_rajal') {
			$list = $this->m_dt_rajal->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$alamat = '';
				$alamat .= ($field['alamat'] != '') ? $field['alamat'] . ', <br>' : '';
				$alamat .= ($field['kelurahan'] != '') ? ucwords(strtolower($field['kelurahan'])) . ', ' : '';
				$alamat .= ($field['kecamatan'] != '') ? ucwords(strtolower($field['kecamatan'])) . ', ' : '';
				$alamat .= ($field['kabupaten'] != '') ? ucwords(strtolower($field['kabupaten'])) . ', ' : '';
				$alamat .= ($field['provinsi'] != '') ? ucwords(strtolower($field['provinsi'])) . ', ' : '';
				$row = array();
				// $row[] = $field['reg_id'];
				$row[] = $field['pasien_id'];
				$row[] = '<b>' . $field['pasien_nm'] . ', ' . $field['sebutan_cd'] . ' (' . $field['sex_cd'] . ')' . '</b><br>' . $alamat;
				$row[] = $field['lokasi_nm'] . '<br><b>' . $field['jenispasien_nm'] . '</b>';
				$row[] = to_date($field['tgl_registrasi'], '', 'full_date');
				$row[] = $field['pegawai_nm'];
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="pilih_reg(' . "'" . $field['reg_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_rajal->count_all(),
				"recordsFiltered" => $this->m_dt_rajal->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'search_ranap') {
			$list = $this->m_dt_ranap->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$alamat = '';
				$alamat .= ($field['alamat'] != '') ? $field['alamat'] . ', <br>' : '';
				$alamat .= ($field['kelurahan'] != '') ? ucwords(strtolower($field['kelurahan'])) . ', ' : '';
				$alamat .= ($field['kecamatan'] != '') ? ucwords(strtolower($field['kecamatan'])) . ', ' : '';
				$alamat .= ($field['kabupaten'] != '') ? ucwords(strtolower($field['kabupaten'])) . ', ' : '';
				$alamat .= ($field['provinsi'] != '') ? ucwords(strtolower($field['provinsi'])) . ', ' : '';
				$row = array();
				// $row[] = $field['reg_id'];
				$row[] = $field['pasien_id'];
				$row[] = '<b>' . $field['pasien_nm'] . ', ' . $field['sebutan_cd'] . ' (' . $field['sex_cd'] . ')' . '</b><br>' . $alamat;
				$row[] = $field['lokasi_nm'] . '<br><b>' . $field['jenispasien_nm'] . '</b>';
				$row[] = to_date($field['tgl_registrasi'], '', 'full_date');
				$row[] = $field['pegawai_nm'];
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="pilih_reg(' . "'" . $field['reg_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_ranap->count_all(),
				"recordsFiltered" => $this->m_dt_ranap->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'search_igd') {
			$list = $this->m_dt_igd->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$alamat = '';
				$alamat .= ($field['alamat'] != '') ? $field['alamat'] . ', <br>' : '';
				$alamat .= ($field['kelurahan'] != '') ? ucwords(strtolower($field['kelurahan'])) . ', ' : '';
				$alamat .= ($field['kecamatan'] != '') ? ucwords(strtolower($field['kecamatan'])) . ', ' : '';
				$alamat .= ($field['kabupaten'] != '') ? ucwords(strtolower($field['kabupaten'])) . ', ' : '';
				$alamat .= ($field['provinsi'] != '') ? ucwords(strtolower($field['provinsi'])) . ', ' : '';
				$row = array();
				// $row[] = $field['reg_id'];
				$row[] = $field['pasien_id'];
				$row[] = '<b>' . $field['pasien_nm'] . ', ' . $field['sebutan_cd'] . ' (' . $field['sex_cd'] . ')' . '</b><br>' . $alamat;
				$row[] = $field['lokasi_nm'] . '<br><b>' . $field['jenispasien_nm'] . '</b>';
				$row[] = to_date($field['tgl_registrasi'], '', 'full_date');
				$row[] = $field['pegawai_nm'];
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="pilih_reg(' . "'" . $field['reg_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_igd->count_all(),
				"recordsFiltered" => $this->m_dt_igd->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}
	}

	// Pemberian Obat
	public function ajax_farmasi($type = null, $id = null)
	{
		if ($type == 'data') {
			$data['nav'] = $this->nav;
			$data['farmasi'] = $this->m_resep->farmasi_data();
			echo json_encode(array(
				'html' => $this->load->view('farmasi/apotek/resep/farmasi_data', $data, true)
			));
		}

		if ($type == 'save') {
			$this->m_resep->farmasi_save();
		}

		if ($type == 'autocomplete') {
			$obat_nm = $this->input->get('obat_nm');
			$map_lokasi_depo = $this->input->get('map_lokasi_depo');
			$res = $this->m_resep->obat_autocomplete($obat_nm, $map_lokasi_depo);
			echo json_encode($res);
		}

		if ($type == 'search_obat') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('farmasi/apotek/resep/search_obat', $data, true)
			));
		}

		if ($type == 'search_data') {
			$map_lokasi_depo = $this->input->post('map_lokasi_depo');
			$list = $this->m_dt_obat->get_datatables($map_lokasi_depo);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['no_batch'];
				$row[] = $field['obat_nm'];
				$row[] = ($field['satuan_cd'] != '') ? get_parameter_value('satuan_cd', $field['satuan_cd']) : '-';
				$row[] = ($field['tgl_expired'] != '0000-00-00') ? to_date($field['tgl_expired'], '-', 'date', ' ') : '-';
				$row[] = num_id($field['harga_jual']);
				$row[] = num_id($field['stok_akhir']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="obat_fill(' . "'" . $field['obat_id'] . "'" . ',' . "'" . $field['stokdepo_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_obat->count_all($map_lokasi_depo),
				"recordsFiltered" => $this->m_dt_obat->count_filtered($map_lokasi_depo),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'search_data_master') {
			$list = $this->m_dt_obat_master->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="obat_fill_master(' . "'" . $field['obat_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_obat_master->count_all(),
				"recordsFiltered" => $this->m_dt_obat_master->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'obat_fill') {
			$data = $this->input->post();
			$res = $this->m_resep->obat_row($data['obat_id'], $data['stokdepo_id']);
			echo json_encode($res);
		}

		if ($type == 'obat_fill_master') {
			$data = $this->input->post();
			$res = $this->m_resep->obat_row_master($data['obat_id']);
			echo json_encode($res);
		}

		if ($type == 'obat_get') {
			$res = $this->m_resep->obat_get();
			echo json_encode($res);
		}

		if ($type == 'delete_data') {
			$farmasi_id = $this->input->post('farmasi_id');
			$this->m_resep->farmasi_delete($farmasi_id);

			$data['farmasi'] = $this->m_resep->farmasi_data();
			$data['nav'] = $this->nav;
			echo json_encode(array(
				'html' => $this->load->view('farmasi/apotek/resep/farmasi_data', $data, true)
			));
		}

		if ($type == 'grand') {
			$resep_id = $this->input->post('resep_id');
			echo json_encode($this->m_resep->get_data($resep_id));
		}
	}

	public function dalam_ajax($type = null, $id = null)
	{
		if ($type == 'get_list') {
			$data = $this->input->post();
			$rinc = $this->m_resep->rinc_dalam($data['resepgroup_id']);
			echo json_encode($rinc);
		}
	}

	public function luar_ajax($type = null, $id = null)
	{
		if ($type == 'get_list') {
			$data = $this->input->post();
			$rinc = $this->m_resep->rinc_luar($data['resepgroup_id']);
			echo json_encode($rinc);
		}
	}

	public function ajax($type = null, $id = null)
	{
		if ($type == 'get_registrasi') {
			$d = $this->input->post();
			$data = $this->m_resep->get_registrasi($d['reg_id']);
			echo json_encode($data);
		}

		if ($type == 'get_grand') {
			$resepgroup_id = $this->input->post('resepgroup_id');
			$res = $this->m_resep->get_data($resepgroup_id);
			echo json_encode($res);
		}

		if ($type == 'update_resep') {
			$data = $this->input->post();
			$this->m_resep->update_resep($data);
		}

		if ($type == 'update_grand') {
			$data = $this->input->post();
			echo json_encode($data);
		}

		if ($type == 'update_telaah') {
			$this->m_resep->update_telaah();
		}
	}

	public function cetak_resep($resep_id)
	{
		$expld_resep_id = explode('-', $resep_id);
		$resep_id = $expld_resep_id[1];
		//generate pdf
		$profile = $this->m_profile->get_first();
		$identitas = $this->m_identitas->get_first();
		$this->load->library('pdf');
		$pdf = new Pdf('p', 'mm', array(105, 330));
		$pdf->AliasNbPages();
		$pdf->SetTitle('Cetak Resep ' . $resep_id);
		$pdf->SetMargins(2, 2, 2);
		$pdf->AddPage();

		$pdf->Image(FCPATH . 'assets/images/icon/pt-cahaya-permata-medika.png', 3, 3, 10, 10);
		$pdf->SetFont('Arial', '', 7);
		$pdf->Cell(13, 1, '', 0, 1, 'L');
		$pdf->Cell(13, 6, '', 0, 0, 'L');
		$pdf->Cell(75, 3, 'PT. CAHAYA PERMATA MEDIKA', 0, 1, 'C');
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(13, 6, '', 0, 0, 'L');
		$pdf->Cell(75, 5, 'RUMAH SAKIT IBU DAN ANAK PERMATA', 0, 1, 'C');
		$pdf->SetFont('Arial', '', 5);
		$pdf->Cell(13, 6, '', 0, 0, 'L');
		$pdf->Cell(75, 2, 'Jl. Mayjen Sutoyo No. 75 Purworejo Telp. (0275) 321031', 0, 1, 'C');
		$pdf->Cell(13, 6, '', 0, 0, 'L');
		$pdf->Cell(75, 2.2, 'Email. rsiapermatapwr@gmail.com', 0, 1, 'C');
		$pdf->Line(3, 16, 102, 16);
		$pdf->SetFont('Arial', '', 12);
		$pdf->Cell(75, 3.5, '', 0, 1, 'C');
		$pdf->Image(FCPATH . 'assets/images/icon/simrs-logo-rs.jpg', 92, 3, 10, 10);

		$pdf->SetFont('Arial', '', 12);
		$pdf->Cell(0, 3.4, 'RESEP OBAT', 0, 1, 'C');
		$pdf->Cell(0, 4, '', 0, 1, 'C');
		$pdf->Cell(70, 2, '', 0, 0, 'C');
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(0, 2, 'Purworejo, ' . to_date(date('Y-m-d'), '', 'date'), 0, 1, 'C');
		$pdf->Cell(0, 5, '', 0, 1, 'C');

		// Resep Non Racik
		$rinc = $this->m_resep->rinc($resep_id);
		foreach ($rinc as $nr) {
			$pdf->SetFont('Arial', '', 10);
			$pdf->Cell(10, 3.4, 'Rx', 0, 0, 'L');
			$pdf->SetFont('Arial', '', 7);
			$pdf->Cell(70, 3.4, $nr['obat_nm'] . ' ' . $nr['aturan_jml'] . ' ' . $nr['satuan_nm'], 0, 0, 'L');
			$pdf->Cell(16, 3.4, to_rome($nr['qty']), 0, 1, 'L');
			if ($nr['obat_alias'] != '') {
				$pdf->Cell(10, 3.4, '', 0, 0, 'L');
				$pdf->Cell(70, 3.4, '(' . $nr['obat_alias'] . ')', 0, 1, 'L');
			}
			$pdf->Cell(10, 3.4, '', 0, 0, 'L');
			$pdf->Cell(70, 3.4, '@ ' . $nr['aturan_waktu'] . ' ' . $nr['waktu_nm'], 0, 1, 'L');
			$pdf->Cell(10, 3.4, '', 0, 0, 'L');
			$pdf->Cell(70, 3.4, $nr['jadwal'], 0, 1, 'L');
			$pdf->Cell(10, 3.4, '', 0, 0, 'L');
			$pdf->Cell(70, 3.4, $nr['aturan_tambahan'], 0, 1, 'L');
			$pdf->Cell(13, 2, '', 0, 1, 'L');
		}

		// Resep Racik
		$rinc_racik = $this->m_resep->rinc_racik($resep_id);
		foreach ($rinc_racik as $r) {
			$pdf->SetFont('Arial', '', 10);
			$pdf->Cell(10, 3.4, 'Rx', 0, 0, 'L');
			$pdf->SetFont('Arial', '', 7);
			$pdf->Cell(70, 3.4, $r['obat_nm'] . ' ' . $r['aturan_jml'] . ' ' . $r['satuan_nm'], 0, 0, 'L');
			$pdf->Cell(16, 3.4, to_rome($r['qty']), 0, 1, 'L');
			if ($r['obat_alias'] != '') {
				$pdf->Cell(10, 3.4, '', 0, 0, 'L');
				$pdf->Cell(70, 3.4, '(' . $r['obat_alias'] . ')', 0, 1, 'L');
			}
			$pdf->Cell(10, 3.4, '', 0, 0, 'L');
			$pdf->Cell(70, 3.4, '@ ' . $r['aturan_waktu'] . ' ' . $r['waktu_nm'], 0, 1, 'L');
			$pdf->Cell(10, 3.4, '', 0, 0, 'L');
			$pdf->Cell(70, 3.4, $r['jadwal'], 0, 1, 'L');
			$pdf->Cell(10, 3.4, '', 0, 0, 'L');
			$pdf->Cell(70, 3.4, $r['aturan_tambahan'], 0, 1, 'L');
			if (count($r['komposisi']) > 0) {
				foreach ($r['komposisi'] as $komposisi) {
					$pdf->Cell(13, 3.4, '', 0, 0, 'L');
					$pdf->Cell(45, 3.4, $komposisi['komposisi'], 0, 0, 'L');
					$pdf->Cell(6, 3.4, $komposisi['komposisi_qty'], 0, 1, 'R');
				}
			}
			$pdf->Cell(13, 2, '', 0, 1, 'L');
		}

		$pdf->Cell(0, 5, '', 0, 1, 'C');
		$main = $this->m_resep->get_data($resep_id);
		if (@$main['sex_cd'] == 'L') {
			$sex_cd = 'Laki-laki';
		} elseif (@$main['sex_cd'] == 'P') {
			$sex_cd = 'Perempuan';
		} else {
			$sex_cd = '';
		}

		$pdf->SetFont('Arial', '', 7);
		$pdf->Cell(19, 3.4, 'NIK', 0, 0, 'L');
		$pdf->Cell(2, 3.4, ':', 0, 0, 'C');
		$pdf->Cell(48, 3.4, @$main['nik'], 0, 0, 'L');
		$pdf->Cell(16, 3.4, 'No. RM', 0, 0, 'L');
		$pdf->Cell(2, 3.4, ':', 0, 0, 'C');
		$pdf->Cell(20, 3.4, @$main['pasien_id'], 0, 1, 'L');
		$pdf->Cell(19, 3.4, 'Nama', 0, 0, 'L');
		$pdf->Cell(2, 3.4, ':', 0, 0, 'C');
		$pdf->Cell(48, 3.4, @$main['sebutan_cd'] . '. ' . @$main['pasien_nm'], 0, 0, 'L');
		$pdf->Cell(16, 3.4, 'Jns. Kelamin', 0, 0, 'L');
		$pdf->Cell(2, 3.4, ':', 0, 0, 'C');
		$pdf->Cell(20, 3.4, @$sex_cd, 0, 1, 'L');
		$pdf->Cell(19, 3.4, 'Tgl. Lahir', 0, 0, 'L');
		$pdf->Cell(2, 3.4, ':', 0, 0, 'C');
		$pdf->Cell(48, 3.4, to_date(@$main['tgl_lahir'], '', 'date'), 0, 1, 'L');
		$pdf->Cell(19, 3.4, 'Ruang/Instalasi', 0, 0, 'L');
		$pdf->Cell(2, 3.4, ':', 0, 0, 'C');
		$pdf->Cell(86, 3.4, @$main['lokasi_layanan'], 0, 1, 'L');
		$pdf->Cell(19, 3.4, 'Nama Dokter', 0, 0, 'L');
		$pdf->Cell(2, 3.4, ':', 0, 0, 'C');
		$pdf->Cell(86, 3.4, @$main['dokter_nm'], 0, 1, 'L');

		// Telaah Resep
		$pdf->Cell(0, 3, '', 0, 1, 'C');
		$align_body_arr = array("L", "C", "C");
		$width_pot_arr = array("83", "9", "9");
		$pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(83, 5, 'Telaah Resep', 1, 0, 'C');
		$pdf->Cell(9, 5, 'Ya', 1, 0, 'C');
		$pdf->Cell(9, 5, 'Tidak', 1, 0, 'C');
		//
		$pdf->SetFont('Arial', '', 7);
		$pdf->Cell(0, 4.9, '', 0, 1);
		//
		$pdf->SetWidths($width_pot_arr);
		$pdf->SetHeights('5');
		$pdf->SetAligns($align_body_arr);

		$pdf->SetDrawColor(0, 0, 0);
		$pdf->SetFillColor(230, 230, 230);
		$pdf->Row(array('   1. Kelengkapan Administratif', '', ''), true);
		$pdf->Row(array('       a. Nama Pasien', (@$main['t_resep_nama'] == 1) ? 'v' : '', (@$main['t_resep_nama'] == -1) ? 'v' : ''), false);
		$pdf->Row(array('       b. Nomer RM', (@$main['t_resep_rm'] == 1) ? 'v' : '', (@$main['t_resep_rm'] == -1) ? 'v' : ''), false);
		$pdf->Row(array('       c. Kejelasan tulisan resep', (@$main['t_resep_kejelasan'] == 1) ? 'v' : '', (@$main['t_resep_kejelasan'] == -1) ? 'v' : ''), false);
		$pdf->SetFillColor(230, 230, 230);
		$pdf->Row(array('   2. Kesesuaian Farmasetik (Cara pemberian, Dosis, & Aturan Pakai)', '', ''), true);
		$pdf->Row(array('       a. Tepat Pasien', (@$main['t_resep_pasien'] == 1) ? 'v' : '', (@$main['t_resep_pasien'] == -1) ? 'v' : ''), false);
		$pdf->Row(array('       b. Tepat Obat/Alkes', (@$main['t_resep_obat'] == 1) ? 'v' : '', (@$main['t_resep_obat'] == -1) ? 'v' : ''), false);
		$pdf->Row(array('       c. Tepat Dosis', (@$main['t_resep_dosis'] == 1) ? 'v' : '', (@$main['t_resep_dosis'] == -1) ? 'v' : ''), false);
		$pdf->Row(array('       d. Tepat Rute', (@$main['t_resep_rute'] == 1) ? 'v' : '', (@$main['t_resep_rute'] == -1) ? 'v' : ''), false);
		$pdf->Row(array('       e. Tepat Waktu', (@$main['t_resep_waktu'] == 1) ? 'v' : '', (@$main['t_resep_waktu'] == -1) ? 'v' : ''), false);
		$pdf->Row(array('       f.  Duplikasi', (@$main['t_resep_duplikasi'] == 1) ? 'v' : '', (@$main['t_resep_duplikasi'] == -1) ? 'v' : ''), false);
		$pdf->SetFillColor(230, 230, 230);
		$pdf->Row(array('   3. Kesesuaian Klinis', '', ''), true);
		$pdf->Row(array('       a. Alergi', (@$main['t_resep_alergi'] == 1) ? 'v' : '', (@$main['t_resep_alergi'] == -1) ? 'v' : ''), false);
		$pdf->Row(array('       b. Interaksi obat dan Kotradik', (@$main['t_resep_interaksi'] == 1) ? 'v' : '', (@$main['t_resep_interaksi'] == -1) ? 'v' : ''), false);
		$pdf->Row(array('       c. Tepat Dosis', (@$main['t_resep_dosis_klinis'] == 1) ? 'v' : '', (@$main['t_resep_dosis_klinis'] == -1) ? 'v' : ''), false);

		// Telaah Obat
		$pdf->SetFillColor(113, 113, 113);
		$pdf->Cell(0, 1, '', 1, 1, 'C', true);
		$align_body_arr = array("L", "C", "C");
		$width_pot_arr = array("83", "9", "9");
		$pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(83, 5, 'Telaah Obat', 1, 0, 'C');
		$pdf->Cell(9, 5, 'Ya', 1, 0, 'C');
		$pdf->Cell(9, 5, 'Tidak', 1, 0, 'C');
		//
		$pdf->SetFont('Arial', '', 7);
		$pdf->Cell(0, 4.9, '', 0, 1);
		//
		$pdf->SetWidths($width_pot_arr);
		$pdf->SetHeights('5');
		$pdf->SetAligns($align_body_arr);

		$pdf->SetDrawColor(0, 0, 0);
		$pdf->Row(array('   1. Obat sesuai resep', (@$main['t_obat_obat'] == 1) ? 'v' : '', (@$main['t_obat_obat'] == -1) ? 'v' : ''), false);
		$pdf->Row(array('   2. Dosis', (@$main['t_obat_dosis'] == 1) ? 'v' : '', (@$main['t_obat_dosis'] == -1) ? 'v' : ''), false);
		$pdf->Row(array('   3. Jumlah', (@$main['t_obat_jumlah'] == 1) ? 'v' : '', (@$main['t_obat_jumlah'] == -1) ? 'v' : ''), false);
		$pdf->Row(array('   4. Frekuensi Pemberian', (@$main['t_obat_frekuensi'] == 1) ? 'v' : '', (@$main['t_obat_frekuensi'] == -1) ? 'v' : ''), false);
		$pdf->Row(array('   5. Rute Obat', (@$main['t_obat_rute'] == 1) ? 'v' : '', (@$main['t_obat_rute'] == -1) ? 'v' : ''), false);

		// Verifikasi Obat
		$pdf->SetFillColor(113, 113, 113);
		$pdf->Cell(0, 1, '', 1, 1, 'C', true);
		$align_body_arr = array("L", "C", "C");
		$width_pot_arr = array("83", "9", "9");
		$pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(101, 5, 'Verifikasi Obat', 1, 1, 'C');
		$pdf->Cell(33.6, 5, 'Tanggal & Jam', 1, 0, 'C');
		$pdf->Cell(33.7, 5, 'Masalah', 1, 0, 'C');
		$pdf->Cell(33.7, 5, 'Hasil Konfirmasi', 1, 1, 'C');
		$getY_1 = $pdf->GetY();
		$pdf->drawTextBox(to_date(@$main['t_tgl_verifikasi'], '', 'full_date'), 33.6, 30, 'C', 'M', true, 0);
		$pdf->drawTextBox(@$main['t_masalah'], 33.7, 30, 'C', 'M', true, 0, $getY_1);
		$pdf->drawTextBox(@$main['t_hasil_konfirmasi'], 33.7, 30, 'C', 'M', true, 1, $getY_1);

		$getY_2 = $getY_1 + 30;
		$pdf->drawTextBox("\n Petugas Farmasi", 50.5, 15, 'C', 'T', true, 0, $getY_2);
		$pdf->drawTextBox("\n Dokter", 50.5, 15, 'C', 'T', true, 1, $getY_2);

		$getY_3 = $getY_2 + 15;
		$pdf->drawTextBox("Nama : " . @$main['apoteker_nm'], 50.5, 7, 'L', 'M', true, 0, $getY_3);
		$pdf->drawTextBox("Nama : " . @$main['dokter_nm'], 50.5, 7, 'L', 'M', true, 1, $getY_3);
		//

		//dokter
		// $pdf->Cell(0, 5.5, '', 0, 1, 'C');
		// $pdf->Cell(50, 5, '', 0, 0, 'C');
		// $pdf->SetFont('Arial', '', 7);
		// $pdf->Cell(0, 5, 'Dokter Penanggungjawab', 0, 1, 'C');
		// $pdf->Cell(0, 6.5, '', 0, 1, 'C');
		// $pdf->Cell(50, 5, '', 0, 0, 'C');
		// $pdf->SetFont('Arial', 'U', 7);
		// $pdf->Cell(0, 5, $main['dokter_nm'], 0, 1, 'C');

		$pdf->Output('I', 'Resep_' . $resep_id . '_' . date('Ymdhis') . '.pdf');
	}

	public function cetak_telaah($resep_id)
	{
		$expld_resep_id = explode('-', $resep_id);
		$resep_id = $expld_resep_id[1];
		//generate pdf
		$profile = $this->m_profile->get_first();
		$identitas = $this->m_identitas->get_first();
		$this->load->library('pdf');
		$pdf = new Pdf('p', 'mm', array(210, 99));
		$pdf->AliasNbPages();
		$pdf->SetTitle('Cetak Resep ' . $resep_id);
		$pdf->SetMargins(2, 2, 2);
		$pdf->AddPage();

		$pdf->Image(FCPATH . 'assets/images/icon/' . $identitas['logo_rumah_sakit'], 3, 3, 10, 10);
		$pdf->SetFont('Arial', '', 7);
		$pdf->Cell(13, 1, '', 0, 1, 'L');
		$pdf->Cell(13, 6, '', 0, 0, 'L');
		$pdf->Cell(0, 3, @$identitas['kepanjangan_unit_kerja'], 0, 1, 'L');
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(13, 6, '', 0, 0, 'L');
		$pdf->Cell(0, 5, @$identitas['rs_tanpa_unit_kerja'], 0, 1, 'L');
		$pdf->SetFont('Arial', '', 6);
		$pdf->Cell(13, 6, '', 0, 0, 'L');
		$pdf->Cell(0, 2, @$identitas['jalan'] . ', ' . ucfirst(strtolower(clear_kab_kota(@$identitas['kabupaten']))) . ', ' . ucfirst(strtolower(@$identitas['propinsi'])), 0, 1, 'L');
		$pdf->Line(3, 15.5, 96, 15.5);
		$pdf->SetFont('Arial', '', 12);
		$pdf->Cell(0, 5, '', 0, 1, 'C');

		$main = $this->m_resep->get_data($resep_id);
		$pdf->SetFont('Arial', '', 7);
		$pdf->Cell(16, 3.4, 'No. RM', 0, 0, 'L');
		$pdf->Cell(2, 3.4, ':', 0, 0, 'C');
		$pdf->Cell(99, 3.4, $main['pasien_id'], 0, 1, 'L');
		$pdf->Cell(16, 3.4, 'Nm. Pasien', 0, 0, 'L');
		$pdf->Cell(2, 3.4, ':', 0, 0, 'C');
		$pdf->Cell(99, 3.4, $main['pasien_nm'], 0, 0, 'L');
		$pdf->Cell(0, 8, '', 0, 1, 'L');

		// Telaah Resep
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(95, 5, "TELAAH RESEP", 0, 1, 'C');
		$align_body_arr = array("L", "C", "C", "L");
		$width_pot_arr = array("46", "9", "9", "31");
		$pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(46, 5, 'Indikator', 1, 0, 'C');
		$pdf->Cell(9, 5, 'Ya', 1, 0, 'C');
		$pdf->Cell(9, 5, 'Tidak', 1, 0, 'C');
		$pdf->Cell(31, 5, "Keterangan", 1, 0, 'C');
		//
		$pdf->SetFont('Arial', '', 7);
		$pdf->Cell(0, 4.9, '', 0, 1);
		//
		$pdf->SetWidths($width_pot_arr);
		$pdf->SetHeights('5');
		$pdf->SetAligns($align_body_arr);

		$pdf->Row(array('Kejelasan tulisan resep', (@$main['t_resep_kejelasan'] == 1) ? 'v' : '', (@$main['t_resep_kejelasan'] == -1) ? 'v' : '', @$main['k_resep_kejelasan']));
		$pdf->Row(array('Kelengkapan administrasi resep', (@$main['t_resep_administrasi'] == 1) ? 'v' : '', (@$main['t_resep_administrasi'] == -1) ? 'v' : '', @$main['k_resep_administrasi']));
		$pdf->Row(array('Kesesuaian persyaratan farmasetis', (@$main['t_resep_farmasetis'] == 1) ? 'v' : '', (@$main['t_resep_farmasetis'] == -1) ? 'v' : '', @$main['k_resep_farmasetis']));
		$pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(95, 5, "Kesesuaian persyaratan klinis :", 1, 1, 'L');
		$pdf->SetFont('Arial', '', 7);
		$pdf->Row(array('Tepat pasien', (@$main['t_resep_pasien'] == 1) ? 'v' : '', (@$main['t_resep_pasien'] == -1) ? 'v' : '', @$main['k_resep_pasien']));
		$pdf->Row(array('Tepat obat', (@$main['t_resep_obat'] == 1) ? 'v' : '', (@$main['t_resep_obat'] == -1) ? 'v' : '', @$main['k_resep_obat']));
		$pdf->Row(array('Tepat dosis', (@$main['t_resep_dosis'] == 1) ? 'v' : '', (@$main['t_resep_dosis'] == -1) ? 'v' : '', @$main['k_resep_dosis']));
		$pdf->Row(array('Tepat rute', (@$main['t_resep_rute'] == 1) ? 'v' : '', (@$main['t_resep_rute'] == -1) ? 'v' : '', @$main['k_resep_rute']));
		$pdf->Row(array('Tepat waktu', (@$main['t_resep_waktu'] == 1) ? 'v' : '', (@$main['t_resep_waktu'] == -1) ? 'v' : '', @$main['k_resep_waktu']));
		$pdf->Row(array('Duplikasi', (@$main['t_resep_duplikasi'] == 1) ? 'v' : '', (@$main['t_resep_duplikasi'] == -1) ? 'v' : '', @$main['k_resep_duplikasi']));
		$pdf->Row(array('Alergi', (@$main['t_resep_alergi'] == 1) ? 'v' : '', (@$main['t_resep_alergi'] == -1) ? 'v' : '', @$main['k_resep_alergi']));
		$pdf->Row(array('Interaksi obat', (@$main['t_resep_interaksi'] == 1) ? 'v' : '', (@$main['t_resep_interaksi'] == -1) ? 'v' : '', @$main['k_resep_interaksi']));
		$pdf->Row(array('Berat badan (pasien anak)', (@$main['t_resep_bb'] == 1) ? 'v' : '', (@$main['t_resep_bb'] == -1) ? 'v' : '', @$main['k_resep_bb']));
		$pdf->Row(array('Tinggi badan (kemoterapi)', (@$main['t_resep_tb'] == 1) ? 'v' : '', (@$main['t_resep_tb'] == -1) ? 'v' : '', @$main['k_resep_tb']));
		$pdf->Row(array('Kontraindikasi lainnya', (@$main['t_resep_kontraindikasi'] == 1) ? 'v' : '', (@$main['t_resep_kontraindikasi'] == -1) ? 'v' : '', @$main['k_resep_kontraindikasi']));

		$pdf->Cell(95, 4, "", 0, 1, 'C');

		// Telaah Obat
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(95, 5, "TELAAH OBAT", 0, 1, 'C');
		$align_body_arr = array("L", "C", "C", "L");
		$width_pot_arr = array("46", "9", "9", "31");
		$pdf->SetFont('Arial', 'B', 7);
		$pdf->Cell(46, 5, 'Indikator', 1, 0, 'C');
		$pdf->Cell(9, 5, 'Ya', 1, 0, 'C');
		$pdf->Cell(9, 5, 'Tidak', 1, 0, 'C');
		$pdf->Cell(31, 5, "Keterangan", 1, 0, 'C');
		//
		$pdf->SetFont('Arial', '', 7);
		$pdf->Cell(0, 4.9, '', 0, 1);
		//
		$pdf->SetWidths($width_pot_arr);
		$pdf->SetHeights('5');
		$pdf->SetAligns($align_body_arr);

		$pdf->Row(array('Nama obat dengan resep', (@$main['t_obat_obat'] == 1) ? 'v' : '', (@$main['t_obat_obat'] == -1) ? 'v' : '', @$main['k_obat_obat']));
		$pdf->Row(array('Jumlah/dosis obat dengan resep', (@$main['t_obat_dosis'] == 1) ? 'v' : '', (@$main['t_obat_dosis'] == -1) ? 'v' : '', @$main['k_obat_dosis']));
		$pdf->Row(array('Rute pemberian obat dengan resep', (@$main['t_obat_rute'] == 1) ? 'v' : '', (@$main['t_obat_rute'] == -1) ? 'v' : '', @$main['k_obat_rute']));
		$pdf->Row(array('Waktu & frekuensi obat dengan resep', (@$main['t_obat_waktu'] == 1) ? 'v' : '', (@$main['t_obat_waktu'] == -1) ? 'v' : '', @$main['k_obat_waktu']));

		//dokter
		$pdf->Cell(0, 5.5, '', 0, 1, 'C');
		$pdf->Cell(50, 5, '', 0, 0, 'C');
		$pdf->SetFont('Arial', '', 7);
		$pdf->Cell(0, 5, 'Apoteker', 0, 1, 'C');
		$pdf->Cell(0, 6.5, '', 0, 1, 'C');
		$pdf->Cell(50, 5, '', 0, 0, 'C');
		$pdf->SetFont('Arial', 'U', 7);
		$pdf->Cell(0, 5, $main['apoteker_nm'], 0, 1, 'C');

		$pdf->Output('I', 'Resep_' . $resep_id . '_' . date('Ymdhis') . '.pdf');
	}

	public function cetak_modal($type = null, $id = null)
	{
		$data['url'] = site_url() . '/farmasi/apotek/resep/' . $type . '/' . $id;

		echo json_encode(array(
			'html' => $this->load->view('farmasi/apotek/resep/cetak_modal', $data, true)
		));
	}

	public function cetak_etiket($resepgroup_id)
	{
		$expld_resep_id = explode('-', $resepgroup_id);
		$resep_id = @$expld_resep_id[1];
		ob_start();
		$data['profile'] = $this->m_profile->get_first();
		$data['identitas'] = $this->m_identitas->get_first();
		$data['main'] = $this->m_resep->get_data($resep_id);
		$data['rinc'] = $this->m_resep->farmasi_rinc($resep_id);
		$this->load->view('farmasi/apotek/resep/etiket_obat', $data);
		$html = ob_get_contents();
		ob_end_clean();

		$html2pdf = new Html2Pdf('P', 'A4', 'en');
		$html2pdf->writeHTML($html);
		$html2pdf->output('Etiket_obat_' . $resep_id . '_' . date('YmdHis') . '_.pdf');
	}

	public function cetak_rincian($resepgroup_id)
	{
		$expld_resep_id = explode('-', $resepgroup_id);
		$resep_id = @$expld_resep_id[1];
		ob_start();
		$data['profile'] = $this->m_profile->get_first();
		$data['identitas'] = $this->m_identitas->get_first();
		$data['main'] = $this->m_resep->get_data($resep_id);
		$data['list_obat'] = $this->m_resep->list_obat($resep_id);
		$this->load->view('farmasi/apotek/resep/rincian_obat', $data);
		$html = ob_get_contents();
		ob_end_clean();

		$html2pdf = new Html2Pdf('P', 'A4', 'en');
		$html2pdf->writeHTML($html);
		$html2pdf->output('Rincian_obat_' . $resep_id . '_' . date('YmdHis') . '_.pdf');
	}

	public function form_resep_non($resep_id, $rincian_id = null)
	{
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save_resep_non/' . $resep_id;
		$data['resep_id'] = $resep_id;
		if ($rincian_id == null) {
			$data['rinc'] = null;
		} else {
			$data['rinc'] = $this->m_resep->rincian_resep_non($rincian_id);
		}
		echo json_encode(array(
			'html' => $this->load->view('farmasi/apotek/resep/form_resep_non', $data, true)
		));
	}

	public function save_resep_non($resep_id = null)
	{
		$this->m_resep->save_resep_non();
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $resep_id);
	}

	public function delete_resep_non($resep_id = null, $rincian_id = null)
	{
		$this->m_resep->delete_resep_non($resep_id, $rincian_id);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $resep_id);
	}

	//Resep Racikan
	public function form_resep_racik($resep_id, $rincian_id = null)
	{
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save_resep_racik/' . $resep_id;
		$data['resep_id'] = $resep_id;
		if ($rincian_id == null) {
			$data['rinc'] = null;
		} else {
			$data['rinc'] = $this->m_resep->rincian_resep_racik($rincian_id);
		}
		echo json_encode(array(
			'html' => $this->load->view('farmasi/apotek/resep/form_resep_racik', $data, true)
		));
	}

	public function save_resep_racik($resep_id = null)
	{
		$this->m_resep->save_resep_racik();
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $resep_id);
	}

	public function delete_resep_racik($resep_id = null, $rincian_id = null)
	{
		$this->m_resep->delete_resep_racik($resep_id, $rincian_id);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $resep_id);
	}

	public function browse($resep_id)
	{
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/new_resep/';
		$data['main'] = $this->m_resep->browse($resep_id);

		echo json_encode(array(
			'html' => $this->load->view('farmasi/apotek/resep/browse_modal', $data, true)
		));
	}
}
