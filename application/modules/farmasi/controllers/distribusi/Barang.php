<?php 
if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Barang extends MY_Controller{

	var $nav_id = '05.02.01', $nav, $cookie;
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array(
			'distribusi/m_barang',
			'master/m_lokasi',
			'm_dt_far_stok',
			'm_dt_far_stok_tidak'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
    $this->cookie = get_cookie_nav($this->nav_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('tgl_order_from' => '', 'tgl_order_to' => '', 'tgl_distribusi_from' => '', 'tgl_distribusi_to' => '', 'lokasi_id' => '', 'distribusi_st' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'distribusi_id','type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}
	
	public function index() {	
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(5, 0);
    $this->cookie['total_rows'] = $this->m_barang->all_rows($this->cookie);
    set_cookie_nav($this->nav_id, $this->cookie);
    //main data
    $data['nav'] = $this->nav;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_barang->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
		$data['lokasi'] = $this->m_lokasi->by_field('is_depo_farmasi', 1, 'result');
    //set pagination
    set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('farmasi/distribusi/barang/index',$data);
	}

	public function form($id=null) {
    $this->authorize($this->nav, ($id != '' ) ? '_update' : '_add');
	
		if($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_barang->get_data($id);
			$data['rinc'] = $this->m_barang->rinc_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['lokasi'] = $this->m_lokasi->by_field('is_depo_farmasi', 1, 'result');
		$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/save/'.$id;
			
		$this->render('farmasi/distribusi/barang/form', $data);
	}
	
	public function save($id = null)
	{
		$this->m_barang->save($id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($id != '' ) ? '_update' : '_add', $this->nav_id);
		redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	public function delete($id = null)
	{
		$this->m_barang->delete($id, true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log('_delete', $this->nav_id);
		redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	public function multiple($type = null)
	{
		$data = $this->input->post();
    if(isset($data['checkitem'])){
      foreach ($data['checkitem'] as $key) {
        switch ($type) {					
					case 'delete':
						$this->authorize($this->nav, '_delete');
            $this->m_barang->delete($key, true);
						$flash = 'Data berhasil dihapus.';
						create_log('_delete', $this->nav_id);
            break;

					case 'enable':
						$this->authorize($this->nav, '_update');
            $this->m_barang->update($key, array('is_active' => 1));
						$flash = 'Data berhasil diaktifkan.';
						create_log('_update', $this->nav_id);
            break;

					case 'disable':
						$this->authorize($this->nav, '_update');
            $this->m_barang->update($key, array('is_active' => 0));
						$flash = 'Data berhasil dinonaktifkan.';
						create_log('_delete', $this->nav_id);
            break;
        }
      }
    }
    create_log($t,$this->this->menu['menu']);
    $this->session->set_flashdata('flash_success', $flash);
    redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	function ajax($type=null, $id=null) {

		if($type == 'autocomplete'){
			$obat_nm = $this->input->get('obat_nm');
			$res = $this->m_barang->obat_autocomplete($obat_nm);
			echo json_encode($res);
		}

		if ($type == 'search_stok') {
			$data['nav'] = $this->nav;
			
			echo json_encode(array(
				'html' => $this->load->view('farmasi/distribusi/barang/search_stok', $data, true)
			));
		}
		
		if ($type == 'search_data') {
			$list = $this->m_dt_far_stok->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['obat_nm'];
				$row[] = $field['no_batch'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = to_date($field['tgl_expired']);
				$row[] = $field['stok_akhir'];
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="stok_fill('."'".$field['stok_id']."'".')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_far_stok->count_all(),
				"recordsFiltered" => $this->m_dt_far_stok->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'search_data_tidak') {
			$list = $this->m_dt_far_stok_tidak->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['obat_nm'];
				$row[] = $field['no_batch'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = to_date($field['tgl_expired']);
				$row[] = $field['stok_akhir'];
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="stok_fill('."'".$field['stok_id']."'".')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_far_stok_tidak->count_all(),
				"recordsFiltered" => $this->m_dt_far_stok_tidak->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}
		
		if($type == 'stok_fill'){
			$data = $this->input->post();
			$res = $this->m_barang->stok_row($data['stok_id']);
			echo json_encode($res);
		}

	}

	
	
}