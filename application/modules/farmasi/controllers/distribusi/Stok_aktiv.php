<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Stok_aktiv extends MY_Controller
{

	var $nav_id = '05.02.02', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'distribusi/m_stok_aktiv',
			'master/m_lokasi',
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id . '.aktiv');
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '', 'tgl_expired_from' => '', 'tgl_expired_to' => '', 'lokasi_id' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'obat_nm', 'type' => 'asc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(5, 0);
		$this->cookie['total_rows'] = $this->m_stok_aktiv->all_rows($this->cookie);
		set_cookie_nav($this->nav_id . '.aktiv', $this->cookie);
		//main data
		$data['nav'] = $this->nav;
		$data['cookie'] = $this->cookie;
		$data['main'] = $this->m_stok_aktiv->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
		$data['lokasi'] = $this->m_lokasi->by_field('is_depo_farmasi', 1, 'result');
		//set pagination
		set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('farmasi/distribusi/stok_aktiv/index', $data);
	}

	public function form_modal($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_stok_aktiv->get_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save/' . $id;
		$data['obat'] = $this->m_stok_aktiv->obat();
		$data['lokasi'] = $this->m_stok_aktiv->lokasi();

		echo json_encode(array(
			'html' => $this->load->view('farmasi/distribusi/stok_aktiv/form_modal', $data, true)
		));
	}

	public function detail($id = null)
	{
		if ($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_stok_aktiv->get_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('farmasi/distribusi/stok_aktiv/detail', $data, true)
		));
	}

	public function save($id = null)
	{
		$this->m_stok_aktiv->save($id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($id != '') ? '_update' : '_add', $this->nav_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	public function ajax_obat($type = null, $id = null)
	{
		if ($type == 'obat_get') {
			$res = $this->m_stok_aktiv->obat_get();
			echo json_encode($res);
		}
	}
}
