<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pengadaan extends MY_Controller
{

	var $nav_id = '05.01.01', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'gudang/m_pengadaan',
			'master/m_supplier',
			'm_dt_obat'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id);
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'order_id', 'type' => 'asc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(5, 0);
		$this->cookie['total_rows'] = $this->m_pengadaan->all_rows($this->cookie);
		set_cookie_nav($this->nav_id, $this->cookie);
		//main data
		$data['nav'] = $this->nav;
		$data['cookie'] = $this->cookie;
		$data['main'] = $this->m_pengadaan->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
		//set pagination
		set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('farmasi/gudang/pengadaan/index', $data);
	}

	public function form($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_pengadaan->get_data($id);
			$data['rinc'] = $this->m_pengadaan->rinc_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['supplier'] = $this->m_supplier->all_data('is_supplier_farmasi', 1);
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save/' . $id;

		$this->render('farmasi/gudang/pengadaan/form', $data);
	}

	public function detail($id = null)
	{
		if ($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_pengadaan->get_data($id);
			$data['rinc'] = $this->m_pengadaan->rinc_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['supplier'] = $this->m_supplier->all_data();

		echo json_encode(array(
			'html' => $this->load->view('farmasi/gudang/pengadaan/detail', $data, true)
		));
	}

	public function save($id = null)
	{
		$this->m_pengadaan->save($id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($id != '') ? '_update' : '_add', $this->nav_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	public function delete($id = null)
	{
		$this->m_pengadaan->delete($id, true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log('_delete', $this->nav_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	public function multiple($type = null)
	{
		$data = $this->input->post();
		if (isset($data['checkitem'])) {
			foreach ($data['checkitem'] as $key) {
				switch ($type) {
					case 'delete':
						$this->authorize($this->nav, '_delete');
						$this->m_pengadaan->delete($key, true);
						$flash = 'Data berhasil dihapus.';
						create_log('_delete', $this->nav_id);
						break;

					case 'enable':
						$this->authorize($this->nav, '_update');
						$this->m_pengadaan->update($key, array('is_active' => 1));
						$flash = 'Data berhasil diaktifkan.';
						create_log('_update', $this->nav_id);
						break;

					case 'disable':
						$this->authorize($this->nav, '_update');
						$this->m_pengadaan->update($key, array('is_active' => 0));
						$flash = 'Data berhasil dinonaktifkan.';
						create_log('_delete', $this->nav_id);
						break;
				}
			}
		}
		create_log($t, $this->this->menu['menu']);
		$this->session->set_flashdata('flash_success', $flash);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	function ajax($type = null, $id = null)
	{

		if ($type == 'autocomplete') {
			$obat_nm = $this->input->get('obat_nm');
			$res = $this->m_pengadaan->obat_autocomplete($obat_nm);
			echo json_encode($res);
		}

		if ($type == 'search_obat') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('farmasi/gudang/pengadaan/search_obat', $data, true)
			));
		}

		if ($type == 'search_data') {
			$list = $this->m_dt_obat->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['no_batch'];
				$row[] = $field['obat_nm'];
				$row[] = $field['satuan_cd'];
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="obat_fill(' . "'" . $field['obat_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_obat->count_all(),
				"recordsFiltered" => $this->m_dt_obat->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'obat_fill') {
			$data = $this->input->post();
			$res = $this->m_pengadaan->obat_row($data['obat_id']);
			echo json_encode($res);
		}
	}
}
