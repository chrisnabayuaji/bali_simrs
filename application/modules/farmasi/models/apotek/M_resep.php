<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_resep extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['tgl_order_from'] != '' && @$cookie['search']['tgl_order_to']) {
      $where .= "AND (DATE(a.tgl_catat) BETWEEN '" . to_date(@$cookie['search']['tgl_order_from']) . "' AND '" . to_date(@$cookie['search']['tgl_order_to']) . "')";
    } else {
      // $where .= "AND DATE(a.tgl_catat) = '" . date('Y-m-d') . "'";
    }
    if (@$cookie['search']['no_rm_nm'] != '') {
      $where .= "AND (b.pasien_id LIKE '%" . $this->db->escape_like_str($cookie['search']['no_rm_nm']) . "%' OR b.pasien_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['no_rm_nm']) . "%')";
    }
    if (@$cookie['search']['lokasi_id'] != '') {
      $where .= "AND c.lokasi_id = '" . $this->db->escape_like_str($cookie['search']['lokasi_id']) . "' ";
    }
    if (@$cookie['search']['lokasi_depo'] != '') {
      $where .= "AND d.lokasi_id = '" . $this->db->escape_like_str($cookie['search']['lokasi_depo']) . "' ";
    }
    if (@$cookie['search']['resepgroup_st'] != '') {
      $where .= "AND a.resepgroup_st = '" . $this->db->escape_like_str($cookie['search']['resepgroup_st']) . "' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
        a.*,
        b.pasien_nm, b.sebutan_cd, b.alamat, b.provinsi, b.kabupaten, b.kecamatan, b.kelurahan,
        b.umur_thn, b.umur_bln, b.umur_hr,
        c.lokasi_nm AS lokasi_pelayanan,
        d.lokasi_nm AS lokasi_depo, 
        e.jenispasien_nm 
      FROM dat_resep a
      JOIN reg_pasien b ON a.reg_id = b.reg_id 
      LEFT JOIN mst_lokasi c ON a.lokasi_id = c.lokasi_id
      LEFT JOIN mst_lokasi d ON c.map_lokasi_depo = d.lokasi_id
      LEFT JOIN mst_jenis_pasien e ON b.jenispasien_id = e.jenispasien_id
      $where AND src_resep_st = 0
      ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    $res = $query->result_array();
    foreach ($res as $key => $value) {
      $res[$key]['check_pendaftaran'] = $this->check_pendaftaran($value['reg_id']);
      $res[$key]['check_poli'] = $this->check_poli($value['reg_id'], $value['lokasi_id']);
      $res[$key]['check_resep_exist'] = $this->check_resep_exist($value['reg_id']);
      $res[$key]['check_resep'] = $this->check_resep($value['reg_id']);
    }
    return $res;
  }

  public function check_resep_exist($reg_id)
  {
    return $this->db->query(
      "SELECT a.* 
      FROM dat_resep a
      WHERE 
        a.reg_id = $reg_id"
    )->row_array();
  }

  public function check_resep($reg_id)
  {
    return $this->db->query(
      "SELECT a.* 
      FROM dat_resep a
      WHERE 
        a.reg_id = $reg_id ORDER BY a.resep_id DESC"
    )->row_array();
  }

  public function check_pendaftaran($reg_id)
  {
    return $this->db->query(
      "SELECT a.* 
      FROM dat_tindakan a
      WHERE 
        a.reg_id = $reg_id AND 
        a.tarif_id LIKE '09.01%'"
    )->row_array();
  }

  public function check_poli($reg_id, $lokasi_id)
  {
    return $this->db->query(
      "SELECT a.* 
      FROM dat_tindakan a
      WHERE 
        a.reg_id = $reg_id AND 
        a.lokasi_id = $lokasi_id AND
        a.tarif_id NOT LIKE '09.01%'"
    )->row_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM dat_resep a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT 
        a.*,
        b.pasien_nm, b.alamat, b.provinsi, b.kabupaten, b.kecamatan, b.kelurahan,
        b.umur_thn, b.umur_bln, b.umur_hr,
        c.lokasi_nm AS lokasi_pelayanan,
        d.lokasi_nm AS lokasi_depo
      FROM dat_resep a
      JOIN reg_pasien b ON a.reg_id = b.reg_id 
      JOIN mst_lokasi c ON a.lokasi_id = c.lokasi_id
      JOIN mst_lokasi d ON c.map_lokasi_depo = d.lokasi_id
      $where AND src_resep_st = 0";
    $query = $this->db->query($sql);
    return $query->num_rows();
  }

  function get_data($id)
  {
    $sql = "SELECT 
          a.*,
          b.pasien_id, b.nik, b.pasien_nm, b.tgl_lahir, b.sex_cd, b.alamat, b.provinsi, b.kabupaten, b.kecamatan, b.kelurahan, b.sebutan_cd, b.tgl_registrasi, b.tgl_pulang, b.jenispasien_id, b.no_kartu, 
          b.umur_thn, b.umur_bln, b.umur_hr, b.sex_cd, b.asalpasien_cd,
          c.lokasi_nm AS lokasi_layanan,
          c.map_lokasi_depo,
          d.jenispasien_nm,
          e.pegawai_nm AS dokter_nm,
          e.spesialisasi_cd,
          g.kelas_nm,
          h.kamar_nm,
          i.pegawai_nm AS apoteker_nm 
        FROM dat_resep a
        JOIN reg_pasien b ON a.reg_id = b.reg_id
        JOIN mst_lokasi c ON a.lokasi_id = c.lokasi_id
        JOIN mst_jenis_pasien d ON b.jenispasien_id = d.jenispasien_id
        JOIN mst_pegawai e ON b.dokter_id = e.pegawai_id
        JOIN mst_lokasi f ON c.map_lokasi_depo = f.lokasi_id
        LEFT JOIN mst_kelas g ON b.kelas_id = g.kelas_id
        LEFT JOIN mst_kamar h ON b.kamar_id = h.kamar_nm
        LEFT JOIN mst_pegawai i ON a.apoteker_id = i.pegawai_id
        WHERE a.resep_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function rinc($id)
  {
    $sql = "SELECT 
        a.*,
        b.parameter_val AS rute_nm,
        c.parameter_val AS satuan_nm,
        d.parameter_val AS waktu_nm
      FROM dat_resep_rinc a 
      INNER JOIN mst_parameter b ON b.parameter_field = 'rute_cd' AND a.rute_cd = b.parameter_cd
      INNER JOIN mst_parameter c ON c.parameter_field = 'satuan_cd' AND a.satuan_cd = c.parameter_cd
      INNER JOIN mst_parameter d ON d.parameter_field = 'waktu_cd' AND a.waktu_cd = d.parameter_cd
      WHERE resep_id = ? AND a.is_racik = 0";
    $query = $this->db->query($sql, array($id));
    $res = $query->result_array();
    return $res;
  }

  public function rinc_racik($id)
  {
    $sql = "SELECT 
        a.*,
        b.parameter_val AS rute_nm,
        c.parameter_val AS satuan_nm,
        d.parameter_val AS waktu_nm,
        e.parameter_val AS sediaan_nm
      FROM dat_resep_rinc a 
      INNER JOIN mst_parameter b ON b.parameter_field = 'rute_cd' AND a.rute_cd = b.parameter_cd
      INNER JOIN mst_parameter c ON c.parameter_field = 'satuan_cd' AND a.satuan_cd = c.parameter_cd
      INNER JOIN mst_parameter d ON d.parameter_field = 'waktu_cd' AND a.waktu_cd = d.parameter_cd
      INNER JOIN mst_parameter e ON e.parameter_field = 'sediaan_cd' AND a.sediaan_cd = e.parameter_cd
      WHERE resep_id = ? AND a.is_racik = 1";
    $query = $this->db->query($sql, array($id));
    $res = $query->result_array();

    foreach ($res as $k => $v) {
      $res[$k]['komposisi'] = $this->db->where('rincian_id', $v['rincian_id'])->get('dat_resep_racik')->result_array();
    }

    return $res;
  }

  public function list_obat($id)
  {
    $sql = "SELECT
              * 
            FROM
              `dat_farmasi` 
            WHERE
              resep_id = ?";
    $query = $this->db->query($sql, array($id));
    $res = $query->result_array();
    return $res;
  }

  public function obat_autocomplete($obat_nm = null, $map_lokasi_depo = null)
  {
    $sql = "SELECT 
              *
            FROM far_stok_depo a 
            WHERE 
              (a.obat_nm LIKE '%$obat_nm%' OR a.obat_id LIKE '%$obat_nm%') AND
              a.stok_st = 1 AND a.stok_akhir > 0 AND a.lokasi_id = '$map_lokasi_depo'
            ORDER BY a.obat_id";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['obat_id'] . "#" . $row['obat_nm'] . "#" . $row['stokdepo_id'],
        'text' => $row['obat_id'] . ' - ' . $row['obat_nm']
      );
    }
    return $res;
  }

  public function obat_row($obat_id = null, $stokdepo_id = null)
  {
    $sql = "SELECT a.* 
            FROM far_stok_depo a
            WHERE a.obat_id=? AND a.stokdepo_id=?";
    $query = $this->db->query($sql, array($obat_id, $stokdepo_id));
    return $query->row_array();
  }

  public function get_alergi($pasien_id)
  {
    return $this->db->where('pasien_id', $pasien_id)->get('dat_catatanmedis')->result_array();
  }

  public function get_diagnosis($reg_id)
  {
    $sql = "SELECT 
              a.diagnosis_id, a.reg_id, a.pasien_id, a.penyakit_id, a.icdx, b.penyakit_nm
            FROM dat_diagnosis a 
            LEFT JOIN mst_penyakit b ON a.penyakit_id=b.penyakit_id
            WHERE a.reg_id=?";
    $query = $this->db->query($sql, array($reg_id));
    $result = $query->result_array();
    return $result;
  }

  public function obat_get()
  {
    $data = $this->input->post();
    $sql = "SELECT 
              *
            FROM far_stok_depo a 
            WHERE 
              stokdepo_id='" . $data['stokdepo_id'] . "'";
    $query = $this->db->query($sql);
    return $query->row_array();
  }

  public function farmasi_data()
  {
    $data = $this->input->post();
    return $this->db->where('resep_id', $data['resep_id'])->get('dat_farmasi')->result_array();
  }

  public function farmasi_rinc($id)
  {
    return $this->db->where('resep_id', $id)->get('dat_farmasi')->result_array();
  }


  public function farmasi_get($id)
  {
    return $this->db->where('farmasi_id', $id)->get('dat_farmasi')->row_array();
  }

  public function update_telaah()
  {
    $data = $this->input->post();
    $this->db->query(
      "UPDATE dat_resep SET " . $data['field'] . " = '" . $data['val'] . "' WHERE resep_id = '" . $data['resep_id'] . "'"
    );
  }

  public function farmasi_save()
  {
    $data = $this->input->post();
    $obat = explode("#", $data['obat_id']);

    if ($data['farmasi_id'] == '') {
      //insert new
      $d = array(
        'farmasi_id' => get_id('dat_farmasi'),
        'resep_id' => $data['resep_id'],
        'reg_id' => $data['reg_id'],
        'dokter_id' => $data['dokter_id'],
        'pasien_id' => $data['pasien_id'],
        'lokasi_id' => $data['lokasi_id'],
        'stokdepo_id' => $obat[2],
        'obat_id' => $obat[0],
        'obat_nm' => $obat[1],
        'harga' => clear_numeric($data['harga']),
        'qty' => clear_numeric($data['qty']),
        'jumlah_awal' => clear_numeric($data['jumlah_awal']),
        'potongan' => clear_numeric($data['potongan']),
        'jumlah_akhir' => clear_numeric($data['jumlah_akhir']),
        'aturan_pakai' => $data['aturan_pakai'],
        'jadwal' => $data['jadwal'],
        'aturan_tambahan' => $data['aturan_tambahan'],
        'tgl_catat' => date('Y-m-d H:i:s'),
        'user_cd' => $this->session->userdata('sess_user_cd'),
        'created_by' => $this->session->userdata('sess_user_realname'),
        'created_at' => date('Y-m-d H:i:s'),
      );

      $this->db->insert('dat_farmasi', $d);
      update_id('dat_farmasi', $d['farmasi_id']);

      //update stok
      $stokdepo = $this->db->where('stokdepo_id', $d['stokdepo_id'])->get('far_stok_depo')->row_array();
      $stok_terpakai = $stokdepo['stok_terpakai'] + $d['qty'];
      $stok_akhir = $stokdepo['stok_awal'] + $stokdepo['stok_penerimaan'] - $stok_terpakai;
      // $stok_akhir = $stokdepo['stok_penerimaan'] - $stok_terpakai - $stokdepo['stok_retur_keluar'];
      $ds = array(
        'stok_terpakai' => $stok_terpakai,
        'stok_akhir' => $stok_akhir
      );
      $this->db->where('stokdepo_id', $d['stokdepo_id'])->update('far_stok_depo', $ds);
      $this->farmasi_grand($d['resep_id']);
    } else {
      //get data
      $farmasi = $this->db->where('farmasi_id', $data['farmasi_id'])->get('dat_farmasi')->row_array();
      $stokdepo = $this->db->where('stokdepo_id', $farmasi['stokdepo_id'])->get('far_stok_depo')->row_array();
      //update stok depo
      $stok_terpakai = $stokdepo['stok_terpakai'] - $farmasi['qty'];
      $stok_akhir = $stokdepo['stok_awal'] + $stokdepo['stok_penerimaan'] - $stok_terpakai;
      // $stok_akhir = $stokdepo['stok_penerimaan'] - $stok_terpakai - $stokdepo['stok_retur_keluar'];
      $ds = array(
        'stok_terpakai' => $stok_terpakai,
        'stok_akhir' => $stok_akhir
      );
      $this->db->where('stokdepo_id', $stokdepo['stokdepo_id'])->update('far_stok_depo', $ds);
      //update data farmasi
      $d = array(
        'resep_id' => $data['resep_id'],
        'reg_id' => $data['reg_id'],
        'dokter_id' => $data['dokter_id'],
        'pasien_id' => $data['pasien_id'],
        'lokasi_id' => $data['lokasi_id'],
        'stokdepo_id' => $obat[2],
        'obat_id' => $obat[0],
        'obat_nm' => $obat[1],
        'harga' => clear_numeric($data['harga']),
        'qty' => clear_numeric($data['qty']),
        'jumlah_awal' => clear_numeric($data['jumlah_awal']),
        'potongan' => clear_numeric($data['potongan']),
        'jumlah_akhir' => clear_numeric($data['jumlah_akhir']),
        'aturan_pakai' => $data['aturan_pakai'],
        'jadwal' => $data['jadwal'],
        'aturan_tambahan' => $data['aturan_tambahan'],
        'tgl_catat' => date('Y-m-d H:i:s'),
        'user_cd' => $this->session->userdata('sess_user_cd'),
        'created_by' => $this->session->userdata('sess_user_realname'),
        'created_at' => date('Y-m-d H:i:s'),
      );
      $this->db->where('farmasi_id', $data['farmasi_id'])->update('dat_farmasi', $d);
      //update stok
      $stokdepo = $this->db->where('stokdepo_id', $d['stokdepo_id'])->get('far_stok_depo')->row_array();
      $stok_terpakai = $stokdepo['stok_terpakai'] + $d['qty'];
      $stok_akhir = $stokdepo['stok_awal'] + $stokdepo['stok_penerimaan'] - $stok_terpakai;
      // $stok_akhir = $stokdepo['stok_penerimaan'] - $stok_terpakai - $stokdepo['stok_retur_keluar'];
      $ds = array(
        'stok_terpakai' => $stok_terpakai,
        'stok_akhir' => $stok_akhir
      );
      $this->db->where('stokdepo_id', $d['stokdepo_id'])->update('far_stok_depo', $ds);
      $this->farmasi_grand($d['resep_id']);
    }
  }

  public function farmasi_delete($farmasi_id)
  {
    //get
    $farmasi = $this->db->where('farmasi_id', $farmasi_id)->get('dat_farmasi')->row_array();
    $stokdepo = $this->db->where('stokdepo_id', $farmasi['stokdepo_id'])->get('far_stok_depo')->row_array();
    //update stok depo
    $stok_terpakai = $stokdepo['stok_terpakai'] - $farmasi['qty'];
    $stok_akhir = $stokdepo['stok_awal'] + $stokdepo['stok_penerimaan'] - $stok_terpakai;
    // $stok_akhir = $stokdepo['stok_penerimaan'] - $stok_terpakai - $stokdepo['stok_retur_keluar'];
    $ds = array(
      'stok_terpakai' => $stok_terpakai,
      'stok_akhir' => $stok_akhir
    );
    $this->db->where('stokdepo_id', $stokdepo['stokdepo_id'])->update('far_stok_depo', $ds);
    //delete farmasi
    $this->db->where('farmasi_id', $farmasi_id)->delete('dat_farmasi');
    $this->farmasi_grand($farmasi['resep_id']);
  }

  public function farmasi_grand($resep_id)
  {
    $farmasi = $this->db->where('resep_id', $resep_id)->get('dat_farmasi')->result_array();
    $resep = $this->db->where('resep_id', $resep_id)->get('dat_resep')->row_array();
    $grand_jml_bruto = 0;
    foreach ($farmasi as $row) {
      $grand_jml_bruto += $row['jumlah_akhir'];
    }
    $grand_nom_ppn = $resep['grand_prs_ppn'] * $grand_jml_bruto;
    $grand_jml_tagihan = $grand_jml_bruto + $grand_nom_ppn + $resep['grand_embalace'] - $resep['grand_jml_potongan'] + $resep['grand_pembulatan'];
    $d = array(
      'grand_jml_bruto' => $grand_jml_bruto,
      'grand_nom_ppn' => $grand_nom_ppn,
      'grand_jml_tagihan' => $grand_jml_tagihan
    );
    $this->db->where('resep_id', $resep_id)->update('dat_resep', $d);
  }

  public function save($id)
  {
    $data = $this->input->post();
    $d = array(
      'grand_jml_bruto' => clear_numeric($data['grand_jml_bruto']),
      'grand_prs_ppn' => clear_numeric($data['grand_prs_ppn']),
      'grand_nom_ppn' => clear_numeric($data['grand_nom_ppn']),
      'grand_embalace' => clear_numeric($data['grand_embalace']),
      'grand_jml_potongan' => clear_numeric($data['grand_jml_potongan']),
      'grand_pembulatan' => clear_numeric($data['grand_pembulatan']),
      'grand_jml_tagihan' => clear_numeric($data['grand_jml_tagihan']),
      'tgl_selesai_resep' => to_date($data['tgl_selesai_resep'], '-', 'full_date'),
      'resep_st' => $data['resep_st'],
      'apoteker_id' => $data['apoteker_id']
    );
    $this->db->where('resep_id', $data['resep_id'])->update('dat_resep', $d);
  }

  public function data_registrasi()
  {
    $data = $this->db->query(
      "SELECT a.*, b.lokasi_nm, c.jenispasien_nm, d.pegawai_nm as dokter_nm
        FROM reg_pasien a
        LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
        LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
        LEFT JOIN mst_pegawai d ON a.dokter_id = d.pegawai_id 
        WHERE a.groupreg_id IS NOT NULL"
    )->result_array();

    return $data;
  }

  public function get_registrasi($id)
  {
    $data = $this->db->query(
      "SELECT a.*, b.lokasi_nm, c.jenispasien_nm, d.pegawai_nm as dokter_nm
        FROM reg_pasien a
        LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
        LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
        LEFT JOIN mst_pegawai d ON a.dokter_id = d.pegawai_id 
        WHERE a.groupreg_id IS NOT NULL AND a.reg_id = '$id'"
    )->row_array();

    return $data;
  }

  public function new_resep($data)
  {
    $reg = $this->db->where('reg_id', $data['reg_id'])->get('reg_pasien')->row_array();
    $dr = array(
      'resep_id' => get_id('dat_resep'),
      'reg_id' => $reg['reg_id'],
      'dokter_id' => $reg['dokter_id'],
      'pasien_id' => $reg['pasien_id'],
      'lokasi_id' => $reg['lokasi_id'],
      'tgl_catat' => to_date($data['tgl_catat'], '-', 'full_date'),
      'created_at' => to_date($data['tgl_catat'], '-', 'full_date'),
      'created_by' => $this->session->userdata('sess_user_realname'),
      'user_cd' => $this->session->userdata('sess_user_cd')
    );
    $this->db->insert('dat_resep', $dr);
    update_id('dat_resep', $dr['resep_id']);

    return $dr['resep_id'];
  }

  public function save_resep_non()
  {
    $data = $this->input->post();
    $resep = $this->db->where('resep_id', $data['resep_id'])->get('dat_resep')->row_array();
    $reg = $this->db->where('reg_id', $resep['reg_id'])->get('reg_pasien')->row_array();
    if ($data['rincian_id'] == '') {
      $detail = array(
        'rincian_id' => get_id('dat_resep_rinc'),
        'resep_id' => $data['resep_id'],
        'reg_id' => $reg['reg_id'],
        'lokasi_id' => $reg['lokasi_id'],
        'pasien_id' => $reg['pasien_id'],
        'obat_nm' => $data['obat_nm'],
        'obat_alias' => $data['obat_alias'],
        'rute_cd' => $data['rute_cd'],
        'qty' => $data['qty'],
        'aturan_jml' => $data['aturan_jml'],
        'satuan_cd' => $data['satuan_cd'],
        'aturan_waktu' => $data['aturan_waktu'],
        'waktu_cd' => $data['waktu_cd'],
        'jadwal' => $data['jadwal'],
        'aturan_tambahan' => $data['aturan_tambahan'],
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $this->session->userdata('sess_user_realname'),
        'tgl_catat' => date('Y-m-d H:i:s'),
        'user_cd' => $this->session->userdata('sess_user_cd')
      );
      $this->db->insert('dat_resep_rinc', $detail);
      update_id('dat_resep_rinc', $detail['rincian_id']);
    } else {
      $detail = array(
        'rincian_id' => $data['rincian_id'],
        'resep_id' => $data['resep_id'],
        'reg_id' => $reg['reg_id'],
        'lokasi_id' => $reg['lokasi_id'],
        'pasien_id' => $reg['pasien_id'],
        'obat_nm' => $data['obat_nm'],
        'obat_alias' => $data['obat_alias'],
        'rute_cd' => $data['rute_cd'],
        'qty' => $data['qty'],
        'aturan_jml' => $data['aturan_jml'],
        'satuan_cd' => $data['satuan_cd'],
        'aturan_waktu' => $data['aturan_waktu'],
        'waktu_cd' => $data['waktu_cd'],
        'jadwal' => $data['jadwal'],
        'aturan_tambahan' => $data['aturan_tambahan'],
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $this->session->userdata('sess_user_realname'),
        'tgl_catat' => date('Y-m-d H:i:s'),
        'user_cd' => $this->session->userdata('sess_user_cd')
      );
      $this->db->where('rincian_id', $data['rincian_id'])->update('dat_resep_rinc', $detail);
    }
  }

  public function rincian_resep_non($rincian_id)
  {
    return $this->db->where('rincian_id', $rincian_id)->get('dat_resep_rinc')->row_array();
  }

  public function delete_resep_non($resep_id, $rincian_id)
  {
    $this->db->where('resep_id', $resep_id)->where('rincian_id', $rincian_id)->delete('dat_resep_rinc');
  }

  public function save_resep_racik($rincian_id)
  {
    $data = $this->input->post();
    $resep = $this->db->where('resep_id', $data['resep_id'])->get('dat_resep')->row_array();
    $reg = $this->db->where('reg_id', $resep['reg_id'])->get('reg_pasien')->row_array();
    // save to dat_resep_rinc -> obat racikan
    if ($data['rincian_id'] == '') {
      $detail = array(
        'rincian_id' => get_id('dat_resep_rinc'),
        'resep_id' => $data['resep_id'],
        'reg_id' => $reg['reg_id'],
        'lokasi_id' => $reg['lokasi_id'],
        'is_racik' => 1,
        'pasien_id' => $data['pasien_id'],
        'obat_nm' => $data['obat_nm'],
        'sediaan_cd' => $data['sediaan_cd'],
        'rute_cd' => $data['rute_cd'],
        'qty' => $data['qty'],
        'aturan_jml' => $data['aturan_jml'],
        'satuan_cd' => $data['satuan_cd'],
        'aturan_waktu' => $data['aturan_waktu'],
        'waktu_cd' => $data['waktu_cd'],
        'jadwal' => $data['jadwal'],
        'aturan_tambahan' => $data['aturan_tambahan'],
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $this->session->userdata('sess_user_realname'),
        'tgl_catat' => date('Y-m-d H:i:s'),
        'user_cd' => $this->session->userdata('sess_user_cd')
      );
      $this->db->insert('dat_resep_rinc', $detail);
      update_id('dat_resep_rinc', $detail['rincian_id']);

      //insert to data racikan
      foreach ($data['komposisi'] as $key => $val) {
        $racik = array(
          'komposisi_id' => get_id('dat_resep_racik'),
          'rincian_id' => $detail['rincian_id'],
          'komposisi' => $data['komposisi'][$key],
          'komposisi_alias' => $data['komposisi_alias'][$key],
          'komposisi_qty' => $data['komposisi_qty'][$key],
        );
        $this->db->insert('dat_resep_racik', $racik);
        update_id('dat_resep_racik', $racik['komposisi_id']);
      }
    } else {
      $detail = array(
        'rincian_id' => $data['rincian_id'],
        'resep_id' => $data['resep_id'],
        'reg_id' => $reg['reg_id'],
        'lokasi_id' => $reg['lokasi_id'],
        'is_racik' => 1,
        'pasien_id' => $data['pasien_id'],
        'obat_nm' => $data['obat_nm'],
        'sediaan_cd' => $data['sediaan_cd'],
        'rute_cd' => $data['rute_cd'],
        'qty' => $data['qty'],
        'aturan_jml' => $data['aturan_jml'],
        'satuan_cd' => $data['satuan_cd'],
        'aturan_waktu' => $data['aturan_waktu'],
        'waktu_cd' => $data['waktu_cd'],
        'jadwal' => $data['jadwal'],
        'aturan_tambahan' => $data['aturan_tambahan'],
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $this->session->userdata('sess_user_realname'),
        'tgl_catat' => date('Y-m-d H:i:s'),
        'user_cd' => $this->session->userdata('sess_user_cd')
      );
      $this->db->where('rincian_id', $data['rincian_id'])->update('dat_resep_rinc', $detail);
      $this->db->where('rincian_id', $data['rincian_id'])->delete('dat_resep_racik');
      //insert to data racikan
      foreach ($data['komposisi'] as $key => $val) {
        $racik = array(
          'komposisi_id' => get_id('dat_resep_racik'),
          'rincian_id' => $detail['rincian_id'],
          'komposisi' => $data['komposisi'][$key],
          'komposisi_alias' => $data['komposisi_alias'][$key],
          'komposisi_qty' => $data['komposisi_qty'][$key],
        );
        $this->db->insert('dat_resep_racik', $racik);
        update_id('dat_resep_racik', $racik['komposisi_id']);
      }
    }
  }

  public function delete_resep_racik($resep_id, $rincian_id)
  {
    $this->db->where('rincian_id', $rincian_id)->delete('dat_resep_racik');
    $this->db->where('resep_id', $resep_id)->where('rincian_id', $rincian_id)->delete('dat_resep_rinc');
  }

  public function rincian_resep_racik($rincian_id)
  {
    $res = $this->db->where('rincian_id', $rincian_id)->get('dat_resep_rinc')->row_array();
    $res['racik'] = $this->db->where('rincian_id', $rincian_id)->get('dat_resep_racik')->result_array();
    return $res;
  }

  public function browse($resep_id)
  {
    return $this->db->query("SELECT 
      a.*,
      b.pasien_nm, b.alamat, b.provinsi, b.kabupaten, b.kecamatan, b.kelurahan,
      b.umur_thn, b.umur_bln, b.umur_hr,
      c.lokasi_nm AS lokasi_pelayanan,
      d.lokasi_nm AS lokasi_depo
    FROM dat_resep a
    JOIN reg_pasien b ON a.reg_id = b.reg_id 
    LEFT JOIN mst_lokasi c ON a.lokasi_id = c.lokasi_id
    LEFT JOIN mst_lokasi d ON c.map_lokasi_depo = d.lokasi_id
    WHERE a.resep_id != '$resep_id' AND a.resep_st = 0")->result_array();
  }
}
