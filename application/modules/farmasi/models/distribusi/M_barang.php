<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_barang extends CI_Model {

	public function where($cookie)
	{
		$where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['tgl_order_from'] != '' && @$cookie['search']['tgl_order_to']) {
      $where .= "AND (DATE(a.tgl_order_depo) BETWEEN '".to_date(@$cookie['search']['tgl_order_from'])."' AND '".to_date(@$cookie['search']['tgl_order_to'])."')";
    }
    if (@$cookie['search']['tgl_distribusi_from'] != '' && @$cookie['search']['tgl_distribusi_to']) {
      $where .= "AND (DATE(a.tgl_distribusi) BETWEEN '".to_date(@$cookie['search']['tgl_distribusi_from'])."' AND '".to_date(@$cookie['search']['tgl_distribusi_to'])."')";
    }
    if (@$cookie['search']['lokasi_id'] != '') {
      $where .= "AND a.lokasi_id = '".$this->db->escape_like_str($cookie['search']['lokasi_id'])."' ";
    }
    if (@$cookie['search']['distribusi_st'] != '') {
      $where .= "AND a.distribusi_st = '".$this->db->escape_like_str($cookie['search']['distribusi_st'])."' ";
    }
		return $where;
	}

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT a.*, b.lokasi_nm 
            FROM far_distribusi a
            LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
            $where
            ORDER BY "
              .$cookie['order']['field']." ".$cookie['order']['type'].
            " LIMIT ".$cookie['cur_page'].",".$cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM far_distribusi a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM far_distribusi a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id) {
    $sql = "SELECT 
              a.* 
            FROM far_distribusi a
            WHERE a.distribusi_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function rinc_data($id)
  {
    $sql = "SELECT a.*, b.parameter_val AS satuan 
    FROM far_stok_depo a
    LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
    WHERE distribusi_id=?
    ORDER BY stokdepo_id ASC";
    $query = $this->db->query($sql, array($id));
    $res = $query->result_array();
    return $res;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());

    // save far_distribusi
    $far_distribusi['lokasi_id'] = $data['lokasi_id'];
    $far_distribusi['no_distribusi'] = $data['no_distribusi'];
    $far_distribusi['tgl_order_depo'] = to_date($data['tgl_order_depo'], '', 'full_date');
    $far_distribusi['keterangan_order_depo'] = $data['keterangan_order_depo'];
    $far_distribusi['tgl_distribusi'] = to_date($data['tgl_distribusi'], '', 'full_date');
    $far_distribusi['keterangan_distribusi'] = $data['keterangan_distribusi'];
    $far_distribusi['distribusi_st'] = $data['distribusi_st'];
    $far_distribusi['user_cd'] = $this->session->userdata('sess_user_cd');
    if ($id == null) {
      $far_distribusi['distribusi_id'] = get_id('far_distribusi');
      $far_distribusi['created_at'] = date('Y-m-d H:i:s');
      $far_distribusi['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('far_distribusi', $far_distribusi);
      update_id('far_distribusi', $far_distribusi['distribusi_id']);
    }else{
      $far_distribusi['updated_at'] = date('Y-m-d H:i:s');
      $far_distribusi['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('distribusi_id', $id)->update('far_distribusi', $far_distribusi);
    }

    // save far_stok_depo
    if (isset($data['stok_id'])) {
      foreach ($data['stok_id'] as $key => $val) {
        $stok = $this->db->where('stok_id', $val)->get('far_stok')->row_array();

        if (@$far_distribusi['distribusi_id'] !='') {
          $far_stok_depo['distribusi_id'] = $far_distribusi['distribusi_id'];
        }else{
          $far_stok_depo['distribusi_id'] = $data['distribusi_id'];
        }
        $far_stok_depo['stok_id'] = $val;
        $far_stok_depo['lokasi_id'] = $data['lokasi_id'];
        $far_stok_depo['obat_id'] = $stok['obat_id'];
        $far_stok_depo['obat_nm'] = $stok['obat_nm'];
        $far_stok_depo['no_batch'] = $stok['no_batch'];
        $far_stok_depo['satuan_cd'] = $stok['satuan_cd'];
        $far_stok_depo['tgl_expired'] = $stok['tgl_expired'];
        $far_stok_depo['harga_beli'] = $stok['harga_beli'];
        $far_stok_depo['harga_jual'] = $stok['harga_jual'];
        $far_stok_depo['harga_akhir'] = $stok['harga_akhir'];
        $far_stok_depo['jenisbarang_cd'] = $stok['jenisbarang_cd'];
        $far_stok_depo['keterangan_obat'] = $data['keterangan_obat'][$key];
        $far_stok_depo['is_obat_indikator'] = $stok['is_obat_indikator'];
        $far_stok_depo['is_napza'] = $stok['is_napza'];
        $far_stok_depo['is_generik'] = $stok['is_generik'];
        $far_stok_depo['is_antibiotik'] = $stok['is_antibiotik'];
        $far_stok_depo['is_injeksi'] = $stok['is_injeksi'];
        $far_stok_depo['stok_awal'] = $stok['stok_awal'];
        $far_stok_depo['stok_penerimaan'] = $data['qty'][$key];
        // $far_stok_depo['stok_terpakai'] = $stok['stok_terpakai'];
        $far_stok_depo['stok_retur_keluar'] = $stok['stok_retur_keluar'];
        $far_stok_depo['stok_akhir'] = $data['qty'][$key];
        $far_stok_depo['stok_opname'] = $stok['stok_opname'];
        $far_stok_depo['tgl_catat'] = date('Y-m-d H:i:s');
        $far_stok_depo['stok_st'] = $data['distribusi_st'];
        if ($data['stokdepo_id'][$key] == null) {
          $far_stok_depo['stokdepo_id'] = get_id('far_stok_depo');
          $far_stok_depo['created_at'] = date('Y-m-d H:i:s');
          $far_stok_depo['created_by'] = $this->session->userdata('sess_user_realname');
          $this->db->insert('far_stok_depo', $far_stok_depo);
          update_id('far_stok_depo', $far_stok_depo['stokdepo_id']);
        }else{
          $far_stok_depo['updated_at'] = date('Y-m-d H:i:s');
          $far_stok_depo['updated_by'] = $this->session->userdata('sess_user_realname');
          $this->db->where('stokdepo_id', $data['stokdepo_id'][$key])->update('far_stok_depo', $far_stok_depo);
        }

        // update far_stok
        if ($data['stokdepo_id'][$key] == null) {
          $sql = "UPDATE far_stok
                  SET stok_distribusi=ifnull(stok_distribusi,0) + '".$data['qty'][$key]."', stok_akhir=ifnull(stok_akhir,0) - '".$data['qty'][$key]."'
                  WHERE stok_id='".$val."'";               
          $query = $this->db->query($sql);
        }else{
          $sql = "UPDATE far_stok
                  SET stok_distribusi=ifnull(stok_distribusi,0) - ".$data['qty_sebelum_edit'][$key]." + '".$data['qty'][$key]."', stok_akhir=ifnull(stok_akhir,0) + ".$data['qty_sebelum_edit'][$key]." - '".$data['qty'][$key]."'
                  WHERE stok_id='".$val."'";     
          $query = $this->db->query($sql);
        }

      }
    }

    if (isset($data['delete_id'])) {
      foreach ($data['delete_id'] as $key => $val) {
        $this->db->where('stokdepo_id', $data['delete_id'][$key])->delete('far_stok_depo');
      }
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('distribusi_id',$id)->update('far_distribusi', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('distribusi_id', $id)->delete('far_distribusi');
      $this->db->where('distribusi_id', $id)->delete('far_distribusi_rinc');
    }else{
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('distribusi_id', $id)->update('far_distribusi', $data);
      $this->db->where('distribusi_id', $id)->update('far_distribusi_rinc', $data);
    }
  }

  public function obat_autocomplete($obat_nm=null) {
    $sql = "SELECT 
              a.*, b.parameter_val AS satuan
            FROM far_stok a
            LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
            WHERE (a.obat_nm LIKE '%$obat_nm%' OR a.obat_id LIKE '%$obat_nm%')
            ORDER BY a.obat_id";                
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['obat_id']."#".$row['obat_nm'].'#'.$row['satuan'].'#'.$row['stok_id'],
        'text' => $row['obat_id'].' - '.$row['obat_nm']
      );
    }
    return $res;
  }

  public function stok_row($id)
  {
    $sql = "SELECT 
              a.*, b.parameter_val AS satuan 
            FROM far_stok a 
            LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
            WHERE a.stok_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }
  
}