<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_stok_aktiv extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 AND stok_st = 1 ";
    if (@$cookie['search']['tgl_expired_from'] != '' && @$cookie['search']['tgl_expired_to']) {
      $where .= "AND (DATE(a.tgl_expired) BETWEEN '" . to_date(@$cookie['search']['tgl_expired_from']) . "' AND '" . to_date(@$cookie['search']['tgl_expired_to']) . "')";
    }
    if (@$cookie['search']['term'] != '') {
      $where .= "AND (a.obat_id LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' OR a.obat_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' ) ";
    }
    if (@$cookie['search']['lokasi_id'] != '') {
      $where .= "AND a.lokasi_id = '" . $this->db->escape_like_str($cookie['search']['lokasi_id']) . "' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT a.*, b.parameter_val as satuan_nm, c.lokasi_nm 
      FROM far_stok_depo a
      LEFT JOIN mst_parameter b ON a.satuan_cd = b.parameter_cd AND b.parameter_field = 'satuan_cd'
      LEFT JOIN mst_lokasi c ON a.lokasi_id = c.lokasi_id
      $where
      ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM far_stok_depo a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function obat()
  {
    return $this->db->query("SELECT * FROM mst_obat")->result_array();
  }

  public function lokasi()
  {
    return $this->db->query("SELECT * FROM mst_lokasi WHERE is_depo_farmasi = 1")->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM far_stok_depo a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id)
  {
    $sql = "SELECT a.*, b.parameter_val as satuan_nm, c.lokasi_nm  
            FROM far_stok_depo a
            LEFT JOIN mst_parameter b ON a.satuan_cd = b.parameter_cd AND b.parameter_field = 'satuan_cd'
            LEFT JOIN mst_lokasi c ON a.lokasi_id = c.lokasi_id
            WHERE a.stokdepo_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function rinc_data($id)
  {
    $sql = "SELECT a.*, b.parameter_val AS satuan 
    FROM far_stok_depo_rinc a
    LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
    WHERE stok_id=?";
    $query = $this->db->query($sql, array($id));
    $res = $query->result_array();
    return $res;
  }

  public function rincian_stok_data($order_id)
  {
    $sql = "SELECT a.*, b.parameter_val AS satuan  
    FROM far_stok_depo_rinc a
    LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
    WHERE a.order_id=?";
    $query = $this->db->query($sql, $order_id);
    $res = $query->result_array();
    return $res;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    $data['tgl_expired'] = to_date($data['tgl_expired']);
    if ($id == null) {
      $data['stokdepo_id'] = get_id('far_stok_depo');

      $obat = $this->db->where('obat_id', $data['obat_id'])->get('mst_obat')->row_array();
      $data['obat_nm'] = $obat['obat_nm'];
      $data['satuan_cd'] = $obat['satuan_cd'];
      $data['jenisbarang_cd'] = $obat['jenisbarang_cd'];

      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      update_id('far_stok_depo', $data['stokdepo_id']);
      $this->db->insert('far_stok_depo', $data);
    } else {
      // get far_stok_depo
      // $get_far_stok_depo = $this->db->where('stokdepo_id', $id)->get('far_stok_depo')->row_array();
      // $data['stok_akhir'] = @$data['stok_awal'] + @$get_far_stok_depo['stok_penerimaan'] - @$get_far_stok_depo['stok_distribusi'] + @$get_far_stok_depo['stok_retur_masuk'] - @$get_far_stok_depo['stok_retur_keluar'] - @$get_far_stok_depo['stok_musnah'];
      $obat = $this->db->where('obat_id', $data['obat_id'])->get('mst_obat')->row_array();
      $data['satuan_cd'] = $obat['satuan_cd'];
      $data['jenisbarang_cd'] = $obat['jenisbarang_cd'];

      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('stokdepo_id', $id)->update('far_stok_depo', $data);
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('stokdepo_id', $id)->update('far_stok_depo', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('stokdepo_id', $id)->delete('far_stok_depo');
      $this->db->where('stokdepo_id', $id)->delete('far_stok_depo_rinc');
    } else {
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('stokdepo_id', $id)->update('far_stok_depo', $data);
      $this->db->where('stokdepo_id', $id)->update('far_stok_depo_rinc', $data);
    }
  }

  public function obat_autocomplete($obat_nm = null)
  {
    $sql = "SELECT 
              a.*, b.parameter_val AS satuan
            FROM mst_obat a
            LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
            WHERE (a.obat_nm LIKE '%$obat_nm%' OR a.obat_id LIKE '%$obat_nm%')
            ORDER BY a.obat_id";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['obat_id'] . "#" . $row['obat_nm'] . '#' . $row['satuan'],
        'text' => $row['obat_id'] . ' - ' . $row['obat_nm']
      );
    }
    return $res;
  }

  public function obat_row($id)
  {
    $sql = "SELECT * FROM mst_obat WHERE obat_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  //surat pesanan
  public function surat_pesanan_row($id)
  {
    $sql = "SELECT * FROM far_stok_depo WHERE order_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  public function obat_get()
  {
    $data = $this->input->post();
    $sql = "SELECT 
              a.*, b.parameter_val AS satuan_nm 
            FROM mst_obat a
            LEFT JOIN mst_parameter b ON a.satuan_cd = b.parameter_cd AND b.parameter_nm = 'SATUAN' AND b.parameter_field = 'satuan_cd'
            WHERE 
              a.obat_id='" . $data['obat_id'] . "'";
    $query = $this->db->query($sql);
    return $query->row_array();
  }
}
