<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pengadaan extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND a.pengadaan_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT a.*, b.supplier_nm FROM far_order a
      LEFT JOIN mst_supplier b ON a.supplier_id = b.supplier_id 
      $where
      ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM far_order a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM far_order a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id)
  {
    $sql = "SELECT 
              a.*, b.supplier_nm 
            FROM far_order a
            LEFT JOIN mst_supplier b ON a.supplier_id = b.supplier_id 
            WHERE a.order_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function rinc_data($id)
  {
    $sql = "SELECT a.*, b.parameter_val AS satuan_nm 
    FROM far_order_rinc a
    LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
    WHERE order_id=?
    ORDER BY orderrincian_id ASC";
    $query = $this->db->query($sql, array($id));
    $res = $query->result_array();
    return $res;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    $d = $data;
    unset($data['orderrincian_id'], $data['obat_id'], $data['qty'], $data['satuan_cd'], $data['keteranganorder_rinc'], $data['status'], $data['delete_id']);
    $data['tgl_order'] = to_date($data['tgl_order'], '', 'date', '');
    $data['user_cd'] = $this->session->userdata('sess_user_cd');
    if ($id == null) {
      $data['order_id'] = get_id('far_order');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('far_order', $data);

      if (isset($d['obat_id'])) {
        foreach ($d['obat_id'] as $k => $v) {
          $obat = $this->db->where('obat_id', $v)->get('mst_obat')->row_array();
          $d_obat = array(
            'orderrincian_id' => get_id('far_order_rinc'),
            'order_id' => $data['order_id'],
            'obat_id' => $obat['obat_id'],
            'obat_nm' => $obat['obat_nm'],
            'satuan_cd' => $d['satuan_cd'][$k],
            'qty' => $d['qty'][$k],
            'keteranganorder_rinc' => $d['keteranganorder_rinc'][$k],
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->session->userdata('sess_user_realname')
          );
          $this->db->insert('far_order_rinc', $d_obat);
          update_id('far_order_rinc', $d_obat['orderrincian_id']);
        }
      }

      update_id('far_order', $data['order_id']);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      if (isset($d['obat_id'])) {
        foreach ($d['obat_id'] as $k => $v) {
          $obat = $this->db->where('obat_id', $v)->get('mst_obat')->row_array();
          $d_obat = array(
            'order_id' => $id,
            'obat_id' => $obat['obat_id'],
            'obat_nm' => $obat['obat_nm'],
            'satuan_cd' => $d['satuan_cd'][$k],
            'qty' => $d['qty'][$k],
            'keteranganorder_rinc' => $d['keteranganorder_rinc'][$k]
          );

          if ($d['status'][$k] == 'new') {
            $d_obat['orderrincian_id'] = get_id('far_order_rinc');
            $d_obat['created_at'] = date('Y-m-d H:i:s');
            $d_obat['created_by'] = $this->session->userdata('sess_user_realname');
            $this->db->insert('far_order_rinc', $d_obat);
            update_id('far_order_rinc', $d_obat['orderrincian_id']);
          }

          if ($d['status'][$k] == 'edit_array') {
            $d_obat['orderrincian_id'] = get_id('far_order_rinc');
            $d_obat['created_at'] = date('Y-m-d H:i:s');
            $d_obat['created_by'] = $this->session->userdata('sess_user_realname');
            $this->db->insert('far_order_rinc', $d_obat);
            update_id('far_order_rinc', $d_obat['orderrincian_id']);
          }

          if ($d['status'][$k] == 'edit') {
            $d_obat['updated_at'] = date('Y-m-d H:i:s');
            $d_obat['updated_by'] = $this->session->userdata('sess_user_realname');
            $this->db->where('orderrincian_id', $d['orderrincian_id'][$k])->update('far_order_rinc', $d_obat);
          }
        }
      }

      if (isset($d['delete_id'])) {
        foreach ($d['delete_id'] as $k => $v) {
          $this->db->where('orderrincian_id', $d['delete_id'][$k])->delete('far_order_rinc');
        }
      }
      $this->db->where('order_id', $id)->update('far_order', $data);
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('order_id', $id)->update('far_order', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('order_id', $id)->delete('far_order');
      $this->db->where('order_id', $id)->delete('far_order_rinc');
    } else {
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('order_id', $id)->update('far_order', $data);
      $this->db->where('order_id', $id)->update('far_order_rinc', $data);
    }
  }

  public function obat_autocomplete($obat_nm = null)
  {
    $sql = "SELECT 
              a.*, b.parameter_val AS satuan
            FROM mst_obat a
            LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
            WHERE (a.obat_nm LIKE '%$obat_nm%' OR a.obat_id LIKE '%$obat_nm%')
            ORDER BY a.obat_id";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['obat_id'] . "#" . $row['obat_nm'] . '#' . $row['satuan'],
        'text' => $row['obat_id'] . ' - ' . $row['obat_nm']
      );
    }
    return $res;
  }

  public function obat_row($id)
  {
    $sql = "SELECT 
              a.*, b.parameter_val AS satuan 
            FROM mst_obat a 
            LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
            WHERE a.obat_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }
}
