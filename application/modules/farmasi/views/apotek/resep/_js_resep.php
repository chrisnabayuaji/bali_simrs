<script>
  var resep_form;
  $(document).ready(() => {
    var resep_form = $("#resep_form").validate({
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-n2 mb-2');
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-n2 mb-2');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      }
    });
    $("#submit_resep").on('click', () => {
      $("#resep_form").submit();
    })

    $('#t_tgl_verifikasi').daterangepicker({
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      opens: 'top',
      isInvalidDate: function(date) {
        return '';
      }
    }).change(function() {
      var tgl = $('#t_tgl_verifikasi').val();
      tgl = to_date(tgl,'-','full_date');
      update_telaah(tgl, 't_tgl_verifikasi');
    });
  });

  function telaah(o, field) {
    update_telaah(o.val(), field)
  }

  function update_telaah(val, field) {
    var resep_id = $("#resep_id").val();
    var reg_id = $("#reg_id").val();
    $.ajax({
      type: 'post',
      url: '<?= site_url($nav['nav_url'] . '/ajax/update_telaah') ?>',
      data: 'resep_id=' + resep_id + '&val=' + val + '&field=' + field,
      success: function(data) {

      }
    })
  }

  function allResep(o) {
    if (o.val() == 1) {
      $('.resep-ya').prop('checked', true).trigger('change');
    } else {
      $('.resep-tidak').prop('checked', true).trigger('change');
    }
  }

  function allObat(o) {
    if (o.val() == 1) {
      $('.obat-ya').prop('checked', true).trigger('change');
    } else {
      $('.obat-tidak').prop('checked', true).trigger('change');
    }
  }

  function farmasi_data(resep_id) {
    $('#farmasi_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_farmasi/data' ?>', {
      resep_id: resep_id
    }, function(data) {
      $('#farmasi_data').html(data.html);
    }, 'json');
  }

  $(document).ready(function() {
    var resep_id = $("#resep_id").val();
    farmasi_data(resep_id);

    $(".autonumeric").autoNumeric({
      aSep: ".",
      aDec: ",",
      vMax: "999999999999999",
      vMin: "0"
    });

    // get_grand();
  })

  function get_grand() {
    var resep_id = $("#resep_id").val();
    $.ajax({
      type: 'post',
      url: '<?= site_url() . '/' . $nav['nav_url'] . '/ajax_farmasi/grand' ?>',
      data: 'resep_id=' + resep_id,
      dataType: 'json',
      success: (data) => {
        $("#grand_jml_bruto").val(num_id(data.grand_jml_bruto));
        // $("#grand_prs_ppn").val(num_id(data.grand_prs_ppn));
        // $("#grand_nom_ppn").val(num_id(data.grand_nom_ppn));
        // $("#grand_nom_ppn").val(num_id(data.grand_nom_ppn));
        // $("#grand_embalace").val(num_id(data.grand_embalace));
        // $("#grand_jml_potongan").val(num_id(data.grand_jml_potongan));
        // $("#grand_pembulatan").val(num_id(data.grand_pembulatan));
        // $("#grand_jml_tagihan").val(num_id(data.grand_jml_tagihan));
        hitung_grand();
      }
    });
  }

  function hitung_grand() {
    var grand_jml_bruto = 0;
    if ($("#grand_jml_bruto").val() != '') {
      grand_jml_bruto = parseInt(num_sys($("#grand_jml_bruto").val()));
    }
    var grand_prs_ppn = 0;
    if ($("#grand_prs_ppn").val() != '') {
      grand_prs_ppn = parseInt(num_sys($("#grand_prs_ppn").val()));
    }
    var nom_ppn = grand_jml_bruto * grand_prs_ppn / 100;
    $("#grand_nom_ppn").val(num_id(nom_ppn));
    var grand_nom_ppn = 0;
    if ($("#grand_nom_ppn").val() != '') {
      grand_nom_ppn = parseInt(num_sys($("#grand_nom_ppn").val()));
    }
    var grand_embalace = 0;
    if ($("#grand_embalace").val() != '') {
      grand_embalace = parseInt(num_sys($("#grand_embalace").val()));
    }
    var grand_jml_potongan = 0;
    if ($("#grand_jml_potongan").val() != '') {
      grand_jml_potongan = parseInt(num_sys($("#grand_jml_potongan").val()));
    }
    var grand_pembulatan = 0;
    if ($("#grand_pembulatan").val() != '') {
      grand_pembulatan = parseInt(num_sys($("#grand_pembulatan").val()));
    }
    var jml_tagihan = grand_jml_bruto + grand_nom_ppn + grand_embalace - grand_jml_potongan + grand_pembulatan;
    $("#grand_jml_tagihan").val(num_id(jml_tagihan));
  }
</script>