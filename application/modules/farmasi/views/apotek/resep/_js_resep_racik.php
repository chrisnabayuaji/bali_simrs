<script type="text/javascript">
  $(document).ready(function() {
    var racik_form = $("#form-resep-racik").validate({
      rules: {
        resep_qty: {
          number: true
        }
      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      }
    });

    $("#table_komposisi").on("click", ".btn-delete-komposisi", function() {
      $(this).closest("tr").remove();
    });

    $("#btn_add_komposisi").on('click', () => {
      var data = {
        komposisi : $("#komposisi").val(),
        komposisi_alias : $("#komposisi_alias").val(),
        komposisi_qty : $("#komposisi_qty").val()
      };
      var alias = '';
      if (data.komposisi_alias != '') {
        alias = ' (' + data.komposisi_alias + ')';
      }
      var html = '<tr>' +
        '<td class="text-center">' +
        '<button type="button" class="btn btn-danger btn-table btn-delete-komposisi"><i class="fas fa-trash-alt"></i></button>' +
        '<input type="hidden" name="komposisi_id[]" value="">' +
        '</td>' +
        '<td>' +
        data.komposisi + alias +
        '<input type="hidden" name="komposisi[]" value="' + data.komposisi + '">' +
        '<input type="hidden" name="komposisi_alias[]" value="' + data.komposisi_alias + '">' +
        '</td>' +
        '<td class="text-right">' +
        data.komposisi_qty +
        '<input type="hidden" name="komposisi_qty[]" value="' + data.komposisi_qty + '">' +
        '</td>' +
        '</tr>';
      $("#komposisi_data").append(html);

      $("#komposisi").val('').removeClass("is-valid").removeClass("is-invalid");
      $("#komposisi_alias").val('').removeClass("is-valid").removeClass("is-invalid");
      $("#komposisi_qty").val(0).removeClass("is-valid").removeClass("is-invalid");
      $("#modal_komposisi").modal('show');
    })

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');
  })
</script>