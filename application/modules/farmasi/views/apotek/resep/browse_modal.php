<?php if (@$main != null): ?>
  <script>
    $(document).ready(function () {
      $('#table-resep').DataTable();
    })
  </script>
<?php endif;?>
<table id="table-resep" class="table table-hover table-striped table-bordered">
  <thead>
    <tr>
      <th class="text-center" width="10">No</th>
      <th class="text-center" width="80">No. Resep</th>
      <th class="text-center" width="80">Tgl. Order</th>
      <th class="text-center" width="200">Nama Pasien</th>
      <th class="text-center" width="50">Umur</th>
      <th class="text-center">Alamat</th>
      <th class="text-center" width="50">Lokasi Pelayanan</th>
      <th class="text-center" width="80">Lokasi Depo</th>
      <th class="text-center" width="50">Status Resep</th>
      <th class="text-center" width="50">Aksi</th>
    </tr>
  </thead>
  <?php if (@$main == null) : ?>
    <tbody>
      <tr>
        <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
      </tr>
    </tbody>
  <?php else : ?>
    <tbody>
      <?php $i = 1;
      foreach ($main as $row) : ?>
        <tr>
          <td class="text-center"><?= ($i++) ?></td>
          <td class="text-center"><?= $row['resep_id'] ?></td>
          <td class="text-center"><?= to_date($row['tgl_catat'], '-', 'full_date', ' ') ?></td>
          <td><?= $row['pasien_nm'] ?></td>
          <td class="text-center">
            <?= $row['umur_thn'] ?> th <br>
            <?= $row['umur_bln'] ?> bl <br>
            <?= $row['umur_hr'] ?> hr
          </td>
          <td>
            <?= $row['alamat'] ?>,
            <?= ucwords(strtolower($row['kelurahan'])) ?>,
            <?= ucwords(strtolower($row['kecamatan'])) ?>,
            <?= ucwords(strtolower($row['kabupaten'])) ?>,
            <?= ucwords(strtolower($row['provinsi'])) ?>
          </td>
          <td><?= $row['lokasi_pelayanan'] ?></td>
          <td><?= $row['lokasi_depo'] ?></td>
          <td class="text-center" width="100">
            <?php if ($row['resep_st'] == 0) : ?>
              <div class="badge badge-xs badge-light blink">Belum</div>
            <?php elseif ($row['resep_st'] == 1) : ?>
              <div class="badge badge-xs badge-warning blink">Diproses</div>
            <?php elseif ($row['resep_st'] == 2) : ?>
              <div class="badge badge-xs badge-success">Sudah</div>
            <?php endif; ?>
          </td>
          <td class="text-center">
            <a class="btn btn-primary btn-xs btn-table" href="<?= site_url() . '/' . $nav['nav_url'] . '/form/' . $row['resep_id'] ?>">Pilih >></a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  <?php endif; ?>
</table>