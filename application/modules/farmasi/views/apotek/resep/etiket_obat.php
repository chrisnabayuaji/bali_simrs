<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <style>
    table {
      width: 500px !important;
      text-align: left;
      border-collapse: collapse;
    }

    table td,
    table th {
      padding: 4px 4px 4px 4px;
    }

    table tbody td {
      font-size: 12px;
    }

    table thead {}

    table thead th {
      font-size: 15px;
      font-weight: bold;
    }

    table tfoot {
      font-weight: bold;
    }

    .row {
      /* display: flex;
      flex-wrap: wrap; */
    }

    .col {
      width: 175px;
      display: inline;
    }
  </style>
</head>

<body>
  <?php
  $r = array_chunk($rinc, 2);
  ?>
  <table style="width:100% !important;">
    <tbody>
      <?php foreach ($r as $row) : ?>
        <tr>
          <?php foreach ($row as $d) : ?>
            <td width="335" style="height:100px!important; border: 1px dashed #AAAAAA;">
              <table style="width: 100%;">
                <tr>
                  <td align="center" style="width: 5%;">
                    <img src="<?= FCPATH . 'assets/images/icon/' . $identitas['logo_rumah_sakit'] ?>" style="width: 30px;">
                  </td>
                  <td align="left">
                    <font style="line-height: 1; font-size: 10px">INSTALASI FARMASI</font><br>
                    <font style="line-height: 1;"><?= @$identitas['rumah_sakit'] ?> <?= strtoupper(clear_kab_kota(@$identitas['kabupaten'])) ?></font>
                  </td>
                  <td align="left" style="width: 23%;">
                    No.<?= $main['resep_id'] ?><br>
                    <?= substr(to_date($main['tgl_catat'], '-', 'full_date'), 0, 10) ?>
                  </td>
                </tr>
                <tr>
                  <td colspan="3" style="width: 100%; padding-top: 5px;">
                    <table style="width: 100%;">
                      <tr>
                        <td valign="top" style="width: 25%;">No RM</td>
                        <td valign="top" align="center" style="width:3%">:</td>
                        <td valign="top" style="width: 76%"><?= @$main['pasien_id'] ?> (<?= $main['lokasi_layanan'] ?>)</td>
                      </tr>
                      <tr>
                        <td valign="top" style="width: 25%;">NIK</td>
                        <td valign="top" align="center" style="width:3%">:</td>
                        <td valign="top" style="width: 76%"><?= @$main['nik'] ?></td>
                      </tr>
                      <tr>
                        <td valign="top" style="width: 25%;">Nama Pasien</td>
                        <td valign="top" align="center" style="width:3%">:</td>
                        <td valign="top" style="width: 76%"><b><?= (@$main['sebutan_cd'] != '') ? @$main['sebutan_cd'] . '.' : '' ?> <?= @$main['pasien_nm'] ?> (<?= @$main['umur_thn'] ?>th)</b></td>
                      </tr>
                      <tr>
                        <td valign="top" style="width: 25%;">Nama Obat</td>
                        <td valign="top" align="center" style="width:3%">:</td>
                        <td valign="top" style="width: 76%"><?= $d['obat_nm'] ?></td>
                      </tr>
                      <tr>
                        <td valign="top" style="width: 25%;">Aturan Pakai</td>
                        <td valign="top" align="center" style="width:3%">:</td>
                        <td valign="top" style="width: 76%"><?= $d['aturan_pakai'] ?></td>
                      </tr>
                      <tr>
                        <td valign="top" style="width: 25%;">Jadwal</td>
                        <td valign="top" align="center" style="width:3%">:</td>
                        <td valign="top" style="width: 76%"><?= $d['jadwal'] ?></td>
                      </tr>
                      <tr>
                        <td valign="top" style="width: 25%;">Aturan Tambahan</td>
                        <td valign="top" align="center" style="width:3%">:</td>
                        <td valign="top" style="width: 76%"><?= $d['aturan_tambahan'] ?></td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          <?php endforeach; ?>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</body>

</html>