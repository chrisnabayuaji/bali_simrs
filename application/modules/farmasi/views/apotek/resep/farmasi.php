<?php $this->load->view('_js_farmasi'); ?>
<div class="row">
  <div class="col-md-12">
    <form id="farmasi_form" action="" method="post" autocomplete="off">
      <input type="hidden" name="farmasi_id" value="<?= @$detail['farmasi_id'] ?>">
      <input type="hidden" name="reg_id" value="<?= $resep['reg_id'] ?>">
      <input type="hidden" name="resep_id" value="<?= $resep['resep_id'] ?>">
      <input type="hidden" name="dokter_id" value="<?= $resep['dokter_id'] ?>">
      <input type="hidden" name="lokasi_id" value="<?= $resep['lokasi_id'] ?>">
      <input type="hidden" name="pasien_id" value="<?= $resep['pasien_id'] ?>">
      <div class="form-group row mb-2">
        <label class="col-lg-4 col-md-3 col-form-label"></label>
        <div class="col-lg-8 col-md-3">
          <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_farmasi/search_obat' ?>" modal-title="List Daftar Obat" modal-size="lg" class="btn btn-xs btn-default modal-href-obat"><i class="fas fa-pills"></i> Daftar Obat</a>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Obat <span class="text-danger">*</span></label>
        <div class="col-lg-8 col-md-3">
          <select class="form-control select2" name="obat_id" id="obat_id" onchange="get_obat($(this))">
            <option value="">- Pilih -</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Harga</label>
        <div class="col-lg-3 col-md-2">
          <input type="text" class="form-control" name="harga" id="harga" value="<?= (@$detail != '') ? @$detail['harga'] : '0' ?>" dir="RTL" onkeyup="hitung_total()">
        </div>
        <label class="col-lg-2 col-md-3 col-form-label">Stok</label>
        <div class="col-lg-3 col-md-2">
          <input type="text" class="form-control" name="stok" id="stok" value="0" readonly dir="RTL">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Qty <span class="text-danger">*</span></label>
        <div class="col-lg-8 col-md-2">
          <input type="text" class="form-control autonumeric col-lg-4" name="qty" id="qty" min="1" value="<?= (@$detail) ? $detail['qty'] : 0 ?>" dir="RTL" onkeyup="hitung_total()">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Jumlah Awal</label>
        <div class="col-lg-4 col-md-2">
          <input type="text" class="form-control" name="jumlah_awal" id="jumlah_awal" value="<?= (@$detail) ? num_id($detail['jumlah_awal']) : 0 ?>" readonly dir="RTL">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Potongan <span class="text-danger">*</span></label>
        <div class="col-lg-4 col-md-2">
          <input type="text" class="form-control autonumeric" name="potongan" id="potongan" value="<?= (@$detail) ? num_id($detail['potongan']) : 0 ?>" dir="RTL" onkeyup="hitung_total()">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Jumlah Akhir</label>
        <div class="col-lg-4 col-md-2">
          <input type="text" class="form-control" name="jumlah_akhir" id="jumlah_akhir" value="<?= (@$detail) ? num_id($detail['jumlah_akhir']) : 0 ?>" readonly dir="RTL">
        </div>
      </div>
      <h6>Label Obat</h6>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Aturan Pakai <span class="text-danger">*</span></label>
        <div class="col-lg-8 col-md-2">
          <input type="text" class="form-control" name="aturan_pakai" id="aturan_pakai" value="<?= @$detail['aturan_pakai'] ?>" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Jadwal Pemakaian</label>
        <div class="col-lg-8 col-md-2">
          <input type="text" class="form-control" name="jadwal" id="jadwal" value="<?= @$detail['jadwal'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Aturan Tambahan</label>
        <div class="col-lg-8 col-md-2">
          <input type="text" class="form-control" name="aturan_tambahan" id="aturan_tambahan" value="<?= @$detail['aturan_tambahan'] ?>">
        </div>
      </div>
      <div class="row">
        <div class="col-lg-9 offset-lg-4 p-0">
          <button type="submit" id="farmasi_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
          <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        </div>
      </div>
    </form>
  </div>
</div>