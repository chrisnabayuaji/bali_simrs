<?php if ($farmasi == null) : ?>
  <tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else : ?>
  <?php $i = 1;
  foreach ($farmasi as $r) : ?>
    <tr>
      <td class="text-center"><?= $i++ ?></td>
      <td class="text-center" width="60">
        <a href="javascript:void(0)" class="btn btn-primary btn-table modal-href"  data-href="<?= site_url() . '/' . $nav['nav_url'] . '/farmasi/' . $r['resep_id'] . '/' . $r['farmasi_id'] ?>" modal-title="Ubah Obat" modal-size="md"><i class="fas fa-pencil-alt"></i></a>
        <a href="javascript:void(0)" class="btn btn-danger btn-table btn-delete-farmasi" data-resep-id="<?= $r['resep_id'] ?>" data-reg-id="<?= $r['reg_id'] ?>" data-lokasi-id="<?= $r['lokasi_id'] ?>" data-farmasi-id="<?= $r['farmasi_id'] ?>" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
      </td>
      <td class="text-left"><?= $r['obat_nm'] ?></td>
      <td class="text-left"><?= $r['aturan_pakai'] ?></td>
      <td class="text-left"><?= $r['jadwal'] ?></td>
      <td class="text-left"><?= $r['aturan_tambahan'] ?></td>
      <td class="text-right"><?= num_id($r['harga']) ?></td>
      <td class="text-right"><?= num_id($r['qty']) ?></td>
      <td class="text-right"><?= num_id($r['jumlah_awal']) ?></td>
      <td class="text-right"><?= num_id($r['potongan']) ?></td>
      <td class="text-right">
        <b><?= num_id($r['jumlah_akhir']) ?></b>
      </td>
    </tr>
  <?php endforeach; ?>
<?php endif; ?>

<script type="text/javascript">
  $(document).ready(function() {
    $('.modal-href').click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");
      var modal_header = $(this).attr("modal-header");
      var modal_content_top = $(this).attr("modal-content-top");

      $("#modal-size").removeClass('modal-lg').removeClass('modal-md').removeClass('modal-sm');

      $("#modal-title").html(modal_title);
      $("#modal-size").addClass('modal-' + modal_size);
      if (modal_custom_size) {
        $("#modal-size").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
      }
      if (modal_content_top) {
        $(".modal-content-top").attr('style', 'margin-top: ' + modal_content_top + ' !important');
      }
      if (modal_header == 'hidden') {
        $("#modal-header").addClass('d-none');
      } else {
        $("#modal-header").removeClass('d-none');
      }
      $("#myModal").modal('show');
      $("#modal-body").html('<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i><br>Loading</div>');
      $.post($(this).data('href'), function(data) {
        $("#modal-body").html(data.html);
      }, 'json');
    });
    
    // edit
    $('.btn-edit').bind('click', function(e) {
      e.preventDefault();
      var resep_id = $(this).attr("data-resep-id");
      var rian_id = $(this).attr("data-rian-id");
      var reg_id = $(this).attr("data-reg-id");
      var lokasi_id = $(this).attr("data-lokasi-id");
      var pasien_id = $("#pasien_id").val();
      //
      $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_resep/get_data' ?>', {
        resep_id: resep_id,
        rian_id: rian_id,
        reg_id: reg_id,
        lokasi_id: lokasi_id,
        pasien_id: pasien_id
      }, function(data) {

        $('#resep_action').html('<i class="fas fa-save"></i> Ubah');
      }, 'json');
    });
    // delete
    $('.btn-delete-farmasi').on('click', function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Apakah Anda yakin?',
        text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#eb3b5a',
        cancelButtonColor: '#b2bec3',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        customClass: 'swal-wide'
      }).then((result) => {
        if (result.value) {
          var farmasi_id = $(this).attr("data-farmasi-id");
          var resep_id = $(this).attr("data-resep-id");

          $('#farmasi_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
          $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_farmasi/delete_data' ?>', {
            farmasi_id: farmasi_id,
            resep_id: resep_id
          }, function(data) {
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil dihapus.',
              icon: 'success',
              position: 'top-right'
            })
            get_grand();
            $('#farmasi_data').html(data.html);
          }, 'json');
        }
      })
    })

  });
</script>