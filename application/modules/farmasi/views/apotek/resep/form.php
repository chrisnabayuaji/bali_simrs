<?php $this->load->view('_js_resep'); ?>
<div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
<form id="resep_form" role="form" method="POST" enctype="multipart/form-data" action="<?= $form_action ?>" autocomplete="off">
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline col-title">
          <div class="col-back">
            <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
          </div>
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            <?= $nav['nav_nm'] ?>
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-user-injured"></i> Registrasi</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item active"><span>Form</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>

    <div class="row full-page mt-4 mb-5">
      <div class="col-lg-12 stretch-card mb-2">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <div class="row mt-1">
              <div class="col-md-6">
                <table class="table" style="width: 100% !important; border-bottom: 1px solid #f2f2f2;">
                  <tbody>
                    <tr>
                      <td width="150" style="font-weight: 500 !important;"><b>No. Resep</b></td>
                      <td width="20"><b>:</b></td>
                      <td>
                        <b>
                          <?= @$main['resep_id'] ?>
                          <input type="hidden" id="resep_id" name="resep_id" value="<?= @$main['resep_id'] ?>">
                          <input type="hidden" id="pasien_id" value="<?= @$main['pasien_id'] ?>">
                          <input type="hidden" id="lokasi_id" value="<?= @$main['lokasi_id'] ?>">
                          <input type="hidden" id="reg_id" value="<?= @$main['reg_id'] ?>">
                          <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/browse/' . @$main['resep_id'] ?>" modal-title="Cari Data" modal-size="lg" class="btn btn-primary btn-table modal-href ml-2" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cari Data"><i class="fas fa-search"></i> Cari</a>
                        </b>
                      </td>
                    </tr>
                    <tr>
                      <td width="150" style="font-weight: 500 !important;">No. Registrasi</td>
                      <td width="20">:</td>
                      <td><?= @$main['reg_id'] ?></td>
                    </tr>
                    <tr>
                      <td width="150" style="font-weight: 500 !important;">NIK</td>
                      <td width="20">:</td>
                      <td><?= @$main['nik'] ?></td>
                    </tr>
                    <tr>
                      <td width="150" style="font-weight: 500 !important;">No.RM / Nama Pasien</td>
                      <td width="20">:</td>
                      <td><?= @$main['pasien_id'] ?> / <?= @$main['pasien_nm'] ?>, <?= @$main['sebutan_cd'] ?></td>
                    </tr>
                    <tr>
                      <td width="150">Alamat Pasien</td>
                      <td width="20">:</td>
                      <td>
                        <?= (@$main['alamat'] != '') ? @$main['alamat'] . ', ' : '' ?>
                        <?= (@$main['kelurahan'] != '') ? ucwords(strtolower(@$main['kelurahan'])) . ', ' : '' ?>
                        <?= (@$main['kecamatan'] != '') ? ucwords(strtolower(@$main['kecamatan'])) . ', ' : '' ?>
                        <?= (@$main['kabupaten'] != '') ? ucwords(strtolower(@$main['kabupaten'])) . ', ' : '' ?>
                        <?= (@$main['provinsi'] != '') ? ucwords(strtolower(@$main['provinsi'])) : '' ?>
                      </td>
                    </tr>
                    <tr>
                      <td width="150">Lokasi Pelayanan</td>
                      <td width="20">:</td>
                      <td><?= @$main['lokasi_layanan'] ?></td>
                      <input type="hidden" name="map_lokasi_depo" id="map_lokasi_depo" value=<?= $main['map_lokasi_depo'] ?>>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col-md-6">
                <table class="table" style="width: 100% !important; border-bottom: 1px solid #f2f2f2;">
                  <tbody>
                    <tr>
                      <td width="150" style="font-weight: 500 !important;"><b>Tgl. Order Resep</b></td>
                      <td width="20"><b>:</b></td>
                      <td><b><?= @to_date($main['tgl_catat'], '-', 'full_date', ' ') ?></b></td>
                    </tr>
                    <tr>
                      <td width="150" style="font-weight: 500 !important;">Dokter PJ</td>
                      <td width="20">:</td>
                      <td><?= @$main['dokter_nm'] ?></td>
                    </tr>
                    <tr>
                      <td width="150" style="font-weight: 500 !important;">Umur/JK</td>
                      <td width="20">:</td>
                      <td>
                        <?= @$main['umur_thn'] ?> th <?= @$main['umur_bln'] ?> bln <?= @$main['umur_hr'] ?> hr /
                        <?= @get_parameter_value('sex_cd', $main['sex_cd']) ?></td>
                    </tr>
                    <tr>
                      <td width="150">Jenis / Asal Pasien</td>
                      <td width="20">:</td>
                      <td><?= @$main['jenispasien_nm'] ?> / <?= get_parameter_value('asalpasien_cd', @$main['asalpasien_cd']) ?></td>
                    </tr>
                    <tr>
                      <td width="150"><b>Alergi</b></td>
                      <td width="20">:</td>
                      <td>
                        <?php if ($alergi != null) : ?>
                          <b class="blink text-danger">
                            <?php foreach ($alergi as $row) : ?>
                              <?= $row['alergi_obat'] ?>,
                            <?php endforeach; ?>
                          </b>
                        <?php endif; ?>
                      </td>
                    </tr>
                    <tr>
                      <td width="150" class="text-top"><b>Diagnosis</b></td>
                      <td width="20" class="text-top">:</td>
                      <td class="text-top">
                        <?php if ($diagnosis != null) : ?>
                          <b class="text-left">
                            <?php foreach ($diagnosis as $row) : ?>
                              <?= $row['icdx'] ?> - <?= $row['penyakit_nm'] ?> <br>
                            <?php endforeach; ?>
                          </b>
                        <?php endif; ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-5">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <h4 class="card-title border-bottom border-2 pb-2 mb-1">Resep</h4>
            <h6 class="mt-3">Resep Non Racik</h6>
            <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/form_resep_non' . '/' . @$main['resep_id'] ?>" modal-title="Tambah Resep Non Racik" modal-custom-size="600" class="btn btn-xs btn-primary modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Tambah Resep Non Racik"><i class="fas fa-plus-circle"></i> Tambah Resep Non Racik</a>
            <div class="table-responsive">
              <table class="table table-hover table-striped table-bordered table-fixed mt-2">
                <thead>
                  <tr>
                    <th style="font-size:11px" class="text-center" width="20">#</th>
                    <th style="font-size:11px" class="text-center" width="60">Aksi</th>
                    <th style="font-size:11px" class="text-center">Nama Obat/Alias</th>
                    <th style="font-size:11px" class="text-center" width="60">Rute</th>
                    <th style="font-size:11px" class="text-center" width="40">Qty</th>
                    <th style="font-size:11px" class="text-center">Aturan Pakai</th>
                    <th style="font-size:11px" class="text-center">Jadwal Pemberian</th>
                    <th style="font-size:11px" class="text-center">Aturan Tambahan</th>
                  </tr>
                </thead>
                <tbody style="height:auto !Important">
                  <?php if ($rinc == null) : ?>
                    <tr>
                      <td class="text-center" colspan="99">Tidak ada data</td>
                    </tr>
                  <?php else : ?>
                    <?php
                    foreach ($rinc as $r) : ?>
                      <tr>
                        <td class="text-top text-center" width="20"><i class="fas fa-prescription"></i></td>
                        <td class="text-center text-top" width="60">
                          <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/form_resep_non/' . @$main['resep_id'] . '/' . $r['rincian_id'] ?>" modal-title="Ubah Resep Non Racik" modal-custom-size="600" class="btn btn-primary btn-table modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Ubah Resep Non Racik"><i class="fas fa-pencil-alt"></i></a>
                          <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/delete_resep_non/' . @$main['resep_id'] . '/' . $r['rincian_id'] ?>" class="btn btn-danger btn-table btn-delete" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-danger" title="Hapus Resep Non Racik"><i class="far fa-trash-alt"></i></a>
                        </td>
                        <td class="text-top text-left">
                          <?= $r['obat_nm'] ?>
                          <?= ($r['obat_alias'] != '') ? ' (' . $r['obat_alias'] . ')' : '' ?>
                        </td>
                        <td class="text-top" width="60"><?= $r['rute_nm'] ?></td>
                        <td class="text-top" width="40"><?= $r['qty'] ?></td>
                        <td class="text-top"><?= $r['aturan_jml'] . ' ' . $r['satuan_nm'] ?> tiap <?= $r['aturan_waktu'] ?> <?= $r['waktu_nm'] ?></td>
                        <td class="text-top"><?= $r['jadwal'] ?></td>
                        <td class="text-top"><?= $r['aturan_tambahan'] ?></td>
                      </tr>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </tbody>
              </table>
            </div>
            <h6 class="mt-3">Resep Racik</h6>
            <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/form_resep_racik' . '/' . @$main['resep_id'] ?>" modal-title="Tambah Resep Racik" modal-custom-size="600" class="btn btn-xs btn-primary modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Tambah Resep Racik"><i class="fas fa-plus-circle"></i> Tambah Resep Racik</a>
            <div class="table-responsive mt-2">
              <table class="table table-bordered table-sm table-fixed">
                <thead>
                  <tr>
                    <th style="font-size:11px" class="text-center" width="20">#</th>
                    <th style="font-size:11px" class="text-center" width="60">Aksi</th>
                    <th style="font-size:11px" class="text-center">Nama Obat/Alias</th>
                    <th style="font-size:11px" class="text-center" width="80">Sediaan</th>
                    <th style="font-size:11px" class="text-center">Rute</th>
                    <th style="font-size:11px" class="text-center" width="40">Qty</th>
                    <th style="font-size:11px" class="text-center">Aturan Pakai</th>
                    <th style="font-size:11px" class="text-center" width="80">Jadwal Pemberian</th>
                    <th style="font-size:11px" class="text-center" width="75">Aturan Tambahan</th>
                  </tr>
                </thead>
                <tbody style="height:auto !important">
                  <?php if ($rinc_racik == null) : ?>
                    <tr>
                      <td class="text-center" colspan="99">Tidak ada data</td>
                    </tr>
                  <?php else : ?>
                    <?php
                    foreach ($rinc_racik as $r) : ?>
                      <tr>
                        <td class="text-top text-center" width="20"><i class="fas fa-prescription"></i></td>
                        <td class="text-center text-top" width="60">
                          <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/form_resep_racik/' . @$main['resep_id'] . '/' . $r['rincian_id'] ?>" modal-title="Ubah Resep Non Racik" modal-custom-size="600" class="btn btn-primary btn-table modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Ubah Resep Non Racik"><i class="fas fa-pencil-alt"></i></a>
                          <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/delete_resep_racik/' . @$main['resep_id'] . '/' . $r['rincian_id'] ?>" class="btn btn-danger btn-table btn-delete" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-danger" title="Hapus Resep Non Racik"><i class="far fa-trash-alt"></i></a>
                        </td>
                        <td class="text-top text-left">
                          <?= $r['obat_nm'] ?>
                          <?= ($r['obat_alias'] != '') ? ' (' . $r['obat_alias'] . ')' : '' ?>
                        </td>
                        <td class="text-top" width="80"><?= $r['sediaan_nm'] ?></td>
                        <td class="text-top"><?= $r['rute_nm'] ?></td>
                        <td class="text-top" width="40"><?= $r['qty'] ?></td>
                        <td class="text-top"><?= $r['aturan_jml'] . ' ' . $r['satuan_nm'] ?> tiap <?= $r['aturan_waktu'] ?> <?= $r['waktu_nm'] ?></td>
                        <td class="text-top" width="80"><?= $r['jadwal'] ?></td>
                        <td class="text-top" width="75"><?= $r['aturan_tambahan'] ?></td>
                      </tr>
                      <tr>
                        <td class="text-right text-top" colspan="2" width="100">Komposisi</td>
                        <td colspan="99">
                          <table style="width:100%">
                            <thead>
                              <tr style="background:#f2f2f2">
                                <th class="text-center" style="border-top: solid rgb(221, 221, 221) 1px" width="">Komposisi / Alias</th>
                                <th class="text-center" style="border-top: solid rgb(221, 221, 221) 1px" width="50">Qty</th>
                              </tr>
                            </thead>
                            <tbody style="height:auto !important">
                              <?php foreach ($r['komposisi'] as $komposisi) : ?>
                                <tr>
                                  <td><?= $komposisi['komposisi'] ?></td>
                                  <td width="50"><?= $komposisi['komposisi_qty'] ?></td>
                                </tr>
                              <?php endforeach; ?>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    <?php endforeach; ?>
                  <?php endif; ?>
                </tbody>
              </table>
            </div>
            <h4 class="card-title border-bottom border-2 pb-2 mt-3 mb-1">Telaah Resep</h4>
            <table class="table table-bordered table-striped table-sm">
              <thead>
                <tr>
                  <th class="text-center text-middle">Indikator</th>
                  <th class="text-center text-middle" width="50">
                    Ya<br>
                    <input class="mt-1" type="radio" name="all_resep" id="all_resep_ya" value="1" onchange="allResep($(this))">
                  </th>
                  <th class="text-center text-middle" width="50">
                    Tidak<br>
                    <input class="mt-1" type="radio" name="all_resep" id="all_resep_tidak" value="-1" onchange="allResep($(this))">
                  </th>
                  <th class="text-center text-middle" width="180">Keterangan / Tindak Lanjut</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td colspan="99"><strong>1. Kelengkapan Administratif :<strong></td>
                </tr>
                <tr>
                  <td>a. Nama Pasien</td>
                  <td class="text-center">
                    <input class="mt-1 resep-ya" type="radio" name="t_resep_nama" value="1" <?= ($main['t_resep_nama'] == 1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_nama')" <?= (@$main['t_resep_nama'] == 1) ? 'checked' : '' ?>>
                  </td>
                  <td class="text-center">
                    <input class="mt-1 resep-tidak" type="radio" name="t_resep_nama" value="-1" <?= ($main['t_resep_nama'] == -1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_nama')" <?= (@$main['t_resep_nama'] == -1) ? 'checked' : '' ?>>
                  </td>
                  <td>
                    <input class="form-control" type="text" name="k_resep_nama" value="<?= @$main['k_resep_nama'] ?>" oninput="telaah($(this), 'k_resep_nama')">
                  </td>
                </tr>
                <tr>
                  <td>b. Nomor RM</td>
                  <td class="text-center">
                    <input class="mt-1 resep-ya" type="radio" name="t_resep_rm" value="1" <?= ($main['t_resep_rm'] == 1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_rm')">
                  </td>
                  <td class="text-center">
                    <input class="mt-1 resep-tidak" type="radio" name="t_resep_rm" value="-1" <?= ($main['t_resep_rm'] == -1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_rm')">
                  </td>
                  <td>
                    <input class="form-control" type="text" name="k_resep_rm" value="<?= @$main['k_resep_rm'] ?>" oninput="telaah($(this), 'k_resep_rm')">
                  </td>
                </tr>
                <tr>
                  <td>c. Kejelasan tulisan resep</td>
                  <td class="text-center">
                    <input class="mt-1 resep-ya" type="radio" name="t_resep_kejelasan" value="1" <?= ($main['t_resep_kejelasan'] == 1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_kejelasan')">
                  </td>
                  <td class="text-center">
                    <input class="mt-1 resep-tidak" type="radio" name="t_resep_kejelasan" value="-1" <?= ($main['t_resep_kejelasan'] == -1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_kejelasan')">
                  </td>
                  <td>
                    <input class="form-control" type="text" name="k_resep_kejelasan" value="<?= @$main['k_resep_kejelasan'] ?>" oninput="telaah($(this), 'k_resep_kejelasan')">
                  </td>
                </tr>
                <tr>
                  <td colspan="99"><strong>2. Kesesuaian Farmasetik (Cara pemberian, Dosis & Aturan Pakai) :<strong></td>
                </tr>
                <tr>
                  <td>a. Tepat pasien</td>
                  <td class="text-center">
                    <input class="mt-1 resep-ya" type="radio" name="t_resep_pasien" value="1" <?= ($main['t_resep_pasien'] == 1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_pasien')">
                  </td>
                  <td class="text-center">
                    <input class="mt-1 resep-tidak" type="radio" name="t_resep_pasien" value="-1" <?= ($main['t_resep_pasien'] == -1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_pasien')">
                  </td>
                  <td>
                    <input class="form-control" type="text" name="k_resep_pasien" value="<?= @$main['k_resep_pasien'] ?>" oninput="telaah($(this), 'k_resep_pasien')">
                  </td>
                </tr>
                <tr>
                  <td>b. Tepat obat / alkes</td>
                  <td class="text-center">
                    <input class="mt-1 resep-ya" type="radio" name="t_resep_obat" value="1" <?= ($main['t_resep_obat'] == 1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_obat')">
                  </td>
                  <td class="text-center">
                    <input class="mt-1 resep-tidak" type="radio" name="t_resep_obat" value="-1" <?= ($main['t_resep_obat'] == -1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_obat')">
                  </td>
                  <td>
                    <input class="form-control" type="text" name="k_resep_obat" value="<?= @$main['k_resep_obat'] ?>" oninput="telaah($(this), 'k_resep_obat')">
                  </td>
                </tr>
                <tr>
                  <td>c. Tepat dosis</td>
                  <td class="text-center">
                    <input class="mt-1 resep-ya" type="radio" name="t_resep_dosis" value="1" <?= ($main['t_resep_dosis'] == 1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_dosis')">
                  </td>
                  <td class="text-center">
                    <input class="mt-1 resep-tidak" type="radio" name="t_resep_dosis" value="-1" <?= ($main['t_resep_dosis'] == -1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_dosis')">
                  </td>
                  <td>
                    <input class="form-control" type="text" name="k_resep_dosis" value="<?= @$main['k_resep_dosis'] ?>" oninput="telaah($(this), 'k_resep_dosis')">
                  </td>
                </tr>
                <tr>
                  <td>d. Tepat rute</td>
                  <td class="text-center">
                    <input class="mt-1 resep-ya" type="radio" name="t_resep_rute" value="1" <?= ($main['t_resep_rute'] == 1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_rute')">
                  </td>
                  <td class="text-center">
                    <input class="mt-1 resep-tidak" type="radio" name="t_resep_rute" value="-1" <?= ($main['t_resep_rute'] == -1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_rute')">
                  </td>
                  <td>
                    <input class="form-control" type="text" name="k_resep_rute" value="<?= @$main['k_resep_rute'] ?>" oninput="telaah($(this), 'k_resep_rute')">
                  </td>
                </tr>
                <tr>
                  <td>e. Tepat waktu</td>
                  <td class="text-center">
                    <input class="mt-1 resep-ya" type="radio" name="t_resep_waktu" value="1" <?= ($main['t_resep_waktu'] == 1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_waktu')">
                  </td>
                  <td class="text-center">
                    <input class="mt-1 resep-tidak" type="radio" name="t_resep_waktu" value="-1" <?= ($main['t_resep_waktu'] == -1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_waktu')">
                  </td>
                  <td>
                    <input class="form-control" type="text" name="k_resep_waktu" value="<?= @$main['k_resep_waktu'] ?>" oninput="telaah($(this), 'k_resep_waktu')">
                  </td>
                </tr>
                <tr>
                  <td>f. Duplikasi</td>
                  <td class="text-center">
                    <input class="mt-1 resep-ya" type="radio" name="t_resep_duplikasi" value="1" <?= ($main['t_resep_duplikasi'] == 1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_duplikasi')">
                  </td>
                  <td class="text-center">
                    <input class="mt-1 resep-tidak" type="radio" name="t_resep_duplikasi" value="-1" <?= ($main['t_resep_duplikasi'] == -1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_duplikasi')">
                  </td>
                  <td>
                    <input class="form-control" type="text" name="k_resep_duplikasi" value="<?= @$main['k_resep_duplikasi'] ?>" oninput="telaah($(this), 'k_resep_duplikasi')">
                  </td>
                </tr>
                <tr>
                  <td colspan="99"><strong>3. Kesesuaian Klinis<strong></td>
                </tr>
                <tr>
                  <td>a. Alergi</td>
                  <td class="text-center">
                    <input class="mt-1 resep-ya" type="radio" name="t_resep_alergi" value="1" <?= ($main['t_resep_alergi'] == 1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_alergi')">
                  </td>
                  <td class="text-center">
                    <input class="mt-1 resep-tidak" type="radio" name="t_resep_alergi" value="-1" <?= ($main['t_resep_alergi'] == -1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_alergi')">
                  </td>
                  <td>
                    <input class="form-control" type="text" name="k_resep_alergi" value="<?= @$main['k_resep_alergi'] ?>" oninput="telaah($(this), 'k_resep_alergi')">
                  </td>
                </tr>
                <tr>
                  <td>b. Interaksi obat dan kontradik</td>
                  <td class="text-center">
                    <input class="mt-1 resep-ya" type="radio" name="t_resep_interaksi" value="1" <?= ($main['t_resep_interaksi'] == 1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_interaksi')">
                  </td>
                  <td class="text-center">
                    <input class="mt-1 resep-tidak" type="radio" name="t_resep_interaksi" value="-1" <?= ($main['t_resep_interaksi'] == -1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_interaksi')">
                  </td>
                  <td>
                    <input class="form-control" type="text" name="k_resep_interaksi" value="<?= @$main['k_resep_interaksi'] ?>" oninput="telaah($(this), 'k_resep_interaksi')">
                  </td>
                </tr>
                <tr>
                  <td>c. Tepat dosis</td>
                  <td class="text-center">
                    <input class="mt-1 resep-ya" type="radio" name="t_resep_dosis_klinis" value="1" <?= ($main['t_resep_dosis_klinis'] == 1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_dosis_klinis')">
                  </td>
                  <td class="text-center">
                    <input class="mt-1 resep-tidak" type="radio" name="t_resep_dosis_klinis" value="-1" <?= ($main['t_resep_dosis_klinis'] == -1) ? 'checked' : '' ?> onchange="telaah($(this), 't_resep_dosis_klinis')">
                  </td>
                  <td>
                    <input class="form-control" type="text" name="k_resep_dosis_klinis" value="<?= @$main['k_resep_dosis_klinis'] ?>" oninput="telaah($(this), 'k_resep_dosis_klinis')">
                  </td>
                </tr>
              </tbody>
            </table>
            <h4 class="card-title border-bottom border-2 pb-2 mt-3 mb-1">Telaah Obat</h4>
            <table class="table table-bordered table-striped table-sm">
              <thead>
                <tr>
                <tr>
                  <th class="text-center text-middle">Indikator</th>
                  <th class="text-center text-middle" width="50">
                    Ya<br>
                    <input class="mt-1" type="radio" name="all_obat" id="all_obat_ya" value="1" onchange="allObat($(this))">
                  </th>
                  <th class="text-center text-middle" width="50">
                    Tidak<br>
                    <input class="mt-1" type="radio" name="all_obat" id="all_obat_tidak" value="-1" onchange="allObat($(this))">
                  </th>
                  <th class="text-center text-middle" width="180">Keterangan / Tindak Lanjut</th>
                </tr>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1. Obat dengan resep</td>
                  <td class="text-center">
                    <input class="mt-1 obat-ya" type="radio" name="t_obat_obat" value="1" <?= ($main['t_obat_obat'] == 1) ? 'checked' : '' ?> onchange="telaah($(this), 't_obat_obat')">
                  </td>
                  <td class="text-center">
                    <input class="mt-1 obat-tidak" type="radio" name="t_obat_obat" value="-1" <?= ($main['t_obat_obat'] == -1) ? 'checked' : '' ?> onchange="telaah($(this), 't_obat_obat')">
                  </td>
                  <td>
                    <input class="form-control" type="text" name="k_obat_obat" value="<?= @$main['k_obat_obat'] ?>" oninput="telaah($(this), 'k_obat_obat')">
                  </td>
                </tr>
                <tr>
                  <td>2. Dosis</td>
                  <td class="text-center">
                    <input class="mt-1 obat-ya" type="radio" name="t_obat_dosis" value="1" <?= ($main['t_obat_dosis'] == 1) ? 'checked' : '' ?> onchange="telaah($(this), 't_obat_dosis')">
                  </td>
                  <td class="text-center">
                    <input class="mt-1 obat-tidak" type="radio" name="t_obat_dosis" value="-1" <?= ($main['t_obat_dosis'] == -1) ? 'checked' : '' ?> onchange="telaah($(this), 't_obat_dosis')">
                  </td>
                  <td>
                    <input class="form-control" type="text" name="k_obat_dosis" value="<?= @$main['k_obat_dosis'] ?>" oninput="telaah($(this), 'k_obat_dosis')">
                  </td>
                </tr>
                <tr>
                  <td>3. Jumlah</td>
                  <td class="text-center">
                    <input class="mt-1 obat-ya" type="radio" name="t_obat_jumlah" value="1" <?= ($main['t_obat_jumlah'] == 1) ? 'checked' : '' ?> onchange="telaah($(this), 't_obat_jumlah')">
                  </td>
                  <td class="text-center">
                    <input class="mt-1 obat-tidak" type="radio" name="t_obat_jumlah" value="-1" <?= ($main['t_obat_jumlah'] == -1) ? 'checked' : '' ?> onchange="telaah($(this), 't_obat_jumlah')">
                  </td>
                  <td>
                    <input class="form-control" type="text" name="k_obat_jumlah" value="<?= @$main['k_obat_jumlah'] ?>" oninput="telaah($(this), 'k_obat_jumlah')">
                  </td>
                </tr>
                <tr>
                  <td>4. Frekuensi</td>
                  <td class="text-center">
                    <input class="mt-1 obat-ya" type="radio" name="t_obat_frekuensi" value="1" <?= ($main['t_obat_frekuensi'] == 1) ? 'checked' : '' ?> onchange="telaah($(this), 't_obat_frekuensi')">
                  </td>
                  <td class="text-center">
                    <input class="mt-1 obat-tidak" type="radio" name="t_obat_frekuensi" value="-1" <?= ($main['t_obat_frekuensi'] == -1) ? 'checked' : '' ?> onchange="telaah($(this), 't_obat_frekuensi')">
                  </td>
                  <td>
                    <input class="form-control" type="text" name="k_obat_frekuensi" value="<?= @$main['k_obat_frekuensi'] ?>" oninput="telaah($(this), 'k_obat_frekuensi')">
                  </td>
                </tr>
                <tr>
                  <td>5. Rute</td>
                  <td class="text-center">
                    <input class="mt-1 obat-ya" type="radio" name="t_obat_rute" value="1" <?= ($main['t_obat_rute'] == 1) ? 'checked' : '' ?> onchange="telaah($(this), 't_obat_rute')">
                  </td>
                  <td class="text-center">
                    <input class="mt-1 obat-tidak" type="radio" name="t_obat_rute" value="-1" <?= ($main['t_obat_rute'] == -1) ? 'checked' : '' ?> onchange="telaah($(this), 't_obat_rute')">
                  </td>
                  <td>
                    <input class="form-control" type="text" name="k_obat_rute" value="<?= @$main['k_obat_rute'] ?>" oninput="telaah($(this), 'k_obat_rute')">
                  </td>
                </tr>
              </tbody>
            </table>
            <br>
            <div class="form-group row">
              <label class="col-lg-3 col-md-3 col-form-label">Tgl. Verifikasi <span class="text-danger">*</span></label>
              <div class="col-lg-4 col-md-9 pr-0">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                  </div>
                  <input type="text" class="form-control datetimepicker" name="t_tgl_verifikasi" id="t_tgl_verifikasi" value="<?php if (@$main['t_tgl_verifikasi'] != '') {
                                                                                                                                echo to_date(@$main['t_tgl_verifikasi'], '-', 'full_date');
                                                                                                                              } else {
                                                                                                                                echo date('d-m-Y H:i:s');
                                                                                                                              } ?>" required aria-invalid="false">
                </div>
              </div>
            </div>
            <div class="form-group row">
              <label class="col-lg-3 col-md-3 col-form-label">Masalah <span class="text-danger">*</span></label>
              <div class="col-lg-9 col-md-9 pr-0">
                <textarea class="form-control " name="t_masalah" id="t_masalah" rows="4" oninput="telaah($(this), 't_masalah')"><?= @$main['t_masalah'] ?></textarea>
              </div>
            </div>
            <div class="form-group row mt-2">
              <label class="col-lg-3 col-md-3 col-form-label">Hasil Konfirmasi <span class="text-danger">*</span></label>
              <div class="col-lg-9 col-md-9 pr-0">
                <textarea class="form-control " name="t_hasil_konfirmasi" id="t_hasil_konfirmasi" rows="4" oninput="telaah($(this), 't_hasil_konfirmasi')"><?= @$main['t_hasil_konfirmasi'] ?></textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-7">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <h4 class="card-title border-bottom border-2 pb-2 mb-1">Farmasi</h4>
            <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/farmasi/' . $main['resep_id'] ?>" modal-title="Tambah Obat" modal-size="md" class="btn btn-primary btn-xs modal-href"><i class="fas fa-plus"></i> Tambah Obat</a>
            <a href="#" data-href="<?= site_url() . '/farmasi/apotek/resep/cetak_modal/cetak_resep/CetakResep-' . @$main['resep_id'] ?>" modal-title="Cetak Resep" modal-size="lg" modal-content-top="-75px" class="btn btn-xs btn-default btn-submit mr-2 modal-href"><i class="fas fa-pills"></i> Cetak Resep</a>
            <!-- <a href="#" data-href="<?= site_url() . '/farmasi/apotek/resep/cetak_modal/cetak_telaah/CetakTelaah-' . @$main['resep_id'] ?>" modal-title="Cetak Telaah" modal-size="lg" modal-content-top="-75px" class="btn btn-xs btn-default btn-submit mr-2 modal-href"><i class="fas fa-pills"></i> Cetak Telaah</a> -->
            <a href="#" data-href="<?= site_url() . '/farmasi/apotek/resep/cetak_modal/cetak_etiket/CetakEtiket-' . @$main['resep_id'] ?>" modal-title="Cetak Etiket Obat" modal-size="lg" modal-content-top="-75px" class="btn btn-xs btn-default btn-submit mr-2 modal-href"><i class="fas fa-pills"></i> Cetak Etiket Obat</a>
            <a href="#" data-href="<?= site_url() . '/farmasi/apotek/resep/cetak_modal/cetak_rincian/CetakRincian-' . @$main['resep_id'] ?>" modal-title="Cetak Rincian Obat" modal-size="lg" modal-content-top="-75px" class="btn btn-xs btn-default btn-submit mr-2 modal-href"><i class="fas fa-pills"></i> Cetak Rincian Obat</a>
            <!-- <a href="#" class="btn btn-xs btn-danger float-right" target="_blank"><i class="fas fa-trash"></i> Hapus Data Obat</a> -->
            <table class="table table-hover table-striped table-bordered mt-2">
              <thead>
                <tr>
                  <th class="text-center text-middle" width="20">No.</th>
                  <th class="text-center text-middle" width="20">Aksi</th>
                  <th class="text-center text-middle">Nama Obat</th>
                  <th class="text-center text-middle" width="80">Aturan Pakai</th>
                  <th class="text-center text-middle" width="80">Jam Minum</th>
                  <th class="text-center text-middle" width="80">Aturan Tambahan</th>
                  <th class="text-center text-middle" width="60">Harga</th>
                  <th class="text-center text-middle" width="50">Qty</th>
                  <th class="text-center text-middle" width="50">Jml Awal</th>
                  <th class="text-center text-middle" width="50">Potongan</th>
                  <th class="text-center text-middle" width="80">Jml Akhir</th>
                </tr>
              </thead>
              <tbody id="farmasi_data">

              </tbody>
            </table>
            <div class="row mt-3">
              <div class="col"></div>
              <div class="col-6">
                <div class="form-group row">
                  <label class="col-lg-6 col-md-3 col-form-label">Tgl. Selesai Resep <span class="text-danger">*</span></label>
                  <div class="col-lg-6 col-md-9 pr-0">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datetimepicker" name="tgl_selesai_resep" id="tgl_selesai_resep" value="<?php if (@$main['tgl_selesai_resep'] != '') {
                                                                                                                                      echo to_date(@$main['tgl_selesai_resep'], '-', 'full_date');
                                                                                                                                    } else {
                                                                                                                                      echo date('d-m-Y H:i:s');
                                                                                                                                    } ?>" required aria-invalid="false">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-6 col-md-3 col-form-label">Status Resep <span class="text-danger">*</span></label>
                  <div class="col-lg-6 col-md-9 pr-0">
                    <select class="form-control form-control-sm chosen-select" name="resep_st" id="resep_st">
                      <?php if (@$main['tgl_selesai_resep'] == '') : ?>
                        <option value="0">Belum</option>
                        <option value="1">Diproses</option>
                        <option value="2" selected>Selesai</option>
                      <?php else : ?>
                        <option value="0" <?= (@$main['resep_st'] == 0) ? 'selected' : '' ?>>Belum</option>
                        <option value="1" <?= (@$main['resep_st'] == 1) ? 'selected' : '' ?>>Diproses</option>
                        <option value="2" <?= (@$main['resep_st'] == 2) ? 'selected' : '' ?>>Selesai</option>
                      <?php endif; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-6 col-md-3 col-form-label">Jumlah Bruto <span class="text-danger">*</span></label>
                  <div class="col-lg-6 col-md-9 pr-0">
                    <input type="text" class="form-control" name="grand_jml_bruto" id="grand_jml_bruto" dir="rtl" value="<?= @num_id($main['grand_jml_bruto']) ?>" required readonly>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-6 col-md-3 col-form-label">Jumlah PPN <span class="text-danger">*</span></label>
                  <div class="col-lg-3 col-md-9">
                    <div class="input-group">
                      <input type="text" class="form-control" name="grand_prs_ppn" id="grand_prs_ppn" onkeyup="hitung_grand()" dir="rtl" value="<?= @num_id($main['grand_prs_ppn']) ?>" required="">
                      <div class="input-group-append">
                        <span class="input-group-text">%</span>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-9 pr-0">
                    <input type="text" class="form-control" name="grand_nom_ppn" id="grand_nom_ppn" dir="rtl" value="<?= @num_id($main['grand_nom_ppn']) ?>" readonly="">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-6 col-md-3 col-form-label">Embalace <span class="text-danger">*</span></label>
                  <div class="col-lg-6 col-md-9 pr-0">
                    <input type="text" class="form-control autonumeric" name="grand_embalace" id="grand_embalace" onkeyup="hitung_grand()" dir="rtl" value="<?= @num_id($main['grand_embalace']) ?>" required="">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-6 col-md-3 col-form-label">Potongan Akhir <span class="text-danger">*</span></label>
                  <div class="col-lg-6 col-md-9 pr-0">
                    <input type="text" class="form-control autonumeric" name="grand_jml_potongan" id="grand_jml_potongan" onkeyup="hitung_grand()" dir="rtl" value="<?= @num_id($main['grand_jml_potongan']) ?>" required="">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-6 col-md-3 col-form-label">Pembulatan <span class="text-danger">*</span></label>
                  <div class="col-lg-6 col-md-9 pr-0">
                    <input type="text" class="form-control autonumeric" name="grand_pembulatan" id="grand_pembulatan" onkeyup="hitung_grand()" dir="rtl" value="<?= @num_id($main['grand_pembulatan']) ?>" required="">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-6 col-md-3 col-form-label">Jumlah Tagihan <span class="text-danger">*</span></label>
                  <div class="col-lg-6 col-md-9 pr-0">
                    <input type="text" class="form-control" name="grand_jml_tagihan" id="grand_jml_tagihan" dir="rtl" value="<?= @num_id($main['grand_jml_tagihan']) ?>" readonly="" required="">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-6 col-md-3 col-form-label">Apoteker <span class="text-danger">*</span></label>
                  <div class="col-lg-6 col-md-9 pr-0">
                    <select class="form-control chosen-select" name="apoteker_id" id="apoteker_id" required>
                      <option value="">- Pilih -</option>
                      <?php foreach ($apoteker as $r) : ?>
                        <option value="<?= $r['pegawai_id'] ?>" <?php if (@$main['apoteker_id'] != NULL) {
                                                                  echo ($main['apoteker_id'] == $r['pegawai_id']) ? 'selected' : '';
                                                                } else {
                                                                  // echo ($this->session->userdata('sess_pegawai_id') == $r['pegawai_id']) ? 'selected' : '';
                                                                  echo ('05.0002' == $r['pegawai_id']) ? 'selected' : '';
                                                                } ?>><?= $r['pegawai_nm'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="btn-form">
    <div class="btn-form-action">
      <div class="btn-form-action-bottom w-100 small-text clearfix">
        <div class="col-lg-10 col-md-8 col-4">
        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-clear">
          <button type="reset" onClick="window.location.reload();" class="btn btn-xs btn-secondary btn-clear"><i class="fas fa-sync-alt"></i> Reset</button>
        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-save">
          <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>