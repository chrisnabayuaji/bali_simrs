<?php $this->load->view('_js_form_modal'); ?>
<ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-rajal-tab" data-toggle="pill" href="#pills-rajal" role="tab" aria-controls="pills-rajal" aria-selected="false"><i class="fas fa-user-injured"></i> PASIEN RAWAT JALAN</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " id="pills-ranap-tab" data-toggle="pill" href="#pills-ranap" role="tab" aria-controls="pills-ranap" aria-selected="true"><i class="fas fa-procedures"></i> PASIEN RAWAT INAP</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " id="pills-igd-tab" data-toggle="pill" href="#pills-igd" role="tab" aria-controls="pills-igd" aria-selected="true"><i class="fas fa-ambulance"></i> PASIEN IGD</a>
  </li>
</ul>
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?= $form_action ?>" autocomplete="off">
  <div class="row">
    <div class="col-md-8">
      <div class="tab-content p-0" id="pills-tabContent" style="border:0px !important">
        <div class="tab-pane fade show active" id="pills-rajal" role="tabpanel" aria-labelledby="pills-rajal-tab">
          <h4 class="card-title border-bottom border-2 pb-2 mb-2"><i class="fas fa-user-injured"></i> List Data Pasien Rawat Jalan Hari Ini</h4>
          <div class="table-responsive">
            <table id="rajal_table" class="table table-hover table-bordered table-striped table-sm w-100">
              <thead>
                <tr>
                  <!-- <th class="text-center" width="50">No. Reg</th> -->
                  <th class="text-center" width="50">No. RM</th>
                  <th class="text-center" width="200">Nama Pasien / Alamat</th>
                  <th class="text-center" width="80">Lokasi</th>
                  <th class="text-center" width="90">Tgl. Registrasi</th>
                  <th class="text-center">Dokter</th>
                  <th class="text-center" width="50">Aksi</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
        <div class="tab-pane fade " id="pills-ranap" role="tabpanel" aria-labelledby="pills-ranap-tab">
          <h4 class="card-title border-bottom border-2 pb-2 mb-2"><i class="fas fa-procedures"></i> List Data Pasien Rawat Inap</h4>
          <div class="table-responsive">
            <table id="ranap_table" class="table table-hover table-bordered table-striped table-sm w-100">
              <thead>
                <tr>
                  <!-- <th class="text-center" width="50">No. Reg</th> -->
                  <th class="text-center" width="50">No. RM</th>
                  <th class="text-center" width="200">Nama Pasien / Alamat</th>
                  <th class="text-center" width="80">Lokasi</th>
                  <th class="text-center" width="90">Tgl. Registrasi</th>
                  <th class="text-center">Dokter</th>
                  <th class="text-center" width="50">Aksi</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
        <div class="tab-pane fade " id="pills-igd" role="tabpanel" aria-labelledby="pills-igd-tab">
          <h4 class="card-title border-bottom border-2 pb-2 mb-2"><i class="fas fa-ambulance"></i> List Data Pasien IGD Hari Ini</h4>
          <div class="table-responsive">
            <table id="igd_table" class="table table-hover table-bordered table-striped table-sm w-100">
              <thead>
                <tr>
                  <!-- <th class="text-center" width="50">No. Reg</th> -->
                  <th class="text-center" width="50">No. RM</th>
                  <th class="text-center" width="200">Nama Pasien / Alamat</th>
                  <th class="text-center" width="80">Lokasi</th>
                  <th class="text-center" width="90">Tgl. Registrasi</th>
                  <th class="text-center">Dokter</th>
                  <th class="text-center" width="50">Aksi</th>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Tanggal Resep <span class="text-danger">*</span></label>
        <div class="col-lg-7 col-md-7">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
            </div>
            <input type="text" class="form-control datetimepicker" name="tgl_catat" id="tgl_catat" value="<?= date('d-m-Y H:i:s') ?>" required>
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">No Registrasi</label>
        <div class="col-lg-8 col-md-9">
          <input type="text" class="form-control" name="reg_id" id="reg_id" value="" readonly>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">No. RM/CM</label>
        <div class="col-lg-8 col-md-9">
          <input type="text" class="form-control" name="pasien_id" id="pasien_id" value="" readonly>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Nama Pasien</label>
        <div class="col-lg-8 col-md-9">
          <input type="text" class="form-control" name="pasien_nm" id="pasien_nm" value="" readonly>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Lokasi</label>
        <div class="col-lg-8 col-md-9">
          <input type="text" class="form-control" name="lokasi_nm" id="lokasi_nm" value="" readonly>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Dokter</label>
        <div class="col-lg-8 col-md-9">
          <input type="text" class="form-control" name="dokter_nm" id="dokter_nm" value="" readonly>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-10 offset-md-7">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Buat Resep Baru</button>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
  var rajalTable, ranapTable, igdTable;
  $(document).ready(function() {
    //datatables
    rajalTable = $('#rajal_table').DataTable({
      'retrieve': true,
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "order": [],
      "ajax": {
        "url": "<?php echo site_url() . '/' . $nav['nav_url'] . '/ajax_list_pasien/search_rajal' ?>",
        "type": "POST"
      },
      "columnDefs": [{
          "targets": 0,
          "className": 'text-center',
          "orderable": true
        },
        {
          "targets": 1,
          "className": 'text-left',
          "orderable": true
        },
        {
          "targets": 2,
          "className": 'text-center',
          "orderable": false
        },
        {
          "targets": 3,
          "className": 'text-center',
          "orderable": false
        },
        {
          "targets": 4,
          "className": 'text-left',
          "orderable": false
        },
        {
          "targets": 5,
          "className": 'text-center',
          "orderable": false
        },
      ],
    });
    rajalTable.columns.adjust().draw();

    //datatables
    ranapTable = $('#ranap_table').DataTable({
      'retrieve': true,
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "order": [],
      "ajax": {
        "url": "<?php echo site_url() . '/' . $nav['nav_url'] . '/ajax_list_pasien/search_ranap' ?>",
        "type": "POST"
      },
      "columnDefs": [{
          "targets": 0,
          "className": 'text-center',
          "orderable": true
        },
        {
          "targets": 1,
          "className": 'text-left',
          "orderable": true
        },
        {
          "targets": 2,
          "className": 'text-center',
          "orderable": false
        },
        {
          "targets": 3,
          "className": 'text-center',
          "orderable": false
        },
        {
          "targets": 4,
          "className": 'text-left',
          "orderable": false
        },
        {
          "targets": 5,
          "className": 'text-center',
          "orderable": false
        },
      ],
    });
    ranapTable.columns.adjust().draw();

    //datatables
    igdTable = $('#igd_table').DataTable({
      'retrieve': true,
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "order": [],
      "ajax": {
        "url": "<?php echo site_url() . '/' . $nav['nav_url'] . '/ajax_list_pasien/search_igd' ?>",
        "type": "POST"
      },
      "columnDefs": [{
          "targets": 0,
          "className": 'text-center',
          "orderable": true
        },
        {
          "targets": 1,
          "className": 'text-left',
          "orderable": true
        },
        {
          "targets": 2,
          "className": 'text-center',
          "orderable": false
        },
        {
          "targets": 3,
          "className": 'text-center',
          "orderable": false
        },
        {
          "targets": 4,
          "className": 'text-left',
          "orderable": false
        },
        {
          "targets": 5,
          "className": 'text-center',
          "orderable": false
        },
      ],
    });
    igdTable.columns.adjust().draw();
  })
</script>