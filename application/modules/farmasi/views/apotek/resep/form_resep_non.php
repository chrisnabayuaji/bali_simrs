<?php $this->load->view('_js_resep_non'); ?>
<form id="form-resep-non" action="<?=$form_action?>" method="post" autocomplete="off">
  <input type="hidden" name="resep_id" id="resep_id" value="<?=$resep_id?>">
  <input type="hidden" name="rincian_id" id="rincian_id" value="<?=@$rinc['rincian_id']?>">
  <div class="form-group row">
    <label class="col-lg-3 col-md-3 col-form-label">Nama Obat <span class="text-danger">*</span></label>
    <div class="col-lg-9 col-md-8">
      <input type="text" class="form-control" name="obat_nm" id="obat_nm" value="<?=@$rinc['obat_nm']?>" required aria-invalid="false">
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-3 col-md-3 col-form-label">Alias Nama Obat</label>
    <div class="col-lg-9 col-md-8">
      <input type="text" class="form-control" name="obat_alias" id="obat_alias" value="<?=@$rinc['obat_alias']?>" aria-invalid="false">
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-3 col-md-3 col-form-label">Rute <span class="text-danger">*<span></label>
    <div class="col-lg-4 col-md-9">
      <select class="form-control chosen-select" name="rute_cd" id="rute_cd" required="">
        <?php foreach (get_parameter('rute_cd') as $r) : ?>
          <option value="<?= $r['parameter_cd'] ?>" <?=($r['parameter_cd'] == $rinc['rute_cd']) ? 'selected' : ''?>><?= $r['parameter_val'] ?></option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-3 col-md-3 col-form-label">Qty <span class="text-danger">*</span></label>
    <div class="col-lg-3 col-md-3">
      <input type="text" class="form-control" name="qty" id="qty" value="<?=@$rinc['qty']?>" required="" aria-invalid="false">
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-3 col-md-3 col-form-label">Aturan Pakai <span class="text-danger">*</span></label>
    <div class="col-lg-2 col-md-3">
      <input type="text" class="form-control" name="aturan_jml" id="aturan_jml" value="<?=@$rinc['aturan_jml']?>" required="" aria-invalid="false" placeholder="Jml">
    </div>
    <div class="col-lg-2 col-md-9">
      <select class="form-control chosen-select" name="satuan_cd" id="satuan_cd" required="">
        <?php foreach (get_parameter('satuan_cd') as $r) : ?>
          <option value="<?= $r['parameter_cd'] ?>" <?=($r['parameter_cd'] == @$rinc['satuan_cd']) ? 'selected' : ''?>><?= $r['parameter_val'] ?></option>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="col-lg-1 col-md-1 pt-3">
      <label><strong>Tiap</strong></label>
    </div>
    <div class="col-lg-2 col-md-3">
      <input type="text" class="form-control" name="aturan_waktu" id="aturan_waktu" value="<?=@$rinc['aturan_waktu']?>" required="" aria-invalid="false" placeholder="Jml">
    </div>
    <div class="col-lg-2 col-md-9">
      <select class="form-control chosen-select" name="waktu_cd" id="waktu_cd" required="">
        <?php foreach (get_parameter('waktu_cd') as $r) : ?>
          <option value="<?= $r['parameter_cd'] ?>" <?=($r['parameter_cd'] == @$rinc['waktu_cd']) ? 'selected' : ''?>><?= $r['parameter_val'] ?></option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-3 col-md-3 col-form-label">Jadwal Pemberian</label>
    <div class="col-lg-9 col-md-9">
      <input type="text" class="form-control" name="jadwal" id="jadwal" value="<?=@$rinc['jadwal']?>" aria-invalid="false">
      <small class="text-info">Tuliskan jam dan pisahkan dengan spasi. Cth: 07:00 15:00 22:00</small>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-3 col-md-3 col-form-label">Aturan Tambahan</label>
    <div class="col-lg-9 col-md-9">
      <input type="text" class="form-control" name="aturan_tambahan" id="aturan_tambahan" value="<?=@$rinc['aturan_tambahan']?>" aria-invalid="false">
    </div>
  </div>
  <div class="row">
    <div class="col-lg-9 offset-lg-3 p-0">
      <button type="submit" id="resep_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
      <button type="button" id="resep_cancel" class="btn btn-xs btn-secondary"><i class="fas fa-times"></i> Batal</button>
    </div>
  </div>
</form>