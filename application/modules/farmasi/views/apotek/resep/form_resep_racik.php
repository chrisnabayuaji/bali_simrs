<?php $this->load->view('_js_resep_racik') ?>
<form id="form-resep-racik" action="<?= $form_action ?>" method="post" autocomplete="off">
  <input type="hidden" name="resep_id" id="resep_id" value="<?= @$resep_id ?>">
  <input type="hidden" name="rincian_id" id="rincian_id" value="<?= @$rinc['rincian_id'] ?>">
  <div class="form-group row">
    <label class="col-lg-3 col-md-3 col-form-label">Nama Racikan <span class="text-danger">*</span></label>
    <div class="col-lg-9 col-md-8">
      <input type="text" class="form-control" name="obat_nm" id="obat_nm" value="<?= @$rinc['obat_nm'] ?>" required="" aria-invalid="false">
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-3 col-md-3 col-form-label">Sediaan <span class="text-danger">*<span></label>
    <div class="col-lg-4 col-md-9">
      <select class="form-control chosen-select" name="sediaan_cd" id="racik_sediaan_cd" required="">
        <?php foreach (get_parameter('sediaan_cd') as $r) : ?>
          <option value="<?= $r['parameter_cd'] ?>" <?= (@$rinc['racik_sediaan_cd'] == $r['parameter_cd']) ? 'selected' : '' ?>><?= $r['parameter_val'] ?></option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-3 col-md-3 col-form-label">Rute <span class="text-danger">*<span></label>
    <div class="col-lg-4 col-md-9">
      <select class="form-control chosen-select" name="rute_cd" id="racik_rute_cd" required="">
        <?php foreach (get_parameter('rute_cd') as $r) : ?>
          <option value="<?= $r['parameter_cd'] ?>" <?= (@$rinc['rute_cd'] == $r['parameter_cd']) ? 'selected' : '' ?>><?= $r['parameter_val'] ?></option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-3 col-md-3 col-form-label">Qty <span class="text-danger">*</span></label>
    <div class="col-lg-3 col-md-3">
      <input type="text" class="form-control" name="qty" id="qty" value="<?= @$rinc['qty'] ?>" required="" aria-invalid="false">
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-3 col-md-3 col-form-label">Aturan Pakai <span class="text-danger">*</span></label>
    <div class="col-lg-2 col-md-3">
      <input type="text" class="form-control" name="aturan_jml" id="aturan_jml" value="<?= @$rinc['aturan_jml'] ?>" required="" aria-invalid="false" placeholder="Jml">
    </div>
    <div class="col-lg-2 col-md-9">
      <select class="form-control chosen-select" name="satuan_cd" id="racik_satuan_cd" required="">
        <?php foreach (get_parameter('satuan_cd') as $r) : ?>
          <option value="<?= $r['parameter_cd'] ?>" <?= (@$rinc['satuan_cd'] == $r['parameter_cd']) ? 'selected' : '' ?>><?= $r['parameter_val'] ?></option>
        <?php endforeach; ?>
      </select>
    </div>
    <div class="col-lg-1 col-md-1 pt-3">
      <label><strong>Tiap</strong></label>
    </div>
    <div class="col-lg-2 col-md-3">
      <input type="text" class="form-control" name="aturan_waktu" id="aturan_waktu" value="<?= @$rinc['aturan_waktu'] ?>" required="" aria-invalid="false" placeholder="Jml">
    </div>
    <div class="col-lg-2 col-md-9">
      <select class="form-control chosen-select" name="waktu_cd" id="racik_waktu_cd" required="">
        <?php foreach (get_parameter('waktu_cd') as $r) : ?>
          <option value="<?= $r['parameter_cd'] ?>" <?= (@$rinc['waktu_cd'] == $r['parameter_cd']) ? 'selected' : '' ?>><?= $r['parameter_val'] ?></option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-3 col-md-3 col-form-label">Jadwal Pemberian</label>
    <div class="col-lg-9 col-md-9">
      <input type="text" class="form-control" name="jadwal" id="jadwal" value="<?= @$rinc['jadwal'] ?>" aria-invalid="false">
      <small class="text-info">Tuliskan jam dan pisahkan dengan spasi. Cth: 07:00 15:00 22:00</small>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-3 col-md-3 col-form-label">Aturan Tambahan</label>
    <div class="col-lg-9 col-md-9">
      <input type="text" class="form-control" name="aturan_tambahan" id="aturan_tambahan" value="<?= @$rinc['aturan_tambahan'] ?>" aria-invalid="false">
    </div>
  </div>
  <div class="border-bottom mb-2"></div>
  <h5>Komposisi</h5>
  <div class="row">
    <div class="col-12">
      <div class="row">
        <div class="col-4">
          <input type="text" class="form-control" name="komposisi" id="komposisi" aria-invalid="false" placeholder="Komposisi">
        </div>
        <div class="col-4">
          <input type="text" class="form-control" name="komposisi_alias" id="komposisi_alias" aria-invalid="false" placeholder="Alias">
        </div>
        <div class="col-2">
          <input type="text" class="form-control" name="komposisi_qty" id="komposisi_qty" aria-invalid="false" placeholder="Qty">
        </div>
        <div class="col-2">
          <button type="button" id="btn_add_komposisi" class="btn btn-xs btn-primary btn-block"><i class="fas fa-plus"></i></button>
        </div>
      </div>
      <table id="table_komposisi" class="table table-bordered table-striped table-sm mt-3">
        <thead>
          <tr>
            <th class="text-center" width="40">Aksi</th>
            <th class="text-center">Nama Komposisi</th>
            <th class="text-center" width="50">Qty</th>
          </tr>
        </thead>
        <tbody id="komposisi_data">
          <?php if(@$rinc['racik']): ?>
            <?php foreach (@$rinc['racik'] as $k) : ?>
              <tr>
                <td class="text-center text-top">
                  <button type="button" class="btn btn-danger btn-table btn-delete-komposisi"><i class="fas fa-trash-alt"></i></button>
                  <input type="hidden" name="komposisi_id[]" value="<?= @$k['komposisi_id'] ?>">
                </td>
                <td class="text-top">
                  <?= @$k['komposisi'] ?> (<?= @$k['komposisi_alias'] ?>)
                  <input type="hidden" name="komposisi[]" value="<?= @$k['komposisi'] ?>">
                  <input type="hidden" name="komposisi_alias[]" value="<?= @$k['komposisi_alias'] ?>">
                </td>
                <td class="text-right text-top">
                  <?= @$k['komposisi_qty'] ?>
                  <input type="hidden" name="komposisi_qty[]" value="<?= @$k['komposisi_qty'] ?>">
                </td>
              </tr>
            <?php endforeach; ?>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="border-bottom mt-4"></div>
  <div class="row mt-2">
    <div class="col-lg-9 offset-lg-3 p-0">
      <button type="submit" id="rincian_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
      <button type="button" id="rincian_cancel" class="btn btn-xs btn-secondary"><i class="fas fa-times"></i> Batal</button>
    </div>
  </div>
</form>