<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
  <style>
    .border-bottom {
      padding-bottom: 10px;
      position: relative;
      border-bottom: 2px solid #000;
    }

    .col {
      width: 175px;
      display: inline;
    }
  </style>
</head>

<body>
  <table style="width: 90%;" class="border-bottom">
    <tr>
      <td>
        <img src="<?= FCPATH . 'assets/images/icon/pt-cahaya-permata-medika.png' ?>" style="width: 55px; margin-left: 20px;">
      </td>
      <td style="text-align: center; width: 88%; padding-left: 8px;" valign="top">
        <font style="line-height: 1.3; font-size: 12px;">PT. CAHAYA PERMATA MEDIKA</font><br>
        <font style="line-height: 1.3; font-weight: bold; font-size: 15px;">RUMAH SAKIT IBU DAN ANAK PERMATA</font><br>
        <font style="line-height: 1.3; font-size: 10px;">Jl. Mayjen Sutoyo No. 75 Purworejo Telp. (0275) 321031</font><br>
        <font style="line-height: 1.3; font-size: 10px;">Email. rsiapermatapwr@gmail.com</font>
      </td>
      <td>
        <img src="<?= FCPATH . 'assets/images/icon/simrs-logo-rs.jpg' ?>" style="width: 55px; margin-right: 20px;">
      </td>
    </tr>
  </table>

  <table style="width: 90%; padding-top: 12px; padding-bottom: 5px;">
    <tr>
      <td>
        <font style="font-weight: bold; font-size: 12px;">RINCIAN OBAT</font>
      </td>
    </tr>
  </table>

  <table style="width: 100%;" class="border-bottom">
    <tr>
      <td valign="top" style="width: 50%">
        <table style="width: 100%;">
          <tr>
            <td valign="top" style="width: 40%; font-size: 12px;">No. RM</td>
            <td valign="top" style="width: 2%; font-size: 12px;">:</td>
            <td valign="top" style="width: 58%; font-size: 12px;"><?= @$main['pasien_id'] ?></td>
          </tr>
          <tr>
            <td valign="top" style="width: 40%; font-size: 12px;">Tanggal Masuk</td>
            <td valign="top" style="width: 2%; font-size: 12px;">:</td>
            <td valign="top" style="width: 58%; font-size: 12px;"><?= strtoupper(to_date_indo(@$main['tgl_registrasi'], 'date')) ?></td>
          </tr>
          <tr>
            <td valign="top" style="width: 40%; font-size: 12px;">Tanggal Keluar</td>
            <td valign="top" style="width: 2%; font-size: 12px;">:</td>
            <td valign="top" style="width: 58%; font-size: 12px;"><?= strtoupper(to_date_indo(@$main['tgl_pulang'], 'date')) ?></td>
          </tr>
          <tr>
            <td valign="top" style="width: 40%; font-size: 12px;">NIK</td>
            <td valign="top" style="width: 2%; font-size: 12px;">:</td>
            <td valign="top" style="width: 58%; font-size: 12px;"><?= @$main['nik'] ?></td>
          </tr>
          <tr>
            <td valign="top" style="width: 40%; font-size: 12px;">Nama Pasien</td>
            <td valign="top" style="width: 2%; font-size: 12px;">:</td>
            <td valign="top" style="width: 58%; font-size: 12px;"><?= (@$main['sebutan_cd'] != '') ? @$main['sebutan_cd'] . '.' : '' ?> <?= @$main['pasien_nm'] ?></td>
          </tr>
          <tr>
            <td valign="top" style="width: 40%; font-size: 12px;">Umur</td>
            <td valign="top" style="width: 2%; font-size: 12px;">:</td>
            <td valign="top" style="width: 58%; font-size: 12px;"><?= @$main['umur_thn'] ?> TH</td>
          </tr>
        </table>
      </td>
      <td valign="top" style="width: 50%">
        <table style="width: 100%;">
          <tr>
            <td valign="top" style="width: 35%; font-size: 12px;">Kelas / Ruang</td>
            <td valign="top" style="width: 2%; font-size: 12px;">:</td>
            <td valign="top" style="width: 63%; font-size: 12px;"><?= (@$main['kelas_nm'] != '') ? @$main['kelas_nm'] : '-' ?> / <?= (@$main['kamar_nm'] != '') ? @$main['kamar_nm'] : '-' ?></td>
          </tr>
          <tr>
            <td valign="top" style="width: 35%; font-size: 12px;">DPJP</td>
            <td valign="top" style="width: 2%; font-size: 12px;">:</td>
            <td valign="top" style="width: 63%; font-size: 12px;"><?= (@$main['dokter_nm'] != '') ? @$main['dokter_nm'] : '-' ?></td>
          </tr>
          <tr>
            <td valign="top" style="width: 35%; font-size: 12px;">Spesialisasi</td>
            <td valign="top" style="width: 2%; font-size: 12px;">:</td>
            <td valign="top" style="width: 63%; font-size: 12px;"><?= get_parameter_value('spesialisasi_cd', @$main['spesialisasi_cd']) ?></td>
          </tr>
          <tr>
            <td valign="top" style="width: 35%; font-size: 12px;">Penjamin Bayar</td>
            <td valign="top" style="width: 2%; font-size: 12px;">:</td>
            <td valign="top" style="width: 63%; font-size: 12px;"><?= (@$main['jenispasien_id'] != '') ? @$main['jenispasien_nm'] : '-' ?></td>
          </tr>
          <tr>
            <td valign="top" style="width: 35%; font-size: 12px;">NO. SEP</td>
            <td valign="top" style="width: 2%; font-size: 12px;">:</td>
            <td valign="top" style="width: 63%; font-size: 12px;"><?= (@$main['no_kartu'] != '') ? @$main['no_kartu'] : '-' ?></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>

  <table style="width: 100%; margin-top: 10px; margin-left: 2px;">
    <?php
    $i = 1;
    $tot_jumlah_akhir = 0;
    foreach ($list_obat as $row) :
      $tot_jumlah_akhir += $row['jumlah_akhir'];
    ?>
      <tr>
        <td valign="top" style="width: 60%">
          <table style="width: 100%;">
            <tr>
              <td valign="top" align="center" style="width: 8%; font-size: 12px;"><?= ($i++) ?></td>
              <td valign="top" align="left" style="width: 62%; font-size: 12px;"><?= $row['obat_nm'] ?></td>
              <td valign="top" align="center" style="width: 15%; font-size: 12px;"><?= num_id($row['qty']) ?></td>
            </tr>
          </table>
        </td>
        <td valign="top" style="width: 40%;">
          <table style="width: 100%;">
            <tr>
              <td valign="top" align="right" style="width: 40%; font-size: 12px;">Rp</td>
              <td valign="top" align="right" style="width: 60%; font-size: 12px;"><?= num_id($row['jumlah_akhir']) ?></td>
            </tr>
          </table>
        </td>
      </tr>
    <?php endforeach; ?>
    <tr>
      <td valign="top" style="width: 60%;"></td>
      <td valign="top" style="width: 40%;">
        <div style="border-bottom: 2px solid #000; margin-left: 80px; margin-top: 20px; margin-bottom: 10px;"></div>
      </td>
    </tr>
    <tr>
      <td valign="top" style="width: 60%;">
        <font style="font-weight: bold; font-size: 12px;">TOTAL RINCIAN OBAT</font>
        <font style="font-weight: bold; margin-left: 130px; font-size: 12px;"><span style="margin-top: -1px;">--------------------------</span><span>></span></font>
      </td>
      <td valign="top" style="width: 40%;">
        <table style="width: 100%;">
          <tr>
            <td valign="top" align="right" style="width: 40%; font-weight: bold; font-size: 12px;">Rp</td>
            <td valign="top" align="right" style="width: 60%; font-weight: bold; font-size: 12px;"><?= num_id($tot_jumlah_akhir) ?></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td valign="top" style="width: 60%;">
        <font style="font-weight: bold; font-style: italic; font-size: 12px;">(TERBILANG : <?= strtoupper(terbilang($tot_jumlah_akhir)) ?>)</font>
      </td>
      <td valign="top" style="width: 40%;">
      </td>
    </tr>
    <tr>
      <td valign="top" colspan="2">
        <div style="margin-top: 35px; margin-bottom: 35px;"></div>
      </td>
    </tr>
    <tr>
      <td valign="top" style="width: 60%;"></td>
      <td valign="top" align="left" style="width: 40%;">
        <table style="width: 100%;">
          <tr>
            <td valign="top" align="center" style="width: 100%; font-size: 12px;">Dokter Penanggungjawab</td>
          </tr>
          <tr>
            <td valign="top" align="center" style="width: 100%; font-size: 12px;"><br><br><br><br></td>
          </tr>
          <tr>
            <td valign="top" align="center" style="width: 100%; font-size: 12px;"><?= @$main['dokter_nm'] ?></td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>

</html>