<?php $this->load->view('_js'); ?>
<div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
<form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?= $form_action ?>" class="needs-validation" novalidate autocomplete="off">
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline col-title">
          <div class="col-back">
            <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
          </div>
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            <?= $nav['nav_nm'] ?>
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-warehouse"></i> Gudang</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item active"><span>Form</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>

    <div class="row full-page mt-4">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <div class="row">
              <input type="hidden" name="distribusi_id" value="<?= @$main['distribusi_id'] ?>">
              <div class="col-5">
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label mt-n3">Lokasi Stok Unit Obat <span class="text-danger">*<span></label>
                  <div class="col-lg-6 col-md-9">
                    <select class="form-control form-control-sm chosen-select" name="lokasi_id" id="lokasi_id" required="">
                      <option value="">- Pilih -</option>
                      <?php foreach ($lokasi as $lok) : ?>
                        <option value="<?= $lok['lokasi_id'] ?>" <?php if (@$main['lokasi_id'] == $lok['lokasi_id']) {
                                                                    echo 'selected';
                                                                  } ?>><?= $lok['lokasi_nm'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label mt-n3">Tgl.Order Stok Unit<span class="text-danger">*<span></label>
                  <div class="col-lg-5 col-md-5">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datetimepicker" name="tgl_order_depo" id="tgl_order_depo" value="<?php if (@$main) {
                                                                                                                                echo to_date(@$main['tgl_order_depo'], '', 'date', '');
                                                                                                                              } else {
                                                                                                                                echo date('d-m-Y H:i:s');
                                                                                                                              } ?>" required aria-invalid="false">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Keterangan <br> Order</label>
                  <div class="col-lg-8 col-md-9">
                    <textarea class="form-control form-control-sm" name="keterangan_order_depo" id="keterangan_order_depo" rows="3"><?= @$main['keterangan_order_depo'] ?></textarea>
                  </div>
                </div>
              </div>
              <div class="col-5">
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">No.Distribusi <span class="text-danger">*<span></label>
                  <div class="col-lg-6 col-md-6">
                    <input type="text" class="form-control" name="no_distribusi" id="no_distribusi" value="<?= @$main['no_distribusi'] ?>" required aria-invalid="false">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Tgl.Distribusi <span class="text-danger">*<span></label>
                  <div class="col-lg-5 col-md-5">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datetimepicker" name="tgl_distribusi" id="tgl_distribusi" value="<?php if (@$main) {
                                                                                                                                echo to_date(@$main['tgl_distribusi'], '', 'date', '');
                                                                                                                              } else {
                                                                                                                                echo date('d-m-Y H:i:s');
                                                                                                                              } ?>" required aria-invalid="false">
                    </div>
                  </div>
                </div>
                <div class="form-group row mb-2">
                  <label class="col-lg-3 col-md-3 col-form-label">Keterangan Distribusi</label>
                  <div class="col-lg-8 col-md-9">
                    <textarea class="form-control form-control-sm" name="keterangan_distribusi" id="keterangan_distribusi" rows="3"><?= @$main['keterangan_distribusi'] ?></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Status Distribusi <span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-4">
                    <select class="form-control form-control-sm chosen-select" name="distribusi_st" id="distribusi_st" required="">
                      <option value="">- Pilih -</option>
                      <option value="1" <?php if (@$main['distribusi_st'] == 1) {
                                          echo 'selected';
                                        } ?>>Terkirim</option>
                      <option value="2" <?php if (@$main['distribusi_st'] == 2) {
                                          echo 'selected';
                                        } ?>>Pending</option>
                      <option value="0" <?php if (@$main['distribusi_st'] == 0) {
                                          echo 'selected';
                                        } ?>>Draft</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-5">
                <div class="card border-none no-shadow">
                  <div class="card-body">
                    <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-file"></i> Form Data Distribusi Barang</h4>
                    <div class="form-group row mb-2">
                      <input type="hidden" id="stok_id" value="">
                      <input type="hidden" id="stokdepo_id" value="">
                      <input type="hidden" id="index" value="">
                      <input type="hidden" id="status" value="new">
                      <label class="col-lg-3 col-md-3 col-form-label">Obat/Alkes <span class="text-danger">*</span></label>
                      <div class="col-lg-8 col-md-3">
                        <select class="form-control" id="obat_id">
                          <option value="">- Pilih -</option>
                        </select>
                      </div>
                      <div class="col-lg-1 col-md-1">
                        <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax/search_stok' ?>" modal-title="List Data Obat" modal-size="lg" class="btn btn-xs btn-default modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Obat" data-original-title="Cari Obat"><i class="fas fa-search"></i></a>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-lg-3 col-md-3 col-form-label">Qty <span class="text-danger">*<span></label>
                      <div class="col-lg-3 col-md-3">
                        <input type="number" class="form-control" id="qty" value="1" min="1">
                        <input type="hidden" class="form-control" id="qty_sebelum_edit">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-lg-3 col-md-3 col-form-label">Keterangan <span class="text-danger">*<spa></label>
                      <div class="col-lg-9 col-md-9">
                        <textarea class="form-control form-control-sm" id="keterangan_obat" rows="2"></textarea>
                      </div>
                    </div>
                    <div class="form-group row mt-2">
                      <label class="col-lg-3 col-md-3 col-form-label"></label>
                      <div class="col-lg-9 col-md-9">
                        <span type="button" id="item_action" class="btn btn-xs btn-primary" onclick="save_item()"><i class="fas fa-save"></i> Simpan</span>
                        <span type="button" id="item_cancel" class="btn btn-xs btn-secondary" onclick="reset_item()"><i class="fas fa-times"></i> Batal</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col">
                <div class="card border-none no-shadow">
                  <div class="card-body">
                    <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-list"></i> Item Distribusi Barang</h4>
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered table-sm">
                        <thead>
                          <tr>
                            <th class="text-center" width="50">No.</th>
                            <th class="text-center" width="60">Aksi</th>
                            <th class="text-center">Nama Item Barang</th>
                            <th class="text-center" width="50">Satuan</th>
                            <th class="text-center" width="50">Qty</th>
                            <th class="text-center" width="200">Keterangan</th>
                          </tr>
                        </thead>
                        <tbody id="item_list"></tbody>
                      </table>
                    </div>
                    <div id="delete_list"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="btn-form">
    <div class="btn-form-action">
      <div class="btn-form-action-bottom w-100 small-text clearfix">
        <div class="col-lg-2">
          <a href="<?= site_url() . '/' . $nav['nav_url'] ?>" class="btn btn-xs btn-secondary"><i class="mdi mdi-keyboard-backspace"></i> Kembali</a>
        </div>
        <div class="col-lg-2 offset-8">
          <button type="submit" class="float-right btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
          <button type="reset" onClick="window.location.reload();" class="float-right btn btn-xs btn-secondary btn-clear mr-2"><i class="fas fa-sync-alt"></i> Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>