<ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-barangeksis-tab" data-toggle="pill" href="#pills-barangeksis" role="tab" aria-controls="pills-barangeksis" aria-selected="false">Barang Eksis</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " id="pills-barangtidakeksis-tab" data-toggle="pill" href="#pills-barangtidakeksis" role="tab" aria-controls="pills-barangtidakeksis" aria-selected="true">Barang Tidak Eksis</a>
  </li>
</ul>
<div class="tab-content p-0" id="pills-tabContent" style="border:0px !important">
  <div class="tab-pane fade show active" id="pills-barangeksis" role="tabpanel" aria-labelledby="pills-barangeksis-tab">
    <div class="table-responsive">
      <table id="stok_table_eksis" class="table table-hover table-bordered table-striped table-sm w-100">
        <thead>
          <tr>
            <th class="text-center" width="20">No.</th>
            <th class="text-center" width="100">Kode</th>
            <th class="text-center">Nama Barang</th>
            <th class="text-center">No.Batch</th>
            <th class="text-center">Satuan</th>
            <th class="text-center">Tgl.Expired</th>
            <th class="text-center">Stok Akhir</th>
            <th class="text-center" width="60">Aksi</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
  <div class="tab-pane fade " id="pills-barangtidakeksis" role="tabpanel" aria-labelledby="pills-barangtidakeksis-tab">
    <div class="table-responsive">
      <table id="stok_table_tidak_eksis" class="table table-hover table-bordered table-striped table-sm w-100">
        <thead>
          <tr>
            <th class="text-center" width="20">No.</th>
            <th class="text-center" width="100">Kode</th>
            <th class="text-center">Nama Barang</th>
            <th class="text-center">No.Batch</th>
            <th class="text-center">Satuan</th>
            <th class="text-center">Tgl.Expired</th>
            <th class="text-center">Stok Akhir</th>
            <th class="text-center" width="60">Aksi</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</div>

<script type="text/javascript">
  var obatTableEksis;
  $(document).ready(function () {
    //datatables
    obatTableEksis = $('#stok_table_eksis').DataTable({
      "autoWidth" : false,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
      "ajax": {
          "url": "<?php echo site_url().'/'.$nav['nav_url'].'/ajax/search_data'?>",
          "type": "POST"
      },      
      "columnDefs": [
        {"targets": 0, "className": 'text-center', "orderable" : false},
        {"targets": 1, "className": 'text-center', "orderable" : false},
        {"targets": 2, "className": 'text-left', "orderable" : false},
        {"targets": 3, "className": 'text-center', "orderable" : false},
        {"targets": 4, "className": 'text-center', "orderable" : false},
        {"targets": 5, "className": 'text-center', "orderable" : false},
        {"targets": 6, "className": 'text-center', "orderable" : false},
        {"targets": 7, "className": 'text-center', "orderable" : false},
      ],
    });
    obatTableEksis.columns.adjust().draw();
    
    //datatables
    obatTableTidakEksis = $('#stok_table_tidak_eksis').DataTable({
      "autoWidth" : false,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
      "ajax": {
          "url": "<?php echo site_url().'/'.$nav['nav_url'].'/ajax/search_data_tidak'?>",
          "type": "POST"
      },      
      "columnDefs": [
        {"targets": 0, "className": 'text-center', "orderable" : false},
        {"targets": 1, "className": 'text-center', "orderable" : false},
        {"targets": 2, "className": 'text-left', "orderable" : false},
        {"targets": 3, "className": 'text-center', "orderable" : false},
        {"targets": 4, "className": 'text-center', "orderable" : false},
        {"targets": 5, "className": 'text-center', "orderable" : false},
        {"targets": 6, "className": 'text-center', "orderable" : false},
        {"targets": 7, "className": 'text-center', "orderable" : false},
      ],
    });
    obatTableTidakEksis.columns.adjust().draw();
  })
</script>