<script type="text/javascript">
  $(document).ready(function() {
    $("#form-data").validate({
      rules: {},
      messages: {},
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    $('.datepicker').daterangepicker({
      // maxDate: new Date(),
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY'
      },
      isInvalidDate: function(date) {
        return '';
      }
    });

    $('.datepicker').val('');
  });

  function get_obat(o) {
    var res = o.val();
    if (res != '') {
      $.ajax({
        type: 'post',
        url: '<?= site_url($nav['nav_url']) ?>/ajax_obat/obat_get',
        data: 'obat_id=' + res,
        dataType: 'json',
        success: function(data) {
          <?php if (@$id == '') : ?>
            $("#no_batch").val(data.no_batch);
            $("#satuan").val(data.satuan_nm);
            $("#harga_beli").val(data.harga_beli);
            $("#harga_jual").val(data.harga_jual);
          <?php endif; ?>
          hitung_total();
        }
      });
    }
  }
</script>