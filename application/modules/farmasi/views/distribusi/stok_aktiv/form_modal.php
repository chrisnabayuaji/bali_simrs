<!-- js -->
<?php $this->load->view('_js') ?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?= $form_action ?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Lokasi</label>
        <div class="col-lg-8 col-md-4">
          <select class="form-control form-control-sm chosen-select" id="lokasi_id" name="lokasi_id">
            <?php foreach ($lokasi as $r) : ?>
              <option value="<?= $r['lokasi_id'] ?>" <?= (@$main['lokasi_id'] == $r['lokasi_id']) ? 'selected' : '' ?>><?= $r['lokasi_nm'] ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Nama item barang</label>
        <div class="col-lg-8 col-md-4">
          <select class="form-control form-control-sm chosen-select" id="obat_id" name="obat_id" onchange="get_obat($(this))">
            <?php foreach ($obat as $r) : ?>
              <option value="<?= $r['obat_id'] ?>" <?= (@$main['obat_id'] == $r['obat_id']) ? 'selected' : '' ?>><?= $r['obat_nm'] ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">No.Batch</label>
        <div class="col-lg-4 col-md-4">
          <input type="text" name="no_batch" id="no_batch" class="form-control" value="<?= @$main['no_batch'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Satuan</label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" id="satuan" value="<?= @$main['satuan_nm'] ?>" disabled>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Tgl.Expired</label>
        <div class="col-lg-3 col-md-3">
          <input type="text" name="tgl_expired" class="form-control datepicker" value="<?= (@$main['tgl_expired'] == '0000-00-00') ? '' : to_date(@$main['tgl_expired']) ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Harga Beli</label>
        <div class="col-lg-4 col-md-4">
          <input type="text" name="harga_beli" id="harga_beli" class="form-control" value="<?= @$main['harga_beli'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Harga Jual</label>
        <div class="col-lg-4 col-md-4">
          <input type="text" name="harga_jual" id="harga_jual" class="form-control" value="<?= @$main['harga_jual'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Stok Awal <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="stok_awal" value="<?= @$main['stok_awal'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Stok Penerimaan <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="stok_penerimaan" value="<?= @$main['stok_penerimaan'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Stok Terpakai <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="stok_terpakai" value="<?= @$main['stok_terpakai'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Retur Keluar <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="stok_retur_keluar" value="<?= @$main['stok_retur_keluar'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Stok Akhir <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="stok_akhir" value="<?= @$main['stok_akhir'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Stok Opname <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="stok_opname" value="<?= @$main['stok_opname'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Status</label>
        <div class="col-lg-3 col-md-5">
          <select class="chosen-select custom-select w-100" name="stok_st">
            <option value="1" <?php if (@$main['stok_st'] == '1') echo 'selected' ?>>Aktif</option>
            <option value="0" <?php if (@$main['stok_st'] == '0') echo 'selected' ?>>Tidak Aktif</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>