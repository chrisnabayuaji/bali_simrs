<div class="row">
  <div class="col">
    <h4 class="card-title border-bottom border-2 pb-2 mb-3">Informasi Item Barang</h4>
    <div class="form-group row">
      <label class="col-lg-3 col-md-4 col-form-label">Kode</label>
      <div class="col-lg-3 col-md-3">
        <input type="text" class="form-control" value="<?=@$main['obat_id']?>" disabled>
      </div>
    </div>
    <div class="form-group row mb-2">
      <label class="col-lg-3 col-md-4 col-form-label">Nama Item Barang</label>
      <div class="col-lg-9 col-md-9">
        <textarea class="form-control" rows="5" disabled=""><?=@$main['obat_nm']?></textarea>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-lg-3 col-md-4 col-form-label">No.Batch</label>
      <div class="col-lg-4 col-md-4">
        <input type="text" class="form-control" value="<?=@$main['no_batch']?>" disabled>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-lg-3 col-md-4 col-form-label">Satuan</label>
      <div class="col-lg-3 col-md-3">
        <input type="text" class="form-control" value="<?=@$main['satuan_nm']?>" disabled>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-lg-3 col-md-4 col-form-label">Tgl.Expired</label>
      <div class="col-lg-3 col-md-3">
        <input type="text" class="form-control" value="<?=to_date(@$main['tgl_expired'])?>" disabled>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-lg-3 col-md-4 col-form-label">Lokasi Depo Obat</label>
      <div class="col-lg-6 col-md-6">
        <input type="text" class="form-control" value="<?=@$main['lokasi_nm']?>" disabled>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-lg-3 col-md-4 col-form-label">Harga Beli</label>
      <div class="col-lg-3 col-md-3">
        <input type="text" class="form-control" value="<?=num_id(@$main['harga_beli'])?>" disabled>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-lg-3 col-md-4 col-form-label">Harga Jual</label>
      <div class="col-lg-3 col-md-3">
        <input type="text" class="form-control" value="<?=num_id(@$main['harga_jual'])?>" disabled>
      </div>
    </div>
  </div>
  <div class="col">
    <h4 class="card-title border-bottom border-2 pb-2 mb-3">Informasi Posisi Stok</h4>
    <div class="form-group row">
      <label class="col-lg-3 col-md-4 col-form-label">Stok Awal</label>
      <div class="col-lg-3 col-md-3">
        <input type="text" class="form-control" value="<?=@$main['stok_awal']?>" disabled>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-lg-3 col-md-4 col-form-label">Stok Penerimaan</label>
      <div class="col-lg-3 col-md-3">
        <input type="text" class="form-control" value="<?=@$main['stok_penerimaan']?>" disabled>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-lg-3 col-md-4 col-form-label">Stok Distribusi</label>
      <div class="col-lg-3 col-md-3">
        <input type="text" class="form-control" value="<?=@$main['stok_distribusi']?>" disabled>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-lg-3 col-md-4 col-form-label">Stok Retur M</label>
      <div class="col-lg-3 col-md-3">
        <input type="text" class="form-control" value="<?=@$main['stok_retur_masuk']?>" disabled>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-lg-3 col-md-4 col-form-label">Stok Retur K</label>
      <div class="col-lg-3 col-md-3">
        <input type="text" class="form-control" value="<?=@$main['stok_retur_keluar']?>" disabled>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-lg-3 col-md-4 col-form-label">Stok Musnah</label>
      <div class="col-lg-3 col-md-3">
        <input type="text" class="form-control" value="<?=@$main['stok_musnah']?>" disabled>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-lg-3 col-md-4 col-form-label">Stok Akhir</label>
      <div class="col-lg-3 col-md-3">
        <input type="text" class="form-control" value="<?=@$main['stok_akhir']?>" disabled>
      </div>
    </div>
    <div class="form-group row">
      <label class="col-lg-3 col-md-4 col-form-label">Stok Opname</label>
      <div class="col-lg-3 col-md-3">
        <input type="text" class="form-control" value="<?=@$main['stok_opname']?>" disabled>
      </div>
    </div>
  </div>
</div>
<div class="border-top border-2 mt-2 pt-2 pb-0">
  <div class="row">
    <div class="offset-lg-11 offset-md-11">
      <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Tutup</button>
    </div>
  </div>
</div>