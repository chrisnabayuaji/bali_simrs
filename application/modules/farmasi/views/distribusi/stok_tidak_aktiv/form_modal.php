<!-- js -->
<?php $this->load->view('_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Kode</label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" value="<?=@$main['obat_id']?>" disabled>
        </div>
      </div>
      <div class="form-group row mb-2">
        <label class="col-lg-4 col-md-4 col-form-label">Nama Item Barang</label>
        <div class="col-lg-8 col-md-8">
          <textarea class="form-control" rows="5" disabled=""><?=@$main['obat_nm']?></textarea>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">No.Batch</label>
        <div class="col-lg-4 col-md-4">
          <input type="text" class="form-control" value="<?=@$main['no_batch']?>" disabled>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Satuan</label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" value="<?=@$main['satuan_nm']?>" disabled>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Tgl.Expired</label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" value="<?=to_date(@$main['tgl_expired'])?>" disabled>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Stok Awal <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="stok_awal" value="<?=@$main['stok_awal']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Stok Opname <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="stok_opname" value="<?=@$main['stok_opname']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Status</label>
        <div class="col-lg-3 col-md-5">
          <select class="chosen-select custom-select w-100" name="stok_st">
            <option value="1" <?php if(@$main['stok_st'] == '1') echo 'selected'?>>Aktif</option>
            <option value="0" <?php if(@$main['stok_st'] == '0') echo 'selected'?>>Tidak Aktif</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>