<script type="text/javascript">
  var item_list = [];
  var item;
  var no = 0;
  $(document).ready(function() {
    <?php if ($id == null || $id == '') : ?>
      $("#bayar_st").val('').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    <?php endif; ?>
    // fetch_list();
    $("#form-data").validate({
      rules: {
        supplier_id: {
          valueNotEquals: ''
        },
      },
      messages: {
        supplier_id: {
          valueNotEquals: 'Pilih salah satu!'
        }
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-n1');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        if ($(".form-control").hasClass("no-validation") == false) {
          $(element).addClass("is-valid").removeClass("is-invalid");
        }
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    rincian_penerimaan_data();

    <?php if ($id != null) : ?>
      surat_pesanan_fill('<?= @$main['order_id'] ?>');
    <?php endif; ?>

    <?php if (@$main['tgl_bayar'] == '0000-00-00' || @$main['tgl_bayar'] == null || @$main['tgl_bayar'] == '') : ?>
      $("#tgl_bayar").val("");
    <?php endif; ?>

    <?php if (@$main['tgl_jatem'] == '0000-00-00' || @$main['tgl_jatem'] == null || @$main['tgl_jatem'] == '') : ?>
      $("#tgl_jatem").val("");
    <?php endif; ?>
  })

  function autocomplete_obat() {
    // autocomplete
    $('.obat_id').select2({
      minimumInputLength: 2,
      width: '280px',
      ajax: {
        url: "<?= site_url($nav['nav_url']) ?>/ajax_obat/autocomplete",
        dataType: "json",

        data: function(params) {
          return {
            obat_nm: params.term
          };
        },
        processResults: function(data) {
          return {
            results: data
          }
        }
      }
    });
    $('.obat_id').on('select2:select', function(e) {
      var data = e.params.data;
      // console.log(data);
      var raw = data.id.split('#');
      var no = $(this).data('no');
      $("#lbl_satuan_" + no).html(raw[2]);
      $("#obat_id_" + no).val(raw[0]);
      $("#obat_nm_" + no).val(raw[1]);
      $("#jenisbarang_cd_" + no).val(raw[3]);
      $("#satuan_cd_" + no).val(raw[4]);
      // console.log(raw[2]);
    });
  }

  function surat_pesanan_fill(id) {
    $.ajax({
      type: 'post',
      url: '<?= site_url($nav['nav_url'] . '/ajax_surat_pesanan/surat_pesanan_fill') ?>',
      dataType: 'json',
      data: 'order_id=' + id,
      success: function(data) {
        $("#order_id").val(data.order_id);
        $("#no_order").val(data.no_order);
        $("#tgl_order").val(to_date(data.tgl_order, '-', 'date', ''));
        $("#keteranganorder").val(data.keteranganorder);
        $('#supplier_id').val(data.supplier_id);
        $('#supplier_id').trigger('change');
        process_rincian_penerimaan(data.order_id);
        $('#note').removeClass('d-none');
        $("#myModal").modal('hide');
      }
    })
  }

  function rincian_pesanan_fill(data) {
    $('#note').removeClass('d-none');

    var nomor = $("#nomor").val();
    if (nomor == '') {
      $("#nomor").val('1');
    } else {
      $("#nomor").val(parseInt(nomor) + 1);
    }

    no++;
    var html = '<tr>' +
      '<td class="text-center" width="36">' + (no) + '</td>' +
      '<td class="text-center" width="50">' +
      '<div class="d-flex justify-content-center">' +
      '<div class="form-check form-check-primary text-center" style="margin-left: -5px !important;">' +
      '<label class="form-check-label">' +
      '<input type="checkbox" class="form-check-input sesuai sesuai_' + no + '" name="is_sesuai_order_' + (no) + '" value="1">' +
      '<i class="input-helper"></i></label>' +
      '</div>' +
      '</div>' +
      '</td>' +
      '<td width="280">' +
      (data[1]) +
      '<input type="hidden" name="penerimaanrincian_id_' + (no) + '" value=""/>' +
      '<input type="hidden" name="stok_id_' + no + '" value=""/>' +
      '<input type="hidden" name="obat_id_' + (no) + '" id="obat_id_' + no + '" value="' + (data[0]) + '">' +
      '<input type="hidden" name="obat_nm_' + no + '" id="obat_nm_' + no + '" value="' + (data[1]) + '"/>' +
      '<input type="hidden" name="jenisbarang_cd_' + no + '" id="jenisbarang_cd_' + no + '" value="' + (data[5]) + '"/>' +
      '<input type="hidden" name="satuan_cd_' + no + '" id="satuan_cd_' + no + '" value="' + (data[2]) + '"/>' +
      '<input type="hidden" name="keteranganpenerimaan_rinc_' + no + '" value=""/>' +
      '</td>' +
      // '<td class="text-center" width="80">' +
      // '<span id="lbl_satuan_' + no + '">' + (data[3]) + '</span>' +
      // '</td>' +
      '<td class="text-center" width="65">' +
      '<div class="d-flex justify-content-center">' +
      '<div class="form-check form-check-primary text-center" style="margin-left: -5px !important;">' +
      '<label class="form-check-label">' +
      '<input type="checkbox" class="form-check-input" id="injeksi_' + no + '" onchange="hitung(' + no + ')" value="1">' +
      '<i class="input-helper"></i></label>' +
      '</div>' +
      '</div>' +
      '</td>' +
      '<td class="text-center" width="60">' +
      '<div id="qty_pesanan_' + no + '">' + (data[4]) + '</div>' +
      '<input type="hidden" name="qty_pesanan_' + no + '" value="' + (data[4]) + '"/>' +
      '</td>' +
      '<td class="text-center" width="60">' +
      '<input type="text" class="form-control no-validation text-center readonly readonly_' + no + '" name="qty_diterima_' + (no) + '" id="qty_diterima_' + no + '" onkeyup="hitung(' + no + ')" value="0" disabled>' +
      '</td>' +
      '<td class="text-center" width="90">' +
      '<input type="text" class="form-control datepicker-' + no + ' datepicker-empty-' + no + ' no-validation readonly readonly_' + no + '" name="tgl_expired_' + (no) + '" id="tgl_expired_' + no + '" onkeyup="hitung(' + no + ')" value="" disabled>' +
      '</td>' +
      '<td class="text-center" width="100">' +
      '<input type="text" class="form-control no-validation readonly readonly_' + no + '" name="no_batch_' + (no) + '" id="no_batch_' + no + '" onkeyup="hitung(' + no + ')" value="" disabled>' +
      '</td>' +
      '<td class="text-center" width="90">' +
      '<input type="text" class="form-control no-validation autonumeric readonly readonly_' + no + '" dir="rtl" name="harga_beli_' + (no) + '" id="harga_beli_' + no + '" onkeyup="hitung(' + no + ')" value="0" disabled>' +
      '</td>' +
      '<td class="text-center" width="90">' +
      '<input type="text" class="form-control no-validation autonumeric" dir="rtl" name="sub_total_awal_' + (no) + '" id="sub_total_awal_' + no + '" onkeyup="hitung(' + no + ')" value="0" readonly>' +
      '</td>' +
      '<td class="text-center" width="70">' +
      '<input type="text" class="form-control no-validation text-center readonly readonly_' + no + '" name="prs_diskon_' + (no) + '" id="prs_diskon_' + no + '" onkeyup="hitung(' + no + ')" value="0" disabled>' +
      '<input type="hidden" class="form-control no-validation text-center" name="nom_diskon_' + (no) + '" id="nom_diskon_' + no + '" onkeyup="hitung(' + no + ')" value="0">' +
      '</td>' +
      '<td class="text-center" width="70">' +
      '<input type="text" class="form-control no-validation text-center readonly readonly_' + no + '" name="prs_ppn_' + (no) + '" id="prs_ppn_' + no + '" onkeyup="hitung(' + no + ')" value="0" disabled>' +
      '<input type="hidden" class="form-control no-validation text-center" name="nom_ppn_' + (no) + '" id="nom_ppn_' + no + '" onkeyup="hitung(' + no + ')" value="0">' +
      '</td>' +
      '<td class="text-center" width="90">' +
      '<input type="text" class="form-control no-validation autonumeric" dir="rtl" name="sub_total_akhir_' + (no) + '" id="sub_total_akhir_' + no + '" onkeyup="hitung(' + no + ')" value="0" readonly>' +
      '</td>' +
      '<td class="text-center" width="90">' +
      '<input type="text" class="form-control no-validation autonumeric readonly readonly_' + no + '" dir="rtl" name="harga_jual_' + (no) + '" id="harga_jual_' + no + '" onkeyup="hitung(' + no + ')" value="0" disabled>' +
      '</td>' +
      // '<td class="text-center" width="80">' +
      // '<input type="text" class="form-control no-validation readonly readonly_' + no + '" name="keteranganpenerimaan_rinc_' + (no) + '" id="keteranganpenerimaan_rinc_' + no + '" onkeyup="hitung(' + no + ')" value="" disabled>' +
      // '</td>' +
      '</tr>';
    $("#rincian_penerimaan_data").append(html);
    js(no, '');
    autocomplete_obat();

    $("#myModal").modal('hide');
  }

  function process_rincian_penerimaan(order_id = '') {
    item_list.splice(0, item_list.length);
    no = 1;
    $('#rincian_penerimaan_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.ajax({
      type: 'post',
      url: '<?= site_url() . '/' . $nav['nav_url'] . '/ajax_surat_pesanan/rincian_penerimaan_data/' . $id ?>',
      data: 'order_id=' + order_id,
      async: false,
      dataType: 'json',
      success: function(data) {
        list_data = [];
        $('#nomor').val(data.main.length);
        $.each(data.main, function(index, value) {
          var data_list = {
            penerimaanrincian_id: value.penerimaanrincian_id,
            stok_id: value.stok_id,
            obat_id: value.obat_id,
            obat_nm: value.obat_nm,
            jenisbarang_cd: value.jenisbarang_cd,
            nama_item: value.obat_nm,
            satuan: value.satuan,
            satuan_cd: value.satuan_cd,
            qty_pesanan: value.qty_pesanan,
            qty_diterima: value.qty_diterima,
            tgl_expired: value.convert_tgl_expired,
            no_batch: value.no_batch,
            harga_beli: value.harga_beli,
            sub_total_awal: value.sub_total_awal,
            prs_diskon: value.prs_diskon,
            nom_diskon: value.nom_diskon,
            prs_ppn: value.prs_ppn,
            nom_ppn: value.nom_ppn,
            sub_total_akhir: value.sub_total_akhir,
            harga_jual: value.harga_jual,
            keteranganpenerimaan_rinc: value.keteranganpenerimaan_rinc,
            is_sesuai_order: value.is_sesuai_order,
          };
          item_list.push(data_list);
        });
        rincian_penerimaan_data();
      }
    });
  }

  function samakan() {
    for (var i = 1; i <= no; i++) {
      var qty_pesanan = $('#qty_pesanan_' + i).html();
      $('#qty_diterima_' + i).val(qty_pesanan);
    }
  }

  function check_all() {
    var clicked = false;
    $(".sesuai").prop("checked", !clicked);
    clicked = !clicked;
    if (clicked) {
      $(".readonly").attr("disabled", false);
    }
  }

  function js(nomor, tgl_expired = '') {
    if (tgl_expired == '') {
      $(document).ready(function() {
        $(".datepicker-empty-" + nomor).val("");
      });
    }
    $(document).ready(function() {
      $(".sesuai_" + nomor).on('click', function() {
        if ($(this).is(':checked')) {
          $(".readonly_" + nomor).attr("disabled", false);
        } else {
          $(".readonly_" + nomor).attr("disabled", true).val('');
          $("#select-obat-id-" + nomor).val('').trigger('change');
        }
        hitung(nomor);
      });
    });
    $(".chosen-select").select2();
    $(".select2-selection").css({
      "height": "25px",
    });
    $(".select2-selection__rendered").css({
      "line-height": "17px",
    });
    $(".select2-selection__arrow").css({
      "height": "17px",
    });
    $(".autonumeric").autoNumeric({
      aSep: ".",
      aDec: ",",
      vMax: "999999999999999",
      vMin: "0"
    });
    $(".datepicker-" + nomor).daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: "Clear",
        format: "DD-MM-YYYY"
      },
      isInvalidDate: function(date) {
        return "";
      }
    });
  }

  function empty_datepicker(nomor) {
    $(document).ready(function() {
      $(".datepicker-emptys-" + nomor).val("");
    });
  }

  function rincian_penerimaan_data() {
    $('#rincian_penerimaan_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $('#rincian_penerimaan_data').html('');
    if (item_list.length == 0) {
      var html = '<tr>' +
        '<td id="item_tidak_ada" class="text-center" colspan="99"><i>Tidak ada data!</i></td>' +
        '</tr>';
      $("#rincian_penerimaan_data").append(html);
    } else {
      no = 0;
      $.each(item_list, function(index, value) {
        no++;
        if (value.is_sesuai_order == 1) {
          is_sesuai_order = '<input type="checkbox" class="form-check-input sesuai sesuai_' + no + '" name="is_sesuai_order_' + no + '" value="1" checked>';
        } else {
          is_sesuai_order = '<input type="checkbox" class="form-check-input sesuai sesuai_' + no + '" name="is_sesuai_order_' + no + '" value="1">';
        }

        if (typeof value.penerimaanrincian_id == 'undefined') {
          var penerimaanrincian_id = '';
        } else {
          var penerimaanrincian_id = value.penerimaanrincian_id;
        }

        if (value.stok_id == null) {
          var stok_id = '';
        } else {
          var stok_id = value.stok_id;
        }

        if (value.satuan == null) {
          var satuan = '';
        } else {
          var satuan = value.satuan;
        }

        if (typeof value.qty_diterima == 'undefined') {
          var qty_diterima = '';
        } else {
          var qty_diterima = value.qty_diterima;
        }

        if (value.tgl_expired == null || value.tgl_expired == '' || value.tgl_expired == '0000-00-00' || value.tgl_expired == '00-00-0000') {
          // var tgl_expired = '';
          var tgl_expired = '<input type="text" class="form-control datepicker-' + no + ' datepicker-emptys-' + no + ' no-validation" name="tgl_expired_' + (no) + '" id="tgl_expired_' + no + '" onkeyup="hitung(' + no + ')" value="">';
        } else {
          // var tgl_expired = value.tgl_expired;
          var tgl_expired = '<input type="text" class="form-control datepicker-' + no + ' datepicker-empty-' + no + ' no-validation" name="tgl_expired_' + no + '" id="tgl_expired_' + no + '" onkeyup="hitung(' + no + ')" value="' + (value.tgl_expired) + '" ' + readonly + '>';
        }

        if (value.no_batch == null || value.no_batch == '') {
          var no_batch = '';
        } else {
          var no_batch = value.no_batch;
        }

        if (value.harga_beli == null || value.harga_beli == '') {
          var harga_beli = '0';
        } else {
          var harga_beli = num_id(value.harga_beli);
        }

        if (value.sub_total_awal == null || value.sub_total_awal == '') {
          var sub_total_awal = '0';
        } else {
          var sub_total_awal = num_id(value.sub_total_awal);
        }

        if (value.prs_diskon == null || value.prs_diskon == '') {
          var prs_diskon = '0';
        } else {
          var prs_diskon = value.prs_diskon;
        }

        if (value.nom_diskon == null || value.nom_diskon == '') {
          var nom_diskon = '0';
        } else {
          var nom_diskon = num_id(value.nom_diskon);
        }

        if (value.prs_ppn == null || value.prs_ppn == '') {
          var prs_ppn = '0';
        } else {
          var prs_ppn = value.prs_ppn;
        }

        if (value.nom_ppn == null || value.nom_ppn == '') {
          var nom_ppn = '0';
        } else {
          var nom_ppn = num_id(value.nom_ppn);
        }

        if (value.sub_total_akhir == null || value.sub_total_akhir == '') {
          var sub_total_akhir = '0';
        } else {
          var sub_total_akhir = num_id(value.sub_total_akhir);
        }

        if (value.harga_jual == null || value.harga_jual == '') {
          var harga_jual = '0';
        } else {
          var harga_jual = num_id(value.harga_jual);
        }

        if (value.keteranganpenerimaan_rinc == null || value.keteranganpenerimaan_rinc == '') {
          var keteranganpenerimaan_rinc = '';
        } else {
          var keteranganpenerimaan_rinc = value.keteranganpenerimaan_rinc;
        }

        if (value.keteranganpenerimaan_rinc == null || value.keteranganpenerimaan_rinc == '') {
          var keteranganpenerimaan_rinc = '';
        } else {
          var keteranganpenerimaan_rinc = value.keteranganpenerimaan_rinc;
        }

        if (value.jenisbarang_cd == null || value.jenisbarang_cd == '') {
          var jenisbarang_cd = '';
        } else {
          var jenisbarang_cd = value.jenisbarang_cd;
        }

        if (value.satuan_cd == null || value.satuan_cd == '') {
          var satuan_cd = '';
        } else {
          var satuan_cd = value.satuan_cd;
        }

        <?php if ($id == null) : ?>
          var readonly = 'disabled';
        <?php else : ?>
          var readonly = '';
        <?php endif; ?>

        var html = '<tr>' +
          '<td class="text-center" width="36">' + (no) + '</td>' +
          '<td class="text-center" width="50">' +
          '<div class="d-flex justify-content-center">' +
          '<div class="form-check form-check-primary text-center" style="margin-left: -5px !important;">' +
          '<label class="form-check-label">' +
          (is_sesuai_order) +
          '<i class="input-helper"></i></label>' +
          '</div>' +
          '</div>' +
          '</td>' +
          '<td width="280">' +
          '<b>' + (value.nama_item) + '</b>' +
          '<br>' +
          'Jenis : Obat | Satuan : butir' +
          '<input type="hidden" name="obat_id_' + no + '" value="' + value.obat_id + '"/>' +
          '<input type="hidden" name="obat_nm_' + no + '" value="' + value.obat_nm + '"/>' +
          '<input type="hidden" name="penerimaanrincian_id_' + no + '" value="' + penerimaanrincian_id + '"/>' +
          '<input type="hidden" name="stok_id_' + no + '" value="' + stok_id + '"/>' +
          '<input type="hidden" name="jenisbarang_cd_' + no + '" id="jenisbarang_cd_' + no + '" value="' + jenisbarang_cd + '"/>' +
          '<input type="hidden" name="satuan_cd_' + no + '" value="' + satuan_cd + '"/>' +
          '<input type="hidden" name="keteranganpenerimaan_rinc_' + no + '" value=""/>' +
          '</td>' +
          // '<td class="text-center" width="80">' +
          // (satuan) +
          // '</td>' +
          '<td class="text-center" width="65">' +
          '<div class="d-flex justify-content-center">' +
          '<div class="form-check form-check-primary text-center" style="margin-left: -5px !important;">' +
          '<label class="form-check-label">' +
          '<input type="checkbox" class="form-check-input" id="injeksi_' + no + '" onchange="hitung(' + no + ')" value="1">' +
          '<i class="input-helper"></i></label>' +
          '</div>' +
          '</div>' +
          '</td>' +
          '<td class="text-center" width="60">' +
          '<div id="qty_pesanan_' + no + '">' + (value.qty_pesanan) + '</div>' +
          '<input type="hidden" name="qty_pesanan_' + no + '" value="' + (value.qty_pesanan) + '"/>' +
          '</td>' +
          '<td class="text-center" width="60">' +
          '<input type="text" class="form-control no-validation text-center readonly readonly_' + no + '" name="qty_diterima_' + no + '" id="qty_diterima_' + no + '" onkeyup="hitung(' + no + ')" value="' + (qty_diterima) + '" ' + readonly + '>' +
          '</td>' +
          '<td class="text-center" width="90">' +
          (tgl_expired) +
          '</td>' +
          '<td class="text-center" width="100">' +
          '<input type="text" class="form-control no-validation readonly readonly_' + no + '" name="no_batch_' + no + '" id="no_batch_' + no + '" onkeyup="hitung(' + no + ')" value="' + (no_batch) + '" ' + readonly + '>' +
          '</td>' +
          '<td class="text-center" width="90">' +
          '<input type="text" class="form-control no-validation autonumeric readonly readonly_' + no + '" dir="rtl" name="harga_beli_' + no + '" id="harga_beli_' + no + '" onkeyup="hitung(' + no + ')" value="' + (harga_beli) + '" ' + readonly + '>' +
          '</td>' +
          '<td class="text-center" width="90">' +
          '<input type="text" class="form-control no-validation autonumeric" dir="rtl" name="sub_total_awal_' + no + '" id="sub_total_awal_' + no + '" onkeyup="hitung(' + no + ')" value="' + (sub_total_awal) + '" readonly>' +
          '</td>' +
          '<td class="text-center" width="70">' +
          '<input type="text" class="form-control no-validation text-center readonly readonly_' + no + '" name="prs_diskon_' + no + '" id="prs_diskon_' + no + '" onkeyup="hitung(' + no + ')" value="' + (prs_diskon) + '" ' + readonly + '>' +
          '<input type="hidden" class="form-control no-validation text-center" name="nom_diskon_' + no + '" id="nom_diskon_' + no + '" value="' + (nom_diskon) + '">' +
          '</td>' +
          '<td class="text-center" width="70">' +
          '<input type="text" class="form-control no-validation text-center readonly readonly_' + no + '" name="prs_ppn_' + no + '" id="prs_ppn_' + no + '" onkeyup="hitung(' + no + ')" value="' + (prs_ppn) + '" ' + readonly + '>' +
          '<input type="hidden" class="form-control no-validation text-center" name="nom_ppn_' + no + '" id="nom_ppn_' + no + '" onkeyup="hitung(' + no + ')" value="' + (nom_ppn) + '">' +
          '</td>' +
          '<td class="text-center" width="90">' +
          '<input type="text" class="form-control no-validation autonumeric" dir="rtl" name="sub_total_akhir_' + no + '" id="sub_total_akhir_' + no + '" onkeyup="hitung(' + no + ')" value="' + (sub_total_akhir) + '" readonly>' +
          '</td>' +
          '<td class="text-center" width="90">' +
          '<input type="text" class="form-control no-validation autonumeric readonly readonly_' + no + '" dir="rtl" name="harga_jual_' + no + '" id="harga_jual_' + no + '" onkeyup="hitung(' + no + ')" value="' + harga_jual + '" ' + readonly + '>' +
          '</td>' +
          // '<td class="text-center" width="80">' +
          // '<input type="text" class="form-control no-validation readonly readonly_' + no + '" name="keteranganpenerimaan_rinc_' + no + '" id="keteranganpenerimaan_rinc_' + no + '" onkeyup="hitung(' + no + ')" value="' + keteranganpenerimaan_rinc + '" ' + readonly + '>' +
          // '</td>' +
          '</tr>';
        $("#rincian_penerimaan_data").append(html);
        js(no, tgl_expired);
        empty_datepicker(no);
      });
    }
  }

  function add_item() {
    $("#item_tidak_ada").hide();
    $('#note').removeClass('d-none');

    var nomor = $("#nomor").val();
    if (nomor == '') {
      $("#nomor").val('1');
    } else {
      $("#nomor").val(parseInt(nomor) + 1);
    }

    no++;
    var html = '<tr>' +
      '<td class="text-center" width="36">' + (no) + '</td>' +
      '<td class="text-center" width="50">' +
      '<div class="d-flex justify-content-center">' +
      '<div class="form-check form-check-primary text-center" style="margin-left: -5px !important;">' +
      '<label class="form-check-label">' +
      '<input type="checkbox" class="form-check-input sesuai sesuai_' + no + '" name="is_sesuai_order_' + (no) + '" value="1" checked>' +
      '<i class="input-helper"></i></label>' +
      '</div>' +
      '</div>' +
      '</td>' +
      '<td width="280">' +
      '<select class="chosen-select obat_id readonly readonly_' + no + '" id="select-obat-id-' + no + '" data-no="' + no + '">' +
      '<option value="">- Pilih -</option>' +
      '</select>' +
      '<input type="hidden" name="penerimaanrincian_id_' + (no) + '" value=""/>' +
      '<input type="hidden" name="stok_id_' + no + '" value=""/>' +
      '<input type="hidden" name="obat_id_' + (no) + '" id="obat_id_' + no + '" value="">' +
      '<input type="hidden" name="obat_nm_' + no + '" id="obat_nm_' + no + '" value=""/>' +
      '<input type="hidden" name="jenisbarang_cd_' + no + '" id="jenisbarang_cd_' + no + '" value=""/>' +
      '<input type="hidden" name="satuan_cd_' + no + '" id="satuan_cd_' + no + '" value=""/>' +
      '<input type="hidden" name="keteranganpenerimaan_rinc_' + no + '" value=""/>' +
      '</td>' +
      // '<td class="text-center" width="80">' +
      // '<span id="lbl_satuan_' + no + '"></span>' +
      // '</td>' +
      '<td class="text-center" width="65">' +
      '<div class="d-flex justify-content-center">' +
      '<div class="form-check form-check-primary text-center" style="margin-left: -5px !important;">' +
      '<label class="form-check-label">' +
      '<input type="checkbox" class="form-check-input" id="injeksi_' + no + '" onchange="hitung(' + no + ')" value="1">' +
      '<i class="input-helper"></i></label>' +
      '</div>' +
      '</div>' +
      '</td>' +
      '<td class="text-center" width="60">' +
      '<div id="qty_pesanan_' + no + '">' + 0 + '</div>' +
      '<input type="hidden" name="qty_pesanan_' + no + '" value="0"/>' +
      '</td>' +
      '<td class="text-center" width="60">' +
      '<input type="text" class="form-control no-validation text-center readonly readonly_' + no + '" name="qty_diterima_' + (no) + '" id="qty_diterima_' + no + '" onkeyup="hitung(' + no + ')" value="0">' +
      '</td>' +
      '<td class="text-center" width="90">' +
      '<input type="text" class="form-control datepicker-' + no + ' datepicker-empty-' + no + ' no-validation readonly readonly_' + no + '" name="tgl_expired_' + (no) + '" id="tgl_expired_' + no + '" onkeyup="hitung(' + no + ')" value="">' +
      '</td>' +
      '<td class="text-center" width="100">' +
      '<input type="text" class="form-control no-validation readonly readonly_' + no + '" name="no_batch_' + (no) + '" id="no_batch_' + no + '" onkeyup="hitung(' + no + ')" value="">' +
      '</td>' +
      '<td class="text-center" width="90">' +
      '<input type="text" class="form-control no-validation autonumeric readonly readonly_' + no + '" dir="rtl" name="harga_beli_' + (no) + '" id="harga_beli_' + no + '" onkeyup="hitung(' + no + ')" value="0">' +
      '</td>' +
      '<td class="text-center" width="90">' +
      '<input type="text" class="form-control no-validation autonumeric" dir="rtl" name="sub_total_awal_' + (no) + '" id="sub_total_awal_' + no + '" onkeyup="hitung(' + no + ')" value="0" readonly>' +
      '</td>' +
      '<td class="text-center" width="70">' +
      '<input type="text" class="form-control no-validation text-center readonly readonly_' + no + '" name="prs_diskon_' + (no) + '" id="prs_diskon_' + no + '" onkeyup="hitung(' + no + ')" value="0">' +
      '<input type="hidden" class="form-control no-validation text-center" name="nom_diskon_' + (no) + '" id="nom_diskon_' + no + '" onkeyup="hitung(' + no + ')" value="0">' +
      '</td>' +
      '<td class="text-center" width="70">' +
      '<input type="text" class="form-control no-validation text-center readonly readonly_' + no + '" name="prs_ppn_' + (no) + '" id="prs_ppn_' + no + '" onkeyup="hitung(' + no + ')" value="0">' +
      '<input type="hidden" class="form-control no-validation text-center" name="nom_ppn_' + (no) + '" id="nom_ppn_' + no + '" onkeyup="hitung(' + no + ')" value="0">' +
      '</td>' +
      '<td class="text-center" width="90">' +
      '<input type="text" class="form-control no-validation autonumeric" dir="rtl" name="sub_total_akhir_' + (no) + '" id="sub_total_akhir_' + no + '" onkeyup="hitung(' + no + ')" value="0" readonly>' +
      '</td>' +
      '<td class="text-center" width="90">' +
      '<input type="text" class="form-control no-validation autonumeric readonly readonly_' + no + '" dir="rtl" name="harga_jual_' + (no) + '" id="harga_jual_' + no + '" onkeyup="hitung(' + no + ')" value="0">' +
      '</td>' +
      // '<td class="text-center" width="80">' +
      // '<input type="text" class="form-control no-validation readonly readonly_' + no + '" name="keteranganpenerimaan_rinc_' + (no) + '" id="keteranganpenerimaan_rinc_' + no + '" onkeyup="hitung(' + no + ')" value="">' +
      // '</td>' +
      '</tr>';
    $("#rincian_penerimaan_data").append(html);
    js(no, '');
    autocomplete_obat();
  }

  function hitung(nomor) {
    var data = {
      qty_diterima: $("#qty_diterima_" + nomor).val(),
      harga_beli: num_sys($("#harga_beli_" + nomor).val()),
      sub_total_awal: num_sys($("#sub_total_awal_" + nomor).val()),
      prs_diskon: num_sys($("#prs_diskon_" + nomor).val()),
      prs_ppn: num_sys($("#prs_ppn_" + nomor).val()),
      sub_total_akhir: num_sys($("#sub_total_akhir_" + nomor).val())
    };
    data.sub_total_awal = data.qty_diterima * data.harga_beli;
    var diskon = (data.sub_total_awal * (data.prs_diskon / 100));
    data.sub_total_akhir = data.sub_total_awal - diskon; //after diskon
    var ppn = (data.sub_total_awal * (data.prs_ppn / 100));
    data.sub_total_akhir = data.sub_total_akhir + ppn; //after tax
    $("#nom_diskon_" + nomor).val(num_id(diskon));
    $("#nom_ppn_" + nomor).val(num_id(ppn));

    $("#sub_total_awal_" + nomor).val(num_id(data.sub_total_awal));
    $("#sub_total_akhir_" + nomor).val(num_id(data.sub_total_akhir));
    // hitung harga jual
    var jenisbarang_cd = $("#jenisbarang_cd_" + nomor).val();
    var injeksi = $("#injeksi_" + nomor).val();
    if (jenisbarang_cd == '01') {
      if ($('#injeksi_' + nomor).is(':checked')) {
        var harga_jual = data.harga_beli * 1.9;
      } else {
        var harga_jual = data.harga_beli * 1.5;
      }
    } else {
      var harga_jual = data.harga_beli * 1.9;
    }
    $("#harga_jual_" + nomor).val(num_id(harga_jual));

    hitung_total();
  }

  function hitung_total(params) {
    var jml_sub_total = 0;
    var jml_prs_diskon = 0;
    var jml_nom_diskon = 0;
    var jml_prs_ppn = 0;
    var jml_nom_ppn = 0;
    var jml_total = 0;
    for (var i = 1; i <= no; i++) {
      jml_sub_total += parseInt(num_sys($("#sub_total_awal_" + i).val()));
      jml_prs_diskon += parseInt(num_sys($("#prs_diskon_" + i).val()));
      jml_nom_diskon += parseInt(num_sys($("#nom_diskon_" + i).val()));
      jml_prs_ppn += parseInt(num_sys($("#prs_ppn_" + i).val()));
      jml_nom_ppn += parseInt(num_sys($("#nom_ppn_" + i).val()));
      jml_total += parseInt(num_sys($("#sub_total_akhir_" + i).val()));
    }
    $("#jml_sub_total").val(num_id(jml_sub_total));
    $("#jml_prs_diskon").val(num_id(jml_prs_diskon));
    $("#jml_nom_diskon").val(num_id(jml_nom_diskon));
    $("#jml_prs_ppn").val(num_id(jml_prs_ppn));
    $("#jml_nom_ppn").val(num_id(jml_nom_ppn));
    $("#jml_total").val(num_id(jml_total));
    $("#jml_tagihan").val(num_id(jml_total));
  }

  function dp() {
    var jml_total = num_sys($("#jml_total").val());
    var jml_dp = num_sys($("#jml_dp").val());
    var jml_tagihan = parseInt(jml_total) - parseInt(jml_dp);
    $("#jml_tagihan").val(num_id(jml_tagihan));
  }

  function num_sys(x) {
    x = x.replace(/\./g, "");
    x = x.replace(',', '.');
    return x;
  }

  function num_id(bilangan) {
    var negativ = false;

    if (bilangan < 0) {
      bilangan = bilangan * -1;
      negativ = true;
    };

    var number_string = bilangan.toString(),
      split = number_string.split('.'),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);

    if (ribuan) {
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;

    // Cetak hasil
    if (negativ == true) {
      rupiah = '-' + rupiah;
    }
    return rupiah;
  }
</script>