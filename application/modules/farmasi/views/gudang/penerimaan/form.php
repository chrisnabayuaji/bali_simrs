<?php $this->load->view('_js'); ?>
<div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
<form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?= $form_action ?>" class="needs-validation" novalidate autocomplete="off">
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline col-title">
          <div class="col-back">
            <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
          </div>
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            <?= $nav['nav_nm'] ?>
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-warehouse"></i> Gudang</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item active"><span>Form</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>
    <div class="row full-page mt-4">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <h4 class="card-title border-bottom border-2 pb-2 mb-3">Surat Pesanan</h4>
            <div class="row">
              <div class="col-5">
                <input type="hidden" class="form-control" name="penerimaan_id" id="penerimaan_id" value="<?= @$main['penerimaan_id'] ?>">
                <input type="hidden" class="form-control" name="order_id" id="order_id" value="<?= @$main['order_id'] ?>">
                <input type="hidden" class="form-control" name="nomor" id="nomor" value="">
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">No.Surat Pesanan <span class="text-danger">*<span></label>
                  <div class="col-lg-6 col-md-9 mb-2">
                    <input type="text" class="form-control" id="no_order" value="<?= @$main['no_order'] ?>" required aria-invalid="false" readonly>
                    <small class="text-danger font-weight-bold">*Silahkan pilih Pesanan dengan klik tombol disamping kanan</small>
                  </div>
                  <?php if ($id == null || $id == '') : ?>
                    <div class="col-lg-3">
                      <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_surat_pesanan/search' ?>" modal-title="Cari Surat Pesanan" modal-size="lg" class="btn btn-xs btn-primary modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cari Surat Pesanan"><i class="fas fa-search"></i> Cari Pesanan</a>
                    </div>
                  <?php endif; ?>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Tgl.Surat Pesanan</label>
                  <div class="col-lg-4 col-md-9">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control" id="tgl_order" value="<?php if (@$main) {
                                                                                      echo to_date(@$main['tgl_order'], '', 'date', '');
                                                                                    } else {
                                                                                      echo date('d-m-Y');
                                                                                    } ?>" required aria-invalid="false" readonly>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Supplier</label>
                  <div class="col-lg-6 col-md-9">
                    <select class="form-control form-control-sm chosen-select" id="supplier_id" disabled>
                      <option value="">---</option>
                      <?php foreach ($supplier as $r) : ?>
                        <option value="<?= $r['supplier_id'] ?>" <?php if (@$main['supplier_id'] == $r['supplier_id']) {
                                                                    echo 'selected';
                                                                  } ?>><?= $r['supplier_nm'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col">
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Keterangan</label>
                  <div class="col-lg-8 col-md-9">
                    <textarea class="form-control form-control-sm" id="keteranganorder" rows="4" readonly><?= @$main['keteranganorder'] ?></textarea>
                  </div>
                </div>
              </div>
            </div>
            <h4 class="card-title border-bottom border-2 pb-2 mb-3 mt-3">Faktur Penerimaan</h4>
            <div class="row">
              <div class="col-5">
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">No. Faktur <span class="text-danger">*<span></label>
                  <div class="col-lg-8 col-md-9">
                    <input type="text" class="form-control" name="no_faktur" id="no_faktur" value="<?= @$main['no_faktur'] ?>" required aria-invalid="false">
                  </div>
                </div>
              </div>
              <div class="col">
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Tgl. Faktur <span class="text-danger">*<span></label>
                  <div class="col-lg-3 col-md-9">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datepicker" name="tgl_faktur" id="tgl_faktur" value="<?php if (@$main) {
                                                                                                                    echo to_date(@$main['tgl_faktur'], '', 'date', '');
                                                                                                                  } else {
                                                                                                                    echo date('d-m-Y H:i:s');
                                                                                                                  } ?>" required aria-invalid="false">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <h4 class="card-title border-bottom border-2 pb-2 mb-3 mt-3">Rincian Penerimaan</h4>
            <div class="table-responsive">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th class="text-center text-middle" width="36">No</th>
                    <th class="text-center text-middle" width="50">Sesuai</th>
                    <th class="text-center text-middle" width="280">Nama Item</th>
                    <th class="text-center text-middle" width="65">Injeksi ?</th>
                    <!-- <th class="text-center text-middle" width="80">Satuan</th> -->
                    <th class="text-center text-middle" width="60">Qty Pesanan</th>
                    <th class="text-center text-middle" width="60">Qty Diterima</th>
                    <th class="text-center text-middle" width="90">Tgl.Expired</th>
                    <th class="text-center text-middle" width="100">No.Batch</th>
                    <th class="text-center text-middle" width="90">Harga Beli</th>
                    <th class="text-center text-middle" width="90">Sub Total</th>
                    <th class="text-center text-middle" width="70">Diskon %</th>
                    <th class="text-center text-middle" width="70">PPN %</th>
                    <th class="text-center text-middle" width="90">Total</th>
                    <th class="text-center text-middle" width="90">Harga Jual</th>
                    <!-- <th class="text-center text-middle" width="80">Keterangan</th> -->
                  </tr>
                </thead>
                <tbody id="rincian_penerimaan_data"></tbody>
              </table>
              <div class="mt-1 mb-n1 d-none" id="note">
                <small class="text-danger font-weight-bold">*PENTING :</small><br>
                <small class="text-danger font-weight-bold">- Centang terlebih dahulu kemudian isikan form yang ada</small><br>
                <small class="text-danger font-weight-bold">- Centang kolom Injeksi jika jenis obat adalah Injeksi</small>
              </div>
              <div class="mt-2">
                <button type="button" class="btn btn-xs btn-sm btn-primary" onclick="add_item()"><i class="fas fa-plus"></i> Tambah Item Barang</button>
                <button type="button" class="btn btn-xs btn-sm btn-primary" onclick="samakan()"><i class="fas fa-hashtag"></i> Samakan QTY Diterima = QTY Pesanan</button>
                <button type="button" class="btn btn-xs btn-sm btn-primary" onclick="check_all()"><i class="fas fa-check"></i> Semua Barang Sesuai SP</button>
                <?php if ($id != null) : ?>
                  <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_surat_pesanan/rincian_pesanan/' . @$main['order_id'] ?>" modal-title="Rincian Pesanan" modal-size="lg" class="btn btn-xs btn-primary modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Rincian Pesanan"><i class="fas fa-list"></i> Rincian Pesanan</a>
                <?php endif; ?>
              </div>
            </div>
            <div class="row">
              <div class="col-6">
                <h4 class="card-title border-bottom border-2 pb-2 mb-3 mt-3">Resume Jumlah</h4>
                <div class="row">
                  <div class="col-8 offset-4">
                    <div class="form-group row">
                      <label class="col-lg-6 col-md-3 col-form-label">Jumlah Sub Total</label>
                      <div class="col-lg-6 col-md-9">
                        <input type="text" class="form-control" name="jml_sub_total" id="jml_sub_total" dir="rtl" value="<?= @num_id($main['jml_total_awal']) ?>" readonly>
                      </div>
                    </div>
                  </div>
                  <div class="col-8 offset-4">
                    <div class="form-group row">
                      <label class="col-lg-6 col-md-3 col-form-label">Jumlah Diskon</label>
                      <div class="col-lg-6 col-md-9">
                        <input type="hidden" class="form-control" name="jml_prs_diskon" id="jml_prs_diskon" dir="rtl" value="<?= @num_id($main['jml_prs_diskon']) ?>" readonly>
                        <input type="text" class="form-control" name="jml_nom_diskon" id="jml_nom_diskon" dir="rtl" value="<?= @num_id($main['jml_nom_diskon']) ?>" readonly>
                      </div>
                    </div>
                  </div>
                  <div class="col-8 offset-4">
                    <div class="form-group row">
                      <label class="col-lg-6 col-md-3 col-form-label">Jumlah PPN</label>
                      <div class="col-lg-6 col-md-9">
                        <input type="hidden" class="form-control" name="jml_prs_ppn" id="jml_prs_ppn" dir="rtl" value="<?= @num_id($main['jml_prs_ppn']) ?>" readonly>
                        <input type="text" class="form-control" name="jml_nom_ppn" id="jml_nom_ppn" dir="rtl" value="<?= @num_id($main['jml_nom_ppn']) ?>" readonly>
                      </div>
                    </div>
                  </div>
                  <div class="col-8 offset-4">
                    <div class="form-group row">
                      <label class="col-lg-6 col-md-3 col-form-label">Jumlah Total</label>
                      <div class="col-lg-6 col-md-9">
                        <input type="text" class="form-control" name="jml_total" id="jml_total" dir="rtl" value="<?= @num_id($main['jml_total_akhir']) ?>" readonly>
                      </div>
                    </div>
                  </div>
                </div>
                <h4 class="card-title border-bottom border-2 pb-2 mb-3 mt-3">Keterangan Penerimaan</h4>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Keterangan</label>
                  <div class="col-lg-10 col-md-9">
                    <textarea class="form-control form-control-sm" name="keteranganpenerimaan" id="keteranganpenerimaan" rows="4"><?= @$main['keteranganpenerimaan'] ?></textarea>
                  </div>
                </div>
              </div>
              <div class="col-6">
                <h4 class="card-title border-bottom border-2 pb-2 mb-3 mt-3">Pembayaran</h4>
                <div class="row">
                  <div class="col-8 offset-4">
                    <div class="form-group row">
                      <label class="col-lg-6 col-md-3 col-form-label">Jumlah Down Payment</label>
                      <div class="col-lg-6 col-md-9">
                        <input type="text" class="form-control autonumeric" name="jml_dp" id="jml_dp" dir="rtl" value="<?= @num_id($main['jml_nom_dp']) ?>" onkeyup="dp()" value="">
                      </div>
                    </div>
                  </div>
                  <div class="col-8 offset-4">
                    <div class="form-group row">
                      <label class="col-lg-6 col-md-3 col-form-label">Jumlah Tagihan</label>
                      <div class="col-lg-6 col-md-9">
                        <input type="text" class="form-control autonumeric" name="jml_tagihan" id="jml_tagihan" value="<?= @num_id($main['jml_total_tagihan']) ?>" dir="rtl">
                      </div>
                    </div>
                  </div>
                  <div class="col-8 offset-4">
                    <div class="form-group row">
                      <label class="col-lg-6 col-md-3 col-form-label">Jumlah Dibayar <span class="text-danger">*</span></label>
                      <div class="col-lg-6 col-md-9">
                        <input type="text" class="form-control autonumeric" name="jml_dibayar" id="jml_dibayar" value="<?= @num_id($main['jml_total_bayar']) ?>" dir="rtl">
                      </div>
                    </div>
                  </div>
                  <div class="col-8 offset-4">
                    <div class="form-group row">
                      <label class="col-lg-6 col-md-3 col-form-label">Status Bayar <span class="text-danger">*</span></label>
                      <div class="col-lg-6 col-md-9">
                        <select class="form-control chosen-select w-100" name="bayar_st" id="bayar_st" required="">
                          <option value="">- Pilih -</option>
                          <option value="1" <?= (@$main['bayar_st'] == 1) ? 'selected' : ''; ?>>Lunas</option>
                          <option value="2" <?= (@$main['bayar_st'] == 2) ? 'selected' : ''; ?>>Kurang Bayar</option>
                          <option value="0" <?= (@$main['bayar_st'] == 0) ? 'selected' : ''; ?>>Belum Bayar</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-8 offset-4 mt-5">
                    <div class="form-group row">
                      <label class="col-lg-6 col-md-3 col-form-label">Sumber Dana</label>
                      <div class="col-lg-6 col-md-9">
                        <select class="form-control chosen-select w-100" name="sumberdana_cd" id="sumberdana_cd">
                          <option value="">---</option>
                          <?php foreach (get_parameter('sumberdana_cd') as $r) : ?>
                            <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['sumberdana_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-8 offset-4">
                    <div class="form-group row">
                      <label class="col-lg-6 col-md-3 col-form-label">Cara Bayar</label>
                      <div class="col-lg-6 col-md-9">
                        <select class="form-control chosen-select" name="carabayar_cd" id="carabayar_cd">
                          <option value="">---</option>
                          <?php foreach (get_parameter('carabayar_cd') as $r) : ?>
                            <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['carabayar_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-8 offset-4">
                    <div class="form-group row">
                      <label class="col-lg-6 col-md-3 col-form-label">Tgl. Jatuh Tempo</label>
                      <div class="col-lg-6 col-md-9">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datepicker-top" name="tgl_jatem" id="tgl_jatem" value="<?= to_date(@$main['tgl_jatem']) ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-8 offset-4">
                    <div class="form-group row">
                      <label class="col-lg-6 col-md-3 col-form-label">Tgl. Bayar</label>
                      <div class="col-lg-6 col-md-9">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datepicker-top" name="tgl_bayar" id="tgl_bayar" value="<?= to_date(@$main['tgl_bayar']) ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="btn-form">
    <div class="btn-form-action">
      <div class="btn-form-action-bottom w-100 small-text clearfix">
        <div class="col-lg-2">
          <a href="<?= site_url() . '/' . $nav['nav_url'] ?>" class="btn btn-xs btn-secondary"><i class="mdi mdi-keyboard-backspace"></i> Kembali</a>
        </div>
        <div class="col-lg-2 offset-8">
          <button type="submit" class="float-right btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
          <button type="reset" onClick="window.location.reload();" class="float-right btn btn-xs btn-secondary btn-clear mr-2"><i class="fas fa-sync-alt"></i> Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>