<div class="table-responsive">
  <table id="obat_table" class="table table-hover table-bordered table-striped table-sm w-100">
    <thead>
      <tr>
        <th class="text-center" width="36">No</th>
        <th class="text-center" width="200">No. Surat Pesanan</th>
        <th class="text-center" width="150">Tgl. Surat Pesanan</th>
        <th class="text-center">Supplier</th>
        <th class="text-center">Keterangan</th>
        <th class="text-center" width="55">Aksi</th>
      </tr>
    </thead>
  </table>
</div>

<script type="text/javascript">
  var obatTable;
  $(document).ready(function () {
    //datatables
    obatTable = $('#obat_table').DataTable({
      "autoWidth" : false,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
      "ajax": {
          "url": "<?php echo site_url().'/'.$nav['nav_url'].'/ajax_surat_pesanan/search_data'?>",
          "type": "POST"
      },      
      "columnDefs": [
        {"targets": 0, "className": 'text-center', "orderable" : false},
        {"targets": 1, "className": 'text-left', "orderable" : false},
        {"targets": 2, "className": 'text-center', "orderable" : false},
        {"targets": 3, "className": 'text-left', "orderable" : false},
        {"targets": 4, "className": 'text-left', "orderable" : false},
        {"targets": 5, "className": 'text-left', "orderable" : false},
      ],
    });
    obatTable.columns.adjust().draw();
  })
</script>