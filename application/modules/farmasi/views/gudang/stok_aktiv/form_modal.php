<!-- js -->
<?php $this->load->view('_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Nama Item Barang</label>
        <div class="col-lg-8 col-md-3">
          <select class="form-control form-control-sm chosen-select" name="obat_id" id="obat_id">
            <?php foreach($obat as $r): ?>
              <option value="<?=$r['obat_id']?>" <?= (@$main['obat_id'] == $r['obat_id']) ? 'selected' : ''?>><?=$r['obat_nm']?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">No.Batch</label>
        <div class="col-lg-4 col-md-4">
          <input type="text" class="form-control" name="no_batch" value="<?=@$main['no_batch']?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Harga Beli</label>
        <div class="col-lg-4 col-md-4">
          <input type="text" name="harga_beli" class="form-control" value="<?=@$main['harga_beli']?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Harga Jual</label>
        <div class="col-lg-4 col-md-4">
          <input type="text" name="harga_jual" class="form-control" value="<?=@$main['harga_jual']?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Tgl.Expired</label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control datepicker" name="tgl_expired" value="<?=to_date(@$main['tgl_expired'])?>" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Stok Awal <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="stok_awal" value="<?=@$main['stok_awal']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Stok Penerimaan <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="stok_penerimaan" value="<?=@$main['stok_penerimaan']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Stok Distribusi <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="stok_distribusi" value="<?=@$main['stok_distribusi']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Retur Keluar <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="stok_retur_keluar" value="<?=@$main['stok_retur_keluar']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Retur Masuk <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="stok_retur_masuk" value="<?=@$main['stok_retur_masuk']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Stok Musnah <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="stok_musnah" value="<?=@$main['stok_musnah']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Stok Opname <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="stok_opname" value="<?=@$main['stok_opname']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Status</label>
        <div class="col-lg-3 col-md-5">
          <select class="chosen-select custom-select w-100" name="stok_st">
            <option value="1" <?php if(@$main['stok_st'] == '1') echo 'selected'?>>Aktif</option>
            <option value="0" <?php if(@$main['stok_st'] == '0') echo 'selected'?>>Tidak Aktif</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>