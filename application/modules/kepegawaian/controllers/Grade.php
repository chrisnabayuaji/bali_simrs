<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Grade extends MY_Controller
{

	var $nav_id = '10.01.05', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'm_grade',
			'kepegawaian/m_kelompokgrade'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id);
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'grade_id', 'type' => 'asc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(4, 0);
		$this->cookie['total_rows'] = $this->m_grade->all_rows($this->cookie);
		set_cookie_nav($this->nav_id, $this->cookie);
		//main data
		$data['nav'] = $this->nav;
		$data['cookie'] = $this->cookie;
		$data['main'] = $this->m_grade->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
		//set pagination
		set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('kepegawaian/grade/index', $data);
	}

	public function form_modal($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_grade->get_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save/' . $id;
		$data['kelompokgrade'] = $this->m_kelompokgrade->all_data();

		echo json_encode(array(
			'html' => $this->load->view('kepegawaian/grade/form_modal', $data, true)
		));
	}

	public function save($id = null)
	{
		$this->m_grade->save($id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($id != '') ? '_update' : '_add', $this->nav_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	public function delete($id = null)
	{
		$this->m_grade->delete($id, true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log('_delete', $this->nav_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	public function multiple($type = null)
	{
		$data = $this->input->post();
		if (isset($data['checkitem'])) {
			foreach ($data['checkitem'] as $key) {
				switch ($type) {
					case 'delete':
						$this->authorize($this->nav, '_delete');
						$this->m_grade->delete($key, true);
						$flash = 'Data berhasil dihapus.';
						create_log('_delete', $this->nav_id);
						break;

					case 'enable':
						$this->authorize($this->nav, '_update');
						$this->m_grade->update($key, array('is_active' => 1));
						$flash = 'Data berhasil diaktifkan.';
						create_log('_update', $this->nav_id);
						break;

					case 'disable':
						$this->authorize($this->nav, '_update');
						$this->m_grade->update($key, array('is_active' => 0));
						$flash = 'Data berhasil dinonaktifkan.';
						create_log('_delete', $this->nav_id);
						break;
				}
			}
		}
		$this->session->set_flashdata('flash_success', $flash);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	function ajax($type = null, $id = null)
	{
		if ($type == 'cek_id') {
			$data = $this->input->post();
			$cek = $this->m_grade->get_data($data['grade_id']);
			if ($id == null) {
				if ($cek == null) {
					echo 'true';
				} else {
					echo 'false';
				}
			} else {
				if ($id != $data['grade_id'] && $cek != null) {
					echo 'false';
				} else {
					echo 'true';
				}
			}
		}
	}
}
