<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pegawai extends MY_Controller
{

	var $nav_id = '10.02.01', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'm_pegawai',
			'm_pangkat',
			'm_jabatan',
			'm_pendidikan',
			'app/m_parameter',
			'master/m_wilayah',
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id);
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '', 'parameter_cd' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'pegawai_id', 'type' => 'asc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(4, 0);
		$this->cookie['total_rows'] = $this->m_pegawai->all_rows($this->cookie);
		set_cookie_nav($this->nav_id, $this->cookie);
		//main data
		$data['nav'] = $this->nav;
		$data['cookie'] = $this->cookie;
		$data['main'] = $this->m_pegawai->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);

		$data['list_jenispegawai'] = $this->m_parameter->list_parameter_by_field('jenispegawai_cd');
		//set pagination
		set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('kepegawaian/pegawai/index', $data);
	}

	public function form_modal($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_pegawai->get_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save/' . $id;
		$data['list_spesialis'] = $this->m_parameter->list_parameter_by_field('spesialisasi_cd');
		$data['list_jenispegawai'] = $this->m_parameter->list_parameter_by_field('jenispegawai_cd');
		$data['list_sex'] = $this->m_parameter->list_parameter_by_field('sex_cd');
		$data['list_agama'] = $this->m_parameter->list_parameter_by_field('agama_cd');
		$data['list_pendidikan'] = $this->m_parameter->list_parameter_by_field('pendidikan_cd');
		$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_parent('');

		echo json_encode(array(
			'html' => $this->load->view('kepegawaian/pegawai/form_modal', $data, true)
		));
	}

	public function form($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_pegawai->get_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save/' . $id;
		$data['list_spesialis'] = $this->m_parameter->list_parameter_by_field('spesialisasi_cd');
		$data['list_jenispegawai'] = $this->m_parameter->list_parameter_by_field('jenispegawai_cd');
		$data['list_sex'] = $this->m_parameter->list_parameter_by_field('sex_cd');
		$data['list_agama'] = $this->m_parameter->list_parameter_by_field('agama_cd');
		$data['list_pendidikan'] = $this->m_parameter->list_parameter_by_field('pendidikan_cd');
		$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_parent('');
		$data['list_riwayat_pangkat'] = $this->m_pegawai->list_riwayat_pangkat($id);
		$data['list_riwayat_jabatan'] = $this->m_pegawai->list_riwayat_jabatan($id);

		$this->render('kepegawaian/pegawai/form', $data);
	}

	public function save($id = null)
	{
		$this->m_pegawai->save($id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($id != '') ? '_update' : '_add', $this->nav_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	public function delete($id = null)
	{
		$this->m_pegawai->delete($id, true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log('_delete', $this->nav_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	public function multiple($type = null)
	{
		$data = $this->input->post();
		if (isset($data['checkitem'])) {
			foreach ($data['checkitem'] as $key) {
				switch ($type) {
					case 'delete':
						$this->authorize($this->nav, '_delete');
						$this->m_pegawai->delete($key, true);
						$flash = 'Data berhasil dihapus.';
						create_log('_delete', $this->nav_id);
						break;

					case 'enable':
						$this->authorize($this->nav, '_update');
						$this->m_pegawai->update($key, array('is_active' => 1));
						$flash = 'Data berhasil diaktifkan.';
						create_log('_update', $this->nav_id);
						break;

					case 'disable':
						$this->authorize($this->nav, '_update');
						$this->m_pegawai->update($key, array('is_active' => 0));
						$flash = 'Data berhasil dinonaktifkan.';
						create_log('_delete', $this->nav_id);
						break;
				}
			}
		}
		$this->session->set_flashdata('flash_success', $flash);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	function ajax($type = null, $id = null)
	{
		if ($type == 'view_tab_menu') {
			$view_name = $this->input->post('view_name');
			$id = $this->input->post('id');
			$data['nav'] = $this->nav;
			$data['pegawai'] = $this->m_pegawai->get_data($id);
			$data['list_spesialis'] = $this->m_parameter->list_parameter_by_field('spesialisasi_cd');
			$data['list_jenispegawai'] = $this->m_parameter->list_parameter_by_field('jenispegawai_cd');
			$data['list_sex'] = $this->m_parameter->list_parameter_by_field('sex_cd');
			$data['list_agama'] = $this->m_parameter->list_parameter_by_field('agama_cd');
			$data['list_pendidikan'] = $this->m_parameter->list_parameter_by_field('pendidikan_cd');
			$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_parent('');
			$data['list_riwayat_pangkat'] = $this->m_pegawai->list_riwayat_pangkat($data['pegawai']['pegawai_id']);
			$data['list_riwayat_jabatan'] = $this->m_pegawai->list_riwayat_jabatan($data['pegawai']['pegawai_id']);
			$data['list_riwayat_pendidikan'] = $this->m_pegawai->list_riwayat_pendidikan($data['pegawai']['pegawai_id']);
			$data['list_riwayat_diklat'] = $this->m_pegawai->list_riwayat_diklat($data['pegawai']['pegawai_id']);
			$data['list_riwayat_penghargaan'] = $this->m_pegawai->list_riwayat_penghargaan($data['pegawai']['pegawai_id']);
			$data['list_riwayat_hukuman'] = $this->m_pegawai->list_riwayat_hukuman($data['pegawai']['pegawai_id']);
			$data['list_riwayat_lain'] = $this->m_pegawai->list_riwayat_lain($data['pegawai']['pegawai_id']);


			echo json_encode(array(
				'html' => $this->load->view('kepegawaian/pegawai/' . $view_name, $data, true)
			));
		}

		if ($type == 'get_pegawai_id') {
			$jenispegawai_cd = $this->input->get('jenispegawai_cd');
			$result = $this->m_pegawai->get_pegawai_id($jenispegawai_cd);
			echo json_encode(array(
				'result' => $result
			));
		}

		if ($type == 'cek_id') {
			$data = $this->input->post();
			$cek = $this->m_pegawai->get_data($data['pegawai_id']);
			if ($id == null) {
				if ($cek == null) {
					echo 'true';
				} else {
					echo 'false';
				}
			} else {
				if ($id != $data['pegawai_id'] && $cek != null) {
					echo 'false';
				} else {
					echo 'true';
				}
			}
		}
	}


	// Riwayat Pangkat
	public function  riwayat_pangkat_modal($pegawai_id, $riwayatpangkat_id = null)
	{
		$this->authorize($this->nav, ($riwayatpangkat_id != '') ? '_update' : '_add');

		if ($riwayatpangkat_id == null) {
			$data['riwayatpangkat'] = array();
		} else {
			$data['riwayatpangkat'] = $this->m_pegawai->get_riwayat_pangkat($pegawai_id, $riwayatpangkat_id);
		}
		$data['riwayatpangkat_id'] = $riwayatpangkat_id;
		$data['pegawai'] = $this->m_pegawai->get_data($pegawai_id);
		$data['list_pangkat'] = $this->m_pangkat->all_data();
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/riwayat_pangkat_save/' . $pegawai_id . '/' . $riwayatpangkat_id;

		echo json_encode(array(
			'html' => $this->load->view('kepegawaian/pegawai/riwayat_pangkat_modal', $data, true)
		));
	}

	public function riwayat_pangkat_save($pegawai_id, $riwayatpangkat_id = null)
	{
		$this->m_pegawai->save_riwayat_pangkat($pegawai_id, $riwayatpangkat_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_pangkat');
	}

	public function riwayat_pangkat_delete($pegawai_id, $riwayatpangkat_id)
	{
		$this->m_pegawai->delete_riwayat_pangkat($pegawai_id, $riwayatpangkat_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_pangkat');
	}

	public function riwayat_pangkat_berkas_delete($pegawai_id, $riwayatpangkatberkas_id)
	{
		$this->m_pegawai->berkas_delete_riwayat_pangkat($pegawai_id, $riwayatpangkatberkas_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_pangkat');
	}
	// End Riwayat Pangkat

	// Riwayat jabatan
	public function  riwayat_jabatan_modal($pegawai_id, $riwayatjabatan_id = null)
	{
		$this->authorize($this->nav, ($riwayatjabatan_id != '') ? '_update' : '_add');

		if ($riwayatjabatan_id == null) {
			$data['riwayatjabatan'] = array();
		} else {
			$data['riwayatjabatan'] = $this->m_pegawai->get_riwayat_jabatan($pegawai_id, $riwayatjabatan_id);
		}
		$data['riwayatjabatan_id'] = $riwayatjabatan_id;
		$data['pegawai'] = $this->m_pegawai->get_data($pegawai_id);
		$data['list_jabatan'] = $this->m_jabatan->all_data();
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/riwayat_jabatan_save/' . $pegawai_id . '/' . $riwayatjabatan_id;

		echo json_encode(array(
			'html' => $this->load->view('kepegawaian/pegawai/riwayat_jabatan_modal', $data, true)
		));
	}

	public function riwayat_jabatan_save($pegawai_id, $riwayatjabatan_id = null)
	{
		$this->m_pegawai->save_riwayat_jabatan($pegawai_id, $riwayatjabatan_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_jabatan');
	}

	public function riwayat_jabatan_delete($pegawai_id, $riwayatjabatan_id)
	{
		$this->m_pegawai->delete_riwayat_jabatan($pegawai_id, $riwayatjabatan_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_jabatan');
	}

	public function riwayat_jabatan_berkas_delete($pegawai_id, $riwayatjabatanberkas_id)
	{
		$this->m_pegawai->berkas_delete_riwayat_jabatan($pegawai_id, $riwayatjabatanberkas_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_jabatan');
	}
	// End Riwayat jabatan

	// Riwayat pendidikan
	public function  riwayat_pendidikan_modal($pegawai_id, $riwayatpendidikan_id = null)
	{
		$this->authorize($this->nav, ($riwayatpendidikan_id != '') ? '_update' : '_add');

		if ($riwayatpendidikan_id == null) {
			$data['riwayatpendidikan'] = array();
		} else {
			$data['riwayatpendidikan'] = $this->m_pegawai->get_riwayat_pendidikan($pegawai_id, $riwayatpendidikan_id);
		}
		$data['riwayatpendidikan_id'] = $riwayatpendidikan_id;
		$data['pegawai'] = $this->m_pegawai->get_data($pegawai_id);
		$data['list_pendidikan'] = $this->m_pendidikan->all_data();
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/riwayat_pendidikan_save/' . $pegawai_id . '/' . $riwayatpendidikan_id;

		echo json_encode(array(
			'html' => $this->load->view('kepegawaian/pegawai/riwayat_pendidikan_modal', $data, true)
		));
	}

	public function riwayat_pendidikan_save($pegawai_id, $riwayatpendidikan_id = null)
	{
		$this->m_pegawai->save_riwayat_pendidikan($pegawai_id, $riwayatpendidikan_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_pendidikan');
	}

	public function riwayat_pendidikan_delete($pegawai_id, $riwayatpendidikan_id)
	{
		$this->m_pegawai->delete_riwayat_pendidikan($pegawai_id, $riwayatpendidikan_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_pendidikan');
	}

	public function riwayat_pendidikan_berkas_delete($pegawai_id, $riwayatpendidikanberkas_id)
	{
		$this->m_pegawai->berkas_delete_riwayat_pendidikan($pegawai_id, $riwayatpendidikanberkas_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_pendidikan');
	}
	// End Riwayat pendidikan

	// Riwayat diklat
	public function  riwayat_diklat_modal($pegawai_id, $riwayatdiklat_id = null)
	{
		$this->authorize($this->nav, ($riwayatdiklat_id != '') ? '_update' : '_add');

		if ($riwayatdiklat_id == null) {
			$data['riwayatdiklat'] = array();
		} else {
			$data['riwayatdiklat'] = $this->m_pegawai->get_riwayat_diklat($pegawai_id, $riwayatdiklat_id);
		}
		$data['riwayatdiklat_id'] = $riwayatdiklat_id;
		$data['pegawai'] = $this->m_pegawai->get_data($pegawai_id);
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/riwayat_diklat_save/' . $pegawai_id . '/' . $riwayatdiklat_id;

		echo json_encode(array(
			'html' => $this->load->view('kepegawaian/pegawai/riwayat_diklat_modal', $data, true)
		));
	}

	public function riwayat_diklat_save($pegawai_id, $riwayatdiklat_id = null)
	{
		$this->m_pegawai->save_riwayat_diklat($pegawai_id, $riwayatdiklat_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_diklat');
	}

	public function riwayat_diklat_delete($pegawai_id, $riwayatdiklat_id)
	{
		$this->m_pegawai->delete_riwayat_diklat($pegawai_id, $riwayatdiklat_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_diklat');
	}

	public function riwayat_diklat_berkas_delete($pegawai_id, $riwayatdiklatberkas_id)
	{
		$this->m_pegawai->berkas_delete_riwayat_diklat($pegawai_id, $riwayatdiklatberkas_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_diklat');
	}
	// End Riwayat diklat

	// Riwayat penghargaan
	public function  riwayat_penghargaan_modal($pegawai_id, $riwayatpenghargaan_id = null)
	{
		$this->authorize($this->nav, ($riwayatpenghargaan_id != '') ? '_update' : '_add');

		if ($riwayatpenghargaan_id == null) {
			$data['riwayatpenghargaan'] = array();
		} else {
			$data['riwayatpenghargaan'] = $this->m_pegawai->get_riwayat_penghargaan($pegawai_id, $riwayatpenghargaan_id);
		}
		$data['riwayatpenghargaan_id'] = $riwayatpenghargaan_id;
		$data['pegawai'] = $this->m_pegawai->get_data($pegawai_id);
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/riwayat_penghargaan_save/' . $pegawai_id . '/' . $riwayatpenghargaan_id;

		echo json_encode(array(
			'html' => $this->load->view('kepegawaian/pegawai/riwayat_penghargaan_modal', $data, true)
		));
	}

	public function riwayat_penghargaan_save($pegawai_id, $riwayatpenghargaan_id = null)
	{
		$this->m_pegawai->save_riwayat_penghargaan($pegawai_id, $riwayatpenghargaan_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_penghargaan');
	}

	public function riwayat_penghargaan_delete($pegawai_id, $riwayatpenghargaan_id)
	{
		$this->m_pegawai->delete_riwayat_penghargaan($pegawai_id, $riwayatpenghargaan_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_penghargaan');
	}

	public function riwayat_penghargaan_berkas_delete($pegawai_id, $riwayatpenghargaanberkas_id)
	{
		$this->m_pegawai->berkas_delete_riwayat_penghargaan($pegawai_id, $riwayatpenghargaanberkas_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_penghargaan');
	}
	// End Riwayat penghargaan

	// Riwayat hukuman
	public function  riwayat_hukuman_modal($pegawai_id, $riwayathukuman_id = null)
	{
		$this->authorize($this->nav, ($riwayathukuman_id != '') ? '_update' : '_add');

		if ($riwayathukuman_id == null) {
			$data['riwayathukuman'] = array();
		} else {
			$data['riwayathukuman'] = $this->m_pegawai->get_riwayat_hukuman($pegawai_id, $riwayathukuman_id);
		}
		$data['riwayathukuman_id'] = $riwayathukuman_id;
		$data['pegawai'] = $this->m_pegawai->get_data($pegawai_id);
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/riwayat_hukuman_save/' . $pegawai_id . '/' . $riwayathukuman_id;

		echo json_encode(array(
			'html' => $this->load->view('kepegawaian/pegawai/riwayat_hukuman_modal', $data, true)
		));
	}

	public function riwayat_hukuman_save($pegawai_id, $riwayathukuman_id = null)
	{
		$this->m_pegawai->save_riwayat_hukuman($pegawai_id, $riwayathukuman_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_hukuman');
	}

	public function riwayat_hukuman_delete($pegawai_id, $riwayathukuman_id)
	{
		$this->m_pegawai->delete_riwayat_hukuman($pegawai_id, $riwayathukuman_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_hukuman');
	}

	public function riwayat_hukuman_berkas_delete($pegawai_id, $riwayathukumanberkas_id)
	{
		$this->m_pegawai->berkas_delete_riwayat_hukuman($pegawai_id, $riwayathukumanberkas_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_hukuman');
	}
	// End Riwayat hukuman

	// Riwayat lain
	public function  riwayat_lain_modal($pegawai_id, $riwayatlain_id = null)
	{
		$this->authorize($this->nav, ($riwayatlain_id != '') ? '_update' : '_add');

		if ($riwayatlain_id == null) {
			$data['riwayatlain'] = array();
		} else {
			$data['riwayatlain'] = $this->m_pegawai->get_riwayat_lain($pegawai_id, $riwayatlain_id);
		}
		$data['riwayatlain_id'] = $riwayatlain_id;
		$data['pegawai'] = $this->m_pegawai->get_data($pegawai_id);
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/riwayat_lain_save/' . $pegawai_id . '/' . $riwayatlain_id;

		echo json_encode(array(
			'html' => $this->load->view('kepegawaian/pegawai/riwayat_lain_modal', $data, true)
		));
	}

	public function riwayat_lain_save($pegawai_id, $riwayatlain_id = null)
	{
		$this->m_pegawai->save_riwayat_lain($pegawai_id, $riwayatlain_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_lain');
	}

	public function riwayat_lain_delete($pegawai_id, $riwayatlain_id)
	{
		$this->m_pegawai->delete_riwayat_lain($pegawai_id, $riwayatlain_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_lain');
	}

	public function riwayat_lain_berkas_delete($pegawai_id, $riwayatlainberkas_id)
	{
		$this->m_pegawai->berkas_delete_riwayat_lain($pegawai_id, $riwayatlainberkas_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $pegawai_id . '#riwayat_lain');
	}
	// End Riwayat lain
}
