<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pegawai extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND a.pegawai_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' ";
    }
    if (@$cookie['search']['parameter_cd'] != '') {
      $where .= "AND b.jenispegawai_cd = '" . $this->db->escape_like_str($cookie['search']['parameter_cd']) . "' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
                a.*, b.jenispegawai_nm  
            FROM mst_pegawai a 
            LEFT JOIN 
            (
                SELECT p.parameter_cd as jenispegawai_cd, p.parameter_val as jenispegawai_nm 
                FROM mst_parameter p 
                WHERE p.parameter_field = 'jenispegawai_cd'
            ) b ON a.jenispegawai_cd = b.jenispegawai_cd 
            $where
            ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data($jenispegawai_cd = '')
  {
    $where = "WHERE a.is_deleted = 0 ";
    if ($jenispegawai_cd != '') {
      $where .= "AND a.jenispegawai_cd='$jenispegawai_cd'";
    }

    $sql = "SELECT * FROM mst_pegawai a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function list_dokter()
  {
    $sql = "SELECT * 
            FROM mst_pegawai a 
            WHERE a.is_deleted = 0 
              AND a.jenispegawai_cd = '02'
            ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT 
              COUNT(1) as total
            FROM mst_pegawai a 
            LEFT JOIN 
            (
                SELECT p.parameter_cd as jenispegawai_cd, p.parameter_val as jenispegawai_nm 
                FROM mst_parameter p 
                WHERE p.parameter_field = 'jenispegawai_cd'
            ) b ON a.jenispegawai_cd = b.jenispegawai_cd 
            $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id)
  {
    $sql = "SELECT * FROM mst_pegawai WHERE pegawai_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  function by_field($field, $val, $type = 'row')
  {
    $sql = "SELECT * FROM mst_pegawai WHERE $field=?";
    $query = $this->db->query($sql, array($val));
    if ($type == 'row') {
      $res = $query->row_array();
    } else if ($type == 'result') {
      $res = $query->result_array();
    }
    return $res;
  }

  function get_pegawai_id($parent_id = null)
  {
    $len = strlen($parent_id);
    $sql = "SELECT MAX(RIGHT(pegawai_id,4)) as pegawai_id FROM mst_pegawai WHERE LEFT(pegawai_id,$len)=?";
    $query = $this->db->query($sql, $parent_id);
    if ($query->num_rows() > 0) {
      $row = $query->row_array();
      $pegawai_id = abs($row['pegawai_id']) + 1;
      $pegawai_id = zerofill($pegawai_id, 4);
      $result = $parent_id . '.' . $pegawai_id;
    } else {
      $result = '';
    }
    return $result;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    if ($id == null) {
      $data['tgl_lahir'] = to_date($data['tgl_lahir']);
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('mst_pegawai', $data);
    } else {
      $data['tgl_lahir'] = to_date($data['tgl_lahir']);
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('pegawai_id', $id)->update('mst_pegawai', $data);
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('pegawai_id', $id)->update('mst_pegawai', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('pegawai_id', $id)->delete('mst_pegawai');
    } else {
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('pegawai_id', $id)->update('mst_pegawai', $data);
    }
  }

  // Riwayat Pangkat
  public function list_riwayat_pangkat($pegawai_id)
  {
    return $this->db->query(
      "SELECT 
        a.*, b.pangkat_nm, b.golongan  
      FROM peg_riwayat_pangkat a 
      JOIN peg_pangkat b ON a.pangkat_id = b.pangkat_id
      WHERE a.pegawai_id='" . $pegawai_id . "'"
    )->result_array();
  }

  public function get_riwayat_pangkat($pegawai_id, $riwayatpangkat_id)
  {
    $data = $this->db->query(
      "SELECT 
        a.*, b.pangkat_nm, b.golongan  
      FROM peg_riwayat_pangkat a 
      JOIN peg_pangkat b ON a.pangkat_id = b.pangkat_id
      WHERE a.pegawai_id='" . $pegawai_id . "' AND a.riwayatpangkat_id='" . $riwayatpangkat_id . "'"
    )->row_array();

    $data['berkas'] = $this->db->where('riwayatpangkat_id', $riwayatpangkat_id)->get('peg_riwayat_pangkat_berkas')->result_array();

    return $data;
  }

  public function save_riwayat_pangkat($pegawai_id, $riwayatpangkat_id)
  {
    $data = $this->input->post();
    $data['pangkat_tmt'] = to_date($data['pangkat_tmt']);
    $data['sk_tgl'] = to_date($data['sk_tgl']);

    $data_riwayat = $data;
    unset($data_riwayat['title']);

    if ($riwayatpangkat_id == null) {
      $data_riwayat['created_at'] = date('Y-m-d H:i:s');
      $data_riwayat['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('peg_riwayat_pangkat', $data_riwayat);
      $riwayatpangkat_id = $this->db->insert_id();
    } else {
      $data_riwayat['updated_at'] = date('Y-m-d H:i:s');
      $data_riwayat['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('riwayatpangkat_id', $riwayatpangkat_id)->update('peg_riwayat_pangkat', $data_riwayat);
    }

    $jumlah_berkas = count($_FILES['berkas']['name']);
    for ($i = 0; $i < $jumlah_berkas; $i++) {
      $config = array(
        'upload_path' => FCPATH . 'assets/images/riwayat_pangkat/',
        'allowed_types' => 'jpg|png|jpeg|gif|bmp|pdf|doc|docx|xls|xlsx|ppt|pptx|rtf',
        'encrypt_name' => TRUE,
      );
      $this->load->library('upload', $config);

      if (!empty($_FILES['berkas']['name'][$i])) {
        $_FILES['file']['name'] = $_FILES['berkas']['name'][$i];
        $_FILES['file']['type'] = $_FILES['berkas']['type'][$i];
        $_FILES['file']['tmp_name'] = $_FILES['berkas']['tmp_name'][$i];
        $_FILES['file']['error'] = $_FILES['berkas']['error'][$i];
        $_FILES['file']['size'] = $_FILES['berkas']['size'][$i];

        if ($this->upload->do_upload('file')) {
          $uploadData = $this->upload->data();
          $b['riwayatpangkat_id'] = $riwayatpangkat_id;
          $b['filename'] = $uploadData['file_name'];
          $b['title'] = $data['title'][$i];
          $b['mime_type'] = $uploadData['file_type'];
          $b['size'] = $uploadData['file_size'];
          $b['created_at'] = date('Y-m-d H:i:s');
          $b['created_by'] = $this->session->userdata('sess_user_realname');
          $this->db->insert('peg_riwayat_pangkat_berkas', $b);
        } else {
          var_dump($this->upload->display_errors());
          die;
        }
      }
    }
  }

  public function delete_riwayat_pangkat($pegawai_id, $riwayatpangkat_id)
  {
    $this->db->where('riwayatpangkat_id', $riwayatpangkat_id)->delete('peg_riwayat_pangkat');
  }

  public function berkas_delete_riwayat_pangkat($pegawai_id, $riwayatpangkatberkas_id)
  {
    $this->db->where('riwayatpangkatberkas_id', $riwayatpangkatberkas_id)->delete('peg_riwayat_pangkat_berkas');
  }
  // End Riwayat Pangkat

  // Riwayat jabatan
  public function list_riwayat_jabatan($pegawai_id)
  {
    return $this->db->query(
      "SELECT 
        a.*, b.jabatan_nm 
      FROM peg_riwayat_jabatan a 
      JOIN peg_jabatan b ON a.jabatan_id = b.jabatan_id
      WHERE a.pegawai_id='" . $pegawai_id . "'"
    )->result_array();
  }

  public function get_riwayat_jabatan($pegawai_id, $riwayatjabatan_id)
  {
    $data = $this->db->query(
      "SELECT 
        a.*, b.jabatan_nm 
      FROM peg_riwayat_jabatan a 
      JOIN peg_jabatan b ON a.jabatan_id = b.jabatan_id
      WHERE a.pegawai_id='" . $pegawai_id . "' AND a.riwayatjabatan_id='" . $riwayatjabatan_id . "'"
    )->row_array();

    $data['berkas'] = $this->db->where('riwayatjabatan_id', $riwayatjabatan_id)->get('peg_riwayat_jabatan_berkas')->result_array();

    return $data;
  }

  public function save_riwayat_jabatan($pegawai_id, $riwayatjabatan_id)
  {
    $data = $this->input->post();
    $data['jabatan_tmt'] = to_date($data['jabatan_tmt']);
    $data['sk_tgl'] = to_date($data['sk_tgl']);

    $data_riwayat = $data;
    unset($data_riwayat['title']);

    if ($riwayatjabatan_id == null) {
      $data_riwayat['created_at'] = date('Y-m-d H:i:s');
      $data_riwayat['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('peg_riwayat_jabatan', $data_riwayat);
      $riwayatjabatan_id = $this->db->insert_id();
    } else {
      $data_riwayat['updated_at'] = date('Y-m-d H:i:s');
      $data_riwayat['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('riwayatjabatan_id', $riwayatjabatan_id)->update('peg_riwayat_jabatan', $data_riwayat);
    }

    $jumlah_berkas = count($_FILES['berkas']['name']);
    for ($i = 0; $i < $jumlah_berkas; $i++) {
      $config = array(
        'upload_path' => FCPATH . 'assets/images/riwayat_jabatan/',
        'allowed_types' => 'jpg|png|jpeg|gif|bmp|pdf|doc|docx|xls|xlsx|ppt|pptx|rtf',
        'encrypt_name' => TRUE,
      );
      $this->load->library('upload', $config);

      if (!empty($_FILES['berkas']['name'][$i])) {
        $_FILES['file']['name'] = $_FILES['berkas']['name'][$i];
        $_FILES['file']['type'] = $_FILES['berkas']['type'][$i];
        $_FILES['file']['tmp_name'] = $_FILES['berkas']['tmp_name'][$i];
        $_FILES['file']['error'] = $_FILES['berkas']['error'][$i];
        $_FILES['file']['size'] = $_FILES['berkas']['size'][$i];

        if ($this->upload->do_upload('file')) {
          $uploadData = $this->upload->data();
          $b['riwayatjabatan_id'] = $riwayatjabatan_id;
          $b['filename'] = $uploadData['file_name'];
          $b['title'] = $data['title'][$i];
          $b['mime_type'] = $uploadData['file_type'];
          $b['size'] = $uploadData['file_size'];
          $b['created_at'] = date('Y-m-d H:i:s');
          $b['created_by'] = $this->session->userdata('sess_user_realname');
          $this->db->insert('peg_riwayat_jabatan_berkas', $b);
        } else {
          var_dump($this->upload->display_errors());
          die;
        }
      }
    }
  }

  public function delete_riwayat_jabatan($pegawai_id, $riwayatjabatan_id)
  {
    $this->db->where('riwayatjabatan_id', $riwayatjabatan_id)->delete('peg_riwayat_jabatan');
  }

  public function berkas_delete_riwayat_jabatan($pegawai_id, $riwayatjabatanberkas_id)
  {
    $this->db->where('riwayatjabatanberkas_id', $riwayatjabatanberkas_id)->delete('peg_riwayat_jabatan_berkas');
  }
  // End Riwayat jabatan

  // Riwayat pendidikan
  public function list_riwayat_pendidikan($pegawai_id)
  {
    return $this->db->query(
      "SELECT 
        a.*, b.pendidikan_nm 
      FROM peg_riwayat_pendidikan a 
      JOIN peg_pendidikan b ON a.pendidikan_id = b.pendidikan_id
      WHERE a.pegawai_id='" . $pegawai_id . "'"
    )->result_array();
  }

  public function get_riwayat_pendidikan($pegawai_id, $riwayatpendidikan_id)
  {
    $data = $this->db->query(
      "SELECT 
        a.*, b.pendidikan_nm 
      FROM peg_riwayat_pendidikan a 
      JOIN peg_pendidikan b ON a.pendidikan_id = b.pendidikan_id
      WHERE a.pegawai_id='" . $pegawai_id . "' AND a.riwayatpendidikan_id='" . $riwayatpendidikan_id . "'"
    )->row_array();

    $data['berkas'] = $this->db->where('riwayatpendidikan_id', $riwayatpendidikan_id)->get('peg_riwayat_pendidikan_berkas')->result_array();

    return $data;
  }

  public function save_riwayat_pendidikan($pegawai_id, $riwayatpendidikan_id)
  {
    $data = $this->input->post();
    $data['lulus_tgl'] = to_date($data['lulus_tgl']);

    $data_riwayat = $data;
    unset($data_riwayat['title']);

    if ($riwayatpendidikan_id == null) {
      $data_riwayat['created_at'] = date('Y-m-d H:i:s');
      $data_riwayat['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('peg_riwayat_pendidikan', $data_riwayat);
      $riwayatpendidikan_id = $this->db->insert_id();
    } else {
      $data_riwayat['updated_at'] = date('Y-m-d H:i:s');
      $data_riwayat['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('riwayatpendidikan_id', $riwayatpendidikan_id)->update('peg_riwayat_pendidikan', $data_riwayat);
    }

    $jumlah_berkas = count($_FILES['berkas']['name']);
    for ($i = 0; $i < $jumlah_berkas; $i++) {
      $config = array(
        'upload_path' => FCPATH . 'assets/images/riwayat_pendidikan/',
        'allowed_types' => 'jpg|png|jpeg|gif|bmp|pdf|doc|docx|xls|xlsx|ppt|pptx|rtf',
        'encrypt_name' => TRUE,
      );
      $this->load->library('upload', $config);

      if (!empty($_FILES['berkas']['name'][$i])) {
        $_FILES['file']['name'] = $_FILES['berkas']['name'][$i];
        $_FILES['file']['type'] = $_FILES['berkas']['type'][$i];
        $_FILES['file']['tmp_name'] = $_FILES['berkas']['tmp_name'][$i];
        $_FILES['file']['error'] = $_FILES['berkas']['error'][$i];
        $_FILES['file']['size'] = $_FILES['berkas']['size'][$i];

        if ($this->upload->do_upload('file')) {
          $uploadData = $this->upload->data();
          $b['riwayatpendidikan_id'] = $riwayatpendidikan_id;
          $b['filename'] = $uploadData['file_name'];
          $b['title'] = $data['title'][$i];
          $b['mime_type'] = $uploadData['file_type'];
          $b['size'] = $uploadData['file_size'];
          $b['created_at'] = date('Y-m-d H:i:s');
          $b['created_by'] = $this->session->userdata('sess_user_realname');
          $this->db->insert('peg_riwayat_pendidikan_berkas', $b);
        } else {
          var_dump($this->upload->display_errors());
          die;
        }
      }
    }
  }

  public function delete_riwayat_pendidikan($pegawai_id, $riwayatpendidikan_id)
  {
    $this->db->where('riwayatpendidikan_id', $riwayatpendidikan_id)->delete('peg_riwayat_pendidikan');
  }

  public function berkas_delete_riwayat_pendidikan($pegawai_id, $riwayatpendidikanberkas_id)
  {
    $this->db->where('riwayatpendidikanberkas_id', $riwayatpendidikanberkas_id)->delete('peg_riwayat_pendidikan_berkas');
  }
  // End Riwayat pendidikan

  // Riwayat diklat
  public function list_riwayat_diklat($pegawai_id)
  {
    return $this->db->query(
      "SELECT 
        a.*
      FROM peg_riwayat_diklat a 
      WHERE a.pegawai_id='" . $pegawai_id . "'"
    )->result_array();
  }

  public function get_riwayat_diklat($pegawai_id, $riwayatdiklat_id)
  {
    $data = $this->db->query(
      "SELECT 
        a.*
      FROM peg_riwayat_diklat a 
      WHERE a.pegawai_id='" . $pegawai_id . "' AND a.riwayatdiklat_id='" . $riwayatdiklat_id . "'"
    )->row_array();

    $data['berkas'] = $this->db->where('riwayatdiklat_id', $riwayatdiklat_id)->get('peg_riwayat_diklat_berkas')->result_array();

    return $data;
  }

  public function save_riwayat_diklat($pegawai_id, $riwayatdiklat_id)
  {
    $data = $this->input->post();

    $data_riwayat = $data;
    unset($data_riwayat['title']);

    if ($riwayatdiklat_id == null) {
      $data_riwayat['created_at'] = date('Y-m-d H:i:s');
      $data_riwayat['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('peg_riwayat_diklat', $data_riwayat);
      $riwayatdiklat_id = $this->db->insert_id();
    } else {
      $data_riwayat['updated_at'] = date('Y-m-d H:i:s');
      $data_riwayat['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('riwayatdiklat_id', $riwayatdiklat_id)->update('peg_riwayat_diklat', $data_riwayat);
    }

    $jumlah_berkas = count($_FILES['berkas']['name']);
    for ($i = 0; $i < $jumlah_berkas; $i++) {
      $config = array(
        'upload_path' => FCPATH . 'assets/images/riwayat_diklat/',
        'allowed_types' => 'jpg|png|jpeg|gif|bmp|pdf|doc|docx|xls|xlsx|ppt|pptx|rtf',
        'encrypt_name' => TRUE,
      );
      $this->load->library('upload', $config);

      if (!empty($_FILES['berkas']['name'][$i])) {
        $_FILES['file']['name'] = $_FILES['berkas']['name'][$i];
        $_FILES['file']['type'] = $_FILES['berkas']['type'][$i];
        $_FILES['file']['tmp_name'] = $_FILES['berkas']['tmp_name'][$i];
        $_FILES['file']['error'] = $_FILES['berkas']['error'][$i];
        $_FILES['file']['size'] = $_FILES['berkas']['size'][$i];

        if ($this->upload->do_upload('file')) {
          $uploadData = $this->upload->data();
          $b['riwayatdiklat_id'] = $riwayatdiklat_id;
          $b['filename'] = $uploadData['file_name'];
          $b['title'] = $data['title'][$i];
          $b['mime_type'] = $uploadData['file_type'];
          $b['size'] = $uploadData['file_size'];
          $b['created_at'] = date('Y-m-d H:i:s');
          $b['created_by'] = $this->session->userdata('sess_user_realname');
          $this->db->insert('peg_riwayat_diklat_berkas', $b);
        } else {
          var_dump($this->upload->display_errors());
          die;
        }
      }
    }
  }

  public function delete_riwayat_diklat($pegawai_id, $riwayatdiklat_id)
  {
    $this->db->where('riwayatdiklat_id', $riwayatdiklat_id)->delete('peg_riwayat_diklat');
  }

  public function berkas_delete_riwayat_diklat($pegawai_id, $riwayatdiklatberkas_id)
  {
    $this->db->where('riwayatdiklatberkas_id', $riwayatdiklatberkas_id)->delete('peg_riwayat_diklat_berkas');
  }
  // End Riwayat diklat

  // Riwayat penghargaan
  public function list_riwayat_penghargaan($pegawai_id)
  {
    return $this->db->query(
      "SELECT 
        a.*
      FROM peg_riwayat_penghargaan a 
      WHERE a.pegawai_id='" . $pegawai_id . "'"
    )->result_array();
  }

  public function get_riwayat_penghargaan($pegawai_id, $riwayatpenghargaan_id)
  {
    $data = $this->db->query(
      "SELECT 
        a.*
      FROM peg_riwayat_penghargaan a 
      WHERE a.pegawai_id='" . $pegawai_id . "' AND a.riwayatpenghargaan_id='" . $riwayatpenghargaan_id . "'"
    )->row_array();

    $data['berkas'] = $this->db->where('riwayatpenghargaan_id', $riwayatpenghargaan_id)->get('peg_riwayat_penghargaan_berkas')->result_array();

    return $data;
  }

  public function save_riwayat_penghargaan($pegawai_id, $riwayatpenghargaan_id)
  {
    $data = $this->input->post();

    $data_riwayat = $data;
    unset($data_riwayat['title']);

    if ($riwayatpenghargaan_id == null) {
      $data_riwayat['created_at'] = date('Y-m-d H:i:s');
      $data_riwayat['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('peg_riwayat_penghargaan', $data_riwayat);
      $riwayatpenghargaan_id = $this->db->insert_id();
    } else {
      $data_riwayat['updated_at'] = date('Y-m-d H:i:s');
      $data_riwayat['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('riwayatpenghargaan_id', $riwayatpenghargaan_id)->update('peg_riwayat_penghargaan', $data_riwayat);
    }

    $jumlah_berkas = count($_FILES['berkas']['name']);
    for ($i = 0; $i < $jumlah_berkas; $i++) {
      $config = array(
        'upload_path' => FCPATH . 'assets/images/riwayat_penghargaan/',
        'allowed_types' => 'jpg|png|jpeg|gif|bmp|pdf|doc|docx|xls|xlsx|ppt|pptx|rtf',
        'encrypt_name' => TRUE,
      );
      $this->load->library('upload', $config);

      if (!empty($_FILES['berkas']['name'][$i])) {
        $_FILES['file']['name'] = $_FILES['berkas']['name'][$i];
        $_FILES['file']['type'] = $_FILES['berkas']['type'][$i];
        $_FILES['file']['tmp_name'] = $_FILES['berkas']['tmp_name'][$i];
        $_FILES['file']['error'] = $_FILES['berkas']['error'][$i];
        $_FILES['file']['size'] = $_FILES['berkas']['size'][$i];

        if ($this->upload->do_upload('file')) {
          $uploadData = $this->upload->data();
          $b['riwayatpenghargaan_id'] = $riwayatpenghargaan_id;
          $b['filename'] = $uploadData['file_name'];
          $b['title'] = $data['title'][$i];
          $b['mime_type'] = $uploadData['file_type'];
          $b['size'] = $uploadData['file_size'];
          $b['created_at'] = date('Y-m-d H:i:s');
          $b['created_by'] = $this->session->userdata('sess_user_realname');
          $this->db->insert('peg_riwayat_penghargaan_berkas', $b);
        } else {
          var_dump($this->upload->display_errors());
          die;
        }
      }
    }
  }

  public function delete_riwayat_penghargaan($pegawai_id, $riwayatpenghargaan_id)
  {
    $this->db->where('riwayatpenghargaan_id', $riwayatpenghargaan_id)->delete('peg_riwayat_penghargaan');
  }

  public function berkas_delete_riwayat_penghargaan($pegawai_id, $riwayatpenghargaanberkas_id)
  {
    $this->db->where('riwayatpenghargaanberkas_id', $riwayatpenghargaanberkas_id)->delete('peg_riwayat_penghargaan_berkas');
  }
  // End Riwayat penghargaan

  // Riwayat hukuman
  public function list_riwayat_hukuman($pegawai_id)
  {
    return $this->db->query(
      "SELECT 
        a.*
      FROM peg_riwayat_hukuman a 
      WHERE a.pegawai_id='" . $pegawai_id . "'"
    )->result_array();
  }

  public function get_riwayat_hukuman($pegawai_id, $riwayathukuman_id)
  {
    $data = $this->db->query(
      "SELECT 
        a.*
      FROM peg_riwayat_hukuman a 
      WHERE a.pegawai_id='" . $pegawai_id . "' AND a.riwayathukuman_id='" . $riwayathukuman_id . "'"
    )->row_array();

    $data['berkas'] = $this->db->where('riwayathukuman_id', $riwayathukuman_id)->get('peg_riwayat_hukuman_berkas')->result_array();

    return $data;
  }

  public function save_riwayat_hukuman($pegawai_id, $riwayathukuman_id)
  {
    $data = $this->input->post();

    $data_riwayat = $data;
    $data_riwayat['hukuman_tmt'] = to_date($data_riwayat['hukuman_tmt']);

    unset($data_riwayat['title']);

    if ($riwayathukuman_id == null) {
      $data_riwayat['created_at'] = date('Y-m-d H:i:s');
      $data_riwayat['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('peg_riwayat_hukuman', $data_riwayat);
      $riwayathukuman_id = $this->db->insert_id();
    } else {
      $data_riwayat['updated_at'] = date('Y-m-d H:i:s');
      $data_riwayat['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('riwayathukuman_id', $riwayathukuman_id)->update('peg_riwayat_hukuman', $data_riwayat);
    }

    $jumlah_berkas = count($_FILES['berkas']['name']);
    for ($i = 0; $i < $jumlah_berkas; $i++) {
      $config = array(
        'upload_path' => FCPATH . 'assets/images/riwayat_hukuman/',
        'allowed_types' => 'jpg|png|jpeg|gif|bmp|pdf|doc|docx|xls|xlsx|ppt|pptx|rtf',
        'encrypt_name' => TRUE,
      );
      $this->load->library('upload', $config);

      if (!empty($_FILES['berkas']['name'][$i])) {
        $_FILES['file']['name'] = $_FILES['berkas']['name'][$i];
        $_FILES['file']['type'] = $_FILES['berkas']['type'][$i];
        $_FILES['file']['tmp_name'] = $_FILES['berkas']['tmp_name'][$i];
        $_FILES['file']['error'] = $_FILES['berkas']['error'][$i];
        $_FILES['file']['size'] = $_FILES['berkas']['size'][$i];

        if ($this->upload->do_upload('file')) {
          $uploadData = $this->upload->data();
          $b['riwayathukuman_id'] = $riwayathukuman_id;
          $b['filename'] = $uploadData['file_name'];
          $b['title'] = $data['title'][$i];
          $b['mime_type'] = $uploadData['file_type'];
          $b['size'] = $uploadData['file_size'];
          $b['created_at'] = date('Y-m-d H:i:s');
          $b['created_by'] = $this->session->userdata('sess_user_realname');
          $this->db->insert('peg_riwayat_hukuman_berkas', $b);
        } else {
          var_dump($this->upload->display_errors());
          die;
        }
      }
    }
  }

  public function delete_riwayat_hukuman($pegawai_id, $riwayathukuman_id)
  {
    $this->db->where('riwayathukuman_id', $riwayathukuman_id)->delete('peg_riwayat_hukuman');
  }

  public function berkas_delete_riwayat_hukuman($pegawai_id, $riwayathukumanberkas_id)
  {
    $this->db->where('riwayathukumanberkas_id', $riwayathukumanberkas_id)->delete('peg_riwayat_hukuman_berkas');
  }
  // End Riwayat hukuman

  // Riwayat lain
  public function list_riwayat_lain($pegawai_id)
  {
    return $this->db->query(
      "SELECT 
        a.*
      FROM peg_riwayat_lain a 
      WHERE a.pegawai_id='" . $pegawai_id . "'"
    )->result_array();
  }

  public function get_riwayat_lain($pegawai_id, $riwayatlain_id)
  {
    $data = $this->db->query(
      "SELECT 
        a.*
      FROM peg_riwayat_lain a 
      WHERE a.pegawai_id='" . $pegawai_id . "' AND a.riwayatlain_id='" . $riwayatlain_id . "'"
    )->row_array();

    $data['berkas'] = $this->db->where('riwayatlain_id', $riwayatlain_id)->get('peg_riwayat_lain_berkas')->result_array();

    return $data;
  }

  public function save_riwayat_lain($pegawai_id, $riwayatlain_id)
  {
    $data = $this->input->post();

    $data_riwayat = $data;
    $data_riwayat['tgl'] = to_date($data_riwayat['tgl']);

    unset($data_riwayat['title']);

    if ($riwayatlain_id == null) {
      $data_riwayat['created_at'] = date('Y-m-d H:i:s');
      $data_riwayat['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('peg_riwayat_lain', $data_riwayat);
      $riwayatlain_id = $this->db->insert_id();
    } else {
      $data_riwayat['updated_at'] = date('Y-m-d H:i:s');
      $data_riwayat['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('riwayatlain_id', $riwayatlain_id)->update('peg_riwayat_lain', $data_riwayat);
    }

    $jumlah_berkas = count($_FILES['berkas']['name']);
    for ($i = 0; $i < $jumlah_berkas; $i++) {
      $config = array(
        'upload_path' => FCPATH . 'assets/images/riwayat_lain/',
        'allowed_types' => 'jpg|png|jpeg|gif|bmp|pdf|doc|docx|xls|xlsx|ppt|pptx|rtf',
        'encrypt_name' => TRUE,
      );
      $this->load->library('upload', $config);

      if (!empty($_FILES['berkas']['name'][$i])) {
        $_FILES['file']['name'] = $_FILES['berkas']['name'][$i];
        $_FILES['file']['type'] = $_FILES['berkas']['type'][$i];
        $_FILES['file']['tmp_name'] = $_FILES['berkas']['tmp_name'][$i];
        $_FILES['file']['error'] = $_FILES['berkas']['error'][$i];
        $_FILES['file']['size'] = $_FILES['berkas']['size'][$i];

        if ($this->upload->do_upload('file')) {
          $uploadData = $this->upload->data();
          $b['riwayatlain_id'] = $riwayatlain_id;
          $b['filename'] = $uploadData['file_name'];
          $b['title'] = $data['title'][$i];
          $b['mime_type'] = $uploadData['file_type'];
          $b['size'] = $uploadData['file_size'];
          $b['created_at'] = date('Y-m-d H:i:s');
          $b['created_by'] = $this->session->userdata('sess_user_realname');
          $this->db->insert('peg_riwayat_lain_berkas', $b);
        } else {
          var_dump($this->upload->display_errors());
          die;
        }
      }
    }
  }

  public function delete_riwayat_lain($pegawai_id, $riwayatlain_id)
  {
    $this->db->where('riwayatlain_id', $riwayatlain_id)->delete('peg_riwayat_lain');
  }

  public function berkas_delete_riwayat_lain($pegawai_id, $riwayatlainberkas_id)
  {
    $this->db->where('riwayatlainberkas_id', $riwayatlainberkas_id)->delete('peg_riwayat_lain_berkas');
  }
  // End Riwayat lain
}
