<script type="text/javascript">
  $(document).ready(function() {
    $("#form-data").validate({
      rules: {
        grade_id: {
          remote: {
            url: '<?= site_url() . '/' . $nav['nav_url'] ?>/ajax/cek_id/<?= @$main['grade_id'] ?>',
            type: 'post',
            data: {
              grade_id: function() {
                return $("#grade_id").val();
              }
            }
          }
        },
      },
      messages: {
        grade_id: {
          remote: 'Kode sudah digunakan!'
        }
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');
    //AUTONUMERIC ============================================================================================================
    $(".autonumeric").autoNumeric({
      aSep: ".",
      aDec: ",",
      vMax: "999999999999999",
      vMin: "0",
    });
    $(".autonumeric-float").autoNumeric({
      aSep: ".",
      aDec: ",",
      aForm: true,
      vMax: "999999999999999999.99999",
      vMin: "-999999999999999999.99999",
      mDec: "4",
      aPad: false
    });
    //END AUTONUMERIC ============================================================================================================
  })
</script>