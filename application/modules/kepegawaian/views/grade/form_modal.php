<!-- js -->
<?php $this->load->view('kepegawaian/grade/_js') ?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?= $form_action ?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Kode <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="grade_id" id="grade_id" value="<?= @$main['grade_id'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Kelompok Grade <span class="text-danger">*</span></label>
        <div class="col-lg-7 col-md-7">
          <select class="form-control chosen-select" name="kelompokgrade_id" id="kelompokgrade_id" required>
            <option value="">-- Pilih --</option>
            <?php foreach ($kelompokgrade as $r) : ?>
              <option value="<?= $r['kelompokgrade_id'] ?>" <?= (@$main['kelompokgrade_id'] == $r['kelompokgrade_id']) ? 'selected' : '' ?>>
                <?= $r['kelompokgrade_id'] . ' - ' . $r['kelompokgrade_nm'] ?>
              </option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Level <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-7">
          <input type="number" class="form-control" name="grade_level" id="grade_level" value="<?= @$main['grade_level'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Grade <span class="text-danger">*</span></label>
        <div class="col-lg-7 col-md-7">
          <input type="text" class="form-control" name="grade_nm" id="grade_nm" value="<?= @$main['grade_nm'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Corp Grade <span class="text-danger">*</span></label>
        <div class="col-lg-4 col-md-7">
          <input type="text" class="form-control autonumeric" name="grade_corp" id="grade_corp" value="<?= @num_id($main['grade_corp']) ?>" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Min Grade <span class="text-danger">*</span></label>
        <div class="col-lg-4 col-md-7">
          <input type="text" class="form-control autonumeric" name="grade_min" id="grade_min" value="<?= @num_id($main['grade_min']) ?>" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Max Grade <span class="text-danger">*</span></label>
        <div class="col-lg-4 col-md-7">
          <input type="text" class="form-control autonumeric" name="grade_max" id="grade_max" value="<?= @num_id($main['grade_max']) ?>" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Status</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_active">
            <option value="1" <?php if (@$main['is_active'] == '1') echo 'selected' ?>>Aktif</option>
            <option value="0" <?php if (@$main['is_active'] == '0') echo 'selected' ?>>Tidak Aktif</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>