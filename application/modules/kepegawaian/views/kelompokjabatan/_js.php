<script type="text/javascript">
  $(document).ready(function() {
    $("#form-data").validate({
      rules: {
        kelompokjabatan_id: {
          remote: {
            url: '<?= site_url() . '/' . $nav['nav_url'] ?>/ajax/cek_id/<?= @$main['kelompokjabatan_id'] ?>',
            type: 'post',
            data: {
              kelompokjabatan_id: function() {
                return $("#kelompokjabatan_id").val();
              }
            }
          }
        },
      },
      messages: {
        kelompokjabatan_id: {
          remote: 'Kode sudah digunakan!'
        }
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');
  })
</script>