<!-- js -->
<?php $this->load->view('kepegawaian/pangkat/_js') ?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?= $form_action ?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Kode <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="pangkat_id" id="pangkat_id" value="<?= @$main['pangkat_id'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Pangkat <span class="text-danger">*</span></label>
        <div class="col-lg-7 col-md-7">
          <input type="text" class="form-control" name="pangkat_nm" id="pangkat_nm" value="<?= @$main['pangkat_nm'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Golongan <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-7">
          <input type="text" class="form-control" name="golongan" id="golongan" value="<?= @$main['golongan'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Tingkat Golongan <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-7">
          <input type="text" class="form-control" name="golongan_tingkat" id="golongan_tingkat" value="<?= @$main['golongan_tingkat'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Status</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_active">
            <option value="1" <?php if (@$main['is_active'] == '1') echo 'selected' ?>>Aktif</option>
            <option value="0" <?php if (@$main['is_active'] == '0') echo 'selected' ?>>Tidak Aktif</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>