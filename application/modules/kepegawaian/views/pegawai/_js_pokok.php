<script>
  $(document).ready(function() {
    $(".datepicker").daterangepicker({
      // maxDate: new Date(),
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: "Clear",
        format: "DD-MM-YYYY",
      },
      isInvalidDate: function(date) {
        return "";
      },
    });

    if ($(".chosen-select").length) {
      $(".chosen-select").select2();
      // addClass
      $(".filter-data").addClass("filter-data-chosen-select");
    }
    if ($(".chosen-select-filter").length) {
      $(".chosen-select-filter").select2();
      //
      $(".select2-container").css("margin-bottom", "-1px");
    }
    if ($(".select-search-menu").length) {
      $(".select-search-menu").select2({
        placeholder: "Pilih cari menu",
      });
    }
    if ($(".select-search-menu-module").length) {
      var title = $(".select-search-menu-module").attr("data-text");
      $(".select-search-menu-module").select2({
        placeholder: "Pilih cari menu di " + title,
      });
    }

    // select2

    $('.select2-container').css('width', '100%');
  })
</script>