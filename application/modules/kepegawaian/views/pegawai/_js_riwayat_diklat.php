<script>
  $(document).ready(function() {
    $("#form-diklat").validate({
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });
    $(".datepicker").daterangepicker({
      // maxDate: new Date(),
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: "Clear",
        format: "DD-MM-YYYY",
      },
      isInvalidDate: function(date) {
        return "";
      },
    });

    // select2  
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    //delete
    $(".btn-delete").on("click", function(e) {
      e.preventDefault();

      const href = $(this).data("href");

      Swal.fire({
        title: "Apakah Anda yakin?",
        text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#eb3b5a",
        cancelButtonColor: "#b2bec3",
        confirmButtonText: "Hapus",
        cancelButtonText: "Batal",
        customClass: "swal-wide",
      }).then((result) => {
        if (result.value) {
          document.location.href = href;
        }
      });
    });

    //
    $(".modal-href").click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");
      var modal_header = $(this).attr("modal-header");
      var modal_content_top = $(this).attr("modal-content-top");

      $("#modal-size")
        .removeClass("modal-lg")
        .removeClass("modal-md")
        .removeClass("modal-sm");

      $("#modal-title").html(modal_title);
      $("#modal-size").addClass("modal-" + modal_size);
      if (modal_custom_size) {
        $("#modal-size").attr(
          "style",
          "max-width: " + modal_custom_size + "px !important"
        );
      }
      if (modal_content_top) {
        $(".modal-content-top").attr(
          "style",
          "margin-top: " + modal_content_top + " !important"
        );
      }
      if (modal_header == "hidden") {
        $("#modal-header").addClass("d-none");
      } else {
        $("#modal-header").removeClass("d-none");
      }
      $("#myModal").modal("show");
      $("#modal-body").html(
        '<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i><br>Loading</div>'
      );
      $.post(
        $(this).data("href"),
        function(data) {
          $("#modal-body").html(data.html);
        },
        "json"
      );
    });
  })

  function diklat_berkas_row_delete(e) {
    $(e).closest(".diklat-berkas-row").remove();
  }

  function diklat_berkas_tambah() {
    var html = '<div class="form-group mb-2 row diklat-berkas-row">' +
      '<label class="col-lg-3 col-md-3 col-form-label">Berkas</label>' +
      '<div class="col-lg-9 col-md-7">' +
      '<input type="file" class="mb-2" name="berkas[]" value="">' +
      '<div class="row">' +
      '<div class="col-10">' +
      '<input type="text" class="form-control form-control-sm" name="title[]" value="">' +
      '</div>' +
      '<div class="col-lg-2">' +
      '<button class="btn btn-danger btn-xs diklat-berkas-delete" onclick="diklat_berkas_row_delete(this)" type="button"><i class="fas fa-times"></i></button>' +
      '</div>' +
      '</div>' +
      '</div>' +
      '</div>';
    $("#diklat-berkas").append(html);
  }
</script>