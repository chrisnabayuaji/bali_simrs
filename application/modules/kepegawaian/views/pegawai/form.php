<div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline col-title">
        <div class="col-back">
          <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
        </div>
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          <?= $nav['nav_nm'] ?>
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item"><a href="javascript:void(0)"><i class="fas fa-ambulance"></i> Rawat Jalan</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item active"><span>Form</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>

  <div class="row full-page mt-4">
    <div class="col-lg-3 grid-margin">
      <div class="card border-none">
        <div class="card-body text-center">
          <img src="<?= base_url() ?>/assets/images/user/no-image.png" class="rounded-circle" alt="<?= @$main['pegawai_nm'] ?>" height="80" width="80">
          <h6 class="mt-2" style="font-weight: bold"><?= @$main['pegawai_nm'] ?></h6>
        </div>
      </div>
    </div>
    <div class="col-lg-9 grid-margin stretch-card" style="padding-left:0!important">
      <div class="card border-none">
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link tab-menu active" id="pokok-tab" data-id="pokok" data-view-name="pokok" data-toggle="pill" href="#pokok" role="tab" aria-controls="pokok" aria-selected="false"><i class="fas fa-database"></i> Data Pokok</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link tab-menu" id="keluarga-tab" data-id="keluarga" data-view-name="keluarga" data-toggle="pill" href="#keluarga" role="tab" aria-controls="pemeriksaan-fisik" aria-selected="true"><i class="fas fa-users"></i> Keluarga</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link tab-menu" id="riwayat_pangkat-tab" data-id="riwayat_pangkat" data-view-name="riwayat_pangkat" data-toggle="pill" href="#riwayat_pangkat" role="tab" aria-controls="pemeriksaan-fisik" aria-selected="true"><i class="fas fa-user"></i> Pangkat</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link tab-menu" id="riwayat_jabatan-tab" data-id="riwayat_jabatan" data-view-name="riwayat_jabatan" data-toggle="pill" href="#riwayat_jabatan" role="tab" aria-controls="riwayat_jabatan" aria-selected="false"><i class="fas fa-user-secret"></i> Jabatan</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link tab-menu" id="riwayat_pendidikan-tab" data-id="riwayat_pendidikan" data-view-name="riwayat_pendidikan" data-toggle="pill" href="#riwayat_pendidikan" role="tab" aria-controls="riwayat_pendidikan" aria-selected="false"><i class="fas fa-graduation-cap"></i> Pendidikan</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link tab-menu" id="riwayat_diklat-tab" data-id="riwayat_diklat" data-view-name="riwayat_diklat" data-toggle="pill" href="#riwayat_diklat" role="tab" aria-controls="riwayat_diklat" aria-selected="false"><i class="fas fa-book"></i> Diklat</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link tab-menu" id="riwayat_penghargaan-tab" data-id="riwayat_penghargaan" data-view-name="riwayat_penghargaan" data-toggle="pill" href="#riwayat_penghargaan" role="tab" aria-controls="riwayat_penghargaan" aria-selected="false"><i class="fas fa-medal"></i> Penghargaan</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link tab-menu" id="riwayat_hukuman-tab" data-id="riwayat_hukuman" data-view-name="riwayat_hukuman" data-toggle="pill" href="#riwayat_hukuman" role="tab" aria-controls="riwayat_hukuman" aria-selected="false"><i class="fas fa-thumbs-down"></i> Hukuman</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link tab-menu" id="riwayat_lain-tab" data-id="riwayat_lain" data-view-name="riwayat_lain" data-toggle="pill" href="#riwayat_lain" role="tab" aria-controls="riwayat_lain" aria-selected="false"><i class="fas fa-folder-open"></i> Berkas</a>
                </li>
              </ul>
              <div class="border-dotted"></div>
              <div class="tab-content p-0 no-border mt-2" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pokok" role="tabpanel" aria-labelledby="pokok-tab">
                  <div class="media">
                    <div class="media-body" id="body-pokok">
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="keluarga" role="tabpanel" aria-labelledby="keluarga-tab">
                  <div class="media">
                    <div class="media-body" id="body-keluarga">
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="riwayat_pangkat" role="tabpanel" aria-labelledby="riwayat_pangkat-tab">
                  <div class="media">
                    <div class="media-body" id="body-riwayat_pangkat">
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="riwayat_jabatan" role="tabpanel" aria-labelledby="riwayat_jabatan-tab">
                  <div class="media">
                    <div class="media-body" id="body-riwayat_jabatan">
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="riwayat_pendidikan" role="tabpanel" aria-labelledby="riwayat_pendidikan-tab">
                  <div class="media">
                    <div class="media-body" id="body-riwayat_pendidikan">
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="riwayat_diklat" role="tabpanel" aria-labelledby="riwayat_diklat-tab">
                  <div class="media">
                    <div class="media-body" id="body-riwayat_diklat">
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="riwayat_penghargaan" role="tabpanel" aria-labelledby="riwayat_penghargaan-tab">
                  <div class="media">
                    <div class="media-body" id="body-riwayat_penghargaan">
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="riwayat_hukuman" role="tabpanel" aria-labelledby="riwayat_hukuman-tab">
                  <div class="media">
                    <div class="media-body" id="body-riwayat_hukuman">
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="riwayat_lain" role="tabpanel" aria-labelledby="riwayat_lain-tab">
                  <div class="media">
                    <div class="media-body" id="body-riwayat_lain">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('#pills-tab a').click(function(e) {
    e.preventDefault();
    $(this).tab('show');
  });

  // store the currently selected tab in the hash value
  $("ul.nav-pills > li > a").on("shown.bs.tab", function(e) {
    var id = $(e.target).attr("href").substr(1);
    var data_id = $(e.target).attr("data-id");
    if (data_id == 'link') {
      window.location.href = $(e.target).attr("href");
    } else {
      window.location.hash = id;
    }
  });

  // on load of the page: switch to the currently selected tab
  var hash = window.location.hash;
  $('#pills-tab a[href="' + hash + '"]').tab('show');

  var data_id = $(hash + '-tab').attr("data-id");
  var view_name = $(hash + '-tab').attr("data-view-name");
  get_view_tab_menu(data_id, view_name);

  function get_view_tab_menu(data_id = '', view_name = '') {
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax/view_tab_menu/' ?>', {
      view_name: view_name,
      id: '<?= $main['pegawai_id'] ?>'
    }, function(data) {
      $('#body-' + data_id).html(data.html);
    }, 'json');
  }

  $(document).ready(function() {
    $('.tab-menu').click(function(e) {
      e.preventDefault();
      var data_id = $(this).attr("data-id");
      var view_name = $(this).attr("data-view-name");

      get_view_tab_menu(data_id, view_name);
    });
  })
</script>