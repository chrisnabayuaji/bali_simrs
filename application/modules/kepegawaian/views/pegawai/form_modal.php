<!-- js -->
<?php $this->load->view('master/pegawai/_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-lg-6">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Jenis Pegawai</label>
        <div class="col-lg-5 col-md-5">
          <select class="chosen-select custom-select w-100" name="jenispegawai_cd" id="jenispegawai_cd" required="">
            <option value="">- Pilih -</option>
            <?php foreach($list_jenispegawai as $lj):?>
            <option value="<?=$lj['parameter_cd']?>" <?php if($lj['parameter_cd'] == @$main['jenispegawai_cd']) echo 'selected'?>><?=$lj['parameter_cd']?> - <?=$lj['parameter_val']?></option>
            <?php endforeach;?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Kode</label>
        <div class="col-lg-3 col-md-5">
          <input type="text" class="form-control" name="pegawai_id" id="pegawai_id" value="<?=@$main['pegawai_id']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Nama Pegawai</label>
        <div class="col-lg-7 col-md-9">
          <input type="text" class="form-control" name="pegawai_nm" value="<?=@$main['pegawai_nm']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Jenis Spesialis</label>
        <div class="col-lg-7 col-md-7">
          <select class="chosen-select custom-select w-100" name="spesialisasi_cd" id="spesialisasi_cd">
            <option value="">---</option>
            <?php foreach($list_spesialis as $ls):?>
            <option value="<?=$ls['parameter_cd']?>" <?php if($ls['parameter_cd'] == @$main['spesialisasi_cd']) echo 'selected'?>><?=$ls['parameter_cd']?> - <?=$ls['parameter_val']?></option>
            <?php endforeach;?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">NIP</label>
        <div class="col-lg-5 col-md-9">
          <input type="text" class="form-control" name="pegawai_nip" value="<?=@$main['pegawai_nip']?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Tempat Lahir</label>
        <div class="col-lg-5 col-md-9">
          <input type="text" class="form-control" name="tmp_lahir" value="<?=@$main['tmp_lahir']?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Tgl.Lahir</label>
        <div class="col-lg-3 col-md-9">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
            </div>
            <input type="text" class="form-control datepicker text-center" name="tgl_lahir" id="tgl_lahir" value="<?=(@$main['tgl_lahir'] == '0000-00-00') ? '' : to_date(@$main['tgl_lahir']) ?>">
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Jenis Kelamin</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="sex_cd" id="sex_cd">
            <option value="">---</option>
            <?php foreach($list_sex as $ls):?>
            <option value="<?=$ls['parameter_cd']?>" <?php if($ls['parameter_cd'] == @$main['sex_cd']) echo 'selected'?>><?=$ls['parameter_cd']?> - <?=$ls['parameter_val']?></option>
            <?php endforeach;?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Agama</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="agama_cd" id="agama_cd">
            <option value="">---</option>
            <?php foreach($list_agama as $la):?>
            <option value="<?=$la['parameter_cd']?>" <?php if($la['parameter_cd'] == @$main['agama_cd']) echo 'selected'?>><?=$la['parameter_cd']?> - <?=$la['parameter_val']?></option>
            <?php endforeach;?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Pendidikan</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="pendidikan_cd" id="pendidikan_cd">
            <option value="">---</option>
            <?php foreach($list_pendidikan as $lp):?>
            <option value="<?=$lp['parameter_cd']?>" <?php if($lp['parameter_cd'] == @$main['pendidikan_cd']) echo 'selected'?>><?=$lp['parameter_cd']?> - <?=$lp['parameter_val']?></option>
            <?php endforeach;?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Status</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_active">
            <option value="1" <?php if(@$main['is_active'] == '1') echo 'selected'?>>Aktif</option>
            <option value="0" <?php if(@$main['is_active'] == '0') echo 'selected'?>>Tidak Aktif</option>
          </select>
        </div>
      </div>
    </div>

    <div class="col-lg-6">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Propinsi</label>
        <div class="col-lg-7 col-md-7">
          <select class="chosen-select custom-select w-100" name="wilayah_prop" id="wilayah_prop">
            <option value="">- Propinsi -</option>
            <?php foreach($list_wilayah_prop as $wp):?>
            <option value="<?=$wp['wilayah_id']?>" <?php if($wp['wilayah_id'] == @$main['wilayah_prop']) echo 'selected'?>><?=$wp['wilayah_id']?> - <?=$wp['wilayah_nm']?></option>
            <?php endforeach;?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Kab/Kota</label>
        <div class="col-lg-7 col-md-7">
          <div id="box_wilayah_kab">
            <select class="chosen-select custom-select w-100" name="wilayah_kab" id="wilayah_kab">
              <option value="">- Kab/Kota -</option>
            </select>
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Kecamatan</label>
        <div class="col-lg-7 col-md-7">
          <div id="box_wilayah_kec">
            <select class="chosen-select custom-select w-100" name="wilayah_kec" id="wilayah_kec">
              <option value="">- Kecamatan -</option>
            </select>
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Desa/Kelurahan</label>
        <div class="col-lg-7 col-md-7">
          <div id="box_wilayah_kel">
            <select class="chosen-select custom-select w-100" name="wilayah_kel" id="wilayah_kel">
              <option value="">- Desa/Kelurahan -</option>
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-10 offset-md-10">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>