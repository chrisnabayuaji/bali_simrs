<?= $this->load->view('_js_pokok') ?>
<form id="pokok_form" action="" method="post" autocomplete="off">
  <div class="form-group row">
    <label class="col-lg-2 col-md-3 col-form-label">Kode Pegawai <span class="text-danger">*</span></label>
    <div class="col-lg-2 col-md-3">
      <input class="form-control" type="text" id="pegawai_id" name="pokok_pegawai_id" value="<?= @$pegawai['pegawai_id'] ?>" required />
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-2 col-md-3 col-form-label">Nama Lengkap <span class="text-danger">*</span></label>
    <div class="col-lg-5 col-md-3">
      <input class="form-control" style="font-weight: bold;" type="text" id="pegawai_nm" name="pokok_pegawai_nm" value="<?= @$pegawai['pegawai_nm'] ?>" required />
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-2 col-md-3 col-form-label">NIP</label>
    <div class="col-lg-3 col-md-3">
      <input class="form-control" type="text" id="pegawai_nip" name="pokok_pegawai_nip" value="<?= @$pegawai['pegawai_nip'] ?>" />
    </div>
    <label class="col-lg-1 col-md-3 col-form-label">NIP Lama </label>
    <div class="col-lg-3 col-md-3">
      <input class="form-control" type="text" id="pegawai_nip_lama" name="pokok_pegawai_nip_lama" value="<?= @$pegawai['pegawai_nip_lama'] ?>" />
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-2 col-md-3 col-form-label">No KTP </label>
    <div class="col-lg-3 col-md-3">
      <input class="form-control" type="text" id="ktp_no" name="pokok_ktp_no" value="<?= @$pegawai['ktp_no'] ?>" />
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-2 col-md-3 col-form-label">NPWP </label>
    <div class="col-lg-3 col-md-3">
      <input class="form-control" type="text" id="npwp_no" name="pokok_npwp_no" value="<?= @$pegawai['npwp_no'] ?>" />
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-2 col-md-3 col-form-label">Tempat Tanggal Lahir <span class="text-danger">*</span></label>
    <div class="col-lg-2 col-md-3">
      <input class="form-control" type="text" id="tmp_lahir" name="pokok_tmp_lahir" value="<?= @$pegawai['tmp_lahir'] ?>" required />
    </div>
    ,
    <div class="col-lg-2 col-md-3">
      <input type="text" class="form-control datepicker" name="tgl_lahir" id="pokok_tgl_lahir" value="<?= (@$pegawai['tgl_lahir'] == '0000-00-00' || @$pegawai['tgl_lahir'] == '') ? '' : to_date(@$pegawai['tgl_lahir']) ?>">
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-2 col-md-3 col-form-label">Jenis Kelamin <span class="text-danger">*</span></label>
    <div class="col-lg-2 col-md-2">
      <select class="form-control chosen-select" name="sex_cd" id="pokok_sex_cd" required>
        <option value="">-- Pilih --</option>
        <?php foreach (get_parameter('sex_cd') as $r) : ?>
          <option value="<?= $r['parameter_cd'] ?>" <?= (@$pegawai['sex_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-2 col-md-3 col-form-label">Agama <span class="text-danger">*</span></label>
    <div class="col-lg-2 col-md-2">
      <select class="form-control chosen-select" name="agama_cd" id="pokok_agama_cd" required>
        <option value="">-- Pilih --</option>
        <?php foreach (get_parameter('agama_cd') as $r) : ?>
          <option value="<?= $r['parameter_cd'] ?>" <?= (@$pegawai['agama_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-2 col-md-3 col-form-label">Alamat <span class="text-danger">*</span></label>
    <div class="col-lg-5 col-md-3">
      <input class="form-control" type="text" id="alamat_lengkap" name="pokok_alamat_lengkap" value="<?= @$pegawai['alamat_lengkap'] ?>" required />
    </div>
  </div>
  <div class="border-dotted mb-2"></div>
  <div class="form-group row">
    <label class="col-lg-2 col-md-3 col-form-label">Jenis Pegawai <span class="text-danger">*</span></label>
    <div class="col-lg-2 col-md-3">
      <select class="chosen-select custom-select w-100" name="jenispegawai_cd" id="pokok_jenispegawai_cd" required>
        <option value="">- Pilih -</option>
        <?php foreach ($list_jenispegawai as $lj) : ?>
          <option value="<?= $lj['parameter_cd'] ?>" <?php if ($lj['parameter_cd'] == @$pegawai['jenispegawai_cd']) echo 'selected' ?>><?= $lj['parameter_cd'] ?> - <?= $lj['parameter_val'] ?></option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-2 col-md-3 col-form-label">Status Pegawai <span class="text-danger">*</span></label>
    <div class="col-lg-2 col-md-3">
      <select class="chosen-select custom-select w-100" name="statuspegawai_cd" id="pokok_statuspegawai_cd" required>
        <option value="">- Pilih -</option>
        <?php foreach (get_parameter('statuspegawai_cd') as $r) : ?>
          <option value="<?= $r['parameter_cd'] ?>" <?= (@$pegawai['statuspegawai_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>
  <div class="border-dotted mt-2"></div>
  <div class="row mt-2">
    <div class="col-lg-9 offset-lg-2 p-0">
      <button type="submit" id="pemeriksaan_fisik_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
    </div>
  </div>
</form>