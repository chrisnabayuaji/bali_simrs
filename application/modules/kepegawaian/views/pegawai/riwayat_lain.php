<?= $this->load->view('_js_riwayat_lain') ?>
<div class="col pl-0 pr-0 mb-2 mt-0">
  <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/riwayat_lain_modal/' . $pegawai['pegawai_id'] ?>" modal-title="Tambah Data" modal-size="md" class="btn btn-xs btn-primary modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Tambah Data"><i class="fas fa-plus-circle"></i> Tambah</a>
</div>
<div class="col pl-0 pr-0">
  <table class="table table-sm table-bordered table-striped">
    <thead>
      <tr>
        <th class="text-center" width="50">No.</th>
        <th class="text-center" width="80">Aksi</th>
        <th class="text-center">Judul Berkas</th>
        <th class="text-center" width="120">Tanggal</th>
      </tr>
    </thead>
    <tbody>
      <?php if ($list_riwayat_lain == null) : ?>
        <tr>
          <td class="text-center" colspan="99"><i>Tidak ada data</i></td>
        </tr>
      <?php else : ?>
        <?php foreach ($list_riwayat_lain as $k => $v) : ?>
          <tr>
            <td class="text-center"><?= $k + 1 ?></td>
            <td class="text-center">
              <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/riwayat_lain_modal/' . $pegawai['pegawai_id'] . '/' . $v['riwayatlain_id'] ?>" modal-title="Tambah Data" modal-size="md" class="btn btn-primary btn-table modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Ubah Data"><i class="fas fa-pencil-alt"></i></a>
              <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/riwayat_lain_delete/' . $pegawai['pegawai_id'] . '/' . $v['riwayatlain_id'] ?>" class="btn btn-danger btn-table btn-delete" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-danger" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
            </td>
            <td><?= $v['lain_nm'] ?></td>
            <td class="text-center"><?= to_date($v['tgl']) ?></td>
          </tr>
        <?php endforeach; ?>
      <?php endif; ?>
    </tbody>
  </table>
</div>