<!-- js -->
<?php $this->load->view('_js_riwayat_lain') ?>
<!-- / -->
<form role="form" id="form-lain" method="post" enctype="multipart/form-data" action="<?= $form_action ?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <input type="hidden" class="form-control" name="riwayatlain_id" value="<?= @$riwayatlain['riwayatlain_id'] ?>" required="">
      <input type="hidden" class="form-control" name="pegawai_id" value="<?= $pegawai['pegawai_id'] ?>" required="">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Judul <span class="text-danger">*</span></label>
        <div class="col-lg-7 col-md-7">
          <input type="text" class="form-control" name="lain_nm" value="<?= @$riwayatlain['lain_nm'] ?>" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Tanggal <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-7">
          <input type="text" class="form-control datepicker" name="tgl" value="<?= (@$riwayatlain['tgl'] == '0000-00-00' || @$riwayatlain['tgl'] == '') ? '' : to_date(@$riwayatlain['tgl']) ?>">
        </div>
      </div>
      <div class="border-dotted mb-2"></div>
      <h6 style="font-weight:bold;">Berkas</h6>
      <div id="lain-berkas" class="mb-2">
        <?php if (@$riwayatlain['berkas'] != null) : ?>
          <?php foreach (@$riwayatlain['berkas'] as $k => $v) : ?>
            <div class="form-group mb-2 row lain-berkas-row">
              <label class="col-lg-3 col-md-3 col-form-label">Berkas</label>
              <div class="col-lg-9 col-md-7">
                <div class="row">
                  <div class="col-10">
                    <div class="input-group">
                      <input type="text" readonly class="form-control form-control-sm" value="<?= $v['title'] ?>">
                      <div class="input-group-append">
                        <a href="<?= base_url() . 'assets/images/riwayat_lain/' . $v['filename'] ?>" target="_blank" class="btn btn-success btn-xs" type="button"><i class="fas fa-eye"></i></a>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/riwayat_lain_berkas_delete/' . $riwayatlain['pegawai_id'] . '/' . $v['riwayatlainberkas_id'] ?>" class="btn btn-danger btn-xs btn-delete" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-danger" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        <?php else : ?>
          <div class="form-group mb-2 row lain-berkas-row">
            <label class="col-lg-3 col-md-3 col-form-label">Berkas</label>
            <div class="col-lg-9 col-md-7">
              <input type="file" class="mb-2" name="berkas[]" value="">
              <div class="row">
                <div class="col-10">
                  <input type="text" class="form-control form-control-sm" name="title[]" value="">
                </div>
                <div class="col-lg-2">
                  <button class="btn btn-danger btn-xs lain-berkas-delete" onclick="lain_berkas_row_delete(this)" type="button"><i class="fas fa-times"></i></button>
                </div>
              </div>
            </div>
          </div>
        <?php endif; ?>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label"></label>
        <div class="col-lg-4 col-md-7">
          <button id="lain-berkas-tambah" onclick="lain_berkas_tambah()" class="btn btn-default btn-xs" type="button"><i class="fas fa-plus"></i> Tambah Berkas</button>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>