<!-- js -->
<?php $this->load->view('_js_riwayat_pangkat') ?>
<!-- / -->
<form role="form" id="form-pangkat" method="post" enctype="multipart/form-data" action="<?= $form_action ?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <input type="hidden" class="form-control" name="riwayatpangkat_id" value="<?= @$riwayatpangkat['riwayatpangkat_id'] ?>" required="">
      <input type="hidden" class="form-control" name="pegawai_id" value="<?= $pegawai['pegawai_id'] ?>" required="">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Pangkat <span class="text-danger">*</span></label>
        <div class="col-lg-7 col-md-7">
          <select class="chosen-select custom-select w-100" name="pangkat_id" required="">
            <option value="">- Pilih -</option>
            <?php foreach ($list_pangkat as $r) : ?>
              <option value="<?= $r['pangkat_id'] ?>" <?php if ($r['pangkat_id'] == @$riwayatpangkat['pangkat_id']) echo 'selected' ?>><?= $r['golongan'] ?> - <?= $r['pangkat_nm'] ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Pangkat TMT <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-7">
          <input type="text" class="form-control datepicker" name="pangkat_tmt" value="<?= (@$riwayatpangkat['pangkat_tmt'] == '0000-00-00' || @$riwayatpangkat['pangkat_tmt'] == '') ? '' : to_date(@$riwayatpangkat['pangkat_tmt']) ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">No SK <span class="text-danger">*</span></label>
        <div class="col-lg-7 col-md-7">
          <input type="text" class="form-control" name="sk_no" value="<?= @$riwayatpangkat['sk_no'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Tgl SK <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-7">
          <input type="text" class="form-control datepicker" name="sk_tgl" value="<?= (@$riwayatpangkat['sk_tgl'] == '0000-00-00' || @$riwayatpangkat['sk_tgl'] == '') ? '' : to_date(@$riwayatpangkat['sk_tgl']) ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Masa Kerja <span class="text-danger">*</span></label>
        <div class="col-lg-4 col-md-7">
          <div class="input-group">
            <input type="number" class="form-control" name="masakerja_thn" value="<?= @$riwayatpangkat['masakerja_thn'] ?>" required="">
            <div class="input-group-append">
              <span class="input-group-text">Thn</span>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-7">
          <div class="input-group">
            <input type="number" class="form-control" name="masakerja_bln" value="<?= @$riwayatpangkat['masakerja_bln'] ?>" required="">
            <div class="input-group-append">
              <span class="input-group-text">Bln</span>
            </div>
          </div>
        </div>
      </div>
      <div class="border-dotted mb-2"></div>
      <h6 style="font-weight:bold;">Berkas</h6>
      <div id="pangkat-berkas" class="mb-2">
        <?php if (@$riwayatpangkat['berkas'] != null) : ?>
          <?php foreach (@$riwayatpangkat['berkas'] as $k => $v) : ?>
            <div class="form-group mb-2 row pangkat-berkas-row">
              <label class="col-lg-3 col-md-3 col-form-label">Berkas</label>
              <div class="col-lg-9 col-md-7">
                <div class="row">
                  <div class="col-10">
                    <div class="input-group">
                      <input type="text" readonly class="form-control form-control-sm" value="<?= $v['title'] ?>">
                      <div class="input-group-append">
                        <a href="<?= base_url() . 'assets/images/riwayat_pangkat/' . $v['filename'] ?>" target="_blank" class="btn btn-success btn-xs" type="button"><i class="fas fa-eye"></i></a>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/riwayat_pangkat_berkas_delete/' . $riwayatpangkat['pegawai_id'] . '/' . $v['riwayatpangkatberkas_id'] ?>" class="btn btn-danger btn-xs btn-delete" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-danger" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        <?php else : ?>
          <div class="form-group mb-2 row pangkat-berkas-row">
            <label class="col-lg-3 col-md-3 col-form-label">Berkas</label>
            <div class="col-lg-9 col-md-7">
              <input type="file" class="mb-2" name="berkas[]" value="">
              <div class="row">
                <div class="col-10">
                  <input type="text" class="form-control form-control-sm" name="title[]" value="">
                </div>
                <div class="col-lg-2">
                  <button class="btn btn-danger btn-xs pangkat-berkas-delete" onclick="pangkat_berkas_row_delete(this)" type="button"><i class="fas fa-times"></i></button>
                </div>
              </div>
            </div>
          </div>
        <?php endif; ?>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label"></label>
        <div class="col-lg-4 col-md-7">
          <button id="pangkat-berkas-tambah" onclick="pangkat_berkas_tambah()" class="btn btn-default btn-xs" type="button"><i class="fas fa-plus"></i> Tambah Berkas</button>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>