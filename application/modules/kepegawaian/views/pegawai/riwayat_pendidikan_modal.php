<!-- js -->
<?php $this->load->view('_js_riwayat_pendidikan') ?>
<!-- / -->
<form role="form" id="form-pendidikan" method="post" enctype="multipart/form-data" action="<?= $form_action ?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <input type="hidden" class="form-control" name="riwayatpendidikan_id" value="<?= @$riwayatpendidikan['riwayatpendidikan_id'] ?>" required="">
      <input type="hidden" class="form-control" name="pegawai_id" value="<?= $pegawai['pegawai_id'] ?>" required="">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Pendidikan <span class="text-danger">*</span></label>
        <div class="col-lg-7 col-md-7">
          <select class="chosen-select custom-select w-100" name="pendidikan_id" required="">
            <option value="">- Pilih -</option>
            <?php foreach ($list_pendidikan as $r) : ?>
              <option value="<?= $r['pendidikan_id'] ?>" <?php if ($r['pendidikan_id'] == @$riwayatpendidikan['pendidikan_id']) echo 'selected' ?>><?= $r['pendidikan_nm'] ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Nama Sekolah <span class="text-danger">*</span></label>
        <div class="col-lg-7 col-md-7">
          <input type="text" class="form-control" name="sekolah_nm" value="<?= @$riwayatpendidikan['sekolah_nm'] ?>" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Tanggal Lulus <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-7">
          <input type="text" class="form-control datepicker" name="lulus_tgl" value="<?= (@$riwayatpendidikan['lulus_tgl'] == '0000-00-00' || @$riwayatpendidikan['lulus_tgl'] == '') ? '' : to_date(@$riwayatpendidikan['pendidikan_tmt']) ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">No Ijazah <span class="text-danger">*</span></label>
        <div class="col-lg-7 col-md-7">
          <input type="text" class="form-control" name="ijazah_no" value="<?= @$riwayatpendidikan['ijazah_no'] ?>" required="">
        </div>
      </div>
      <div class="border-dotted mb-2"></div>
      <h6 style="font-weight:bold;">Berkas</h6>
      <div id="pendidikan-berkas" class="mb-2">
        <?php if (@$riwayatpendidikan['berkas'] != null) : ?>
          <?php foreach (@$riwayatpendidikan['berkas'] as $k => $v) : ?>
            <div class="form-group mb-2 row pendidikan-berkas-row">
              <label class="col-lg-3 col-md-3 col-form-label">Berkas</label>
              <div class="col-lg-9 col-md-7">
                <div class="row">
                  <div class="col-10">
                    <div class="input-group">
                      <input type="text" readonly class="form-control form-control-sm" value="<?= $v['title'] ?>">
                      <div class="input-group-append">
                        <a href="<?= base_url() . 'assets/images/riwayat_pendidikan/' . $v['filename'] ?>" target="_blank" class="btn btn-success btn-xs" type="button"><i class="fas fa-eye"></i></a>
                      </div>
                    </div>
                  </div>
                  <div class="col-lg-2">
                    <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/riwayat_pendidikan_berkas_delete/' . $riwayatpendidikan['pegawai_id'] . '/' . $v['riwayatpendidikanberkas_id'] ?>" class="btn btn-danger btn-xs btn-delete" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-danger" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
                  </div>
                </div>
              </div>
            </div>
          <?php endforeach; ?>
        <?php else : ?>
          <div class="form-group mb-2 row pendidikan-berkas-row">
            <label class="col-lg-3 col-md-3 col-form-label">Berkas</label>
            <div class="col-lg-9 col-md-7">
              <input type="file" class="mb-2" name="berkas[]" value="">
              <div class="row">
                <div class="col-10">
                  <input type="text" class="form-control form-control-sm" name="title[]" value="">
                </div>
                <div class="col-lg-2">
                  <button class="btn btn-danger btn-xs pendidikan-berkas-delete" onclick="pendidikan_berkas_row_delete(this)" type="button"><i class="fas fa-times"></i></button>
                </div>
              </div>
            </div>
          </div>
        <?php endif; ?>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label"></label>
        <div class="col-lg-4 col-md-7">
          <button id="pendidikan-berkas-tambah" onclick="pendidikan_berkas_tambah()" class="btn btn-default btn-xs" type="button"><i class="fas fa-plus"></i> Tambah Berkas</button>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>