<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_barang extends CI_Model {

	public function where($cookie)
	{
		$where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['tgl_order_from'] != '' && @$cookie['search']['tgl_order_to']) {
      $where .= "AND (DATE(a.tgl_order_depo) BETWEEN '".to_date(@$cookie['search']['tgl_order_from'])."' AND '".to_date(@$cookie['search']['tgl_order_to'])."')";
    }
    if (@$cookie['search']['tgl_distribusi_from'] != '' && @$cookie['search']['tgl_distribusi_to']) {
      $where .= "AND (DATE(a.tgl_distribusi) BETWEEN '".to_date(@$cookie['search']['tgl_distribusi_from'])."' AND '".to_date(@$cookie['search']['tgl_distribusi_to'])."')";
    }
    if (@$cookie['search']['lokasi_id'] != '') {
      $where .= "AND a.lokasi_id = '".$this->db->escape_like_str($cookie['search']['lokasi_id'])."' ";
    }
    if (@$cookie['search']['distribusi_st'] != '') {
      $where .= "AND a.distribusi_st = '".$this->db->escape_like_str($cookie['search']['distribusi_st'])."' ";
    }
		return $where;
	}

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT a.*, b.lokasi_nm 
            FROM inv_distribusi a
            LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
            $where
            ORDER BY "
              .$cookie['order']['field']." ".$cookie['order']['type'].
            " LIMIT ".$cookie['cur_page'].",".$cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM inv_distribusi a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM inv_distribusi a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id) {
    $sql = "SELECT 
              a.* 
            FROM inv_distribusi a
            WHERE a.distribusi_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function rinc_data($id)
  {
    $sql = "SELECT a.*, b.parameter_val AS satuan 
    FROM inv_stok_depo a
    LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
    WHERE distribusi_id=?
    ORDER BY stokdepo_id ASC";
    $query = $this->db->query($sql, array($id));
    $res = $query->result_array();
    return $res;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());

    // save inv_distribusi
    $inv_distribusi['lokasi_id'] = $data['lokasi_id'];
    $inv_distribusi['no_distribusi'] = $data['no_distribusi'];
    $inv_distribusi['tgl_order_depo'] = to_date($data['tgl_order_depo'], '', 'full_date');
    $inv_distribusi['keterangan_order_depo'] = $data['keterangan_order_depo'];
    $inv_distribusi['tgl_distribusi'] = to_date($data['tgl_distribusi'], '', 'full_date');
    $inv_distribusi['keterangan_distribusi'] = $data['keterangan_distribusi'];
    $inv_distribusi['distribusi_st'] = $data['distribusi_st'];
    $inv_distribusi['user_cd'] = $this->session->userdata('sess_user_cd');
    if ($id == null) {
      $inv_distribusi['distribusi_id'] = get_id('inv_distribusi');
      $inv_distribusi['created_at'] = date('Y-m-d H:i:s');
      $inv_distribusi['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('inv_distribusi', $inv_distribusi);
      update_id('inv_distribusi', $inv_distribusi['distribusi_id']);
    }else{
      $inv_distribusi['updated_at'] = date('Y-m-d H:i:s');
      $inv_distribusi['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('distribusi_id', $id)->update('inv_distribusi', $inv_distribusi);
    }

    // save inv_stok_depo
    if (isset($data['stok_id'])) {
      foreach ($data['stok_id'] as $key => $val) {
        $stok = $this->db->where('stok_id', $val)->get('inv_stok')->row_array();

        if (@$inv_distribusi['distribusi_id'] !='') {
          $inv_stok_depo['distribusi_id'] = $inv_distribusi['distribusi_id'];
        }else{
          $inv_stok_depo['distribusi_id'] = $data['distribusi_id'];
        }
        $inv_stok_depo['stok_id'] = $val;
        $inv_stok_depo['lokasi_id'] = $data['lokasi_id'];
        $inv_stok_depo['barang_id'] = $stok['barang_id'];
        $inv_stok_depo['barang_nm'] = $stok['barang_nm'];
        $inv_stok_depo['satuan_cd'] = $stok['satuan_cd'];
        $inv_stok_depo['tgl_expired'] = $stok['tgl_expired'];
        $inv_stok_depo['harga_beli'] = $stok['harga_beli'];
        $inv_stok_depo['harga_jual'] = $stok['harga_jual'];
        $inv_stok_depo['harga_akhir'] = $stok['harga_akhir'];
        $inv_stok_depo['jenisbarang_cd'] = $stok['jenisbarang_cd'];
        $inv_stok_depo['keterangan_barang'] = $data['keterangan_barang'][$key];
        $inv_stok_depo['stok_awal'] = $stok['stok_awal'];
        $inv_stok_depo['stok_penerimaan'] = $data['qty'][$key];
        // $inv_stok_depo['stok_terpakai'] = $stok['stok_terpakai'];
        $inv_stok_depo['stok_retur_keluar'] = $stok['stok_retur_keluar'];
        $inv_stok_depo['stok_akhir'] = $data['qty'][$key];
        $inv_stok_depo['stok_opname'] = $stok['stok_opname'];
        $inv_stok_depo['tgl_catat'] = date('Y-m-d H:i:s');
        $inv_stok_depo['stok_st'] = $data['distribusi_st'];
        if ($data['stokdepo_id'][$key] == null) {
          $inv_stok_depo['stokdepo_id'] = get_id('inv_stok_depo');
          $inv_stok_depo['created_at'] = date('Y-m-d H:i:s');
          $inv_stok_depo['created_by'] = $this->session->userdata('sess_user_realname');
          $this->db->insert('inv_stok_depo', $inv_stok_depo);
          update_id('inv_stok_depo', $inv_stok_depo['stokdepo_id']);
        }else{
          $inv_stok_depo['updated_at'] = date('Y-m-d H:i:s');
          $inv_stok_depo['updated_by'] = $this->session->userdata('sess_user_realname');
          $this->db->where('stokdepo_id', $data['stokdepo_id'][$key])->update('inv_stok_depo', $inv_stok_depo);
        }

        // update inv_stok
        if ($data['stokdepo_id'][$key] == null) {
          $sql = "UPDATE inv_stok
                  SET stok_distribusi=ifnull(stok_distribusi,0) + '".$data['qty'][$key]."', stok_akhir=ifnull(stok_akhir,0) - '".$data['qty'][$key]."'
                  WHERE stok_id='".$val."'";               
          $query = $this->db->query($sql);
        }else{
          $sql = "UPDATE inv_stok
                  SET stok_distribusi=ifnull(stok_distribusi,0) - ".$data['qty_sebelum_edit'][$key]." + '".$data['qty'][$key]."', stok_akhir=ifnull(stok_akhir,0) + ".$data['qty_sebelum_edit'][$key]." - '".$data['qty'][$key]."'
                  WHERE stok_id='".$val."'";     
          $query = $this->db->query($sql);
        }

      }
    }

    if (isset($data['delete_id'])) {
      foreach ($data['delete_id'] as $key => $val) {
        $this->db->where('stokdepo_id', $data['delete_id'][$key])->delete('inv_stok_depo');
      }
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('distribusi_id',$id)->update('inv_distribusi', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('distribusi_id', $id)->delete('inv_distribusi');
      $this->db->where('distribusi_id', $id)->delete('inv_distribusi_rinc');
    }else{
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('distribusi_id', $id)->update('inv_distribusi', $data);
      $this->db->where('distribusi_id', $id)->update('inv_distribusi_rinc', $data);
    }
  }

  public function barang_autocomplete($barang_nm=null) {
    $sql = "SELECT 
              a.*, b.parameter_val AS satuan
            FROM inv_stok a
            LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
            WHERE (a.barang_nm LIKE '%$barang_nm%' OR a.barang_id LIKE '%$barang_nm%')
            ORDER BY a.barang_id";                
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['barang_id']."#".$row['barang_nm'].'#'.$row['satuan'].'#'.$row['stok_id'],
        'text' => $row['barang_id'].' - '.$row['barang_nm']
      );
    }
    return $res;
  }

  public function stok_row($id)
  {
    $sql = "SELECT 
              a.*, b.parameter_val AS satuan 
            FROM inv_stok a 
            LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
            WHERE a.stok_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }
  
}