<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_penerimaan extends CI_Model {

	public function where($cookie)
	{
		$where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND a.penerimaan_nm LIKE '%".$this->db->escape_like_str($cookie['search']['term'])."%' ";
		}
		return $where;
	}

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT a.*, b.no_order, c.supplier_nm
      FROM inv_penerimaan a
      LEFT JOIN inv_order b ON a.order_id = b.order_id
      LEFT JOIN mst_supplier c ON b.supplier_id = c.supplier_id
      $where
      ORDER BY "
        .$cookie['order']['field']." ".$cookie['order']['type'].
      " LIMIT ".$cookie['cur_page'].",".$cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM inv_penerimaan a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM inv_penerimaan a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id) {
    $sql = "SELECT * FROM inv_penerimaan WHERE penerimaan_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function rinc_data($id)
  {
    $sql = "SELECT a.*, b.parameter_val AS satuan 
    FROM inv_penerimaan_rinc a
    LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
    WHERE penerimaan_id=?";
    $query = $this->db->query($sql, array($id));
    $res = $query->result_array();
    return $res;
  }

  public function rincian_penerimaan_data($order_id, $id = null)
  {
    if ($id == null) {
      $sql = "SELECT a.*, b.parameter_val AS satuan, c.jenisbarang_cd, a.qty AS qty_pesanan
        FROM inv_order_rinc a
        LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
        LEFT JOIN mst_barang c ON a.barang_id = c.barang_id
        WHERE a.order_id=?";
      $query = $this->db->query($sql, $order_id);
    }else{
      $sql = "SELECT a.*, b.parameter_val AS satuan, c.jenisbarang_cd, DATE_FORMAT(a.tgl_expired, '%d-%m-%Y') AS convert_tgl_expired 
        FROM inv_penerimaan_rinc a
        LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
        LEFT JOIN mst_barang c ON a.barang_id = c.barang_id
        WHERE a.penerimaan_id=?";
      $query = $this->db->query($sql, $id);
    }
    
    $res = $query->result_array();
    return $res;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());

    // save inv_penerimaan
    $data_penerimaan['order_id'] = $data['order_id'];
    $data_penerimaan['no_faktur'] = $data['no_faktur'];
    $data_penerimaan['jml_total_awal'] = clear_numeric($data['jml_sub_total']);
    $data_penerimaan['jml_prs_diskon'] = clear_numeric($data['jml_prs_diskon']);
    $data_penerimaan['jml_nom_diskon'] = clear_numeric($data['jml_nom_diskon']);
    $data_penerimaan['jml_prs_ppn'] = clear_numeric($data['jml_prs_ppn']);
    $data_penerimaan['jml_nom_ppn'] = clear_numeric($data['jml_nom_ppn']);
    $data_penerimaan['jml_total_akhir'] = clear_numeric($data['jml_total']);
    $data_penerimaan['jml_nom_dp'] = clear_numeric($data['jml_dp']);
    $data_penerimaan['jml_total_tagihan'] = clear_numeric($data['jml_tagihan']);
    $data_penerimaan['jml_total_bayar'] = clear_numeric($data['jml_dibayar']);
    $data_penerimaan['sumberdana_cd'] = $data['sumberdana_cd'];
    $data_penerimaan['bayar_st'] = $data['bayar_st'];
    $data_penerimaan['carabayar_cd'] = $data['carabayar_cd'];
    $data_penerimaan['tgl_bayar'] = to_date($data['tgl_bayar']);
    $data_penerimaan['tgl_faktur'] = to_date($data['tgl_faktur']);
    $data_penerimaan['tgl_jatem'] = to_date($data['tgl_jatem']);
    $data_penerimaan['keteranganpenerimaan'] = $data['keteranganpenerimaan'];
    $data_penerimaan['tgl_catat'] = date("Y-m-d H:i:s");
    $data_penerimaan['user_cd'] = $this->session->userdata('sess_user_cd');
    $data_penerimaan['jurnal_id'] = '';
    if ($id == null) {
      $data_penerimaan['penerimaan_id'] = get_id('inv_penerimaan');
      $data_penerimaan['created_at'] = date('Y-m-d H:i:s');
      $data_penerimaan['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('inv_penerimaan', $data_penerimaan);
      update_id('inv_penerimaan', $data_penerimaan['penerimaan_id']);
    }else{
      $data_penerimaan['updated_at'] = date('Y-m-d H:i:s');
      $data_penerimaan['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('penerimaan_id', $id)->update('inv_penerimaan', $data_penerimaan);
    }

    for ($i=1; $i <= @$data['nomor'] ; $i++) { 
      if (@$data['is_sesuai_order_'.$i] == 1) {
        // get mst_barang
        $get_barang = $this->db->where('barang_id', $data['barang_id_'.$i])->get('mst_barang')->row_array();
        // save inv_stok
        $stok_id = @$data['stok_id_'.$i];
        $data_stok['barang_id'] = $data['barang_id_'.$i];
        $data_stok['barang_nm'] = $data['barang_nm_'.$i];
        $data_stok['satuan_cd'] = $data['satuan_cd_'.$i];
        $data_stok['tgl_expired'] = to_date($data['tgl_expired_'.$i]);
        $data_stok['harga_beli'] = clear_numeric($data['harga_beli_'.$i]);
        $data_stok['harga_jual'] = clear_numeric($data['harga_jual_'.$i]);
        // $data_stok['harga_akhir'] = clear_numeric($data['sub_total_akhir_'.$i]);
        $data_stok['harga_akhir'] = clear_numeric($data['harga_jual_'.$i]);
        $data_stok['jenisbarang_cd'] = $data['jenisbarang_cd_'.$i];
        $data_stok['keterangan_barang'] = $data['keteranganpenerimaan_rinc_'.$i];
        $data_stok['stok_penerimaan'] = $data['qty_diterima_'.$i];
        // $data_stok['stok_terpakai'] = 0;
        $data_stok['stok_retur_keluar'] = 0;
        $data_stok['stok_akhir'] = 0;
        $data_stok['stok_opname'] = 0;
        $data_stok['tgl_catat'] = date("Y-m-d H:i:s");
        $data_stok['stok_st'] = 1;
        //
        if($stok_id != '') {
          // get_inv_stok
          $get_inv_stok = $this->db->where('stok_id', $stok_id)->get('inv_stok')->row_array();
          $data_stok['stok_akhir'] = @$get_inv_stok['stok_awal'] + $data['qty_diterima_'.$i] - @$get_inv_stok['stok_distribusi'] + @$get_inv_stok['stok_retur_masuk'] - @$get_inv_stok['stok_retur_keluar'] - @$get_inv_stok['stok_musnah'];
          $data_stok['updated_at'] = date('Y-m-d H:i:s');
          $data_stok['updated_by'] = $this->session->userdata('sess_user_realname');
          $this->db->where('stok_id', $stok_id);
          $this->db->update('inv_stok', $data_stok);
        } else {
          $data_stok['stok_akhir'] = $data['qty_diterima_'.$i];
          $data_stok['stok_id'] = get_id('inv_stok');
          $data_stok['created_at'] = date('Y-m-d H:i:s');
          $data_stok['created_by'] = $this->session->userdata('sess_user_realname');
          $this->db->insert('inv_stok', $data_stok);
          update_id('inv_stok', $data_stok['stok_id']);
        }

        // save inv_penerimaan_rinc
        $penerimaanrincian_id = @$data['penerimaanrincian_id_'.$i];
        $data_pn_rinc['barang_id'] = $data['barang_id_'.$i];
        $data_pn_rinc['barang_nm'] = $data['barang_nm_'.$i];
        $data_pn_rinc['satuan_cd'] = $data['satuan_cd_'.$i];
        $data_pn_rinc['qty_pesanan'] = $data['qty_pesanan_'.$i];
        $data_pn_rinc['qty_diterima'] = $data['qty_diterima_'.$i];
        $data_pn_rinc['harga_beli'] = clear_numeric($data['harga_beli_'.$i]);
        $data_pn_rinc['sub_total_awal'] = clear_numeric($data['sub_total_awal_'.$i]);
        $data_pn_rinc['prs_diskon'] = clear_numeric($data['prs_diskon_'.$i]);
        $data_pn_rinc['nom_diskon'] = clear_numeric($data['nom_diskon_'.$i]);
        $data_pn_rinc['prs_ppn'] = clear_numeric($data['prs_ppn_'.$i]);
        $data_pn_rinc['nom_ppn'] = clear_numeric($data['nom_ppn_'.$i]);
        $data_pn_rinc['sub_total_akhir'] = clear_numeric($data['sub_total_akhir_'.$i]);
        $data_pn_rinc['harga_jual'] = clear_numeric($data['harga_jual_'.$i]);
        $data_pn_rinc['tgl_expired'] = to_date($data['tgl_expired_'.$i]);
        $data_pn_rinc['keteranganpenerimaan_rinc'] = $data['keteranganpenerimaan_rinc_'.$i];
        $data_pn_rinc['is_sesuai_order'] = 1;
        if (@$data_penerimaan['penerimaan_id'] != '') {
          $data_pn_rinc['penerimaan_id'] = $data_penerimaan['penerimaan_id'];
        }else{
          $data_pn_rinc['penerimaan_id'] = $data['penerimaan_id'];
        }
        //
        if($penerimaanrincian_id != '') {
          $data_pn_rinc['stok_id'] = $data['stok_id_'.$i];
          $data_pn_rinc['updated_at'] = date('Y-m-d H:i:s');
          $data_pn_rinc['updated_by'] = $this->session->userdata('sess_user_realname');
          $this->db->where('penerimaanrincian_id', $penerimaanrincian_id);
          $this->db->update('inv_penerimaan_rinc', $data_pn_rinc);
        } else {
          $data_pn_rinc['stok_id'] = $data_stok['stok_id'];
          $data_pn_rinc['penerimaanrincian_id'] = get_id('inv_penerimaan_rinc');
          $data_pn_rinc['created_at'] = date('Y-m-d H:i:s');
          $data_pn_rinc['created_by'] = $this->session->userdata('sess_user_realname');
          $this->db->insert('inv_penerimaan_rinc', $data_pn_rinc);
          update_id('inv_penerimaan_rinc', $data_pn_rinc['penerimaanrincian_id']);
        }   
      }
    } 
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('penerimaan_id',$id)->update('inv_penerimaan', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('penerimaan_id', $id)->delete('inv_penerimaan');
      $this->db->where('penerimaan_id', $id)->delete('inv_penerimaan_rinc');
    }else{
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('penerimaan_id', $id)->update('inv_penerimaan', $data);
      $this->db->where('penerimaan_id', $id)->update('inv_penerimaan_rinc', $data);
    }
  }

  public function barang_autocomplete($barang_nm=null) {
    $sql = "SELECT 
              a.*, b.parameter_val AS satuan
            FROM mst_barang a
            LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
            WHERE (a.barang_nm LIKE '%$barang_nm%' OR a.barang_id LIKE '%$barang_nm%')
            ORDER BY a.barang_id";                
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['barang_id']."#".$row['barang_nm'].'#'.$row['satuan'].'#'.$row['jenisbarang_cd'].'#'.$row['satuan_cd'],
        'text' => $row['barang_id'].' - '.$row['barang_nm']
      );
    }
    return $res;
  }

  public function barang_row($id)
  {
    $sql = "SELECT * FROM mst_barang WHERE barang_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  //surat pesanan
  public function surat_pesanan_row($id)
  {
    $sql = "SELECT * FROM inv_order WHERE order_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }
  
}