<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_stok_tidak_aktiv extends CI_Model {

	public function where($cookie)
	{
		$where = "WHERE a.is_deleted = 0 AND stok_st = 0 ";
    if (@$cookie['search']['tgl_expired_from'] != '' && @$cookie['search']['tgl_expired_to']) {
      $where .= "AND (DATE(a.tgl_expired) BETWEEN '".to_date(@$cookie['search']['tgl_expired_from'])."' AND '".to_date(@$cookie['search']['tgl_expired_to'])."')";
    }
    if (@$cookie['search']['term'] != '') {
      $where .= "AND (a.barang_id LIKE '%".$this->db->escape_like_str($cookie['search']['term'])."%' OR a.barang_nm LIKE '%".$this->db->escape_like_str($cookie['search']['term'])."%' ) ";
    }
    if (@$cookie['search']['jenisbarang_cd'] != '') {
      $where .= "AND a.jenisbarang_cd = '".$this->db->escape_like_str($cookie['search']['jenisbarang_cd'])."'";
    }
    if (@$cookie['search']['subjenisbarang_cd'] != '') {
      $where .= "AND a.subjenisbarang_cd = '".$this->db->escape_like_str($cookie['search']['subjenisbarang_cd'])."'";
    }
		return $where;
	}

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT a.*, b.parameter_val as satuan_nm
      FROM inv_stok a
      LEFT JOIN mst_parameter b ON a.satuan_cd = b.parameter_cd AND b.parameter_field = 'satuan_cd'
      $where
      ORDER BY "
        .$cookie['order']['field']." ".$cookie['order']['type'].
      " LIMIT ".$cookie['cur_page'].",".$cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM inv_stok a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM inv_stok a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id) {
    $sql = "SELECT a.*, b.parameter_val as satuan_nm  
            FROM inv_stok a
            LEFT JOIN mst_parameter b ON a.satuan_cd = b.parameter_cd AND b.parameter_field = 'satuan_cd'
            WHERE a.stok_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function rinc_data($id)
  {
    $sql = "SELECT a.*, b.parameter_val AS satuan 
    FROM inv_stok_rinc a
    LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
    WHERE stok_id=?";
    $query = $this->db->query($sql, array($id));
    $res = $query->result_array();
    return $res;
  }

  public function rincian_stok_data($order_id)
  {
    $sql = "SELECT a.*, b.parameter_val AS satuan  
    FROM inv_stok_rinc a
    LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
    WHERE a.order_id=?";
    $query = $this->db->query($sql, $order_id);
    $res = $query->result_array();
    return $res;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    if ($id == null) {
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('inv_stok', $data);
    }else{
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('stok_id', $id)->update('inv_stok', $data);
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('stok_id',$id)->update('inv_stok', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('stok_id', $id)->delete('inv_stok');
      $this->db->where('stok_id', $id)->delete('inv_stok_rinc');
    }else{
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('stok_id', $id)->update('inv_stok', $data);
      $this->db->where('stok_id', $id)->update('inv_stok_rinc', $data);
    }
  }

  public function barang_autocomplete($barang_nm=null) {
    $sql = "SELECT 
              a.*, b.parameter_val AS satuan
            FROM mst_barang a
            LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
            WHERE (a.barang_nm LIKE '%$barang_nm%' OR a.barang_id LIKE '%$barang_nm%')
            ORDER BY a.barang_id";                
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['barang_id']."#".$row['barang_nm'].'#'.$row['satuan'],
        'text' => $row['barang_id'].' - '.$row['barang_nm']
      );
    }
    return $res;
  }

  public function barang_row($id)
  {
    $sql = "SELECT * FROM mst_barang WHERE barang_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  //surat pesanan
  public function surat_pesanan_row($id)
  {
    $sql = "SELECT * FROM inv_stok WHERE order_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }
  
}