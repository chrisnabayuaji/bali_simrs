<?php if(count(@$main) == 0):?>
	<tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else: ?>
	<?php $i=1;foreach($main as $row):?>
	<tr>
    <td class="text-center" width="36"><?=$i++?></td>
    <td class="text-center" width="50">
      <div class="d-flex justify-content-center">
        <div class="form-check form-check-primary text-center" style="margin-left: -5px !important;">
          <label class="form-check-label">
            <input type="checkbox" class="form-check-input" name="checkitem[]" value="01.0001" data-itemlab-id="01_0001" data-tgl-hasil="" data-hasil-lab="" data-catatan-lab="">
          <i class="input-helper"></i></label>
        </div>
      </div>
    </td>
    <td class="text-center">
      <input type="text" class="form-control no-validation" value="<?=$row['barang_nm']?>" disabled>
    </td>
    <td class="text-center" width="80">
      <input type="text" class="form-control no-validation text-center" value="<?=get_parameter_value('satuan_cd', $row['satuan_cd'])?>" disabled>
    </td>
    <td class="text-center" width="60">
      <input type="text" class="form-control no-validation text-center" value="<?=$row['qty']?>" disabled>
    </td>
    <td class="text-center" width="60">
      <input type="text" class="form-control no-validation" name="hasil_lab[]" value="">
    </td>
    <td class="text-center">
      <input type="text" class="form-control datepicker datepicker-empty no-validation" name="hasil_lab[]" value="">
    </td>
    <td class="text-center">
      <input type="text" class="form-control no-validation" name="hasil_lab[]" value="">
    </td>
    <td class="text-center">
      <input type="text" class="form-control no-validation" name="hasil_lab[]" value="">
    </td>
    <td class="text-center">
      <input type="text" class="form-control no-validation" name="hasil_lab[]" value="">
    </td>
    <td class="text-center">
      <input type="text" class="form-control no-validation" name="hasil_lab[]" value="">
    </td>
    <td class="text-center">
      <input type="text" class="form-control no-validation" name="hasil_lab[]" value="">
    </td>
    <td class="text-center">
      <input type="text" class="form-control no-validation" name="hasil_lab[]" value="">
    </td>
    <td class="text-center">
      <input type="text" class="form-control no-validation" name="hasil_lab[]" value="">
    </td>
    <td class="text-center">
      <input type="text" class="form-control no-validation" name="hasil_lab[]" value="">
    </td>
  </tr>
	<?php endforeach; ?>
<?php endif; ?>

<script type="text/javascript">
$(document).ready(function () {
  $(".datepicker-empty").val("");
})
$(".datepicker").daterangepicker({
  singleDatePicker: true,
  showDropdowns: true,
  locale: {
    cancelLabel: "Clear",
    format: "DD-MM-YYYY"
  },
  isInvalidDate: function(date) {
    return "";
  }
})
</script>