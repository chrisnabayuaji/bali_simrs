<div class="table-responsive">
  <table id="rincian_pesanan_table" class="table table-hover table-bordered table-striped table-sm w-100">
    <thead>
      <tr>
        <th class="text-center" width="36">No</th>
        <th class="text-center">Nama Item</th>
        <th class="text-center" width="100">Satuan</th>
        <th class="text-center" width="60">QTY</th>
        <th class="text-center" width="55">Aksi</th>
      </tr>
    </thead>
  </table>
</div>

<script type="text/javascript">
  var rincianPesananTable;
  $(document).ready(function () {
    //datatables
    rincianPesananTable = $('#rincian_pesanan_table').DataTable({
      "autoWidth" : false,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
      "ajax": {
          "url": "<?php echo site_url().'/'.$nav['nav_url'].'/ajax_surat_pesanan/rincian_pesanan_data'?>",
          "type": "POST",
          "dataType" : "json",
          "data" : {
            'order_id' : '<?=$order_id?>'
          }
      },      
      "columnDefs": [
        {"targets": 0, "className": 'text-center', "orderable" : false},
        {"targets": 1, "className": 'text-left', "orderable" : false},
        {"targets": 2, "className": 'text-center', "orderable" : false},
        {"targets": 3, "className": 'text-center', "orderable" : false},
        {"targets": 4, "className": 'text-center', "orderable" : false},
      ],
    });
    rincianPesananTable.columns.adjust().draw();
  })
</script>