<div class="table-responsive">
  <table id="barang_table" class="table table-hover table-bordered table-striped table-sm w-100">
    <thead>
      <tr>
        <th class="text-center" width="20">No.</th>
        <th class="text-center" width="120">Kode</th>
        <th class="text-center">Nama Barang</th>
        <th class="text-center">Satuan</th>
        <th class="text-center">Jenis Barang</th>
        <th class="text-center" width="60">Aksi</th>
      </tr>
    </thead>
  </table>
</div>

<script type="text/javascript">
  var barangTable;
  $(document).ready(function () {
    //datatables
    barangTable = $('#barang_table').DataTable({
      "autoWidth" : false,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
      "ajax": {
          "url": "<?php echo site_url().'/'.$nav['nav_url'].'/ajax/search_data'?>",
          "type": "POST"
      },      
      "columnDefs": [
        {"targets": 0, "className": 'text-center', "orderable" : false},
        {"targets": 1, "className": 'text-center', "orderable" : false},
        {"targets": 2, "className": 'text-left', "orderable" : false},
        {"targets": 3, "className": 'text-center', "orderable" : false},
        {"targets": 4, "className": 'text-center', "orderable" : false},
        {"targets": 5, "className": 'text-center', "orderable" : false},
      ],
    });
    barangTable.columns.adjust().draw();
  })
</script>