<script type="text/javascript">
  var item_list = [];
  var item;
  <?php if($id != null):?>
    <?php foreach($rinc as $row): ?>
      item = {
        orderrincian_id : '<?=$row['orderrincian_id']?>',
        barang_id : '<?=$row['barang_id']?>',
        barang_nm : '<?=$row['barang_nm']?>',
        satuan : '<?=$row['satuan']?>',
        qty : '<?=$row['qty']?>',
        keteranganorder_rinc : '<?=$row['keteranganorder_rinc']?>',
        status : 'idle',
      };
      item_list.push(item);
    <?php endforeach;?>
  <?php endif;?>
  $(document).ready(function () {
    fetch_list();
    $("#form-data").validate( {
      rules: {
        supplier_id:{
          valueNotEquals: ''
        },
      },
      messages: {
        supplier_id : {
          valueNotEquals : 'Pilih salah satu!'
        }
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container"));
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    // select2
    $(".chosen-select").select2();
    // autocomplete
    $('#barang_id').select2({
      minimumInputLength: 2,
      ajax: {
        url: "<?=site_url($nav['nav_url'])?>/ajax/autocomplete",
        dataType: "json",
        
        data: function(params) {
          return{
            barang_nm : params.term
          };
        },
        processResults: function(data){
          return{
            results: data
          }
        }
      }
    });
    $('.select2-container').css('width', '100%');
  })

  function barang_fill(id) {
    $.ajax({
      type : 'post',
      url : '<?=site_url($nav['nav_url'].'/ajax/barang_fill')?>',
      dataType : 'json',
      data : 'barang_id='+id,
      success : function (data) {
        // autocomplete
        var data2 = {
          id: data.barang_id+'#'+data.barang_nm+'#'+data.satuan,
          text: data.barang_id+' - '+data.barang_nm
        };
        var newOption = new Option(data2.text, data2.id, false, false);
        $('#barang_id').append(newOption).trigger('change');
        $('#barang_id').val(data.barang_id+'#'+data.barang_nm+'#'+data.satuan);
        $('#barang_id').trigger('change');
        $('.select2-container').css('width', '100%');
        $("#myModal").modal('hide');
      }
    })
  }

  function save_item() {
    var barang = $("#barang_id").val().split('#');
    var orderrincian_id = $("#orderrincian_id").val();
    var index = $("#index").val();
    var status = $("#status").val();
    var data = {
      orderrincian_id : orderrincian_id,
      barang_id : barang[0],
      barang_nm : barang[1],
      satuan : barang[2],
      qty : $("#qty").val(),
      keteranganorder_rinc : $("#keteranganorder_rinc").val(),
      status : status
    };
    if(data.barang_id == ''){
      alert('Pilih item terlebih dulu!');
    }else{
      if(orderrincian_id == ''){
        if (status == 'edit' || status == 'edit_array') {
          item_list[index] = data;
        }else if(status == 'new'){
          item_list.push(data);
        }
      }else{
        item_list[index] = data;
      }
      // console.log(barang);
      fetch_list();
      reset_item();
    }
    $("#orderrincian_id").val('').removeClass('is-valid').removeClass('is-invalid');
    $("#qty").val('1').removeClass('is-valid').removeClass('is-invalid');
    $("#keteranganorder_rinc").val('').removeClass('is-valid').removeClass('is-invalid');
  }

  function reset_item() {
    $("#orderrincian_id").val('').removeClass('is-valid').removeClass('is-invalid');
    $("#index").val('').removeClass('is-valid').removeClass('is-invalid');
    $("#qty").val('1').removeClass('is-valid').removeClass('is-invalid');
    $("#status").val('new').removeClass('is-valid').removeClass('is-invalid');
    $("#keteranganorder_rinc").val('').removeClass('is-valid').removeClass('is-invalid');
    var data = {
      id: '',
      text: '- Pilih -'
    };
    var newOption = new Option(data.text, data.id, false, false);
    $('#barang_id').append(newOption).trigger('change');
    $('#barang_id').val(data.id);
    $('#barang_id').trigger('change');
  }

  function fetch_list() {
    $("#item_list").html('');
    $("#delete_list").html('');
    var no = 1;
    if (item_list.length == 0) {
      var html = '<tr>'+
                    '<td class="text-center" colspan="99"><i>Tidak ada data!</i></td>'+
                  '</tr>';
      $("#item_list").append(html);            
    }else{
      $.each(item_list, function(index, value) {
        if (value.status != 'delete') {
          if (value.orderrincian_id !='') {
            var type_edit = 0;
          }else{
            var type_edit = 1;
          }

          if (value.satuan == 'null' || value.satuan == null) {
            var satuan = '';
          }else{
            var satuan = value.satuan;
          }

          var html = '<tr>'+
            '<td class="text-center">'+(no++)+
              '<input type="hidden" name="barang_id[]" value="'+(value.barang_id)+'" />'+
              '<input type="hidden" name="orderrincian_id[]" value="'+(value.orderrincian_id)+'" />'+
            '</td>'+
            '<td class="text-center">'+
              '<button class="btn btn-primary btn-table" type="button" onclick="item_edit('+index+', '+type_edit+')"><i class="fas fa-pencil-alt"></i></button> '+
              '<button class="btn btn-danger btn-table" type="button" onclick="item_delete('+index+')"><i class="fas fa-trash-alt"></i></button>'+
            '</td>'+
            '<td class="text-left">'+(value.barang_nm)+'</td>'+
            '<td class="text-center">'+(satuan)+'</td>'+
            '<td class="text-center">'+(value.qty)+
              '<input type="hidden" name="qty[]" value="'+(value.qty)+'" />'+
            '</td>'+
            '<td class="text-left">'+(value.keteranganorder_rinc)+
              '<input type="hidden" name="keteranganorder_rinc[]" value="'+(value.keteranganorder_rinc)+'" />'+
              '<input type="hidden" name="status[]" value="'+value.status+'" />'+
            '</td>'+
          '</tr>';
          $("#item_list").append(html);
        }else{
          var html = '<input type="hidden" name="delete_id[]" value="'+(value.orderrincian_id)+'" />';
          $("#delete_list").append(html);
        }
      });
    }
  }

  function item_delete(index) {
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Menghapus item ini",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#eb3b5a',
      cancelButtonColor: '#b2bec3',
      confirmButtonText: 'Hapus',
      cancelButtonText: 'Batal',
      customClass: 'swal-wide'
    }).then((result) => {
      if (result.value) {
        item_list[index].status = 'delete';
        fetch_list();
      }
    })
  }

  function item_edit(index, type_edit='') {
    var row = item_list[index];
    var data = {
      id: row.barang_id+'#'+row.barang_nm+'#'+row.satuan,
      text: row.barang_nm
    };
    var newOption = new Option(data.text, data.id, false, false);
    $('#barang_id').append(newOption).trigger('change');
    $('#barang_id').val(data.id);
    $('#barang_id').trigger('change');

    $("#orderrincian_id").val(row.orderrincian_id).removeClass('is-valid').removeClass('is-invalid');
    $("#index").val(index).removeClass('is-valid').removeClass('is-invalid');
    $("#qty").val(row.qty).removeClass('is-valid').removeClass('is-invalid');
    if (type_edit == 1) {
      $("#status").val('edit_array').removeClass('is-valid').removeClass('is-invalid');
    }else{
      $("#status").val('edit').removeClass('is-valid').removeClass('is-invalid');
    }
    $("#keteranganorder_rinc").val(row.keteranganorder_rinc).removeClass('is-valid').removeClass('is-invalid');
  }
</script>