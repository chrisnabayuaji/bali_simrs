<?php $this->load->view('_js'); ?>
<div class="flash-success" data-flashsuccess="<?=$this->session->flashdata('flash_success')?>"></div>
<form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?=$form_action?>" class="needs-validation" novalidate autocomplete="off">
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline col-title">
          <div class="col-back">
            <a href="<?=site_url($nav['nav_module'].'/dashboard')?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
          </div>
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            <?=$nav['nav_nm']?>
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?=site_url('app/dashboard')?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?=site_url($module_nav['nav_url'])?>"><i class="fas fa-folder-open"></i> <?=$module_nav['nav_nm']?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-warehouse"></i> Gudang</a></li>
            <li class="breadcrumb-item"><a href="<?=site_url($nav['nav_url'])?>"><i class="<?=$nav['nav_icon']?>"></i> <?=$nav['nav_nm']?></a></li>
            <li class="breadcrumb-item active"><span>Form</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>

    <div class="row full-page mt-4">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <h4 class="card-title border-bottom border-2 pb-2 mb-3"><?=$nav['nav_nm']?></h4>
            <div class="row">
              <div class="col-5">  
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">No. Surat Pesanan<span class="text-danger">*<span></label>
                  <div class="col-lg-8 col-md-9">
                    <input type="text" class="form-control" name="no_order" id="no_order" value="<?=@$main['no_order']?>" required aria-invalid="false">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Tgl. Surat Pesanan<span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-9">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datepicker" name="tgl_order" id="tgl_order" value="<?php if(@$main){echo to_date(@$main['tgl_order'],'','date','');}else{echo date('d-m-Y H:i:s');}?>" required aria-invalid="false">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Supplier <span class="text-danger">*<span></label>
                  <div class="col-lg-8 col-md-9">
                    <select class="form-control form-control-sm chosen-select" name="supplier_id" id="supplier_id">
                      <option value="">- Pilih -</option>
                      <?php foreach($supplier as $r): ?>
                        <option value="<?=$r['supplier_id']?>" <?php if(@$main['supplier_id'] == $r['supplier_id']){echo 'selected';}?>><?=$r['supplier_nm']?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col">  
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Keterangan</label>
                  <div class="col-lg-8 col-md-9">
                    <textarea class="form-control form-control-sm" name="keteranganorder" id="keteranganorder" rows="4"><?=@$main['keteranganorder']?></textarea>
                  </div>
                </div>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-5">
                <div class="card border-none no-shadow">
                  <div class="card-body">
                    <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-file"></i> Form Data</h4>
                    <div class="form-group row mb-2">
                      <input type="hidden" id="orderrincian_id" value="">
                      <input type="hidden" id="index" value="">
                      <input type="hidden" id="status" value="new">
                      <label class="col-lg-3 col-md-3 col-form-label">Barang <span class="text-danger">*</span></label>
                      <div class="col-lg-8 col-md-3">
                        <select class="form-control" id="barang_id">
                          <option value="">- Pilih -</option>
                        </select>
                      </div>
                      <div class="col-lg-1 col-md-1">
                        <a href="javascript:void(0)" data-href="<?=site_url().'/'.$nav['nav_url'].'/ajax/search_barang'?>" modal-title="List Data Barang" modal-size="lg" class="btn btn-xs btn-default modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Barang" data-original-title="Cari Barang"><i class="fas fa-search"></i></a>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-lg-3 col-md-3 col-form-label">Qty <span class="text-danger">*<span></label>
                      <div class="col-lg-3 col-md-2">
                        <input type="number" class="form-control" id="qty" value="1" min="1">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-lg-3 col-md-3 col-form-label">Keterangan <span class="text-danger">*<spa></label>
                      <div class="col-lg-9 col-md-9">
                        <textarea class="form-control form-control-sm" id="keteranganorder_rinc" rows="2"></textarea>
                      </div>
                    </div>
                    <div class="form-group row mt-2">
                      <label class="col-lg-3 col-md-3 col-form-label"></label>
                      <div class="col-lg-9 col-md-9">
                        <span type="button" id="item_action" class="btn btn-xs btn-primary" onclick="save_item()"><i class="fas fa-save"></i> Simpan</span>
                        <span type="button" id="item_cancel" class="btn btn-xs btn-secondary" onclick="reset_item()"><i class="fas fa-times"></i> Batal</span>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col">
                <div class="card border-none no-shadow">
                  <div class="card-body">
                    <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-list"></i> Item Barang</h4>
                    <div class="table-responsive">
                      <table class="table table-striped table-bordered table-sm">
                        <thead>
                          <tr>
                            <th class="text-center" width="50">No.</th>
                            <th class="text-center" width="60">Aksi</th>
                            <th class="text-center">Nama Item Barang</th>
                            <th class="text-center" width="50">Satuan</th>
                            <th class="text-center" width="50">Qty</th>
                            <th class="text-center" width="200">Keterangan</th>
                          </tr>
                        </thead>
                        <tbody id="item_list"></tbody>
                      </table>
                    </div>
                    <div id="delete_list"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>  
  <div class="btn-form">
    <div class="btn-form-action">
      <div class="btn-form-action-bottom w-100 small-text clearfix">
        <div class="col-lg-2">
          <a href="<?=site_url().'/'.$nav['nav_url']?>" class="btn btn-xs btn-secondary"><i class="mdi mdi-keyboard-backspace"></i> Kembali</a>
        </div>
        <div class="col-lg-2 offset-8">
          <button type="submit" class="float-right btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
          <button type="reset" onClick="window.location.reload();" class="float-right btn btn-xs btn-secondary btn-clear mr-2"><i class="fas fa-sync-alt"></i> Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>