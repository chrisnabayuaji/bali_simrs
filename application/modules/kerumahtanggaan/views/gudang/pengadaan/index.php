<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline col-title">
        <div class="col-back">
          <a href="<?=site_url($nav['nav_module'].'/dashboard')?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
        </div>
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          <?=$nav['nav_nm']?>
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?=site_url('app/dashboard')?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url($module_nav['nav_url'])?>"><i class="fas fa-folder-open"></i> <?=$module_nav['nav_nm']?></a></li>
          <li class="breadcrumb-item"><a href="#"><i class="fas fa-warehouse"></i> Gudang</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url($nav['nav_url'])?>"><?=$nav['nav_nm']?></a></li>
          <li class="breadcrumb-item active"><span>Index</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>
  <!-- flash message -->
  <div class="flash-success" data-flashsuccess="<?=$this->session->flashdata('flash_success')?>"></div>
  <!-- /flash message -->
  <div class="row full-page mt-4 mb-n2">
    <div class="col-lg-12 grid-margin-xl-0 stretch-card">
      <div class="card border-none">
        <div class="card-body">
          <form id="form-search" action="<?=site_url().'/app/search/'.$nav['nav_id']?>" method="post" autocomplete="off">
            <div class="row">
              <div class="col-2">
                <?php if ($nav['_add']):?>
                  <a href="<?=site_url().'/'.$nav['nav_url'].'/form'?>" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Tambah Data"><i class="fas fa-plus-circle"></i> Tambah</a>
                <?php endif;?>
              </div>
              <div class="col-3 offset-7">
                <!-- <div class="input-group">
                  <input type="text" name="term" value="<?=@$cookie['search']['term']?>" class="form-control form-control-sm" placeholder="Pencarian...">
                  <div class="input-group-append">
                    <button type="submit" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cari Data"><i class="fas fa-search"></i></button>
                    <a class="btn btn-xs btn-default" href="<?=site_url().'/app/reset/'.$nav['nav_id']?>" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-default" title="" data-original-title="Reset"><i class="fas fa-sync-alt"></i></a>
                  </div>
                </div> -->
              </div>
            </div><!-- /.row -->
          </form>
          <div class="row mt-2">
            <div class="col-md-12">
              <div class="table-responsive">
                <form id="form-multiple" action="<?=site_url().'/'.$nav['nav_url'].'/multiple/enable'?>" method="post">
                  <table class="table table-hover table-striped table-bordered table-fixed">
                    <thead>
                      <tr>
                        <th class="text-center" width="36">No</th>
                        <th class="text-center" width="36">
                          <div class="form-check form-check-primary form-check-th">
                            <label class="form-check-label">
                              <input type="checkbox" class="form-check-input cb-all">
                            </label>
                          </div>
                        </th>
                        <th class="text-center" width="60">Aksi</th>
                        <th class="text-center" width="250"><?=table_sort($nav['nav_id'], 'No. Surat Pesanan', 'no_order', $cookie['order'])?></th>
                        <th class="text-center" width="150"><?=table_sort($nav['nav_id'], 'Tgl. Surat Pesanan', 'tgl_order', $cookie['order'])?></th>
                        <th class="text-center">Supplier</th>
                        <th class="text-center">Keterangan</th>
                        <th class="text-center" width="55">Status</th>
                      </tr>
                    </thead>
                    <?php if(@$main == null):?>
                      <tbody>
                        <tr>
                          <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                        </tr>
                      </tbody>
                    <?php else:?>
                      <tbody>
                        <?php $i=1;foreach($main as $row):?>
                          <tr>
                            <td class="text-center" width="36"><?=$cookie['cur_page']+($i++)?></td>
                            <td class="text-center" width="36">
                              <div class="form-check form-check-primary form-check-td">
                                <label class="form-check-label">
                                  <input type="checkbox" class="form-check-input cb-item" name="checkitem[]" value="<?=$row['order_id']?>">
                                </label>
                              </div>
                            </td>
                            <td class="text-center" width="60">
                              <div class="btn-group">
                                <button type="button" class="btn btn-primary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Aksi
                                </button>
                                <div class="dropdown-menu">
                                  <a class="dropdown-item modal-href" href="javascript:void(0)" data-href="<?=site_url().'/'.$nav['nav_url'].'/detail/'.$row['order_id']?>" modal-title="Detail Data" modal-size="lg"><i class="fas fa-list"></i> Detail</a>
                                  <?php if($nav['_update'] || $nav['_delete']):?>
                                    <?php if($row['order_st'] == 0): ?>
                                      <?php if($nav['_update']):?>
                                        <a class="dropdown-item" href="<?=site_url().'/'.$nav['nav_url'].'/form/'.$row['order_id']?>"><i class="fas fa-pencil-alt"></i> Ubah</a>
                                      <?php endif; ?>
                                      <?php if($nav['_delete']):?>
                                        <a href="#" data-href="<?=site_url().'/'.$nav['nav_url'].'/delete/'.$row['order_id']?>" class="dropdown-item btn-delete"><i class="far fa-trash-alt"></i> Hapus</a>
                                      <?php endif; ?>
                                    <?php endif; ?>
                                  <?php endif; ?>
                                  <!-- <a class="dropdown-item" href="<?=site_url().'/'.$nav['nav_url'].'/cetak/'.$row['order_id']?>"><i class="fas fa-print"></i> Cetak Surat Pesanan</a> -->
                                </div>
                              </div>
                            </td>
                            <td class="text-center" width="250"><?=$row['no_order']?></td>
                            <td class="text-center" width="150"><?=to_date($row['tgl_order'], '', 'date', '')?></td>
                            <td class="text-left"><?=$row['supplier_nm']?></td>
                            <td class="text-left"><?=$row['keteranganorder']?></td>
                            <td class="text-center" width="55"><li class="fa <?=($row['order_st'] == '1' ? 'fa-check-circle text-success' : 'fa-minus-circle text-danger blink')?>"></li></td>
                          </tr>
                        <?php endforeach;?>
                      </tbody>
                    <?php endif;?>
                  </table>
                </form>
              </div>
            </div>
          </div><!-- /.row -->
          <div class="row mt-2">
            <div class="col-6 text-middle pt-1">
              <div class="row">
                <div class="col">
                  <div class="row mt-2 pagination-info">
                    <div class="col-2">
                      <?php if($nav['_update'] == 1 || $nav['_delete'] == 1):?>
                        <div class="input-group-prepend">
                          <button type="button" class="btn btn-block btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            Aksi Multi
                          </button>
                          <div class="dropdown-menu">
                            <?php if($nav['_update'] == 1):?>
                              <a class="dropdown-item" href="javascript:multipleAction('enable')">Aktif</a>
                              <a class="dropdown-item" href="javascript:multipleAction('disable')">Non Aktif</a>
                            <?php endif; ?>
                            <?php if($nav['_delete'] == 1):?>
                              <a class="dropdown-item" href="javascript:multipleAction('delete')">Hapus</a>
                            <?php endif; ?>
                          </div>
                        </div>
                      <?php endif; ?>
                    </div>
                    <div class="col-md-8 p-0 pt-1">
                      <form id="form-paging" action="<?=site_url().'/app/per_page/'.$nav['nav_id']?>" method="post">
                        <label><i class="fas fa-bookmark"></i> Perhalaman</label>
                        <select name="per_page" class="select-pagination" onchange="$('#form-paging').submit()">
                          <option value="10" <?php if($cookie['per_page'] == 10){echo 'selected';}?>>10</option>
                          <option value="50" <?php if($cookie['per_page'] == 50){echo 'selected';}?>>50</option>
                          <option value="100" <?php if($cookie['per_page'] == 100){echo 'selected';}?>>100</option>
                        </select>
                        <label class="ml-2"><?=@$pagination_info?></label>
                      </form>
                    </div>
                  </div><!-- /.row -->
                </div>
              </div>
            </div>
            <div class="col-6 text-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>