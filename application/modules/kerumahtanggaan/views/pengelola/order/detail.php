<form id="laboratorium_form" action="" method="post" autocomplete="off">
  <div class="row">
    <div class="col-5">  
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">No. Surat Pesanan</label>
        <div class="col-lg-8 col-md-9">
          <input type="text" class="form-control" value="<?=@$main['no_order']?>" disabled aria-invalid="false">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Tgl. Surat Pesanan</label>
        <div class="col-lg-4 col-md-9">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
            </div>
            <input type="text" class="form-control datepicker" value="<?php if(@$main){echo to_date(@$main['tgl_order'],'','date','');}else{echo date('d-m-Y H:i:s');}?>" disabled aria-invalid="false">
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Supplier</label>
        <div class="col-lg-6 col-md-9">
          <input type="text" class="form-control"value="<?=@$main['supplier_nm']?>" disabled aria-invalid="false">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Status</label>
        <div class="col-lg-6 col-md-9">
          <input type="text" class="form-control"value="<?=(@$main['order_st'] == 1) ? 'Sudah diproses Penerimaan' : 'Belum diproses Penerimaan'?>" disabled aria-invalid="false">
        </div>
      </div>
    </div>
    <div class="col">  
      <div class="form-group row">
        <label class="col-lg-2 col-md-3 col-form-label">Keterangan</label>
        <div class="col-lg-8 col-md-9">
          <textarea class="form-control form-control-sm" rows="4" disabled><?=@$main['keteranganorder']?></textarea>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="table-responsive">
    <table class="table table-striped table-bordered table-sm">
      <thead>
        <tr>
          <th class="text-center" width="50">No.</th>
          <th class="text-center">Nama Item Barang</th>
          <th class="text-center" width="100">Satuan</th>
          <th class="text-center" width="100">Qty</th>
          <th class="text-center" width="200">Keterangan</th>
        </tr>
      </thead>
      <tbody>
        <?php $i=1;foreach($rinc as $row): ?>
        <tr>
          <td class="text-center"><?=($i++)?></td>
          <td class="text-left"><?=$row['obat_nm']?></td>
          <td class="text-center"><?=get_parameter_value('satuan_cd', $row['satuan_cd'])?></td>
          <td class="text-center"><?=$row['qty']?></td>
          <td class="text-left"><?=$row['keteranganorder_rinc']?></td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div> 
  <hr>
  <div class="col-2 offset-md-10 mt-n2">
    <div class="float-right">
      <button type="button" class="btn btn-xs btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Tutup</button>
    </div>
  </div>
</form>