<div class="table-responsive">
  <table id="stok_table_eksis" class="table table-hover table-bordered table-striped table-sm w-100">
    <thead>
      <tr>
        <th class="text-center" width="20">No.</th>
        <th class="text-center" width="100">Kode</th>
        <th class="text-center">Nama Barang</th>
        <th class="text-center">Jenis Barang</th>
        <th class="text-center">Sub Jenis Barang</th>
        <th class="text-center">Satuan</th>
        <th class="text-center">Tgl.Expired</th>
        <th class="text-center">Stok Akhir</th>
        <th class="text-center" width="60">Aksi</th>
      </tr>
    </thead>
  </table>
</div>

<script type="text/javascript">
  var barangTableEksis;
  $(document).ready(function () {
    //datatables
    barangTableEksis = $('#stok_table_eksis').DataTable({
      "autoWidth" : false,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
      "ajax": {
          "url": "<?php echo site_url().'/'.$nav['nav_url'].'/ajax/search_data'?>",
          "type": "POST"
      },      
      "columnDefs": [
        {"targets": 0, "className": 'text-center', "orderable" : false},
        {"targets": 1, "className": 'text-center', "orderable" : false},
        {"targets": 2, "className": 'text-left', "orderable" : false},
        {"targets": 3, "className": 'text-center', "orderable" : false},
        {"targets": 4, "className": 'text-center', "orderable" : false},
        {"targets": 5, "className": 'text-center', "orderable" : false},
        {"targets": 6, "className": 'text-center', "orderable" : false},
        {"targets": 7, "className": 'text-center', "orderable" : false},
        {"targets": 8, "className": 'text-center', "orderable" : false},
      ],
    });
    barangTableEksis.columns.adjust().draw();
  })
</script>