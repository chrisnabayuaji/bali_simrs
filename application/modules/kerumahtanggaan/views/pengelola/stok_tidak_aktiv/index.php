<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline col-title">
        <div class="col-back">
          <a href="<?=site_url($nav['nav_module'].'/dashboard')?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
        </div>
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          <?=$nav['nav_nm']?>
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?=site_url('app/dashboard')?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url($module_nav['nav_url'])?>"><i class="fas fa-folder-open"></i> <?=$module_nav['nav_nm']?></a></li>
          <li class="breadcrumb-item"><a href="#"><i class="fas fa-box-open"></i> Pengelola</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url($nav['nav_url'])?>"><?=$nav['nav_nm']?></a></li>
          <li class="breadcrumb-item active"><span>Index</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>
  <!-- flash message -->
  <div class="flash-success" data-flashsuccess="<?=$this->session->flashdata('flash_success')?>"></div>
  <!-- /flash message -->
  <div class="row full-page mt-4 mb-n2">
    <div class="col-lg-12 grid-margin-xl-0 stretch-card">
      <div class="card border-none">
        <div class="card-body">
          <div class="row mt-n3 mb-1">
            <div class="col-md-12 mt-3">
              <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link tab-menu" href="<?=site_url('kerumahtanggaan/pengelola/stok_aktiv')?>"><i class="fas fa-check-circle"></i> STOK AKTIF</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link tab-menu active" href="<?=site_url('kerumahtanggaan/pengelola/stok_tidak_aktiv')?>"><i class="fas fa-times-circle"></i> STOK TIDAK AKTIF</a>
                </li>
              </ul>
            </div>
          </div>
          <form id="form-search" action="<?=site_url().'/app/search_redirect/'.$nav['nav_id'].'.tidakaktif'?>" method="post" autocomplete="off">
            <input type="hidden" name="redirect" value="kerumahtanggaan/pengelola/stok_tidak_aktiv">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Kode/Nama Item</label>
                  <div class="col-lg-8 col-md-8">
                    <input type="text" class="form-control" name="term" id="term" value="<?=@$cookie['search']['term']?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Tgl. Expired</label>
                  <div class="col-lg-8 col-md-8">
                    <div class="row">
                      <div class="col-md-5" style="padding-right: 5px; flex: 0 0 46.4%; max-width: 46.4%;">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datepicker text-center datepicker-empty" name="tgl_expired_from" id="tgl_expired_from" value="<?=@$cookie['search']['tgl_expired_from']?>">
                        </div>
                      </div>
                      <div class="col-md-2 text-center-group">s.d</div>
                      <div class="col-md-5" style="padding-left: 5px; flex: 0 0 46.4%; max-width: 46.4%;">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datepicker text-center datepicker-empty" name="tgl_expired_to" id="tgl_expired_to" value="<?=@$cookie['search']['tgl_expired_to']?>">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Jenis Barang</label>
                  <div class="col-lg-6 col-md-8">
                    <select class="form-control chosen-select" name="jenisbarang_cd" id="jenisbarang_cd">
                      <option value="">- Semua -</option>
                      <?php foreach(get_parameter('jenisbarang_cd') as $r): ?>
                        <option value="<?=$r['parameter_cd']?>" <?=(@$cookie['search']['jenisbarang_cd'] == $r['parameter_cd']) ? 'selected' : '';?>><?=$r['parameter_val']?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 row">
                <div class="col-md-6 offset-md-4 pl-1">
                  <button type="submit" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cari Data"><i class="fas fa-search"></i> Cari</button>
                  <a class="btn btn-xs btn-default" href="<?=site_url().'/app/reset_redirect/'.$nav['nav_id'].'.tidakaktif/'.str_replace('/','-','kerumahtanggaan/pengelola/stok_tidak_aktiv')?>" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-default" title="" data-original-title="Reset"><i class="fas fa-sync-alt"></i> Reset</a>
                </div>
              </div>
            </div>
          </form>
          <div class="row mt-2">
            <div class="col-md-12">
              <div class="table-responsive">
                <form id="form-multiple" action="<?=site_url().'/'.$nav['nav_url'].'/multiple/enable'?>" method="post">
                  <table class="table table-hover table-striped table-bordered table-fixed" style="min-width:1300px !important">
                    <thead>
                      <tr>
                        <th class="text-center text-middle" rowspan="2" width="40">No</th>
                        <th class="text-center text-middle" rowspan="2" width="60">Aksi</th>
                        <th class="text-center text-middle" rowspan="2" width="60">Kode</th>
                        <th class="text-center text-middle" rowspan="2" width="200">Nama Item Barang</th>
                        <th class="text-center text-middle" rowspan="2" width="120">Jenis Barang</th>
                        <th class="text-center text-middle" rowspan="2" width="60">Satuan</th>
                        <th class="text-center text-middle" rowspan="2" width="80">Tgl. Expired</th>
                        <th class="text-center text-middle" rowspan="2" width="90">Harga Beli</th>
                        <th class="text-center text-middle" rowspan="2" width="90">Harga Jual</th>
                        <th class="text-center text-middle" colspan="5">Posisi Stok</th>
                        <th class="text-center text-middle" rowspan="2" width="50">Status</th>
                      </tr>
                      <tr>
                        <th class="text-center text-middle">S.Awal</th>
                        <th class="text-center text-middle">S.Penerimaan</th>
                        <th class="text-center text-middle">S.Retur K</th>
                        <th class="text-center text-middle">S.Akhir</th>
                        <th class="text-center text-middle">S.Opname</th>
                      </tr>
                    </thead>
                    <?php if(@$main == null):?>
                      <tbody>
                        <tr>
                          <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                        </tr>
                      </tbody>
                    <?php else:?>
                      <tbody>
                        <?php $i=1;foreach($main as $row):?>
                          <tr>
                            <td class="text-center text-middle" width="40"><?=$i++?></td>
                            <td class="text-center text-middle" width="60">
                              <div class="btn-group">
                                <button type="button" class="btn btn-primary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Aksi
                                </button>
                                <div class="dropdown-menu">
                                  <a class="dropdown-item modal-href" href="javascript:void(0)" data-href="<?=site_url().'/kerumahtanggaan/pengelola/stok_tidak_aktiv/detail/'.$row['stok_id']?>" modal-title="" modal-header="hidden" modal-size="lg"><i class="fas fa-list"></i> Detail</a>
                                  <?php if($nav['_update']):?>
                                    <a class="dropdown-item modal-href" href="javascript:void(0)" data-href="<?=site_url().'/kerumahtanggaan/pengelola/stok_tidak_aktiv/form_modal/'.$row['stok_id']?>" modal-title="Ubah Data" modal-size="md"><i class="fas fa-pencil-alt"></i> Ubah</a>
                                  <?php endif; ?>
                                </div>
                              </div>
                            </td>
                            <td class="text-center text-middle" width="60"><?=$row['barang_id']?></td>
                            <td class="text-left text-middle" width="200"><?=$row['barang_nm']?></td>
                            <td class="text-center text-middle" width="120"><?=get_parameter_value('jenisbarang_cd', $row['jenisbarang_cd'])?></td>
                            <td class="text-center text-middle" width="60"><?=$row['satuan_nm']?></td>
                            <td class="text-center text-middle" width="80"><?=to_date($row['tgl_expired'], '-', 'date', '')?></td>
                            <td class="text-right text-middle" width="90"><?=num_id($row['harga_beli'])?></td>
                            <td class="text-right text-middle" width="90"><?=num_id($row['harga_jual'])?></td>
                            <td class="text-right text-middle"><?=num_id($row['stok_awal'])?></td>
                            <td class="text-right text-middle"><?=num_id($row['stok_penerimaan'])?></td>
                            <td class="text-right text-middle"><?=num_id($row['stok_retur_keluar'])?></td>
                            <td class="text-right text-middle"><?=num_id($row['stok_akhir'])?></td>
                            <td class="text-right text-middle"><?=num_id($row['stok_opname'])?></td>
                            <td class="text-center text-middle" width="50"><li class="fa <?=($row['stok_st'] == '1' ? 'fa-check-circle text-success' : 'fa-minus-circle text-danger blink')?>"></li></td>
                          </tr>
                        <?php endforeach;?>
                      </tbody>
                    <?php endif;?>
                  </table>
                </form>
              </div>
            </div>
          </div><!-- /.row -->
          <div class="row mt-2">
            <div class="col-6 text-middle pt-1">
              <div class="row">
                <div class="col">
                  <div class="row mt-2 pagination-info">
                    <div class="col-md-8 pt-1">
                      <form id="form-paging" action="<?=site_url().'/app/per_page/'.$nav['nav_id']?>" method="post">
                        <label><i class="fas fa-bookmark"></i> Perhalaman</label>
                        <select name="per_page" class="select-pagination" onchange="$('#form-paging').submit()">
                          <option value="10" <?php if($cookie['per_page'] == 10){echo 'selected';}?>>10</option>
                          <option value="50" <?php if($cookie['per_page'] == 50){echo 'selected';}?>>50</option>
                          <option value="100" <?php if($cookie['per_page'] == 100){echo 'selected';}?>>100</option>
                        </select>
                        <label class="ml-2"><?=@$pagination_info?></label>
                      </form>
                    </div>
                  </div><!-- /.row -->
                </div>
              </div>
            </div>
            <div class="col-6 text-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function () {
    $(".datepicker-empty").val("");
  });
</script>