<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller{

	var $nav_id = '07', $nav;
	
	public function __construct() {
		parent::__construct();
		$this->nav = $this->m_app->_get_nav($this->nav_id);
	}

	public function index() {
		$this->authorize($this->nav, '_view');
		$data['nav'] = $this->nav;

		$this->render('keuangan/template/dashboard', $data);
	}
	
}