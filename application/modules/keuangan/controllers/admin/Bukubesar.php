<?php 
if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Bukubesar extends MY_Controller{

	var $nav_id = '07.02.02', $nav, $cookie;
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array(
			'admin/m_bukubesar',
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
	    $this->cookie = get_cookie_nav($this->nav_id);
	    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '', 'tahun' => date('Y'), 'akun_id' => '', 'tgl_jurnal_awal' => date('d-m-Y'), 'tgl_jurnal_akhir' => date('d-m-Y'),'is_search'=> '');
	    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'jurnal_id','type' => 'desc');
	    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}
	
	public function index() {	
		$this->authorize($this->nav, '_view');
		//main data
	    $data['nav'] = $this->nav;
	    $data['cookie'] = $this->cookie;
	    $data['form_action'] = site_url().'/app/search/'.$data['nav']['nav_id'];
	    if(@$data['cookie']['search']['is_search'] == 'true') {
	    	//
	    	$data['saldo_awal_tahun'] = $this->m_bukubesar->_get_saldo_awal($data['cookie']['search']['akun_id'], $data['cookie']['search']['tahun']);
            $data['saldo_bukubesar_sd'] = $this->m_bukubesar->_get_saldo_bukubesar_sd($data['cookie']['search']['akun_id'], $data['cookie']['search']['tahun'], $data['cookie']['search']['tgl_jurnal_awal']);
            $data['saldo_awal'] = $data['saldo_awal_tahun'] + $data['saldo_bukubesar_sd'];
			//
            $rs_bukubesar_head[0] = array(
            	'jurnal_id' 		=> '',
				'tgl_jurnal' 		=> '',
				'no_bukti' 			=> '',
				'akun_nm' 			=> 'SALDO AWAL',
				'nominal' 			=> '',
				'mdd' 				=> '',
				'mdb' 				=> '',
				'akun_id' 			=> '',
				'sub_nominal' 		=> '',
				'sub_uraian' 		=> '',
				'tp_saldo' 			=> '',
				'sub_nominal_debet' => '',
				'sub_nominal_kredit'=> '',
				'akun_nm' 			=> '',
				'tp_akun' 			=> '',
				'tp_laporan' 		=> '',
				'no' 				=> '1',
				'saldo' 			=> $data['saldo_awal'],
            );
			$rs_bukubesar_body = $this->m_bukubesar->list_data($data['cookie'], $data['saldo_awal']);
			$data['main'] = array_merge($rs_bukubesar_head, $rs_bukubesar_body);
	    }	    
		//param
		$data['tahun_awal'] = '2019';
		$data['tahun_akhir'] = date('Y') + 1;
		$data['list_akun'] = $this->m_bukubesar->all_data_akun();
		//
	    //set pagination
	    set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('admin/bukubesar/index',$data);
	}

	
}