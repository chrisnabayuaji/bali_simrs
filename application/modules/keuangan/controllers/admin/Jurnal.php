<?php 
if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Jurnal extends MY_Controller{

	var $nav_id = '07.02.01', $nav, $cookie;
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array(
			'admin/m_jurnal',
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
	    $this->cookie = get_cookie_nav($this->nav_id);
	    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '', 'no_bukti' => '');
	    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'jurnal_id','type' => 'desc');
	    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}
	
	public function index() {	
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(5, 0);
	    $this->cookie['total_rows'] = $this->m_jurnal->all_rows($this->cookie);
	    set_cookie_nav($this->nav_id, $this->cookie);
	    //main data
	    $data['nav'] = $this->nav;
	    $data['cookie'] = $this->cookie;
	    $data['main'] = $this->m_jurnal->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
		//param
		$data['tahun_awal'] = '2019';
		$data['tahun_akhir'] = date('Y') + 1;
		//
	    //set pagination
	    set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('admin/jurnal/index',$data);
	}

	public function form($id=null) {
    	$this->authorize($this->nav, ($id != '' ) ? '_update' : '_add');
	
		if($id == null) {
			$tgl_jurnal = ($this->input->get('tgl_jurnal') != '' ? $this->input->get('tgl_jurnal') : date('d-m-Y'));
			$get_data_trx = $this->input->get('get_data_trx');
			//
			$data['main'] = array();
			$data['main']['tgl_jurnal'] = to_date($tgl_jurnal);
			$data['main']['get_data_trx'] = $get_data_trx;
			//
			if($get_data_trx == 'ya') {
				$data['main']['list_item_debet'] = $this->m_jurnal->_list_item_data_raw_by_date(to_date($tgl_jurnal),'D');
				$data['main']['list_item_kredit'] = $this->m_jurnal->_list_item_data_raw_by_date(to_date($tgl_jurnal),'K');
			}			
			//
		} else {
			$data['main'] = $this->m_jurnal->get_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/save/'.$id;
			
		$this->render('admin/jurnal/form', $data);
	}

	public function detail_transaksi($akun_id=null, $tgl_jurnal=null) {
		$data['main'] = $this->m_jurnal->_detail_item_data_raw_by_date($akun_id, $tgl_jurnal);
		$data['akun_id'] = $akun_id;
		$data['tgl_jurnal'] = $tgl_jurnal;
		$data['nav'] = $this->nav;			
		echo json_encode(array(
			'html' => $this->load->view('admin/jurnal/_ajax_detail_transaksi', $data, true)
		));
	}

	public function save($id = null)
	{
		$this->m_jurnal->save($id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($id != '' ) ? '_update' : '_add', $this->nav_id);
		redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	public function delete($id = null)
	{
		$this->m_jurnal->delete($id,true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log('_delete', $this->nav_id);
		redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	public function multiple($type = null)
	{
		$data = $this->input->post();
	    if(isset($data['checkitem'])){
	      foreach ($data['checkitem'] as $key) {
	        switch ($type) {					
				case 'delete':
					$this->authorize($this->nav, '_delete');
        			$this->m_jurnal->delete($key);
					$flash = 'Data berhasil dihapus.';
					create_log('_delete', $this->nav_id);
	            break;

				case 'enable':
					$this->authorize($this->nav, '_update');
        			$this->m_jurnal->update($key, array('is_active' => 1));
					$flash = 'Data berhasil diaktifkan.';
					create_log('_update', $this->nav_id);
	            break;

				case 'disable':
					$this->authorize($this->nav, '_update');
        			$this->m_jurnal->update($key, array('is_active' => 0));
					$flash = 'Data berhasil dinonaktifkan.';
					create_log('_delete', $this->nav_id);
	            break;
	        }
	      }
	    }
	    create_log($t,$this->this->menu['menu']);
	    $this->session->set_flashdata('flash_success', $flash);
	    redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	function ajax($type=null, $id=null) {
		if($type == 'akun_autocomplete'){
			$akun_nm = $this->input->get('akun_nm');
			$res = $this->m_jurnal->akun_autocomplete($akun_nm);
			echo json_encode($res);
		} else if($type == 'add_item_akun') {
			$data['tp_saldo'] = $this->input->get('tp_saldo');
			$data['nav'] = $this->nav;
			echo json_encode(array(
				'html' => $this->load->view('admin/jurnal/_ajax_akun', $data, true)
			));
		}
	}
	
}