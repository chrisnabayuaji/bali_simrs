<?php 
if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Neraca extends MY_Controller{

	var $nav_id = '07.02.05', $nav, $cookie;
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array(
			'admin/m_neraca',
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
	    $this->cookie = get_cookie_nav($this->nav_id);
	    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '', 'tahun' => date('Y'), 'tgl_jurnal_awal' => date('d-m-Y'), 'tgl_jurnal_akhir' => date('d-m-Y'),'is_search'=> '');
	    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'jurnal_id','type' => 'desc');
	    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}
	
	public function index() {	
		$this->authorize($this->nav, '_view');
		//main data
	    $data['nav'] = $this->nav;
	    $data['cookie'] = $this->cookie;
	    $data['form_action'] = site_url().'/app/search/'.$data['nav']['nav_id'];
	    if(@$data['cookie']['search']['is_search'] == 'true') {
	    	$data['list_aktiva'] = $this->m_neraca->list_neraca($data['cookie'], 'AKT');
			$data['list_pasiva'] = $this->m_neraca->list_neraca($data['cookie'], 'PSV');
			$data['total_aktiva'] = $this->m_neraca->_get_jumlah_total($data['list_aktiva']);
			$data['total_pasiva'] = $this->m_neraca->_get_jumlah_total($data['list_pasiva']);
            $data['selisih'] = $data['total_aktiva'] - $data['total_pasiva'];
	    }	    
		//param
		$data['tahun_awal'] = '2019';
		$data['tahun_akhir'] = date('Y') + 1;
		//
	    //set pagination
	    set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('admin/neraca/index',$data);
	}

	
}