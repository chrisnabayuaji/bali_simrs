<?php 
if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Saldoawal extends MY_Controller{

	var $nav_id = '07.02.06', $nav, $cookie;
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array(
			'admin/m_saldoawal',
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
	    $this->cookie = get_cookie_nav($this->nav_id);
	    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '', 'tahun' => date('Y'), 'akun_id' => '', 'tgl_jurnal_awal' => date('d-m-Y'), 'tgl_jurnal_akhir' => date('d-m-Y'),'is_search'=> '');
	    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'jurnal_id','type' => 'desc');
	    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}
	
	public function index() {	
		$this->authorize($this->nav, '_view');
		//main data
	    $data['nav'] = $this->nav;
	    $data['cookie'] = $this->cookie;
	    $data['form_action'] = site_url().'/app/search/'.$data['nav']['nav_id'];
	    $data['form_action_save'] = site_url().'/'.$this->nav['nav_url'].'/save';
	    if(@$data['cookie']['search']['is_search'] == 'true') {
	    	$data['main'] = $this->m_saldoawal->list_data($this->cookie);
	    }	    
		//param
		$data['tahun_awal'] = '2019';
		$data['tahun_akhir'] = date('Y') + 1;
		//
	    //set pagination
	    set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('admin/saldoawal/index',$data);
	}

	public function save()
	{
		$this->m_saldoawal->save();
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		redirect(site_url().'/'.$this->nav['nav_url']);
	}

	
}