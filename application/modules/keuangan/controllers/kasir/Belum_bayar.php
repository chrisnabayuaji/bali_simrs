<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Belum_bayar extends MY_Controller
{

	var $nav_id = '07.01.01', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'kasir/m_belum_bayar',
			'master/m_lokasi',
			'master/m_jenis_pasien',
			'm_dt_belum_bayar',
			'm_dt_tarifkelas',
			'm_dt_petugas',
			'app/m_profile',
			'master/m_identitas',
			'kasir/m_dt_obat',
			'kasir/m_dt_alkes',
			'kasir/m_dt_bhp'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id . '.belum');
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('tgl_registrasi_from' => '', 'tgl_registrasi_to' => '', 'no_rm_nm' => '', 'lokasi_kasir' => '', 'lokasi_pelayanan' => '', 'jenispasien_id' => '', 'is_ranap' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'tgl_registrasi', 'type' => 'desc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(5, 0);
		$this->cookie['total_rows'] = $this->m_belum_bayar->all_rows($this->cookie);
		set_cookie_nav($this->nav_id . '.belum', $this->cookie);
		//main data
		$data['nav'] = $this->nav;
		$data['cookie'] = $this->cookie;
		$data['main'] = $this->m_belum_bayar->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
		$data['lokasi_kasir'] = $this->m_lokasi->by_field('is_kasir', 1, 'result');
		$data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
		//set pagination
		set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('keuangan/kasir/belum_bayar/index', $data);
	}

	public function form($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');
		$data['groupreg_in'] = $this->input->get('groupreg_in');
		// delete semua TINDAKAN KEPERAWATAN kemudian insert 1 TINDAKAN KEPERAWATAN
		$this->m_belum_bayar->delete_and_insert_tindakan_keperawatan($id, $data['groupreg_in']);
		// hitung biaya kamar
		$this->m_belum_bayar->hitung_biaya_kamar($id);
		//
		if ($id == null) {
			$data['main'] = array();
			$data['list_resep'] = array();
		} else {
			$data['main'] = $this->m_belum_bayar->get_data($id, $data['groupreg_in']);
			$data['list_resep'] = $this->m_belum_bayar->list_resep($id, @$data['main']['pasien_id']);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save/' . $id . '?groupreg_in=' . $data['groupreg_in'];
		$last = $this->m_belum_bayar->get_last();
		$get_billing = $this->m_belum_bayar->get_billing($id);
		if ($get_billing != null) {
			$data['no_invoice'] = $data['main']['no_invoice'];
		} else {
			if ($last == null) {
				$no = 1;
			} else {
				$raw_no = explode('/', $last['no_invoice']);
				$no = intval($raw_no[0]) + 1;
			}
			$data['no_invoice'] = $no . '/INV/RSIAPMT-' . @$data['main']['jenispasien_nm'] . '/' . to_rome(date('m')) . '/' . date('Y');
		}

		$this->render('keuangan/kasir/belum_bayar/form', $data);
	}

	public function save($groupreg_id = null)
	{
		$groupreg_in = $this->input->get('groupreg_in');
		$this->m_belum_bayar->save($groupreg_id, $groupreg_in);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $groupreg_id . '?groupreg_in=' . $groupreg_in);
		// redirect(site_url() . '/' . $this->nav['nav_url'] . '/index');
	}

	// public function cetak_kwitansi($groupreg_id=null) {
	// 	$data['groupreg_in'] = $this->input->get('groupreg_in');
	// 	$data['main'] = $this->m_belum_bayar->get_data($groupreg_id, $data['groupreg_in']);
	// 	$data['identitas'] = $this->m_app->_get_identitas();
	// 	$data['list_rincian'] = $this->m_belum_bayar->list_rincian($groupreg_id, $data['main']['billing_id'], $data['groupreg_in']);
	// 	$data['list_riwayat_bangsal'] = $this->m_belum_bayar->list_riwayat_bangsal($groupreg_id, $data['groupreg_in']);
	// 	$data['nav'] = $this->nav;

	// 	$this->load->view('keuangan/kasir/belum_bayar/cetak_kwitansi', $data);
	// }

	public function cetak_modal($type = null, $id = null, $id2 = null)
	{
		$groupreg_in = $this->input->get('groupreg_in');
		if ($type == 'cetak_kwitansi_parsial') {
			$data['url'] = site_url() . '/' . $this->nav['nav_url'] . '/' . $type . '/' . $id . '/' . $id2;
		} else {
			$data['url'] = site_url() . '/' . $this->nav['nav_url'] . '/' . $type . '/' . $id . '?groupreg_in=' . $groupreg_in;
		}

		echo json_encode(array(
			'html' => $this->load->view('keuangan/kasir/belum_bayar/cetak_modal', $data, true)
		));
	}

	public function cetak_kwitansi($id = null)
	{
		$groupreg_in = $this->input->get('groupreg_in');
		$main = $this->m_belum_bayar->get_data($id, $groupreg_in);
		$config = $this->m_app->_get_config();
		$identitas = $this->m_app->_get_identitas();
		$list_tindakan = $this->m_belum_bayar->tindakan_data(@$main['pasien_id'], @$main['reg_id']);
		$list_pendaftaran = $this->m_belum_bayar->list_pendaftaran(@$main['pasien_id'], @$main['reg_id']);
		if (@$main['jenispasien_id'] == '02') {
			$list_detail_obat = $this->m_belum_bayar->list_detail_obat($id, $main['pasien_id']);
		}
		$nav = $this->nav;
		//generate pdf
		$profile = $this->m_profile->get_first();
		$this->load->library('pdf');
		// $pdf = new Pdf('l', 'mm', array(240, 145)); //A5
		$pdf = new Pdf('l', 'mm', array(240, 150)); //A5
		$pdf->AliasNbPages();
		$pdf->SetTitle('Cetak Kwitansi ' . $id);
		$pdf->AddPage();

		$pdf->Image(FCPATH . 'assets/images/icon/' . $identitas['logo_rumah_sakit'], 12, 9, 12, 12);
		$pdf->SetFont('Arial', '', 9);
		$pdf->Cell(15, 6, '', 0, 0, 'L');
		$pdf->Cell(90, 2, $profile['title_logo_login'], 0, 0, 'L');
		$pdf->SetFont('Arial', 'B', 9);
		$pdf->Cell(75, 2, @$main['reg_id'], 0, 1, 'R');

		$pdf->SetFont('Arial', 'B', 11);
		$pdf->Cell(15, 6, '', 0, 0, 'L');
		$pdf->Cell(90, 6, $profile['sub_title_logo_login'], 0, 0, 'L');
		$pdf->SetFont('Arial', '', 9);
		$pdf->Cell(75, 6, to_date($main['tgl_registrasi'], '', 'full_date'), 0, 1, 'R');

		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(15, 6, '', 0, 0, 'L');
		$pdf->Cell(0, 2, @$identitas['jalan'] . ', ' . ucfirst(strtolower(clear_kab_kota(@$identitas['kabupaten']))) . ', ' . ucfirst(strtolower(@$identitas['propinsi'])), 0, 1, 'L');
		// $pdf->Line(10, 23.5, 200, 23.5);
		// $pdf->Line(10, 23.5, 200, 23.5);
		// $pdf->Line(10, 23.5, 200, 23.5);
		// $pdf->Line(10, 23.5, 200, 23.5);
		// $pdf->Line(10, 23.5, 200, 23.5);

		$pdf->Cell(0, 3, '', 0, 1, 'L');
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(0, 4, 'RINCIAN PEMBAYARAN', 0, 1, 'C');
		$pdf->Cell(0, 0, '', 0, 1, 'C');

		//Identitas Pasien
		$pdf->SetFont('Arial', '', 8);
		// $pdf->Cell(30, 4, 'No. Transaksi', 0, 0, 'L');
		// $pdf->Cell(170, 4, ':  ' . @$main['reg_id'], 0, 1, 'L');

		$pdf->Cell(30, 4, 'No.RM', 0, 0, 'L');
		$pdf->Cell(170, 4, ':  ' . strtoupper($main['pasien_id']), 0, 1, 'L');

		$pdf->Cell(30, 4, 'Nama Pasien', 0, 0, 'L');
		$pdf->Cell(170, 4, ':  ' . strtoupper(@$main['pasien_nm'] . @$main['sebutan_cd']), 0, 1, 'L');

		$pdf->Cell(30, 4, 'Umur', 0, 0, 'L');
		$pdf->Cell(170, 4, ':  ' . $main['umur_thn'] . ' Thn ' . $main['umur_bln'] . ' Bln ' . $main['umur_hr'] . ' Hari', 0, 1, 'L');

		$pdf->Cell(30, 4, 'Alamat', 0, 0, 'L');
		$pdf->Cell(170, 4, (@$main['alamat'] != '') ? ':  ' . strtoupper(@$main['alamat']) . ', ' . strtoupper(@$main['kelurahan']) . strtoupper(@$main['kecamatan']) . ' ' . strtoupper(@$main['kabupaten']) : ':  ' . strtoupper(@$main['kelurahan']) . strtoupper(@$main['kecamatan']) . ' ' . strtoupper(@$main['kabupaten']), 0, 1, 'L');

		// $pdf->Cell(30, 4, '', 0, 0, 'L');
		// $pdf->Cell(170, 4, '   ' . strtoupper(@$main['kecamatan']) . ' ' . strtoupper(@$main['kabupaten']), 0, 1, 'L');

		// $pdf->Cell(30, 4, 'Tgl. Periksa', 0, 0, 'L');
		// $pdf->Cell(170, 4, ':  ' . strtoupper(to_date_indo($main['tgl_registrasi'])), 0, 1, 'L');

		//list biaya
		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(0, 3, '', 0, 1, 'C');
		$pdf->Cell(0, 4, 'Biaya Tindakan & Pemeriksaan :', 0, 0, 'L');

		$pdf->Cell(0, 4, '', 0, 1, 'C');
		$tot_awal = 0;
		$tot_potongan = 0;
		foreach ($list_tindakan as $tindakan) {
			$tot_awal += $tindakan['jml_awal'];
			$tot_potongan += $tindakan['jml_potongan'];

			$pdf->SetFont('Arial', '', 8);
			$pdf->Cell(90, 4, '- ' . $tindakan['tarif_nm'], 0, 0, 'L');
			$pdf->SetFont('Arial', 'B', 8);
			$pdf->Cell(90, 4, num_id($tindakan['jml_awal']) . ',00', 0, 1, 'R');
		}

		$tot_obat = @$main['grand_jml_obat'] + @$main['grand_jml_alkes'] + @$main['grand_jml_bhp'];

		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(0, 0, '', 0, 1, 'C');
		$pdf->Cell(90, 4, 'Biaya Obat / Alkes / BMHP :', 0, 0, 'L');
		if (@$main['jenispasien_id'] == '02') {
			$pdf->Cell(90, 4, '', 0, 1, 'R');
		} else {
			$pdf->Cell(90, 4, num_id($tot_obat) . ',00', 0, 1, 'R');
		}

		if (@$main['jenispasien_id'] == '02') {
			foreach ($list_detail_obat as $obat) {
				$pdf->SetFont('Arial', '', 8);
				$pdf->Cell(90, 4, '- ' . $obat['obat_nm'], 0, 0, 'L');
				$pdf->SetFont('Arial', 'B', 8);
				$pdf->Cell(90, 4, num_id($obat['jumlah_akhir']) . ',00', 0, 1, 'R');
			}
		}

		//administrasi
		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(0, 4, 'Administrasi :', 0, 0, 'L');

		$pdf->Cell(0, 4, '', 0, 1, 'C');
		$tot_awal_pendaftaran = 0;
		$tot_potongan_pendaftaran = 0;
		foreach ($list_pendaftaran as $pendaftaran) {
			$tot_awal_pendaftaran += $pendaftaran['jml_awal'];
			$tot_potongan_pendaftaran += $pendaftaran['jml_potongan'];

			$pdf->SetFont('Arial', '', 8);
			$pdf->Cell(90, 4, '- ' . $pendaftaran['tarif_nm'], 0, 0, 'L');
			$pdf->SetFont('Arial', 'B', 8);
			$pdf->Cell(90, 4, num_id($pendaftaran['jml_awal']) . ',00', 0, 1, 'R');
		}

		$pdf->Cell(0, 0, '', 0, 1, 'L');
		$pdf->Cell(0, 1, '', 'B', 1, 'C');

		$tot_biaya = $tot_obat + $tot_awal + $tot_awal_pendaftaran;

		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(0, 0, '', 0, 1, 'C');
		$pdf->Cell(90, 6, 'Total Biaya :', 0, 0, 'L');
		$pdf->Cell(90, 6, num_id($tot_biaya) . ',00', 0, 1, 'R');

		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(0, 0, '', 0, 1, 'C');
		$pdf->Cell(90, 4, 'Potongan - potongan :', 0, 0, 'L');
		$pdf->Cell(90, 4, num_id($tot_potongan) . ',00', 0, 1, 'R');

		$pdf->Cell(0, 0, '', 0, 1, 'L');
		$pdf->Cell(0, 1, '', 'B', 1, 'C');

		$tot_bayar = $tot_biaya - $tot_potongan - $tot_potongan_pendaftaran;

		if ($main['jenispasien_id'] == '02') {
			$tot_bayar = 0;
		}

		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(0, 1, '', 0, 1, 'C');
		$pdf->Cell(90, 4, 'Total Bayar / Kekurangan :', 0, 0, 'L');
		$pdf->Cell(90, 4, num_id($tot_bayar) . ',00', 0, 1, 'R');

		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(0, 1, '', 0, 1, 'C');
		$pdf->Cell(155, 4, 'Dibayar   :', 0, 0, 'R');
		$pdf->Cell(25, 4, num_id(@$main['grand_total_bayar']) . ',00', 0, 1, 'R');

		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(0, 1, '', 0, 1, 'C');
		$pdf->Cell(155, 4, 'Kembalian   :', 0, 0, 'R');
		$pdf->Cell(25, 4, num_id(@$main['grand_total_bayar'] - @$tot_bayar) . ',00', 0, 1, 'R');

		// petugas
		$pdf->Cell(0, 1, '', 0, 1, 'C');
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(0, 2, '', 0, 1, 'C');
		$pdf->Cell(70, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, $identitas['kabupaten'] . ', ' . strtoupper(to_date_indo(@$main['tgl_registrasi'], 'date')), 0, 1, 'C');
		$pdf->Cell(70, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, 'BAGIAN KEUANGAN', 0, 1, 'C');
		$pdf->Cell(0, 8, '', 0, 1, 'C');
		$pdf->Cell(70, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, @$this->session->userdata('sess_user_realname'), 0, 1, 'C');

		$pdf->Output('I', 'Kwitansi_' . $id . '_' . date('Ymdhis') . '.pdf');
	}

	public function cetak_kwitansi_parsial($billing_id = null, $split_id = null)
	{
		$main = $this->m_belum_bayar->get_kwitansi_parsial($billing_id, $split_id);
		$identitas = $this->m_app->_get_identitas();
		$list_tindakan = $this->m_belum_bayar->tindakan_data(@$main['pasien_id'], @$main['reg_id']);
		$list_pendaftaran = $this->m_belum_bayar->list_pendaftaran(@$main['pasien_id'], @$main['reg_id']);
		//generate pdf
		$profile = $this->m_profile->get_first();
		$this->load->library('pdf');
		// $pdf = new Pdf('l', 'mm', array(240, 145)); //A5
		$pdf = new Pdf('l', 'mm', array(240, 150)); //A5
		$pdf->AliasNbPages();
		$pdf->SetTitle('Cetak Kwitansi ' . $billing_id);
		$pdf->AddPage();

		$pdf->Image(FCPATH . 'assets/images/icon/' . $identitas['logo_rumah_sakit'], 12, 9, 12, 12);
		$pdf->SetFont('Arial', '', 9);
		$pdf->Cell(15, 6, '', 0, 0, 'L');
		$pdf->Cell(90, 2, $profile['title_logo_login'], 0, 0, 'L');
		$pdf->SetFont('Arial', 'B', 9);
		$pdf->Cell(75, 2, @$main['reg_id'], 0, 1, 'R');

		$pdf->SetFont('Arial', 'B', 11);
		$pdf->Cell(15, 6, '', 0, 0, 'L');
		$pdf->Cell(90, 6, $profile['sub_title_logo_login'], 0, 0, 'L');
		$pdf->SetFont('Arial', '', 9);
		$pdf->Cell(75, 6, to_date($main['tgl_registrasi'], '', 'full_date'), 0, 1, 'R');

		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(15, 6, '', 0, 0, 'L');
		$pdf->Cell(0, 2, @$identitas['jalan'] . ', ' . ucfirst(strtolower(clear_kab_kota(@$identitas['kabupaten']))) . ', ' . ucfirst(strtolower(@$identitas['propinsi'])), 0, 1, 'L');
		// $pdf->Line(10, 23.5, 200, 23.5);
		// $pdf->Line(10, 23.5, 200, 23.5);
		// $pdf->Line(10, 23.5, 200, 23.5);
		// $pdf->Line(10, 23.5, 200, 23.5);
		// $pdf->Line(10, 23.5, 200, 23.5);

		$pdf->Cell(0, 3, '', 0, 1, 'L');
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(0, 4, 'RINCIAN PEMBAYARAN', 0, 1, 'C');
		$pdf->Cell(0, 0, '', 0, 1, 'C');

		//Identitas Pasien
		$pdf->SetFont('Arial', '', 8);
		// $pdf->Cell(30, 4, 'No. Transaksi', 0, 0, 'L');
		// $pdf->Cell(170, 4, ':  ' . @$main['reg_id'], 0, 1, 'L');

		$pdf->Cell(30, 4, 'No.RM', 0, 0, 'L');
		$pdf->Cell(170, 4, ':  ' . strtoupper($main['pasien_id']), 0, 1, 'L');

		$pdf->Cell(30, 4, 'Nama Pasien', 0, 0, 'L');
		$pdf->Cell(170, 4, ':  ' . strtoupper(@$main['pasien_nm'] . @$main['sebutan_cd']), 0, 1, 'L');

		$pdf->Cell(30, 4, 'Umur', 0, 0, 'L');
		$pdf->Cell(170, 4, ':  ' . $main['umur_thn'] . ' Thn ' . $main['umur_bln'] . ' Bln ' . $main['umur_hr'] . ' Hari', 0, 1, 'L');

		$pdf->Cell(30, 4, 'Alamat', 0, 0, 'L');
		$pdf->Cell(170, 4, (@$main['alamat'] != '') ? ':  ' . strtoupper(@$main['alamat']) . ', ' . strtoupper(@$main['kelurahan']) . strtoupper(@$main['kecamatan']) . ' ' . strtoupper(@$main['kabupaten']) : ':  ' . strtoupper(@$main['kelurahan']) . strtoupper(@$main['kecamatan']) . ' ' . strtoupper(@$main['kabupaten']), 0, 1, 'L');

		// $pdf->Cell(30, 4, '', 0, 0, 'L');
		// $pdf->Cell(170, 4, '   ' . strtoupper(@$main['kecamatan']) . ' ' . strtoupper(@$main['kabupaten']), 0, 1, 'L');

		// $pdf->Cell(30, 4, 'Tgl. Periksa', 0, 0, 'L');
		// $pdf->Cell(170, 4, ':  ' . strtoupper(to_date_indo($main['tgl_registrasi'])), 0, 1, 'L');

		//list biaya
		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(0, 3, '', 0, 1, 'C');
		$pdf->Cell(90, 4, @$main['tarif_nm'] . ' :', 0, 0, 'L');
		$pdf->Cell(90, 4, num_id($main['grand_total_tagihan']) . ',00', 0, 1, 'R');

		$pdf->Cell(0, 0, '', 0, 1, 'L');
		$pdf->Cell(0, 1, '', 'B', 1, 'C');

		$tot_biaya = $main['grand_total_tagihan'];

		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(0, 0, '', 0, 1, 'C');
		$pdf->Cell(90, 6, 'Total Biaya :', 0, 0, 'L');
		$pdf->Cell(90, 6, num_id($tot_biaya) . ',00', 0, 1, 'R');

		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(0, 0, '', 0, 1, 'C');
		$pdf->Cell(90, 4, 'Potongan - potongan :', 0, 0, 'L');
		$pdf->Cell(90, 4, num_id($main['grand_total_potongan']) . ',00', 0, 1, 'R');

		$pdf->Cell(0, 0, '', 0, 1, 'L');
		$pdf->Cell(0, 1, '', 'B', 1, 'C');

		$tot_bayar = $tot_biaya - $main['grand_total_potongan'];

		if ($main['jenispasien_id'] == '02') {
			$tot_bayar = 0;
		}

		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(0, 1, '', 0, 1, 'C');
		$pdf->Cell(90, 4, 'Total Bayar / Kekurangan :', 0, 0, 'L');
		$pdf->Cell(90, 4, num_id($tot_bayar) . ',00', 0, 1, 'R');

		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(0, 1, '', 0, 1, 'C');
		$pdf->Cell(155, 4, 'Dibayar   :', 0, 0, 'R');
		$pdf->Cell(25, 4, num_id(@$main['grand_total_bayar']) . ',00', 0, 1, 'R');

		$pdf->SetFont('Arial', 'B', 8);
		$pdf->Cell(0, 1, '', 0, 1, 'C');
		$pdf->Cell(155, 4, 'Kembalian   :', 0, 0, 'R');
		$pdf->Cell(25, 4, num_id(@$main['grand_total_bayar'] - @$tot_bayar) . ',00', 0, 1, 'R');

		// petugas
		$pdf->Cell(0, 1, '', 0, 1, 'C');
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(0, 2, '', 0, 1, 'C');
		$pdf->Cell(70, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, $identitas['kabupaten'] . ', ' . strtoupper(to_date_indo(@$main['tgl_registrasi'], 'date')), 0, 1, 'C');
		$pdf->Cell(70, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, 'BAGIAN KEUANGAN', 0, 1, 'C');
		$pdf->Cell(0, 8, '', 0, 1, 'C');
		$pdf->Cell(70, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, @$this->session->userdata('sess_user_realname'), 0, 1, 'C');

		$pdf->Output('I', 'Kwitansi_' . $billing_id . '_' . date('Ymdhis') . '.pdf');
	}

	public function cetak_tindakan($id = null)
	{
		$groupreg_in = $this->input->get('groupreg_in');
		$main = $this->m_belum_bayar->get_data($id, $groupreg_in);
		$config = $this->m_app->_get_config();
		$identitas = $this->m_app->_get_identitas();
		$list_tindakan = $this->m_belum_bayar->tindakan_data(@$main['pasien_id'], @$main['reg_id']);
		$nav = $this->nav;
		//generate pdf
		$profile = $this->m_profile->get_first();
		$this->load->library('pdf');
		$pdf = new Pdf('p', 'mm', array(210, 297));
		$pdf->AliasNbPages();
		$pdf->SetTitle('Cetak Rincian Tindakan ' . $id);
		$pdf->AddPage();

		// $pdf->Image(FCPATH . 'assets/images/icon/pt-cahaya-permata-medika.png', 10, 10, 15, 15);
		// $pdf->SetFont('Arial', '', 9);
		// $pdf->Cell(0, 4, 'PT. CAHAYA PERMATA MEDIKA', 0, 1, 'C');
		// $pdf->SetFont('Arial', 'B', 11);
		// $pdf->Cell(0, 6, @$profile['title_logo_login'] . ' ' . @$profile['sub_title_logo_login'], 0, 1, 'C');
		// $pdf->SetFont('Arial', '', 7);
		// $pdf->Cell(0, 3, 'Jl. Mayjen Sutoyo No. 75 Purworejo Telp. (0275) 321031', 0, 1, 'C');
		// $pdf->Cell(0, 3, 'Email. rsiapermatapwr@gmail.com', 0, 1, 'C');
		// $pdf->Image(FCPATH . 'assets/images/icon/simrs-logo-rs.jpg', 184, 10, 15, 15);
		$pdf->Image(FCPATH . 'assets/images/icon/new-kop.jpg', 10, 6, 160, 20);
		$pdf->Cell(0, 16, '', 0, 1, 'C');
		$pdf->Line(10, 28, 200, 28);
		$pdf->Line(10, 28, 200, 28);
		$pdf->Line(10, 28, 200, 28);
		$pdf->Line(10, 28, 200, 28);
		$pdf->Line(10, 28, 200, 28);

		$pdf->Cell(0, 3, '', 0, 1, 'L');
		$pdf->SetFont('Arial', 'B', 9);
		$pdf->Cell(0, 4, 'RINCIAN TINDAKAN', 0, 1, 'C');
		$pdf->Cell(0, 2, '', 0, 1, 'C');

		$pdf->Line(10, 33.7, 200, 33.7);
		//Identitas Pasien
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(30, 4, 'Nama Pasien', 0, 0, 'L');
		$pdf->Cell(80, 4, ': ' . strtoupper(@$main['pasien_nm'] . @$main['sebutan_cd']), 0, 0, 'L');
		$pdf->Cell(30, 4, 'Tgl. Lahir', 0, 0, 'L');
		$pdf->Cell(60, 4, ': ' . to_date(@$main['tgl_lahir'], '/'), 0, 1, 'L');

		$pdf->Cell(30, 4, 'NO. RM', 0, 0, 'L');
		$pdf->Cell(80, 4, ': ' . strtoupper($main['pasien_id']), 0, 0, 'L');
		$pdf->Cell(30, 4, 'Status', 0, 0, 'L');
		$pdf->Cell(60, 4, ': ' . @$main['jenispasien_nm'], 0, 1, 'L');

		$pdf->Cell(30, 4, 'Alamat', 0, 0, 'L');
		$pdf->Cell(80, 4, (@$main['alamat'] != '') ? ': ' . strtoupper(@$main['alamat']) . ' ' . strtoupper(@$main['kelurahan']) : ': ' . strtoupper(@$main['kelurahan']), 0, 0, 'L');
		$pdf->Cell(30, 4, 'No. Peserta', 0, 0, 'L');
		$pdf->Cell(60, 4, ': ' . @$main['no_kartu'], 0, 1, 'L');

		$pdf->Cell(30, 4, '', 0, 0, 'L');
		$pdf->Cell(80, 4, '  ' . strtoupper(@$main['kecamatan']) . ' ' . strtoupper(@$main['kabupaten']), 0, 0, 'L');
		$pdf->Cell(30, 4, '', 0, 0, 'L');
		$pdf->Cell(60, 4, '', 0, 1, 'L');

		$pdf->Line(10, 52, 200, 52);
		$pdf->Line(10, 52, 200, 52);
		$pdf->Line(10, 52, 200, 52);
		$pdf->Line(10, 52, 200, 52);
		$pdf->Line(10, 52, 200, 52);

		// thead
		$pdf->Cell(0, 2, '', 0, 1, 'L');
		$pdf->SetFont('Arial', '', 9);
		$pdf->Cell(30, 4, 'KODE', 0, 0, 'C');
		$pdf->Cell(120, 4, 'KETERANGAN', 0, 0, 'C');
		$pdf->Cell(10, 4, 'JML', 0, 0, 'C');
		$pdf->Cell(30, 4, 'TAGIHAN', 0, 1, 'C');
		$pdf->Line(10, 57.4, 200, 57.4);

		// tbody
		$pdf->Cell(0, 2, '', 0, 1, 'L');
		$pdf->SetFont('Arial', '', 9);

		$tot_tagihan = 0;
		if (count($list_tindakan) > 0) {
			foreach ($list_tindakan as $row) {
				$tot_tagihan += $row['jml_tagihan'];

				$pdf->Cell(30, 5, $row['tarif_id'], 0, 0, 'C');
				$pdf->Cell(120, 5, $row['tarif_nm'] . ' - ' . $row['kelas_nm'], 0, 0, 'L');
				$pdf->Cell(10, 5, $row['qty'], 0, 0, 'C');
				$pdf->Cell(30, 5, num_id($row['jml_tagihan']), 0, 1, 'R');
			}
		} else {
			$pdf->Cell(0, 5, 'Data tidak ada', 0, 1, 'C');
		}

		$pdf->Cell(0, 1, '', 'B', 1, 'C');
		$pdf->Cell(0, 0, '', 'B', 1, 'C');
		$pdf->Cell(0, 0, '', 'B', 1, 'C');
		$pdf->Cell(0, 0, '', 'B', 1, 'C');
		$pdf->Cell(0, 0, '', 'B', 1, 'C');
		$pdf->Cell(0, 0, '', 'B', 1, 'C');

		// footer
		$pdf->Cell(0, 2, '', 0, 1, 'L');
		$pdf->SetFont('Arial', '', 9);
		$pdf->Cell(90, 4, 'Terbilang : ', 0, 0, 'L');
		$pdf->Cell(70, 4, 'Total Tagihan', 0, 0, 'R');
		$pdf->SetFont('Arial', 'B', 9);
		$pdf->Cell(30, 4, num_id(@$tot_tagihan), 0, 1, 'R');

		$pdf->SetFont('Arial', '', 9);
		$pdf->MultiCell(100, 4, strtoupper(terbilang(@$tot_tagihan)), 0, 'L');

		// petugas
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(0, 10, '', 0, 1, 'C');
		$pdf->Cell(60, 4, 'Pasien', 0, 0, 'C');
		$pdf->Cell(60, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, $identitas['kabupaten'] . ', ' . strtoupper(to_date_indo(date('Y-m-d'))), 0, 1, 'C');
		$pdf->Cell(120, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, 'BAGIAN KEUANGAN', 0, 1, 'C');
		$pdf->Cell(0, 15, '', 0, 1, 'C');
		$pdf->Cell(60, 4, strtoupper(@$main['pasien_nm'] . ', ' . @$main['sebutan_cd']), 0, 0, 'C');
		$pdf->Cell(60, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, @$this->session->userdata('sess_user_realname'), 0, 1, 'C');

		$pdf->Output('I', 'Rincian_Tindakan_' . $id . '_' . date('Ymdhis') . '.pdf');
	}

	public function cetak_obat($id = null)
	{
		$groupreg_in = $this->input->get('groupreg_in');
		$main = $this->m_belum_bayar->get_data($id, $groupreg_in);
		$config = $this->m_app->_get_config();
		$identitas = $this->m_app->_get_identitas();
		$list_obat = $this->m_belum_bayar->pemberian_obat_data(@$main['reg_id'], @$main['pasien_id']);
		$nav = $this->nav;
		//generate pdf
		$profile = $this->m_profile->get_first();
		$this->load->library('pdf');
		$pdf = new Pdf('p', 'mm', array(210, 297));
		$pdf->AliasNbPages();
		$pdf->SetTitle('Cetak Rincian Obat ' . $id);
		$pdf->AddPage();

		// $pdf->Image(FCPATH . 'assets/images/icon/pt-cahaya-permata-medika.png', 10, 10, 15, 15);
		// $pdf->SetFont('Arial', '', 9);
		// $pdf->Cell(0, 4, 'PT. CAHAYA PERMATA MEDIKA', 0, 1, 'C');
		// $pdf->SetFont('Arial', 'B', 11);
		// $pdf->Cell(0, 6, @$profile['title_logo_login'] . ' ' . @$profile['sub_title_logo_login'], 0, 1, 'C');
		// $pdf->SetFont('Arial', '', 7);
		// $pdf->Cell(0, 3, 'Jl. Mayjen Sutoyo No. 75 Purworejo Telp. (0275) 321031', 0, 1, 'C');
		// $pdf->Cell(0, 3, 'Email. rsiapermatapwr@gmail.com', 0, 1, 'C');
		// $pdf->Image(FCPATH . 'assets/images/icon/simrs-logo-rs.jpg', 184, 10, 15, 15);
		$pdf->Image(FCPATH . 'assets/images/icon/new-kop.jpg', 10, 6, 160, 20);
		$pdf->Cell(0, 16, '', 0, 1, 'C');
		$pdf->Line(10, 28, 200, 28);
		$pdf->Line(10, 28, 200, 28);
		$pdf->Line(10, 28, 200, 28);
		$pdf->Line(10, 28, 200, 28);
		$pdf->Line(10, 28, 200, 28);

		$pdf->Cell(0, 5, '', 0, 1, 'L');

		//Identitas Pasien
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(30, 4, 'Nama Pasien', 0, 0, 'L');
		$pdf->Cell(80, 4, ': ' . strtoupper(@$main['pasien_nm'] . @$main['sebutan_cd']), 0, 0, 'L');
		$pdf->Cell(30, 4, 'Tgl. Lahir', 0, 0, 'L');
		$pdf->Cell(60, 4, ': ' . to_date(@$main['tgl_lahir'], '/'), 0, 1, 'L');

		$pdf->Cell(30, 4, 'NO. RM', 0, 0, 'L');
		$pdf->Cell(80, 4, ': ' . strtoupper($main['pasien_id']), 0, 0, 'L');
		$pdf->Cell(30, 4, 'Status', 0, 0, 'L');
		$pdf->Cell(60, 4, ': ' . @$main['jenispasien_nm'], 0, 1, 'L');

		$pdf->Cell(30, 4, 'Alamat', 0, 0, 'L');
		$pdf->Cell(80, 4, (@$main['alamat'] != '') ? ': ' . strtoupper(@$main['alamat']) . ' ' . strtoupper(@$main['kelurahan']) : ': ' . strtoupper(@$main['kelurahan']), 0, 0, 'L');
		$pdf->Cell(30, 4, 'No. Peserta', 0, 0, 'L');
		$pdf->Cell(60, 4, ': ' . @$main['no_kartu'], 0, 1, 'L');

		$pdf->Cell(30, 4, '', 0, 0, 'L');
		$pdf->Cell(80, 4, '  ' . strtoupper(@$main['kecamatan']) . ' ' . strtoupper(@$main['kabupaten']), 0, 0, 'L');
		$pdf->Cell(30, 4, '', 0, 0, 'L');
		$pdf->Cell(60, 4, '', 0, 1, 'L');

		$pdf->Line(10, 48, 200, 48);
		$pdf->Line(10, 48, 200, 48);
		$pdf->Line(10, 48, 200, 48);
		$pdf->Line(10, 48, 200, 48);
		$pdf->Line(10, 48, 200, 48);

		// thead
		$pdf->Cell(0, 2, '', 0, 1, 'L');
		$pdf->SetFont('Arial', '', 9);
		$pdf->Cell(10, 4, 'No', 0, 0, 'C');
		$pdf->Cell(100, 4, 'Pelayanan', 0, 0, 'C');
		$pdf->Cell(20, 4, 'Tarif', 0, 0, 'C');
		$pdf->Cell(20, 4, 'Jml', 0, 0, 'C');
		$pdf->Cell(20, 4, 'Tagihan', 0, 0, 'C');
		$pdf->Cell(20, 4, 'Potongan', 0, 1, 'C');
		$pdf->Line(10, 53.5, 200, 53.5);

		// tbody
		$pdf->SetFont('Arial', 'I', 9);

		$tot_tagihan = 0;
		$tot_potongan = 0;
		if (count($list_obat) > 0) {
			foreach ($list_obat as $obat) {
				$pdf->Cell(0, 2, '', 0, 1, 'L');
				$pdf->Cell(45, 4, 'Resep No : ' . $obat['resep_id'], 0, 0, 'L');
				$pdf->Cell(70, 4, 'Tanggal : ' . to_date_indo($obat['tgl_catat']), 0, 0, 'L');
				$pdf->Cell(0, 3, '', 0, 1, 'L');

				$pdf->SetFont('Arial', '', 9);
				$pdf->Cell(0, 2, '', 0, 1, 'L');

				$i = 1;
				foreach ($obat['list_dat_resep'] as $dat_resep) {
					$tot_tagihan += $dat_resep['jumlah_awal'];
					$tot_potongan += $dat_resep['potongan'];

					$pdf->Cell(10, 4, $i++, 0, 0, 'C');
					$pdf->Cell(100, 4, $dat_resep['obat_nm'], 0, 0, 'L');
					$pdf->Cell(20, 4, num_id($dat_resep['harga']), 0, 0, 'R');
					$pdf->Cell(20, 4, $dat_resep['qty'], 0, 0, 'C');
					$pdf->Cell(20, 4, num_id($dat_resep['jumlah_awal']), 0, 0, 'R');
					$pdf->Cell(20, 4, num_id($dat_resep['potongan']), 0, 1, 'R');
				}
			}
		} else {
			$pdf->SetFont('Arial', '', 9);
			$pdf->Cell(0, 3, '', 0, 1, 'L');
			$pdf->Cell(0, 1, 'Data tidak ada', 0, 1, 'C');
		}

		$pdf->Cell(0, 2, '', 0, 1, 'L');
		$pdf->Cell(0, 1, '', 'B', 1, 'C');

		$pdf->Cell(0, 2, '', 0, 1, 'L');
		$pdf->SetFont('Arial', '', 9);
		$pdf->Cell(90, 4, 'Terbilang : ', 0, 0, 'L');
		$pdf->Cell(50, 4, 'Total Tagihan', 0, 0, 'R');
		$pdf->SetFont('Arial', 'B', 9);
		$pdf->Cell(30, 4, num_id(@$tot_tagihan), 0, 1, 'R');

		$pdf->SetFont('Arial', '', 9);
		$pdf->tablewidths = array(90, 50, 30);
		$pdf->tablealign = array('L', 'R', 'R');
		$data[] = array(strtoupper(terbilang(@$tot_tagihan)), 'Total Potongan', num_id(@$tot_potongan));
		$pdf->morepagestable($data, 4, 0);

		$tot_bayar = $tot_tagihan - $tot_potongan;

		$pdf->Cell(0, 2, '', 0, 1, 'L');
		$pdf->SetFont('Arial', '', 9);
		$pdf->Cell(90, 4, '', 0, 0, 'L');
		$pdf->Cell(50, 4, 'Total Bayar', 0, 0, 'R');
		$pdf->SetFont('Arial', 'B', 9);
		$pdf->Cell(30, 4, num_id(@$tot_bayar), 0, 1, 'R');

		// petugas
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(0, 10, '', 0, 1, 'C');
		$pdf->Cell(60, 4, 'Pasien', 0, 0, 'C');
		$pdf->Cell(60, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, $identitas['kabupaten'] . ', ' . strtoupper(to_date_indo(date('Y-m-d'))), 0, 1, 'C');
		$pdf->Cell(120, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, 'BAGIAN KEUANGAN', 0, 1, 'C');
		$pdf->Cell(0, 15, '', 0, 1, 'C');
		$pdf->Cell(60, 4, strtoupper(@$main['pasien_nm'] . ', ' . @$main['sebutan_cd']), 0, 0, 'C');
		$pdf->Cell(60, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, @$this->session->userdata('sess_user_realname'), 0, 1, 'C');

		$pdf->Output('I', 'Rincian_Obat_' . $id . '_' . date('Ymdhis') . '.pdf');
	}

	public function cetak_invoice($id = null)
	{
		$groupreg_in = $this->input->get('groupreg_in');
		$main = $this->m_belum_bayar->get_data($id, $groupreg_in);
		// $config = $this->m_app->_get_config();
		$identitas = $this->m_app->_get_identitas();
		//
		$cek_non_bedah = $this->m_belum_bayar->cek_bedah_non_bedah($id, $main['pasien_id'], '01'); // tarif_id Prosedur Non Bedah
		$cek_bedah = $this->m_belum_bayar->cek_bedah_non_bedah($id, $main['pasien_id'], '02'); // tarif_id Prosedur Bedah
		$non = "";
		if ($cek_non_bedah > 0 && $cek_bedah > 0) {
			$non = "";
		} else {
			if ($cek_non_bedah > 0) {
				$non = " AND a.tarif_id != '02' ";
			} elseif ($cek_bedah > 0) {
				$non = " AND a.tarif_id != '01' ";
			} else {
				$non = " AND a.tarif_id != '02' ";
			}
		}

		$billing_rinc = $this->db->query(
			"SELECT 
				a.billingrinc_id, 
				a.billingrinc_id,
				a.billing_id,
				a.split_id,
				a.tarif_id,
				SUM(a.jml_seharusnya) as jml_seharusnya,
				SUM(a.jml_potongan) as jml_potongan,
				SUM(a.jml_tagihan) as jml_tagihan,
				a.data_tp,
				b.tarif_nm, b.invoice_order, b.invoice_group
			FROM dat_billing_rinc a 
			JOIN mst_tarif b ON a.tarif_id = b.tarif_id
			WHERE a.billing_id = '" . $main['billing_id'] . "' $non  
			GROUP BY b.invoice_group 
			ORDER BY b.invoice_order"
		)->result_array();

		// if ($cek_non_bedah > 0 && $cek_bedah > 0) {
		// 	$list_rincian = $this->m_belum_bayar->list_rincian($id, $main['billing_id'], $groupreg_in);
		// } else {
		// 	if ($cek_non_bedah > 0) {
		// 		$list_rincian = $this->m_belum_bayar->list_rincian($id, $main['billing_id'], $groupreg_in, '02');
		// 	} elseif ($cek_bedah > 0) {
		// 		$list_rincian = $this->m_belum_bayar->list_rincian($id, $main['billing_id'], $groupreg_in, '01');
		// 	} else {
		// 		$list_rincian = $this->m_belum_bayar->list_rincian($id, $main['billing_id'], $groupreg_in, '02');
		// 	}
		// }
		//
		$last = $this->m_belum_bayar->get_last();
		$get_billing = $this->m_belum_bayar->get_billing($id);
		if ($get_billing != null) {
			$no_invoice = $main['no_invoice'];
		} else {
			if ($last == null) {
				$no = 1;
			} else {
				$raw_no = explode('/', $last['no_invoice']);
				$no = intval($raw_no[0]) + 1;
			}
			$no_invoice = $no . '/INV/RSIAPMT-' . @$main['jenispasien_nm'] . '/' . to_rome(date('m')) . '/' . date('Y');
		}
		//generate pdf
		$profile = $this->m_profile->get_first();
		$this->load->library('pdf');
		$pdf = new Pdf('p', 'mm', array(210, 297));
		$pdf->AliasNbPages();
		$pdf->SetTitle('Cetak Invoice ' . $id);
		$pdf->AddPage();

		// $pdf->Image(FCPATH . 'assets/images/icon/pt-cahaya-permata-medika.png', 10, 10, 15, 15);
		// $pdf->SetFont('Arial', '', 9);
		// $pdf->Cell(0, 4, 'PT. CAHAYA PERMATA MEDIKA', 0, 1, 'C');
		// $pdf->SetFont('Arial', 'B', 11);
		// $pdf->Cell(0, 6, @$profile['title_logo_login'] . ' ' . @$profile['sub_title_logo_login'], 0, 1, 'C');
		// $pdf->SetFont('Arial', '', 7);
		// $pdf->Cell(0, 3, 'Jl. Mayjen Sutoyo No. 75 Purworejo Telp. (0275) 321031', 0, 1, 'C');
		// $pdf->Cell(0, 3, 'Email. rsiapermatapwr@gmail.com', 0, 1, 'C');
		// $pdf->Image(FCPATH . 'assets/images/icon/simrs-logo-rs.jpg', 184, 10, 15, 15);
		$pdf->Image(FCPATH . 'assets/images/icon/new-kop.jpg', 10, 6, 160, 20);
		$pdf->Cell(0, 16, '', 0, 1, 'C');
		$pdf->Line(10, 28, 200, 28);

		$pdf->Cell(0, 6, '', 0, 1, 'L');
		$pdf->SetFont('Arial', 'B', 14);
		$pdf->Cell(100, 4, 'INVOICE', 0, 0, 'L');
		$pdf->SetFont('Arial', 'B', 9);
		$pdf->Cell(90, 4, 'NO. FORM : ' . $no_invoice, 0, 1, 'R');
		$pdf->SetFont('Arial', 'B', 9);
		$pdf->Cell(0, 4, 'BIAYA PERAWATAN RAWAT INAP', 0, 1, 'L');
		$pdf->Cell(0, 2, '', 0, 1, 'C');

		//Identitas Pasien
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(30, 4, 'NO. RM', 0, 0, 'L');
		$pdf->Cell(60, 4, ': ' . strtoupper(@$main['pasien_id']), 0, 0, 'L');
		$pdf->Cell(30, 4, 'Kelas / Ruang', 0, 0, 'L');
		$pdf->Cell(70, 4, ': ' . strtoupper(@$main['kelas_nm']) . ' / ' . strtoupper($main['lokasi_nm']), 0, 1, 'L');

		$pdf->Cell(30, 4, 'Tanggal Masuk', 0, 0, 'L');
		$pdf->Cell(60, 4, ': ' . strtoupper(to_date_indo($main['tgl_registrasi'], 'date')), 0, 0, 'L');
		$pdf->Cell(30, 4, 'DPJP', 0, 0, 'L');
		$pdf->Cell(70, 4, ': ' . strtoupper(@$main['dokter_nm']), 0, 1, 'L');

		$pdf->Cell(30, 4, 'Tanggal Keluar', 0, 0, 'L');
		$pdf->Cell(60, 4, ': ' . strtoupper(to_date_indo($main['tgl_pulang'], 'date')), 0, 0, 'L');
		$pdf->Cell(30, 4, 'Spesialisasi', 0, 0, 'L');
		$pdf->Cell(70, 4, ': ' . strtoupper(@$main['spesialisasi_nm']), 0, 1, 'L');

		$pdf->Cell(30, 4, 'Nama Pasien', 0, 0, 'L');
		$pdf->Cell(60, 4, ': ' . strtoupper($main['pasien_nm'] . @$main['sebutan_cd']), 0, 0, 'L');
		$pdf->Cell(30, 4, 'Penjamin Bayar', 0, 0, 'L');
		if (@$main['jenispasien_id'] == '01') {
			$pdf->Cell(70, 4, ': ' . strtoupper(@$main['penjamin_nm']), 0, 1, 'L');
		} elseif (@$main['jenispasien_id'] == '02') {
			$pdf->Cell(70, 4, ': BPJS KESEHATAN', 0, 1, 'L');
		}

		$pdf->Cell(30, 4, 'Umur', 0, 0, 'L');
		$pdf->Cell(60, 4, ': ' . $main['umur_thn'] . ' TH', 0, 0, 'L');
		if (@$main['jenispasien_id'] == '02') {
			$pdf->Cell(30, 4, 'NO. SEP', 0, 0, 'L');
			$pdf->Cell(70, 4, ': ' . @$main['sep_no'], 0, 1, 'L');
		} else {
			$pdf->Cell(30, 4, '', 0, 0, 'L');
			$pdf->Cell(70, 4, '', 0, 1, 'L');
		}
		$pdf->Cell(0, 3, '', 0, 1, 'C');
		$pdf->Line(10, 63, 200, 63);

		$i = 1;
		$no = 1;
		$tot_jml_seharusnya = 0;
		$tot_jml_potongan = 0;
		$tot_jml_tagihan = 0;

		$jumlah_rincian = count($billing_rinc);

		foreach ($billing_rinc as $row) {
			$tot_jml_seharusnya += $row['jml_seharusnya'];
			$tot_jml_potongan += $row['jml_potongan'];
			$tot_jml_tagihan += $row['jml_tagihan'];

			if ($i == $jumlah_rincian) {
				$pdf->Cell(10, 4, $i++ . '. ', 0, 0, 'C');
				$pdf->Cell(135, 4, 'OBAT', 0, 0, 'L');
				$pdf->Cell(10, 4, 'Rp', 0, 0, 'L');
				$pdf->Cell(30, 4, (@$main['grand_jml_obat'] != '') ? num_id(@$main['grand_jml_obat']) . ',00' : '0,00', 0, 1, 'R');

				$pdf->Cell(10, 4, $i++ . '. ', 0, 0, 'C');
				$pdf->Cell(135, 4, 'ALKES', 0, 0, 'L');
				$pdf->Cell(10, 4, 'Rp', 0, 0, 'L');
				$pdf->Cell(30, 4, (@$main['grand_jml_alkes'] != '') ? num_id(@$main['grand_jml_alkes']) . ',00' : '0,00', 0, 1, 'R');

				$pdf->Cell(10, 4, $i++ . '. ', 0, 0, 'C');
				$pdf->Cell(135, 4, 'BHP', 0, 0, 'L');
				$pdf->Cell(10, 4, 'Rp', 0, 0, 'L');
				$pdf->Cell(30, 4, (@$main['grand_jml_bhp'] != '') ? num_id(@$main['grand_jml_bhp']) . ',00' : '0,00', 0, 1, 'R');
			}

			$pdf->Cell(10, 4, $i . '. ', 0, 0, 'C');
			$pdf->Cell(135, 4, $row['tarif_nm'], 0, 0, 'L');
			$pdf->Cell(10, 4, 'Rp', 0, 0, 'L');
			$pdf->Cell(30, 4, num_id($row['jml_tagihan']) . ',00', 0, 1, 'R');

			$i++;
		}

		// foreach ($list_rincian as $row) :
		// 	$tot_jml_seharusnya += $row['jml_seharusnya'];
		// 	$tot_jml_potongan += $row['jml_potongan'];
		// 	$tot_jml_tagihan += $row['jml_tagihan'];

		// 	if ($i == $jumlah_rincian) {
		// 		$pdf->Cell(10, 4, $i++ . '. ', 0, 0, 'C');
		// 		$pdf->Cell(135, 4, 'OBAT', 0, 0, 'L');
		// 		$pdf->Cell(10, 4, 'Rp', 0, 0, 'L');
		// 		$pdf->Cell(30, 4, (@$main['grand_jml_obat'] != '') ? num_id(@$main['grand_jml_obat']) . ',00' : '0,00', 0, 1, 'R');

		// 		$pdf->Cell(10, 4, $i++ . '. ', 0, 0, 'C');
		// 		$pdf->Cell(135, 4, 'ALKES', 0, 0, 'L');
		// 		$pdf->Cell(10, 4, 'Rp', 0, 0, 'L');
		// 		$pdf->Cell(30, 4, (@$main['grand_jml_alkes'] != '') ? num_id(@$main['grand_jml_alkes']) . ',00' : '0,00', 0, 1, 'R');

		// 		$pdf->Cell(10, 4, $i++ . '. ', 0, 0, 'C');
		// 		$pdf->Cell(135, 4, 'BHP', 0, 0, 'L');
		// 		$pdf->Cell(10, 4, 'Rp', 0, 0, 'L');
		// 		$pdf->Cell(30, 4, (@$main['grand_jml_bhp'] != '') ? num_id(@$main['grand_jml_bhp']) . ',00' : '0,00', 0, 1, 'R');
		// 	}

		// 	$pdf->Cell(10, 4, $i . '. ', 0, 0, 'C');
		// 	$pdf->Cell(135, 4, $row['tarif_nm'], 0, 0, 'L');
		// 	$pdf->Cell(10, 4, 'Rp', 0, 0, 'L');
		// 	$pdf->Cell(30, 4, num_id($row['jml_tagihan']) . ',00', 0, 1, 'R');

		// 	$i++;
		// endforeach;
		// $pdf->Cell(10, 4, $i++ . '. ', 0, 0, 'C');
		// $pdf->Cell(135, 4, 'OBAT', 0, 0, 'L');
		// $pdf->Cell(10, 4, 'Rp', 0, 0, 'L');
		// $pdf->Cell(30, 4, (@$main['grand_jml_obat'] != '') ? num_id(@$main['grand_jml_obat']) . ',00' : '0,00', 0, 1, 'R');

		// $pdf->Cell(10, 4, $i++ . '. ', 0, 0, 'C');
		// $pdf->Cell(135, 4, 'ALKES', 0, 0, 'L');
		// $pdf->Cell(10, 4, 'Rp', 0, 0, 'L');
		// $pdf->Cell(30, 4, (@$main['grand_jml_alkes'] != '') ? num_id(@$main['grand_jml_alkes']) . ',00' : '0,00', 0, 1, 'R');

		// $pdf->Cell(10, 4, $i++ . '. ', 0, 0, 'C');
		// $pdf->Cell(135, 4, 'BHP', 0, 0, 'L');
		// $pdf->Cell(10, 4, 'Rp', 0, 0, 'L');
		// $pdf->Cell(30, 4, (@$main['grand_jml_bhp'] != '') ? num_id(@$main['grand_jml_bhp']) . ',00' : '0,00', 0, 1, 'R');

		// $pdf->Cell(0,4,'',0,1,'C');

		$grand_total_tagihan = $tot_jml_tagihan + @$main['grand_jml_obat'] + @$main['grand_jml_alkes'] + @$main['grand_jml_bhp'];

		$pdf->Cell(145, 4, '', 0, 0, 'L');
		$pdf->Cell(45, 4, '------------------------------------- +', 0, 1, 'L');

		$pdf->SetFont('Arial', 'B', 9);
		// $pdf->Cell(0,4,'',0,1,'C');
		$pdf->Cell(60, 4, 'TOTAL BIAYA RUMAH SAKIT', 0, 0, 'L');
		$pdf->Cell(85, 4, '---------------------------------------->', 0, 0, 'L');
		$pdf->Cell(10, 4, 'Rp', 0, 0, 'L');
		if (@$main['status_ranap_cd'] == 'NK' && @$main['jenispasien_id'] == '02') {
			$pdf->Cell(30, 4, num_id(@$main['grand_total_bruto']) . ',00', 0, 1, 'R');
		} else {
			$pdf->Cell(30, 4, (@$main['grand_total_tagihan'] != '') ? num_id(@$main['grand_total_tagihan']) . ',00' : num_id($grand_total_tagihan) . ',00', 0, 1, 'R');
		}

		// kondisi ketika naik kelas
		if (@$main['status_ranap_cd'] == 'NK' && @$main['jenispasien_id'] == '02') {
			$expld_src_kelas_nm = explode(" ", @$main['src_kelas_nm']);
			if ($expld_src_kelas_nm[0] == 'KELAS') {
				$src_kelas_nm = @$main['src_kelas_nm'];
			} else {
				$src_kelas_nm = 'KELAS ' . @$main['src_kelas_nm'];
			}

			$expld_kelas_nm = explode(" ", @$main['kelas_nm']);
			if ($expld_kelas_nm[0] == 'KELAS') {
				$kelas_nm = @$main['kelas_nm'];
			} else {
				$kelas_nm = 'KELAS ' . @$main['kelas_nm'];
			}

			$pdf->Cell(145, 4, '', 0, 1, 'L');
			$pdf->Cell(145, 4, 'TOTAL KEKURANGAN BIAYA DIBAYAR PASIEN MENURUT TARIF INACBG', 0, 0, 'L');
			$pdf->Cell(10, 4, 'Rp', 'L,T,B', 0, 'L');
			$pdf->Cell(30, 4, (@$main['grand_total_tagihan'] != '') ? num_id(@$main['grand_total_tagihan']) . ',00' : num_id($grand_total_tagihan) . ',00', 'R,T,B', 1, 'R');
			$pdf->Cell(145, 4, '( NAIK ' . $src_kelas_nm . ' MENJADI ' . $kelas_nm . ' )', 0, 1, 'L');
		}

		if (@$main['grand_total_tagihan'] != 0) {
			$pdf->Cell(0, 6, '', 0, 1, 'C');
			$pdf->SetFont('Arial', 'IB', 9);
			$pdf->Cell(60, 3, (@$main['grand_total_tagihan'] != '') ? '( TERBILANG : ' . strtoupper(terbilang(@$main['grand_total_tagihan'])) . ' RUPIAH )' : '( TERBILANG : ' . strtoupper(terbilang(@$grand_total_tagihan)) . ' RUPIAH )', 0, 0, 'L');
		}
		// petugas
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(0, 10, '', 0, 1, 'C');
		$pdf->Cell(120, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, 'PURWOREJO, ' . strtoupper(to_date_indo(date('Y-m-d'))), 0, 1, 'C');
		$pdf->Cell(120, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, 'BAGIAN KEUANGAN', 0, 1, 'C');
		$pdf->Cell(0, 15, '', 0, 1, 'C');
		$pdf->Cell(120, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, @$this->session->userdata('sess_user_realname'), 0, 1, 'C');

		$pdf->Output('I', 'Invoice_' . $id . '_' . date('Ymdhis') . '.pdf');
	}

	public function cetak_invoice_old($id = null)
	{
		$groupreg_in = $this->input->get('groupreg_in');
		$main = $this->m_belum_bayar->get_data($id, $groupreg_in);
		// $config = $this->m_app->_get_config();
		$identitas = $this->m_app->_get_identitas();
		//
		$cek_non_bedah = $this->m_belum_bayar->cek_bedah_non_bedah($id, $main['pasien_id'], '01'); // tarif_id Prosedur Non Bedah
		$cek_bedah = $this->m_belum_bayar->cek_bedah_non_bedah($id, $main['pasien_id'], '02'); // tarif_id Prosedur Bedah

		if ($cek_non_bedah > 0 && $cek_bedah > 0) {
			$list_rincian = $this->m_belum_bayar->list_rincian($id, $main['billing_id'], $groupreg_in);
		} else {
			if ($cek_non_bedah > 0) {
				$list_rincian = $this->m_belum_bayar->list_rincian($id, $main['billing_id'], $groupreg_in, '02');
			} elseif ($cek_bedah > 0) {
				$list_rincian = $this->m_belum_bayar->list_rincian($id, $main['billing_id'], $groupreg_in, '01');
			} else {
				$list_rincian = $this->m_belum_bayar->list_rincian($id, $main['billing_id'], $groupreg_in, '02');
			}
		}
		//
		$last = $this->m_belum_bayar->get_last();
		$get_billing = $this->m_belum_bayar->get_billing($id);
		if ($get_billing != null) {
			$no_invoice = $main['no_invoice'];
		} else {
			if ($last == null) {
				$no = 1;
			} else {
				$raw_no = explode('/', $last['no_invoice']);
				$no = intval($raw_no[0]) + 1;
			}
			$no_invoice = $no . '/INV/RSIAPMT-' . @$main['jenispasien_nm'] . '/' . to_rome(date('m')) . '/' . date('Y');
		}
		//generate pdf
		$profile = $this->m_profile->get_first();
		$this->load->library('pdf');
		$pdf = new Pdf('p', 'mm', array(210, 297));
		$pdf->AliasNbPages();
		$pdf->SetTitle('Cetak Invoice ' . $id);
		$pdf->AddPage();

		$pdf->Image(FCPATH . 'assets/images/icon/pt-cahaya-permata-medika.png', 10, 10, 15, 15);
		$pdf->SetFont('Arial', '', 9);
		$pdf->Cell(0, 4, 'PT. CAHAYA PERMATA MEDIKA', 0, 1, 'C');
		$pdf->SetFont('Arial', 'B', 11);
		$pdf->Cell(0, 6, @$profile['title_logo_login'] . ' ' . @$profile['sub_title_logo_login'], 0, 1, 'C');
		$pdf->SetFont('Arial', '', 7);
		$pdf->Cell(0, 3, 'Jl. Mayjen Sutoyo No. 75 Purworejo Telp. (0275) 321031', 0, 1, 'C');
		$pdf->Cell(0, 3, 'Email. rsiapermatapwr@gmail.com', 0, 1, 'C');
		$pdf->Image(FCPATH . 'assets/images/icon/simrs-logo-rs.jpg', 184, 10, 15, 15);
		$pdf->Line(10, 28, 200, 28);

		$pdf->Cell(0, 6, '', 0, 1, 'L');
		$pdf->SetFont('Arial', 'B', 14);
		$pdf->Cell(100, 4, 'INVOICE', 0, 0, 'L');
		$pdf->SetFont('Arial', 'B', 9);
		$pdf->Cell(90, 4, 'NO. FORM : ' . $no_invoice, 0, 1, 'R');
		$pdf->SetFont('Arial', 'B', 9);
		$pdf->Cell(0, 4, 'BIAYA PERAWATAN RAWAT INAP', 0, 1, 'L');
		$pdf->Cell(0, 2, '', 0, 1, 'C');

		//Identitas Pasien
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(30, 4, 'NO. RM', 0, 0, 'L');
		$pdf->Cell(60, 4, ': ' . strtoupper(@$main['pasien_id']), 0, 0, 'L');
		$pdf->Cell(30, 4, 'Kelas / Ruang', 0, 0, 'L');
		$pdf->Cell(70, 4, ': ' . strtoupper(@$main['kelas_nm']) . ' / ' . strtoupper($main['lokasi_nm']), 0, 1, 'L');

		$pdf->Cell(30, 4, 'Tanggal Masuk', 0, 0, 'L');
		$pdf->Cell(60, 4, ': ' . strtoupper(to_date_indo($main['tgl_registrasi'], 'date')), 0, 0, 'L');
		$pdf->Cell(30, 4, 'DPJP', 0, 0, 'L');
		$pdf->Cell(70, 4, ': ' . strtoupper(@$main['dokter_nm']), 0, 1, 'L');

		$pdf->Cell(30, 4, 'Tanggal Keluar', 0, 0, 'L');
		$pdf->Cell(60, 4, ': ' . strtoupper(to_date_indo($main['tgl_pulang'], 'date')), 0, 0, 'L');
		$pdf->Cell(30, 4, 'Spesialisasi', 0, 0, 'L');
		$pdf->Cell(70, 4, ': ' . strtoupper(@$main['spesialisasi_nm']), 0, 1, 'L');

		$pdf->Cell(30, 4, 'Nama Pasien', 0, 0, 'L');
		$pdf->Cell(60, 4, ': ' . strtoupper($main['pasien_nm'] . @$main['sebutan_cd']), 0, 0, 'L');
		$pdf->Cell(30, 4, 'Penjamin Bayar', 0, 0, 'L');
		if (@$main['jenispasien_id'] == '01') {
			$pdf->Cell(70, 4, ': ' . strtoupper(@$main['penjamin_nm']), 0, 1, 'L');
		} elseif (@$main['jenispasien_id'] == '02') {
			$pdf->Cell(70, 4, ': BPJS KESEHATAN', 0, 1, 'L');
		}

		$pdf->Cell(30, 4, 'Umur', 0, 0, 'L');
		$pdf->Cell(60, 4, ': ' . $main['umur_thn'] . ' TH', 0, 0, 'L');
		if (@$main['jenispasien_id'] == '02') {
			$pdf->Cell(30, 4, 'NO. SEP', 0, 0, 'L');
			$pdf->Cell(70, 4, ': ' . @$main['sep_no'], 0, 1, 'L');
		} else {
			$pdf->Cell(30, 4, '', 0, 0, 'L');
			$pdf->Cell(70, 4, '', 0, 1, 'L');
		}
		$pdf->Cell(0, 3, '', 0, 1, 'C');
		$pdf->Line(10, 63, 200, 63);

		$i = 1;
		$no = 1;
		$tot_jml_seharusnya = 0;
		$tot_jml_potongan = 0;
		$tot_jml_tagihan = 0;
		foreach ($list_rincian as $row) :
			$tot_jml_seharusnya += $row['jml_seharusnya'];
			$tot_jml_potongan += $row['jml_potongan'];
			$tot_jml_tagihan += $row['jml_tagihan'];

			$pdf->Cell(10, 4, $i . '. ', 0, 0, 'C');
			$pdf->Cell(135, 4, $row['tarif_nm'], 0, 0, 'L');
			$pdf->Cell(10, 4, 'Rp', 0, 0, 'L');
			$pdf->Cell(30, 4, num_id($row['jml_tagihan']) . ',00', 0, 1, 'R');

			$i++;
		endforeach;
		$pdf->Cell(10, 4, $i++ . '. ', 0, 0, 'C');
		$pdf->Cell(135, 4, 'OBAT', 0, 0, 'L');
		$pdf->Cell(10, 4, 'Rp', 0, 0, 'L');
		$pdf->Cell(30, 4, (@$main['grand_jml_obat'] != '') ? num_id(@$main['grand_jml_obat']) . ',00' : '0,00', 0, 1, 'R');

		$pdf->Cell(10, 4, $i++ . '. ', 0, 0, 'C');
		$pdf->Cell(135, 4, 'ALKES', 0, 0, 'L');
		$pdf->Cell(10, 4, 'Rp', 0, 0, 'L');
		$pdf->Cell(30, 4, (@$main['grand_jml_alkes'] != '') ? num_id(@$main['grand_jml_alkes']) . ',00' : '0,00', 0, 1, 'R');

		$pdf->Cell(10, 4, $i++ . '. ', 0, 0, 'C');
		$pdf->Cell(135, 4, 'BHP', 0, 0, 'L');
		$pdf->Cell(10, 4, 'Rp', 0, 0, 'L');
		$pdf->Cell(30, 4, (@$main['grand_jml_bhp'] != '') ? num_id(@$main['grand_jml_bhp']) . ',00' : '0,00', 0, 1, 'R');

		// $pdf->Cell(0,4,'',0,1,'C');

		$grand_total_tagihan = $tot_jml_tagihan + @$main['grand_jml_obat'] + @$main['grand_jml_alkes'] + @$main['grand_jml_bhp'];

		$pdf->Cell(145, 4, '', 0, 0, 'L');
		$pdf->Cell(45, 4, '------------------------------------- +', 0, 1, 'L');

		$pdf->SetFont('Arial', 'B', 9);
		// $pdf->Cell(0,4,'',0,1,'C');
		$pdf->Cell(60, 4, 'TOTAL BIAYA RUMAH SAKIT', 0, 0, 'L');
		$pdf->Cell(85, 4, '---------------------------------------->', 0, 0, 'L');
		$pdf->Cell(10, 4, 'Rp', 0, 0, 'L');
		$pdf->Cell(30, 4, (@$main['grand_total_tagihan'] != '') ? num_id(@$main['grand_total_tagihan']) . ',00' : num_id($grand_total_tagihan) . ',00', 0, 1, 'R');

		$pdf->Cell(0, 3, '', 0, 1, 'C');
		$pdf->SetFont('Arial', 'I', 8);
		$pdf->Cell(60, 3, (@$main['grand_total_tagihan'] != '') ? '(TERBILANG : ' . strtoupper(terbilang(@$main['grand_total_tagihan'])) . ')' : '(TERBILANG : ' . strtoupper(terbilang(@$grand_total_tagihan)) . ')', 0, 0, 'L');
		// petugas
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(0, 4, '', 0, 1, 'C');
		$pdf->Cell(120, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, 'PURWOREJO, ' . strtoupper(to_date_indo(date('Y-m-d'))), 0, 1, 'C');
		$pdf->Cell(120, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, 'BAGIAN KEUANGAN', 0, 1, 'C');
		$pdf->Cell(0, 15, '', 0, 1, 'C');
		$pdf->Cell(120, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, @$this->session->userdata('sess_user_realname'), 0, 1, 'C');

		$pdf->Output('I', 'Invoice_' . $id . '_' . date('Ymdhis') . '.pdf');
	}

	public function add_item_pembayaran($groupreg_id = null, $kelas_id = null)
	{
		$data['groupreg_in'] = $this->input->get('groupreg_in');
		if ($groupreg_id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_belum_bayar->get_data($groupreg_id, $data['groupreg_in']);
		}
		$data['groupreg_id'] = $groupreg_id;
		$data['kelas_id'] = $kelas_id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save_item_pembayaran/' . $groupreg_id . '?groupreg_in=' . $data['groupreg_in'];

		echo json_encode(array(
			'html' => $this->load->view('keuangan/kasir/belum_bayar/add_item_pembayaran', $data, true)
		));
	}

	public function detail_item_pembayaran($groupreg_id = null, $tarif_id = null)
	{
		$data['groupreg_in'] = $this->input->get('groupreg_in');
		$data['main'] = $this->m_belum_bayar->detail_item_pembayaran($groupreg_id, $tarif_id, $data['groupreg_in']);
		$data['groupreg_id'] = $groupreg_id;
		$data['tarif_id'] = $tarif_id;
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('keuangan/kasir/belum_bayar/detail_item_pembayaran', $data, true)
		));
	}

	public function detail_obat($groupreg_id = null)
	{
		$data['groupreg_in'] = $this->input->get('groupreg_in');
		$data['main'] = $this->m_belum_bayar->get_dat_resep($data['groupreg_in']);
		$data['last_resep'] = $this->m_belum_bayar->get_last_resep($data['groupreg_in']);
		$data['detail_obat'] = $this->m_belum_bayar->detail_obat($groupreg_id, $data['groupreg_in']);
		$data['groupreg_id'] = $groupreg_id;
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('keuangan/kasir/belum_bayar/detail_obat', $data, true)
		));
	}

	public function form_obat($reg_id = null, $resep_id = null, $farmasi_id = null)
	{
		if ($farmasi_id == null) {
			$data['detail'] = array();
		} else {
			$data['detail'] = $this->m_belum_bayar->farmasi_get($farmasi_id);
		}
		$data['nav'] = $this->nav;
		if ($resep_id == null) {
			$data['resep'] = array();
		} else {
			$data['resep'] = $this->m_belum_bayar->resep_get($resep_id);
		}
		$data['reg'] = $this->m_belum_bayar->get_registrasi($reg_id);
		$data['reg_id'] = $reg_id;
		$data['resep_id'] = $resep_id;
		$data['farmasi_id'] = $farmasi_id;
		// $data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/farmasi_save/' . $farmasi_id;

		echo json_encode(array(
			'html' => $this->load->view('keuangan/kasir/belum_bayar/form_obat', $data, true)
		));
	}

	public function ajax_farmasi($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_belum_bayar->farmasi_save();
		}

		if ($type == 'autocomplete') {
			$obat_nm = $this->input->get('obat_nm');
			$map_lokasi_depo = $this->input->get('map_lokasi_depo');
			$res = $this->m_belum_bayar->obat_autocomplete_list($obat_nm, $map_lokasi_depo);
			echo json_encode($res);
		}

		if ($type == 'search_obat') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('keuangan/kasir/belum_bayar/search_obat', $data, true)
			));
		}

		if ($type == 'search_data') {
			$map_lokasi_depo = $this->input->post('map_lokasi_depo');
			$list = $this->m_dt_obat->get_datatables($map_lokasi_depo);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['no_batch'];
				$row[] = $field['obat_nm'];
				$row[] = ($field['satuan_cd'] != '') ? get_parameter_value('satuan_cd', $field['satuan_cd']) : '-';
				$row[] = ($field['tgl_expired'] != '0000-00-00') ? to_date($field['tgl_expired'], '-', 'date', ' ') : '-';
				$row[] = num_id($field['harga_jual']);
				$row[] = num_id($field['stok_akhir']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="obat_fill(' . "'" . $field['obat_id'] . "'" . ',' . "'" . $field['stokdepo_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_obat->count_all($map_lokasi_depo),
				"recordsFiltered" => $this->m_dt_obat->count_filtered($map_lokasi_depo),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'obat_fill') {
			$data = $this->input->post();
			$res = $this->m_belum_bayar->obat_list_row($data['obat_id'], $data['stokdepo_id']);
			echo json_encode($res);
		}

		if ($type == 'obat_get') {
			$res = $this->m_belum_bayar->obat_get();
			echo json_encode($res);
		}

		if ($type == 'delete_data') {
			$farmasi_id = $this->input->post('farmasi_id');
			$this->m_belum_bayar->farmasi_delete($farmasi_id);

			$output = array(
				"status" => 'success',
			);
			echo json_encode($output);
		}

		if ($type == 'grand') {
			$resep_id = $this->input->post('resep_id');
			echo json_encode($this->m_resep->get_data($resep_id));
		}
	}

	public function detail_alkes($groupreg_id = null)
	{
		$data['groupreg_in'] = $this->input->get('groupreg_in');
		$data['main'] = $this->m_belum_bayar->detail_alkes($groupreg_id, $data['groupreg_in']);
		$data['groupreg_id'] = $groupreg_id;
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('keuangan/kasir/belum_bayar/detail_alkes', $data, true)
		));
	}

	public function form_alkes($reg_id = null, $alkes_id = null)
	{
		if ($alkes_id == null) {
			$data['detail'] = array();
		} else {
			$data['detail'] = $this->m_belum_bayar->alkes_get($alkes_id, $reg_id);
		}
		$data['nav'] = $this->nav;
		$data['reg'] = $this->m_belum_bayar->get_registrasi($reg_id);
		$data['reg_id'] = $reg_id;
		$data['alkes_id'] = $alkes_id;

		echo json_encode(array(
			'html' => $this->load->view('keuangan/kasir/belum_bayar/form_alkes', $data, true)
		));
	}

	public function ajax_alkes($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_belum_bayar->alkes_save();
		} elseif ($type == 'autocomplete') {
			$obat_nm = $this->input->get('obat_nm');
			$map_lokasi_depo = $this->input->get('map_lokasi_depo');
			$res = $this->m_belum_bayar->alkes_autocomplete($obat_nm, $map_lokasi_depo);
			echo json_encode($res);
		} elseif ($type == 'get_data') {
			$alkes_id = $this->input->post('alkes_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_belum_bayar->alkes_get($alkes_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$alkes_id = $this->input->post('alkes_id');
			$reg_id = $this->input->post('reg_id');

			$this->m_belum_bayar->alkes_delete($alkes_id, $reg_id);

			$output = array(
				"status" => 'success',
			);
			echo json_encode($output);
		} elseif ($type == 'search_alkes') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('keuangan/kasir/belum_bayar/search_alkes', $data, true)
			));
		} elseif ($type == 'search_data') {
			$map_lokasi_depo = $this->input->post('map_lokasi_depo');
			$list = $this->m_dt_alkes->get_datatables($map_lokasi_depo);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['no_batch'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = to_date($field['tgl_expired'], '-', 'date', ' ');
				$row[] = num_id($field['stok_akhir']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="alkes_fill(' . "'" . $field['obat_id'] . "'" . ',' . "'" . $field['stokdepo_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_alkes->count_all($map_lokasi_depo),
				"recordsFiltered" => $this->m_dt_alkes->count_filtered($map_lokasi_depo),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'alkes_fill') {
			$data = $this->input->post();
			$res = $this->m_belum_bayar->alkes_row($data);
			echo json_encode($res);
		}
	}

	public function detail_bhp($groupreg_id = null)
	{
		$data['groupreg_in'] = $this->input->get('groupreg_in');
		$data['main'] = $this->m_belum_bayar->detail_bhp($groupreg_id, $data['groupreg_in']);
		$data['groupreg_id'] = $groupreg_id;
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('keuangan/kasir/belum_bayar/detail_bhp', $data, true)
		));
	}

	public function form_bhp($reg_id = null, $bhp_id = null)
	{
		if ($bhp_id == null) {
			$data['detail'] = array();
		} else {
			$data['detail'] = $this->m_belum_bayar->bhp_get($bhp_id, $reg_id);
		}
		$data['nav'] = $this->nav;
		$data['reg'] = $this->m_belum_bayar->get_registrasi($reg_id);
		$data['reg_id'] = $reg_id;
		$data['bhp_id'] = $bhp_id;

		echo json_encode(array(
			'html' => $this->load->view('keuangan/kasir/belum_bayar/form_bhp', $data, true)
		));
	}

	public function ajax_bhp($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_belum_bayar->bhp_save();
		} elseif ($type == 'autocomplete') {
			$obat_nm = $this->input->get('obat_nm');
			$map_lokasi_depo = $this->input->get('map_lokasi_depo');
			$res = $this->m_belum_bayar->bhp_autocomplete($obat_nm, $map_lokasi_depo);
			echo json_encode($res);
		} elseif ($type == 'get_data') {
			$bhp_id = $this->input->post('bhp_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_belum_bayar->bhp_get($bhp_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$bhp_id = $this->input->post('bhp_id');
			$reg_id = $this->input->post('reg_id');

			$this->m_belum_bayar->bhp_delete($bhp_id, $reg_id);

			$output = array(
				"status" => 'success',
			);
			echo json_encode($output);
		} elseif ($type == 'search_bhp') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('keuangan/kasir/belum_bayar/search_bhp', $data, true)
			));
		} elseif ($type == 'search_data') {
			$map_lokasi_depo = $this->input->post('map_lokasi_depo');
			$list = $this->m_dt_bhp->get_datatables($map_lokasi_depo);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['no_batch'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = to_date($field['tgl_expired'], '-', 'date', ' ');
				$row[] = num_id($field['stok_akhir']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="bhp_fill(' . "'" . $field['obat_id'] . "'" . ',' . "'" . $field['stokdepo_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_bhp->count_all($map_lokasi_depo),
				"recordsFiltered" => $this->m_dt_bhp->count_filtered($map_lokasi_depo),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'bhp_fill') {
			$data = $this->input->post();
			$res = $this->m_belum_bayar->bhp_row($data);
			echo json_encode($res);
		}
	}

	public function save_item_pembayaran($groupreg_id = null)
	{
		$groupreg_in = $this->input->get('groupreg_in');
		$this->m_belum_bayar->tindakan_save($groupreg_id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $groupreg_id . '?groupreg_in=' . $groupreg_in);
	}

	function ajax($type = null, $id = null)
	{
		if ($type == 'get_lokasi_pelayanan') {
			$lokasi_kasir = $this->input->post('lokasi_kasir');
			$lokasi_pelayanan = $this->input->post('lokasi_pelayanan');
			$list_lokasi_pelayanan = $this->m_lokasi->list_lokasi_pelayanan($lokasi_kasir);
			//
			$html = '';
			$html .= '<select name="lokasi_pelayanan" id="lokasi_pelayanan" class="chosen-select custom-select w-100">';
			if (count($list_lokasi_pelayanan) > 1 || count($list_lokasi_pelayanan) == 0) {
				$html .= '<option value="">- Semua -</option>';
			}
			foreach ($list_lokasi_pelayanan as $lokasi) {
				if (@$lokasi_pelayanan == $lokasi['lokasi_id']) {
					$html .= '<option value="' . $lokasi['lokasi_id'] . '" selected>' . $lokasi['lokasi_nm'] . '</option>';
				} else {
					$html .= '<option value="' . $lokasi['lokasi_id'] . '">' . $lokasi['lokasi_nm'] . '</option>';
				}
			}
			$html .= '</select>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}

		if ($type == 'list_belum_bayar') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('keuangan/kasir/belum_bayar/list_belum_bayar', $data, true)
			));
		}

		if ($type == 'search_belum_bayar') {
			$nav = $this->nav;
			$list = $this->m_dt_belum_bayar->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;

				if ($field['bayar_st'] == 1) {
					$bayar_st = '<div class="badge badge-xs badge-primary">Sudah</div>';
				} elseif ($field['bayar_st'] == 2) {
					$bayar_st = '<div class="badge badge-xs badge-warning blink">Pending</div>';
				} else {
					$bayar_st = '<div class="badge badge-xs badge-danger blink">Belum</div>';
				}

				$alamat = '';
				if ($field['alamat'] != '') {
					$alamat .= $field['alamat'] . ', ';
				} else {
					$alamat .= '';
				}

				if ($field['kelurahan'] != '') {
					$alamat .= ucwords(strtolower($field['kelurahan'])) . ', ';
				} else {
					$alamat .= '';
				}

				if ($field['kecamatan'] != '') {
					$alamat .= ucwords(strtolower($field['kecamatan'])) . ', ';
				} else {
					$alamat .= '';
				}

				if ($field['kabupaten'] != '') {
					$alamat .= ucwords(strtolower($field['kabupaten'])) . ', ';
				} else {
					$alamat .= '';
				}

				if ($field['provinsi'] != '') {
					$alamat .= ucwords(strtolower($field['provinsi'])) . ', ';
				} else {
					$alamat .= '';
				}

				$row = array();
				$row[] = $no;
				$row[] = $field['pasien_id'];
				$row[] = $field['pasien_nm'] . ', ' . $field['sebutan_cd'];
				$row[] = $alamat;
				$row[] = $field['lokasi_nm'];
				$row[] = $field['jenispasien_nm'];
				$row[] = to_date($field['tgl_registrasi'], '', 'full_date', '<br>');
				$row[] = $bayar_st;
				$row[] = '<a href="' . site_url() . '/' . $nav['nav_url'] . '/form/' . $field['groupreg_id'] . '?groupreg_in=' . $field['groupreg_in'] . '" class="btn btn-xs btn-primary pt-1 pb-1">Pilih >></a>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_belum_bayar->count_all(),
				"recordsFiltered" => $this->m_dt_belum_bayar->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'rincian_pembayaran') {
			$id = $this->input->post('id');
			$groupreg_in = $this->input->post('groupreg_in');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_belum_bayar->get_data_pembayaran($id, $groupreg_in);
			$data['cek_non_bedah'] = $this->m_belum_bayar->cek_bedah_non_bedah($id, $data['main']['pasien_id'], '01'); // tarif_id Prosedur Non Bedah
			$data['cek_bedah'] = $this->m_belum_bayar->cek_bedah_non_bedah($id, $data['main']['pasien_id'], '02'); // tarif_id Prosedur Bedah
			$data['id'] = $id;
			$data['groupreg_in'] = $groupreg_in;

			if ($data['cek_non_bedah'] > 0 && $data['cek_bedah'] > 0) {
				$data['list_rincian'] = $this->m_belum_bayar->list_rincian($id, $data['main']['billing_id'], $groupreg_in);
			} else {
				if ($data['cek_non_bedah'] > 0) {
					$data['list_rincian'] = $this->m_belum_bayar->list_rincian($id, $data['main']['billing_id'], $groupreg_in, '02');
				} elseif ($data['cek_bedah'] > 0) {
					$data['list_rincian'] = $this->m_belum_bayar->list_rincian($id, $data['main']['billing_id'], $groupreg_in, '01');
				} else {
					$data['list_rincian'] = $this->m_belum_bayar->list_rincian($id, $data['main']['billing_id'], $groupreg_in, '02');
				}
			}

			echo json_encode(array(
				'html' => $this->load->view('keuangan/kasir/belum_bayar/rincian_pembayaran', $data, true)
			));
		}

		if ($type == 'rincian_pembayaran_parsial') {
			$id = $this->input->post('id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_belum_bayar->list_billing_parsial($id, $pasien_id);

			echo json_encode(array(
				'html' => $this->load->view('keuangan/kasir/belum_bayar/rincian_pembayaran_parsial', $data, true)
			));
		}
	}

	// Tindakan
	public function ajax_tindakan($type = null, $id = null)
	{
		if ($type == 'tarifkelas_autocomplete') {
			$tarif_nm = $this->input->get('tarif_nm');
			$res = $this->m_belum_bayar->tarifkelas_autocomplete($tarif_nm);
			echo json_encode($res);
		} elseif ($type == 'petugas_autocomplete') {
			$petugas_nm = $this->input->get('petugas_nm');
			$res = $this->m_belum_bayar->petugas_autocomplete($petugas_nm);
			echo json_encode($res);
		} elseif ($type == 'get_data') {
			$tindakan_id = $this->input->post('tindakan_id');
			$groupreg_id = $this->input->post('groupreg_id');
			$pasien_id = $this->input->post('pasien_id');

			$main = $this->m_belum_bayar->tindakan_get($tindakan_id, $groupreg_id, $pasien_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'search_tarifkelas') {
			$data['nav'] = $this->nav;
			$data['kelas_id'] = $id;

			echo json_encode(array(
				'html' => $this->load->view('keuangan/kasir/belum_bayar/search_tarifkelas', $data, true)
			));
		} elseif ($type == 'search_tarifkelas_data') {
			$list = $this->m_dt_tarifkelas->get_datatables($id);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['tarifkelas_id'];
				$row[] = $field['tarif_nm'];
				$row[] = $field['kelas_nm'];
				$row[] = num_id($field['nominal']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="tarifkelas_fill(' . "'" . $field['tarifkelas_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_tarifkelas->count_all($id),
				"recordsFiltered" => $this->m_dt_tarifkelas->count_filtered($id),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'tarifkelas_fill') {
			$data = $this->input->post();
			$res = $this->m_belum_bayar->tarifkelas_row($data['tarifkelas_id']);
			echo json_encode($res);
		} elseif ($type == 'search_petugas') {
			$data['nav'] = $this->nav;
			$data['form_name'] = @$id;

			echo json_encode(array(
				'html' => $this->load->view('keuangan/kasir/belum_bayar/search_petugas', $data, true)
			));
		} elseif ($type == 'search_petugas_data') {
			$list = $this->m_dt_petugas->get_datatables();
			$form_name = $this->input->post('form_name');
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['pegawai_id'];
				$row[] = $field['pegawai_nm'];
				$row[] = $field['pegawai_nip'];
				$row[] = get_parameter_value('jenispegawai_cd', $field['jenispegawai_cd']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="petugas_fill(' . "'" . $field['pegawai_id'] . "'" . ',' . "'" . $form_name . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_petugas->count_all(),
				"recordsFiltered" => $this->m_dt_petugas->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'petugas_fill') {
			$data = $this->input->post();
			$res = $this->m_belum_bayar->petugas_row($data['pegawai_id']);
			echo json_encode($res);
		} else if ($type == 'add_petugas') {
			$tindakan_id = $this->input->get('tindakan_id');
			$data['nav'] = $this->nav;

			$html = '';
			if ($tindakan_id != '') {
				$petugas_no = $this->input->get('petugas_no');
				$get_tindakan = $this->m_belum_bayar->get_tindakan($tindakan_id);
				$data['tindakan_id'] = $tindakan_id;
				$data['petugas_no_post'] = $petugas_no;
				$data['type'] = 'edit';

				if ($get_tindakan['petugas_id'] != '') {
					$data['petugas_no'] = '';
					$data['form_name'] = 'petugas_id';
					$data['pegawai_id'] = $get_tindakan['petugas_id'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_1'];

					$html .= $this->load->view('keuangan/kasir/belum_bayar/add_petugas', $data, true);
				}
				if ($get_tindakan['petugas_id_2'] != '') {
					$data['petugas_no'] = '2';
					$data['form_name'] = 'petugas_id_2';
					$data['pegawai_id'] = $get_tindakan['petugas_id_2'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_2'];

					$html .= $this->load->view('keuangan/kasir/belum_bayar/add_petugas', $data, true);
				}
				if ($get_tindakan['petugas_id_3'] != '') {
					$data['petugas_no'] = '3';
					$data['form_name'] = 'petugas_id_3';
					$data['pegawai_id'] = $get_tindakan['petugas_id_3'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_3'];

					$html .= $this->load->view('keuangan/kasir/belum_bayar/add_petugas', $data, true);
				}
				if ($get_tindakan['petugas_id_4'] != '') {
					$data['petugas_no'] = '4';
					$data['form_name'] = 'petugas_id_4';
					$data['pegawai_id'] = $get_tindakan['petugas_id_4'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_4'];

					$html .= $this->load->view('keuangan/kasir/belum_bayar/add_petugas', $data, true);
				}
				if ($get_tindakan['petugas_id_5'] != '') {
					$data['petugas_no'] = '5';
					$data['form_name'] = 'petugas_id_5';
					$data['pegawai_id'] = $get_tindakan['petugas_id_5'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_5'];

					$html .= $this->load->view('keuangan/kasir/belum_bayar/add_petugas', $data, true);
				}
			} else {
				$petugas_no = $this->input->get('petugas_no') + 1;
				$data['petugas_no'] = ($petugas_no == '1') ? '' : $petugas_no;
				$data['form_name'] = ($petugas_no == '1') ? 'petugas_id' : 'petugas_id_' . $petugas_no;
				$data['petugas_no_post'] = $petugas_no;
				$data['type'] = 'add';

				$html .= $this->load->view('keuangan/kasir/belum_bayar/add_petugas', $data, true);
			}

			echo json_encode(array(
				'html' => $html,
				'petugas_no' => $petugas_no,
			));
		} elseif ($type == 'delete_petugas') {
			$tindakan_id = $this->input->post('tindakan_id');
			$form_name = $this->input->post('form_name');
			//
			$callback = 'true';
			//
			echo json_encode(array(
				'callback' => $callback
			));
		}
	}

	public function form_parsial($billing_id, $tarif_id)
	{
		$data['main'] = $this->m_belum_bayar->item_pembayaran($billing_id, $tarif_id);
		$data['nav'] = $this->nav;
		$data['billing_id'] = $billing_id;
		$data['tarif_id'] = $tarif_id;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/split_save/';

		echo json_encode(array(
			'html' => $this->load->view('keuangan/kasir/belum_bayar/form_parsial', $data, true)
		));
	}

	public function delete_tindakan($tarif_id, $groupreg_id)
	{
		$groupreg_in = $this->input->get('groupreg_in');
		//
		$this->m_belum_bayar->delete_tindakan($tarif_id, $groupreg_id);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $groupreg_id . '?groupreg_in=' . $groupreg_in);
	}

	public function ajax_split($id = '')
	{
		if ($id == 'kembalian') {
			$data = $this->input->post();
			$data['grand_total_kembalian'] = clear_numeric($data['grand_total_kembalian']);
			if ($data['grand_total_kembalian'] < 0) {
				echo 'false';
			} else {
				echo 'true';
			}
		}
	}

	public function split_save()
	{
		$data = $this->input->post();
		$data['grand_total_bruto'] = clear_numeric($data['grand_total_bruto']);
		$data['grand_total_potongan'] = clear_numeric($data['grand_total_potongan']);
		$data['grand_total_netto'] = clear_numeric($data['grand_total_netto']);
		$data['grand_pembulatan'] = clear_numeric($data['grand_pembulatan']);
		$data['grand_total_tagihan'] = clear_numeric($data['grand_total_tagihan']);
		$data['grand_total_bayar'] = clear_numeric($data['grand_total_bayar']);
		$data['grand_total_kembalian'] = clear_numeric($data['grand_total_kembalian']);

		//create new billing
		$this->m_belum_bayar->split_save($data);
		$cur_billing = $this->db->where('billing_id', $data['billing_id'])->get('dat_billing')->row_array();
		$cur_reg = $this->db->where('groupreg_id', $cur_billing['groupreg_id'])->get('reg_pasien')->row_array();

		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/' . $cur_reg['reg_id'] . '?groupreg_in=' . $cur_reg['groupreg_in']);
	}
}
