<?php 
if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Piutang extends MY_Controller{

	var $nav_id = '07.01.02', $nav, $cookie;
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array(
			'kasir/m_piutang',
			'master/m_lokasi'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
    $this->cookie = get_cookie_nav($this->nav_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('tgl_registrasi_from' => '', 'tgl_registrasi_to' => '', 'no_rm_nm' => '', 'lokasi_id' => '', 'jenisreg_st' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'reg_id','type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}
	
	public function index() {	
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(4, 0);
    $this->cookie['total_rows'] = $this->m_piutang->all_rows($this->cookie);
    set_cookie_nav($this->nav_id, $this->cookie);
    //main data
    $data['nav'] = $this->nav;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_piutang->list_data($this->cookie);
    $data['lokasi'] = $this->m_lokasi->all_data();
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
    //set pagination
    set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('keuangan/kasir/piutang/index',$data);
  }
  
  public function piutang_modal($id=null) {
    $this->authorize($this->nav, ($id != '' ) ? '_update' : '_add');
    $groupreg_in = $this->input->get('groupreg_in');
		if($id == null) {
			$data['main'] = null;
      $data['tindakan'] = null;
      $data['obat'] = null;
      $data['alkes'] = null;
      $data['bhp'] = null;
      $data['kamar'] = null;
		} else {
      $data['main'] = $this->m_piutang->piutang_data($id);
      $data['tindakan'] = $this->m_piutang->piutang_tindakan($groupreg_in);
      $data['obat'] = $this->m_piutang->piutang_obat($groupreg_in);
      $data['alkes'] = $this->m_piutang->piutang_alkes($groupreg_in);
      $data['bhp'] = $this->m_piutang->piutang_bhp($groupreg_in);
      $data['kamar'] = $this->m_piutang->piutang_kamar($groupreg_in);
    }
    
		$data['id'] = $id;
		$data['nav'] = $this->nav;
			
		echo json_encode(array(
			'html' => $this->load->view('keuangan/kasir/piutang/piutang_modal', $data, true)
		));
	}

}