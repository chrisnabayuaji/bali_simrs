<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class surat_ket_lahir extends MY_Controller
{

  var $nav_id = '07.01.03', $nav, $cookie;

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'kasir/m_surat_ket_lahir',
      'master/m_lokasi',
      'app/m_profile',
      'master/m_identitas',
      'master/m_kelas',
      'master/m_jenis_pasien',
      'master/m_pegawai'
    ));

    $this->nav = $this->m_app->_get_nav($this->nav_id);

    //cookie
    $this->cookie = get_cookie_nav($this->nav_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('tgl_lahir_from' => '', 'tgl_lahir_to' => '', 'no_rm_nm' => '', 'lokasi_id' => '', 'jenisreg_st' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'tgl_lahir', 'type' => 'desc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
  }

  public function index()
  {
    $this->authorize($this->nav, '_view');
    //cookie
    $this->cookie['cur_page'] = $this->uri->segment(5, 0);
    $this->cookie['total_rows'] = $this->m_surat_ket_lahir->all_rows($this->cookie);
    set_cookie_nav($this->nav_id, $this->cookie);
    //main data
    $data['nav'] = $this->nav;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_surat_ket_lahir->list_data($this->cookie);
    $data['lokasi'] = $this->m_lokasi->all_data();
    $data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
    //set pagination
    set_pagination($this->nav, $this->cookie);
    //render
    create_log('_view', $this->nav_id);
    $this->render('keuangan/kasir/surat_ket_lahir/index', $data);
  }

  public function form_bayi($id = null)
  {
    $this->authorize($this->nav, ($id != '') ? '_update' : '_add');

    $data['main'] = $this->m_surat_ket_lahir->get_data($id);
    $data['diagnosis'] = $this->m_surat_ket_lahir->diagnosis($id);
    $data['id'] = $id;
    $data['nav'] = $this->nav;
    $data['lokasi_asal'] = $this->m_lokasi->by_field('is_active', 1, 'result');
    $data['lokasi_bangsal'] = $this->m_lokasi->by_field('jenisreg_st', 2, 'result');
    $data['kelas'] = $this->m_kelas->list_kelas();
    $data['dokter'] = $this->m_pegawai->by_field('jenispegawai_cd', '02', 'result');
    $data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
    $data['form_action'] = site_url() . '/' . 'keuangan/kasir/surat_ket_lahir' . '/save/' . $id;

    $this->render('keuangan/kasir/surat_ket_lahir/form_bayi', $data);
  }

  public function save($id = null)
  {
    $this->m_surat_ket_lahir->save($id);
    $this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
    create_log(($id != '') ? '_update' : '_add', $this->nav_id);
    redirect(site_url() . '/' . 'keuangan/kasir/surat_ket_lahir/form_bayi/' . $id);
  }

  public function cetak_modal($type = null, $id = null)
  {
    $groupreg_in = $this->input->get('groupreg_in');
    $data['url'] = site_url() . '/keuangan/kasir/surat_ket_lahir/' . $type . '/' . $id;

    echo json_encode(array(
      'html' => $this->load->view('keuangan/kasir/surat_ket_lahir/cetak_modal', $data, true)
    ));
  }

  public function cetak_surat_kelahiran($id = null)
  {
    $main = $this->m_surat_ket_lahir->get_data($id);
    $identitas = $this->m_app->_get_identitas();
    // generate qrcode
    $this->load->library('ciqrcode');
    $config['cacheable']    = true;
    $config['cachedir']     = 'assets/images/qrcode/';
    $config['errorlog']     = 'assets/images/qrcode/';
    $config['imagedir']     = 'assets/images/qrcode/';
    $config['quality']      = true;
    $config['size']         = '1024';
    $config['black']        = array(224, 255, 255);
    $config['white']        = array(70, 130, 180);
    $this->ciqrcode->initialize($config);

    $params['data'] = @$main['reg_id'];
    $params['level'] = 'H';
    $params['size'] = 10;
    $params['savename'] = FCPATH . $config['imagedir'] . 'qrcode-kelahiran-' . @$main['reg_id'] . '.png';
    $this->ciqrcode->generate($params);
    //
    if ($main['sex_cd'] == 'L') {
      $jns_kelamin = 'LAKI-LAKI';
    } elseif ($main['sex_cd'] == 'P') {
      $jns_kelamin = 'PEREMPUAN';
    } else {
      $jns_kelamin = '';
    }

    if ($main['spesialisasi_cd'] != '') {
      $spesialisasi_nm = $main['spesialisasi_nm'];
    } else {
      $spesialisasi_nm = '-';
    }

    //generate pdf
    $this->load->library('pdf');
    $pdf = new Pdf('p', 'mm', array(230, 340));
    // $pdf = new Pdf('p', 'mm', array(210, 297));
    $pdf->AliasNbPages();
    $pdf->SetTitle('Cetak Surat Keterangan Lahir ' . $id);
    $pdf->AddPage();
    $pdf->Rect(10, 10, 210, 310, 'D');
    $pdf->Rect(10.2, 10.2, 210, 310, 'D');
    $pdf->Rect(10.4, 10.4, 210, 310, 'D');
    $pdf->Rect(10.6, 10.6, 210, 310, 'D');

    // $pdf->Image(FCPATH . 'assets/images/icon/' . $identitas['logo_rumah_sakit'], 20, 15, 20, 20);
    // $pdf->SetFont('Times', 'B', 17);
    // $pdf->Cell(0, 5, '', 0, 1, 'L');
    // $pdf->Cell(18, 6, '', 0, 0, 'L');
    // $pdf->Cell(0, 6, "RUMAH SAKIT IBU DAN ANAK PERMATA  ", 0, 1, 'C');
    // $pdf->SetFont('Times', 'B', 13);
    // $pdf->Cell(18, 9, '', 0, 0, 'L');
    // $pdf->Cell(0, 9, "Jl. Mayjend Sutoyo no 75 Telp. (0272) 321031", 0, 1, 'C');
    // $pdf->Cell(18, 6, '', 0, 0, 'L');
    // $pdf->Cell(0, 6, "PURWOREJO", 0, 1, 'C');
    $pdf->Image(FCPATH . 'assets/images/icon/new-kop.jpg', 20, 12, 170, 24);
    $pdf->Cell(0, 20, '', 0, 1, 'C');

    $pdf->Cell(0, 12, '', 0, 1, 'L');
    $pdf->SetFont('Times', 'BU', 14);
    $pdf->Cell(0, 4, 'S U R A T  K E T E R A N G A N  K E L A H I R A N', 0, 1, 'C');
    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(0, 6, 'NO. REG. ' . @$main['reg_id'], 0, 1, 'C');
    $pdf->Cell(0, 0, '', 0, 1, 'C');

    $pdf->Cell(0, 6, '', 0, 1, 'L');
    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(0, 5, 'Yang bertanda tangan di bawah ini, menerangkan bahwa pada hari : ', 0, 1, 'C');
    $pdf->writeHTML('<p align="center"><b>' . strtoupper(to_dayname($main['tgl_lahir'])) . ' ' . strtoupper(pasaran_jawa($main['tgl_lahir'])) . ' Tanggal ' . strtoupper(to_date_indo($main['tgl_lahir'])) . '</b></p>');
    if ($main['sex_cd'] == 'P') {
      $pdf->SetX(60);
    } else {
      $pdf->SetX(64);
    }
    $pdf->writeHTML('Telah lahir bayi <b>' . $jns_kelamin . '</b> dari orang tua yang bernama : ');
    $pdf->Cell(0, 14, '', 0, 1, 'C');

    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(15, 0, '', 0, 0, 'C');
    $pdf->Cell(25, 6, 'Ibu', 0, 0, 'L');
    $pdf->Cell(10, 6, ':', 0, 0, 'C');
    $pdf->SetFont('Times', 'B', 12);
    $pdf->Cell(30, 6, 'Ny. ' . str_replace("&#039;", "'", @$main['ibu_nm']), 0, 1, 'L');
    $pdf->Cell(0, 6, '', 0, 1, 'C');

    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(15, 0, '', 0, 0, 'C');
    $pdf->Cell(25, 6, 'Umur', 0, 0, 'L');
    $pdf->Cell(10, 6, ':', 0, 0, 'C');
    $pdf->SetFont('Times', 'B', 12);
    $pdf->Cell(30, 6, @$main['ibu_umur'] . ' TH', 0, 1, 'L');
    $pdf->Cell(0, 6, '', 0, 1, 'C');

    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(15, 0, '', 0, 0, 'C');
    $pdf->Cell(25, 6, 'Bapak', 0, 0, 'L');
    $pdf->Cell(10, 6, ':', 0, 0, 'C');
    $pdf->SetFont('Times', 'B', 12);
    $pdf->Cell(30, 6, 'Tn. ' . str_replace("&#039;", "'", @$main['ayah_nm']), 0, 1, 'L');
    $pdf->Cell(0, 6, '', 0, 1, 'C');

    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(15, 0, '', 0, 0, 'C');
    $pdf->Cell(25, 6, 'Alamat', 0, 0, 'L');
    $pdf->Cell(10, 6, ':', 0, 0, 'C');
    $pdf->SetFont('Times', 'B', 12);
    $pdf->Cell(30, 6, @$main['alamat'], 0, 1, 'L');
    $pdf->Cell(50, 0, '', 0, 0, 'C');
    $pdf->Cell(30, 6, @$main['kabupaten'], 0, 1, 'L');
    $pdf->Cell(0, 15, '', 0, 1, 'C');

    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(0, 5, 'Yang selanjutnya diberi nama : ', 0, 1, 'C');
    if (@$main['bayi_nm'] != '') {
      $pdf->SetFont('Times', 'B', 16);
      $pdf->Cell(0, 15, str_replace('&#039;', "'", str_replace("&#039;", "'", @$main['bayi_nm'])), 0, 1, 'C');
    } else {
      $pdf->SetFont('Times', '', 16);
      $pdf->Cell(0, 15, "                                          ", 0, 1, 'C');
    }
    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(0, 5, 'Harap yang berkepentingan memaklumi', 0, 1, 'C');

    // petugas
    $pdf->Cell(0, 10, '', 0, 1, 'C');
    $pdf->SetFont('Times', 'B', 12);
    $pdf->Cell(0, 2, '', 0, 1, 'C');

    $pdf->Cell(10, 4, '', 0, 0, 'L');
    $pdf->Cell(80, 4, 'Catatan', 0, 0, 'L');
    $pdf->Cell(0, 4, 'Purworejo, ' . strtoupper(to_date_indo(date('Y-m-d H:i:s'), 'date')), 0, 1, 'C');

    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(10, 9, '', 0, 0, 'L');
    $pdf->Cell(20, 9, 'Persalinan', 0, 0, 'L');
    $pdf->Cell(10, 9, ':', 0, 0, 'R');
    $pdf->SetFont('Times', 'B', 12);
    $pdf->Cell(50, 9, (@$main['persalinan'] != '') ? @$main['persalinan'] : '-', 0, 0, 'L');
    $pdf->SetFont('Times', 'B', 12);
    $pdf->Cell(0, 9, 'Penolong Persalinan', 0, 1, 'C');

    if ($main['pegawai_id'] == '000004') {
      $pdf->Image(FCPATH . 'assets/images/ttd/dr-lukman.png', 145, 183, 33, 50);
    } else {
      $pdf->Image(FCPATH . 'assets/images/ttd/dr-dradjat.png', 138, 193, 40, 20);
    }

    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(10, 9, '', 0, 0, 'L');
    $pdf->Cell(20, 7, 'Lahir Jam', 0, 0, 'L');
    $pdf->Cell(10, 7, ':', 0, 0, 'R');
    $pdf->SetFont('Times', 'B', 12);
    $pdf->Cell(50, 7, @$main['jam_lahir'] . ' WIB', 0, 0, 'L');
    $pdf->Cell(0, 7, '', 0, 1, 'C');

    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(10, 9, '', 0, 0, 'L');
    $pdf->Cell(20, 7, 'Anak ke', 0, 0, 'L');
    $pdf->Cell(10, 7, ':', 0, 0, 'R');
    $pdf->SetFont('Times', 'B', 12);
    $pdf->Cell(50, 7, to_rome(@$main['anak_ke']) . ' (' . strtoupper(terbilang(@$main['anak_ke'])) . ')', 0, 0, 'L');
    $pdf->Cell(0, 7, '', 0, 1, 'C');

    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(10, 9, '', 0, 0, 'L');
    $pdf->Cell(20, 7, 'BB', 0, 0, 'L');
    $pdf->Cell(10, 7, ':', 0, 0, 'R');
    $pdf->SetFont('Times', 'B', 12);
    $pdf->Cell(50, 7, @$main['bb'] . ' Gram', 0, 0, 'L');
    $pdf->Cell(0, 7, '', 0, 1, 'C');

    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(10, 9, '', 0, 0, 'L');
    $pdf->Cell(20, 7, 'PB', 0, 0, 'L');
    $pdf->Cell(10, 7, ':', 0, 0, 'R');
    $pdf->SetFont('Times', 'B', 12);
    $pdf->Cell(50, 7, @$main['pb'] . ' Cm', 0, 0, 'L');
    $pdf->SetFont('Times', 'B', 13);
    $pdf->Cell(5, 4, '', 0, 0, 'C');
    $pdf->Cell(0, 4, @$main['penolong_persalinan'], 0, 1, 'C');
    $pdf->SetFont('Times', 'B', 11);
    $pdf->Cell(95, 12, '', 0, 0, 'C');
    $pdf->Cell(0, 12, @$spesialisasi_nm, 0, 1, 'C');

    $pdf->Cell(0, 10, '', 0, 1, 'C');
    $pdf->SetFont('Times', '', 12);
    $pdf->Cell(1, 9, '', 0, 0, 'L');
    $pdf->Cell(0, 5, '*Surat Keterangan Lahir ini hanya dikeluarkan satu kali', 0, 1, 'L');

    $pdf->Cell(0, 24, '', 0, 1, 'C');
    $pdf->SetFont('Times', 'BI', 22);
    if ($main['sex_cd'] == 'L') {
      $pdf->Cell(0, 5, 'Semoga menjadi anak yang sholeh', 0, 1, 'C');
    }
    if ($main['sex_cd'] == 'P') {
      $pdf->Cell(0, 5, 'Semoga menjadi anak yang sholehah', 0, 1, 'C');
    }

    $pdf->Image(FCPATH . 'assets/images/qrcode/qrcode-kelahiran-' . @$main['reg_id'] . '.png', 165, 285, 30, 30);

    $pdf->Output('I', 'SuratKeteranganLahir_' . $id . '_' . date('Ymdhis') . '.pdf');
  }
}
