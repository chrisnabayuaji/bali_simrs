<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_bukubesar extends CI_Model {

  var $saldo_awal;

  public function where($cookie,$tp='')
	{
    $where = "";
    //
    if($tp == 'item') {
      $where = " AND a.is_deleted = 0 ";
      if (@$cookie['search']['akun_id'] != '') {
        $akun_id = ($cookie['search']['akun_id']);
        $akun_id_len = strlen($akun_id);
        //
        $where .= "AND LEFT(b.akun_id,$akun_id_len)='$akun_id' ";
      }
      if (@$cookie['search']['tgl_jurnal_awal'] != '' && @$cookie['search']['tgl_jurnal_akhir'] != '') {        
        $where .= "AND DATE(a.tgl_jurnal) BETWEEN '".to_date($cookie['search']['tgl_jurnal_awal'])."' AND '".to_date($cookie['search']['tgl_jurnal_akhir'])."' ";
      }
    } else {
      if (@$cookie['search']['tahun'] != '') {        
        $where .= "AND YEAR(a.tgl_jurnal)='".$cookie['search']['tahun']."' ";
      }
    }		
    //
		return $where;
	}

  public function list_data($cookie, $saldo_awal)
  {
    // saldo awal
    if($this->saldo_awal == '') $this->saldo_awal = $saldo_awal;
    //
    $where = $this->where($cookie);
    $where_item = $this->where($cookie,'item');
    $sql = "SELECT 
                    a.*,b.akun_nm,b.tp_akun,b.tp_saldo,b.tp_laporan 
                FROM 
                (
                    (
                        SELECT 
                            a.*,b.akun_id,b.sub_nominal,'debet' as tp_saldo,
                            b.sub_nominal as sub_nominal_debet, 
                            '' as sub_nominal_kredit
                        FROM trx_jurnal a 
                        INNER JOIN trx_jurnal_debet b on a.jurnal_id=b.jurnal_id
                        WHERE 1
                            $where_item
                    )
                    union all 
                    (
                        SELECT 
                            a.*,b.akun_id,b.sub_nominal,'kredit' as tp_saldo,
                            '' as sub_nominal_debet,
                            b.sub_nominal as sub_nominal_kredit
                        FROM trx_jurnal a 
                        INNER JOIN trx_jurnal_kredit b on a.jurnal_id=b.jurnal_id
                        WHERE 1
                            $where_item
                    )
                ) a
                LEFT JOIN mst_akun b ON a.akun_id=b.akun_id
                WHERE 1 
                    $where 
                ORDER BY a.tgl_jurnal ASC, a.jurnal_id ASC";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $no=2;
    foreach($result as $key => $val) {
      $result[$key]['no'] = $no++;
      $result[$key]['saldo'] = $this->_get_saldo($this->saldo_awal, $result[$key]['tp_saldo'], $result[$key]['sub_nominal_debet'], $result[$key]['sub_nominal_kredit']);
      //
      $this->saldo_awal = $result[$key]['saldo'];
    }
    return $result;
  }

  public function _get_saldo($saldo_awal, $tp_saldo, $sub_nominal_debet, $sub_nominal_kredit) {
      switch ($tp_saldo) {
          //
          // DEBET
          case 'D':  
              if($sub_nominal_debet != '') {
                  $result = $saldo_awal + $sub_nominal_debet;
              } elseif($sub_nominal_kredit != '') {
                  $result = $saldo_awal - $sub_nominal_kredit;
              }
              //
              break;            
          // 
          // KREDIT
          case 'K':
              if($sub_nominal_kredit != '') {
                  $result = $saldo_awal + $sub_nominal_kredit;
              } elseif($sub_nominal_debet != '') {
                  $result = $saldo_awal - $sub_nominal_debet;
              }
              //
              break;            
          default:
              $result = 0;
              break;
      }
      return $result;
  }

  public function _get_saldo_awal($akun_parent,$tahun=null) 
  {
      if($tahun != '') $this->tahun = $tahun;
      //
      $field = 'saldo_awal_'.$this->tahun;
      $len = strlen($akun_parent);
      //
      $sql = "SELECT SUM($field) as saldo_awal 
              FROM mst_akun 
              WHERE LEFT(akun_id,$len) = ?";
      $query = $this->db->query($sql, $akun_parent);
      $row = $query->row_array();
      return (@$row['saldo_awal'] != '' ? @$row['saldo_awal'] : '0');
  }

  public function _get_saldo_bukubesar_sd($akun_id, $tahun, $tanggal) 
  {
        $tanggal = to_date($tanggal);
        $akun_id_len = strlen($akun_id);
        //
        $sql_where = "";
        if($akun_id != '')  $sql_where .= " AND LEFT(b.akun_id,$akun_id_len)='$akun_id'";
        // DEBET
        $sql_d = "SELECT 
                    SUM(IF(c.tp_akun='K', (a.sub_nominal * -1), a.sub_nominal)) as saldo 
                FROM trx_jurnal_debet a 
                INNER JOIN trx_jurnal b ON a.jurnal_id=b.jurnal_id
                INNER JOIN mst_akun c ON a.akun_id=c.akun_id
                WHERE LEFT(a.akun_id,$akun_id_len)=? AND YEAR(b.tgl_jurnal)=? AND b.tgl_jurnal < ?";
        $query_d = $this->db->query($sql_d, array($akun_id, $tahun, $tanggal));        
        $row_d = $query_d->row_array();
        $saldo_d = @$row_d['saldo'];
        //
        // KREDIT
        $sql_k = "SELECT 
                    SUM(IF(c.tp_akun='D', (a.sub_nominal * -1), a.sub_nominal)) as saldo 
                FROM trx_jurnal_kredit a 
                INNER JOIN trx_jurnal b ON a.jurnal_id=b.jurnal_id
                INNER JOIN mst_akun c ON a.akun_id=c.akun_id
                WHERE LEFT(a.akun_id,$akun_id_len)=? AND YEAR(b.tgl_jurnal)=? AND b.tgl_jurnal < ?";
        $query_k = $this->db->query($sql_k, array($akun_id, $tahun, $tanggal));
        $row_k = $query_k->row_array();
        $saldo_k = @$row_k['saldo'];
        //
        // TOTAL
        $saldo_total = $saldo_d + $saldo_k;
        return $saldo_total;
    }

  public function all_data_akun()
  {
    $where = "WHERE a.is_deleted = 0 ";
    $sql = "SELECT * FROM mst_akun a $where ORDER BY akun_id ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
  
}