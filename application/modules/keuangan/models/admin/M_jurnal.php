<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_jurnal extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND a.no_bukti LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' ";
    }
    // if (@$cookie['search']['parameter_cd'] != '') {
    //   $where .= "AND b.jenisjurnal_cd = '".$this->db->escape_like_str($cookie['search']['parameter_cd'])."' ";
    // }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
                a.*
            FROM trx_jurnal a 
            $where
            ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    $result = $query->result_array();
    foreach ($result as $key => $val) {
      $result[$key]['list_item_debet'] = $this->_list_item_data($result[$key]['jurnal_id'], 'debet');
      $result[$key]['list_item_kredit'] = $this->_list_item_data($result[$key]['jurnal_id'], 'kredit');
    }
    return $result;
  }

  public function _list_item_data($jurnal_id, $tp_saldo = '')
  {
    $tbl_sub = 'trx_jurnal_' . $tp_saldo;
    $sql = "SELECT a.*,b.akun_nm,b.akun_nm as sub_uraian,DATE(c.tgl_jurnal) as tgl_jurnal
            FROM $tbl_sub a 
            LEFT JOIN mst_akun b ON a.akun_id=b.akun_id 
            INNER JOIN trx_jurnal c ON a.jurnal_id=c.jurnal_id
            WHERE a.jurnal_id=? ORDER BY a.jurnalitem_id ASC";
    $query = $this->db->query($sql, $jurnal_id);
    return $query->result_array();
  }

  public function _list_item_data_raw_by_date($tgl_jurnal, $tp_saldo = '')
  {
    $akun_id_obat = '04.01.11';
    $akun_id_alkes = '04.01.12';
    $akun_id_bhp = '04.01.13';
    $akun_id_kamar = '04.01.10';
    //
    $sql = "SELECT x.*, 'true' as is_raw FROM 
            (
              (
                SELECT c.akun_id,c.akun_nm,DATE(a.tgl_catat) as tgl_jurnal,SUM(a.jml_tagihan) as sub_nominal,'' as jurnalitem_id 
                FROM dat_tindakan a 
                INNER JOIN mst_tarif b ON a.tarif_id=b.tarif_id
                INNER JOIN mst_akun c ON b.akun_id=c.akun_id
                WHERE DATE(a.tgl_catat)=? AND c.tp_saldo='$tp_saldo'
                GROUP BY c.akun_id
              )
              UNION ALL 
              (
                SELECT c.akun_id,c.akun_nm,DATE(a.tgl_catat) as tgl_jurnal,SUM(a.grand_jml_tagihan) as sub_nominal,'' as jurnalitem_id 
                FROM dat_resep a 
                LEFT JOIN 
                (
                    SELECT * FROM mst_akun WHERE akun_id='$akun_id_obat' AND tp_saldo='$tp_saldo'
                ) c ON 1=1
                WHERE DATE(a.tgl_catat)=?
              )
              UNION ALL 
              (
                SELECT c.akun_id,c.akun_nm,DATE(a.tgl_catat) as tgl_jurnal,SUM(a.jml_tagihan) as sub_nominal,'' as jurnalitem_id 
                FROM dat_alkes a 
                LEFT JOIN 
                (
                    SELECT * FROM mst_akun WHERE akun_id='$akun_id_alkes' AND tp_saldo='$tp_saldo'
                ) c ON 1=1
                WHERE DATE(a.tgl_catat)=?
              )
              UNION ALL 
              (
                SELECT c.akun_id,c.akun_nm,DATE(a.tgl_catat) as tgl_jurnal,SUM(a.jml_tagihan) as sub_nominal,'' as jurnalitem_id 
                FROM dat_bhp a 
                LEFT JOIN 
                (
                    SELECT * FROM mst_akun WHERE akun_id='$akun_id_bhp' AND tp_saldo='$tp_saldo'
                ) c ON 1=1
                WHERE DATE(a.tgl_catat)=?
              )
              UNION ALL 
              (
                SELECT c.akun_id,c.akun_nm,DATE(a.tgl_keluar) as tgl_jurnal,SUM(a.jml_tagihan) as sub_nominal,'' as jurnalitem_id 
                FROM reg_pasien_kamar a 
                LEFT JOIN 
                (
                    SELECT * FROM mst_akun WHERE akun_id='$akun_id_kamar' AND tp_saldo='$tp_saldo'
                ) c ON 1=1
                WHERE DATE(a.tgl_keluar)=?
              )
            ) as x WHERE x.akun_id != ''";
    $query = $this->db->query($sql, array($tgl_jurnal, $tgl_jurnal, $tgl_jurnal, $tgl_jurnal, $tgl_jurnal));
    if ($query->num_rows() > 0) {
      $result = $query->result_array();
      return $result;
    } else {
      return array();
    }
  }

  public function _detail_item_data_raw_by_date($akun_id, $tgl_jurnal)
  {
    $akun_id_obat = '04.01.11';
    if ($akun_id == $akun_id_obat) {  // obat
      $sql = "SELECT 
                  c.akun_id,c.akun_nm,a.resep_id as transaksi_id,a.obat_nm as uraian,a.qty,a.jumlah_awal as jml_awal,a.jumlah_akhir as jml_tagihan,a.tgl_catat,
                  d.pasien_nm,d.pasien_id,e.lokasi_nm
                FROM dat_farmasi a 
                LEFT JOIN 
                (
                    SELECT * FROM mst_akun WHERE akun_id='$akun_id_obat'
                ) c ON 1=1
                INNER JOIN reg_pasien d ON a.reg_id=d.reg_id
                INNER JOIN mst_lokasi e ON d.lokasi_id=e.lokasi_id
                WHERE DATE(a.tgl_catat)='$tgl_jurnal' AND c.akun_id='$akun_id'
                ORDER BY a.reg_id ASC, a.resep_id ASC";
    } else {
      $sql = "SELECT 
                  c.akun_id,c.akun_nm,a.tindakan_id as transaksi_id,a.tarif_nm as uraian,a.qty,a.jml_awal,a.jml_tagihan,a.tgl_catat,
                  d.pasien_nm,d.pasien_id,e.lokasi_nm
                FROM dat_tindakan a 
                INNER JOIN mst_tarif b ON a.tarif_id=b.tarif_id
                INNER JOIN mst_akun c ON b.akun_id=c.akun_id
                INNER JOIN reg_pasien d ON a.reg_id=d.reg_id
                INNER JOIN mst_lokasi e ON d.lokasi_id=e.lokasi_id
                WHERE DATE(a.tgl_catat)='$tgl_jurnal' AND c.akun_id='$akun_id'
                ORDER BY a.reg_id ASC, a.tindakan_id ASC";
    }
    //
    $query = $this->db->query($sql);
    if ($query->num_rows() > 0) {
      $result = $query->result_array();
      return $result;
    } else {
      return array();
    }
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";
    // if ($jenisjurnal_cd != '') {
    //   $where .= "AND a.jenisjurnal_cd='$jenisjurnal_cd'";
    // }

    $sql = "SELECT * FROM trx_jurnal a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data_akun()
  {
    $where = "WHERE a.is_deleted = 0 ";
    $sql = "SELECT * FROM mst_akun a $where ORDER BY akun_id ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT 
              COUNT(1) as total
            FROM trx_jurnal a 
            $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id)
  {
    $sql = "SELECT * FROM trx_jurnal WHERE jurnal_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    $row['list_item_debet'] = $this->_list_item_data($row['jurnal_id'], 'debet');
    $row['list_item_kredit'] = $this->_list_item_data($row['jurnal_id'], 'kredit');
    return $row;
  }

  function by_field($field, $val, $type = 'row')
  {
    $sql = "SELECT * FROM trx_jurnal WHERE $field=?";
    $query = $this->db->query($sql, array($val));
    if ($type == 'row') {
      $res = $query->row_array();
    } else if ($type == 'result') {
      $res = $query->result_array();
    }
    return $res;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    if ($id == null) {
      $data_main['jurnal_id'] = get_id('trx_jurnal');
      $data_main['tgl_jurnal'] = to_date($data['tgl_jurnal']);
      $data_main['nominal'] = $this->_set_nominal($data);
      $data_main['uraian'] = $data['uraian'];
      $data_main['user_cd'] = $this->session->userdata('sess_user_cd');
      $data_main['created_at'] = date('Y-m-d H:i:s');
      $data_main['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('trx_jurnal', $data_main);
      update_id('trx_jurnal', $data_main['jurnal_id']);
    } else {
      $data_main['jurnal_id'] = $id;
      $data_main['tgl_jurnal'] = to_date($data['tgl_jurnal']);
      $data_main['nominal'] = $this->_set_nominal($data);
      $data_main['uraian'] = $data['uraian'];
      $data_main['updated_at'] = date('Y-m-d H:i:s');
      $data_main['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('jurnal_id', $id)->update('trx_jurnal', $data_main);
    }
    //
    // save item
    $this->_save_item($data_main['jurnal_id'], 'debet');
    $this->_save_item($data_main['jurnal_id'], 'kredit');
  }

  public function _set_nominal($data = array())
  {
    $total = 0;
    foreach (@$data['sub_nominal']['debet'] as $key => $val) {
      $total += $val;
    }
    return $total;
  }

  public function _save_item($jurnal_id = null, $tp_saldo = null)
  {
    $tbl_sub = 'trx_jurnal_' . $tp_saldo;
    //
    $no = 0;
    $data = html_escape($this->input->post());
    foreach ($data['akun_id'][$tp_saldo] as $key => $val) {
      if ($val != '') {
        $no++;
        $jurnalitem_id = @$data['jurnalitem_id'][$tp_saldo][$key];
        //                
        $data_sub['jurnal_id'] = $jurnal_id;
        $data_sub['akun_id'] = @$data['akun_id'][$tp_saldo][$key];
        $data_sub['sub_nominal'] = clear_numeric(@$data['sub_nominal'][$tp_saldo][$key]);
        //
        if ($jurnalitem_id != '') {
          $this->db->where('jurnalitem_id', $jurnalitem_id);
          $outp = $this->db->update($tbl_sub, $data_sub);
        } else {
          $data_sub['jurnalitem_id'] = $jurnal_id . sprintf("%02d", $no);
          $outp = $this->db->insert($tbl_sub, $data_sub);
        }
      }
    }
    return @$outp;
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('jurnal_id', $id)->delete('trx_jurnal_debet');
      $this->db->where('jurnal_id', $id)->delete('trx_jurnal_kredit');
      $this->db->where('jurnal_id', $id)->delete('trx_jurnal');
    } else {
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('jurnal_id', $id)->update('trx_jurnal_debet', $data);
      $this->db->where('jurnal_id', $id)->update('trx_jurnal_kredit', $data);
      $this->db->where('jurnal_id', $id)->update('trx_jurnal', $data);
    }
  }

  public function akun_autocomplete($akun_nm = null)
  {
    $sql = "SELECT 
              a.*
            FROM mst_akun a
            WHERE (a.akun_nm LIKE '%$akun_nm%' OR a.akun_id LIKE '%$akun_nm%') ";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['akun_id'],
        'text' => $row['akun_id'] . ' - ' . $row['akun_nm']
      );
    }
    return $res;
  }
}
