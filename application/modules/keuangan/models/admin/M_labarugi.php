<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_labarugi extends CI_Model {

  var $saldo;

  public function where($cookie)
	{
    $where = "";
    if (@$cookie['search']['tgl_jurnal_awal'] != '' && @$cookie['search']['tgl_jurnal_akhir'] != '') {        
      $where .= "AND DATE(a.tgl_jurnal) BETWEEN '".to_date($cookie['search']['tgl_jurnal_awal'])."' AND '".to_date($cookie['search']['tgl_jurnal_akhir'])."' ";
    }
		return $where;
	}

  public function list_labarugi($cookie, $tp_laporan_sub=null) {
      $tahun = $cookie['search']['tahun'];
      //
      $sql = "SELECT * FROM mst_akun WHERE tp_laporan='LR' AND tp_laporan_sub=? ORDER BY akun_id ASC";
      $query = $this->db->query($sql, $tp_laporan_sub);
      $result = $query->result_array();
      // 
      $no=1;
      foreach($result as $key => $val) {
          if($result[$key]['tp_akun'] == 'G') {
              $result[$key]['saldo_awal'] = $this->_get_saldo_awal($tahun, $result[$key]['akun_id']);
          } else {
              $result[$key]['saldo_awal'] = $result[$key]['saldo_awal_'.$tahun];
          }                        
          //
          $rs_saldo = $this->_get_saldo_debet_kredit($cookie, $result[$key]['akun_id'], $result[$key]['tp_akun'], $result[$key]['tp_saldo'], $result[$key]['saldo_awal']);
          //
          $result[$key]['no'] = $no;
          $result[$key]['lr_debet'] = ($result[$key]['tp_laporan'] == 'LR' && $result[$key]['tp_saldo'] == 'D') ? @$rs_saldo['saldo_debet'] : '';
          $result[$key]['lr_kredit'] = ($result[$key]['tp_laporan'] == 'LR' && $result[$key]['tp_saldo'] == 'K') ? @$rs_saldo['saldo_kredit'] : '';            
          $result[$key]['jumlah'] = $result[$key]['lr_debet'] + $result[$key]['lr_kredit'];
          //
          $no++;
      }
      return $result;
  }

  public function _get_jumlah_total($list_labarugi=array()) {
      $total = 0;
      foreach($list_labarugi as $row) {
          if($row['tp_akun'] == 'D') {
              $total += $row['jumlah'];
          }
      }
      return $total;
  }

  public function _get_saldo_awal($tahun, $akun_parent) {
    if($tahun != '') $this->tahun = $tahun;
    //
    $field = 'saldo_awal_'.$this->tahun;
    $len = strlen($akun_parent);
    //
    $sql = "SELECT SUM($field) as saldo_awal 
            FROM mst_akun 
            WHERE LEFT(akun_id,$len) = ?";
    $query = $this->db->query($sql, $akun_parent);
    $row = $query->row_array();
    return @$row['saldo_awal'];
  }

  public function _get_saldo_debet_kredit($cookie, $akun_id=null, $tp_akun=null, $tp_saldo=null, $saldo_awal=null) {
      $akun_id_len = strlen($akun_id);
      $sql_where = $this->where($cookie);
      //
      $sql = "SELECT 
                  SUM(b.jml_nominal_debet) as jml_nominal_debet,
                  SUM(c.jml_nominal_kredit) as jml_nominal_kredit
              FROM trx_jurnal a 
              LEFT JOIN 
              (
                  SELECT 
                      jurnal_id,
                      IF('$tp_saldo' = 'D', SUM(sub_nominal), (SUM(sub_nominal) * -1)) as jml_nominal_debet 
                  FROM trx_jurnal_debet
                  WHERE LEFT(akun_id, $akun_id_len)='$akun_id'
                  GROUP BY jurnal_id
              ) b ON a.jurnal_id=b.jurnal_id
              LEFT JOIN 
              (
                  SELECT 
                      jurnal_id,
                      IF('$tp_saldo' = 'K', SUM(sub_nominal), (SUM(sub_nominal) * -1)) as jml_nominal_kredit
                  FROM trx_jurnal_kredit
                  WHERE LEFT(akun_id, $akun_id_len)='$akun_id'
                  GROUP BY jurnal_id
              ) c ON a.jurnal_id=c.jurnal_id
              WHERE 1 
                  $sql_where";
      $query = $this->db->query($sql);
      $row = $query->row_array();
      //
      $saldo_akhir = $saldo_awal + $row['jml_nominal_debet'] + $row['jml_nominal_kredit'];
      //
      $result['saldo_debet'] = ($tp_saldo == 'D' ? $saldo_akhir : '');
      $result['saldo_kredit'] = ($tp_saldo == 'K' ? $saldo_akhir : '');
      //
      return $result;
  }

  
}