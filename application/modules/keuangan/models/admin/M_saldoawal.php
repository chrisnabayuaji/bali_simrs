<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_saldoawal extends CI_Model {

  public function list_data($cookie)
  {
    $tahun = $cookie['search']['tahun'];
    //
    $sql = "SELECT * FROM mst_akun ORDER BY akun_id ASC";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $no=1;
    foreach($result as $key => $val) {
      $result[$key]['no'] = $no++;
      if($result[$key]['tp_akun'] == 'G') {
          $result[$key]['saldo_awal'] = $this->_get_saldo_awal($tahun, $result[$key]['akun_id']);
      } else {
          $result[$key]['saldo_awal'] = $result[$key]['saldo_awal_'.$tahun];
      }                        
      $result[$key]['tp_akun_nm'] = ($result[$key]['tp_akun'] == 'G' ? 'GROUP' : 'DETAIL');
      $result[$key]['tp_saldo_nm'] = ($result[$key]['tp_saldo'] == 'D' ? 'DEBET' : 'KREDIT');
      $result[$key]['tp_laporan_nm'] = ($result[$key]['tp_laporan'] == 'NR' ? 'NERACA' : 'LABA/RUGI');
      $result[$key]['tp_laporan_sub_nm'] = $this->_get_tp_laporan_sub_nm($result[$key]['tp_laporan_sub']);
    }
    return $result;
  }

  public function _get_saldo_awal($tahun, $akun_parent) 
  {
      $field = 'saldo_awal_'.$tahun;
      $len = strlen($akun_parent);
      //
      $sql = "SELECT SUM($field) as saldo_awal 
              FROM mst_akun 
              WHERE LEFT(akun_id,$len) = ?";
      $query = $this->db->query($sql, $akun_parent);
      $row = $query->row_array();
      return (@$row['saldo_awal'] != '' ? @$row['saldo_awal'] : '0');
  }

  public function _get_tp_laporan_sub_nm($tp_laporan_sub=null) 
  {
      switch ($tp_laporan_sub) {
          case 'AKT':
              $result = 'AKTIVA';
              break;
          case 'PSV':
              $result = 'PASIVA';
              break;
          case 'PND':
              $result = 'PENDAPATAN';
              break;
          case 'PNG':
              $result = 'PENGELUARAN';
              break;            
          default:
              $result = '';
              break;
      }
      return $result;
  }

  public function save()
  {
    $data = html_escape($this->input->post());
    $tahun = $data['tahun'];
    foreach($data['akun_id'] as $key => $val) {
      $saldo_awal = @$data['saldo_awal_'.$tahun][$key];
      if($saldo_awal != '') {
        $akun_id = $val;
        $data_main['saldo_awal_'.$tahun] = clear_numeric($saldo_awal);
        $this->db->where('akun_id', $akun_id)->update('mst_akun', $data_main);
      }
    }    
  }

  
}