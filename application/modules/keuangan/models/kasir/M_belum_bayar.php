<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_belum_bayar extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 AND a.bayar_st != 1 AND a.is_billing = 1 ";
    if (@$cookie['search']['tgl_registrasi_from'] != '' && @$cookie['search']['tgl_registrasi_to']) {
      $where .= "AND (DATE(a.tgl_registrasi) BETWEEN '" . to_date(@$cookie['search']['tgl_registrasi_from']) . "' AND '" . to_date(@$cookie['search']['tgl_registrasi_to']) . "')";
    } else {
      $where .= "AND DATE(a.tgl_registrasi) = '" . date('Y-m-d') . "'";
    }
    if (@$cookie['search']['no_rm_nm'] != '') {
      $where .= "AND (a.pasien_id LIKE '%" . $this->db->escape_like_str($cookie['search']['no_rm_nm']) . "%' OR a.pasien_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['no_rm_nm']) . "%' ) ";
    }
    if (@$cookie['search']['lokasi_kasir'] != '') {
      $where .= "AND b.map_lokasi_kasir = '" . $this->db->escape_like_str($cookie['search']['lokasi_kasir']) . "' ";
    }
    if (@$cookie['search']['lokasi_pelayanan'] != '') {
      $where .= "AND a.lokasi_id = '" . $this->db->escape_like_str($cookie['search']['lokasi_pelayanan']) . "' ";
    }
    if (@$cookie['search']['jenispasien_id'] != '') {
      $where .= "AND a.jenispasien_id = '" . $this->db->escape_like_str($cookie['search']['jenispasien_id']) . "' ";
    }
    if (@$cookie['search']['is_ranap'] != '') {
      if (@$cookie['search']['is_ranap'] == 'rajal') {
        $where .= "AND a.is_ranap = '0' ";
      } elseif (@$cookie['search']['is_ranap'] == 'ranap') {
        $where .= "AND a.is_ranap = '1' ";
      }
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT a.*, b.lokasi_nm, c.jenispasien_nm 
            FROM reg_pasien a
            LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
            $where
            GROUP BY a.groupreg_id
            ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    $res = $query->result_array();
    foreach ($res as $key => $value) {
      $res[$key]['check_pendaftaran'] = $this->check_pendaftaran($value['reg_id']);
      $res[$key]['check_poli'] = $this->check_poli($value['reg_id'], $value['lokasi_id']);
      $res[$key]['check_resep_exist'] = $this->check_resep_exist($value['reg_id']);
      $res[$key]['check_resep'] = $this->check_resep($value['reg_id']);
    }
    return $res;
  }

  public function check_resep_exist($reg_id)
  {
    return $this->db->query(
      "SELECT a.* 
      FROM dat_resep a
      WHERE 
        a.reg_id = $reg_id"
    )->row_array();
  }

  public function check_resep($reg_id)
  {
    return $this->db->query(
      "SELECT a.* 
      FROM dat_resep a
      WHERE 
        a.reg_id = $reg_id ORDER BY a.resep_id DESC"
    )->row_array();
  }

  public function check_pendaftaran($reg_id)
  {
    return $this->db->query(
      "SELECT a.* 
      FROM dat_tindakan a
      WHERE 
        a.reg_id = $reg_id AND 
        a.tarif_id LIKE '09.01%'"
    )->row_array();
  }

  public function check_poli($reg_id, $lokasi_id)
  {
    return $this->db->query(
      "SELECT a.* 
      FROM dat_tindakan a
      WHERE 
        a.reg_id = $reg_id AND 
        a.lokasi_id = $lokasi_id AND
        a.tarif_id NOT LIKE '09.01%'"
    )->row_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 AND a.bayar_st !=1 ";

    $sql = "SELECT * FROM reg_pasien a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total 
            FROM reg_pasien a 
            LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
            $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  public function delete_and_insert_tindakan_keperawatan($reg_id, $groupreg_in)
  {
    $reg = $this->db->where('reg_id', $reg_id)->get('reg_pasien')->row_array();
    if ($reg['is_ranap'] == 1) {
      // delete semua tindakan keperawatan
      $this->db->query("DELETE FROM dat_tindakan WHERE '$groupreg_in' LIKE CONCAT('%', reg_id,';%') AND tarif_id = '04.01.001' AND reg_id !='$reg_id'");
      // insert 1 tindakan keperawatan
      $cek = $this->db->where('reg_id', $reg_id)->where('tarif_id', '04.01.001')->get('dat_tindakan')->row_array();
      if ($cek == null) {
        // $tarif_kelas = $this->db->where('tarif_id', '04.01.001')->where('kelas_id', @$reg['kelas_id'])->get('mst_tarif_kelas')->row_array();
        $sql_tarif_kelas = "SELECT a.tarif_nm, b.*  
                            FROM mst_tarif a 
                            LEFT JOIN mst_tarif_kelas b ON a.tarif_id = b.tarif_id
                            WHERE a.tarif_id='04.01.001' AND b.kelas_id='" . @$reg['kelas_id'] . "'";
        $tarif_kelas = $this->db->query($sql_tarif_kelas)->row_array();
        $rinc = array(
          'tindakan_id' => get_id('dat_tindakan'),
          'reg_id' => @$reg_id,
          'pasien_id' => @$reg['pasien_id'],
          'lokasi_id' => @$reg['lokasi_id'],
          'src_lokasi_id' => @$reg['src_lokasi_id'],
          'kelas_id' => @$reg['kelas_id'],
          'tarif_id' => '04.01.001',
          'tarif_nm' => @$tarif_kelas['tarif_nm'],
          'js' => @$tarif_kelas['js'],
          'jp' => @$tarif_kelas['jp'],
          'jb' => @$tarif_kelas['jb'],
          'nom_tarif' => @$tarif_kelas['nominal'],
          'qty' => '1',
          'jml_awal' => @$tarif_kelas['nominal'],
          'jml_tagihan' => @$tarif_kelas['nominal'],
          'petugas_id' => @$reg['dokter_id'],
          'tgl_catat' => date('Y-m-d H:i:s'),
          'user_cd' => $this->session->userdata('sess_user_cd'),
          'created_by' => $this->session->userdata('sess_user_realname'),
          'created_at' => date('Y-m-d H:i:s'),
        );
        if ($this->db->insert('dat_tindakan', $rinc)) {
          update_id('dat_tindakan', $rinc['tindakan_id']);
        }
      }
    }
  }

  function get_data($id, $groupreg_in)
  {
    $reg = $this->db->where('reg_id', $id)->get('reg_pasien')->row_array();
    $billing = $this->db
      ->where('split_id', 0)
      ->where('groupreg_id', $id)
      ->get('dat_billing')->row_array();
    $rincian = $this->list_rincian($id, @$billing['billing_id'], $reg['groupreg_in']);

    $main = $this->get_data_pembayaran($id, $reg['groupreg_in']);

    $raw_reg_in = explode(';', $groupreg_in);
    $reg_pertama = $this->db->where('reg_id', $raw_reg_in[0])->get('reg_pasien')->row_array();

    //jika billing tidak ada
    if ($billing == null) {
      $last = $this->m_belum_bayar->get_last();
      if ($last == null) {
        $no = 1;
      } else {
        $raw_no = explode('/', $last['no_invoice']);
        $no = intval($raw_no[0]) + 1;
      }
      $dat_billing = array(
        'billing_id' => get_id('dat_billing'),
        'no_invoice' => $no . '/INV/RSIAPMT-' . @$reg['jenispasien_nm'] . '/' . to_rome(date('m')) . '/' . date('Y'),
        'tgl_transaksi' => date('Y-m-d H:i:s'),
        'groupreg_id' => $reg['groupreg_id'],
        'pasien_id' => $reg['pasien_id'],
        'pasien_nm' => $reg['pasien_nm'],
        'lokasi_id' => $reg['lokasi_id'],
        'jenispasien_id' => $reg['jenispasien_id'],
        'tgl_masuk' => (@$reg_pertama['tgl_registrasi']) ? $reg_pertama['tgl_registrasi'] : $reg['tgl_registrasi'],
        'tgl_keluar' => $reg['tgl_pulang'],
        'grand_jml_tindakan' => 0,
        'grand_jml_obat' => 0,
        'grand_jml_alkes' => 0,
        'grand_jml_bhp' => 0,
        'grand_total_bruto' => 0,
        'grand_total_potongan' => 0,
        'grand_total_netto' => 0,
        'grand_pembulatan' => 0,
        'grand_total_tagihan' => 0,
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $this->session->userdata('sess_user_realname'),
      );

      $dat_billing['grand_jml_obat'] = $main['grand_jml_awal_obat'] - $main['grand_jml_potongan_obat'];
      $dat_billing['grand_jml_bhp'] = $main['grand_jml_awal_bhp'] - $main['grand_jml_potongan_bhp'];
      $dat_billing['grand_jml_alkes'] = $main['grand_jml_awal_alkes'] - $main['grand_jml_potongan_alkes'];
      foreach ($rincian as $k => $v) {
        $dat_billing['grand_jml_tindakan'] += $v['jml_tagihan'];
      }

      $dat_billing['grand_total_bruto'] = $dat_billing['grand_jml_tindakan'] + $dat_billing['grand_jml_obat'] + $dat_billing['grand_jml_alkes'] + $dat_billing['grand_jml_bhp'];

      // $dat_billing['potongan_cd'] = ($reg['jenispasien_id'] == '02') ? '01' : '';
      if ($reg['jenispasien_id'] == '02') {
        if ($reg['status_ranap_cd'] == 'NK') {
          // potongan ketika naik kelas / potonga INACBGS
          $dat_billing['potongan_cd'] = '04';
        } else {
          // potongan BPJS
          $dat_billing['potongan_cd'] = '01';
        }
      } else {
        $dat_billing['potongan_cd'] = '';
      }

      if ($reg['jenispasien_id'] == '02') {
        if ($reg['status_ranap_cd'] == 'NK') {
          //pasien naik kelas
          $dat_billing['grand_total_potongan'] = 0;
        } else {
          $dat_billing['grand_total_potongan'] = $dat_billing['grand_total_bruto'];
        }
        //$dat_billing['grand_total_potongan'] = ($reg['jenispasien_id'] == '02') ? $dat_billing['grand_total_bruto'] : 0;
      } else {
        // pasien umum
        $dat_billing['grand_total_potongan'] = 0;
      }

      $dat_billing['grand_total_netto'] = $dat_billing['grand_total_bruto'] - $dat_billing['grand_total_potongan'];
      $dat_billing['grand_total_tagihan'] = $dat_billing['grand_total_netto'] + $dat_billing['grand_pembulatan'];

      //insert dat billing
      if ($this->db->insert('dat_billing', $dat_billing)) {
        update_id('dat_billing', $dat_billing['billing_id']);

        //insert into billing_rinc
        //insert tindakan
        foreach ($rincian as $k => $v) {
          $rinc = array(
            'billingrinc_id' => get_id('dat_billing_rinc'),
            'billing_id' => $dat_billing['billing_id'],
            'tarif_id' => $v['tarif_id'],
            'jml_seharusnya' => ($v['jml_seharusnya'] == null) ? 0 : $v['jml_seharusnya'],
            'jml_potongan' => ($v['jml_potongan'] == null) ? 0 : $v['jml_potongan'],
            'jml_tagihan' => ($v['jml_tagihan'] == null) ? 0 : $v['jml_tagihan'],
            'data_tp' => $v['tarif_tp'],
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->session->userdata('sess_user_realname'),
          );
          if ($this->db->insert('dat_billing_rinc', $rinc)) {
            update_id('dat_billing_rinc', $rinc['billingrinc_id']);
          }
        }

        //insert obat
        $rinc = array(
          'billingrinc_id' => get_id('dat_billing_rinc'),
          'billing_id' => $dat_billing['billing_id'],
          'tarif_id' => 'OBAT',
          'jml_seharusnya' => ($main['grand_jml_awal_obat'] == null) ? 0 : $main['grand_jml_awal_obat'],
          'jml_potongan' => ($main['grand_jml_potongan_obat'] == null) ? 0 : $main['grand_jml_potongan_obat'],
          'data_tp' => 'G',
          'created_at' => date('Y-m-d H:i:s'),
          'created_by' => $this->session->userdata('sess_user_realname'),
        );
        $rinc['jml_tagihan'] = $rinc['jml_seharusnya'] - $rinc['jml_potongan'];
        if ($this->db->insert('dat_billing_rinc', $rinc)) {
          update_id('dat_billing_rinc', $rinc['billingrinc_id']);
        }

        //alkes
        $rinc = array(
          'billingrinc_id' => get_id('dat_billing_rinc'),
          'billing_id' => $dat_billing['billing_id'],
          'tarif_id' => 'ALKES',
          'jml_seharusnya' => ($main['grand_jml_awal_alkes'] == null) ? 0 : $main['grand_jml_awal_alkes'],
          'jml_potongan' => ($main['grand_jml_potongan_alkes'] == null) ? 0 : $main['grand_jml_potongan_alkes'],
          'data_tp' => 'G',
          'created_at' => date('Y-m-d H:i:s'),
          'created_by' => $this->session->userdata('sess_user_realname'),
        );
        $rinc['jml_tagihan'] = $rinc['jml_seharusnya'] - $rinc['jml_potongan'];
        if ($this->db->insert('dat_billing_rinc', $rinc)) {
          update_id('dat_billing_rinc', $rinc['billingrinc_id']);
        }

        //bmhp
        $rinc = array(
          'billingrinc_id' => get_id('dat_billing_rinc'),
          'billing_id' => $dat_billing['billing_id'],
          'tarif_id' => 'BMHP',
          'jml_seharusnya' => ($main['grand_jml_awal_bhp'] == null) ? 0 : $main['grand_jml_awal_bhp'],
          'jml_potongan' => ($main['grand_jml_potongan_bhp'] == null) ? 0 : $main['grand_jml_potongan_bhp'],
          'data_tp' => 'G',
          'created_at' => date('Y-m-d H:i:s'),
          'created_by' => $this->session->userdata('sess_user_realname'),
        );
        $rinc['jml_tagihan'] = $rinc['jml_seharusnya'] - $rinc['jml_potongan'];
        if ($this->db->insert('dat_billing_rinc', $rinc)) {
          update_id('dat_billing_rinc', $rinc['billingrinc_id']);
        }
      };
    }

    //jika billing ada
    if ($billing != null) {
      //update rincian 
      foreach ($rincian as $k => $v) {
        $rinc = array(
          'billing_id' => $billing['billing_id'],
          'tarif_id' => $v['tarif_id'],
          'jml_seharusnya' => ($v['jml_seharusnya'] == null) ? 0 : $v['jml_seharusnya'],
          'jml_potongan' => ($v['jml_potongan'] == null) ? 0 : $v['jml_potongan'],
          'jml_tagihan' => ($v['jml_tagihan'] == null) ? 0 : $v['jml_tagihan'],
          'data_tp' => $v['tarif_tp'],
          'updated_at' => date('Y-m-d H:i:s'),
          'updated_by' => $this->session->userdata('sess_user_realname'),
        );
        $this->db
          ->where('billing_id', $billing['billing_id'])
          ->where('tarif_id', $v['tarif_id'])
          ->update('dat_billing_rinc', $rinc);
      }

      //update obat
      $rinc = array(
        'billing_id' => $billing['billing_id'],
        'tarif_id' => 'OBAT',
        'jml_seharusnya' => ($main['grand_jml_awal_obat'] == null) ? 0 : $main['grand_jml_awal_obat'],
        'jml_potongan' => ($main['grand_jml_potongan_obat'] == null) ? 0 : $main['grand_jml_potongan_obat'],
        'data_tp' => 'G',
        'updated_at' => date('Y-m-d H:i:s'),
        'updated_by' => $this->session->userdata('sess_user_realname'),
      );
      $rinc['jml_tagihan'] = $rinc['jml_seharusnya'] - $rinc['jml_potongan'];
      $this->db
        ->where('billing_id', $billing['billing_id'])
        ->where('tarif_id', 'OBAT')
        ->update('dat_billing_rinc', $rinc);

      //update alkes
      $rinc = array(
        'billing_id' => $billing['billing_id'],
        'tarif_id' => 'ALKES',
        'jml_seharusnya' => ($main['grand_jml_awal_alkes'] == null) ? 0 : $main['grand_jml_awal_alkes'],
        'jml_potongan' => ($main['grand_jml_potongan_alkes'] == null) ? 0 : $main['grand_jml_potongan_alkes'],
        'data_tp' => 'G',
        'updated_at' => date('Y-m-d H:i:s'),
        'updated_by' => $this->session->userdata('sess_user_realname'),
      );
      $rinc['jml_tagihan'] = $rinc['jml_seharusnya'] - $rinc['jml_potongan'];
      $this->db
        ->where('billing_id', $billing['billing_id'])
        ->where('tarif_id', 'ALKES')
        ->update('dat_billing_rinc', $rinc);

      //update BHP
      $rinc = array(
        'billing_id' => $billing['billing_id'],
        'tarif_id' => 'BMHP',
        'jml_seharusnya' => ($main['grand_jml_awal_bhp'] == null) ? 0 : $main['grand_jml_awal_bhp'],
        'jml_potongan' => ($main['grand_jml_potongan_bhp'] == null) ? 0 : $main['grand_jml_potongan_bhp'],
        'data_tp' => 'G',
        'updated_at' => date('Y-m-d H:i:s'),
        'updated_by' => $this->session->userdata('sess_user_realname'),
      );
      $rinc['jml_tagihan'] = $rinc['jml_seharusnya'] - $rinc['jml_potongan'];
      $this->db
        ->where('billing_id', $billing['billing_id'])
        ->where('tarif_id', 'BMHP')
        ->update('dat_billing_rinc', $rinc);

      //UPDATE dat_billing
      $new_rinc = $this->db->where('billing_id', $billing['billing_id'])->get('dat_billing_rinc')->result_array();

      $dat_billing = array(
        'grand_jml_tindakan' => 0,
        'grand_jml_obat' => 0,
        'grand_jml_alkes' => 0,
        'grand_jml_bhp' => 0,
        'grand_total_bruto' => 0,
        'grand_total_potongan' => 0,
        'grand_total_netto' => 0,
        'grand_pembulatan' => 0,
        'grand_total_tagihan' => 0,
      );

      foreach ($new_rinc as $k => $v) {
        if ($v['tarif_id'] == 'OBAT') {
          $dat_billing['grand_jml_obat'] = $v['jml_tagihan'];
        } else if ($v['tarif_id'] == 'ALKES') {
          $dat_billing['grand_jml_alkes'] = $v['jml_tagihan'];
        } else if ($v['tarif_id'] == 'BMHP') {
          $dat_billing['grand_jml_bhp'] = $v['jml_tagihan'];
        } else {
          $dat_billing['grand_jml_tindakan'] += $v['jml_tagihan'];
        }
        $dat_billing['grand_total_bruto'] += $v['jml_tagihan'];
      }

      // $dat_billing['potongan_cd'] = ($reg['jenispasien_id'] == '02') ? '01' : $billing['potongan_cd'];
      if ($reg['jenispasien_id'] == '02') {
        if ($reg['status_ranap_cd'] == 'NK') {
          $dat_billing['potongan_cd'] = '04';
        } else {
          $dat_billing['potongan_cd'] = '01';
        }
      } else {
        $dat_billing['potongan_cd'] = $billing['potongan_cd'];
      }

      // $dat_billing['grand_total_potongan'] = ($reg['jenispasien_id'] == '02') ? $dat_billing['grand_total_bruto'] : $billing['grand_total_potongan'];
      if ($reg['jenispasien_id'] == '02') {
        if ($reg['status_ranap_cd'] == 'NK') {
          //pasien naik kelas
          if ($billing['grand_total_potongan'] != 0) {
            $dat_billing['grand_total_potongan'] = $billing['grand_total_potongan'];
          } else {
            $dat_billing['grand_total_potongan'] = 0;
          }
        } else {
          $dat_billing['grand_total_potongan'] = $dat_billing['grand_total_bruto'];
        }
      } else {
        // pasien umum
        $dat_billing['grand_total_potongan'] = $billing['grand_total_potongan'];
      }

      $dat_billing['grand_total_netto'] = $dat_billing['grand_total_bruto'] - $dat_billing['grand_total_potongan'];
      $dat_billing['grand_total_tagihan'] = $dat_billing['grand_total_netto'] + $dat_billing['grand_pembulatan'];

      //update billing
      $this->db->where('split_id', 0)->where('billing_id', $billing['billing_id'])->update('dat_billing', $dat_billing);
    }

    $sql = "SELECT
              a.*,
              b.pegawai_nm,
              c.jenispasien_nm,
              d.lokasi_nm,
              d.map_lokasi_depo,
              d.jenisreg_st,
              e.kelas_nm,
              f.billing_id,
              f.no_invoice,
              f.grand_jml_tindakan,
              f.grand_jml_obat,
              f.grand_jml_alkes,
              f.grand_jml_bhp,
              f.grand_total_bruto,
              f.grand_total_potongan,
              f.grand_total_netto,
              f.grand_pembulatan,
              f.grand_total_tagihan,
              f.tgl_bayar,
              f.grand_titip_bayar,
              f.grand_total_bayar,
              f.keterangan_bayar,
              f.tgl_transaksi,
              f.potongan_cd,
              k.pegawai_nm AS dokter_nm,
              k.spesialisasi_cd, 
              l.parameter_val AS spesialisasi_nm,
              m.kelas_nm AS src_kelas_nm 
            FROM
              reg_pasien a
              LEFT JOIN mst_pegawai b ON a.dokter_id = b.pegawai_id
              LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
              LEFT JOIN mst_lokasi d ON a.lokasi_id = d.lokasi_id
              LEFT JOIN mst_kelas e ON a.kelas_id = e.kelas_id
              LEFT JOIN dat_billing f ON a.groupreg_id = f.groupreg_id 
              LEFT JOIN mst_pegawai k ON a.dokter_id = k.pegawai_id
              LEFT JOIN mst_parameter l ON k.spesialisasi_cd = l.parameter_cd AND l.parameter_field = 'spesialisasi_cd'
              LEFT JOIN mst_kelas m ON a.src_kelas_id = m.kelas_id
            WHERE
              a.groupreg_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    if ($row['sebutan_cd'] != '') {
      $row['sebutan_cd'] = ', ' . $row['sebutan_cd'];
    }
    return $row;
  }

  public function list_resep($reg_id = null, $pasien_id = null)
  {
    $sql = "SELECT a.resep_id, a.resep_st 
            FROM dat_resep a
            WHERE a.reg_id=? AND a.pasien_id=?";
    $query = $this->db->query($sql, array($reg_id, $pasien_id));
    return $query->result_array();
  }

  function get_data_pembayaran($id, $groupreg_in)
  {
    $sql = "SELECT
              a.*,
              b.pegawai_nm,
              c.jenispasien_nm,
              d.lokasi_nm,
              d.map_lokasi_depo,
              d.jenisreg_st,
              e.kelas_nm,
              f.billing_id,
              f.no_invoice,
              f.grand_jml_tindakan,
              f.grand_jml_obat,
              f.grand_jml_alkes,
              f.grand_jml_bhp,
              f.grand_total_bruto,
              f.grand_total_potongan,
              f.grand_total_netto,
              f.grand_pembulatan,
              f.grand_total_tagihan,
              f.tgl_bayar,
              f.grand_titip_bayar,
              f.grand_total_bayar,
              f.keterangan_bayar,
              f.tgl_transaksi,
              f.potongan_cd,
              SUM(g.grand_jml_tagihan) AS grand_jml_obat,
              SUM(g.grand_jml_potongan) AS grand_jml_potongan_obat,
              SUM(g.grand_jml_awal) AS grand_jml_awal_obat,
              SUM(h.jml_tagihan) AS grand_jml_bhp,
              SUM(h.jml_potongan) AS grand_jml_potongan_bhp,
              SUM(h.jml_awal) AS grand_jml_awal_bhp,
              SUM(i.jml_tagihan) AS grand_jml_alkes,
              SUM(i.jml_potongan) AS grand_jml_potongan_alkes,
              SUM(i.jml_awal) AS grand_jml_awal_alkes,
              (SUM(g.grand_jml_tagihan) + SUM(h.jml_tagihan) + SUM(i.jml_tagihan)) AS total_bruto,
              k.pegawai_nm AS dokter_nm,
              l.parameter_val AS spesialisasi_nm 
            FROM
              reg_pasien a
              LEFT JOIN mst_pegawai b ON a.dokter_id = b.pegawai_id
              LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
              LEFT JOIN mst_lokasi d ON a.lokasi_id = d.lokasi_id
              LEFT JOIN mst_kelas e ON a.kelas_id = e.kelas_id
              LEFT JOIN dat_billing f ON a.groupreg_id = f.groupreg_id 
              LEFT JOIN 
              (
                SELECT 
                '$groupreg_in' as groupreg_in,
                SUM(grand_jml_tagihan) as grand_jml_tagihan,
                SUM(grand_jml_potongan) as grand_jml_potongan, 
                (SUM(grand_jml_tagihan) + SUM(grand_jml_potongan)) AS grand_jml_awal 
                FROM dat_resep 
                WHERE '$groupreg_in' LIKE CONCAT('%', reg_id,';%')
              ) g ON CONVERT(a.groupreg_in USING utf8) = CONVERT(g.groupreg_in USING utf8) 
              LEFT JOIN 
              (
                SELECT 
                '$groupreg_in' as groupreg_in,
                SUM(jml_tagihan) as jml_tagihan,
                SUM(jml_potongan) as jml_potongan, 
                SUM(jml_awal) as jml_awal 
                FROM dat_bhp 
                WHERE '$groupreg_in' LIKE CONCAT('%', reg_id,';%')                
              ) h ON CONVERT(a.groupreg_in USING utf8) = CONVERT(h.groupreg_in USING utf8) 
              LEFT JOIN 
              (
                SELECT 
                '$groupreg_in' as groupreg_in,
                SUM(jml_tagihan) as jml_tagihan,
                SUM(jml_potongan) as jml_potongan, 
                SUM(jml_awal) as jml_awal 
                FROM dat_alkes 
                WHERE '$groupreg_in' LIKE CONCAT('%', reg_id,';%')
              ) i ON CONVERT(a.groupreg_in USING utf8) = CONVERT(i.groupreg_in USING utf8) 
              LEFT JOIN mst_pegawai k ON a.dokter_id = k.pegawai_id
              LEFT JOIN mst_parameter l ON k.spesialisasi_cd = l.parameter_cd AND l.parameter_field = 'spesialisasi_cd'
            WHERE
              a.groupreg_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    if ($row['sebutan_cd'] != '') {
      $row['sebutan_cd'] = ', ' . $row['sebutan_cd'];
    }
    return $row;
  }

  public function list_detail_obat($reg_id = null, $pasien_id = null)
  {
    $sql = "SELECT
              farmasi_id,
              resep_id,
              reg_id,
              pasien_id,
              stokdepo_id,
              obat_id,
              obat_nm,
              harga,
              qty,
              jumlah_awal,
              potongan,
              jumlah_akhir 
            FROM
              dat_farmasi 
            WHERE
              reg_id = ? 
              AND pasien_id = ?";
    $query = $this->db->query($sql, array($reg_id, $pasien_id));
    return $query->result_array();
  }

  public function farmasi_get($id)
  {
    return $this->db->where('farmasi_id', $id)->get('dat_farmasi')->row_array();
  }

  public function resep_get($resep_id)
  {
    return $this->db->where('resep_id', $resep_id)->get('dat_resep')->row_array();
  }

  public function obat_list_row($obat_id = null, $stokdepo_id = null)
  {
    $sql = "SELECT a.* 
            FROM far_stok_depo a
            WHERE a.obat_id=? AND a.stokdepo_id=?";
    $query = $this->db->query($sql, array($obat_id, $stokdepo_id));
    return $query->row_array();
  }

  public function obat_get()
  {
    $data = $this->input->post();
    $sql = "SELECT 
              *
            FROM far_stok_depo a 
            WHERE 
              stokdepo_id='" . $data['stokdepo_id'] . "'";
    $query = $this->db->query($sql);
    return $query->row_array();
  }

  public function obat_autocomplete_list($obat_nm = null, $map_lokasi_depo = null)
  {
    $sql = "SELECT 
              *
            FROM far_stok_depo a 
            WHERE 
              (a.obat_nm LIKE '%$obat_nm%' OR a.obat_id LIKE '%$obat_nm%') AND
              a.stok_st = 1 AND a.stok_akhir > 0 AND a.lokasi_id = '$map_lokasi_depo'
            ORDER BY a.obat_id";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['obat_id'] . "#" . $row['obat_nm'] . "#" . $row['stokdepo_id'],
        'text' => $row['obat_id'] . ' - ' . $row['obat_nm']
      );
    }
    return $res;
  }

  public function get_registrasi($reg_id)
  {
    $data = $this->db->query(
      "SELECT a.*
        FROM reg_pasien a
        WHERE a.reg_id = '$reg_id'"
    )->row_array();

    return $data;
  }

  public function farmasi_save()
  {
    $data = $this->input->post();
    $obat = explode("#", $data['obat_id']);

    if ($data['resep_id'] == '') {
      // insert dat_resep
      $dr = array(
        'resep_id' => get_id('dat_resep'),
        'reg_id' => $data['reg_id'],
        'dokter_id' => $data['dokter_id'],
        'pasien_id' => $data['pasien_id'],
        'lokasi_id' => $data['lokasi_id'],
        'tgl_catat' => date('Y-m-d H:i:s'),
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $this->session->userdata('sess_user_realname'),
        'user_cd' => $this->session->userdata('sess_user_cd')
      );
      $this->db->insert('dat_resep', $dr);
      update_id('dat_resep', $dr['resep_id']);
      //insert new
      $d = array(
        'farmasi_id' => get_id('dat_farmasi'),
        'resep_id' => $dr['resep_id'],
        'reg_id' => $data['reg_id'],
        'dokter_id' => $data['dokter_id'],
        'pasien_id' => $data['pasien_id'],
        'lokasi_id' => $data['lokasi_id'],
        'stokdepo_id' => $obat[2],
        'obat_id' => $obat[0],
        'obat_nm' => $obat[1],
        'harga' => clear_numeric($data['harga']),
        'qty' => clear_numeric($data['qty']),
        'jumlah_awal' => clear_numeric($data['jumlah_awal']),
        'potongan' => clear_numeric($data['potongan']),
        'jumlah_akhir' => clear_numeric($data['jumlah_akhir']),
        'aturan_pakai' => $data['aturan_pakai'],
        'jadwal' => $data['jadwal'],
        'aturan_tambahan' => $data['aturan_tambahan'],
        'tgl_catat' => date('Y-m-d H:i:s'),
        'user_cd' => $this->session->userdata('sess_user_cd'),
        'created_by' => $this->session->userdata('sess_user_realname'),
        'created_at' => date('Y-m-d H:i:s'),
      );

      $this->db->insert('dat_farmasi', $d);
      update_id('dat_farmasi', $d['farmasi_id']);

      //update stok
      $stokdepo = $this->db->where('stokdepo_id', $d['stokdepo_id'])->get('far_stok_depo')->row_array();
      $stok_terpakai = $stokdepo['stok_terpakai'] + $d['qty'];
      $stok_akhir = $stokdepo['stok_awal'] + $stokdepo['stok_penerimaan'] - $stok_terpakai;
      // $stok_akhir = $stokdepo['stok_penerimaan'] - $stok_terpakai - $stokdepo['stok_retur_keluar'];
      $ds = array(
        'stok_terpakai' => $stok_terpakai,
        'stok_akhir' => $stok_akhir
      );
      $this->db->where('stokdepo_id', $d['stokdepo_id'])->update('far_stok_depo', $ds);
      $this->farmasi_grand($d['resep_id']);
    } else {
      if ($data['farmasi_id'] == '') {
        //insert new
        $d = array(
          'farmasi_id' => get_id('dat_farmasi'),
          'resep_id' => $data['resep_id'],
          'reg_id' => $data['reg_id'],
          'dokter_id' => $data['dokter_id'],
          'pasien_id' => $data['pasien_id'],
          'lokasi_id' => $data['lokasi_id'],
          'stokdepo_id' => $obat[2],
          'obat_id' => $obat[0],
          'obat_nm' => $obat[1],
          'harga' => clear_numeric($data['harga']),
          'qty' => clear_numeric($data['qty']),
          'jumlah_awal' => clear_numeric($data['jumlah_awal']),
          'potongan' => clear_numeric($data['potongan']),
          'jumlah_akhir' => clear_numeric($data['jumlah_akhir']),
          'aturan_pakai' => $data['aturan_pakai'],
          'jadwal' => $data['jadwal'],
          'aturan_tambahan' => $data['aturan_tambahan'],
          'tgl_catat' => date('Y-m-d H:i:s'),
          'user_cd' => $this->session->userdata('sess_user_cd'),
          'created_by' => $this->session->userdata('sess_user_realname'),
          'created_at' => date('Y-m-d H:i:s'),
        );

        $this->db->insert('dat_farmasi', $d);
        update_id('dat_farmasi', $d['farmasi_id']);

        //update stok
        $stokdepo = $this->db->where('stokdepo_id', $d['stokdepo_id'])->get('far_stok_depo')->row_array();
        $stok_terpakai = $stokdepo['stok_terpakai'] + $d['qty'];
        $stok_akhir = $stokdepo['stok_awal'] + $stokdepo['stok_penerimaan'] - $stok_terpakai;
        // $stok_akhir = $stokdepo['stok_penerimaan'] - $stok_terpakai - $stokdepo['stok_retur_keluar'];
        $ds = array(
          'stok_terpakai' => $stok_terpakai,
          'stok_akhir' => $stok_akhir
        );
        $this->db->where('stokdepo_id', $d['stokdepo_id'])->update('far_stok_depo', $ds);
        $this->farmasi_grand($d['resep_id']);
      } else {
        //get data
        $farmasi = $this->db->where('farmasi_id', $data['farmasi_id'])->get('dat_farmasi')->row_array();
        $stokdepo = $this->db->where('stokdepo_id', $farmasi['stokdepo_id'])->get('far_stok_depo')->row_array();
        //update stok depo
        $stok_terpakai = $stokdepo['stok_terpakai'] - $farmasi['qty'];
        $stok_akhir = $stokdepo['stok_awal'] + $stokdepo['stok_penerimaan'] - $stok_terpakai;
        // $stok_akhir = $stokdepo['stok_penerimaan'] - $stok_terpakai - $stokdepo['stok_retur_keluar'];
        $ds = array(
          'stok_terpakai' => $stok_terpakai,
          'stok_akhir' => $stok_akhir
        );
        $this->db->where('stokdepo_id', $stokdepo['stokdepo_id'])->update('far_stok_depo', $ds);
        //update data farmasi
        $d = array(
          'resep_id' => $data['resep_id'],
          'reg_id' => $data['reg_id'],
          'dokter_id' => $data['dokter_id'],
          'pasien_id' => $data['pasien_id'],
          'lokasi_id' => $data['lokasi_id'],
          'stokdepo_id' => $obat[2],
          'obat_id' => $obat[0],
          'obat_nm' => $obat[1],
          'harga' => clear_numeric($data['harga']),
          'qty' => clear_numeric($data['qty']),
          'jumlah_awal' => clear_numeric($data['jumlah_awal']),
          'potongan' => clear_numeric($data['potongan']),
          'jumlah_akhir' => clear_numeric($data['jumlah_akhir']),
          'aturan_pakai' => $data['aturan_pakai'],
          'jadwal' => $data['jadwal'],
          'aturan_tambahan' => $data['aturan_tambahan'],
          'tgl_catat' => date('Y-m-d H:i:s'),
          'user_cd' => $this->session->userdata('sess_user_cd'),
          'created_by' => $this->session->userdata('sess_user_realname'),
          'created_at' => date('Y-m-d H:i:s'),
        );
        $this->db->where('farmasi_id', $data['farmasi_id'])->update('dat_farmasi', $d);
        //update stok
        $stokdepo = $this->db->where('stokdepo_id', $d['stokdepo_id'])->get('far_stok_depo')->row_array();
        $stok_terpakai = $stokdepo['stok_terpakai'] + $d['qty'];
        $stok_akhir = $stokdepo['stok_awal'] + $stokdepo['stok_penerimaan'] - $stok_terpakai;
        // $stok_akhir = $stokdepo['stok_penerimaan'] - $stok_terpakai - $stokdepo['stok_retur_keluar'];
        $ds = array(
          'stok_terpakai' => $stok_terpakai,
          'stok_akhir' => $stok_akhir
        );
        $this->db->where('stokdepo_id', $d['stokdepo_id'])->update('far_stok_depo', $ds);
        $this->farmasi_grand($d['resep_id']);
      }
    }
  }

  public function farmasi_grand($resep_id)
  {
    $farmasi = $this->db->where('resep_id', $resep_id)->get('dat_farmasi')->result_array();
    $resep = $this->db->where('resep_id', $resep_id)->get('dat_resep')->row_array();
    $grand_jml_bruto = 0;
    foreach ($farmasi as $row) {
      $grand_jml_bruto += $row['jumlah_akhir'];
    }
    $grand_nom_ppn = $resep['grand_prs_ppn'] * $grand_jml_bruto;
    $grand_jml_tagihan = $grand_jml_bruto + $grand_nom_ppn + $resep['grand_embalace'] - $resep['grand_jml_potongan'] + $resep['grand_pembulatan'];
    $d = array(
      'grand_jml_bruto' => $grand_jml_bruto,
      'grand_nom_ppn' => $grand_nom_ppn,
      'grand_jml_tagihan' => $grand_jml_tagihan
    );
    $this->db->where('resep_id', $resep_id)->update('dat_resep', $d);
  }

  public function farmasi_delete($farmasi_id)
  {
    //get
    $farmasi = $this->db->where('farmasi_id', $farmasi_id)->get('dat_farmasi')->row_array();
    $stokdepo = $this->db->where('stokdepo_id', $farmasi['stokdepo_id'])->get('far_stok_depo')->row_array();
    //update stok depo
    $stok_terpakai = $stokdepo['stok_terpakai'] - $farmasi['qty'];
    $stok_akhir = $stokdepo['stok_awal'] + $stokdepo['stok_penerimaan'] - $stok_terpakai;
    // $stok_akhir = $stokdepo['stok_penerimaan'] - $stok_terpakai - $stokdepo['stok_retur_keluar'];
    $ds = array(
      'stok_terpakai' => $stok_terpakai,
      'stok_akhir' => $stok_akhir
    );
    $this->db->where('stokdepo_id', $stokdepo['stokdepo_id'])->update('far_stok_depo', $ds);
    //delete farmasi
    $this->db->where('farmasi_id', $farmasi_id)->delete('dat_farmasi');
    $this->farmasi_grand($farmasi['resep_id']);
  }

  function detail_item_pembayaran($groupreg_id = '', $tarif_id = '', $groupreg_in = '')
  {
    $sql = "SELECT 
              a.*,
              b.pegawai_nm AS pegawai_nm_1, 
              c.pegawai_nm AS pegawai_nm_2, 
              d.pegawai_nm AS pegawai_nm_3, 
              e.pegawai_nm AS pegawai_nm_4, 
              f.pegawai_nm AS pegawai_nm_5  
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            LEFT JOIN mst_pegawai c ON a.petugas_id_2 = c.pegawai_id
            LEFT JOIN mst_pegawai d ON a.petugas_id_3 = d.pegawai_id
            LEFT JOIN mst_pegawai e ON a.petugas_id_4 = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_5 = f.pegawai_id  
            WHERE ? LIKE CONCAT('%', a.reg_id,';%') AND left(a.tarif_id,9)=?";
    $query = $this->db->query($sql, array($groupreg_in, $tarif_id));
    $result = $query->result_array();
    return $result;
  }

  function list_rincian($groupreg_id = '', $billing_id = '', $groupreg_in = '', $is_else = '')
  {
    $where = "";
    if ($is_else != '') {
      $where = "AND a.tarif_id !='$is_else'";
    }
    $sql = "SELECT
              a.tarif_id,
              a.tarif_nm,
              a.tarif_tp,
              b.jml_seharusnya,
              b.jml_potongan,
            IF
              ( b.jml_tagihan IS NULL || b.jml_tagihan = 0 || b.jml_tagihan != b.jml_seharusnya, b.jml_seharusnya, b.jml_tagihan ) AS jml_tagihan,
              b.billing_id 
            FROM
              mst_tarif a
              LEFT JOIN (
                (
                SELECT LEFT
                  ( a.tarif_id, 2 ) AS tarif_id,
                  SUM( a.jml_tagihan ) AS jml_seharusnya,
                  b.jml_potongan,
                  b.jml_tagihan,
                  b.billing_id 
                FROM
                  reg_pasien_kamar a
                  LEFT JOIN (
                  SELECT
                    billing_id,
                    LEFT ( tarif_id, 2 ) AS tarif_id,
                    SUM( jml_potongan ) AS jml_potongan,
                    SUM( jml_tagihan ) AS jml_tagihan 
                  FROM
                    dat_billing_rinc 
                  WHERE
                    billing_id = '$billing_id' AND split_id = 0
                  GROUP BY
                    LEFT ( tarif_id, 2 ) 
                  ) b ON LEFT ( a.tarif_id, 2 )= LEFT ( b.tarif_id, 2 ) 
                WHERE
                  '$groupreg_in' LIKE CONCAT( '%', a.reg_id, ';%' ) 
                GROUP BY
                  LEFT ( a.tarif_id, 2 ) 
                ) UNION ALL
                (
                SELECT LEFT
                  ( a.tarif_id, 2 ) AS tarif_id,
                  SUM( a.jml_tagihan ) AS jml_seharusnya,
                  b.jml_potongan,
                  b.jml_tagihan,
                  b.billing_id 
                FROM
                  dat_tindakan a
                  LEFT JOIN (
                  SELECT
                    billing_id,
                    LEFT ( tarif_id, 2 ) AS tarif_id,
                    SUM( jml_potongan ) AS jml_potongan,
                    SUM( jml_tagihan ) AS jml_tagihan 
                  FROM
                    dat_billing_rinc 
                  WHERE
                    billing_id = '$billing_id' AND split_id = 0
                  GROUP BY
                    LEFT ( tarif_id, 2 ) 
                  ) b ON LEFT ( a.tarif_id, 2 )= LEFT ( b.tarif_id, 2 ) 
                WHERE
                  '$groupreg_in' LIKE CONCAT( '%', a.reg_id, ';%' ) AND a.is_bayar = 0
                GROUP BY
                  LEFT ( a.tarif_id, 2 ) 
                ) 
              ) b ON a.tarif_id = b.tarif_id 
            WHERE
              LENGTH( a.tarif_id ) = 2 
              AND a.is_kwitansi = 1 
              $where 
            ORDER BY
              a.tarif_id ASC";
    $query = $this->db->query($sql);
    $result = $query->result_array();

    foreach ($result as $key => $val) {
      $result[$key]['tagihan'] = $val;
      $result[$key]['list_detail'] = $this->list_rincian_detail($groupreg_id, $billing_id, $result[$key]['tarif_id'], $groupreg_in);
    }
    return $result;
  }

  function list_rincian_detail($groupreg_id = '', $billing_id = '', $tarif_id = '', $groupreg_in = '')
  {
    $sql = "SELECT 
             a.*,
             b.jml_seharusnya, 
             b.jml_potongan,
             IF(b.jml_tagihan IS NULL, b.jml_seharusnya, b.jml_tagihan) as jml_tagihan,
             b.billing_id
            FROM mst_tarif a 
            INNER JOIN 
            (
             (
              SELECT  
               LEFT(a.tarif_id,9) as tarif_id,
               SUM(a.jml_tagihan) as jml_seharusnya,
               b.jml_potongan,
               b.jml_tagihan,
               b.billing_id
              FROM reg_pasien_kamar a
              LEFT JOIN 
              (
               SELECT 
                billing_id,
                LEFT(tarif_id,9) as tarif_id,
                SUM(jml_potongan) as jml_potongan,
                SUM(jml_tagihan) as jml_tagihan
               FROM dat_billing_rinc WHERE billing_id='$billing_id' AND split_id = 0 GROUP BY LEFT(tarif_id,9)
              ) b ON LEFT(a.tarif_id,9)=LEFT(b.tarif_id,9)
              WHERE '$groupreg_in' LIKE CONCAT('%', a.reg_id,';%') GROUP BY LEFT(a.tarif_id,9)
             )
             UNION ALL 
             (
              SELECT  
               LEFT(a.tarif_id,9) as tarif_id,
               SUM(a.jml_tagihan) as jml_seharusnya,
               b.jml_potongan,
               b.jml_tagihan,
               b.billing_id
              FROM dat_tindakan a 
              LEFT JOIN 
              (
               SELECT 
                billing_id,
                LEFT(tarif_id,9) as tarif_id,
                SUM(jml_potongan) as jml_potongan,
                SUM(jml_tagihan) as jml_tagihan
               FROM dat_billing_rinc WHERE billing_id='$billing_id' AND split_id = 0 GROUP BY LEFT(tarif_id,9)
              ) b ON LEFT(a.tarif_id,9)=LEFT(b.tarif_id,9)
              WHERE '$groupreg_in' LIKE CONCAT('%', a.reg_id,';%') AND is_bayar = 0 GROUP BY LEFT(a.tarif_id,9)
             )
            ) b ON a.tarif_id=b.tarif_id
            WHERE LENGTH(a.tarif_id)='9' AND LEFT(a.tarif_id,2)='$tarif_id' ORDER BY a.tarif_id ASC";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    foreach ($result as $key => $val) {
      if ($val['parent_id'] == '09.02') {
        $get_mst_tarif = $this->db->query("SELECT tarif_nm FROM mst_tarif WHERE tarif_id = '" . $val['parent_id'] . "'")->row_array();
        $result[$key]['tarif_nm'] = $get_mst_tarif['tarif_nm'];
      }
      $result[$key]['tagihan'] = $val;
    }
    return $result;
  }

  public function cek_bedah_non_bedah($reg_id = '', $pasien_id = '', $tarif_id = '')
  {
    $sql = "SELECT
              COUNT(tindakan_id) AS jml_data
            FROM
              dat_tindakan 
            WHERE
              reg_id = ? 
              AND pasien_id = ?
              AND LEFT(tarif_id,2) = ?";
    $query = $this->db->query($sql, array($reg_id, $pasien_id, $tarif_id));
    $row = $query->row_array();
    // 
    return $row['jml_data'];
  }

  public function get_dat_resep($groupreg_in = '')
  {
    $sql = "SELECT
              SUM( a.grand_jml_bruto ) AS grand_jml_bruto,
              SUM( a.grand_prs_ppn) AS grand_prs_ppn,
              SUM( a.grand_nom_ppn ) AS grand_nom_ppn,
              SUM( a.grand_embalace ) AS grand_embalace,
              SUM( a.grand_jml_potongan ) AS grand_jml_potongan,
              SUM( a.grand_pembulatan ) AS grand_pembulatan,
              SUM( a.grand_jml_tagihan ) AS grand_jml_tagihan 
            FROM
              dat_resep a 
            WHERE
              a.is_deleted = 0 
              AND '$groupreg_in' LIKE CONCAT( '%', a.reg_id, ';%' ) 
            ORDER BY
              a.reg_id";
    $query = $this->db->query($sql);
    $row = $query->row_array();
    // 
    return $row;
  }

  public function get_last_resep($groupreg_in = '')
  {
    $sql = "SELECT
              a.resep_id  
            FROM
              dat_resep a 
            WHERE
              a.is_deleted = 0 
              AND '$groupreg_in' LIKE CONCAT( '%', a.reg_id, ';%' ) 
            ORDER BY
              a.resep_id DESC
            LIMIT 1";
    $query = $this->db->query($sql);
    $row = $query->row_array();
    // 
    return $row;
  }

  public function detail_obat($groupreg_id = '', $groupreg_in = '')
  {
    $sql = "SELECT 
              a.* 
            FROM dat_resep a
            WHERE a.is_deleted=0 AND ? LIKE CONCAT('%', a.reg_id,';%')
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, $groupreg_in);
    $result = $query->result_array();
    // 
    foreach ($result as $key => $val) {
      $result[$key]['list_dat_resep'] = $this->list_dat_resep($val['resep_id'], $groupreg_id, $groupreg_in);
    }
    return $result;
  }

  public function list_dat_resep($resep_id = '', $groupreg_id = '', $groupreg_in = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_farmasi a
            LEFT JOIN mst_obat b ON a.obat_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.resep_id=? AND ? LIKE CONCAT('%', a.reg_id,';%')
            ORDER BY a.reg_id ASC";
    $query = $this->db->query($sql, array($resep_id, $groupreg_in));
    return $query->result_array();
  }

  public function detail_alkes($groupreg_id = '', $groupreg_in = '')
  {
    $sql = "SELECT 
              a.*
            FROM dat_alkes a
            WHERE a.is_deleted=0 AND ? LIKE CONCAT('%', a.reg_id,';%')
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, $groupreg_in);
    return $query->result_array();
  }

  public function alkes_save()
  {
    $data = $this->input->post();
    $barang_id = explode("#", $data['barang_id']);
    $data['barang_id'] = $barang_id[0];
    $data['barang_nm'] = $barang_id[1];
    $data['stokdepo_id'] = @$barang_id[2];

    $far_stok_depo = $this->db->where('stokdepo_id', @$barang_id[2])->get('far_stok_depo')->row_array();

    $data['stokdepo_id'] = $far_stok_depo['stokdepo_id'];
    $data['harga'] = $far_stok_depo['harga_akhir'];
    $data['jml_awal'] = $data['harga'] * $data['qty'];
    $data['jml_potongan'] = null;
    $data['jml_tagihan'] = $data['jml_awal'] - $data['jml_potongan'];
    $data['tgl_catat'] = to_date($data['tgl_catat'], '', 'full_date');
    $data['user_cd'] = $this->session->userdata('sess_user_cd');

    $qty_sebelum = $data['qty_sebelum'];
    $stokdepo_id_sebelum = $data['stokdepo_id_sebelum'];
    unset($data['qty_sebelum'], $data['stokdepo_id_sebelum']);

    if ($data['alkes_id'] == null) {
      $data['alkes_id'] = get_id('dat_alkes');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_alkes', $data);
      update_id('dat_alkes', $data['alkes_id']);

      // update far_stok_depo
      $sql = "UPDATE far_stok_depo 
              SET stok_terpakai=ifnull(stok_terpakai,0) + '" . $data['qty'] . "', 
                  stok_akhir=ifnull(stok_akhir,0) - '" . $data['qty'] . "' 
              WHERE stokdepo_id='" . $far_stok_depo['stokdepo_id'] . "'";
      $this->db->query($sql);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('alkes_id', $data['alkes_id'])->update('dat_alkes', $data);

      // update far_stok_depo
      if ($stokdepo_id_sebelum != $far_stok_depo['stokdepo_id']) {
        $sql1 = "UPDATE far_stok_depo 
              SET stok_terpakai=ifnull(stok_terpakai,0) - '" . $qty_sebelum . "', 
                  stok_akhir=ifnull(stok_akhir,0) + '" . $qty_sebelum . "' 
              WHERE stokdepo_id='" . $stokdepo_id_sebelum . "'";
        $this->db->query($sql1);

        $sql2 = "UPDATE far_stok_depo 
              SET stok_terpakai=ifnull(stok_terpakai,0) + '" . $data['qty'] . "', 
                  stok_akhir=ifnull(stok_akhir,0) - '" . $data['qty'] . "' 
              WHERE stokdepo_id='" . $far_stok_depo['stokdepo_id'] . "'";
        $this->db->query($sql2);
      } else {
        $sql = "UPDATE far_stok_depo 
                SET stok_terpakai=ifnull(stok_terpakai,0) - '" . $qty_sebelum . "' + '" . $data['qty'] . "', 
                    stok_akhir=ifnull(stok_akhir,0) + '" . $qty_sebelum . "' - '" . $data['qty'] . "' 
                WHERE stokdepo_id='" . $far_stok_depo['stokdepo_id'] . "'";
        $this->db->query($sql);
      }
    }
  }

  public function alkes_get($alkes_id = '', $reg_id = '')
  {
    $sql = "SELECT 
              a.*, DATE_FORMAT(a.tgl_catat, '%Y-%m-%d %H:%i:%s') AS tgl_catat
            FROM dat_alkes a 
            WHERE a.alkes_id=? AND a.reg_id=?";
    $query = $this->db->query($sql, array($alkes_id, $reg_id));
    return $query->row_array();
  }

  public function alkes_row($data)
  {
    $sql = "SELECT * FROM far_stok_depo WHERE stokdepo_id=?";
    $query = $this->db->query($sql, array($data['stokdepo_id']));
    return $query->row_array();
  }

  public function alkes_autocomplete($obat_nm = null, $map_lokasi_depo = null)
  {
    $sql = "SELECT 
            *
          FROM far_stok_depo a
          WHERE 
            (a.obat_nm LIKE '%$obat_nm%' OR a.obat_id LIKE '%$obat_nm%') AND
            a.stok_st = 1 AND a.stok_akhir > 0 AND a.lokasi_id = '$map_lokasi_depo' 
            AND a.jenisbarang_cd = '02'
          ORDER BY a.obat_id";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['obat_id'] . "#" . $row['obat_nm'] . "#" . $row['stokdepo_id'],
        'text' => $row['obat_id'] . ' - ' . $row['obat_nm']
      );
    }
    return $res;
  }

  public function alkes_delete($alkes_id = '', $reg_id = '')
  {
    trash('dat_alkes', array(
      'alkes_id' => $alkes_id,
      'reg_id' => $reg_id
    ));

    $get_alkes = $this->db->where('alkes_id', $alkes_id)->where('reg_id', $reg_id)->get('dat_alkes')->row_array();

    $this->db->where('alkes_id', $alkes_id)->where('reg_id', $reg_id)->delete('dat_alkes');

    // update far_stok_depo
    $sql = "UPDATE far_stok_depo 
          SET stok_terpakai=ifnull(stok_terpakai,0) - '" . $get_alkes['qty'] . "', 
              stok_akhir=ifnull(stok_akhir,0) + '" . $get_alkes['qty'] . "' 
          WHERE stokdepo_id='" . $get_alkes['stokdepo_id'] . "'";
    $this->db->query($sql);
  }

  public function detail_bhp($groupreg_id = '', $groupreg_in = '')
  {
    $sql = "SELECT 
              a.*
            FROM dat_bhp a
            WHERE a.is_deleted=0 AND ? LIKE CONCAT('%', a.reg_id,';%')
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, $groupreg_in);
    return $query->result_array();
  }

  public function bhp_save()
  {
    $data = $this->input->post();
    $barang_id = explode("#", $data['barang_id']);
    $data['barang_id'] = $barang_id[0];
    $data['barang_nm'] = $barang_id[1];
    $data['stokdepo_id'] = @$barang_id[2];

    $far_stok_depo = $this->db->where('stokdepo_id', @$barang_id[2])->get('far_stok_depo')->row_array();

    $data['stokdepo_id'] = $far_stok_depo['stokdepo_id'];
    $data['harga'] = $far_stok_depo['harga_akhir'];
    $data['jml_awal'] = $data['harga'] * $data['qty'];
    $data['jml_potongan'] = null;
    $data['jml_tagihan'] = $data['jml_awal'] - $data['jml_potongan'];
    $data['tgl_catat'] = to_date($data['tgl_catat'], '', 'full_date');
    $data['user_cd'] = $this->session->userdata('sess_user_cd');

    $qty_sebelum = $data['qty_sebelum'];
    $stokdepo_id_sebelum = $data['stokdepo_id_sebelum'];
    unset($data['qty_sebelum'], $data['stokdepo_id_sebelum']);

    if ($data['bhp_id'] == null) {
      $data['bhp_id'] = get_id('dat_bhp');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_bhp', $data);
      update_id('dat_bhp', $data['bhp_id']);

      // update far_stok_depo
      $sql = "UPDATE far_stok_depo 
              SET stok_terpakai=ifnull(stok_terpakai,0) + '" . $data['qty'] . "', 
                  stok_akhir=ifnull(stok_akhir,0) - '" . $data['qty'] . "' 
              WHERE stokdepo_id='" . $far_stok_depo['stokdepo_id'] . "'";
      $this->db->query($sql);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('bhp_id', $data['bhp_id'])->update('dat_bhp', $data);

      // update far_stok_depo
      if ($stokdepo_id_sebelum != $far_stok_depo['stokdepo_id']) {
        $sql1 = "UPDATE far_stok_depo 
              SET stok_terpakai=ifnull(stok_terpakai,0) - '" . $qty_sebelum . "', 
                  stok_akhir=ifnull(stok_akhir,0) + '" . $qty_sebelum . "' 
              WHERE stokdepo_id='" . $stokdepo_id_sebelum . "'";
        $this->db->query($sql1);

        $sql2 = "UPDATE far_stok_depo 
              SET stok_terpakai=ifnull(stok_terpakai,0) + '" . $data['qty'] . "', 
                  stok_akhir=ifnull(stok_akhir,0) - '" . $data['qty'] . "' 
              WHERE stokdepo_id='" . $far_stok_depo['stokdepo_id'] . "'";
        $this->db->query($sql2);
      } else {
        $sql = "UPDATE far_stok_depo 
                SET stok_terpakai=ifnull(stok_terpakai,0) - '" . $qty_sebelum . "' + '" . $data['qty'] . "', 
                    stok_akhir=ifnull(stok_akhir,0) + '" . $qty_sebelum . "' - '" . $data['qty'] . "' 
                WHERE stokdepo_id='" . $far_stok_depo['stokdepo_id'] . "'";
        $this->db->query($sql);
      }
    }
  }

  public function bhp_autocomplete($obat_nm = null, $map_lokasi_depo = null)
  {
    $sql = "SELECT 
            *
          FROM far_stok_depo a
          WHERE 
            (a.obat_nm LIKE '%$obat_nm%' OR a.obat_id LIKE '%$obat_nm%') AND
            a.stok_st = 1 AND a.stok_akhir > 0 AND a.lokasi_id = '$map_lokasi_depo' 
            AND a.jenisbarang_cd = '03'
          ORDER BY a.obat_id";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['obat_id'] . "#" . $row['obat_nm'] . "#" . $row['stokdepo_id'],
        'text' => $row['obat_id'] . ' - ' . $row['obat_nm']
      );
    }
    return $res;
  }

  public function bhp_get($bhp_id = '', $reg_id = '')
  {
    $sql = "SELECT 
              a.*, DATE_FORMAT(a.tgl_catat, '%Y-%m-%d %H:%i:%s') AS tgl_catat
            FROM dat_bhp a 
            WHERE a.bhp_id=? AND a.reg_id=?";
    $query = $this->db->query($sql, array($bhp_id, $reg_id));
    return $query->row_array();
  }

  public function bhp_delete($bhp_id = '', $reg_id = '')
  {
    trash('dat_bhp', array(
      'bhp_id' => $bhp_id,
      'reg_id' => $reg_id
    ));

    $get_bhp = $this->db->where('bhp_id', $bhp_id)->where('reg_id', $reg_id)->get('dat_bhp')->row_array();

    $this->db->where('bhp_id', $bhp_id)->where('reg_id', $reg_id)->delete('dat_bhp');

    // update far_stok_depo
    $sql = "UPDATE far_stok_depo 
          SET stok_terpakai=ifnull(stok_terpakai,0) - '" . $get_bhp['qty'] . "', 
              stok_akhir=ifnull(stok_akhir,0) + '" . $get_bhp['qty'] . "' 
          WHERE stokdepo_id='" . $get_bhp['stokdepo_id'] . "'";
    $this->db->query($sql);
  }

  public function bhp_row($data)
  {
    $sql = "SELECT * FROM far_stok_depo WHERE stokdepo_id=?";
    $query = $this->db->query($sql, array($data['stokdepo_id']));
    return $query->row_array();
  }

  public function save($groupreg_id = null, $groupreg_in = null)
  {
    $data = html_escape($this->input->post());
    // hitung_biaya_kamar_final
    $this->hitung_biaya_kamar_final($groupreg_id);
    // echo '<pre>' . var_export($data, true) . '</pre>';
    // exit;
    //
    // arr_groupreg_in
    $dat_groupreg_in = explode(';', $groupreg_in);
    $arr_groupreg_in = [];
    $set_groupreg_in = "";
    foreach ($dat_groupreg_in as $key => $val) {
      if ($val != '') {
        $arr_groupreg_in[] = $val;
        $set_groupreg_in .= "'" . $val . "',";
      }
    }
    $set_groupreg_in = rtrim($set_groupreg_in, ",");

    // update reg_pasien
    $reg_pasien['sep_no'] = $data['sep_no'];
    $reg_pasien['bayar_st'] = $data['bayar_st'];
    $reg_pasien['updated_at'] = date('Y-m-d H:i:s');
    $reg_pasien['updated_by'] = $this->session->userdata('sess_user_realname');
    $reg_pasien['pulang_st'] = 1;
    $reg_pasien['tgl_pulang'] = date('Y-m-d H:i:s');
    $this->db->where_in('reg_id', $arr_groupreg_in)->update('reg_pasien', $reg_pasien);
    // $this->db->where('reg_id', $groupreg_id)->update('reg_pasien', $reg_pasien);

    // insert dat_billing
    $dat_billing['tgl_transaksi'] = to_date($data['tgl_transaksi'], '', 'full_date');
    $dat_billing['groupreg_id'] = $groupreg_id;
    $dat_billing['pasien_id'] = $data['pasien_id'];
    $dat_billing['pasien_nm'] = $data['pasien_nm'];
    $dat_billing['lokasi_id'] = $data['lokasi_id'];
    $dat_billing['jenispasien_id'] = $data['jenispasien_id'];
    $dat_billing['tgl_masuk'] = $data['tgl_masuk'];
    $dat_billing['tgl_keluar'] = $data['tgl_keluar'];
    $dat_billing['grand_jml_tindakan'] = clear_numeric($data['grand_jml_tindakan']);
    $dat_billing['grand_jml_obat'] = clear_numeric($data['grand_jml_obat']);
    $dat_billing['grand_jml_alkes'] = clear_numeric($data['grand_jml_alkes']);
    $dat_billing['grand_jml_bhp'] = clear_numeric($data['grand_jml_bhp']);
    $dat_billing['grand_total_bruto'] = clear_numeric($data['grand_total_bruto']);
    $dat_billing['grand_total_potongan'] = clear_numeric($data['grand_total_potongan']);
    $dat_billing['grand_total_netto'] = clear_numeric($data['grand_total_netto']);
    $dat_billing['grand_pembulatan'] = clear_numeric($data['grand_pembulatan']);
    $dat_billing['grand_total_tagihan'] = clear_numeric($data['grand_total_tagihan']);
    $dat_billing['potongan_cd'] = $data['potongan_cd'];
    $dat_billing['bayar_st'] = $data['bayar_st'];
    $dat_billing['tgl_bayar'] = to_date($data['tgl_bayar'], '', 'full_date');
    $dat_billing['grand_titip_bayar'] = clear_numeric($data['grand_titip_bayar']);
    if ($data['bayar_st'] == 1) {
      $dat_billing['grand_total_bayar'] = clear_numeric($data['grand_total_bayar']);
    } elseif ($data['bayar_st'] == 2) {
      $dat_billing['grand_total_bayar'] = clear_numeric($data['jml_pelunasan']);
    }
    $dat_billing['keterangan_bayar'] = $data['keterangan_bayar'];
    $dat_billing['user_cd'] = $this->session->userdata('sess_user_cd');
    $dat_billing['no_invoice'] = $data['no_invoice'];

    if ($data['billing_id'] == 'otomatis') {
      $dat_billing['billing_id'] = get_id('dat_billing');
      $dat_billing['created_at'] = date('Y-m-d H:i:s');
      $dat_billing['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_billing', $dat_billing);
      update_id('dat_billing', $dat_billing['billing_id']);
      // set billing_id
      $data['billing_id'] = $dat_billing['billing_id'];
    } else {
      $dat_billing['updated_at'] = date('Y-m-d H:i:s');
      $dat_billing['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('billing_id', $data['billing_id'])->update('dat_billing', $dat_billing);
    }

    // insert dat_billing_rinc
    foreach ($data['tarif_id'] as $key => $val) {

      $cek_dat_billing_rinc = $this->db->where('billing_id', $data['billing_id'])->where('tarif_id', $data['tarif_id'][$key])->get('dat_billing_rinc')->row_array();

      $dat_billing_rinc['billing_id'] = $data['billing_id'];
      $dat_billing_rinc['tarif_id'] = $data['tarif_id'][$key];
      $dat_billing_rinc['jml_seharusnya'] = ($data['jml_seharusnya'][$key]);
      $dat_billing_rinc['jml_potongan'] = ($data['jml_potongan'][$key]);
      $dat_billing_rinc['jml_tagihan'] = ($data['jml_tagihan'][$key]);
      $dat_billing_rinc['data_tp'] = $data['data_tp'][$key];
      //
      if ($cek_dat_billing_rinc['billingrinc_id'] == '') {
        $dat_billing_rinc['billingrinc_id'] = get_id('dat_billing_rinc');
        $dat_billing_rinc['created_at'] = date('Y-m-d H:i:s');
        $dat_billing_rinc['created_by'] = $this->session->userdata('sess_user_realname');
        $this->db->insert('dat_billing_rinc', $dat_billing_rinc);
        update_id('dat_billing_rinc', $dat_billing_rinc['billingrinc_id']);
        // set billingrinc_id
        $billingrinc_id = $dat_billing_rinc['billingrinc_id'];
      } else {
        $dat_billing_rinc['updated_at'] = date('Y-m-d H:i:s');
        $dat_billing_rinc['updated_by'] = $this->session->userdata('sess_user_realname');
        $this->db->where('billing_id', $data['billing_id'])->where('tarif_id', $data['tarif_id'][$key])->update('dat_billing_rinc', $dat_billing_rinc);
        // set billingrinc_id
        $billingrinc_id = @$cek_dat_billing_rinc['billingrinc_id'];
      }

      // update dat_tindakan
      $sql_dat_tindakan = "UPDATE dat_tindakan 
              SET billingrinc_id='" . $billingrinc_id . "'  
              WHERE reg_id IN (" . $set_groupreg_in . ") AND left(tarif_id,9)='" . $data['tarif_id'][$key] . "'";
      $this->db->query($sql_dat_tindakan);

      // update reg_pasien_kamar
      $sql_reg_pasien_kamar = "UPDATE reg_pasien_kamar 
              SET billingrinc_id='" . $billingrinc_id . "'  
              WHERE reg_id IN (" . $set_groupreg_in . ") AND left(tarif_id,9)='" . $data['tarif_id'][$key] . "'";
      $this->db->query($sql_reg_pasien_kamar);
    }

    // update dat_resep
    $dat_resep['billing_id'] = $data['billing_id'];
    $dat_resep['updated_at'] = date('Y-m-d H:i:s');
    $dat_resep['updated_by'] = $this->session->userdata('sess_user_realname');
    $this->db->where_in('reg_id', $arr_groupreg_in)->update('dat_farmasi', $dat_resep);
    // $this->db->where('reg_id', $groupreg_id)->update('dat_resep', $dat_resep);

    // update dat_alkes
    $dat_alkes['billing_id'] = $data['billing_id'];
    $dat_alkes['updated_at'] = date('Y-m-d H:i:s');
    $dat_alkes['updated_by'] = $this->session->userdata('sess_user_realname');
    $this->db->where_in('reg_id', $arr_groupreg_in)->update('dat_alkes', $dat_alkes);
    // $this->db->where('reg_id', $groupreg_id)->update('dat_alkes', $dat_alkes);

    // update dat_bhp
    $dat_bhp['billing_id'] = $data['billing_id'];
    $dat_bhp['updated_at'] = date('Y-m-d H:i:s');
    $dat_bhp['updated_by'] = $this->session->userdata('sess_user_realname');
    $this->db->where_in('reg_id', $arr_groupreg_in)->update('dat_bhp', $dat_bhp);
    // $this->db->where('reg_id', $groupreg_id)->update('dat_bhp', $dat_bhp);
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('stok_id', $id)->update('far_stok', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('stok_id', $id)->delete('far_stok');
      $this->db->where('stok_id', $id)->delete('far_stok_rinc');
    } else {
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('stok_id', $id)->update('far_stok', $data);
      $this->db->where('stok_id', $id)->update('far_stok_rinc', $data);
    }
  }

  public function obat_autocomplete($obat_nm = null)
  {
    $sql = "SELECT 
              a.*, b.parameter_val AS satuan
            FROM mst_obat a
            LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
            WHERE (a.obat_nm LIKE '%$obat_nm%' OR a.obat_id LIKE '%$obat_nm%')
            ORDER BY a.obat_id";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['obat_id'] . "#" . $row['obat_nm'] . '#' . $row['satuan'],
        'text' => $row['obat_id'] . ' - ' . $row['obat_nm']
      );
    }
    return $res;
  }

  public function obat_row($id)
  {
    $sql = "SELECT * FROM mst_obat WHERE obat_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  //surat pesanan
  public function surat_pesanan_row($id)
  {
    $sql = "SELECT * FROM far_stok WHERE order_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  // Tindakan
  public function tarifkelas_autocomplete($tarif_nm = null)
  {
    $sql = "SELECT 
              a.*,b.tarif_nm,c.kelas_nm
            FROM mst_tarif_kelas a
            JOIN mst_tarif b ON a.tarif_id = b.tarif_id
            JOIN mst_kelas c ON a.kelas_id = c.kelas_id
            WHERE b.tarif_nm LIKE '%$tarif_nm%' ";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['tarifkelas_id'],
        'text' => $row['tarifkelas_id'] . ' - ' . $row['tarif_nm'] . ' - ' . $row['kelas_nm']
      );
    }
    return $res;
  }

  public function tarifkelas_row($id)
  {
    $sql = "SELECT a.*, b.tarif_nm, c.kelas_nm FROM mst_tarif_kelas a
      LEFT JOIN mst_tarif b ON a.tarif_id = b.tarif_id
      LEFT JOIN mst_kelas c ON a.kelas_id = c.kelas_id
      WHERE tarifkelas_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  public function petugas_autocomplete($petugas_nm = null)
  {
    $sql = "SELECT 
              a.*
            FROM mst_pegawai a
            WHERE a.pegawai_nm LIKE '%$petugas_nm%' ";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['pegawai_id'],
        'text' => $row['pegawai_nm']
      );
    }
    return $res;
  }

  public function tindakan_get($tindakan_id = '', $groupreg_id = '', $pasien_id = '')
  {
    $sql = "SELECT 
              a.*, b.kelas_nm, c.pegawai_nm, DATE_FORMAT(a.tgl_catat, '%Y-%m-%d %H:%i:%s') AS tgl_catat
            FROM dat_tindakan a
            LEFT JOIN mst_kelas b ON a.kelas_id = b.kelas_id
            LEFT JOIN mst_pegawai c ON a.petugas_id = c.pegawai_id 
            WHERE a.tindakan_id=? AND a.reg_id=? AND a.pasien_id=?";
    $query = $this->db->query($sql, array($tindakan_id, $groupreg_id, $pasien_id));
    $row = $query->row_array();
    $jml_petugas = 0;
    if ($row['petugas_id'] != '') {
      $jml_petugas += 1;
    }
    if ($row['petugas_id_2'] != '') {
      $jml_petugas += 1;
    }
    if ($row['petugas_id_3'] != '') {
      $jml_petugas += 1;
    }
    if ($row['petugas_id_4'] != '') {
      $jml_petugas += 1;
    }
    if ($row['petugas_id_5'] != '') {
      $jml_petugas += 1;
    }
    $row['jml_petugas'] = $jml_petugas;
    return $row;
  }

  public function petugas_row($id)
  {
    $sql = "SELECT * FROM mst_pegawai WHERE pegawai_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  public function get_tindakan($tindakan_id = null)
  {
    $sql = "SELECT 
              a.*, 
              b.pegawai_nm AS pegawai_nm_1, 
              c.pegawai_nm AS pegawai_nm_2, 
              d.pegawai_nm AS pegawai_nm_3, 
              e.pegawai_nm AS pegawai_nm_4, 
              f.pegawai_nm AS pegawai_nm_5
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            LEFT JOIN mst_pegawai c ON a.petugas_id_2 = c.pegawai_id
            LEFT JOIN mst_pegawai d ON a.petugas_id_3 = d.pegawai_id
            LEFT JOIN mst_pegawai e ON a.petugas_id_4 = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_5 = f.pegawai_id
            WHERE a.tindakan_id=?";
    $query = $this->db->query($sql, $tindakan_id);
    return $query->row_array();
  }

  public function tindakan_save($groupreg_id = '')
  {
    $data = $this->input->post();
    unset($data['petugas_no']);
    $data['reg_id'] = $groupreg_id;
    $data['tgl_catat'] = to_date($data['tgl_catat'], '', 'full_date');
    $data['user_cd'] = $this->session->userdata('sess_user_cd');
    $tk = $this->db
      ->select('a.*, b.tarif_nm')
      ->join('mst_tarif b', 'a.tarif_id = b.tarif_id', 'left')
      ->where('a.tarifkelas_id', $data['tarifkelas_id'])
      ->get('mst_tarif_kelas a')->row_array();
    unset($data['tarifkelas_id']);
    $data['tarif_id'] = $tk['tarif_id'];
    $data['kelas_id'] = $tk['kelas_id'];
    $data['tarif_nm'] = $tk['tarif_nm'];
    $data['js'] = $tk['js'];
    $data['jp'] = $tk['jp'];
    $data['jb'] = $tk['jb'];
    $data['nom_tarif'] = $tk['nominal'];
    $data['jml_awal'] = $tk['nominal'] * $data['qty'];
    $data['jml_tagihan'] = $tk['nominal'] * $data['qty'];

    // petugas
    // $d = $data;
    // unset($d['petugas_id'], $d['petugas_id_2'], $d['petugas_id_3'], $d['petugas_id_4'], $d['petugas_id_5']);
    // $petugas = array();
    // for ($i = 1; $i <= 5; $i++) {
    //   if ($i == '1') {
    //     $petugas_id = $data['petugas_id'];
    //   } else {
    //     $petugas_id = $data['petugas_id_' . $i];
    //   }

    //   if ($petugas_id != '0') {
    //     array_push($petugas, $petugas_id);
    //   }
    // }

    // for ($j = 0; $j < count($petugas); $j++) {
    //   $d['petugas_id_' . ($j + 1)] = $petugas[$j];
    // }

    // unset($data['petugas_id'], $data['petugas_id_2'], $data['petugas_id_3'], $data['petugas_id_4'], $data['petugas_id_5']);

    // $data['petugas_id'] = @$d['petugas_id_1'];
    // $data['petugas_id_2'] = @$d['petugas_id_2'];
    // $data['petugas_id_3'] = @$d['petugas_id_3'];
    // $data['petugas_id_4'] = @$d['petugas_id_4'];
    // $data['petugas_id_5'] = @$d['petugas_id_5'];

    $data['petugas_id'] = '000054';

    if ($data['tindakan_id'] == null) {
      $data['tindakan_id'] = get_id('dat_tindakan');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_tindakan', $data);
      update_id('dat_tindakan', $data['tindakan_id']);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('tindakan_id', $data['tindakan_id'])->update('dat_tindakan', $data);
    }
  }

  public function get_last()
  {
    return $this->db->order_by('billing_id', 'desc')->get('dat_billing')->row_array();
  }

  public function get_billing($id)
  {
    return $this->db->where('groupreg_id', $id)->get('dat_billing')->row_array();
  }

  public function tindakan_data($pasien_id = '', $reg_id = '')
  {
    $sql = "SELECT 
              a.*, 
              g.kelas_nm, 
              h.parent_id 
            FROM dat_tindakan a
            LEFT JOIN mst_kelas g ON a.kelas_id = g.kelas_id 
            LEFT JOIN mst_tarif h ON a.tarif_id = h.tarif_id
            WHERE a.is_deleted = 0 AND a.pasien_id=? AND a.reg_id=? AND a.tarif_id !='09.01.001' AND a.tarif_id !='09.01.002' AND a.is_bayar = 0
            ORDER BY a.tindakan_id";
    $query = $this->db->query($sql, array($pasien_id, $reg_id));
    $result = $query->result_array();
    foreach ($result as $key => $val) {
      if ($val['parent_id'] == '09.02') {
        $get_mst_tarif = $this->db->query("SELECT parent_id, tarif_nm FROM mst_tarif WHERE tarif_id = '" . $val['parent_id'] . "'")->row_array();
        $result[$key]['tarif_nm'] = $get_mst_tarif['tarif_nm'];
      }
    }
    return $result;
  }

  public function list_pendaftaran($pasien_id = '', $reg_id = '')
  {
    $sql = "SELECT 
              a.tarif_nm, 
              a.kelas_id, 
              a.jml_awal,
              a.jml_potongan, 
              g.kelas_nm 
            FROM dat_tindakan a
            LEFT JOIN mst_kelas g ON a.kelas_id = g.kelas_id 
            WHERE a.is_deleted = 0 AND a.pasien_id='$pasien_id' AND a.reg_id='$reg_id' AND ( a.tarif_id = '09.01.001' OR a.tarif_id = '09.01.002' )
            ORDER BY a.tindakan_id";
    $query = $this->db->query($sql, array($pasien_id, $reg_id));
    $result = $query->result_array();
    return $result;
  }

  public function pemberian_obat_data($reg_id = '', $pasien_id = '')
  {
    $sql = "SELECT 
              a.* 
            FROM dat_resep a
            WHERE a.is_deleted=0 AND a.pasien_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($pasien_id));
    $result = $query->result_array();
    // 
    foreach ($result as $key => $val) {
      $result[$key]['list_dat_resep'] = $this->list_dat_resep_report($val['resep_id'], $reg_id, $pasien_id);
    }
    return $result;
  }

  public function list_dat_resep_report($resep_id = '', $reg_id = '', $pasien_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_farmasi a
            LEFT JOIN mst_obat b ON a.obat_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.resep_id=? AND a.pasien_id=?
            ORDER BY a.reg_id ASC";
    $query = $this->db->query($sql, array($resep_id, $pasien_id));
    return $query->result_array();
  }

  public function hitung_biaya_kamar($id)
  {
    //get reg_pasien
    $reg = $this->db->where('reg_id', $id)->get('reg_pasien')->row_array();
    $arr_reg_id = explode(';', $reg['groupreg_in']);
    if (count($arr_reg_id) > 0) {
      foreach ($arr_reg_id as $key => $val) {
        $this->db->query(
          "UPDATE reg_pasien_kamar SET 
            jml_hari = (DATEDIFF(DATE(NOW()), DATE(tgl_masuk)) + 1),
            jml_awal = nom_tarif * (DATEDIFF(DATE(NOW()), DATE(tgl_masuk)) + 1),
            jml_tagihan = nom_tarif * (DATEDIFF(DATE(NOW()), DATE(tgl_masuk)) + 1),
            updated_at = NOW()
          WHERE tgl_keluar IS NULL AND reg_id = '" . $val . "' AND tarif_id != '05.01.002'"
        );
      }
    }
  }

  public function hitung_biaya_kamar_final($id)
  {
    //get reg_pasien
    $reg = $this->db->where('groupreg_id', $id)->get('reg_pasien')->row_array();
    $arr_reg_id = explode(';', $reg['groupreg_in']);
    if (count($arr_reg_id) > 0) {
      foreach ($arr_reg_id as $key => $val) {
        $this->db->query(
          "UPDATE reg_pasien_kamar SET 
            tgl_keluar = NOW(),
            jml_hari = (DATEDIFF(DATE(NOW()), DATE(tgl_masuk)) + 1),
            jml_awal = nom_tarif * (DATEDIFF(DATE(NOW()), DATE(tgl_masuk)) + 1),
            jml_tagihan = nom_tarif * (DATEDIFF(DATE(NOW()), DATE(tgl_masuk)) + 1),
            updated_at = NOW()
          WHERE tgl_keluar IS NULL AND reg_id = '" . $val . "' AND tarif_id != '05.01.002'"
        );
      }
    }
  }

  public function item_pembayaran($billing_id, $tarif_id)
  {
    return $this->db->query(
      "SELECT 
        a.*, b.tarif_nm 
      FROM dat_billing_rinc a 
      LEFT JOIN mst_tarif b ON a.tarif_id = b.tarif_id
      WHERE
        a.billing_id = '$billing_id' AND a.tarif_id = '$tarif_id'"
    )->row_array();
  }

  public function split_save($data)
  {
    //get last billing 
    $last = $this->db->where('billing_id', $data['billing_id'])->order_by('split_id', 'DESC')->get('dat_billing')->row_array();
    $reg = $this->db->where('groupreg_id', $last['groupreg_id'])->get('reg_pasien')->row_array();
    //create new billing
    $dat_billing = array(
      'billing_id' => @$last['billing_id'],
      'split_id' => @$last['split_id'] + 1,
      'tgl_transaksi' => @$last['tgl_transaksi'],
      'no_invoice' => @$last['no_invoice'],
      'groupreg_id' => @$last['groupreg_id'],
      'pasien_id' => @$last['pasien_id'],
      'pasien_nm' => @$last['pasien_nm'],
      'lokasi_id' => @$last['lokasi_id'],
      'jenispasien_id' => @$last['jenispasien_id'],
      'tgl_masuk' => @$last['tgl_masuk'],
      'tgl_keluar' => @$last['tgl_keluar'],
      'grand_jml_tindakan' => $data['grand_total_bruto'],
      'grand_jml_obat' => 0,
      'grand_jml_alkes' => 0,
      'grand_jml_bhp' => 0,
      'grand_total_bruto' => $data['grand_total_bruto'],
      'grand_total_potongan' => $data['grand_total_potongan'],
      'grand_total_netto' => $data['grand_total_netto'],
      'grand_pembulatan' => $data['grand_pembulatan'],
      'grand_total_tagihan' => $data['grand_total_tagihan'],
      'potongan_cd' => $data['potongan_cd'],
      'bayar_st' => $data['bayar_st'],
      'tgl_bayar' => to_date($data['tgl_bayar'], '', 'full_date'),
      'grand_titip_bayar' => 0,
      'grand_total_bayar' => $data['grand_total_bayar'],
      'user_cd' => $this->session->userdata('sess_user_cd'),
      'created_by' => $this->session->userdata('sess_user_realname'),
      'created_at' => date('Y-m-d H:i:s'),
    );
    if ($this->db->insert('dat_billing', $dat_billing)) {
      //insert dat_billing_rinc
      $dat_rinc = array(
        'billingrinc_id' => get_id('dat_billing_rinc'),
        'billing_id' => @$last['billing_id'],
        'split_id' => @$dat_billing['split_id'],
        'tarif_id' => $data['tarif_id'],
        'jml_seharusnya' => $data['jml_seharusnya'],
        'jml_potongan' => $data['jml_potongan'],
        'jml_tagihan' => $data['jml_tagihan'],
        'created_by' => $this->session->userdata('sess_user_realname'),
        'created_at' => date('Y-m-d H:i:s'),
      );
      if ($this->db->insert('dat_billing_rinc', $dat_rinc)) {
        update_id('dat_billing_rinc', $dat_rinc['billingrinc_id']);
        //update dat_tindakan
        $groupreg_in = $reg['groupreg_in'];
        $this->db->query(
          "UPDATE 
            dat_tindakan a
          SET 
            a.is_bayar = 1 
          WHERE
            a.tarif_id LIKE '" . $data['tarif_id'] . "%'AND
            '$groupreg_in' LIKE CONCAT( '%', a.reg_id, ';%' ) "
        );
      } else {
        var_dump($this->db->error());
        die;
      }
    } else {
      var_dump($this->db->error());
      die;
    }
  }

  public function list_billing_parsial($id, $pasien_id)
  {
    $sql = "SELECT
              a.* 
            FROM
              dat_billing a
            WHERE
              a.groupreg_id = ? 
              AND a.pasien_id = ?
              AND a.split_id !=0";
    $query = $this->db->query($sql, array($id, $pasien_id));
    $result = $query->result_array();
    // 
    foreach ($result as $key => $val) {
      $result[$key]['billing_parsial_rinc'] = $this->billing_parsial_rinc($val['billing_id'], $val['split_id']);
    }
    return $result;
  }

  public function billing_parsial_rinc($billing_id, $split_id)
  {
    $sql = "SELECT
              a.*, b.tarif_nm 
            FROM
              dat_billing_rinc a
            LEFT JOIN mst_tarif b ON a.tarif_id = b.tarif_id
            WHERE
              a.billing_id = ? 
              AND a.split_id = ?";
    $query = $this->db->query($sql, array($billing_id, $split_id));
    return $query->row_array();
  }

  public function get_kwitansi_parsial($billing_id, $split_id)
  {
    $sql = "SELECT
              b.tarif_nm, 
              c.*, 
              d.tgl_registrasi, d.umur_thn, d.umur_bln, d.umur_hr, d.reg_id, d.sebutan_cd, d.alamat, d.kelurahan, d.kecamatan, d.kabupaten, d.provinsi    
            FROM
              dat_billing_rinc a
              LEFT JOIN mst_tarif b ON a.tarif_id = b.tarif_id 
              LEFT JOIN dat_billing c ON a.billing_id = c.billing_id AND a.split_id = c.split_id
              LEFT JOIN reg_pasien d ON c.groupreg_id = d.reg_id
            WHERE
              a.billing_id = ? 
              AND a.split_id = ?";
    $query = $this->db->query($sql, array($billing_id, $split_id));
    return $query->row_array();
  }

  public function delete_tindakan($tarif_id, $groupreg_id)
  {
    $this->db->where('tarif_id', $tarif_id)->where('reg_id', $groupreg_id)->delete('dat_tindakan');
  }
}
