<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_piutang extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE 1 = 1 ";
    if (@$cookie['search']['tgl_registrasi_from'] != '' && @$cookie['search']['tgl_registrasi_to']) {
      $where .= "AND (DATE(x.tgl_registrasi) BETWEEN '" . to_date(@$cookie['search']['tgl_registrasi_from']) . "' AND '" . to_date(@$cookie['search']['tgl_registrasi_to']) . "')";
    } else {
      $where .= "AND DATE(x.tgl_registrasi) = '" . date('Y-m-d') . "' ";
    }
    if (@$cookie['search']['no_rm_nm'] != '') {
      $where .= "AND (x.pasien_id LIKE '%" . $this->db->escape_like_str($cookie['search']['no_rm_nm']) . "%' OR x.pasien_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['no_rm_nm']) . "%' ) ";
    }
    if (@$cookie['search']['jenisreg_st'] != '') {
      $where .= "AND x.jenisreg_st = '" . $this->db->escape_like_str($cookie['search']['jenisreg_st']) . "' ";
    }
    if (@$cookie['search']['lokasi_id'] != '') {
      $where .= "AND x.lokasi_id = '" . $this->db->escape_like_str($cookie['search']['lokasi_id']) . "' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
        x.*,
        (x.jml_biaya_tindakan+x.jml_biaya_obat+x.jml_biaya_alkes+x.jml_biaya_bhp+x.jml_biaya_kamar) as jml_total_biaya
        FROM 
        (
          SELECT a.reg_id,a.pasien_id,a.nik,a.groupreg_id,a.groupreg_in,a.pasien_nm,a.sebutan_cd,a.alamat,a.provinsi,a.kabupaten,a.kecamatan,a.kelurahan,a.tgl_registrasi,b.lokasi_nm,b.jenisreg_st,c.jenispasien_nm,
          (SELECT IFNULL(SUM(b.jml_tagihan),0) FROM dat_tindakan b WHERE a.groupreg_in LIKE CONCAT('%',b.reg_id,'%;')) as jml_biaya_tindakan,
          (SELECT IFNULL(SUM(b.grand_jml_tagihan),0) FROM dat_resep b WHERE a.groupreg_in LIKE CONCAT('%',b.reg_id,'%;')) as jml_biaya_obat,
          (SELECT IFNULL(SUM(b.jml_tagihan),0) FROM dat_alkes b WHERE a.groupreg_in LIKE CONCAT('%',b.reg_id,'%;')) as jml_biaya_alkes,
          (SELECT IFNULL(SUM(b.jml_tagihan),0) FROM dat_bhp b WHERE a.groupreg_in LIKE CONCAT('%',b.reg_id,'%;')) as jml_biaya_bhp,
          (SELECT IFNULL(SUM(IF(b.tgl_keluar IS NULL, (DATEDIFF(DATE(NOW()), DATE(b.tgl_masuk)) * b.nom_tarif), b.jml_tagihan)),0) FROM reg_pasien_kamar b WHERE a.groupreg_in LIKE CONCAT('%',b.reg_id,'%;')) as jml_biaya_kamar
          FROM reg_pasien a 
          INNER JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id
          INNER JOIN mst_jenis_pasien c ON a.jenispasien_id=c.jenispasien_id
          WHERE a.is_billing='1' AND a.bayar_st='0' ORDER BY a.tgl_registrasi DESC
        ) as x  
      $where        
      ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM reg_pasien a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT 
        x.*,
        (x.jml_biaya_tindakan+x.jml_biaya_obat+x.jml_biaya_alkes+x.jml_biaya_bhp+x.jml_biaya_kamar) as jml_total_biaya
        FROM 
        (
          SELECT a.reg_id,a.pasien_id,a.nik,a.groupreg_id,a.groupreg_in,a.pasien_nm,a.alamat,a.provinsi,a.kabupaten,a.kecamatan,a.kelurahan,a.tgl_registrasi,b.lokasi_nm,b.jenisreg_st,c.jenispasien_nm,
          (SELECT IFNULL(SUM(b.jml_tagihan),0) FROM dat_tindakan b WHERE a.groupreg_in LIKE CONCAT('%',b.reg_id,'%;')) as jml_biaya_tindakan,
          (SELECT IFNULL(SUM(b.grand_jml_tagihan),0) FROM dat_resep b WHERE a.groupreg_in LIKE CONCAT('%',b.reg_id,'%;')) as jml_biaya_obat,
          (SELECT IFNULL(SUM(b.jml_tagihan),0) FROM dat_alkes b WHERE a.groupreg_in LIKE CONCAT('%',b.reg_id,'%;')) as jml_biaya_alkes,
          (SELECT IFNULL(SUM(b.jml_tagihan),0) FROM dat_bhp b WHERE a.groupreg_in LIKE CONCAT('%',b.reg_id,'%;')) as jml_biaya_bhp,
          (SELECT IFNULL(SUM(IF(b.tgl_keluar IS NULL, (DATEDIFF(DATE(NOW()), DATE(b.tgl_masuk)) * b.nom_tarif), b.jml_tagihan)),0) FROM reg_pasien_kamar b WHERE a.groupreg_in LIKE CONCAT('%',b.reg_id,'%;')) as jml_biaya_kamar
          FROM reg_pasien a 
          INNER JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id
          INNER JOIN mst_jenis_pasien c ON a.jenispasien_id=c.jenispasien_id
          WHERE a.is_billing='1' AND a.bayar_st='0' ORDER BY a.tgl_registrasi DESC
        ) as x  
      $where";
    $query = $this->db->query($sql);
    return $query->num_rows();
  }

  function piutang_data($id)
  {
    $sql = "SELECT 
        x.*,
        (x.jml_biaya_tindakan+x.jml_biaya_obat+x.jml_biaya_alkes+x.jml_biaya_bhp+x.jml_biaya_kamar) as jml_total_biaya,
        z.pegawai_nm AS dokter_nm
        FROM 
        (
          SELECT 
            a.reg_id,a.pasien_id,a.nik,a.groupreg_id,a.groupreg_in,
            a.pasien_nm,a.alamat,a.provinsi,a.kabupaten,a.kecamatan,a.kelurahan,
            a.dokter_id,a.tgl_registrasi,b.lokasi_nm,b.jenisreg_st,c.jenispasien_nm,
          (SELECT IFNULL(SUM(b.jml_tagihan),0) FROM dat_tindakan b WHERE a.groupreg_in LIKE CONCAT('%',b.reg_id,'%;') AND reg_id IS NOT NULL AND reg_id !='') as jml_biaya_tindakan,
          (SELECT IFNULL(SUM(b.grand_jml_tagihan),0) FROM dat_resep b WHERE a.groupreg_in LIKE CONCAT('%',b.reg_id,'%;')) as jml_biaya_obat,
          (SELECT IFNULL(SUM(b.jml_tagihan),0) FROM dat_alkes b WHERE a.groupreg_in LIKE CONCAT('%',b.reg_id,'%;')) as jml_biaya_alkes,
          (SELECT IFNULL(SUM(b.jml_tagihan),0) FROM dat_bhp b WHERE a.groupreg_in LIKE CONCAT('%',b.reg_id,'%;')) as jml_biaya_bhp,
          (SELECT IFNULL(SUM(IF(b.tgl_keluar IS NULL, (DATEDIFF(DATE(NOW()), DATE(b.tgl_masuk)) * b.nom_tarif), b.jml_tagihan)),0) FROM reg_pasien_kamar b WHERE a.groupreg_in LIKE CONCAT('%',b.reg_id,'%;')) as jml_biaya_kamar
          FROM reg_pasien a 
          INNER JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id
          INNER JOIN mst_jenis_pasien c ON a.jenispasien_id=c.jenispasien_id
          WHERE a.is_billing='1' ORDER BY a.tgl_registrasi DESC
        ) as x 
        LEFT JOIN mst_pegawai z ON x.dokter_id = z.pegawai_id
        WHERE x.reg_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function piutang_tindakan($id)
  {
    $sql = "SELECT * FROM dat_tindakan WHERE '$id' LIKE CONCAT('%', reg_id,';%') AND reg_id IS NOT NULL AND reg_id !='' ORDER BY tgl_catat ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function piutang_obat($id)
  {
    $sql = "SELECT * FROM dat_farmasi WHERE '$id' LIKE CONCAT('%',reg_id,'%;') ORDER BY tgl_catat ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function piutang_alkes($id)
  {
    $sql = "SELECT * FROM dat_alkes WHERE '$id' LIKE CONCAT('%',reg_id,'%;') ORDER BY tgl_catat ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function piutang_bhp($id)
  {
    $sql = "SELECT * FROM dat_bhp WHERE '$id' LIKE CONCAT('%',reg_id,'%;') ORDER BY tgl_catat ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function piutang_kamar($id)
  {
    $sql = "SELECT a.*,b.kamar_nm 
      FROM reg_pasien_kamar a 
      INNER JOIN mst_kamar b ON a.kamar_id=b.kamar_id WHERE '$id' LIKE CONCAT('%',a.reg_id,'%;') ORDER BY a.tgl_masuk ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
}
