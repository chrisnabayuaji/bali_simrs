<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_surat_ket_lahir extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE 1=1 ";
    // if (@$cookie['search']['tgl_lahir_from'] != '' && @$cookie['search']['tgl_lahir_to']) {
    //   $where .= "AND (DATE(a.tgl_lahir) BETWEEN '" . to_date(@$cookie['search']['tgl_lahir_from']) . "' AND '" . to_date(@$cookie['search']['tgl_lahir_to']) . "')";
    // }
    if (@$cookie['search']['no_rm_nm'] != '') {
      $where .= "AND (a.pasien_id LIKE '%" . $this->db->escape_like_str($cookie['search']['no_rm_nm']) . "%' OR b.pasien_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['no_rm_nm']) . "%' ) ";
    }
    if (@$cookie['search']['jenisreg_st'] != '') {
      $where .= "AND e.jenisreg_st = '" . $this->db->escape_like_str($cookie['search']['jenisreg_st']) . "' ";
    }
    if (@$cookie['search']['lokasi_id'] != '') {
      $where .= "AND e.lokasi_id = '" . $this->db->escape_like_str($cookie['search']['lokasi_id']) . "' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
              a.reg_id, a.pasien_id, a.anak_ke, a.tgl_lahir, a.jam_lahir, a.sex_cd, a.pasien_id,
              b.pasien_nm, b.sebutan_cd, b.alamat, b.kelurahan, b.kecamatan, b.kabupaten, b.provinsi, b.tmp_lahir, b.lokasi_id, b.kelas_id, b.kamar_id, b.jenispasien_id, b.no_kartu, b.periksa_st, b.pulang_st, b.sep_no,   
              c.lokasi_nm, 
              d.jenispasien_nm, 
              e.kelas_nm  
            FROM reg_pasien_bayi a
            LEFT JOIN reg_pasien b ON a.reg_id = b.reg_id
            LEFT JOIN mst_lokasi c ON b.lokasi_id = c.lokasi_id
            LEFT JOIN mst_jenis_pasien d ON b.jenispasien_id = d.jenispasien_id
            LEFT JOIN mst_kelas e ON b.kelas_id = e.kelas_id  
              $where        
              ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM reg_pasien a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT a.*
            FROM reg_pasien_bayi a
            LEFT JOIN reg_pasien b ON a.reg_id = b.reg_id
            $where";
    $query = $this->db->query($sql);
    return $query->num_rows();
  }

  function get_data($id)
  {
    $sql = "SELECT 
              a.sex_cd, a.persalinan, a.tmp_lahir, a.tgl_lahir, a.anak_ke, a.alamat, a.pasien_id, a.reg_id,
              c.ibureg_id, c.ibu_nm, c.ibu_umur, c.ayah_nm, c.jam_lahir, c.bb, c.pb, c.bayi_nm, c.kabupaten, 
              d.parameter_val AS spesialisasi_nm, 
              e.pegawai_nm AS penolong_persalinan, 
              e.spesialisasi_cd, e.pegawai_id, c.dr_persalinan 
            FROM reg_pasien a 
            LEFT JOIN reg_pasien_bayi c ON a.reg_id=c.reg_id
            LEFT JOIN mst_pegawai e ON c.dr_persalinan=e.pegawai_id
            LEFT JOIN mst_parameter d ON e.spesialisasi_cd=d.parameter_cd AND d.parameter_field='spesialisasi_cd'
            WHERE a.reg_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  function get_data_bayi($id)
  {
    $sql = "SELECT 
              a.*, c.ibureg_id, c.ibu_nm, c.ibu_umur, c.ayah_nm, c.jam_lahir, c.bb, c.pb, c.dr_persalinan 
            FROM reg_pasien a 
            LEFT JOIN reg_pasien_bayi c ON a.reg_id=c.reg_id
            WHERE a.reg_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function diagnosis($id)
  {
    return $this->db->where('reg_id', $id)->get('dat_diagnosis')->row_array();
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());

    // update reg_pasien
    $reg_pasien = array(
      // 'sebutan_cd' => $data['sebutan_cd'],
      // 'pasien_nm' => $data['pasien_nm'],
      'anak_ke' => $data['anak_ke'],
      'sex_cd' => $data['sex_cd'],
      'tmp_lahir' => $data['tmp_lahir'],
      'tgl_lahir' => to_date($data['tgl_lahir']),
      'persalinan' => $data['persalinan'],
      'alamat' => $data['alamat'],
      'updated_at' => date('Y-m-d H:i:s'),
      'updated_by' => $this->session->userdata('sess_user_realname')
    );
    $this->db->where('reg_id', $id)->update('reg_pasien', $reg_pasien);

    // update reg_pasien_bayi
    // var_dump($data);
    // die;
    $rb = array(
      'anak_ke' => $data['anak_ke'],
      'bayi_nm' => $data['bayi_nm'],
      'ibu_umur' => $data['ibu_umur'],
      'ibu_nm' => $data['ibu_nm'],
      'ayah_nm' => $data['ayah_nm'],
      'tgl_lahir' => to_date($data['tgl_lahir']),
      'jam_lahir' => $data['jam_lahir'],
      'bb' => $data['bb'],
      'pb' => $data['pb'],
      'sex_cd' => $data['sex_cd'],
      'dr_persalinan' => $data['dr_persalinan'],
      'kabupaten' => $data['kabupaten']
    );
    $this->db->where('reg_id', $id)->update('reg_pasien_bayi', $rb);

    return true;
  }
}
