<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_sudah_bayar extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 AND a.bayar_st=1 AND a.is_billing = 1 ";
    if (@$cookie['search']['tgl_registrasi_from'] != '' && @$cookie['search']['tgl_registrasi_to']) {
      $where .= "AND (DATE(a.tgl_registrasi) BETWEEN '" . to_date(@$cookie['search']['tgl_registrasi_from']) . "' AND '" . to_date(@$cookie['search']['tgl_registrasi_to']) . "')";
    }
    if (@$cookie['search']['no_rm_nm'] != '') {
      $where .= "AND (a.pasien_id LIKE '%" . $this->db->escape_like_str($cookie['search']['no_rm_nm']) . "%' OR a.pasien_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['no_rm_nm']) . "%' ) ";
    }
    if (@$cookie['search']['lokasi_kasir'] != '') {
      $where .= "AND b.map_lokasi_kasir = '" . $this->db->escape_like_str($cookie['search']['lokasi_kasir']) . "' ";
    }
    if (@$cookie['search']['lokasi_pelayanan'] != '') {
      $where .= "AND a.lokasi_id = '" . $this->db->escape_like_str($cookie['search']['lokasi_pelayanan']) . "' ";
    }
    if (@$cookie['search']['jenispasien_id'] != '') {
      $where .= "AND a.jenispasien_id = '" . $this->db->escape_like_str($cookie['search']['jenispasien_id']) . "' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT a.*, b.lokasi_nm, c.jenispasien_nm, b.jenisreg_st  
            FROM reg_pasien a
            LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
            INNER JOIN dat_billing d ON a.groupreg_id = d.groupreg_id
            $where
            GROUP BY a.groupreg_id
            ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 AND a.bayar_st=1 ";

    $sql = "SELECT * FROM reg_pasien a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total 
            FROM reg_pasien a 
            LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
            INNER JOIN dat_billing d ON a.groupreg_id = d.groupreg_id
            $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id, $groupreg_in)
  {
    $sql = "SELECT
              a.*,
              b.pegawai_nm,
              c.jenispasien_nm,
              c.penjamin_bayar,
              d.lokasi_nm,
              d.map_lokasi_depo,
              d.jenisreg_st,
              e.kelas_nm,
              f.billing_id,
              f.no_invoice,
              f.grand_jml_tindakan,
              f.grand_jml_obat,
              f.grand_jml_alkes,
              f.grand_jml_bhp,
              f.grand_total_bruto,
              f.grand_total_potongan,
              f.grand_total_netto,
              f.grand_pembulatan,
              f.grand_total_tagihan,
              f.tgl_bayar,
              f.grand_titip_bayar,
              f.grand_total_bayar,
              f.keterangan_bayar,
              f.tgl_transaksi,
              f.potongan_cd,
              SUM(g.grand_jml_tagihan) AS grand_jml_obat,
              SUM(h.jml_tagihan) AS grand_jml_bhp,
              SUM(i.jml_tagihan) AS grand_jml_alkes,
              (SUM(g.grand_jml_tagihan) + SUM(h.jml_tagihan) + SUM(i.jml_tagihan)) AS total_bruto,
              k.pegawai_nm AS dokter_nm,
              l.parameter_val AS spesialisasi_nm
            FROM
              reg_pasien a
              LEFT JOIN mst_pegawai b ON a.dokter_id = b.pegawai_id
              LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
              LEFT JOIN mst_lokasi d ON a.lokasi_id = d.lokasi_id
              LEFT JOIN mst_kelas e ON a.kelas_id = e.kelas_id
              LEFT JOIN dat_billing f ON a.groupreg_id = f.groupreg_id 
              LEFT JOIN 
              (
                SELECT 
                '$groupreg_in' as groupreg_in,
                SUM(grand_jml_tagihan) as grand_jml_tagihan 
                FROM dat_resep 
                WHERE '$groupreg_in' LIKE CONCAT('%', reg_id,';%')
              ) g ON CONVERT(a.groupreg_in USING utf8) = CONVERT(g.groupreg_in USING utf8) 
              LEFT JOIN 
              (
                SELECT 
                '$groupreg_in' as groupreg_in,
                SUM(jml_tagihan) as jml_tagihan
                FROM dat_bhp 
                WHERE '$groupreg_in' LIKE CONCAT('%', reg_id,';%')                
              ) h ON CONVERT(a.groupreg_in USING utf8) = CONVERT(h.groupreg_in USING utf8) 
              LEFT JOIN 
              (
                SELECT 
                '$groupreg_in' as groupreg_in,
                SUM(jml_tagihan) as jml_tagihan
                FROM dat_alkes 
                WHERE '$groupreg_in' LIKE CONCAT('%', reg_id,';%')
              ) i ON CONVERT(a.groupreg_in USING utf8) = CONVERT(i.groupreg_in USING utf8) 
              LEFT JOIN mst_pegawai k ON a.dokter_id = k.pegawai_id
              LEFT JOIN mst_parameter l ON k.spesialisasi_cd = l.parameter_cd AND l.parameter_field = 'spesialisasi_cd'
            WHERE
              a.groupreg_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function list_riwayat_bangsal($reg_id = null, $groupreg_in = null)
  {
    $sql = "SELECT a.*, b.lokasi_nm, c.kelas_nm, d.kamar_nm  
            FROM reg_pasien_kamar a 
            LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
            LEFT JOIN mst_kelas c ON a.kelas_id = c.kelas_id
            LEFT JOIN mst_kamar d ON a.kamar_id = d.kamar_id
            WHERE a.is_deleted = 0 AND ? LIKE CONCAT('%', a.reg_id,';%')
            ORDER BY regkamar_id ASC";
    $query = $this->db->query($sql, $groupreg_in);
    return $query->result_array();
  }

  function detail_item_pembayaran($groupreg_id = '', $tarif_id = '', $groupreg_in = '')
  {
    $sql = "SELECT 
              a.*,
              b.pegawai_nm AS pegawai_nm_1, 
              c.pegawai_nm AS pegawai_nm_2, 
              d.pegawai_nm AS pegawai_nm_3, 
              e.pegawai_nm AS pegawai_nm_4, 
              f.pegawai_nm AS pegawai_nm_5  
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            LEFT JOIN mst_pegawai c ON a.petugas_id_2 = c.pegawai_id
            LEFT JOIN mst_pegawai d ON a.petugas_id_3 = d.pegawai_id
            LEFT JOIN mst_pegawai e ON a.petugas_id_4 = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_5 = f.pegawai_id  
            WHERE ? LIKE CONCAT('%', a.reg_id,';%') AND left(a.tarif_id,6)=?";
    $query = $this->db->query($sql, array($groupreg_in, $tarif_id));
    $result = $query->result_array();
    return $result;
  }

  function list_rincian($groupreg_id = '', $billing_id = '', $groupreg_in = '')
  {
    $sql = "SELECT 
              a.*,
              b.jml_seharusnya, 
              b.jml_potongan,
              IF(b.jml_tagihan IS NULL, b.jml_seharusnya, b.jml_tagihan) as jml_tagihan,
              b.billing_id
            FROM mst_tarif a 
            INNER JOIN 
            (
             (
              SELECT  
               LEFT(a.tarif_id,2) as tarif_id,
               SUM(a.jml_tagihan) as jml_seharusnya,
               b.jml_potongan,
               b.jml_tagihan,
               b.billing_id
              FROM reg_pasien_kamar a
              LEFT JOIN 
              (
               SELECT 
                billing_id,
                LEFT(tarif_id,2) as tarif_id,
                SUM(jml_potongan) as jml_potongan,
                SUM(jml_tagihan) as jml_tagihan
               FROM dat_billing_rinc WHERE billing_id='$billing_id' GROUP BY LEFT(tarif_id,2)
              ) b ON LEFT(a.tarif_id,2)=LEFT(b.tarif_id,2)
              WHERE '$groupreg_in' LIKE CONCAT('%', a.reg_id,';%') GROUP BY LEFT(a.tarif_id,2)
             )
             UNION ALL 
             (
              SELECT  
               LEFT(a.tarif_id,2) as tarif_id,
               SUM(a.jml_tagihan) as jml_seharusnya,
               b.jml_potongan,
               b.jml_tagihan,
               b.billing_id
              FROM dat_tindakan a 
              LEFT JOIN 
              (
               SELECT 
                billing_id,
                LEFT(tarif_id,2) as tarif_id,
                SUM(jml_potongan) as jml_potongan,
                SUM(jml_tagihan) as jml_tagihan
               FROM dat_billing_rinc WHERE billing_id='$billing_id' GROUP BY LEFT(tarif_id,2)
              ) b ON LEFT(a.tarif_id,2)=LEFT(b.tarif_id,2)
              WHERE '$groupreg_in' LIKE CONCAT('%', a.reg_id,';%') GROUP BY LEFT(a.tarif_id,2)
             )
            ) b ON a.tarif_id=b.tarif_id
            WHERE LENGTH(a.tarif_id)='2' ORDER BY a.tarif_id ASC";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    foreach ($result as $key => $val) {
      $result[$key]['tagihan'] = $val;
      $result[$key]['list_detail'] = $this->list_rincian_detail($groupreg_id, $billing_id, $result[$key]['tarif_id'], $groupreg_in);
    }
    return $result;
  }

  function list_rincian_detail($groupreg_id = '', $billing_id = '', $tarif_id = '', $groupreg_in = '')
  {
    $sql = "SELECT 
             a.*,
             b.jml_seharusnya, 
             b.jml_potongan,
             IF(b.jml_tagihan IS NULL, b.jml_seharusnya, b.jml_tagihan) as jml_tagihan,
             b.billing_id
            FROM mst_tarif a 
            INNER JOIN 
            (
             (
              SELECT  
               LEFT(a.tarif_id,6) as tarif_id,
               SUM(a.jml_tagihan) as jml_seharusnya,
               b.jml_potongan,
               b.jml_tagihan,
               b.billing_id
              FROM reg_pasien_kamar a
              LEFT JOIN 
              (
               SELECT 
                billing_id,
                LEFT(tarif_id,6) as tarif_id,
                SUM(jml_potongan) as jml_potongan,
                SUM(jml_tagihan) as jml_tagihan
               FROM dat_billing_rinc WHERE billing_id='$billing_id' GROUP BY LEFT(tarif_id,6)
              ) b ON LEFT(a.tarif_id,6)=LEFT(b.tarif_id,6)
              WHERE '$groupreg_in' LIKE CONCAT('%', a.reg_id,';%') GROUP BY LEFT(a.tarif_id,6)
             )
             UNION ALL 
             (
              SELECT  
               LEFT(a.tarif_id,6) as tarif_id,
               SUM(a.jml_tagihan) as jml_seharusnya,
               b.jml_potongan,
               b.jml_tagihan,
               b.billing_id
              FROM dat_tindakan a 
              LEFT JOIN 
              (
               SELECT 
                billing_id,
                LEFT(tarif_id,6) as tarif_id,
                SUM(jml_potongan) as jml_potongan,
                SUM(jml_tagihan) as jml_tagihan
               FROM dat_billing_rinc WHERE billing_id='$billing_id' GROUP BY LEFT(tarif_id,6)
              ) b ON LEFT(a.tarif_id,6)=LEFT(b.tarif_id,6)
              WHERE '$groupreg_in' LIKE CONCAT('%', a.reg_id,';%') GROUP BY LEFT(a.tarif_id,6)
             )
            ) b ON a.tarif_id=b.tarif_id
            WHERE LENGTH(a.tarif_id)='6' AND LEFT(a.tarif_id,2)='$tarif_id' ORDER BY a.tarif_id ASC";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    foreach ($result as $key => $val) {
      $result[$key]['tagihan'] = $val;
    }
    return $result;
  }

  public function detail_obat($reg_id = '', $groupreg_in = '')
  {
    $sql = "SELECT 
              a.* 
            FROM dat_farmasi a
            WHERE a.is_deleted=0 AND ? LIKE CONCAT('%', a.reg_id,';%')
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, $groupreg_in);
    $result = $query->result_array();
    // 
    foreach ($result as $key => $val) {
      $result[$key]['list_dat_resep'] = $this->list_dat_resep($val['resep_id'], $reg_id, $groupreg_in);
    }
    return $result;
  }

  public function list_dat_resep($resep_id = '', $reg_id = '', $groupreg_in = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_farmasi a
            LEFT JOIN mst_obat b ON a.obat_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.resep_id=? AND ? LIKE CONCAT('%', a.reg_id,';%')
            ORDER BY a.reg_id ASC";
    $query = $this->db->query($sql, array($resep_id, $groupreg_in));
    return $query->result_array();
  }

  public function detail_bhp($reg_id = '', $groupreg_in = '')
  {
    $sql = "SELECT 
              a.*
            FROM dat_bhp a
            WHERE a.is_deleted=0 AND ? LIKE CONCAT('%', a.reg_id,';%')
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, $groupreg_in);
    return $query->result_array();
  }

  public function detail_alkes($reg_id = '', $groupreg_in = '')
  {
    $sql = "SELECT 
              a.*
            FROM dat_alkes a
            WHERE a.is_deleted=0 AND ? LIKE CONCAT('%', a.reg_id,';%')
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, $groupreg_in);
    return $query->result_array();
  }

  public function save($reg_id = null)
  {
    $data = html_escape($this->input->post());

    // update reg_pasien
    $reg_pasien['bayar_st'] = $data['bayar_st'];
    $reg_pasien['updated_at'] = date('Y-m-d H:i:s');
    $reg_pasien['updated_by'] = $this->session->userdata('sess_user_realname');
    $this->db->where('reg_id', $reg_id)->update('reg_pasien', $reg_pasien);

    // insert dat_billing
    $dat_billing['tgl_transaksi'] = to_date($data['tgl_transaksi'], '', 'full_date');
    $dat_billing['reg_id'] = $reg_id;
    $dat_billing['pasien_id'] = $data['pasien_id'];
    $dat_billing['pasien_nm'] = $data['pasien_nm'];
    $dat_billing['lokasi_id'] = $data['lokasi_id'];
    $dat_billing['jenispasien_id'] = $data['jenispasien_id'];
    $dat_billing['tgl_masuk'] = $data['tgl_masuk'];
    $dat_billing['tgl_keluar'] = $data['tgl_keluar'];
    $dat_billing['grand_jml_tindakan'] = clear_numeric($data['grand_jml_tindakan']);
    $dat_billing['grand_jml_obat'] = clear_numeric($data['grand_jml_obat']);
    $dat_billing['grand_jml_alkes'] = clear_numeric($data['grand_jml_alkes']);
    $dat_billing['grand_jml_bhp'] = clear_numeric($data['grand_jml_bhp']);
    $dat_billing['grand_total_bruto'] = clear_numeric($data['grand_total_bruto']);
    $dat_billing['grand_total_potongan'] = clear_numeric($data['grand_total_potongan']);
    $dat_billing['grand_total_netto'] = clear_numeric($data['grand_total_netto']);
    $dat_billing['grand_pembulatan'] = clear_numeric($data['grand_pembulatan']);
    $dat_billing['grand_total_tagihan'] = clear_numeric($data['grand_total_tagihan']);
    $dat_billing['bayar_st'] = $data['bayar_st'];
    $dat_billing['tgl_bayar'] = to_date($data['tgl_bayar'], '', 'full_date');
    $dat_billing['grand_titip_bayar'] = clear_numeric($data['grand_titip_bayar']);
    $dat_billing['grand_total_bayar'] = clear_numeric($data['grand_total_bayar']);
    $dat_billing['keterangan_bayar'] = $data['keterangan_bayar'];
    $dat_billing['user_cd'] = $this->session->userdata('sess_user_cd');

    if ($data['billing_id'] == 'otomatis') {
      $dat_billing['billing_id'] = get_id('dat_billing');
      $dat_billing['created_at'] = date('Y-m-d H:i:s');
      $dat_billing['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_billing', $dat_billing);
      update_id('dat_billing', $dat_billing['billing_id']);
      // set billing_id
      $data['billing_id'] = $dat_billing['billing_id'];
    } else {
      $dat_billing['updated_at'] = date('Y-m-d H:i:s');
      $dat_billing['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('billing_id', $data['billing_id'])->update('dat_billing', $dat_billing);
    }

    // insert dat_billing_rinc
    foreach ($data['tarif_id'] as $key => $val) {

      $cek_dat_billing_rinc = $this->db->where('billing_id', $data['billing_id'])->where('tarif_id', $data['tarif_id'][$key])->get('dat_billing_rinc')->row_array();

      $dat_billing_rinc['billing_id'] = $data['billing_id'];
      $dat_billing_rinc['tarif_id'] = $data['tarif_id'][$key];
      $dat_billing_rinc['jml_seharusnya'] = clear_numeric($data['jml_seharusnya'][$key]);
      $dat_billing_rinc['jml_potongan'] = clear_numeric($data['jml_potongan'][$key]);
      $dat_billing_rinc['jml_tagihan'] = clear_numeric($data['jml_tagihan'][$key]);
      $dat_billing_rinc['data_tp'] = $data['data_tp'][$key];
      //
      if ($cek_dat_billing_rinc['billingrinc_id'] == '') {
        $dat_billing_rinc['billingrinc_id'] = get_id('dat_billing_rinc');
        $dat_billing_rinc['created_at'] = date('Y-m-d H:i:s');
        $dat_billing_rinc['created_by'] = $this->session->userdata('sess_user_realname');
        $this->db->insert('dat_billing_rinc', $dat_billing_rinc);
        update_id('dat_billing_rinc', $dat_billing_rinc['billingrinc_id']);
        // set billingrinc_id
        $billingrinc_id = $dat_billing_rinc['billingrinc_id'];
      } else {
        $dat_billing_rinc['updated_at'] = date('Y-m-d H:i:s');
        $dat_billing_rinc['updated_by'] = $this->session->userdata('sess_user_realname');
        $this->db->where('billing_id', $data['billing_id'])->where('tarif_id', $data['tarif_id'][$key])->update('dat_billing_rinc', $dat_billing_rinc);
        // set billingrinc_id
        $billingrinc_id = @$cek_dat_billing_rinc['billingrinc_id'];
      }

      // update dat_tindakan
      $sql_dat_tindakan = "UPDATE dat_tindakan 
              SET billingrinc_id='" . $billingrinc_id . "'  
              WHERE reg_id='" . $reg_id . "' AND left(tarif_id,6)='" . $data['tarif_id'][$key] . "'";
      $this->db->query($sql_dat_tindakan);

      // update reg_pasien_kamar
      $sql_reg_pasien_kamar = "UPDATE reg_pasien_kamar 
              SET billingrinc_id='" . $billingrinc_id . "'  
              WHERE reg_id='" . $reg_id . "' AND left(tarif_id,6)='" . $data['tarif_id'][$key] . "'";
      $this->db->query($sql_reg_pasien_kamar);
    }

    // update dat_resep_group
    $dat_resep_group['billing_id'] = $data['billing_id'];
    $dat_resep_group['updated_at'] = date('Y-m-d H:i:s');
    $dat_resep_group['updated_by'] = $this->session->userdata('sess_user_realname');
    $this->db->where('reg_id', $reg_id)->update('dat_resep_group', $dat_resep_group);

    // update dat_alkes
    $dat_alkes['billing_id'] = $data['billing_id'];
    $dat_alkes['updated_at'] = date('Y-m-d H:i:s');
    $dat_alkes['updated_by'] = $this->session->userdata('sess_user_realname');
    $this->db->where('reg_id', $reg_id)->update('dat_alkes', $dat_alkes);

    // update dat_bhp
    $dat_bhp['billing_id'] = $data['billing_id'];
    $dat_bhp['updated_at'] = date('Y-m-d H:i:s');
    $dat_bhp['updated_by'] = $this->session->userdata('sess_user_realname');
    $this->db->where('reg_id', $reg_id)->update('dat_bhp', $dat_bhp);
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('stok_id', $id)->update('far_stok', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('stok_id', $id)->delete('far_stok');
      $this->db->where('stok_id', $id)->delete('far_stok_rinc');
    } else {
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('stok_id', $id)->update('far_stok', $data);
      $this->db->where('stok_id', $id)->update('far_stok_rinc', $data);
    }
  }

  public function obat_autocomplete($obat_nm = null)
  {
    $sql = "SELECT 
              a.*, b.parameter_val AS satuan
            FROM mst_obat a
            LEFT JOIN mst_parameter b ON b.parameter_field = 'satuan_cd' AND b.parameter_cd = a.satuan_cd
            WHERE (a.obat_nm LIKE '%$obat_nm%' OR a.obat_id LIKE '%$obat_nm%')
            ORDER BY a.obat_id";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['obat_id'] . "#" . $row['obat_nm'] . '#' . $row['satuan'],
        'text' => $row['obat_id'] . ' - ' . $row['obat_nm']
      );
    }
    return $res;
  }

  public function obat_row($id)
  {
    $sql = "SELECT * FROM mst_obat WHERE obat_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  //surat pesanan
  public function surat_pesanan_row($id)
  {
    $sql = "SELECT * FROM far_stok WHERE order_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  // Tindakan
  public function tarifkelas_autocomplete($tarif_nm = null)
  {
    $sql = "SELECT 
              a.*,b.tarif_nm,c.kelas_nm
            FROM mst_tarif_kelas a
            JOIN mst_tarif b ON a.tarif_id = b.tarif_id
            JOIN mst_kelas c ON a.kelas_id = c.kelas_id
            WHERE b.tarif_nm LIKE '%$tarif_nm%' ";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['tarifkelas_id'],
        'text' => $row['tarifkelas_id'] . ' - ' . $row['tarif_nm'] . ' - ' . $row['kelas_nm']
      );
    }
    return $res;
  }

  public function tarifkelas_row($id)
  {
    $sql = "SELECT a.*, b.tarif_nm, c.kelas_nm FROM mst_tarif_kelas a
      LEFT JOIN mst_tarif b ON a.tarif_id = b.tarif_id
      LEFT JOIN mst_kelas c ON a.kelas_id = c.kelas_id
      WHERE tarifkelas_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  public function petugas_autocomplete($petugas_nm = null)
  {
    $sql = "SELECT 
              a.*
            FROM mst_pegawai a
            WHERE a.pegawai_nm LIKE '%$petugas_nm%' ";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['pegawai_id'],
        'text' => $row['pegawai_nm']
      );
    }
    return $res;
  }

  public function tindakan_get($tindakan_id = '', $reg_id = '', $pasien_id = '')
  {
    $sql = "SELECT 
              a.*, b.kelas_nm, c.pegawai_nm, DATE_FORMAT(a.tgl_catat, '%Y-%m-%d %H:%i:%s') AS tgl_catat
            FROM dat_tindakan a
            LEFT JOIN mst_kelas b ON a.kelas_id = b.kelas_id
            LEFT JOIN mst_pegawai c ON a.petugas_id = c.pegawai_id 
            WHERE a.tindakan_id=? AND a.reg_id=? AND a.pasien_id=?";
    $query = $this->db->query($sql, array($tindakan_id, $reg_id, $pasien_id));
    $row = $query->row_array();
    $jml_petugas = 0;
    if ($row['petugas_id'] != '') {
      $jml_petugas += 1;
    }
    if ($row['petugas_id_2'] != '') {
      $jml_petugas += 1;
    }
    if ($row['petugas_id_3'] != '') {
      $jml_petugas += 1;
    }
    if ($row['petugas_id_4'] != '') {
      $jml_petugas += 1;
    }
    if ($row['petugas_id_5'] != '') {
      $jml_petugas += 1;
    }
    $row['jml_petugas'] = $jml_petugas;
    return $row;
  }

  public function petugas_row($id)
  {
    $sql = "SELECT * FROM mst_pegawai WHERE pegawai_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  public function get_tindakan($tindakan_id = null)
  {
    $sql = "SELECT 
              a.*, 
              b.pegawai_nm AS pegawai_nm_1, 
              c.pegawai_nm AS pegawai_nm_2, 
              d.pegawai_nm AS pegawai_nm_3, 
              e.pegawai_nm AS pegawai_nm_4, 
              f.pegawai_nm AS pegawai_nm_5
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            LEFT JOIN mst_pegawai c ON a.petugas_id_2 = c.pegawai_id
            LEFT JOIN mst_pegawai d ON a.petugas_id_3 = d.pegawai_id
            LEFT JOIN mst_pegawai e ON a.petugas_id_4 = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_5 = f.pegawai_id
            WHERE a.tindakan_id=?";
    $query = $this->db->query($sql, $tindakan_id);
    return $query->row_array();
  }

  public function tindakan_save($reg_id = '')
  {
    $data = $this->input->post();
    unset($data['petugas_no']);
    $data['reg_id'] = $reg_id;
    $data['tgl_catat'] = to_date($data['tgl_catat'], '', 'full_date');
    $data['user_cd'] = $this->session->userdata('sess_user_cd');
    $tk = $this->db
      ->select('a.*, b.tarif_nm')
      ->join('mst_tarif b', 'a.tarif_id = b.tarif_id', 'left')
      ->where('a.tarifkelas_id', $data['tarifkelas_id'])
      ->get('mst_tarif_kelas a')->row_array();
    unset($data['tarifkelas_id']);
    $data['tarif_id'] = $tk['tarif_id'];
    $data['kelas_id'] = $tk['kelas_id'];
    $data['tarif_nm'] = $tk['tarif_nm'];
    $data['js'] = $tk['js'];
    $data['jp'] = $tk['jp'];
    $data['jb'] = $tk['jb'];
    $data['nom_tarif'] = $tk['nominal'];
    $data['jml_awal'] = $tk['nominal'] * $data['qty'];
    $data['jml_tagihan'] = $tk['nominal'] * $data['qty'];

    // petugas
    $d = $data;
    unset($d['petugas_id'], $d['petugas_id_2'], $d['petugas_id_3'], $d['petugas_id_4'], $d['petugas_id_5']);
    $petugas = array();
    for ($i = 1; $i <= 5; $i++) {
      if ($i == '1') {
        $petugas_id = $data['petugas_id'];
      } else {
        $petugas_id = $data['petugas_id_' . $i];
      }

      if ($petugas_id != '0') {
        array_push($petugas, $petugas_id);
      }
    }

    for ($j = 0; $j < count($petugas); $j++) {
      $d['petugas_id_' . ($j + 1)] = $petugas[$j];
    }

    unset($data['petugas_id'], $data['petugas_id_2'], $data['petugas_id_3'], $data['petugas_id_4'], $data['petugas_id_5']);

    $data['petugas_id'] = @$d['petugas_id_1'];
    $data['petugas_id_2'] = @$d['petugas_id_2'];
    $data['petugas_id_3'] = @$d['petugas_id_3'];
    $data['petugas_id_4'] = @$d['petugas_id_4'];
    $data['petugas_id_5'] = @$d['petugas_id_5'];

    if ($data['tindakan_id'] == null) {
      $data['tindakan_id'] = get_id('dat_tindakan');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_tindakan', $data);
      update_id('dat_tindakan', $data['tindakan_id']);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('tindakan_id', $data['tindakan_id'])->update('dat_tindakan', $data);
    }
  }

  public function get_last()
  {
    return $this->db->order_by('billing_id', 'desc')->get('dat_billing')->row_array();
  }

  public function get_billing($id)
  {
    return $this->db->where('groupreg_id', $id)->get('dat_billing')->row_array();
  }

  public function tindakan_data($pasien_id = '', $reg_id = '')
  {
    $sql = "SELECT 
              a.*, 
              g.kelas_nm
            FROM dat_tindakan a
            LEFT JOIN mst_kelas g ON a.kelas_id = g.kelas_id 
            WHERE a.is_deleted = 0 AND a.pasien_id=? AND a.reg_id=?
            ORDER BY a.tindakan_id";
    $query = $this->db->query($sql, array($pasien_id, $reg_id));
    return $query->result_array();
  }

  public function pemberian_obat_data($reg_id = '', $pasien_id = '')
  {
    $sql = "SELECT 
              a.* 
            FROM dat_resep a
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pasien_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pasien_id));
    $result = $query->result_array();
    // 
    foreach ($result as $key => $val) {
      $result[$key]['list_dat_resep'] = $this->list_dat_resep_report($val['resep_id'], $reg_id, $pasien_id);
    }
    return $result;
  }

  public function list_dat_resep_report($resep_id = '', $reg_id = '', $pasien_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_farmasi a
            LEFT JOIN mst_obat b ON a.obat_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.resep_id=? AND a.reg_id=? AND a.pasien_id=?
            ORDER BY a.reg_id ASC";
    $query = $this->db->query($sql, array($resep_id, $reg_id, $pasien_id));
    return $query->result_array();
  }
}
