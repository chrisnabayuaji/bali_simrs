<?php $this->load->view('_js'); ?>
<!--  -->
<form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?=$form_action?>" class="needs-validation" novalidate autocomplete="off">
  <!--  -->
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline col-title">
          <div class="col-back">
            <a href="<?=site_url($nav['nav_module'].'/dashboard')?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
          </div>
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            <?=$nav['nav_nm']?>
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?=site_url('app/dashboard')?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?=site_url($module_nav['nav_url'])?>"><i class="fas fa-folder-open"></i> <?=$module_nav['nav_nm']?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-warehouse"></i> Keuangan</a></li>
            <li class="breadcrumb-item"><a href="<?=site_url($nav['nav_url'])?>"><i class="<?=$nav['nav_icon']?>"></i> <?=$nav['nav_nm']?></a></li>
            <li class="breadcrumb-item active"><span>Index</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>

    <div class="row full-page mt-4">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fa fa-edit"></i> Preview <?=$nav['nav_nm']?></h4>
            <div class="row mt-n1">
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Posisi Tahun</label>
                  <div class="col-lg-3 col-md-3">
                      <select class="form-control tahun chosen-select" name="tahun">
                          <?php for($t=$tahun_awal; $t<=$tahun_akhir; $t++):?>                                                    
                          <option value="<?=$t?>" <?php if($t == @$cookie['search']['tahun']) echo 'selected'?>><?=$t?></option>
                          <?php endfor;?>
                      </select>
                  </div>                  
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Akun Rekening</label>
                  <div class="col-lg-9 col-md-9">
                      <select class="form-control akun_id chosen-select" name="akun_id">
                        <?php foreach($list_akun as $akun):?>
                        <option value="<?=$akun['akun_id']?>" <?php if(@$cookie['search']['akun_id'] == $akun['akun_id']) echo 'selected'?>><?=$akun['akun_id']?> - <?=$akun['akun_nm']?></option>
                        <?php endforeach;?>
                      </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Periode Tanggal</label>
                  <div class="col-lg-3 col-md-3">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datepicker" name="tgl_jurnal_awal" id="tgl_jurnal_awal" value="<?=@$cookie['search']['tgl_jurnal_awal']?>" required aria-invalid="false">
                    </div>
                  </div>                  
                  <div class="col-lg-1 col-md-1">s/d</div>
                  <div class="col-lg-3 col-md-3">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datepicker" name="tgl_jurnal_akhir" id="tgl_jurnal_akhir" value="<?=@$cookie['search']['tgl_jurnal_akhir']?>" required aria-invalid="false">
                    </div>
                  </div>                  
                </div>
                <div class="form-group row">
                  <div class="col-lg-5 col-md-5">
                    <input type="hidden" name="is_search" value="true">
                    <button type="submit" class="float-right btn btn-xs btn-primary btn-submit"><i class="fas fa-search"></i> Tampilkan</button>
                  </div>
                </div>
              </div>
            </div>
            <!--  -->

            <?php if(@$cookie['search']['is_search'] == 'true'):?>            
            <!--  -->
            <br>
            <table class="table table-hover table-striped table-bordered table-fixed">
              <thead>
                <tr>
                  <th class="text-center" width="36">No</th>
                  <th class="text-center" width="60">Tanggal</th>
                  <th class="text-center" width="250">Uraian Jurnal / Akun</th>
                  <th class="text-center" width="60">Ref</th>
                  <th class="text-center" width="80">Debet (Rp)</th>
                  <th class="text-center" width="80">Kredit (Rp)</th>
                  <th class="text-center" width="80">Saldo (Rp)</th>
                </tr>
              </thead>
              <?php if(@$main == null):?>
                <tbody>
                  <tr>
                    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                  </tr>
                </tbody>
              <?php else:?>
                <tbody>
                  <?php foreach($main as $row):?>
                    <tr>
                      <td class="text-center" width="36"><?=$row['no']?></td>
                      <td class="text-center" width="60"><?=to_date($row['tgl_jurnal'])?></td>
                      <td class="text-left" width="250"><?=$row['akun_nm']?></td>
                      <td class="text-center" width="60"><?=$row['akun_id']?></td>
                      <td class="text-center" width="80"><?=num_id($row['sub_nominal_debet'])?></td>
                      <td class="text-center" width="80"><?=num_id($row['sub_nominal_kredit'])?></td>
                      <td class="text-center" width="80"><?=num_id($row['saldo'])?></td>
                    </tr>
                  <?php endforeach;?>
                </tbody>
              <?php endif;?>
            </table>
            <!--  -->
            <?php endif;?>
          </div>
        </div>
      </div>
    </div>
  </div>  
</form>