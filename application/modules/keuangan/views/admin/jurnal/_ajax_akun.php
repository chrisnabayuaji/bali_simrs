                  <script type="text/javascript">
                  $(function() {
                    // autonumeric
                    $(".autonumeric").autoNumeric({ aSep: ".", aDec: ",", vMax: "999999999999999", vMin: "0" });
                    // select2
                    $(".chosen-select").select2();
                    // autocomplete
                    $('.akun_id').select2({
                      minimumInputLength: 2,
                      ajax: {
                        url: "<?=site_url($nav['nav_url'])?>/ajax/akun_autocomplete",
                        dataType: "json",
                        
                        data: function(params) {
                          return{
                            akun_nm : params.term
                          };
                        },
                        processResults: function(data){
                          return{
                            results: data
                          }
                        }
                      }
                    });
                    //
                    // keyup
                    $('.sub_nominal_debet').bind('keyup',function() {
                      var total = _get_total_sub_nominal('debet');
                      $('#total_sub_nominal_debet').autoNumericSet(total);
                    });
                    $('.sub_nominal_kredit').bind('keyup',function() {
                      var total = _get_total_sub_nominal('kredit');
                      $('#total_sub_nominal_kredit').autoNumericSet(total);
                    });
                    //
                    function _get_total_sub_nominal(tp_saldo) {
                        var t = 0;
                        $('.sub_nominal_'+tp_saldo).each(function() {
                            var i = ($(this).val() != '' ? $(this).val().replace(/[.]/g,"") : 0);
                                t += parseFloat(i);
                        });
                        return t;
                    }
                  });
                  </script>
                  <!--  -->
                  <div class="form-group row">
                    <label class="col-lg-2 col-md-2 col-form-label">Akun <?=ucwords($tp_saldo)?></label>
                    <div class="col-lg-6 col-md-6">
                      <select class="form-control autocomplete akun_id" name="akun_id[<?=$tp_saldo?>][]">
                        <option value="">- Pilih -</option>
                      </select>
                    </div>
                    <div class="col-lg-3 col-md-3">
                        <input type="text" class="form-control autonumeric text-right sub_nominal_<?=$tp_saldo?>" name="sub_nominal[<?=$tp_saldo?>][]">
                    </div>
                  </div>