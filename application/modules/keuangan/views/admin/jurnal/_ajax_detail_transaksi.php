<div class="row">
  <div class="col-12">
    <div class="table-responsive">
      <table class="table table-hover table-bordered table-fixed">
        <thead>
          <tr>
            <th class="text-center" width="30">No</th>
            <th class="text-center" width="50">No.Ref</th>
            <th class="text-center" width="150">Uraian</th>
            <th class="text-center" width="80">Jml.Awal</th>
            <th class="text-center" width="30">Qty</th>
            <th class="text-center" width="80">Jml.Tagihan</th>
            <th class="text-center" width="150">Nama Pasien</th>
            <th class="text-center" width="50">No.RM</th>
            <th class="text-center" width="100">Lokasi Pelayanan</th>
            <th class="text-center" width="100">Waktu Transaksi</th>
          </tr>
        </thead>
        <?php if(count($main) == 0):?>
          <tbody style="height: 100% !important;">
            <tr>
              <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
            </tr>
          </tbody>
        <?php else:?>
          <tbody style="height: 100% !important;">
            <?php $i=1; $grand_jml_tagihan=0; foreach($main as $row): ?>
            <tr>
              <td class="text-center" width="30"><?=$i++?></td>
              <td class="text-left" width="50"><?=$row['transaksi_id']?></td>
              <td class="text-left" width="150"><?=$row['uraian']?></td>
              <td class="text-right" width="80"><?=num_id($row['jml_awal'])?></td>
              <td class="text-center" width="30"><?=$row['qty']?></td>
              <td class="text-right" width="80"><?=num_id($row['jml_tagihan'])?></td>
              <td class="text-center" width="150"><?=$row['pasien_nm']?></td>
              <td class="text-center" width="50"><?=$row['pasien_id']?></td>
              <td class="text-center" width="100"><?=$row['lokasi_nm']?></td>
              <td class="text-center" width="100"><?=to_date($row['tgl_catat'],'-','full_date')?></td>
            </tr>
            <?php $grand_jml_tagihan += $row['jml_tagihan']; endforeach; ?>
            <tr>
              <td class="text-left" width="163" colspan="5"><b>Total Tagihan Rp. <?=num_id($grand_jml_tagihan)?></b></td>
            </tr>
          </tbody>          
        <?php endif; ?>
      </table>
    </div>
  </div>
</div>
<div class="border-top border-2 mt-2 pt-2 pb-0">
  <div class="row">
    <div class="offset-lg-11 offset-md-11">
      <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Tutup</button>
    </div>
  </div>
</div>