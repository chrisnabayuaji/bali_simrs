<script type="text/javascript">
$(function() {
  $(document).ready(function () {
    $("#form-data").validate( {
      rules: {
        // null
      },
      messages: {
        // null
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container"));
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });
  })
  // select2
  $(".chosen-select").select2();
  $('.select2-container').css('width', '100%');
  //datepicker
  $('.datepicker').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    locale: {
      cancelLabel: 'Clear',
      format: 'DD-MM-YYYY'
    },
    isInvalidDate: function(date) {
      return '';
    }
  })
  //
  // autocomplete
  $('.akun_id').select2({
    minimumInputLength: 2,
    ajax: {
      url: "<?=site_url($nav['nav_url'])?>/ajax/akun_autocomplete",
      dataType: "json",
      
      data: function(params) {
        return{
          akun_nm : params.term
        };
      },
      processResults: function(data){
        return{
          results: data
        }
      }
    }
  });
  //
  // add_item_akun
  $('#add_item_debet').bind('click',function(e) {
    e.preventDefault();
    _add_item_akun('debet','insert');
  });
  $('#add_item_kredit').bind('click',function(e) {
    e.preventDefault();
    _add_item_akun('kredit','insert');
  });
  // on load
  _add_item_akun('debet','<?=($id != "" ? "update" : "insert")?>');
  _add_item_akun('kredit','<?=($id != "" ? "update" : "insert")?>');
  //
  function _add_item_akun(tp_saldo,tp_data) {
    if(tp_data == 'insert') {
      $.get('<?=site_url().'/'.$nav['nav_url']?>/ajax/add_item_akun?tp_saldo='+tp_saldo,null,function(data) {
        $('#box_'+tp_saldo).append(data.html);
      },'json');
    }    
  }
  //
  // keyup
  $('.sub_nominal_debet').bind('keyup',function() {
    var total = _get_total_sub_nominal('debet');
    $('#total_sub_nominal_debet').val(total);
  });
  $('.sub_nominal_kredit').bind('keyup',function() {
    var total = _get_total_sub_nominal('kredit');
    $('#total_sub_nominal_kredit').val(total);
  });
  //
  _set_val_total_sub_nominal('debet');
  _set_val_total_sub_nominal('kredit');
  function _set_val_total_sub_nominal(tp_saldo) {
    var total = _get_total_sub_nominal(tp_saldo);
    $('#total_sub_nominal_'+tp_saldo).autoNumericSet(total);
  }
  function _get_total_sub_nominal(tp_saldo) {
      var t = 0;
      $('.sub_nominal_'+tp_saldo).each(function() {
          var i = ($(this).val() != '' ? $(this).val().replace(/[.]/g,"") : 0);
              t += parseFloat(i);
      });
      return t;
  }
  //
  $('#tgl_jurnal, #get_data_trx').bind('change',function() {
    var tgl_jurnal = $('#tgl_jurnal').val();
    var get_data_trx = $('#get_data_trx').val();
    location.href = '<?=site_url().'/'.$nav['nav_url']?>/form?tgl_jurnal='+tgl_jurnal+'&get_data_trx='+get_data_trx;
  });
});
</script>