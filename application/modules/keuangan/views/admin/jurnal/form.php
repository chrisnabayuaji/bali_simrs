<?php $this->load->view('_js'); ?>
<div class="flash-success" data-flashsuccess="<?=$this->session->flashdata('flash_success')?>"></div>
<form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?=$form_action?>" class="needs-validation" novalidate autocomplete="off">
  <!--  -->
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline col-title">
          <div class="col-back">
            <a href="<?=site_url($nav['nav_module'].'/dashboard')?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
          </div>
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            <?=$nav['nav_nm']?>
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?=site_url('app/dashboard')?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?=site_url($module_nav['nav_url'])?>"><i class="fas fa-folder-open"></i> <?=$module_nav['nav_nm']?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-warehouse"></i> Keuangan</a></li>
            <li class="breadcrumb-item"><a href="<?=site_url($nav['nav_url'])?>"><i class="<?=$nav['nav_icon']?>"></i> <?=$nav['nav_nm']?></a></li>
            <li class="breadcrumb-item active"><span>Form</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>

    <div class="row full-page mt-4">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fa fa-edit"></i> Form <?=$nav['nav_nm']?></h4>
            <div class="row mt-n1">
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Tanggal Jurnal <span class="text-danger">*<span></label>
                  <div class="col-lg-3 col-md-3">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datepicker" name="tgl_jurnal" id="tgl_jurnal" value="<?php if(@$main['tgl_jurnal']){echo to_date(@$main['tgl_jurnal']);}else{echo date('d-m-Y');}?>" required aria-invalid="false">
                    </div>
                  </div>                  
                  <label class="col-lg-3 col-md-3 col-form-label">Ambil Transaksi SIMRS ?</label>
                  <div class="col-lg-3 col-md-3">
                      <select name="get_data_trx" id="get_data_trx" class="form-control chosen-select">
                          <option value="tidak" <?php if(@$main['get_data_trx'] == 'tidak') echo 'selected'?>>Tidak</option>
                          <option value="ya" <?php if(@$main['get_data_trx'] == 'ya') echo 'selected'?>>Ya</option>
                      </select>
                  </div>                  
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Uraian Jurnal <span class="text-danger">*<span></label>
                  <div class="col-lg-9 col-md-9">
                      <textarea name="uraian" class="form-control" rows="5" required=""><?=@$main['uraian']?></textarea>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <label class="col-lg-5 col-md-5 col-form-label" style="color:red"><i class="fa fa-bars"></i> Pastikan Total Debet = Total Kredit</label>
              </div>
            </div>
            <!--  -->            
            <!--  -->
            <div class="row mt-n1">
              <div class="col-md-6">
                <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fa fa-file"></i> DEBET (D)</h4>                
                <div id="box_debet">             
                  <!--  -->
                  <?php if(!empty(@$main['list_item_debet'])):?>     
                     <?php foreach($main['list_item_debet'] as $debet):?>
                     <div class="form-group row">
                      <label class="col-lg-2 col-md-2 col-form-label">Akun Debet</label>
                      <div class="col-lg-6 col-md-6">
                        <select class="form-control autocomplete akun_id" name="akun_id[debet][]">
                          <option value="<?=$debet['akun_id']?>"><?=$debet['akun_id']?> - <?=$debet['akun_nm']?></option>
                        </select>
                      </div>
                      <div class="col-lg-3 col-md-3">
                          <input type="hidden" name="jurnalitem_id[debet][]" value="<?=$debet['jurnalitem_id']?>">
                          <input type="text" class="form-control autonumeric text-right sub_nominal_debet" name="sub_nominal[debet][]" value="<?=num_id($debet['sub_nominal'])?>">
                      </div>
                      <div class="col-lg-1 col-md-1">
                          <a href="javascript:void(0)" data-href="<?=site_url().'/'.$nav['nav_url'].'/detail_transaksi/'.$debet['akun_id'].'/'.$debet['tgl_jurnal']?>" modal-title="Detail Transaksi : <?=$debet['akun_nm']?>" modal-size="lg" class="btn btn-xs btn-default float-right modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Detail Transaksi : <?=$debet['akun_nm']?>"><i class="fas fa-search"></i></a>
                      </div>
                    </div>
                    <?php endforeach;?>
                  <?php endif;?>
                  <!--  -->
                </div>
                <div class="form-group row">
                  <div class="col-lg-4 col-md-4">
                      <a href="javascript:void(0)" id="add_item_debet" class="btn btn-xs btn-default float-right" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Tambah Item Debet" data-original-title="Tambah Item Debet"><i class="fas fa-plus-circle"></i> Tambah Item</a>
                  </div>
                  <label class="col-lg-4 col-md-2 col-form-label"><b>Total Debet Rp.</b></label>
                  <div class="col-lg-3 col-md-3">
                      <input type="text" class="form-control autonumeric text-right" name="total_sub_nominal_debet" id="total_sub_nominal_debet" readonly="true">
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fa fa-file"></i> KREDIT (K)</h4>                
                <div id="box_kredit">                  
                  <!--  -->
                  <?php if(!empty(@$main['list_item_kredit'])):?>     
                     <?php foreach($main['list_item_kredit'] as $kredit):?>
                     <div class="form-group row">
                      <label class="col-lg-2 col-md-2 col-form-label">Akun Kredit</label>
                      <div class="col-lg-6 col-md-6">
                        <select class="form-control autocomplete akun_id" name="akun_id[kredit][]">
                          <option value="<?=$kredit['akun_id']?>"><?=$kredit['akun_id']?> - <?=$kredit['akun_nm']?></option>
                        </select>
                      </div>
                      <div class="col-lg-3 col-md-3">
                          <input type="hidden" name="jurnalitem_id[kredit][]" value="<?=$kredit['jurnalitem_id']?>">
                          <input type="text" class="form-control autonumeric text-right sub_nominal_kredit" name="sub_nominal[kredit][]" value="<?=num_id($kredit['sub_nominal'])?>">
                      </div>
                      <div class="col-lg-1 col-md-1">
                          <a href="javascript:void(0)" data-href="<?=site_url().'/'.$nav['nav_url'].'/detail_transaksi/'.$kredit['akun_id'].'/'.$kredit['tgl_jurnal']?>" modal-title="Detail Transaksi : <?=$kredit['akun_nm']?>" modal-size="lg" class="btn btn-xs btn-default float-right modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Detail Transaksi : <?=$kredit['akun_nm']?>"><i class="fas fa-search"></i></a>
                      </div>
                    </div>
                    <?php endforeach;?>
                  <?php endif;?>
                  <!--  -->
                </div>
                <div class="form-group row">
                  <div class="col-lg-4 col-md-4">
                      <a href="javascript:void(0)" id="add_item_kredit" class="btn btn-xs btn-default float-right" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Tambah Item Kredit" data-original-title="Tambah Item Kredit"><i class="fas fa-plus-circle"></i> Tambah Item</a>
                  </div>
                  <label class="col-lg-4 col-md-2 col-form-label"><b>Total Kredit Rp.</b></label>
                  <div class="col-lg-3 col-md-3">
                      <input type="text" class="form-control autonumeric text-right" name="total_sub_nominal_kredit" id="total_sub_nominal_kredit" readonly="true">
                  </div>
                </div>
              </div>
            </div>
            <!--  -->
          </div>
        </div>
      </div>
    </div>
  </div>  
  <div class="btn-form">
    <div class="btn-form-action">
      <div class="btn-form-action-bottom w-100 small-text clearfix">
        <div class="col-lg-2">
          <a href="<?=site_url().'/'.$nav['nav_url']?>" class="btn btn-xs btn-secondary"><i class="mdi mdi-keyboard-backspace"></i> Kembali</a>
        </div>
        <div class="col-lg-2 offset-8">
          <button type="submit" class="float-right btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
          <button type="reset" onClick="window.location.reload();" class="float-right btn btn-xs btn-secondary btn-clear mr-2"><i class="fas fa-sync-alt"></i> Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>