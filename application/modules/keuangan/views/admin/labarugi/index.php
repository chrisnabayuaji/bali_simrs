<?php $this->load->view('_js'); ?>
<!--  -->
<form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?=$form_action?>" class="needs-validation" novalidate autocomplete="off">
  <!--  -->
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline col-title">
          <div class="col-back">
            <a href="<?=site_url($nav['nav_module'].'/dashboard')?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
          </div>
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            <?=$nav['nav_nm']?>
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?=site_url('app/dashboard')?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?=site_url($module_nav['nav_url'])?>"><i class="fas fa-folder-open"></i> <?=$module_nav['nav_nm']?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-warehouse"></i> Keuangan</a></li>
            <li class="breadcrumb-item"><a href="<?=site_url($nav['nav_url'])?>"><i class="<?=$nav['nav_icon']?>"></i> <?=$nav['nav_nm']?></a></li>
            <li class="breadcrumb-item active"><span>Index</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>

    <div class="row full-page mt-4">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fa fa-edit"></i> Preview <?=$nav['nav_nm']?></h4>
            <div class="row mt-n1">
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Posisi Tahun</label>
                  <div class="col-lg-3 col-md-3">
                      <select class="form-control tahun chosen-select" name="tahun">
                          <?php for($t=$tahun_awal; $t<=$tahun_akhir; $t++):?>                                                    
                          <option value="<?=$t?>" <?php if($t == @$cookie['search']['tahun']) echo 'selected'?>><?=$t?></option>
                          <?php endfor;?>
                      </select>
                  </div>                  
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Periode Tanggal</label>
                  <div class="col-lg-3 col-md-3">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datepicker" name="tgl_jurnal_awal" id="tgl_jurnal_awal" value="<?=@$cookie['search']['tgl_jurnal_awal']?>" required aria-invalid="false">
                    </div>
                  </div>                  
                  <div class="col-lg-1 col-md-1">s/d</div>
                  <div class="col-lg-3 col-md-3">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datepicker" name="tgl_jurnal_akhir" id="tgl_jurnal_akhir" value="<?=@$cookie['search']['tgl_jurnal_akhir']?>" required aria-invalid="false">
                    </div>
                  </div>                  
                </div>
                <div class="form-group row">
                  <div class="col-lg-5 col-md-5">
                    <input type="hidden" name="is_search" value="true">
                    <button type="submit" class="float-right btn btn-xs btn-primary btn-submit"><i class="fas fa-search"></i> Tampilkan</button>
                  </div>
                </div>
              </div>
            </div>
            <!--  -->

            <?php if(@$cookie['search']['is_search'] == 'true'):?>            
            <!--  -->
            <br>
            <div class="row mt-n1">
              <div class="col-md-6">
                
                  <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fa fa-file"></i> PENDAPATAN</h4>
                  <table class="table table-bordered table-condensed table-hover table-striped" id="responsiveTable">
                  <thead>
                      <tr>
                          <th class="text-center" style="height: 40px; vertical-align: middle;">Kode Akun</th>
                          <th class="text-center" style="height: 40px; vertical-align: middle;">Nama Akun</th>
                          <th class="text-center" style="height: 40px; vertical-align: middle;">Jumlah</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td style="background-color: #fff4a8" class="bold" colspan="2"><b>TOTAL PENDAPATAN</b></td>
                          <td style="background-color: #fff4a8" class="bold text-right"><?=num_id($total_pendapatan)?></td>
                      </tr>
                      <?php foreach($list_pendapatan as $row):?>
                      <tr>
                          <td <?php if(@$row['tp_akun'] == 'G') echo 'style=font-weight:bold!important'?> class="text-left"><?=$row['akun_id']?></td>
                          <td <?php if(@$row['tp_akun'] == 'G') echo 'style=font-weight:bold!important'?> class="text-left"><?=$row['akun_nm']?></td>
                          <td <?php if(@$row['tp_akun'] == 'G') echo 'style=font-weight:bold!important'?> class="text-right"><?=num_id($row['jumlah'])?></td>
                      </tr>
                      <?php endforeach;?>
                  </tbody>
                  </table>

              </div>
              <div class="col-md-6">
                  
                  <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fa fa-file"></i> PENGELUARAN</h4>
                  <table class="table table-bordered table-condensed table-hover table-striped" id="responsiveTable">
                  <thead>
                      <tr>
                          <th class="text-center" style="height: 40px; vertical-align: middle;">Kode Akun</th>
                          <th class="text-center" style="height: 40px; vertical-align: middle;">Nama Akun</th>
                          <th class="text-center" style="height: 40px; vertical-align: middle;">Jumlah</th>
                      </tr>
                  </thead>
                  <tbody>
                      <tr>
                          <td style="background-color: #fff4a8" class="bold" colspan="2"><b> TOTAL PENGELUARAN </b></td>
                          <td style="background-color: #fff4a8" class="bold text-right"><?=num_id($total_pengeluaran)?></td>
                      </tr>
                      <?php foreach($list_pengeluaran as $row):?>
                      <tr>
                          <td <?php if(@$row['tp_akun'] == 'G') echo 'style=font-weight:bold!important'?> class="text-left"><?=$row['akun_id']?></td>
                          <td <?php if(@$row['tp_akun'] == 'G') echo 'style=font-weight:bold!important'?> class="text-left"><?=$row['akun_nm']?></td>
                          <td <?php if(@$row['tp_akun'] == 'G') echo 'style=font-weight:bold!important'?> class="text-right"><?=num_id($row['jumlah'])?></td>
                      </tr>
                      <?php endforeach;?>
                  </tbody>
                  </table>

              </div>
            </div>
            <br>
            <div class="row mt-n1">
              <div class="col-md-6">
                
                  <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fa fa-file"></i> RESUME AKHIR</h4>
                  <table class="table table-bordered table-condensed table-hover table-striped" id="responsiveTable">
                  <tr>
                      <td class="bold" width="70%">TOTAL PENDAPATAN</td>
                      <td class="bold text-right" width="30%"><?=num_id($total_pendapatan)?></td>
                  </tr>
                  <tr>
                      <td class="bold">TOTAL PENGELUARAN</td>
                      <td class="bold text-right"><?=num_id($total_pengeluaran)?></td>
                  </tr>
                  <tr>
                      <td class="bold">LABA BERSIH</td>
                      <td class="bold text-right"><?=num_id($total_laba)?></td>
                  </tr>
                  </table>

              </div>
              <div class="col-md-6">

              </div>
            </div>
            <!--  -->
            <?php endif;?>
          </div>
        </div>
      </div>
    </div>
  </div>  
</form>