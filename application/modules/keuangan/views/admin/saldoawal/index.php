<?php $this->load->view('_js'); ?>
<!--  -->
  <!--  -->
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline col-title">
          <div class="col-back">
            <a href="<?=site_url($nav['nav_module'].'/dashboard')?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
          </div>
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            <?=$nav['nav_nm']?>
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?=site_url('app/dashboard')?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?=site_url($module_nav['nav_url'])?>"><i class="fas fa-folder-open"></i> <?=$module_nav['nav_nm']?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-warehouse"></i> Keuangan</a></li>
            <li class="breadcrumb-item"><a href="<?=site_url($nav['nav_url'])?>"><i class="<?=$nav['nav_icon']?>"></i> <?=$nav['nav_nm']?></a></li>
            <li class="breadcrumb-item active"><span>Index</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>
    <!-- flash message -->
    <div class="flash-success" data-flashsuccess="<?=$this->session->flashdata('flash_success')?>"></div>
    <!-- /flash message -->
    <div class="row full-page mt-4">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fa fa-edit"></i> Preview <?=$nav['nav_nm']?></h4>

            <form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?=$form_action?>" class="needs-validation" novalidate autocomplete="off">
            <div class="row mt-n1">
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Posisi Tahun</label>
                  <div class="col-lg-3 col-md-3">
                      <select class="form-control tahun chosen-select" name="tahun">
                          <?php for($t=$tahun_awal; $t<=$tahun_akhir; $t++):?>                                                    
                          <option value="<?=$t?>" <?php if($t == @$cookie['search']['tahun']) echo 'selected'?>><?=$t?></option>
                          <?php endfor;?>
                      </select>
                  </div>                  
                </div>
                <div class="form-group row">
                  <div class="col-lg-5 col-md-5">
                    <input type="hidden" name="is_search" value="true">
                    <button type="submit" class="float-right btn btn-xs btn-primary btn-submit"><i class="fas fa-search"></i> Tampilkan</button>
                  </div>
                </div>
              </div>
            </div>
            </form>

            <!--  -->

            <?php if(@$cookie['search']['is_search'] == 'true'):?>            
            <!--  -->
            <br>
            <form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?=$form_action_save?>" class="needs-validation" novalidate autocomplete="off">
            <input type="hidden" name="tahun" value="<?=$cookie['search']['tahun']?>">
            <table class="table table-bordered table-condensed table-hover table-striped" id="responsiveTable">
            <thead>
                <tr>
                    <th class="text-center" width="3%">No</th>                                  
                    <th class="text-center" width="10%">Kode Akun</th>
                    <th class="text-center" width="30%">Nama Akun</th>
                    <th class="text-center" width="9%">Posisi Data</th>
                    <th class="text-center" width="9%">Posisi Saldo</th>
                    <th class="text-center" width="9%">Posisi Laporan</th>
                    <th class="text-center" width="9%">Posisi Sub Lap</th>
                    <th class="text-center" width="10%">Saldo Awal <?=$cookie['search']['tahun']?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($main as $row):?>
                <tr>
                    <td class="text-center"><?=$row['no']?></td>
                    <td <?php if(@$row['tp_akun'] == 'G') echo 'style="font-weight:bold!important"'?> class="text-left"><?=$row['akun_id']?></td>         
                    <td <?php if(@$row['tp_akun'] == 'G') echo 'style="font-weight:bold!important"'?> class="text-left"><?=$row['akun_nm']?></td>         
                    <td <?php if(@$row['tp_akun'] == 'G') echo 'style="font-weight:bold!important"'?> class="text-center"><?=$row['tp_akun_nm']?></td>         
                    <td <?php if(@$row['tp_akun'] == 'G') echo 'style="font-weight:bold!important"'?> class="text-center"><?=$row['tp_saldo_nm']?></td>         
                    <td <?php if(@$row['tp_akun'] == 'G') echo 'style="font-weight:bold!important"'?> class="text-center"><?=$row['tp_laporan_nm']?></td>         
                    <td <?php if(@$row['tp_akun'] == 'G') echo 'style="font-weight:bold!important"'?> class="text-center"><?=$row['tp_laporan_sub_nm']?></td>         
                    <td <?php if(@$row['tp_akun'] == 'G') echo 'style="font-weight:bold!important"'?> class="text-right">
                      <?php if($row['tp_akun'] == 'G'):?>
                      <input type="text" class="form-control input-sm autonumeric text-right" value="<?=num_id($row['saldo_awal'])?>" disabled="true">  
                      <?php else:?>
                      <input type="text" class="form-control input-sm autonumeric text-right" name="saldo_awal_<?=$cookie['search']['tahun']?>[]" value="<?=num_id($row['saldo_awal'])?>">  
                      <input type="hidden" name="akun_id[]" value="<?=$row['akun_id']?>">
                      <?php endif;?>
                    </td>           
                </tr>
                <?php endforeach;?>
            </tbody>
            </table>

            <div class="btn-form">
              <div class="btn-form-action">
                <div class="btn-form-action-bottom w-100 small-text clearfix">
                  <div class="col-lg-2">                    
                  </div>
                  <div class="col-lg-2 offset-8">
                    <button type="submit" class="float-right btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
                    <button type="reset" onClick="window.location.reload();" class="float-right btn btn-xs btn-secondary btn-clear mr-2"><i class="fas fa-sync-alt"></i> Batal</button>
                  </div>
                </div>
              </div>
            </div>
            </form>
            <!--  -->
            <?php endif;?>
          </div>
        </div>
      </div>
    </div>
  </div>  