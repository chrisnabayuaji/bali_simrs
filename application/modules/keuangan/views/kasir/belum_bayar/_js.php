<script type="text/javascript">
  $(document).ready(function() {
    $("#form-data").validate({
      rules: {},
      messages: {},
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        // $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        // $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {


        var grand_total_tagihan = num_sys($("#grand_total_tagihan").val());
        var grand_total_bayar = num_sys($("#grand_total_bayar").val());

        console.log(grand_total_tagihan);
        console.log(grand_total_bayar);
        if (parseInt(grand_total_bayar) < parseInt(grand_total_tagihan)) {
          $.toast({
            heading: "Error",
            text: "Uang yang di bayarkan kurang!",
            icon: "error",
            position: "top-right",
          });
        } else {
          $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
          $(".btn-submit").attr("disabled", "disabled");
          $(".btn-cancel").attr("disabled", "disabled");
          form.submit();
        }
      }
    });

    rincian_pembayaran('<?= @$id ?>', '<?= @$groupreg_in ?>');
    rincian_pembayaran_parsial('<?= @$id ?>', '<?= @$main['pasien_id'] ?>');

    <?php if (@$main['bayar_st'] == 1) : ?>
      $('#box_lunas').removeClass('d-none');
      $('#box_kembalian').removeClass('d-none');
      $('#box_titip').addClass('d-none');
    <?php elseif (@$main['bayar_st'] == 2) : ?>
      $('#box_titip').removeClass('d-none');
      $('#box_lunas').addClass('d-none');
      $('#box_kembalian').addClass('d-none');
    <?php endif; ?>

    $('#bayar_st').bind('change', function(e) {
      e.preventDefault();
      var i = $(this).val();
      if (i == 1) {
        $('#box_lunas').removeClass('d-none');
        $('#box_kembalian').removeClass('d-none');
        $('#box_titip').addClass('d-none');
      } else if (i == 2) {
        $('#box_titip').removeClass('d-none');
        $('#box_lunas').addClass('d-none');
        $('#box_kembalian').addClass('d-none');
      }
    })

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    $('.jml_potongan').bind('keyup', function() {
      var sum = 0;
      $(".jml_potongan_group").each(function() {
        sum += +($(this).val() != '' ? num_sys($(this).val()) : '0');
      });
      $(".jml_potongan").each(function() {
        sum += +($(this).val() != '' ? num_sys($(this).val()) : '0');
      });
      $("#tot_jml_potongan").autoNumericSet(sum);
      $("#grand_total_potongan").autoNumericSet(sum);

      var grand_total_bruto = ($('#grand_total_bruto').val() != '' ? num_sys($('#grand_total_bruto').val()) : '0');
      var grand_total_potongan = ($('#grand_total_potongan').val() != '' ? num_sys($('#grand_total_potongan').val()) : '0');
      var res_sum = grand_total_bruto - grand_total_potongan;
      $("#grand_total_netto").autoNumericSet(res_sum);
      $("#grand_total_tagihan").autoNumericSet(res_sum);
    });

    $('#grand_pembulatan').bind('keyup', function() {
      var grand_total_netto = ($('#grand_total_netto').val() != '' ? num_sys($('#grand_total_netto').val()) : '0');
      var grand_pembulatan = ($(this).val() != '' ? num_sys($(this).val()) : '0');
      var res_sum = parseFloat(grand_total_netto) + parseFloat(grand_pembulatan);
      $("#grand_total_tagihan").autoNumericSet(res_sum);
    });

    <?php if (@$main['bayar_st'] == 1) : ?>
      // var grand_total_tagihan = ($('#grand_total_tagihan').val() != '' ? num_sys($('#grand_total_tagihan').val()) : '0');
      // var grand_total_bayar = ($('#grand_total_bayar').val() != '' ? num_sys($('#grand_total_bayar').val()) : '0');
      // var res_sum = grand_total_bayar - grand_total_tagihan;
      // $("#kembalian").autoNumericSet(res_sum);
    <?php endif; ?>

    $('#grand_total_bayar').bind('keyup', function() {
      var grand_total_tagihan = ($('#grand_total_tagihan').val() != '' ? num_sys($('#grand_total_tagihan').val()) : '0');
      var grand_total_bayar = ($(this).val() != '' ? num_sys($(this).val()) : '0');
      var res_sum = grand_total_bayar - grand_total_tagihan;
      if (res_sum < 0) {
        $(this).addClass('is-invalid');
        $('#pembayaran_kurang').addClass('d-block').removeClass('d-none');
      } else {
        $(this).removeClass('is-invalid');
        $('#pembayaran_kurang').addClass('d-none').removeClass('d-block');
      }
      $("#kembalian").autoNumericSet(res_sum);
    });

    $(".modal-href-custom").click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");
      var modal_header = $(this).attr("modal-header");
      var modal_content_top = $(this).attr("modal-content-top");

      $("#modal-size")
        .removeClass("modal-lg")
        .removeClass("modal-md")
        .removeClass("modal-sm");

      $("#modal-title").html(modal_title);
      $("#modal-size").addClass("modal-" + modal_size);
      if (modal_custom_size) {
        $("#modal-size").attr(
          "style",
          "max-width: " + modal_custom_size + "px !important"
        );
      }
      if (modal_content_top) {
        $(".modal-content-top").attr(
          "style",
          "margin-top: " + modal_content_top + " !important"
        );
      }
      if (modal_header == "hidden") {
        $("#modal-header").addClass("d-none");
      } else {
        $("#modal-header").removeClass("d-none");
      }
      $("#myModal").modal("show");
      $("#modal-body").html(
        '<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i><br>Loading</div>'
      );

      $.ajax({
        type: 'post',
        url: $(this).data("href"),
        dataType: 'json',
        success: function(data) {
          $("#modal-body").html(data.html);
        }
      })

      // $.post(
      //   $(this).data("href"),
      //   function(data) {
      //     $("#modal-body").html(data.html);
      //   },
      //   "json"
      // );
    });

    $(".modal-href-add-item").click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");
      var modal_header = $(this).attr("modal-header");
      var modal_content_top = $(this).attr("modal-content-top");

      $("#modal-size")
        .removeClass("modal-lg")
        .removeClass("modal-md")
        .removeClass("modal-sm");

      $("#modal-title").html(modal_title);
      $("#modal-size").addClass("modal-" + modal_size);
      if (modal_custom_size) {
        $("#modal-size").attr(
          "style",
          "max-width: " + modal_custom_size + "px !important"
        );
      }
      if (modal_content_top) {
        $(".modal-content-top").attr(
          "style",
          "margin-top: " + modal_content_top + " !important"
        );
      }
      if (modal_header == "hidden") {
        $("#modal-header").addClass("d-none");
      } else {
        $("#modal-header").removeClass("d-none");
      }
      $("#myModal").modal("show");
      $("#modal-body").html(
        '<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i><br>Loading</div>'
      );

      $.ajax({
        type: 'post',
        url: $(this).data("href"),
        dataType: 'json',
        success: function(data) {
          $("#modal-body").html(data.html);
        }
      })

      // $.post(
      //   $(this).data("href"),
      //   function(data) {
      //     $("#modal-body").html(data.html);
      //   },
      //   "json"
      // );
    });
  })

  function rincian_pembayaran(id = '', groupreg_in = '') {
    $('#rincian_pembayaran').html('<tr><td class="text-center" colspan="99"><div class="mt-3 mb-2"><i class="fas fa-spin fa-spinner fa-2x"></i><div style="font-size: 14px;" class="mt-1">Memuat...</div></div></td></tr>');
    $('#form_grand_total_bruto').html('<div class="text-right mt-1"><i class="fas fa-spin fa-spinner"></i> Memuat...</div>');
    $('#form_total_potongan').html('<div class="text-right mt-1"><i class="fas fa-spin fa-spinner"></i> Memuat...</div>');
    $('#form_total_akhir').html('<div class="text-right mt-1"><i class="fas fa-spin fa-spinner"></i> Memuat...</div>');
    $('#form_jml_harus_dibayar').html('<div class="text-right mt-1"><i class="fas fa-spin fa-spinner"></i> Memuat...</div>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax/rincian_pembayaran' ?>', {
      id: id,
      groupreg_in: groupreg_in
    }, function(data) {

      $(document).ready(function() {
        $('#grand_total_potongan').bind('keyup', function() {
          var grand_total_bruto = ($('#grand_total_bruto').val() != '' ? num_sys($('#grand_total_bruto').val()) : '0');
          var grand_total_potongan = ($(this).val() != '' ? num_sys($(this).val()) : '0');
          var res_sum = grand_total_bruto - grand_total_potongan;
          $("#grand_total_netto").autoNumericSet(res_sum);
          $("#grand_total_tagihan").autoNumericSet(res_sum);
        });

        $(".autonumeric").autoNumeric({
          aSep: ".",
          aDec: ",",
          vMax: "999999999999999",
          vMin: "0",
        });
      })

      $('#rincian_pembayaran').html(data.html);
      // Resume Pembayaran
      var total_seharusnya = $('#total_seharusnya').val();
      var total_potongan = $('#total_potongan').val();
      var total_tagihan = $('#total_tagihan').val();
      var total_jumlah = $('#total_jumlah').val();
      $('#grand_jml_tindakan').val(total_seharusnya);
      // Form Label Total Biaya
      $('#form_grand_total_bruto').html('<input type="text" class="form-control text-right" name="grand_total_bruto" id="grand_total_bruto" value="' + total_seharusnya + '">');

      // Form Label Total Potongan
      <?php if (@$main['bayar_st'] == '1' || @$main['bayar_st'] == '2') : ?>
        $('#form_total_potongan').html('<input type="text" class="form-control autonumeric text-right" name="grand_total_potongan" id="grand_total_potongan" value="<?= num_id(@$main['grand_total_potongan']) ?>">');
      <?php else : ?>
        <?php if (@$main['jenisreg_st'] == 1) : ?>
          // Pasien Rawata Jalan
          <?php if (@$main['penjamin_cd'] == 'AS' && @$main['penjamin_nm'] == 'BPJS') : ?>
            // Jika Pasien BPJS
            $('#form_total_potongan').html('<input type="text" class="form-control autonumeric text-right" name="grand_total_potongan" id="grand_total_potongan" value="' + total_seharusnya + '">');
          <?php else : ?>
            $('#form_total_potongan').html('<input type="text" class="form-control autonumeric text-right" name="grand_total_potongan" id="grand_total_potongan" value="' + total_potongan + '">');
          <?php endif; ?>
        <?php elseif (@$main['is_ranap'] == 1) : ?>
          // Pasien Rawat Inap
          <?php if (@$main['penjamin_cd'] == 'AS' && @$main['penjamin_nm'] == 'BPJS' && @$main['status_ranap_cd'] == 'S') : ?>
            // Jika Pasien BPJS & Status Ranap Sesuai
            $('#form_total_potongan').html('<input type="text" class="form-control autonumeric text-right" name="grand_total_potongan" id="grand_total_potongan" value="' + total_seharusnya + '">');
          <?php else : ?>
            $('#form_total_potongan').html('<input type="text" class="form-control autonumeric text-right" name="grand_total_potongan" id="grand_total_potongan" value="' + total_potongan + '">');
          <?php endif; ?>
        <?php else : ?>
          $('#form_total_potongan').html('<input type="text" class="form-control autonumeric text-right" name="grand_total_potongan" id="grand_total_potongan" value="' + total_potongan + '">');
        <?php endif; ?>
      <?php endif; ?>

      // Form Label Total Akhir

      <?php if (@$main['jenisreg_st'] == 1) : ?>
        <?php if (@$main['penjamin_cd'] == 'AS' && @$main['penjamin_nm'] == 'BPJS') : ?>
          var grand_total_netto = 0;
        <?php else : ?>
          var grand_total_netto = total_jumlah;
        <?php endif; ?>
      <?php elseif (@$main['is_ranap'] == 1) : ?>
        <?php if (@$main['penjamin_cd'] == 'AS' && @$main['penjamin_nm'] == 'BPJS' && @$main['status_ranap_cd'] == 'S') : ?>
          <?php if (@$main['status_ranap_cd'] == 'NK') : ?>
            var grand_total_netto = total_jumlah;
          <?php else : ?>
            var grand_total_netto = 0;
          <?php endif; ?>
        <?php else : ?>
          var grand_total_netto = total_jumlah;
        <?php endif; ?>
      <?php else : ?>
        var grand_total_netto = total_jumlah;
      <?php endif; ?>

      <?php if (@$main['bayar_st'] == '1' || @$main['bayar_st'] == '2') : ?>
        $('#form_total_akhir').html('<input type="text" class="form-control autonumeric text-right" name="grand_total_netto" id="grand_total_netto" value="<?= num_id(@$main['grand_total_netto']) ?>">');
      <?php else : ?>
        $('#form_total_akhir').html('<input type="text" class="form-control autonumeric text-right" name="grand_total_netto" id="grand_total_netto" value="' + grand_total_netto + '">');
      <?php endif; ?>

      // Form Label Jumlah Yang Harus Dibayar
      <?php if (@$main['billing_id'] != '') : ?>
        $('#form_jml_harus_dibayar').html('<input type="text" class="form-control form-control-lg font-weight-bold mb-2 text-success autonumeric text-right" style="font-size:14px;" name="grand_total_tagihan" id="grand_total_tagihan" value="<?= num_id(@$main['grand_total_tagihan']) ?>">');
      <?php else : ?>
        $('#form_jml_harus_dibayar').html('<input type="text" class="form-control form-control-lg font-weight-bold mb-2 text-success autonumeric text-right" style="font-size:14px;" name="grand_total_tagihan" id="grand_total_tagihan" value="' + grand_total_netto + '">');
      <?php endif; ?>
    }, 'json');
  }

  function rincian_pembayaran_parsial(id = '', pasien_id = '') {
    $('#rincian_pembayaran_parsial').html('<tr><td class="text-center" colspan="99"><div class="mt-3 mb-2"><i class="fas fa-spin fa-spinner fa-2x"></i><div style="font-size: 14px;" class="mt-1">Memuat...</div></div></td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax/rincian_pembayaran_parsial' ?>', {
      id: id,
      pasien_id: pasien_id
    }, function(data) {
      $('#rincian_pembayaran_parsial').html(data.html);
    }, 'json');
  }
</script>