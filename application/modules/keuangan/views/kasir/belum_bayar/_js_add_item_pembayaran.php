<script type="text/javascript">
  $(document).ready(function() {
    $("#form-data-add-item-pembayaran").validate({
      rules: {
        tarifkelas_id: {
          valueNotEquals: ""
        },
        petugas_id: {
          valueNotEquals: ""
        },
        qty: {
          valueNotEquals: ""
        }
      },
      messages: {
        tarifkelas_id: "Pilih salah satu!",
        petugas_id: "Pilih salah satu!",
        qty: "Kosong!"
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('autocomplete')) {
          error.insertAfter(element.next(".select2-container"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $("#tindakan_action").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#tindakan_action").attr("disabled", "disabled");
        $("#tindakan_cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    // autocomplete
    $('#tarifkelas_id').select2({
      minimumInputLength: 2,
      ajax: {
        url: "<?= site_url($nav['nav_url']) ?>/ajax_tindakan/tarifkelas_autocomplete",
        dataType: "json",

        data: function(params) {
          return {
            tarif_nm: params.term
          };
        },
        processResults: function(data) {
          return {
            results: data
          }
        }
      }
    });

    // petugas
    $('#add_petugas').bind('click', function(e) {
      e.preventDefault();
      var no = $('#petugas_no').val();
      add_petugas(no);
    });
    //

    $("#tindakan_cancel").on('click', function() {
      tindakan_reset('cancel');
    });

    $('.modal-href-tindakan').click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");

      $("#modal-size-2")
        .removeClass("modal-lg")
        .removeClass("modal-md")
        .removeClass("modal-sm");

      $("#modal-title-2").html(modal_title);
      $("#modal-size-2").addClass('modal-' + modal_size);
      if (modal_custom_size) {
        $("#modal-size-2").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
      }
      $("#myModal-2").modal('show');

      $.ajax({
        type: 'post',
        url: $(this).data('href'),
        dataType: 'json',
        success: function(data) {
          $("#modal-body-2").html(data.html);
        }
      })

      // $.post($(this).data('href'), function(data) {
      //   $("#modal-body-2").html(data.html);
      // }, 'json');
    });

    $('.datetimepicker').daterangepicker({
      startDate: moment("<?= date('Y-m-d H:i:s') ?>"),
      endDate: moment(),
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })

  })

  function tindakan_reset(type = '') {
    $("#tindakan_id").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#tarifkelas_id").val('').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#box_petugas").html('');
    $("#qty").val('1').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#jenistindakan_cd").val('1').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $('#tindakan_action').html('<i class="fas fa-save"></i> Simpan');
    $('#add_petugas').removeClass('disabled');
    if (type == 'cancel') {
      add_petugas('0');
    }

    $('.datetimepicker').daterangepicker({
      startDate: moment("<?= date('Y-m-d H:i:s') ?>"),
      endDate: moment(),
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })
  }

  function tarifkelas_fill(id) {
    $.ajax({
      type: 'post',
      url: '<?= site_url($nav['nav_url'] . '/ajax_tindakan/tarifkelas_fill') ?>',
      async: false,
      dataType: 'json',
      data: 'tarifkelas_id=' + id,
      success: function(data) {
        // autocomplete
        var tarifKelasData = {
          id: data.tarif_id + '.' + data.kelas_id,
          text: data.tarif_id + '.' + data.kelas_id + ' - ' + data.tarif_nm + ' - ' + data.kelas_nm
        };
        var tarifKelasOpt = new Option(tarifKelasData.text, tarifKelasData.id, false, false);
        $('#tarifkelas_id').append(tarifKelasOpt).trigger('change');
        $('#tarifkelas_id').val(data.tarif_id + '.' + data.kelas_id);
        $('#tarifkelas_id').trigger('change');

        $('.select2-container').css('width', '100%');
        $("#myModal-2").modal('hide');
      }
    })
  }
</script>