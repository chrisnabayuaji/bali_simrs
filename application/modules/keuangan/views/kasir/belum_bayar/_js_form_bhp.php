<script type="text/javascript">
  var bhp_form;
  $(document).ready(function() {
    var bhp_form = $("#bhp_form").validate({
      rules: {
        obat_id: {
          valueNotEquals: ""
        },
        qty: {
          valueNotEquals: ""
        },
      },
      messages: {
        obat_id: {
          valueNotEquals: "Pilih salah satu!"
        },
        qty: {
          valueNotEquals: "Kosong!"
        },
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $("#bhp_action").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#bhp_action").attr("disabled", "disabled");
        $("#bhp_cancel").attr("disabled", "disabled");
        var pasien_id = $("#pasien_id").val();
        var resep_id = $("#resep_id").val();
        var reg_id = $("#reg_id").val();
        var lokasi_id = $("#lokasi_id").val();
        $.ajax({
          type: 'post',
          url: '<?= site_url($nav['nav_url']) ?>/ajax_bhp/save',
          data: $(form).serialize(),
          async: false,
          success: function(data) {
            bhp_reset();
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil disimpan.',
              icon: 'success',
              position: 'top-right'
            })
            $("#bhp_action").html('<i class="fas fa-save"></i> Simpan');
            $("#bhp_action").attr("disabled", false);
            $("#bhp_cancel").attr("disabled", false);
            location.reload();
          }
        })
        return false;
      }
    });

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    // autocomplete
    $('#barang_id_bhp').select2({
      minimumInputLength: 2,
      ajax: {
        url: "<?= site_url($nav['nav_url']) ?>/ajax_bhp/autocomplete",
        dataType: "json",

        data: function(params) {
          return {
            obat_nm: params.term,
            map_lokasi_depo: $('#map_lokasi_depo').val()
          };
        },
        processResults: function(data) {
          return {
            results: data
          }
        }
      }
    });
    $('.select2-container').css('width', '100%');

    var pasien_id = $("#pasien_id").val();
    var reg_id = $("#reg_id").val();
    var lokasi_id = $("#lokasi_id").val();

    $("#bhp_cancel").on('click', function() {
      bhp_reset();
    });

    $('.modal-href-daftar-bhp').click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");

      $("#modal-size-3")
        .removeClass("modal-lg")
        .removeClass("modal-md")
        .removeClass("modal-sm");

      $("#modal-title-3").html(modal_title);
      $("#modal-size-3").addClass('modal-' + modal_size);
      if (modal_custom_size) {
        $("#modal-size-3").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
      }
      $("#myModal-3").modal('show');

      $.ajax({
        type: 'post',
        url: $(this).data('href'),
        dataType: 'json',
        success: function(data) {
          $("#modal-body-3").html(data.html);
        }
      });
    });

    $(".datetimepicker").daterangepicker({
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: "Clear",
        format: "DD-MM-YYYY H:mm:ss",
      },
      isInvalidDate: function(date) {
        return "";
      },
    });

    <?php if (@$detail) : ?>
      var data2 = {
        id: '<?= $detail['barang_id'] ?>' + '#' + '<?= $detail['barang_nm'] ?>' + '#' + '<?= $detail['stokdepo_id'] ?>',
        text: '<?= $detail['barang_id'] ?>' + ' - ' + '<?= $detail['barang_nm'] ?>'
      };
      var newOption = new Option(data2.text, data2.id, false, false);
      $('#barang_id_bhp').append(newOption).trigger('change');
      $('#barang_id_bhp').val('<?= $detail['barang_id'] ?>' + '#' + '<?= $detail['barang_nm'] ?>' + '#' + '<?= $detail['stokdepo_id'] ?>', );
      $('#barang_id_bhp').trigger('change');
      $('.select2-container').css('width', '100%');
    <?php endif; ?>
  })

  function bhp_reset() {
    $("#bhp_id").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#barang_id_bhp").val('').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#stokdepo_id").val('').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#stokdepo_id_sebelum").val('').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#qty").val('1').removeClass("is-valid").removeClass("is-invalid");
    $("#qty_sebelum").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#keterangan_bhp").val('').removeClass("is-valid").removeClass("is-invalid");
    $('#bhp_action').html('<i class="fas fa-save"></i> Simpan');

    $('.datetimepicker').daterangepicker({
      startDate: moment("<?= date('Y-m-d H:i:s') ?>"),
      endDate: moment(),
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })
  }

  function bhp_fill(id, stokdepo_id = null) {
    $.ajax({
      type: 'post',
      url: '<?= site_url($nav['nav_url'] . '/ajax_bhp/bhp_fill') ?>',
      dataType: 'json',
      data: 'obat_id=' + id + '&stokdepo_id=' + stokdepo_id,
      success: function(data) {
        // autocomplete
        var data2 = {
          id: data.obat_id + '#' + data.obat_nm + '#' + data.stokdepo_id,
          text: data.obat_id + ' - ' + data.obat_nm
        };
        var newOption = new Option(data2.text, data2.id, false, false);
        $('#barang_id_bhp').append(newOption).trigger('change');
        $('#barang_id_bhp').val(data.obat_id + '#' + data.obat_nm + '#' + data.stokdepo_id);
        $('#barang_id_bhp').trigger('change');
        $('.select2-container').css('width', '100%');
        //update stokdepo_id
        $("#stokdepo_id").val(data.stokdepo_id);
        $("#myModal-3").modal('hide');
      }
    })
  }
</script>