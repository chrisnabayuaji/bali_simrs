<script type="text/javascript">
  var farmasi_form;
  $(document).ready(function() {
    var farmasi_form = $("#farmasi_form").validate({
      rules: {
        obat_id: {
          valueNotEquals: ""
        },
        qty: {
          valueNotEquals: ""
        },
      },
      messages: {
        obat_id: {
          valueNotEquals: "Pilih salah satu!"
        },
        qty: {
          valueNotEquals: "Kosong!"
        },
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $("#farmasi_action").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#farmasi_action").attr("disabled", "disabled");
        $("#farmasi_cancel").attr("disabled", "disabled");
        var pasien_id = $("#pasien_id").val();
        var resep_id = $("#resep_id").val();
        var reg_id = $("#reg_id").val();
        var lokasi_id = $("#lokasi_id").val();
        $.ajax({
          type: 'post',
          url: '<?= site_url($nav['nav_url']) ?>/ajax_farmasi/save',
          data: $(form).serialize(),
          async: false,
          success: function(data) {
            farmasi_reset();
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil disimpan.',
              icon: 'success',
              position: 'top-right'
            })
            $("#farmasi_action").html('<i class="fas fa-save"></i> Simpan');
            $("#farmasi_action").attr("disabled", false);
            $("#farmasi_cancel").attr("disabled", false);
            location.reload();
          }
        })
        get_grand();
        return false;
      }
    });

    $(".autonumeric").autoNumeric({
      aSep: ".",
      aDec: ",",
      vMax: "999999999999999",
      vMin: "0"
    });

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    // autocomplete
    $('#obat_id').select2({
      minimumInputLength: 2,
      ajax: {
        url: "<?= site_url($nav['nav_url']) ?>/ajax_farmasi/autocomplete",
        dataType: "json",
        data: function(params) {
          return {
            obat_nm: params.term,
            map_lokasi_depo: $('#map_lokasi_depo').val(),
          };
        },
        processResults: function(data) {
          return {
            results: data
          }
        }
      }
    });
    $('.select2-container').css('width', '100%');

    var reg_id = $("#reg_id").val();
    var lokasi_id = $("#lokasi_id").val();
    var resep_id = $("#resep_id").val();

    $('.modal-href-daftar-obat').click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");

      $("#modal-size-3")
        .removeClass("modal-lg")
        .removeClass("modal-md")
        .removeClass("modal-sm");

      $("#modal-title-3").html(modal_title);
      $("#modal-size-3").addClass('modal-' + modal_size);
      if (modal_custom_size) {
        $("#modal-size-3").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
      }
      $("#myModal-3").modal('show');

      $.ajax({
        type: 'post',
        url: $(this).data('href'),
        dataType: 'json',
        success: function(data) {
          $("#modal-body-3").html(data.html);
        }
      });
    });

    <?php if (@$detail) : ?>
      var data2 = {
        id: '<?= $detail['obat_id'] ?>' + '#' + '<?= $detail['obat_nm'] ?>' + '#' + '<?= $detail['stokdepo_id'] ?>',
        text: '<?= $detail['obat_id'] ?>' + ' - ' + '<?= $detail['obat_nm'] ?>'
      };
      var newOption = new Option(data2.text, data2.id, false, false);
      $('#obat_id').append(newOption).trigger('change');
      $('#obat_id').val('<?= $detail['obat_id'] ?>' + '#' + '<?= $detail['obat_nm'] ?>' + '#' + '<?= $detail['stokdepo_id'] ?>', );
      $('#obat_id').trigger('change');
      $('.select2-container').css('width', '100%');
    <?php endif; ?>
  })

  function obat_fill(obat_id, stokdepo_id) {
    $.ajax({
      type: 'post',
      url: '<?= site_url($nav['nav_url'] . '/ajax_farmasi/obat_fill') ?>',
      dataType: 'json',
      data: 'obat_id=' + obat_id + '&stokdepo_id=' + stokdepo_id,
      success: function(data) {
        // autocomplete
        var data2 = {
          id: data.obat_id + '#' + data.obat_nm + '#' + data.stokdepo_id,
          text: data.obat_id + '-' + data.obat_nm
        };
        var newOption = new Option(data2.text, data2.id, false, false);
        $('#obat_id').append(newOption).trigger('change');
        $('#obat_id').val(data.obat_id + '#' + data.obat_nm + '#' + data.stokdepo_id);
        $('#obat_id').trigger('change');

        $('#harga').val(num_id(data.harga_jual));

        $('.select2-container').css('width', '100%');
        $("#myModal-3").modal('hide');
      }
    })
  }

  function farmasi_data(resep_id) {
    $('#farmasi_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_farmasi/data' ?>', {
      resep_id: resep_id
    }, function(data) {
      $('#farmasi_data').html(data.html);
    }, 'json');
  }

  function hitung_total() {
    var harga = num_sys($("#harga").val());
    var qty = num_sys($("#qty").val());
    var jumlah_awal = harga * qty;
    $("#jumlah_awal").val(num_id(jumlah_awal));
    var potongan = num_sys($("#potongan").val());
    var jumlah_akhir = jumlah_awal - potongan;
    $("#jumlah_akhir").val(num_id(jumlah_akhir));
  }

  function get_obat(o) {
    var res = o.val().split("#");
    if (res.length == 3) {
      if (res[0] != '' && res[1] != '' && res[2] != '') {
        $.ajax({
          type: 'post',
          url: '<?= site_url($nav['nav_url']) ?>/ajax_farmasi/obat_get',
          data: 'obat_id=' + res[0] + '&obat_nm=' + res[1] + '&stokdepo_id=' + res[2],
          dataType: 'json',
          success: function(data) {
            $("#harga").val(num_id(data.harga_jual));
            $("#stok").val(num_id(data.stok_akhir));
            hitung_total();
          }
        });
      }
    }
  }

  function farmasi_reset() {
    $("#obat_id").val('').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#harga").val(0);
    $("#stok").val(0);
    $("#qty").val(0);
    $("#jumlah_awal").val(0);
    $("#potongan").val(0);
    $("#jumlah_akhir").val(0);
    $("#aturan_pakai").val('');
    $("#jadwal").val('');
    $("#aturan_tambahan").val('');
    $('#farmasi_action').html('<i class="fas fa-save"></i> Simpan');
  }
</script>