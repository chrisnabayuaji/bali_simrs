<script type="text/javascript">
  $(document).ready(function () {
    $('#lokasi_kasir').bind('change',function(e) {
      e.preventDefault();
      var i = $(this).val();
      _get_lokasi_pelayanan(i);
    })

    <?php if (@$cookie['search']['lokasi_kasir'] !='' && @$cookie['search']['lokasi_pelayanan'] !=''): ?>
      _get_lokasi_pelayanan('<?=@$cookie['search']['lokasi_kasir']?>', '<?=@$cookie['search']['lokasi_pelayanan']?>');
    <?php endif; ?>

    function _get_lokasi_pelayanan(i, j) {
      $.post('<?=site_url($nav['nav_url'].'/ajax/get_lokasi_pelayanan')?>', {lokasi_kasir: i, lokasi_pelayanan: j},function(data) {
        $('#box_lokasi_pelayanan').html(data.html);
      },'json');
    }
  })
</script>