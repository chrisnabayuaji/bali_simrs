<!-- js -->
<?php $this->load->view('_js_add_item_pembayaran') ?>
<!-- / -->
<form role="form" id="form-data-add-item-pembayaran" method="post" enctype="multipart/form-data" action="<?= $form_action ?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <input type="hidden" name="tindakan_id" id="tindakan_id">
      <input type="hidden" name="pasien_id" value="<?= @$main['pasien_id'] ?>">
      <input type="hidden" name="lokasi_id" value="<?= @$main['lokasi_id'] ?>">
      <input type="hidden" name="src_lokasi_id" value="<?= @$main['src_lokasi_id'] ?>">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Tanggal Catat <span class="text-danger">*</span></label>
        <div class="col-lg-5 col-md-6">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
            </div>
            <input type="text" class="form-control datetimepicker" name="tgl_catat" id="tgl_catat" required="" aria-invalid="false">
          </div>
        </div>
      </div>
      <div class="form-group row mb-2">
        <label class="col-lg-3 col-md-3 col-form-label">Tindakan <span class="text-danger">*</span></label>
        <div class="col-lg-8 col-md-3">
          <select class="form-control autocomplete" name="tarifkelas_id" id="tarifkelas_id">
            <option value="">- Pilih -</option>
          </select>
        </div>
        <div class="col-lg-1 col-md-1">
          <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_tindakan/search_tarifkelas/' . @$kelas_id ?>" modal-title="List Data Tindakan" modal-size="lg" class="btn btn-xs btn-default modal-href-tindakan" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Tindakan" data-original-title="Cari Tindakan"><i class="fas fa-search"></i></a>
        </div>
      </div>
      <div class="form-group row mb-2">
        <label class="col-lg-3 col-md-3 col-form-label">Qty <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="number" class="form-control" name="qty" id="qty" value="1" min="1" required>
        </div>
        <label class="col-lg-2 col-md-2 col-form-label">Jenis <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <select class="form-control chosen-select" name="jenistindakan_cd" id="jenistindakan_cd">
            <?php foreach (get_parameter('jenistindakan_cd') as $r) : ?>
              <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['jenistindakan_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" id="tindakan_cancel" class="btn btn-xs btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" id="tindakan_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>