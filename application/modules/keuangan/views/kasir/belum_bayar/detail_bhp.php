<div class="row">
  <div class="col-12">
    <div class="mb-2">
      <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/form_bhp/' . @$groupreg_id ?>" modal-title="Tambah BHP (Barang Habis Pakai)" modal-size="md" class="btn btn-primary btn-xs modal-href-bhp"><i class="fas fa-plus"></i> Tambah BHP</a>
    </div>
    <div>
      <table class="table table-bordered table-striped table-sm">
        <thead>
          <tr>
            <th class="text-center" width="20">No</th>
            <th class="text-center" width="80">Aksi</th>
            <th class="text-center" width="100">Kode</th>
            <th class="text-center">Nama BHP</th>
            <th class="text-center" width="100">Harga Jual</th>
            <th class="text-center" width="60">Qty</th>
            <th class="text-center" width="100">Seharusnya</th>
            <th class="text-center" width="100">Potongan</th>
            <th class="text-center" width="100">Tagihan</th>
            <th class="text-center" width="200">Keterangan</th>
          </tr>
        </thead>
        <tbody>
          <?php if (@$main == null) : ?>
            <tr>
              <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
            </tr>
          <?php else : ?>
            <?php $i = 1;
            $total_jml_awal = 0;
            $total_jml_potongan = 0;
            $total_jml_tagihan = 0;
            foreach ($main as $row) :
              $total_jml_awal += $row['jml_awal'];
              $total_jml_potongan += $row['jml_potongan'];
              $total_jml_tagihan += $row['jml_tagihan'];
            ?>
              <tr>
                <td class="text-center"><?= $i++ ?></td>
                <td class="text-center" width="80">
                  <div class="btn-group">
                    <button type="button" class="btn btn-primary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      Aksi
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item modal-href-bhp" href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/form_bhp/' . @$groupreg_id . '/' . $row['bhp_id'] ?>" modal-title="Edit BHP (Barang Habis Pakai)" modal-size="md"><i class="fas fa-pencil-alt"></i> Edit BHP</a>
                      <a class="dropdown-item btn-delete-bhp" href="javascript:void(0)" data-reg-id="<?= $groupreg_id ?>" data-bhp-id="<?= $row['bhp_id'] ?>" title="Hapus BHP"><i class="far fa-trash-alt"></i> Hapus BHP</a>
                    </div>
                  </div>
                </td>
                <td class="text-center"><?= $row['barang_id'] ?></td>
                <td class="text-left"><?= $row['barang_nm'] ?></td>
                <td class="text-right"><?= num_id($row['harga']) ?></td>
                <td class="text-center"><?= $row['qty'] ?></td>
                <td class="text-right"><?= num_id($row['jml_awal']) ?></td>
                <td class="text-right"><?= num_id($row['jml_potongan']) ?></td>
                <td class="text-right"><?= num_id($row['jml_tagihan']) ?></td>
                <td class="text-left"><?= $row['keterangan_bhp'] ?></td>
              </tr>
            <?php endforeach; ?>
            <tr>
              <th class="text-center" colspan="5">Total</th>
              <th class="text-right"><?= num_id($total_jml_awal) ?></th>
              <th class="text-right"><?= num_id($total_jml_potongan) ?></th>
              <th class="text-right"><?= num_id($total_jml_tagihan) ?></th>
            </tr>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="border-top border-2 mt-2 pt-2 pb-0">
  <div class="row">
    <div class="offset-lg-11 offset-md-11">
      <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Tutup</button>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('.modal-href-bhp').click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");

      $("#modal-size-2")
        .removeClass("modal-lg")
        .removeClass("modal-md")
        .removeClass("modal-sm");

      $("#modal-title-2").html(modal_title);
      $("#modal-size-2").addClass('modal-' + modal_size);
      if (modal_custom_size) {
        $("#modal-size-2").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
      }
      $("#myModal").addClass('blur-bg');
      $("#myModal-2").modal('show');

      $.ajax({
        type: 'post',
        url: $(this).data('href'),
        dataType: 'json',
        success: function(data) {
          $("#modal-body-2").html(data.html);
        }
      });
    });

    $('#myModal-2').on('hidden.bs.modal', function() {
      $("#myModal").removeClass('blur-bg');
    })

    $('.btn-delete-bhp').on('click', function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Apakah Anda yakin?',
        text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#eb3b5a',
        cancelButtonColor: '#b2bec3',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        customClass: 'swal-wide'
      }).then((result) => {
        if (result.value) {
          var bhp_id = $(this).attr("data-bhp-id");
          var reg_id = $(this).attr("data-reg-id");

          $('#farmasi_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
          $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_bhp/delete_data' ?>', {
            bhp_id: bhp_id,
            reg_id: reg_id
          }, function(data) {
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil dihapus.',
              icon: 'success',
              position: 'top-right'
            })
            location.reload();
          }, 'json');
        }
      })
    });
  })
</script>