<div class="row">
  <div class="col-12">
    <div class="table-responsive">
      <table class="table table-hover table-bordered table-fixed">
        <thead>
          <tr>
            <th class="text-center" width="30">No</th>
            <th class="text-center" width="150">Tanggal</th>
            <th class="text-center">Nama Tindakan</th>
            <th class="text-center" width="70">Qty</th>
            <th class="text-center" width="120">Biaya</th>
            <th class="text-center">Petugas</th>
          </tr>
        </thead>
        <?php if(count($main) == 0):?>
          <tbody style="height: 100% !important;">
            <tr>
              <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
            </tr>
          </tbody>
        <?php else:?>
          <tbody style="height: 100% !important;">
            <?php $i=1; foreach($main as $row): ?>
            <tr>
              <td class="text-center" width="30"><?=$i++?></td>
              <td class="text-center" width="150"><?=to_date($row['tgl_catat'], '', 'full_date')?></td>
              <td class="text-left"><?=$row['tarif_nm']?></td>
              <td class="text-center" width="70"><?=$row['qty']?></td>
              <td class="text-right" width="120"><?=num_id($row['jml_tagihan'])?></td>
              <td class="text-left">
                <?=($row['pegawai_nm_1'] !='') ? $row['pegawai_nm_1'].',' : ''?>
                <?=($row['pegawai_nm_2'] !='') ? $row['pegawai_nm_2'].',' : ''?>
                <?=($row['pegawai_nm_3'] !='') ? $row['pegawai_nm_3'].',' : ''?>
                <?=($row['pegawai_nm_4'] !='') ? $row['pegawai_nm_4'].',' : ''?>
                <?=($row['pegawai_nm_5'] !='') ? $row['pegawai_nm_5'].'' : ''?>
              </td>
            </tr>
            <?php endforeach; ?>
          </tbody>
        <?php endif; ?>
      </table>
    </div>
  </div>
</div>
<div class="border-top border-2 mt-2 pt-2 pb-0">
  <div class="row">
    <div class="offset-lg-11 offset-md-11">
      <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Tutup</button>
    </div>
  </div>
</div>