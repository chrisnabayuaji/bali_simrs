<div class="row">
  <div class="col-12">
    <div class="mb-2">
      <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/form_obat/' . @$groupreg_id . '/' . @$last_resep['resep_id'] ?>" modal-title="Tambah Obat" modal-size="md" class="btn btn-primary btn-xs modal-href-obat"><i class="fas fa-plus"></i> Tambah Obat</a>
    </div>
    <div class="table-responsive">
      <table class="table table-bordered table-sm">
        <tbody>
          <?php if (@$detail_obat == null) : ?>
            <tr>
              <th class="text-center" width="20">No</th>
              <th class="text-center" width="80">Aksi</th>
              <th class="text-center text-middle">Nama Obat</th>
              <th class="text-center text-middle" width="80">Aturan Pakai</th>
              <th class="text-center text-middle" width="80">Jam Minum</th>
              <th class="text-center text-middle" width="80">Aturan Tambahan</th>
              <th class="text-center text-middle" width="80">Harga</th>
              <th class="text-center text-middle" width="50">Qty</th>
              <th class="text-center text-middle" width="80">Seharusnya</th>
              <th class="text-center text-middle" width="80">Potongan</th>
              <th class="text-center text-middle" width="80">Tagihan</th>
            </tr>
            <tr>
              <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
            </tr>
          <?php else : ?>
            <?php foreach ($detail_obat as $row) : ?>
              <tr bgcolor="#f5f5f5">
                <td class="text-left" colspan="99"><b>NO.RESEP : <?= $row['resep_id'] ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; TANGGAL : <?= to_date($row['tgl_catat'], '', 'date') ?></b></td>
              </tr>
              <tr>
                <th class="text-center" width="20">No</th>
                <th class="text-center" width="80">Aksi</th>
                <th class="text-center text-middle">Nama Obat</th>
                <th class="text-center text-middle" width="80">Aturan Pakai</th>
                <th class="text-center text-middle" width="80">Jam Minum</th>
                <th class="text-center text-middle" width="80">Aturan Tambahan</th>
                <th class="text-center text-middle" width="80">Harga</th>
                <th class="text-center text-middle" width="50">Qty</th>
                <th class="text-center text-middle" width="80">Seharusnya</th>
                <th class="text-center text-middle" width="80">Potongan</th>
                <th class="text-center text-middle" width="80">Tagihan</th>
              </tr>
              <?php $i = 1;
              foreach ($row['list_dat_resep'] as $dat_resep) : ?>
                <tr>
                  <td class="text-center" width="20"><?= $i++ ?></td>
                  <td class="text-center" width="80">
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Aksi
                      </button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item modal-href-obat" href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/form_obat/' . @$groupreg_id . '/' . $dat_resep['resep_id'] . '/' . $dat_resep['farmasi_id'] ?>" modal-title="Edit Obat" modal-size="md"><i class="fas fa-pencil-alt"></i> Edit Obat</a>
                        <a class="dropdown-item btn-delete-farmasi" href="javascript:void(0)" data-resep-id="<?= $dat_resep['resep_id'] ?>" data-reg-id="<?= $dat_resep['reg_id'] ?>" data-lokasi-id="<?= $dat_resep['lokasi_id'] ?>" data-farmasi-id="<?= $dat_resep['farmasi_id'] ?>" title="Hapus Data"><i class="far fa-trash-alt"></i> Hapus Obat</a>
                      </div>
                    </div>
                  </td>
                  <td class="text-left"><?= $dat_resep['obat_nm'] ?></td>
                  <td class="text-left"><?= $dat_resep['aturan_pakai'] ?></td>
                  <td class="text-left"><?= $dat_resep['jadwal'] ?></td>
                  <td class="text-left"><?= $dat_resep['aturan_tambahan'] ?></td>
                  <td class="text-right"><?= num_id($dat_resep['harga']) ?></td>
                  <td class="text-center"><?= num_id($dat_resep['qty']) ?></td>
                  <td class="text-right"><?= num_id($dat_resep['jumlah_awal']) ?></td>
                  <td class="text-right"><?= num_id($dat_resep['potongan']) ?></td>
                  <td class="text-right">
                    <b><?= num_id($dat_resep['jumlah_akhir']) ?></b>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php endforeach; ?>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="col-8 mt-3"></div>
  <div class="col-4 mt-3">
    <?php if (@$detail_obat != null) : ?>
      <div class="table-responsive">
        <table class="table table-sm">
          <tbody>
            <tr>
              <th class="text-right">Total Bruto</th>
              <th class="text-center">:</th>
              <td class="text-right" width="100"><?= @num_id($main['grand_jml_bruto']) ?></td>
            </tr>
            <tr>
              <th class="text-right">Total PPN</th>
              <th class="text-center">:</th>
              <td class="text-right" width="100"><?= @num_id($main['grand_prs_ppn']) ?>% (<?= @num_id($main['grand_nom_ppn']) ?>)</td>
            </tr>
            <tr>
              <th class="text-right">Total Embalace</th>
              <th class="text-center">:</th>
              <td class="text-right" width="100"><?= @num_id($main['grand_embalace']) ?></td>
            </tr>
            <tr>
              <th class="text-right">Total Potongan Akhir</th>
              <th class="text-center">:</th>
              <td class="text-right" width="100"><?= @num_id($main['grand_jml_potongan']) ?></td>
            </tr>
            <tr>
              <th class="text-right">Total Pembulatan</th>
              <th class="text-center">:</th>
              <td class="text-right" width="100"><?= @num_id($main['grand_pembulatan']) ?></td>
            </tr>
            <tr>
              <th class="text-right text-top">
                <div>Jumlah Seharusnya</div>
                <div style="font-size: 10px; margin-top: 3px;">(Sebelum Total Potongan Akhir)</div>
              </th>
              <th class="text-center text-top">:</th>
              <td class="text-top text-right" width="100"><?= @num_id($main['grand_jml_tagihan'] + @$main['grand_jml_potongan']) ?></td>
            </tr>
            <tr>
              <th class="text-right">Total Tagihan</th>
              <th class="text-center">:</th>
              <th class="text-right" width="100"><?= @num_id($main['grand_jml_tagihan']) ?></th>
            </tr>
          </tbody>
        </table>
      </div>
    <?php endif; ?>
  </div>
</div>
<div class="border-top border-2 mt-2 pt-2 pb-0">
  <div class="row">
    <div class="offset-lg-11 offset-md-11">
      <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Tutup</button>
    </div>
  </div>
</div>

<script>
  $(document).ready(function() {
    $('.modal-href-obat').click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");

      $("#modal-size-2")
        .removeClass("modal-lg")
        .removeClass("modal-md")
        .removeClass("modal-sm");

      $("#modal-title-2").html(modal_title);
      $("#modal-size-2").addClass('modal-' + modal_size);
      if (modal_custom_size) {
        $("#modal-size-2").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
      }
      $("#myModal").addClass('blur-bg');
      $("#myModal-2").modal('show');

      $.ajax({
        type: 'post',
        url: $(this).data('href'),
        dataType: 'json',
        success: function(data) {
          $("#modal-body-2").html(data.html);
        }
      });
    });

    $('#myModal-2').on('hidden.bs.modal', function() {
      $("#myModal").removeClass('blur-bg');
    })

    $('.btn-delete-farmasi').on('click', function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Apakah Anda yakin?',
        text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#eb3b5a',
        cancelButtonColor: '#b2bec3',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        customClass: 'swal-wide'
      }).then((result) => {
        if (result.value) {
          var farmasi_id = $(this).attr("data-farmasi-id");
          var resep_id = $(this).attr("data-resep-id");

          $('#farmasi_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
          $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_farmasi/delete_data' ?>', {
            farmasi_id: farmasi_id,
            resep_id: resep_id
          }, function(data) {
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil dihapus.',
              icon: 'success',
              position: 'top-right'
            })
            location.reload();
            get_grand();
          }, 'json');
        }
      })
    });
  })
</script>