<?php $this->load->view('_js'); ?>
<div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
<form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?= $form_action ?>" class="needs-validation" novalidate autocomplete="off">
  <input type="hidden" name="pasien_id" value="<?= @$main['pasien_id'] ?>">
  <input type="hidden" name="pasien_nm" value="<?= @$main['pasien_nm'] ?>">
  <input type="hidden" name="lokasi_id" value="<?= @$main['lokasi_id'] ?>">
  <input type="hidden" name="jenispasien_id" value="<?= @$main['jenispasien_id'] ?>">
  <input type="hidden" name="tgl_masuk" value="<?= @$main['tgl_registrasi'] ?>">
  <input type="hidden" name="tgl_keluar" value="<?= @$main['tgl_pulang'] ?>">
  <input type="hidden" id="map_lokasi_depo" value="<?= @$main['map_lokasi_depo'] ?>">
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline col-title">
          <div class="col-back">
            <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
          </div>
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            <?= $nav['nav_nm'] ?>
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-warehouse"></i> Gudang</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item active"><span>Form</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>

    <div class="row full-page mt-4">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <h4 class="card-title border-bottom border-2 pb-2 mb-3"><?= $nav['nav_nm'] ?></h4>
            <div class="row mt-n1">
              <?php if (@$main['is_ranap'] == 0) : ?>
                <div class="col-md-6">
                  <table class="table" style="width: 100% !important; border-bottom: 1px solid #f2f2f2;">
                    <tbody>
                      <tr>
                        <td class="text-middle" width="150" style="font-weight: 500 !important;">No.Reg</td>
                        <td class="text-middle" width="20">:</td>
                        <td class="text-middle"><?= @$main['reg_id'] ?></td>
                        <td>
                          <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax/list_belum_bayar' ?>" modal-title="Data Pasien Belum Bayar" modal-size="lg" class="btn btn-xs btn-default modal-href float-right" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Pasien Belum Bayar" data-original-title="Cari Obat"><i class="fas fa-search"></i> Browse</a>
                        </td>
                      </tr>
                      <tr>
                        <td width="150" style="font-weight: 500 !important;">NIK</td>
                        <td width="20">:</td>
                        <td colspan="2"><?= @$main['nik'] ?></td>
                      </tr>
                      <tr>
                        <td width="150" style="font-weight: 500 !important;">No.RM / Nama Pasien</td>
                        <td width="20">:</td>
                        <td colspan="2"><?= @$main['pasien_id'] ?> / <?= @$main['pasien_nm'] ?><?= @$main['sebutan_cd'] ?></td>
                      </tr>
                      <tr>
                        <td width="150">Alamat Pasien</td>
                        <td width="20">:</td>
                        <td colspan="2">
                          <?= (@$main['alamat'] != '') ? @$main['alamat'] . ', ' : '' ?>
                          <?= (@$main['kelurahan'] != '') ? ucwords(strtolower(@$main['kelurahan'])) . ', ' : '' ?>
                          <?= (@$main['kecamatan'] != '') ? ucwords(strtolower(@$main['kecamatan'])) . ', ' : '' ?>
                          <?= (@$main['kabupaten'] != '') ? ucwords(strtolower(@$main['kabupaten'])) . ', ' : '' ?>
                          <?= (@$main['provinsi'] != '') ? ucwords(strtolower(@$main['provinsi'])) : '' ?>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="col-md-6">
                  <table class="table" style="width: 100% !important; border-bottom: 1px solid #f2f2f2;">
                    <tbody>
                      <tr>
                        <td width="150">DPJP / Spesialisasi</td>
                        <td width="20">:</td>
                        <td><?= @$main['pegawai_nm'] ?> / <?= @$main['spesialisasi_nm'] ?></td>
                      </tr>
                      <tr>
                        <td width="150">Umur / JK</td>
                        <td width="20">:</td>
                        <td><?= @$main['umur_thn'] ?> Th <?= @$main['umur_bln'] ?> Bl <?= @$main['umur_hr'] ?> Hr / <?= get_parameter_value('sex_cd', @$main['sex_cd']) ?></td>
                      </tr>
                      <tr>
                        <td width="150">Jenis / Asal Pasien</td>
                        <td width="20">:</td>
                        <td><?= @$main['jenispasien_nm'] ?> / <?= get_parameter_value('asalpasien_cd', @$main['asalpasien_cd']) ?></td>
                      </tr>
                      <tr>
                        <td width="150">Tgl.Registrasi</td>
                        <td width="20">:</td>
                        <td><?= to_date(@$main['tgl_registrasi'], '', 'full_date') ?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              <?php else : ?>
                <div class="col-md-6">
                  <table class="table" style="width: 100% !important; border-bottom: 1px solid #f2f2f2;">
                    <tbody>
                      <tr>
                        <td class="text-middle" width="150" style="font-weight: 500 !important;">No. RM</td>
                        <td class="text-middle" width="20">:</td>
                        <td class="text-middle"><?= @$main['pasien_id'] ?></td>
                      </tr>
                      <tr>
                        <td width="150" style="font-weight: 500 !important;">Tanggal Masuk</td>
                        <td width="20">:</td>
                        <td><?= strtoupper(to_date_indo(@$main['tgl_registrasi'], 'date')) ?></td>
                      </tr>
                      <tr>
                        <td width="150" style="font-weight: 500 !important;">Tanggal Keluar</td>
                        <td width="20">:</td>
                        <td><?= strtoupper(to_date_indo(@$main['tgl_pulang'], 'date')) ?></td>
                      </tr>
                      <tr>
                        <td width="150" style="font-weight: 500 !important;">Nama Pasien</td>
                        <td width="20">:</td>
                        <td><?= @$main['pasien_nm'] ?><?= @$main['sebutan_cd'] ?></td>
                      </tr>
                      <tr>
                        <td width="150">Umur</td>
                        <td width="20">:</td>
                        <td><?= @$main['umur_thn'] ?> TH</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div class="col-md-6">
                  <table class="table" style="width: 100% !important; border-bottom: 1px solid #f2f2f2;">
                    <tbody>
                      <tr>
                        <td width="150">Kelas / Ruang</td>
                        <td width="20">:</td>
                        <td><?= @$main['kelas_nm'] ?> / <?= @$main['lokasi_nm'] ?> <?= (@$main['status_ranap_cd'] == 'NK') ? '<span class="text-danger font-weight-bold ml-1 blink">NAIK KELAS</span>' : '' ?></td>
                      </tr>
                      <tr>
                        <td width="150">DPJP</td>
                        <td width="20">:</td>
                        <td><?= @$main['pegawai_nm'] ?></td>
                      </tr>
                      <tr>
                        <td width="150">Spesialisasi</td>
                        <td width="20">:</td>
                        <td><?= @$main['spesialisasi_nm'] ?></td>
                      </tr>
                      <tr>
                        <td width="150">Penjamin Bayar</td>
                        <td width="20">:</td>
                        <td>
                          <?php if (@$main['jenispasien_id'] == '01') : ?>
                            <?= @$main['penjamin_nm'] ?>
                          <?php elseif ($main['jenispasien_id'] == '02') : ?>
                            BPJS KESEHATAN
                          <?php endif; ?>
                        </td>
                      </tr>
                      <?php if (@$main['jenispasien_id'] == '02') : ?>
                        <tr>
                          <td width="150">NO. SEP</td>
                          <td width="20">:</td>
                          <td><?= @$main['sep_no'] ?></td>
                        </tr>
                      <?php endif; ?>
                    </tbody>
                  </table>
                </div>
              <?php endif; ?>
            </div>
            <div class="row mt-2 mb-n3">
              <?php if (@$main['is_ranap'] == 0) : ?>
                <div class="col-md-4">
                  <div class="form-group row">
                    <label class="col-lg-3 col-md-3 col-form-label">Tgl.Transaksi <span class="text-danger">*<span></label>
                    <div class="col-lg-7 col-md-4">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                        </div>
                        <input type="text" class="form-control datetimepicker" name="tgl_transaksi" id="tgl_transaksi" value="<?php if (@$main['tgl_transaksi']) {
                                                                                                                                echo to_date(@$main['tgl_transaksi'], '-', 'full_date');
                                                                                                                              } else {
                                                                                                                                echo date('d-m-Y H:i:s');
                                                                                                                              } ?>" required aria-invalid="false">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group row">
                    <label class="col-lg-3 col-md-3 col-form-label">ID.Billing <span class="text-danger">*<span></label>
                    <div class="col-lg-4 col-md-4">
                      <input type="text" class="form-control" name="billing_id" id="billing_id" value="<?= (@$main['billing_id'] != '') ? @$main['billing_id'] : 'otomatis' ?>" required readonly="">
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group row">
                    <label class="col-lg-3 col-md-3 col-form-label">No. Form <span class="text-danger">*<span></label>
                    <div class="col-lg-8 col-md-4">
                      <input type="text" class="form-control" name="no_invoice" id="no_invoice" value="<?= $no_invoice ?>" required>
                    </div>
                  </div>
                </div>
              <?php else : ?>
                <div class="col-md-3">
                  <div class="form-group row">
                    <label class="col-lg-4 col-md-4 col-form-label">Tgl.Transaksi <span class="text-danger">*<span></label>
                    <div class="col-lg-8 col-md-4">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                        </div>
                        <input type="text" class="form-control datetimepicker" name="tgl_transaksi" id="tgl_transaksi" value="<?php if (@$main['tgl_transaksi']) {
                                                                                                                                echo to_date(@$main['tgl_transaksi'], '-', 'full_date');
                                                                                                                              } else {
                                                                                                                                echo date('d-m-Y H:i:s');
                                                                                                                              } ?>" required aria-invalid="false">
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group row">
                    <label class="col-lg-4 col-md-4 col-form-label">ID.Billing <span class="text-danger">*<span></label>
                    <div class="col-lg-5 col-md-5">
                      <input type="text" class="form-control" name="billing_id" id="billing_id" value="<?= (@$main['billing_id'] != '') ? @$main['billing_id'] : 'otomatis' ?>" required readonly="">
                    </div>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group row">
                    <label class="col-lg-4 col-md-4 col-form-label">No. Form <span class="text-danger">*<span></label>
                    <div class="col-lg-8 col-md-4">
                      <input type="text" class="form-control" name="no_invoice" id="no_invoice" value="<?= $no_invoice ?>" required>
                    </div>
                  </div>
                </div>
                <?php if (@$main['jenispasien_id'] == '02') : ?>
                  <div class="col-md-3">
                    <div class="form-group row">
                      <label class="col-lg-4 col-md-4 col-form-label">No. SEP</label>
                      <div class="col-lg-8 col-md-4">
                        <input type="text" class="form-control" name="sep_no" id="sep_no" value="<?= @$main['sep_no'] ?>">
                      </div>
                    </div>
                  </div>
                <?php endif; ?>
              <?php endif; ?>
            </div>
            <hr>
            <div class="row">
              <div class="col-7">
                <div class="card border-none no-shadow">
                  <div class="card-body">
                    <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-file"></i> Rincian Pembayaran</h4>
                    <div class="table-responsive mt-n1">
                      <table class="table table-bordered table-fixed">
                        <thead>
                          <tr>
                            <th class="text-center text-middle" rowspan="2" width="30">NO</th>
                            <th class="text-center text-middle" rowspan="2" width="50">AKSI</th>
                            <th class="text-center text-middle" rowspan="2">JENIS ITEM PEMBAYARAN</th>
                            <th class="text-center" colspan="3" width="375">BIAYA</th>
                            <th class="text-center text-middle" rowspan="2" width="125">TOTAL</th>
                          </tr>
                          <tr>
                            <th class="text-center" width="125">SEHARUSNYA</th>
                            <th class="text-center" width="125">POTONGAN</th>
                            <th class="text-center" width="125">TAGIHAN</th>
                          </tr>
                        </thead>
                        <tbody style="height: 100% !important;" id="rincian_pembayaran">
                        </tbody>
                      </table>
                    </div>
                    <div class="row">
                      <div class="col-md-4 text-left">
                        <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/add_item_pembayaran/' . @$main['reg_id'] . '/' . @$main['kelas_id'] . '?groupreg_in=' . $groupreg_in ?>" modal-title="Tambah Item Pembayaran" modal-size="md" class="btn btn-xs btn-primary modal-href-add-item mt-2"><i class="fas fa-plus-circle"></i> Tambah Item Pembayaran</a>
                      </div>
                      <div class="col-md-8 text-right">
                        <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/detail_obat/' . @$main['reg_id'] . '?groupreg_in=' . $groupreg_in ?>" modal-title="Detail Biaya Obat" modal-size="lg" modal-content-top="-75px" class="btn btn-form-control btn-primary modal-href-custom mt-2"><i class="fas fa-pills"></i> Detail Biaya Obat</a>
                        <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/detail_alkes/' . @$main['reg_id'] . '?groupreg_in=' . $groupreg_in ?>" modal-title="Detail Biaya Alkes" modal-size="lg" modal-content-top="0px" class="btn btn-form-control btn-primary modal-href-custom mt-2"><i class="fas fa-syringe"></i></i> Detail Biaya Alkes</a>
                        <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/detail_bhp/' . @$main['reg_id'] . '?groupreg_in=' . $groupreg_in ?>" modal-title="Detail Biaya BHP" modal-size="lg" modal-content-top="0px" class="btn btn-form-control btn-primary modal-href-custom mt-2"><i class="fas fa-box-open"></i></i> Detail Biaya BHP</a>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="mt-3 mb-3">
                          <?php if (count(@$list_resep) == 0) : ?>
                            <div class="font-weight-bold">Resep : Tidak Ada Resep</div>
                          <?php else : ?>
                            <?php $no = 1;
                            foreach ($list_resep as $resep) :
                              if ($resep['resep_st'] == 2) {
                                $resep_st = '<span class="text-success">Selesai Diproses Farmasi</span>';
                              } elseif ($resep['resep_st'] == 1) {
                                $resep_st = '<span class="text-warning">Sedang Diproses Farmasi</span>';
                              } else {
                                $resep_st = '<span class="text-danger">Belum Diproses Farmasi</span>';
                              }
                            ?>
                              <div class="font-weight-bold"><?= $no++ ?>. Resep : <?= $resep['resep_id'] ?> | Status Resep : <?= @$resep_st ?></div>
                            <?php endforeach; ?>
                          <?php endif; ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-5">
                <div class="card border-none no-shadow">
                  <div class="card-body">
                    <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-file"></i> Resume Pembayaran</h4>
                    <input type="hidden" name="grand_jml_tindakan" id="grand_jml_tindakan" value="<?= @$total_seharusnya ?>">
                    <div class="form-group row mt-n2">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Total Biaya</label>
                      <div class="col-lg-4 col-md-4" id="form_grand_total_bruto">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Total Potongan</label>
                      <div class="col-lg-4 col-md-4" id="form_total_potongan">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Jenis Potongan</label>
                      <div class="col-lg-4 col-md-4">
                        <select class="form-control chosen-select" name="potongan_cd" id="potongan_cd">
                          <option value="">---</option>
                          <?php foreach (get_parameter('potongan_cd') as $r) : ?>
                            <?php if (@$main['bayar_st'] == 0) : ?>
                              <?php if (@$main['jenispasien_id'] == 02) : ?>
                                <?php if (@$main['is_ranap'] == 1 && @$main['status_ranap_cd'] == 'NK') : ?>
                                  <option value="<?= $r['parameter_cd'] ?>" <?= (04 == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                                <?php else : ?>
                                  <option value="<?= $r['parameter_cd'] ?>" <?= (01 == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                                <?php endif; ?>
                              <?php else : ?>
                                <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['potongan_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                              <?php endif; ?>
                            <?php else : ?>
                              <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['potongan_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                            <?php endif; ?>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row mr-2">
                      <div class="col-lg-9 col-md-9 text-right font-weight-bold">
                        ---------------------------------------------------------------- -
                      </div>
                    </div>
                    <div class="form-group row mt-1">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Total Akhir</label>
                      <div class="col-lg-4 col-md-4" id="form_total_akhir">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Pembulatan</label>
                      <div class="col-lg-4 col-md-4">
                        <input type="text" class="form-control autonumeric text-right" name="grand_pembulatan" value="<?= num_id(@$main['grand_pembulatan']) ?>" id="grand_pembulatan" value="<?= num_id(@$main['grand_pembulatan']) ?>">
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-lg-5 col-md-5 col-form-label text-left font-weight-bold">Jumlah Yang Harus Dibayar</label>
                      <div class="col-lg-4 col-md-4" id="form_jml_harus_dibayar">
                      </div>
                    </div>
                    <div class="border-bottom border-2 mb-2 mt-1"></div>
                    <div class="form-group row">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Status Bayar</label>
                      <div class="col-lg-5 col-md-5">
                        <select class="form-control form-control-sm chosen-select" name="bayar_st" id="bayar_st" required="">
                          <option value="1" <?php if (@$main['bayar_st'] == 1) {
                                              echo 'selected';
                                            } ?>>Lunas</option>
                          <option value="2" <?php if (@$main['bayar_st'] == 2) {
                                              echo 'selected';
                                            } ?>>Titip</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Tanggal Bayar</label>
                      <div class="col-lg-5 col-md-5">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datetimepicker" name="tgl_bayar" id="tgl_bayar" value="<?php if (@$main['tgl_bayar']) {
                                                                                                                          echo to_date(@$main['tgl_bayar'], '-', 'full_date');
                                                                                                                        } else {
                                                                                                                          echo date('d-m-Y H:i:s');
                                                                                                                        } ?>" required aria-invalid="false">
                        </div>
                      </div>
                    </div>
                    <div class="form-group row" id="box_lunas">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Jumlah Dibayar <span class="text-danger">*</span></label>
                      <div class="col-lg-4 col-md-4">
                        <?php if (@$main['bayar_st'] == '1') : ?>
                          <input type="text" class="form-control autonumeric text-right" name="grand_total_bayar" id="grand_total_bayar" value="<?= num_id(@$main['grand_total_bayar']) ?>" required="">
                        <?php else : ?>
                          <?php if (@$main['jenisreg_st'] == 1) : ?>
                            <!-- Pasien Rawat Jalan -->
                            <?php if (@$main['penjamin_cd'] == 'AS' && @$main['penjamin_nm'] == 'BPJS') : ?>
                              <!-- Jika Pasien BPJS -->
                              <input type="text" class="form-control autonumeric text-right" name="grand_total_bayar" id="grand_total_bayar" value="0" required="">
                            <?php else : ?>
                              <input type="text" class="form-control autonumeric text-right" name="grand_total_bayar" id="grand_total_bayar" required="">
                            <?php endif; ?>
                          <?php elseif (@$main['is_ranap'] == 1) : ?>
                            <!-- Pasien Rawat Inap -->
                            <?php if (@$main['penjamin_cd'] == 'AS' && @$main['penjamin_nm'] == 'BPJS' && @$main['status_ranap_cd'] == 'S') : ?>
                              <!-- Jika Pasien BPJS & Status Ranap Sesuai -->
                              <input type="text" class="form-control autonumeric text-right" name="grand_total_bayar" id="grand_total_bayar" value="0" required="">
                            <?php else : ?>
                              <input type="text" class="form-control autonumeric text-right" name="grand_total_bayar" id="grand_total_bayar" required="">
                            <?php endif; ?>
                          <?php else : ?>
                            <input type="text" class="form-control autonumeric text-right" name="grand_total_bayar" id="grand_total_bayar" required="">
                          <?php endif; ?>
                        <?php endif; ?>
                        <em class="error invalid-feedback mb-2" id="pembayaran_kurang">Pembayaran masih kurang !</em>
                      </div>
                    </div>
                    <div class="form-group row" id="box_kembalian">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Kembalian</label>
                      <div class="col-lg-4 col-md-4">
                        <?php if (@$main['billing_id'] != '') : ?>
                          <input type="text" class="form-control autonumeric text-right" id="kembalian" required readonly="" value="<?= num_id(@$main['grand_total_bayar'] - @$main['grand_total_tagihan']) ?>">
                        <?php else : ?>
                          <input type="text" class="form-control autonumeric text-right" id="kembalian" required readonly="">
                        <?php endif; ?>
                      </div>
                    </div>
                    <div class="form-group row <?= (@$main['bayar_st'] == 2) ? '' : 'd-none' ?>" id="box_titip">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Jumlah Titip</label>
                      <div class="col-lg-4 col-md-4">
                        <input type="text" class="form-control autonumeric text-right" name="grand_titip_bayar" value="<?= num_id(@$main['grand_titip_bayar']) ?>" required="">
                      </div>
                    </div>
                    <div class="form-group row d-none" id="box_pelunasan">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Jumlah Pelunasan</label>
                      <div class="col-lg-4 col-md-4">
                        <input type="text" class="form-control autonumeric text-right" name="jml_pelunasan" value="<?= num_id(@$main['grand_total_bayar']) ?>" required="">
                      </div>
                    </div>
                    <h4 class="card-title border-bottom border-2 pt-2 pb-2 mb-3"><i class="fas fa-file"></i> Catatan Pembayaran</h4>
                    <div class="form-group row mt-n1">
                      <div class="col-12">
                        <textarea class="form-control" name="keterangan_bayar" rows="4"><?= @$main['keterangan_bayar'] ?></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-7 mt-2">
                <div class="card border-none no-shadow">
                  <div class="card-body">
                    <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-copy"></i> Rincian Pembayaran Parsial</h4>
                    <div class="table-responsive mt-n1">
                      <table class="table table-bordered table-fixed">
                        <thead>
                          <tr>
                            <th class="text-center text-middle" width="30">NO</th>
                            <th class="text-center text-middle" width="100">AKSI</th>
                            <th class="text-center text-middle" width="70">NO. BILLING</th>
                            <th class="text-center text-middle" width="70">NO. SPLIT</th>
                            <th class="text-center text-middle" width="120">TGL.BAYAR</th>
                            <th class="text-center text-middle" width="100">TOTAL TAGIHAN</th>
                            <th class="text-center text-middle" width="100">TOTAL BAYAR</th>
                          </tr>
                        </thead>
                        <tbody style="height: 100% !important;" id="rincian_pembayaran_parsial">
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="btn-form">
    <div class="btn-form-action">
      <div class="btn-form-action-bottom w-100 small-text clearfix">
        <div class="col-lg-2">
          <a href="<?= site_url() . '/' . $nav['nav_url'] ?>" class="btn btn-xs btn-secondary"><i class="mdi mdi-keyboard-backspace"></i> Kembali</a>
        </div>
        <div class="col-lg-8 offset-2">
          <button type="submit" class="float-right btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
          <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/cetak_modal/cetak_obat/' . @$main['groupreg_id'] . '?groupreg_in=' . $groupreg_in ?>" modal-title="Cetak Rincian Obat" modal-size="lg" modal-content-top="-75px" class="float-right btn btn-xs btn-primary btn-submit mr-2 modal-href"><i class="fas fa-pills"></i> Cetak Rincian Obat</a>
          <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/cetak_modal/cetak_tindakan/' . @$main['groupreg_id'] . '?groupreg_in=' . $groupreg_in ?>" modal-title="Cetak Rincian Tindakan" modal-size="lg" modal-content-top="-75px" class="float-right btn btn-xs btn-primary btn-submit mr-2 modal-href"><i class="fas fa-procedures"></i> Cetak Rincian Tindakan</a>
          <?php if (@$main['bayar_st'] == '1' && (@$main['jenisreg_st'] == 1 || @$main['jenisreg_st'] == 3)) : ?>
            <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/cetak_modal/cetak_kwitansi/' . @$main['groupreg_id'] . '?groupreg_in=' . $groupreg_in ?>" modal-title="Cetak Kwitansi" modal-size="lg" modal-content-top="-75px" class="float-right btn btn-xs btn-primary btn-submit mr-2 modal-href"><i class="fas fa-print"></i> Cetak Kwitansi</a>
          <?php endif; ?>
          <?php if (@$main['is_ranap'] == 1) : ?>
            <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/cetak_modal/cetak_invoice/' . @$main['groupreg_id'] . '?groupreg_in=' . $groupreg_in ?>" modal-title="Cetak Invoice" modal-size="lg" modal-content-top="-75px" class="float-right btn btn-xs btn-primary btn-submit mr-2 modal-href"><i class="fas fa-print"></i> Cetak Invoice</a>
          <?php endif; ?>
          <button type="reset" onClick="window.location.reload();" class="float-right btn btn-xs btn-secondary btn-clear mr-2"><i class="fas fa-sync-alt"></i> Batal</button>
        </div>
      </div>
    </div>
  </div>
</form>