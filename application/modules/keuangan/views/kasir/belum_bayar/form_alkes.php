<?php $this->load->view('_js_form_alkes'); ?>
<div class="row">
  <div class="col-md-12">
    <form id="alkes_form" action="" method="post" autocomplete="off">
      <input type="hidden" name="alkes_id" id="alkes_id" value="<?= @$alkes_id ?>">
      <input type="hidden" name="reg_id" id="reg_id" value="<?= @$reg_id ?>">
      <input type="hidden" name="pasien_id" id="pasien_id" value="<?= @$reg['pasien_id'] ?>">
      <input type="hidden" name="lokasi_id" id="lokasi_id" value="<?= @$reg['lokasi_id'] ?>">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Tanggal Catat <span class="text-danger">*</span></label>
        <div class="col-lg-5 col-md-6">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
            </div>
            <input type="text" class="form-control datetimepicker" name="tgl_catat" id="tgl_catat" value="<?= (@$detail) ? to_date($detail['tgl_catat'], '-', 'full_date') : date("d-m-Y H:i:s") ?>" required="" aria-invalid="false">
          </div>
        </div>
      </div>
      <div class="form-group row mb-2">
        <label class="col-lg-3 col-md-3 col-form-label">Alkes <span class="text-danger">*</span></label>
        <div class="col-lg-8 col-md-3">
          <select class="form-control select2" name="barang_id" id="barang_id_alkes">
            <option value="">- Pilih -</option>
          </select>
          <input type="hidden" class="form-control" name="stokdepo_id" id="stokdepo_id">
          <input type="hidden" class="form-control" name="stokdepo_id_sebelum" id="stokdepo_id_sebelum">
        </div>
        <div class="col-lg-1 col-md-1">
          <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_alkes/search_alkes' ?>" modal-title="List Data Alkes" modal-size="lg" class="btn btn-xs btn-default modal-href-daftar-alkes" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Alkes" data-original-title="Cari Alkes"><i class="fas fa-search"></i></a>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Qty <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="number" class="form-control" name="qty" id="qty" value="<?= (@$detail) ? $detail['qty'] : 1 ?>" min="1">
          <input type="hidden" class="form-control" name="qty_sebelum" id="qty_sebelum">
        </div>
      </div>
      <div class="form-group row mb-2">
        <label class="col-lg-3 col-md-3 col-form-label">Keterangan</label>
        <div class="col-lg-9 col-md-3">
          <textarea class="form-control" name="keterangan_alkes" id="keterangan_alkes" value="" rows="5"><?= (@$detail) ? $detail['keterangan_alkes'] : '' ?></textarea>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-9 offset-lg-4 p-0">
          <button type="submit" id="alkes_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
          <button type="button" id="alkes_cancel" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        </div>
      </div>
    </form>
  </div>
</div>