<form role="form" id="form-split" method="post" enctype="multipart/form-data" action="<?= $form_action ?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <input type="hidden" name="billing_id" id="split_billing_id" value="<?= $billing_id ?>">
      <input type="hidden" name="tarif_id" id="split_tarif_id" value="<?= $tarif_id ?>">
      <input type="hidden" name="jml_seharusnya" id="split_jml_seharusnya" value="<?= $main['jml_seharusnya'] ?>">
      <input type="hidden" name="jml_potongan" id="split_jml_potongan" value="<?= $main['jml_potongan'] ?>">
      <input type="hidden" name="jml_tagihan" id="split_jml_tagihan" value="<?= $main['jml_tagihan'] ?>">
      <input type="hidden" name="data_tp" id="split_data_tp" value="<?= $main['data_tp'] ?>">
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Jenis Item Pembayaran <span class="text-danger">*</span></label>
        <div class="col-lg-8 col-md-3">
          <input type="text" class="form-control" name="tarif_nm" id="split_tarif_nm" value="<?= @$main['tarif_nm'] ?>" required="" readonly>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Total Biaya <span class="text-danger">*</span></label>
        <div class="col-lg-5 col-md-7">
          <input type="text" class="form-control" name="grand_total_bruto" id="split_grand_total_bruto" value="<?= @num_id($main['jml_tagihan']) ?>" required="" readonly dir="rtl">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Total Potongan <span class="text-danger">*</span></label>
        <div class="col-lg-5 col-md-7">
          <input type="text" class="form-control autonumeric" name="grand_total_potongan" id="split_grand_total_potongan" value="0" required="" dir="rtl" onkeyup="calc_split()">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Jenis Potongan <span class="text-danger">*</span></label>
        <div class="col-lg-5 col-md-7">
          <select class="form-control chosen-select" name="potongan_cd" id="split_potongan_cd" onchange="calc_split()">
            <option value="">---</option>
            <?php foreach (get_parameter('potongan_cd') as $r) : ?>
              <option value="<?= $r['parameter_cd'] ?>"><?= $r['parameter_val'] ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label"></label>
        <div class="col-lg-7 col-md-7">
          <b>--------------------------------------------- -</b>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Total Akhir <span class="text-danger">*</span></label>
        <div class="col-lg-5 col-md-7">
          <input type="text" class="form-control" name="grand_total_netto" id="split_grand_total_netto" value="<?= @num_id($main['jml_tagihan']) ?>" required="" dir="rtl" readonly>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Pembulatan <span class="text-danger">*</span></label>
        <div class="col-lg-5 col-md-7">
          <input type="text" class="form-control autonumeric" name="grand_pembulatan" id="split_grand_pembulatan" value="0" required="" dir="rtl" onkeyup="calc_split()">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label"></label>
        <div class="col-lg-7 col-md-7">
          <b>--------------------------------------------- +</b>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label"><b>Jml harus dibayar</b> <span class="text-danger">*</span></label>
        <div class="col-lg-5 col-md-7">
          <input type="text" class="form-control text-success" style="font-size:18px !important;font-weight:bold;" name="grand_total_tagihan" id="split_grand_total_tagihan" value="<?= @num_id($main['jml_tagihan']) ?>" required="" dir="rtl" readonly>
        </div>
      </div>
      <hr>
      <input type="hidden" name="bayar_st" id="split_bayar_st" value="1">
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Tanggal Dibayar <span class="text-danger">*</span></label>
        <div class="col-lg-5 col-md-7">
          <input type="text" class="form-control datetimepicker text-right" name="tgl_bayar" id="split_tgl_bayar" value="<?= date('d-m-Y H:i:s') ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label"><b>Jumlah Dibayar</b> <span class="text-danger">*</span></label>
        <div class="col-lg-5 col-md-7">
          <input type="text" class="form-control autonumeric text-primary" style="font-size:18px !important;font-weight:bold;" name="grand_total_bayar" id="split_grand_total_bayar" value="0" required="" dir="rtl" onkeyup="calc_split()">
        </div>
      </div>
      <div class="form-group row mt-2">
        <label class="col-lg-4 col-md-3 col-form-label">Kembalian <span class="text-danger">*</span></label>
        <div class="col-lg-5 col-md-7">
          <input type="text" class="form-control text-right" name="grand_total_kembalian" id="split_grand_total_kembalian" value="0" required="" readonly>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>
<script>
  $(document).ready(function() {
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');
    $(".autonumeric").autoNumeric({
      aSep: ".",
      aDec: ",",
      vMax: "999999999999999",
      vMin: "0",
    });
    $(".autonumeric-float").autoNumeric({
      aSep: ".",
      aDec: ",",
      aForm: true,
      vMax: "999999999999999999.99999",
      vMin: "-999999999999999999.99999",
      mDec: "4",
      aPad: false
    });
    $(".datetimepicker").daterangepicker({
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: "Clear",
        format: "DD-MM-YYYY H:mm:ss",
      },
      isInvalidDate: function(date) {
        return "";
      },
    });

    $("#form-split").validate({
      rules: {
        grand_total_kembalian: {
          remote: {
            url: '<?= site_url() . '/' . $nav['nav_url'] ?>/ajax_split/kembalian',
            type: 'post',
          }
        },
      },
      messages: {
        grand_total_kembalian: {
          remote: 'Jumlah bayar kurang!'
        }
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    calc_split();
  })

  function calc_split() {
    var grand_total_bruto = parseInt(num_sys($("#split_grand_total_bruto").val()));
    var grand_total_potongan = parseInt(num_sys($("#split_grand_total_potongan").val()));
    var potongan_cd = ($("#split_potongan_cd").val());
    var grand_total_potongan = parseInt(num_sys($("#split_grand_total_potongan").val()));
    var grand_total_netto = parseInt(num_sys($("#split_grand_total_netto").val()));
    var grand_pembulatan = parseInt(num_sys($("#split_grand_pembulatan").val()));
    var grand_total_tagihan = parseInt(num_sys($("#split_grand_total_tagihan").val()));
    var grand_total_bayar = parseInt(num_sys($("#split_grand_total_bayar").val()));
    var grand_total_kembalian = parseInt(num_sys($("#split_grand_total_kembalian").val()));

    if (potongan_cd == '01') {
      grand_total_potongan = grand_total_bruto;
      $("#split_grand_total_potongan").val(num_id(grand_total_potongan));
    }

    grand_total_netto = grand_total_bruto - grand_total_potongan;
    $("#split_grand_total_netto").val(num_id(grand_total_netto));

    //hitung akhir
    grand_total_tagihan = grand_total_netto + grand_pembulatan;
    $("#split_grand_total_tagihan").val(num_id(grand_total_tagihan));

    //hitung susuk
    grand_total_kembalian = grand_total_bayar - grand_total_tagihan;
    $("#split_grand_total_kembalian").val(num_id(grand_total_kembalian));
  }
</script>