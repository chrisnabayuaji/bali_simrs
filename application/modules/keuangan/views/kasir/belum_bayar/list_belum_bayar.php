<div class="table-responsive">
  <table id="obat_table" class="table table-hover table-bordered table-striped table-sm w-100">
    <thead>
      <tr>
        <th class="text-center" width="20">No.</th>
        <th class="text-center" width="100">No.RM</th>
        <th class="text-center" width="200">Nama Pasien</th>
        <th class="text-center">Alamat</th>
        <th class="text-center" width="100">Lokasi Pelayanan</th>
        <th class="text-center" width="100">Jenis Pasien</th>
        <th class="text-center" width="90">Tgl.Registrasi</th>
        <th class="text-center" width="80">Status Bayar</th>
        <th class="text-center" width="80">Pilih</th>
      </tr>
    </thead>
  </table>
</div>

<script type="text/javascript">
  var obatTable;
  $(document).ready(function () {
    //datatables
    obatTable = $('#obat_table').DataTable({
      "autoWidth" : false,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
      "ajax": {
          "url": "<?php echo site_url().'/'.$nav['nav_url'].'/ajax/search_belum_bayar'?>",
          "type": "POST"
      },      
      "columnDefs": [
        {"targets": 0, "className": 'text-center', "orderable" : false},
        {"targets": 1, "className": 'text-center', "orderable" : true},
        {"targets": 2, "className": 'text-left', "orderable" : true},
        {"targets": 3, "className": 'text-left', "orderable" : false},
        {"targets": 4, "className": 'text-center', "orderable" : false},
        {"targets": 5, "className": 'text-center', "orderable" : false},
        {"targets": 6, "className": 'text-center', "orderable" : false},
        {"targets": 7, "className": 'text-center', "orderable" : false},
        {"targets": 8, "className": 'text-center', "orderable" : false},
      ],
    });
    obatTable.columns.adjust().draw();
  })
</script>