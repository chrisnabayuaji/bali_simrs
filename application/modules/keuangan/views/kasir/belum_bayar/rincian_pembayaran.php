<?php $no = 1;
$total_seharusnya = 0;
$total_potongan = 0;
$total_tagihan = 0;
$total_jumlah = 0;
foreach ($list_rincian as $row) :
  $total_seharusnya += $row['jml_seharusnya'];
  $total_potongan += $row['jml_potongan'];
  $total_tagihan += $row['jml_tagihan'];
  $total_jumlah += $row['jml_tagihan'];
?>
  <tr style="background-color: #eaeaf1;">
    <td class="text-center" width="30">
      <b><?= $no ?></b>
    </td>
    <td class="text-center" width="50">
      <?php if ($row['tarif_id'] == '06' || $row['tarif_id'] == '07') : ?>
        <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/form_parsial/' . $main['billing_id'] . '/' . $row['tarif_id'] ?>" modal-title="Bayar Parsial" modal-size="md" class="btn btn-primary btn-table modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Bayar Parsial"><i class="fas fa-money-bill-alt"></i></a>
      <?php endif; ?>
    </td>
    <td class="text-left">
      <b><?= $row['tarif_nm'] ?></b>
      <input type="hidden" name="tarif_id[]" value="<?= $row['tarif_id'] ?>">
      <input type="hidden" name="data_tp[]" value="G">
    </td>
    <td class="text-right" width="125">
      <input type="hidden" name="jml_seharusnya[]" value="<?= ($row['jml_seharusnya'] != '') ? $row['jml_seharusnya'] : 0 ?>">
    </td>
    <td class="text-right" width="125">
      <input type="hidden" class="jml_potongan_group" data-nomor="<?= $no ?>" name="jml_potongan[]" value="<?= ($row['jml_potongan'] != '') ? $row['jml_potongan'] : 0 ?>">
    </td>
    <td class="text-right" width="125">
      <input type="hidden" name="jml_tagihan[]" value="<?= ($row['jml_tagihan'] != '') ? $row['jml_tagihan'] : 0 ?>">
    </td>
    <td class="text-right" width="125">
      <b><?= num_id($row['jml_tagihan']) ?></b>
      <input type="hidden" name="jml_total[]" value="">
    </td>
  </tr>
  <?php
  foreach ($row['list_detail'] as $row2) :
  ?>
    <tr>
      <td class="text-center" width="30"></td>
      <td class="text-center" width="50">
        <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/delete_tindakan/' . $row2['tarif_id'] . '/' . $id . '?groupreg_in=' . $groupreg_in ?>" class="btn btn-danger btn-table btn-delete" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-danger" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
      </td>
      <td class="text-left">
        - <?= $row2['tarif_nm'] ?>
      </td>
      <td class="text-right" width="125">
        <?= num_id($row2['jml_seharusnya']) ?>
      </td>
      <td class="text-right" width="125">
        <?= num_id($row2['jml_potongan']) ?>
      </td>
      <td class="text-right" width="125">
        <?= num_id($row2['jml_tagihan']) ?>
      </td>
      <td class="text-right" width="125">

      </td>
    </tr>
  <?php endforeach; ?>
<?php $no++;
endforeach; ?>
<?php
// Penjumlahan Total Obat
$total_seharusnya += @$main['grand_jml_awal_obat'];
$total_potongan += @$main['grand_jml_potongan_obat'];
$total_tagihan += @$main['grand_jml_obat'];
$total_jumlah += @$main['grand_jml_obat'];
// Penjumlahan Total Alkes
$total_seharusnya += @$main['grand_jml_awal_alkes'];
$total_potongan += @$main['grand_jml_potongan_alkes'];
$total_tagihan += @$main['grand_jml_alkes'];
$total_jumlah += @$main['grand_jml_alkes'];
// Penjumlahan Total BHP
$total_seharusnya += @$main['grand_jml_awal_bhp'];
$total_potongan += @$main['grand_jml_potongan_bhp'];
$total_tagihan += @$main['grand_jml_bhp'];
$total_jumlah += @$main['grand_jml_bhp'];

$no_akhir = 1;
for ($i = $no; $i <= ($no + 2); $i++) :
  if ($no_akhir == 1) {
    $tarif_id = 'OBAT';
    $tarif_nm = 'OBAT';
    $seharusnya = @$main['grand_jml_awal_obat'];
    $potongan = @$main['grand_jml_potongan_obat'];
    $tagihan = @$main['grand_jml_obat'];
    $total = @$main['grand_jml_obat'];
  }
  if ($no_akhir == 2) {
    $tarif_id = 'ALKES';
    $tarif_nm = 'ALKES';
    $seharusnya = @$main['grand_jml_awal_alkes'];
    $potongan = @$main['grand_jml_potongan_alkes'];
    $tagihan = @$main['grand_jml_alkes'];
    $total = @$main['grand_jml_alkes'];
  }
  if ($no_akhir == 3) {
    $tarif_id = 'BMHP';
    $tarif_nm = 'BMHP';
    $seharusnya = @$main['grand_jml_awal_bhp'];
    $potongan = @$main['grand_jml_potongan_nhp'];
    $tagihan = @$main['grand_jml_bhp'];
    $total = @$main['grand_jml_bhp'];
  }

?>
  <tr style="background-color: #eaeaf1;">
    <td class="text-center" width="30"><b><?= $i ?></b></td>
    <td class="text-left">
      <b><?= $tarif_nm ?></b>
      <input type="hidden" name="tarif_id[]" value="<?= $tarif_id ?>">
      <input type="hidden" name="data_tp[]" value="G">
    </td>
    <td class="text-right" width="125">
      <b><?= ($seharusnya != '') ? num_id($seharusnya) : '' ?></b>
      <input type="hidden" name="jml_seharusnya[]" value="<?= num_id($seharusnya) ?>">
    </td>
    <td class="text-right" width="125">
      <b><?= ($seharusnya != '') ? num_id($potongan) : '' ?></b>
      <input type="hidden" class="jml_potongan_group" data-nomor="<?= $i ?>" name="jml_potongan[]" value="<?= $potongan ?>">
    </td>
    <td class="text-right" width="125">
      <b><?= ($tagihan != '') ? num_id($tagihan) : '' ?></b>
      <input type="hidden" name="jml_tagihan[]" value="<?= $tagihan ?>">
    </td>
    <td class="text-right" width="125">
      <b><?= num_id($total) ?></b>
      <input type="hidden" name="jml_total[]" value="<?= $total ?>">
    </td>
  </tr>
<?php $no_akhir++;
endfor; ?>
<tr>
  <td class="text-center" colspan="3"><b>Total Biaya</b></td>
  <td class="text-right" width="125"><b><?= ($total_seharusnya != '') ? num_id($total_seharusnya) : '' ?></b></td>
  <td class="text-right" width="125"><b><?= ($total_potongan != '') ? num_id($total_potongan) : '' ?></b></td>
  <td class="text-right" width="125"><b><?= ($total_tagihan != '') ? num_id($total_tagihan) : '' ?></b></td>
  <td class="text-right" width="125">
    <b><?= num_id($total_jumlah) ?></b>
    <input type="hidden" name="total_seharusnya" id="total_seharusnya" value="<?= num_id($total_seharusnya) ?>">
    <input type="hidden" name="total_potongan" id="total_potongan" value="<?= num_id($total_potongan) ?>">
    <input type="hidden" name="total_tagihan" id="total_tagihan" value="<?= num_id($total_tagihan) ?>">
    <input type="hidden" name="total_jumlah" id="total_jumlah" value="<?= num_id($total_jumlah) ?>">
    <input type="hidden" name="grand_jml_obat" id="grand_jml_obat" value="<?= @$main['grand_jml_obat'] ?>">
    <input type="hidden" name="grand_jml_alkes" id="grand_jml_alkes" value="<?= @$main['grand_jml_alkes'] ?>">
    <input type="hidden" name="grand_jml_bhp" id="grand_jml_bhp" value="<?= @$main['grand_jml_bhp'] ?>">
  </td>
</tr>

<script>
  $(document).ready(function() {
    $(".modal-href").click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");
      var modal_header = $(this).attr("modal-header");
      var modal_content_top = $(this).attr("modal-content-top");

      $("#modal-size")
        .removeClass("modal-lg")
        .removeClass("modal-md")
        .removeClass("modal-sm");

      $("#modal-title").html(modal_title);
      $("#modal-size").addClass("modal-" + modal_size);
      if (modal_custom_size) {
        $("#modal-size").attr(
          "style",
          "max-width: " + modal_custom_size + "px !important"
        );
      }
      if (modal_content_top) {
        $(".modal-content-top").attr(
          "style",
          "margin-top: " + modal_content_top + " !important"
        );
      }
      if (modal_header == "hidden") {
        $("#modal-header").addClass("d-none");
      } else {
        $("#modal-header").removeClass("d-none");
      }
      $("#myModal").modal("show");
      $("#modal-body").html(
        '<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i><br>Loading</div>'
      );
      $.post(
        $(this).data("href"),
        function(data) {
          $("#modal-body").html(data.html);
        },
        "json"
      );
    });

    $(".btn-delete").on("click", function(e) {
      e.preventDefault();

      const href = $(this).data("href");

      Swal.fire({
        title: "Apakah Anda yakin?",
        text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#eb3b5a",
        cancelButtonColor: "#b2bec3",
        confirmButtonText: "Hapus",
        cancelButtonText: "Batal",
        customClass: "swal-wide",
      }).then((result) => {
        if (result.value) {
          document.location.href = href;
        }
      });
    });
  })
</script>