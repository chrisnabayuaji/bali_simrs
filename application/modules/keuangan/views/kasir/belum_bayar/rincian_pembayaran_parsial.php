<?php if (count($main) > 0) : ?>
  <?php $no = 1;
  foreach ($main as $row) :
  ?>
    <tr style="background-color: #eaeaf1;">
      <td class="text-center" width="30"><b><?= $no ?></b></td>
      <td class="text-center" width="100">
        <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/cetak_modal/cetak_kwitansi_parsial/' . $row['billing_id'] . '/' . $row['split_id'] ?>" modal-title="Cetak Kwitansi Laboratorium" modal-size="lg" modal-content-top="-75px" class=" btn btn-primary btn-table modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cetak Kwitansi Laboratorium"><i class="fas fa-print"></i> Cetak Kwitansi</a>
      </td>
      <td class="text-center" width="70"><b><?= $row['billing_id'] ?></b></td>
      <td class="text-center" width="70"><b><?= $row['split_id'] ?></b></td>
      <td class="text-center" width="120"><b><?= to_date($row['tgl_bayar'], '', 'full_date') ?></b></td>
      <td class="text-right" width="100"><b><?= num_id($row['grand_total_tagihan']) ?></b></td>
      <td class="text-right" width="100"><b><?= num_id($row['grand_total_bayar']) ?></b></td>
    </tr>
    <tr>
      <td width="130" colspan="2"></td>
      <td width="460" colspan="5"> - <?= $row['billing_parsial_rinc']['tarif_nm'] ?></td>
    </tr>
  <?php $no++;
  endforeach; ?>
<?php else : ?>
  <tr>
    <td class="text-center" colspan="7">Data tidak ada</td>
  </tr>
<?php endif; ?>
<script>
  $(document).ready(function() {
    $(".modal-href").click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");
      var modal_header = $(this).attr("modal-header");
      var modal_content_top = $(this).attr("modal-content-top");

      $("#modal-size")
        .removeClass("modal-lg")
        .removeClass("modal-md")
        .removeClass("modal-sm");

      $("#modal-title").html(modal_title);
      $("#modal-size").addClass("modal-" + modal_size);
      if (modal_custom_size) {
        $("#modal-size").attr(
          "style",
          "max-width: " + modal_custom_size + "px !important"
        );
      }
      if (modal_content_top) {
        $(".modal-content-top").attr(
          "style",
          "margin-top: " + modal_content_top + " !important"
        );
      }
      if (modal_header == "hidden") {
        $("#modal-header").addClass("d-none");
      } else {
        $("#modal-header").removeClass("d-none");
      }
      $("#myModal").modal("show");
      $("#modal-body").html(
        '<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i><br>Loading</div>'
      );
      $.post(
        $(this).data("href"),
        function(data) {
          $("#modal-body").html(data.html);
        },
        "json"
      );
    });
  })
</script>