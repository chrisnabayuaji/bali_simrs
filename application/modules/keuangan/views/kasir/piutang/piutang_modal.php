<?php if (@$main == null) : ?>
  <div class="alert alert-info">
    <i class="fas fa-info"></i> Tagihan berada di bagian rawat inap
  </div>
<?php else : ?>
  <div class="row mt-1">
    <div class="col-md-6">
      <table class="table" style="width: 100% !important; border-bottom: 1px solid #f2f2f2;">
        <tbody>
          <tr>
            <td width="150">No.Reg</td>
            <td width="20">:</td>
            <td><?= @$main['reg_id'] ?></td>
          </tr>
          <tr>
            <td width="150">NIK</td>
            <td width="20">:</td>
            <td><?= @$main['nik'] ?></td>
          </tr>
          <tr>
            <td width="150" style="font-weight: 500 !important;">No.RM / Nama Pasien</td>
            <td width="20">:</td>
            <td><?= @$main['pasien_id'] ?> / <?= @$main['pasien_nm'] ?></td>
          </tr>
          <tr>
            <td width="150">Alamat Pasien</td>
            <td width="20">:</td>
            <td>
              <?= (@$main['alamat'] != '') ? @$main['alamat'] . ', ' : '' ?>
              <?= (@$main['kelurahan'] != '') ? ucwords(strtolower(@$main['kelurahan'])) . ', ' : '' ?>
              <?= (@$main['kecamatan'] != '') ? ucwords(strtolower(@$main['kecamatan'])) . ', ' : '' ?>
              <?= (@$main['kabupaten'] != '') ? ucwords(strtolower(@$main['kabupaten'])) . ', ' : '' ?>
              <?= (@$main['provinsi'] != '') ? ucwords(strtolower(@$main['provinsi'])) : '' ?>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="col-md-6">
      <table class="table" style="width: 100% !important; border-bottom: 1px solid #f2f2f2;">
        <tbody>
          <tr>
            <td width="150">Dokter PJ</td>
            <td width="20">:</td>
            <td><?= @$main['dokter_nm'] ?></td>
          </tr>
          <tr>
            <td width="150">Umur / JK</td>
            <td width="20">:</td>
            <td><?= @$main['umur_thn'] ?> Th <?= @$main['umur_bln'] ?> Bl <?= @$main['umur_hr'] ?> Hr / <?= get_parameter_value('sex_cd', @$main['sex_cd']) ?></td>
          </tr>
          <tr>
            <td width="150">Jenis / Asal Pasien</td>
            <td width="20">:</td>
            <td><?= @$main['jenispasien_nm'] ?> / <?= get_parameter_value('asalpasien_cd', @$main['asalpasien_cd']) ?></td>
          </tr>
          <tr>
            <td width="150">Tgl. Registrasi</td>
            <td width="20">:</td>
            <td><?= @to_date($main['tgl_mainistrasi'], '-', 'full_date') ?></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
  <hr>
  <div class="row">
    <div class="col">
      <h6><b><i class="fas fa-procedures"></i> A. Tindakan</b></h6>
      <table class="table table-bordered table-striped table-sm">
        <thead>
          <tr>
            <th class="text-center" width="50">No.</th>
            <th class="text-center" width="150">Tgl. Catat</th>
            <th class="text-center">Jenis Tindakan</th>
            <th class="text-center" width="80">Qty</th>
            <th class="text-center" width="200">Tagihan</th>
          </tr>
        </thead>
        <?php $total_tindakan = 0;
        if ($tindakan == null) : ?>
          <tbody>
            <tr>
              <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
            </tr>
          </tbody>
        <?php else : ?>
          <tbody>
            <?php $i = 1;
            foreach ($tindakan as $r) : ?>
              <tr>
                <td class="text-center"><?= $i++ ?></td>
                <td class="text-center"><?= to_date($r['tgl_catat'], '-', 'full_date') ?></td>
                <td class="text-left"><?= $r['tarif_nm'] ?></td>
                <td class="text-center"><?= $r['qty'] ?></td>
                <td class="text-right"><?= num_id($r['jml_tagihan']) ?></td>
                <?php $total_tindakan += $r['jml_tagihan']; ?>
              </tr>
            <?php endforeach; ?>
          </tbody>
        <?php endif; ?>
        <tfoot>
          <tr>
            <th class="text-right" colspan="3">Jumlah Tagihan</th>
            <th class="text-right"></th>
            <th class="text-right"><?= num_id($total_tindakan) ?></th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>

  <div class="row mt-2">
    <div class="col">
      <h6><b><i class="fas fa-pills"></i> B. Obat</b></h6>
      <table class="table table-bordered table-striped table-sm">
        <thead>
          <tr>
            <th class="text-center" width="50">No.</th>
            <th class="text-center" width="150">Tgl. Catat</th>
            <th class="text-center">Nama Obat</th>
            <th class="text-center" width="80">Qty</th>
            <th class="text-center" width="200">Tagihan</th>
          </tr>
        </thead>
        <?php $total_obat = 0;
        if ($obat == null) : ?>
          <tbody>
            <tr>
              <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
            </tr>
          </tbody>
        <?php else : ?>
          <tbody>
            <?php $i = 1;
            foreach ($obat as $r) : ?>
              <tr>
                <td class="text-center"><?= $i++ ?></td>
                <td class="text-center"><?= to_date($r['tgl_catat'], '-', 'full_date') ?></td>
                <td class="text-left"><?= $r['obat_nm'] ?></td>
                <td class="text-center"><?= $r['qty'] ?></td>
                <td class="text-right"><?= num_id($r['jumlah_akhir']) ?></td>
                <?php $total_obat += $r['jumlah_akhir']; ?>
              </tr>
            <?php endforeach; ?>
          </tbody>
        <?php endif; ?>
        <tfoot>
          <tr>
            <th class="text-right" colspan="3">Jumlah Tagihan</th>
            <th class="text-right"></th>
            <th class="text-right"><?= num_id($total_obat) ?></th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>

  <div class="row mt-2">
    <div class="col">
      <h6><b><i class="fas fa-box-open"></i> C. BHP</b></h6>
      <table class="table table-bordered table-striped table-sm">
        <thead>
          <tr>
            <th class="text-center" width="50">No.</th>
            <th class="text-center" width="150">Tgl. Catat</th>
            <th class="text-center">Nama BHP</th>
            <th class="text-center" width="80">Qty</th>
            <th class="text-center" width="200">Tagihan</th>
          </tr>
        </thead>
        <?php $total_bhp = 0;
        if ($bhp == null) : ?>
          <tbody>
            <tr>
              <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
            </tr>
          </tbody>
        <?php else : ?>
          <tbody>
            <?php $i = 1;
            foreach ($bhp as $r) : ?>
              <tr>
                <td class="text-center"><?= $i++ ?></td>
                <td class="text-center"><?= to_date($r['tgl_catat'], '-', 'full_date') ?></td>
                <td class="text-left"><?= $r['barang_nm'] ?></td>
                <td class="text-center"><?= $r['qty'] ?></td>
                <td class="text-right"><?= num_id($r['jml_tagihan']) ?></td>
                <?php $total_bhp += $r['jml_tagihan']; ?>
              </tr>
            <?php endforeach; ?>
          </tbody>
        <?php endif; ?>
        <tfoot>
          <tr>
            <th class="text-right" colspan="3">Jumlah Tagihan</th>
            <th class="text-right"></th>
            <th class="text-right"><?= num_id($total_bhp) ?></th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>

  <div class="row mt-2">
    <div class="col">
      <h6><b><i class="fas fa-syringe"></i> D. ALKES</b></h6>
      <table class="table table-bordered table-striped table-sm">
        <thead>
          <tr>
            <th class="text-center" width="50">No.</th>
            <th class="text-center" width="150">Tgl. Catat</th>
            <th class="text-center">Nama Alkes</th>
            <th class="text-center" width="80">Qty</th>
            <th class="text-center" width="200">Tagihan</th>
          </tr>
        </thead>
        <?php $total_alkes = 0;
        if ($alkes == null) : ?>
          <tbody>
            <tr>
              <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
            </tr>
          </tbody>
        <?php else : ?>
          <tbody>
            <?php $i = 1;
            foreach ($alkes as $r) : ?>
              <tr>
                <td class="text-center"><?= $i++ ?></td>
                <td class="text-center"><?= to_date($r['tgl_catat'], '-', 'full_date') ?></td>
                <td class="text-left"><?= $r['barang_nm'] ?></td>
                <td class="text-center"><?= $r['qty'] ?></td>
                <td class="text-right"><?= num_id($r['jml_tagihan']) ?></td>
                <?php $total_alkes += $r['jml_tagihan']; ?>
              </tr>
            <?php endforeach; ?>
          </tbody>
        <?php endif; ?>
        <tfoot>
          <tr>
            <th class="text-right" colspan="3">Jumlah Tagihan</th>
            <th class="text-right"></th>
            <th class="text-right"><?= num_id($total_alkes) ?></th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>

  <div class="row mt-2">
    <div class="col">
      <h6><b><i class="fas fa-bed"></i> E. KAMAR</b></h6>
      <table class="table table-bordered table-striped table-sm">
        <thead>
          <tr>
            <th class="text-center" width="50">No.</th>
            <th class="text-center" width="150">Tgl. Masuk</th>
            <th class="text-center">Nama Kamar</th>
            <th class="text-center" width="80">Hari</th>
            <th class="text-center" width="200">Tagihan</th>
          </tr>
        </thead>
        <?php $total_kamar = 0;
        if ($kamar == null) : ?>
          <tbody>
            <tr>
              <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
            </tr>
          </tbody>
        <?php else : ?>
          <tbody>
            <?php $i = 1;
            foreach ($kamar as $r) : ?>
              <tr>
                <td class="text-center"><?= $i++ ?></td>
                <td class="text-center"><?= to_date($r['tgl_masuk'], '-', 'full_date') ?></td>
                <td class="text-left"><?= $r['kamar_nm'] ?></td>
                <td class="text-center"><?= $r['jml_hari'] ?></td>
                <td class="text-right"><?= num_id($r['jml_tagihan']) ?></td>
                <?php $total_kamar += $r['jml_tagihan']; ?>
              </tr>
            <?php endforeach; ?>
          </tbody>
        <?php endif; ?>
        <tfoot>
          <tr>
            <th class="text-right" colspan="3">Jumlah Tagihan</th>
            <th class="text-right"></th>
            <th class="text-right"><?= num_id($total_kamar) ?></th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>

  <div class="row mt-4">
    <div class="col text-right">
      <h5 class="text-primary"><b>Total Tagihan : <?= num_id($main['jml_total_biaya']) ?></b></h5>
    </div>
  </div>
<?php endif; ?>