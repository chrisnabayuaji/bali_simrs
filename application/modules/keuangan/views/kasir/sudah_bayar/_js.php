<script type="text/javascript">
  $(document).ready(function() {
    $("#form-data").validate({
      rules: {},
      messages: {},
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    rincian_pembayaran('<?= @$id ?>', '<?= @$groupreg_in ?>');

    <?php if (@$main['bayar_st'] == 1) : ?>
      $('#box_lunas').removeClass('d-none');
      $('#box_kembalian').removeClass('d-none');
      $('#box_titip').addClass('d-none');
    <?php elseif (@$main['bayar_st'] == 2) : ?>
      $('#box_titip').removeClass('d-none');
      $('#box_lunas').addClass('d-none');
      $('#box_kembalian').addClass('d-none');
    <?php endif; ?>

    $('#bayar_st').bind('change', function(e) {
      e.preventDefault();
      var i = $(this).val();
      if (i == 1) {
        $('#box_lunas').removeClass('d-none');
        $('#box_kembalian').removeClass('d-none');
        $('#box_titip').addClass('d-none');
      } else if (i == 2) {
        $('#box_titip').removeClass('d-none');
        $('#box_lunas').addClass('d-none');
        $('#box_kembalian').addClass('d-none');
      }
    })

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    $('.jml_potongan').bind('keyup', function() {
      var sum = 0;
      $(".jml_potongan_group").each(function() {
        sum += +($(this).val() != '' ? $(this).val().replace(/\./g, '') : '0');
      });
      $(".jml_potongan").each(function() {
        sum += +($(this).val() != '' ? $(this).val().replace(/\./g, '') : '0');
      });
      $("#tot_jml_potongan").autoNumericSet(sum);
      $("#grand_total_potongan").autoNumericSet(sum);

      var grand_total_bruto = ($('#grand_total_bruto').val() != '' ? $('#grand_total_bruto').val().replace(/\./g, '') : '0');
      var grand_total_potongan = ($('#grand_total_potongan').val() != '' ? $('#grand_total_potongan').val().replace(/\./g, '') : '0');
      var res_sum = grand_total_bruto - grand_total_potongan;
      $("#grand_total_netto").autoNumericSet(res_sum);
      $("#grand_total_tagihan").autoNumericSet(res_sum);
    });

    $('#grand_total_potongan').bind('keyup', function() {
      var grand_total_bruto = ($('#grand_total_bruto').val() != '' ? $('#grand_total_bruto').val().replace(/\./g, '') : '0');
      var grand_total_potongan = ($(this).val() != '' ? $(this).val().replace(/\./g, '') : '0');
      var res_sum = grand_total_bruto - grand_total_potongan;
      $("#grand_total_netto").autoNumericSet(res_sum);
    });

    $('#grand_pembulatan').bind('keyup', function() {
      var grand_total_netto = ($('#grand_total_netto').val() != '' ? $('#grand_total_netto').val().replace(/\./g, '') : '0');
      var grand_pembulatan = ($(this).val() != '' ? $(this).val().replace(/\./g, '') : '0');
      var res_sum = parseFloat(grand_total_netto) + parseFloat(grand_pembulatan);
      $("#grand_total_tagihan").autoNumericSet(res_sum);
    });

    <?php if (@$main['bayar_st'] == 1) : ?>
      var grand_total_tagihan = ($('#grand_total_tagihan').val() != '' ? $('#grand_total_tagihan').val().replace(/\./g, '') : '0');
      var grand_total_bayar = ($('#grand_total_bayar').val() != '' ? $('#grand_total_bayar').val().replace(/\./g, '') : '0');
      var res_sum = grand_total_bayar - grand_total_tagihan;
      $("#kembalian").autoNumericSet(res_sum);
    <?php endif; ?>

    $('#grand_total_bayar').bind('keyup', function() {
      var grand_total_tagihan = ($('#grand_total_tagihan').val() != '' ? $('#grand_total_tagihan').val().replace(/\./g, '') : '0');
      var grand_total_bayar = ($(this).val() != '' ? $(this).val().replace(/\./g, '') : '0');
      var res_sum = grand_total_bayar - grand_total_tagihan;
      if (res_sum < 0) {
        $(this).addClass('is-invalid');
        $('#pembayaran_kurang').addClass('d-block').removeClass('d-none');
      } else {
        $(this).removeClass('is-invalid');
        $('#pembayaran_kurang').addClass('d-none').removeClass('d-block');
      }
      $("#kembalian").autoNumericSet(res_sum);
    });
  })

  function rincian_pembayaran(id = '', groupreg_in = '') {
    $('#rincian_pembayaran').html('<tr><td class="text-center" colspan="99"><div class="mt-3 mb-2"><i class="fas fa-spin fa-spinner fa-2x"></i><div style="font-size: 14px;" class="mt-1">Memuat...</div></div></td></tr>');
    $.post('<?= site_url('keuangan/kasir/sudah_bayar/ajax/rincian_pembayaran') ?>', {
      id: id,
      groupreg_in: groupreg_in
    }, function(data) {
      $('#rincian_pembayaran').html(data.html);
    }, 'json');
  }
</script>