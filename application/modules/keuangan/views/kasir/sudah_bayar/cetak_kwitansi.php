<!DOCTYPE html>
<html>
<head>
  <title>Cetak Kwitansi</title>
  <style type="text/css">
    body { 
      font-size: 14px; 
      font-family: Calibri;
    }
    .table-border-all {
      border-collapse: collapse;
    }

    .table-border-all, .table-border-all th, .table-border-all td {
      border: 1px solid black;
    }
  </style>
</head>
<body onload="window.print()">
  <table border="0" width="600px" style="border: 1px solid black;">
    <tr>
      <td align="center" width="65px"><img src="<?=base_url()?>assets/images/icon/<?=@$identitas['logo_rumah_sakit']?>" style="width: 65px;"></td>
      <td valign="top" align="center">
        <table>
          <tr>
            <td align="center" style="font-size: 15px;"><b><?=@$config['title_logo_login']?></b></td>
          </tr>
          <tr>
            <td align="center" style="font-size: 15px;"><b><?=@$config['sub_title_logo_login']?></b></td>
          </tr>
          <tr>
            <td align="center"><div style="font-size: 13px;"><?=@$identitas['jalan']?>, <?=ucfirst(strtolower(clear_kab_kota(@$identitas['kabupaten'])))?>, <?=ucfirst(strtolower(@$identitas['propinsi']))?></div></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="99">------------------------------------------------------------------------------------------------------------------------------------------</td>
    </tr>
    <tr>
      <td colspan="2">
        <table border="0" width="100%">
          <tr>
            <td width="25%">ID.Billing</td>
            <td width="2%">:</td>
            <td><?=@$main['billing_id']?></td>

            <td align="right">Tgl.Kwitansi</td>
            <td align="center">:</td>
            <td align="left" width="20%"><?=to_date_indo(@$main['tgl_transaksi'], 'date')?></td>
          </tr>
          <tr>
            <td width="25%">Sudah Terima dari</td>
            <td width="2%">:</td>
            <td colspan="4"><?=@$main['pasien_nm']?></td>
          </tr>
          <tr>
            <td width="25%">Jumlah Uang</td>
            <td width="2%">:</td>
            <td colspan="4">Rp <?=(@$main['grand_total_tagihan'] !='') ? num_id(@$main['grand_total_tagihan'])." <i>(".terbilang(@$main['grand_total_tagihan']).")</i>" : '0' ?></td>
          </tr>
          <tr>
            <td width="25%">Guna Membayar</td>
            <td width="2%">:</td>
            <td colspan="4">Biaya perawatan dan pengobatan</td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="99">------------------------------------------------------------------------------------------------------------------------------------------</td>
    </tr>
    <tr>
      <td colspan="2">
        <table border="0" width="100%">
          <tr>
            <td width="25%" valign="top">Nama / No.RM</td>
            <td width="2%" valign="top">:</td>
            <td><?=@$main['pasien_id']?> / <?=@$main['pasien_nm']?></td>
          </tr>
          <tr>
            <td width="25%" valign="top">Alamat</td>
            <td width="2%" valign="top">:</td>
            <td><?=@$main['alamat']?>, <?=strtoupper(@$main['kelurahan'])?>, <?=strtoupper(@$main['kecamatan'])?>, <?=strtoupper(@$main['kabupaten'])?>, <?=strtoupper(@$main['provinsi'])?></td>
          </tr>
          <tr>
            <td width="25%" valign="top">Jenis / Asal Pasien</td>
            <td width="2%" valign="top">:</td>
            <td><?=@$main['jenispasien_nm']?> / <?=get_parameter_value('asalpasien_cd', @$main['asalpasien_cd'])?></td>
          </tr>
          <tr>
            <td width="25%" valign="top">Dokter</td>
            <td width="2%" valign="top">:</td>
            <td><?=@$main['pegawai_nm']?></td>
          </tr>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <table class="table-border-all" width="100%">
          <thead>
            <tr>
              <th align="center" rowspan="2" width="28"><b>No</b></th>
              <th align="center" rowspan="2" width="200"><b>Jenis Item Pembayaran</b></th>
              <th align="center" colspan="3" width="300"><b>Biaya</b></th>
            </tr>
            <tr>
              <th align="center" width="100"><b>Seharusnya</b></th>
              <th align="center" width="100"><b>Potongan</b></th>
              <th align="center" width="100"><b>Tagihan</b></th>
            </tr>
          </thead>
          <?php if(@$list_rincian == null):?>
          <tbody>
            <tr>
              <td colspan="99"><i>Tidak ada data!</i></td>
            </tr>
          </tbody>
          <?php else: ?>
          <tbody>
            <?php 
              $i=1;
              $no=1;
              $tot_jml_seharusnya = 0;
              $tot_jml_potongan = 0;
              $tot_jml_tagihan = 0;
              foreach($list_rincian as $row):
              $tot_jml_seharusnya += $row['jml_seharusnya'];
              $tot_jml_potongan += $row['jml_potongan'];
              $tot_jml_tagihan += $row['jml_tagihan'];
            ?>
            <tr>
              <td align="center" width="28"><b><?=$i?></b></td>
              <td width="200"><b><?=$row['tarif_nm']?></b></td>
              <td width="100" align="right"><b><?=num_id($row['jml_seharusnya'])?></b></td>
              <td width="100" align="right"><b><?=num_id($row['jml_potongan'])?></b></td>
              <td width="100" align="right"><b><?=num_id($row['jml_tagihan'])?></b></td>
            </tr>
            <?php 
              $i++;
              $no = $no++;
              foreach ($row['list_detail'] as $row2): 
            ?>
            <tr>
              <td align="center" width="28"></td>
              <td width="200">- <?=$row2['tarif_nm']?></td>
              <td width="100" align="right"><?=num_id($row2['jml_seharusnya'])?></td>
              <td width="100" align="right"><?=num_id($row2['jml_potongan'])?></td>
              <td width="100" align="right"><?=num_id($row2['jml_tagihan'])?></td>
            </tr>
            <?php $no++; endforeach;?>
            <?php endforeach; ?>
          </tbody>
          <?php endif; ?>
        </table>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <table border="0" width="100%">
          <tr>
            <td width="50%" valign="top">
              <?php if (count($list_riwayat_bangsal) > 0): ?>
              <table width="100%">
                <tr>
                  <td>Riwayat Pemondokan</td>
                </tr>
                <?php 
                $no=1;
                foreach ($list_riwayat_bangsal as $row): 
                ?>
                <tr>
                  <td style="font-size: 12px;">
                    <?php if ($row['jml_hari'] !=''): ?>
                      <?=($no++)?>) <?=$row['kamar_nm']?>, Rp <?=num_id($row['jml_tagihan'])?>, Tgl : <?=to_date($row['tgl_masuk'], '', 'date')?> s.d <?=to_date($row['tgl_keluar'], '', 'date')?> (<?=$row['jml_hari']?> hari), dihitung <?=$row['jml_hari']?> hari x Rp. <?=num_id($row['nom_tarif'])?>
                    <?php else: ?>
                      <?=($no++)?>) <?=$row['kamar_nm']?>, Rp. <?=num_id($row['nom_tarif'] * selisih_hari(date('d-m-Y H:i:s'), to_date($row['tgl_masuk'],'','date')))?>, Tgl : <?=to_date($row['tgl_masuk'], '', 'date')?> sd <?=date('d-m-Y')?> (<?=selisih_hari(date('d-m-Y H:i:s'), to_date($row['tgl_masuk'],'','date'))?> hari), dihitung <?=selisih_hari(date('d-m-Y H:i:s'), to_date($row['tgl_masuk'],'','date'))?> hari x Rp. <?=num_id($row['nom_tarif'])?>
                    <?php endif; ?>
                  </td>
                </tr>
                <?php endforeach; ?>
              </table>
              <table width="100%">
                <tr>
                  <td><br></td>
                </tr>
              </table>
              <?php endif; ?>
              <table width="100%">
                <tr>
                  <td>Catatan Pembayaran</td>
                </tr>
                <tr>
                  <td><i><?=@$main['keterangan_bayar']?></i></td>
                </tr>
              </table>
            </td>
            <td width="50%" valign="top">
              <table border="0" width="100%">
                <tr>
                  <td width="60%">Biaya Perawatan/Tindakan</td>
                  <td align="center" valign="top" width="2%">:</td>
                  <td align="right" valign="top" width="38%">Rp <?=(@$main['grand_jml_tindakan'] !='') ? num_id(@$main['grand_jml_tindakan']) : '0' ?></td>
                </tr>
                <tr>
                  <td width="60%">Biaya Obat</td>
                  <td align="center" valign="top" width="2%">:</td>
                  <td align="right" valign="top" width="38%">Rp <?=(@$main['grand_jml_obat'] !='') ? num_id(@$main['grand_jml_obat']) : '0' ?></td>
                </tr>
                <tr>
                  <td width="60%">Biaya Alkes</td>
                  <td align="center" valign="top" width="2%">:</td>
                  <td align="right" valign="top" width="38%">Rp <?=(@$main['grand_jml_alkes'] !='') ? num_id(@$main['grand_jml_alkes']) : '0' ?></td>
                </tr>
                <tr>
                  <td width="60%">Biaya BHP</td>
                  <td align="center" valign="top" width="2%">:</td>
                  <td align="right" valign="top" width="38%">Rp <?=(@$main['grand_jml_bhp'] !='') ? num_id(@$main['grand_jml_bhp']) : '0' ?></td>
                </tr>
                <tr>
                  <td colspan="3" align="right">------------------------------------------- + </td>
                </tr>
                <tr>
                  <td width="60%">Total Biaya Perawatan + Obat + Alkes + BHP</td>
                  <td align="center" valign="top" width="2%">:</td>
                  <td align="right" valign="top" width="38%">Rp <?=(@$main['grand_total_bruto'] !='') ? num_id(@$main['grand_total_bruto']) : '0' ?></td>
                </tr>
                <tr>
                  <td width="60%">Total Potongan</td>
                  <td align="center" valign="top" width="2%">:</td>
                  <td align="right" valign="top" width="38%">Rp <?=(@$main['grand_total_potongan'] !='') ? num_id(@$main['grand_total_potongan']) : '0' ?></td>
                </tr>
                <tr>
                  <td colspan="3" align="right">------------------------------------------- - </td>
                </tr>
                <tr>
                  <td width="60%">Total Akhir</td>
                  <td align="center" valign="top" width="2%">:</td>
                  <td align="right" valign="top" width="38%">Rp <?=(@$main['grand_total_netto'] !='') ? num_id(@$main['grand_total_netto']) : '0' ?></td>
                </tr>
                <tr>
                  <td width="60%">Pembulatan</td>
                  <td align="center" valign="top" width="2%">:</td>
                  <td align="right" valign="top" width="38%">Rp <?=(@$main['grand_pembulatan'] !='') ? num_id(@$main['grand_pembulatan']) : '0' ?></td>
                </tr>
                <tr>
                  <td width="60%">Jumlah Yang Harus Dibayar</td>
                  <td align="center" valign="top" width="2%">:</td>
                  <td align="right" valign="top" width="38%">Rp <?=(@$main['grand_total_tagihan'] !='') ? num_id(@$main['grand_total_tagihan']) : '0' ?></td>
                </tr>
                <tr>
                  <td colspan="3"><br></td>
                </tr>
                <tr>
                  <td align="center" colspan="3">Purworejo, <?=to_date_indo(date('Y-m-d'))?></td>
                </tr>
                <tr>
                  <td align="center" colspan="3">Kasir</td>
                </tr>
                <tr>
                  <td colspan="3"><br><br></td>
                </tr>
                <tr>
                  <td align="center" colspan="3"><?=@$this->session->userdata('sess_user_realname')?></td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
  </table>
</body>
</html>