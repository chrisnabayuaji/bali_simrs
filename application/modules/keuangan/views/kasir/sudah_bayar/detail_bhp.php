<div class="row">
  <div class="col-12">
    <div class="table-responsive">
      <table class="table table-bordered table-striped table-sm">
        <thead>
          <tr>
            <th class="text-center" width="20">No</th>
            <th class="text-center" width="100">Kode</th>
            <th class="text-center">Nama BHP</th>
            <th class="text-center" width="100">Harga Jual</th>
            <th class="text-center" width="60">Qty</th>
            <th class="text-center" width="100">Seharusnya</th>
            <th class="text-center" width="100">Potongan</th>
            <th class="text-center" width="100">Tagihan</th>
            <th class="text-center" width="200">Keterangan</th>
          </tr>
        </thead>
        <tbody>
          <?php if (@$main == null) : ?>
            <tr>
              <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
            </tr>
          <?php else : ?>
            <?php $i = 1;
            $total_jml_awal = 0;
            $total_jml_potongan = 0;
            $total_jml_tagihan = 0;
            foreach ($main as $row) :
              $total_jml_awal += $row['jml_awal'];
              $total_jml_potongan += $row['jml_potongan'];
              $total_jml_tagihan += $row['jml_tagihan'];
            ?>
              <tr>
                <td class="text-center"><?= $i++ ?></td>
                <td class="text-center"><?= $row['barang_id'] ?></td>
                <td class="text-left"><?= $row['barang_nm'] ?></td>
                <td class="text-right"><?= num_id($row['harga']) ?></td>
                <td class="text-center"><?= $row['qty'] ?></td>
                <td class="text-right"><?= num_id($row['jml_awal']) ?></td>
                <td class="text-right"><?= num_id($row['jml_potongan']) ?></td>
                <td class="text-right"><?= num_id($row['jml_tagihan']) ?></td>
                <td class="text-left"><?= $row['keterangan_bhp'] ?></td>
              </tr>
            <?php endforeach; ?>
            <tr>
              <th class="text-center" colspan="5">Total</th>
              <th class="text-right"><?= num_id($total_jml_awal) ?></th>
              <th class="text-right"><?= num_id($total_jml_potongan) ?></th>
              <th class="text-right"><?= num_id($total_jml_tagihan) ?></th>
            </tr>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="border-top border-2 mt-2 pt-2 pb-0">
  <div class="row">
    <div class="offset-lg-11 offset-md-11">
      <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Tutup</button>
    </div>
  </div>
</div>