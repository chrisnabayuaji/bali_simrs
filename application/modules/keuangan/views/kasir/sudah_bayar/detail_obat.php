<div class="row">
  <div class="col-12">
    <div class="table-responsive">
      <table class="table table-bordered table-sm">
        <tbody>
          <?php if (@$detail_obat == null) : ?>
            <tr>
              <th class="text-center" width="20">No</th>
              <th class="text-center text-middle">Nama Obat</th>
              <th class="text-center text-middle" width="80">Aturan Pakai</th>
              <th class="text-center text-middle" width="80">Jam Minum</th>
              <th class="text-center text-middle" width="80">Aturan Tambahan</th>
              <th class="text-center text-middle" width="80">Harga</th>
              <th class="text-center text-middle" width="50">Qty</th>
              <th class="text-center text-middle" width="80">Seharusnya</th>
              <th class="text-center text-middle" width="80">Potongan</th>
              <th class="text-center text-middle" width="80">Tagihan</th>
            </tr>
            <tr>
              <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
            </tr>
          <?php else : ?>
            <?php foreach ($detail_obat as $row) : ?>
              <tr bgcolor="#f5f5f5">
                <td class="text-left" colspan="99"><b>NO.RESEP : <?= $row['resep_id'] ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; TANGGAL : <?= to_date($row['tgl_catat'], '', 'date') ?></b></td>
              </tr>
              <tr>
                <th class="text-center" width="20">No</th>
                <th class="text-center text-middle">Nama Obat</th>
                <th class="text-center text-middle" width="80">Aturan Pakai</th>
                <th class="text-center text-middle" width="80">Jam Minum</th>
                <th class="text-center text-middle" width="80">Aturan Tambahan</th>
                <th class="text-center text-middle" width="80">Harga</th>
                <th class="text-center text-middle" width="50">Qty</th>
                <th class="text-center text-middle" width="80">Seharusnya</th>
                <th class="text-center text-middle" width="80">Potongan</th>
                <th class="text-center text-middle" width="80">Tagihan</th>
              </tr>
              <?php $i = 1;
              foreach ($row['list_dat_resep'] as $dat_resep) : ?>
                <tr>
                  <td class="text-center" width="20"><?= $i++ ?></td>
                  <td class="text-left"><?= $dat_resep['obat_nm'] ?></td>
                  <td class="text-left"><?= $dat_resep['aturan_pakai'] ?></td>
                  <td class="text-left"><?= $dat_resep['jadwal'] ?></td>
                  <td class="text-left"><?= $dat_resep['aturan_tambahan'] ?></td>
                  <td class="text-right"><?= num_id($dat_resep['harga']) ?></td>
                  <td class="text-center"><?= num_id($dat_resep['qty']) ?></td>
                  <td class="text-right"><?= num_id($dat_resep['jumlah_awal']) ?></td>
                  <td class="text-right"><?= num_id($dat_resep['potongan']) ?></td>
                  <td class="text-right">
                    <b><?= num_id($dat_resep['jumlah_akhir']) ?></b>
                  </td>
                </tr>
              <?php endforeach; ?>
            <?php endforeach; ?>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="col-8 mt-3"></div>
  <div class="col-4 mt-3">
    <?php if (@$detail_obat != null) : ?>
      <div class="table-responsive">
        <table class="table table-sm">
          <tbody>
            <tr>
              <th class="text-right">Total Bruto</th>
              <th class="text-center">:</th>
              <td class="text-right" width="100"><?= @num_id($main['grand_jml_bruto']) ?></td>
            </tr>
            <tr>
              <th class="text-right">Total PPN</th>
              <th class="text-center">:</th>
              <td class="text-right" width="100"><?= @num_id($main['grand_prs_ppn']) ?>% (<?= @num_id($main['grand_nom_ppn']) ?>)</td>
            </tr>
            <tr>
              <th class="text-right">Total Embalace</th>
              <th class="text-center">:</th>
              <td class="text-right" width="100"><?= @num_id($main['grand_embalace']) ?></td>
            </tr>
            <tr>
              <th class="text-right">Total Potongan Akhir</th>
              <th class="text-center">:</th>
              <td class="text-right" width="100"><?= @num_id($main['grand_jml_potongan']) ?></td>
            </tr>
            <tr>
              <th class="text-right">Total Pembulatan</th>
              <th class="text-center">:</th>
              <td class="text-right" width="100"><?= @num_id($main['grand_pembulatan']) ?></td>
            </tr>
            <tr>
              <th class="text-right text-top">
                <div>Jumlah Seharusnya</div>
                <div style="font-size: 10px; margin-top: 3px;">(Sebelum Total Potongan Akhir)</div>
              </th>
              <th class="text-center text-top">:</th>
              <td class="text-top text-right" width="100"><?= @num_id($main['grand_jml_tagihan'] + @$main['grand_jml_potongan']) ?></td>
            </tr>
            <tr>
              <th class="text-right">Total Tagihan</th>
              <th class="text-center">:</th>
              <th class="text-right" width="100"><?= @num_id($main['grand_jml_tagihan']) ?></th>
            </tr>
          </tbody>
        </table>
      </div>
    <?php endif; ?>
  </div>
</div>
<div class="border-top border-2 mt-2 pt-2 pb-0">
  <div class="row">
    <div class="offset-lg-11 offset-md-11">
      <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Tutup</button>
    </div>
  </div>
</div>