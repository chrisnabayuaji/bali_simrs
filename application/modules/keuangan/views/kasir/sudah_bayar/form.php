<?php $this->load->view('_js'); ?>
<div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
<form role="form" method="POST" enctype="multipart/form-data" action="" class="needs-validation" novalidate autocomplete="off">
  <input type="hidden" name="pasien_id" value="<?= @$main['pasien_id'] ?>">
  <input type="hidden" name="pasien_nm" value="<?= @$main['pasien_nm'] ?>">
  <input type="hidden" name="lokasi_id" value="<?= @$main['lokasi_id'] ?>">
  <input type="hidden" name="jenispasien_id" value="<?= @$main['jenispasien_id'] ?>">
  <input type="hidden" name="tgl_masuk" value="<?= @$main['tgl_registrasi'] ?>">
  <input type="hidden" name="tgl_keluar" value="<?= @$main['tgl_pulang'] ?>">
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline col-title">
          <div class="col-back">
            <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
          </div>
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            <?= $nav['nav_nm'] ?>
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-warehouse"></i> Gudang</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url('keuangan/kasir/sudah_bayar') ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item active"><span>Form</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>

    <div class="row full-page mt-4">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <h4 class="card-title border-bottom border-2 pb-2 mb-3"><?= $nav['nav_nm'] ?></h4>
            <div class="row mt-n1">
              <div class="col-md-6">
                <table class="table" style="width: 100% !important; border-bottom: 1px solid #f2f2f2;">
                  <tbody>
                    <tr>
                      <td class="text-middle" width="150" style="font-weight: 500 !important;">No.Reg</td>
                      <td class="text-middle" width="20">:</td>
                      <td class="text-middle"><?= @$main['reg_id'] ?></td>
                    </tr>
                    <tr>
                      <td width="150" style="font-weight: 500 !important;">NIK</td>
                      <td width="20">:</td>
                      <td colspan="2"><?= @$main['nik'] ?></td>
                    </tr>
                    <tr>
                      <td width="150" style="font-weight: 500 !important;">No.RM / Nama Pasien</td>
                      <td width="20">:</td>
                      <td colspan="2"><?= @$main['pasien_id'] ?> / <?= @$main['pasien_nm'] ?>, <?= @$main['sebutan_cd'] ?></td>
                    </tr>
                    <tr>
                      <td width="150">Alamat Pasien</td>
                      <td width="20">:</td>
                      <td colspan="2">
                        <?= (@$main['alamat'] != '') ? @$main['alamat'] . ', ' : '' ?>
                        <?= (@$main['kelurahan'] != '') ? ucwords(strtolower(@$main['kelurahan'])) . ', ' : '' ?>
                        <?= (@$main['kecamatan'] != '') ? ucwords(strtolower(@$main['kecamatan'])) . ', ' : '' ?>
                        <?= (@$main['kabupaten'] != '') ? ucwords(strtolower(@$main['kabupaten'])) . ', ' : '' ?>
                        <?= (@$main['provinsi'] != '') ? ucwords(strtolower(@$main['provinsi'])) : '' ?>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col-md-6">
                <table class="table" style="width: 100% !important; border-bottom: 1px solid #f2f2f2;">
                  <tbody>
                    <tr>
                      <td width="150">Dokter PJ</td>
                      <td width="20">:</td>
                      <td><?= @$main['pegawai_nm'] ?></td>
                    </tr>
                    <tr>
                      <td width="150">Umur / JK</td>
                      <td width="20">:</td>
                      <td><?= @$main['umur_thn'] ?> Th <?= @$main['umur_bln'] ?> Bl <?= @$main['umur_hr'] ?> Hr / <?= get_parameter_value('sex_cd', @$main['sex_cd']) ?></td>
                    </tr>
                    <tr>
                      <td width="150">Jenis / Asal Pasien</td>
                      <td width="20">:</td>
                      <td><?= @$main['jenispasien_nm'] ?> / <?= get_parameter_value('asalpasien_cd', @$main['asalpasien_cd']) ?></td>
                    </tr>
                    <tr>
                      <td width="150">Tgl.Registrasi</td>
                      <td width="20">:</td>
                      <td><?= to_date(@$main['tgl_registrasi'], '', 'full_date') ?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <div class="row mt-2 mb-n3">
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Tgl.Transaksi <span class="text-danger">*<span></label>
                  <div class="col-lg-6 col-md-4">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datetimepicker" name="tgl_transaksi" id="tgl_transaksi" value="<?php if (@$main['tgl_transaksi']) {
                                                                                                                              echo to_date(@$main['tgl_transaksi'], '-', 'full_date');
                                                                                                                            } else {
                                                                                                                              echo date('d-m-Y H:i:s');
                                                                                                                            } ?>" required disabled>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">ID.Billing <span class="text-danger">*<span></label>
                  <div class="col-lg-6 col-md-4">
                    <input type="text" class="form-control" name="billing_id" id="billing_id" value="<?= (@$main['billing_id'] != '') ? @$main['billing_id'] : 'otomatis' ?>" required disabled>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">No. Form <span class="text-danger">*<span></label>
                  <div class="col-lg-6 col-md-4">
                    <input type="text" class="form-control" name="no_invoice" id="no_invoice" value="<?= @$main['no_invoice'] ?>" required disabled>
                  </div>
                </div>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-7">
                <div class="card border-none no-shadow">
                  <div class="card-body">
                    <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-file"></i> Rincian Pembayaran</h4>
                    <div class="table-responsive mt-n1">
                      <table class="table table-hover table-bordered table-fixed">
                        <thead>
                          <tr>
                            <th class="text-center text-middle" rowspan="2" width="30">NO</th>
                            <th class="text-center text-middle" rowspan="2">JENIS ITEM PEMBAYARAN</th>
                            <th class="text-center" colspan="3" width="375">BIAYA</th>
                            <th class="text-center text-middle" rowspan="2" width="125">TOTAL</th>
                          </tr>
                          <tr>
                            <th class="text-center" width="125">SEHARUSNYA</th>
                            <th class="text-center" width="125">POTONGAN</th>
                            <th class="text-center" width="125">TAGIHAN</th>
                          </tr>
                        </thead>
                        <tbody style="height: 100% !important;" id="rincian_pembayaran">
                        </tbody>
                      </table>
                    </div>
                    <div class="row">
                      <div class="col-md-4 text-left">
                      </div>
                      <div class="col-md-8 text-right">
                        <a href="javascript:void(0)" data-href="<?= site_url() . '/keuangan/kasir/sudah_bayar/detail_obat/' . @$main['reg_id'] . '?groupreg_in=' . $groupreg_in ?>" modal-title="Detail Biaya Obat" modal-size="lg" modal-content-top="-75px" class="btn btn-form-control btn-primary modal-href mt-2"><i class="fas fa-pills"></i> Detail Biaya Obat</a>
                        <a href="javascript:void(0)" data-href="<?= site_url() . '/keuangan/kasir/sudah_bayar/detail_alkes/' . @$main['reg_id'] . '?groupreg_in=' . $groupreg_in ?>" modal-title="Detail Biaya Alkes" modal-size="lg" modal-content-top="0px" class="btn btn-form-control btn-primary modal-href mt-2"><i class="fas fa-syringe"></i></i> Detail Biaya Alkes</a>
                        <a href="javascript:void(0)" data-href="<?= site_url() . '/keuangan/kasir/sudah_bayar/detail_bhp/' . @$main['reg_id'] . '?groupreg_in=' . $groupreg_in ?>" modal-title="Detail Biaya BHP" modal-size="lg" modal-content-top="0px" class="btn btn-form-control btn-primary modal-href mt-2"><i class="fas fa-box-open"></i></i> Detail Biaya BHP</a>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="mt-3 mb-3">
                          <?php if (count(@$list_resep) == 0) : ?>
                            <div class="font-weight-bold">Resep : Tidak Ada Resep</div>
                          <?php else : ?>
                            <?php $no = 1;
                            foreach ($list_resep as $resep) :
                              if ($resep['resep_st'] == 2) {
                                $resep_st = '<span class="text-success">Selesai Diproses Farmasi</span>';
                              } elseif ($resep['resep_st'] == 1) {
                                $resep_st = '<span class="text-warning">Sedang Diproses Farmasi</span>';
                              } else {
                                $resep_st = '<span class="text-danger">Belum Diproses Farmasi</span>';
                              }
                            ?>
                              <div class="font-weight-bold"><?= $no++ ?>. Resep : <?= $resep['resep_id'] ?> | Status Resep : <?= @$resep_st ?></div>
                            <?php endforeach; ?>
                          <?php endif; ?>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-5">
                <div class="card border-none no-shadow">
                  <div class="card-body">
                    <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-file"></i> Resume Pembayaran</h4>
                    <div class="form-group row mt-n2">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Total Biaya</label>
                      <div class="col-lg-4 col-md-4">
                        <input type="text" class="form-control text-right" name="grand_total_bruto" id="grand_total_bruto" value="<?= num_id(@$main['grand_total_bruto']) ?>" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Total Potongan</label>
                      <div class="col-lg-4 col-md-4">
                        <input type="text" class="form-control autonumeric text-right" name="grand_total_potongan" id="grand_total_potongan" value="<?= num_id(@$main['grand_total_potongan']) ?>" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Jenis Potongan</label>
                      <div class="col-lg-4 col-md-4">
                        <select class="form-control chosen-select" name="potongan_cd" id="potongan_cd" disabled="">
                          <option value="">---</option>
                          <?php foreach (get_parameter('potongan_cd') as $r) : ?>
                            <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['potongan_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row mr-2">
                      <div class="col-lg-9 col-md-9 text-right font-weight-bold">
                        ---------------------------------------------------------------- -
                      </div>
                    </div>
                    <div class="form-group row mt-1">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Total Akhir</label>
                      <div class="col-lg-4 col-md-4">
                        <input type="text" class="form-control autonumeric text-right" name="grand_total_netto" id="grand_total_netto" value="<?= num_id(@$main['grand_total_netto']) ?>" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Pembulatan</label>
                      <div class="col-lg-4 col-md-4">
                        <input type="text" class="form-control autonumeric text-right" name="grand_pembulatan" value="<?= num_id(@$main['grand_pembulatan']) ?>" id="grand_pembulatan" value="" disabled>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-lg-5 col-md-5 col-form-label text-left font-weight-bold">Jumlah Yang Harus Dibayar</label>
                      <div class="col-lg-4 col-md-4">
                        <input type="text" class="form-control form-control form-control-lg font-weight-bold mb-2 text-success autonumeric text-right" style="font-size: 14px;" name="grand_total_tagihan" id="grand_total_tagihan" value="<?= num_id(@$main['grand_total_tagihan']) ?>" disabled>
                      </div>
                    </div>
                    <div class="border-bottom border-2 mb-2 mt-1"></div>
                    <div class="form-group row">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Status Bayar</label>
                      <div class="col-lg-5 col-md-5">
                        <select class="form-control form-control-sm chosen-select" name="bayar_st" id="bayar_st" disabled="">
                          <option value="1" <?php if (@$main['bayar_st'] == 1) {
                                              echo 'selected';
                                            } ?>>Lunas</option>
                          <option value="2" <?php if (@$main['bayar_st'] == 2) {
                                              echo 'selected';
                                            } ?>>Titip</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-group row">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Tanggal Bayar</label>
                      <div class="col-lg-5 col-md-5">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datetimepicker" name="tgl_bayar" id="tgl_bayar" value="<?php if (@$main['tgl_bayar']) {
                                                                                                                          echo to_date(@$main['tgl_bayar'], '-', 'full_date');
                                                                                                                        } else {
                                                                                                                          echo date('d-m-Y H:i:s');
                                                                                                                        } ?>" required disabled>
                        </div>
                      </div>
                    </div>
                    <div class="form-group row <?= (@$main['billing_id'] == '') ? '' : 'd-none' ?>" id="box_lunas">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Jumlah Dibayar</label>
                      <div class="col-lg-4 col-md-4">
                        <?php if (@$main['bayar_st'] == '1') : ?>
                          <input type="text" class="form-control autonumeric text-right" name="grand_total_bayar" id="grand_total_bayar" value="<?= num_id(@$main['grand_total_bayar']) ?>" readonly="">
                        <?php else : ?>
                          <?php if (@$main['jenisreg_st'] == 1) : ?>
                            <!-- Pasien Rawat Jalan -->
                            <?php if (@$main['penjamin_cd'] == 'AS' && @$main['penjamin_nm'] == 'BPJS') : ?>
                              <!-- Jika Pasien BPJS -->
                              <input type="text" class="form-control autonumeric text-right" name="grand_total_bayar" id="grand_total_bayar" value="0" readonly="">
                            <?php else : ?>
                              <input type="text" class="form-control autonumeric text-right" name="grand_total_bayar" id="grand_total_bayar" readonly="">
                            <?php endif; ?>
                          <?php elseif (@$main['is_ranap'] == 1) : ?>
                            <!-- Pasien Rawat Inap -->
                            <?php if (@$main['penjamin_cd'] == 'AS' && @$main['penjamin_nm'] == 'BPJS' && @$main['status_ranap_cd'] == 'S') : ?>
                              <!-- Jika Pasien BPJS & Status Ranap Sesuai -->
                              <input type="text" class="form-control autonumeric text-right" name="grand_total_bayar" id="grand_total_bayar" value="0" readonly="">
                            <?php else : ?>
                              <input type="text" class="form-control autonumeric text-right" name="grand_total_bayar" id="grand_total_bayar" readonly="">
                            <?php endif; ?>
                          <?php else : ?>
                            <input type="text" class="form-control autonumeric text-right" name="grand_total_bayar" id="grand_total_bayar" readonly="">
                          <?php endif; ?>
                        <?php endif; ?>
                      </div>
                    </div>
                    <div class="form-group row <?= (@$main['billing_id'] == '') ? '' : 'd-none' ?>" id="box_kembalian">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Kembalian</label>
                      <div class="col-lg-4 col-md-4">
                        <input type="text" class="form-control autonumeric text-right" id="kembalian" readonly="">
                      </div>
                    </div>
                    <div class="form-group row <?= (@$main['bayar_st'] == 2) ? '' : 'd-none' ?>" id="box_titip">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Jumlah Titip</label>
                      <div class="col-lg-4 col-md-4">
                        <input type="text" class="form-control autonumeric text-right" name="grand_titip_bayar" value="<?= num_id(@$main['grand_titip_bayar']) ?>" readonly="">
                      </div>
                    </div>
                    <div class="form-group row d-none" id="box_pelunasan">
                      <label class="col-lg-5 col-md-5 col-form-label text-left">Jumlah Pelunasan</label>
                      <div class="col-lg-4 col-md-4">
                        <input type="text" class="form-control autonumeric text-right" name="grand_total_bayar" value="<?= num_id(@$main['grand_total_bayar']) ?>" readonly="">
                      </div>
                    </div>
                    <h4 class="card-title border-bottom border-2 pt-2 pb-2 mb-3"><i class="fas fa-file"></i> Catatan Pembayaran</h4>
                    <div class="form-group row mt-n1">
                      <div class="col-12">
                        <textarea class="form-control" name="keterangan_bayar" rows="4" disabled=""><?= @$main['keterangan_bayar'] ?></textarea>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="btn-form">
    <div class="btn-form-action">
      <div class="btn-form-action-bottom w-100 small-text clearfix">
        <div class="col-lg-2">
          <a href="<?= site_url() . '/keuangan/kasir/sudah_bayar' ?>" class="btn btn-xs btn-secondary"><i class="mdi mdi-keyboard-backspace"></i> Kembali</a>
        </div>
        <div class="col-lg-8 offset-2">
          <a href="#" data-href="<?= site_url() . '/keuangan/kasir/sudah_bayar/cetak_modal/cetak_obat/' . @$main['groupreg_id'] . '?groupreg_in=' . $groupreg_in ?>" modal-title="Cetak Rincian Obat" modal-size="lg" modal-content-top="-75px" class="float-right btn btn-xs btn-primary btn-submit mr-2 modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cetak Rincian Obat"><i class="fas fa-pills"></i> Cetak Rincian Obat</a>
          <a href="#" data-href="<?= site_url() . '/keuangan/kasir/sudah_bayar/cetak_modal/cetak_tindakan/' . @$main['groupreg_id'] . '?groupreg_in=' . $groupreg_in ?>" modal-title="Cetak Rincian Tindakan" modal-size="lg" modal-content-top="-75px" class="float-right btn btn-xs btn-primary btn-submit mr-2 modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cetak Rincian Tindakan"><i class="fas fa-procedures"></i> Cetak Rincian Tindakan</a>
          <?php if (@$main['bayar_st'] == '1' && @$main['jenisreg_st'] == 1) : ?>
            <a href="#" data-href="<?= site_url() . '/keuangan/kasir/sudah_bayar/cetak_modal/cetak_kwitansi/' . @$main['groupreg_id'] . '?groupreg_in=' . $groupreg_in ?>" modal-title="Cetak Kwitansi" modal-size="lg" modal-content-top="-75px" class="float-right btn btn-xs btn-primary btn-submit mr-2 modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cetak Kwitansi"><i class="fas fa-print"></i> Cetak Kwitansi</a>
          <?php endif; ?>
          <?php if (@$main['is_ranap'] == 1) : ?>
            <a href="#" data-href="<?= site_url() . '/keuangan/kasir/sudah_bayar/cetak_modal/cetak_invoice/' . @$main['groupreg_id'] . '?groupreg_in=' . $groupreg_in ?>" modal-title="Cetak Invoice" modal-size="lg" modal-content-top="-75px" class="float-right btn btn-xs btn-primary btn-submit mr-2 modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cetak Invoice"><i class="fas fa-print"></i> Cetak Invoice</a>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</form>