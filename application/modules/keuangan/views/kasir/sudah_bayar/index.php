<!-- js -->
<?php $this->load->view('_js_index') ?>
<!-- / -->
<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline col-title">
        <div class="col-back">
          <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
        </div>
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          <?= $nav['nav_nm'] ?>
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item"><a href="#"><i class="fas fa-cash-register menu-icon"></i> Kasir</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url('keuangan/kasir/sudah_bayar') ?>"><?= $nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item active"><span>Index</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>
  <!-- flash message -->
  <div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
  <!-- /flash message -->
  <div class="row full-page mt-4 mb-n2">
    <div class="col-lg-12 grid-margin-xl-0 stretch-card">
      <div class="card border-none">
        <div class="card-body">
          <div class="row mt-n3 mb-1">
            <div class="col-md-12 mt-3">
              <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link tab-menu" href="<?= site_url('keuangan/kasir/belum_bayar') ?>"><i class="fas fa-times-circle"></i> BELUM BAYAR</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link tab-menu active" href="<?= site_url('keuangan/kasir/sudah_bayar') ?>"><i class="fas fa-check-circle"></i> SUDAH BAYAR</a>
                </li>
              </ul>
            </div>
          </div>
          <form id="form-search" action="<?= site_url() . '/app/search_redirect/' . $nav['nav_id'] . '.sudah' ?>" method="post" autocomplete="off">
            <input type="hidden" name="redirect" value="keuangan/kasir/sudah_bayar">
            <div class="row mb-n1">
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Tgl.Registrasi</label>
                  <div class="col-lg-8 col-md-8">
                    <div class="row">
                      <div class="col-md-5" style="padding-right: 5px; flex: 0 0 46.4%; max-width: 46.4%;">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datepicker text-center" name="tgl_registrasi_from_2" id="tgl_registrasi_from_2" value="<?= @$cookie['search']['tgl_registrasi_from_2'] ?>">
                        </div>
                      </div>
                      <div class="col-md-2 text-center-group">s.d</div>
                      <div class="col-md-5" style="padding-left: 5px; flex: 0 0 46.4%; max-width: 46.4%;">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datepicker text-center" name="tgl_registrasi_to_2" id="tgl_registrasi_to_2" value="<?= @$cookie['search']['tgl_registrasi_to_2'] ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Tgl.Bayar</label>
                  <div class="col-lg-8 col-md-8">
                    <div class="row">
                      <div class="col-md-5" style="padding-right: 5px; flex: 0 0 46.4%; max-width: 46.4%;">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datepicker <?php if (@$cookie['search']['tgl_registrasi_from'] == '') {
                                                                              echo 'datepicker-empty';
                                                                            } ?> text-center" name="tgl_registrasi_from" id="tgl_registrasi_from" value="<?= @$cookie['search']['tgl_registrasi_from'] ?>">
                        </div>
                      </div>
                      <div class="col-md-2 text-center-group">s.d</div>
                      <div class="col-md-5" style="padding-left: 5px; flex: 0 0 46.4%; max-width: 46.4%;">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datepicker <?php if (@$cookie['search']['tgl_registrasi_to'] == '') {
                                                                              echo 'datepicker-empty';
                                                                            } ?> text-center" name="tgl_registrasi_to" id="tgl_registrasi_to" value="<?= @$cookie['search']['tgl_registrasi_to'] ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">No.RM/Nama Pasien</label>
                  <div class="col-lg-8 col-md-8">
                    <input type="text" class="form-control" name="no_rm_nm" id="no_rm_nm" value="<?= @$cookie['search']['no_rm_nm'] ?>">
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group row">
                  <label class="col-lg-5 col-md-5 col-form-label">Lokasi Kasir</label>
                  <div class="col-lg-7 col-md-7">
                    <select class="form-control chosen-select" name="lokasi_kasir" id="lokasi_kasir">
                      <option value="">- Semua -</option>
                      <?php foreach ($lokasi_kasir as $r) : ?>
                        <option value="<?= $r['lokasi_id'] ?>" <?= (@$cookie['search']['lokasi_kasir'] == $r['lokasi_id']) ? 'selected' : ''; ?>><?= $r['lokasi_nm'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-5 col-md-5 col-form-label">Lokasi Pelayanan</label>
                  <div class="col-lg-7 col-md-7">
                    <div id="box_lokasi_pelayanan">
                      <select class="form-control chosen-select" name="lokasi_pelayanan" id="lokasi_pelayanan">
                        <option value="">- Semua -</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Jenis Pasien</label>
                  <div class="col-lg-6 col-md-8">
                    <select class="form-control chosen-select" name="jenispasien_id" id="jenispasien_id" onchange="$('#form-search').submit()">
                      <option value="">- Semua -</option>
                      <?php foreach ($jenis_pasien as $r) : ?>
                        <option value="<?= $r['jenispasien_id'] ?>" <?= (@$cookie['search']['jenispasien_id'] == $r['jenispasien_id']) ? 'selected' : ''; ?>><?= $r['jenispasien_nm'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Jenis Rawat</label>
                  <div class="col-lg-6 col-md-8">
                    <select class="form-control chosen-select" name="is_ranap" id="is_ranap" onchange="$('#form-search').submit()">
                      <option value="">- Semua -</option>
                      <option value="rajal" <?= (@$cookie['search']['is_ranap'] == 'rajal') ? 'selected' : ''; ?>>Rawat Jalan</option>
                      <option value="ranap" <?= (@$cookie['search']['is_ranap'] == 'ranap') ? 'selected' : ''; ?>>Rawat Inap</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 row">
                <div class="col-md-6 offset-md-4 pl-1">
                  <button type="submit" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cari Data"><i class="fas fa-search"></i> Cari</button>
                  <a class="btn btn-xs btn-default" href="<?= site_url() . '/app/reset_redirect/' . $nav['nav_id'] . '.sudah/' . str_replace('/', '-', 'keuangan/kasir/sudah_bayar') ?>" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-default" title="" data-original-title="Reset"><i class="fas fa-sync-alt"></i> Reset</a>
                </div>
              </div>
            </div>
          </form>
          <div class="row mt-2">
            <div class="col-md-12">
              <div class="table-responsive">
                <form id="form-multiple" action="<?= site_url() . '/keuangan/kasir/sudah_bayar/multiple/enable' ?>" method="post">
                  <table class="table table-hover table-striped table-bordered table-fixed" style="min-width:1300px !important">
                    <thead>
                      <tr>
                        <th class="text-center" width="30">No</th>
                        <th class="text-center" width="80">Aksi</th>
                        <th class="text-center" width="80"><?= table_sort('keuangan/kasir/sudah_bayar', 'No.RM', 'pasien_id', $cookie['order']) ?></th>
                        <th class="text-center"><?= table_sort('keuangan/kasir/sudah_bayar', 'Nama Pasien', 'pasien_nm', $cookie['order']) ?></th>
                        <th class="text-center">Alamat</th>
                        <th class="text-center" width="180">Lokasi Pelayanan</th>
                        <th class="text-center" width="120">Jenis Pasien</th>
                        <th class="text-center" width="100">Jenis Rawat</th>
                        <th class="text-center" width="100">Tgl.Registrasi</th>
                        <th class="text-center" width="150">Tindakan</th>
                        <th class="text-center" width="100">Status Bayar</th>
                      </tr>
                    </thead>
                    <?php if (@$main == null) : ?>
                      <tbody>
                        <tr>
                          <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                        </tr>
                      </tbody>
                    <?php else : ?>
                      <tbody>
                        <?php $i = 1;
                        foreach ($main as $row) : ?>
                          <tr>
                            <td class="text-center" width="30"><?= $i++ ?></td>
                            <td class="text-center" width="80">
                              <div class="btn-group">
                                <button type="button" class="btn btn-primary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Aksi
                                </button>
                                <div class="dropdown-menu">
                                  <a class="dropdown-item" href="<?= site_url() . '/keuangan/kasir/belum_bayar/form/' . $row['groupreg_id'] . '?groupreg_in=' . $row['groupreg_in'] ?>"><i class="fas fa-list"></i> Detail</a>
                                  <?php if ($row['jenisreg_st'] == 1) : ?>
                                    <a href="#" data-href="<?= site_url() . '/keuangan/kasir/belum_bayar/cetak_modal/cetak_kwitansi/' . $row['groupreg_id'] . '?groupreg_in=' . $row['groupreg_in'] ?>" modal-title="Cetak Kwitansi" modal-size="lg" modal-content-top="-75px" class="dropdown-item modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cetak Kwitansi"><i class="fas fa-print"></i> Cetak Kwitansi</a>
                                  <?php elseif ($row['is_ranap'] == 1) : ?>
                                    <a href="#" data-href="<?= site_url() . '/keuangan/kasir/belum_bayar/cetak_modal/cetak_invoice/' . $row['groupreg_id'] . '?groupreg_in=' . $row['groupreg_in'] ?>" modal-title="Cetak Invoice" modal-size="lg" modal-content-top="-75px" class="dropdown-item modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cetak Invoice"><i class="fas fa-print"></i> Cetak Invoice</a>
                                  <?php endif; ?>
                                  <a href="#" data-href="<?= site_url() . '/keuangan/kasir/belum_bayar/cetak_modal/cetak_obat/' . $row['groupreg_id'] . '?groupreg_in=' . $row['groupreg_in'] ?>" modal-title="Cetak Rincian Obat" modal-size="lg" modal-content-top="-75px" class="dropdown-item modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cetak Rincian Obat"><i class="fas fa-pills"></i> Cetak Rincian Obat</a>
                                  <a href="#" data-href="<?= site_url() . '/keuangan/kasir/belum_bayar/cetak_modal/cetak_tindakan/' . $row['groupreg_id'] . '?groupreg_in=' . $row['groupreg_in'] ?>" modal-title="Cetak Rincian Tindakan" modal-size="lg" modal-content-top="-75px" class="dropdown-item modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cetak Rincian Tindakan"><i class="fas fa-procedures"></i> Cetak Rincian Tindakan</a>
                                </div>
                              </div>
                            </td>
                            <td class="text-center" width="80"><?= $row['pasien_id'] ?></td>
                            <td class="text-left"><?= $row['pasien_nm'] . ', ' . $row['sebutan_cd'] ?></td>
                            <td class="text-left">
                              <?= ($row['alamat'] != '') ? $row['alamat'] . ', ' : '' ?>
                              <?= ($row['kelurahan'] != '') ? ucwords(strtolower($row['kelurahan'])) . ', ' : '' ?>
                              <?= ($row['kecamatan'] != '') ? ucwords(strtolower($row['kecamatan'])) . ', ' : '' ?>
                              <?= ($row['kabupaten'] != '') ? ucwords(strtolower($row['kabupaten'])) . ', ' : '' ?>
                              <?= ($row['provinsi'] != '') ? ucwords(strtolower($row['provinsi'])) : '' ?>
                            </td>
                            <td class="text-center" width="180"><?= $row['lokasi_nm'] ?></td>
                            <td class="text-center" width="120"><?= $row['jenispasien_nm'] ?></td>
                            <td class="text-center" width="100">
                              <?php if ($row['is_ranap'] == '0') : ?>
                                <div class="badge badge-xs badge-info">Rawat Jalan</div>
                              <?php elseif ($row['is_ranap'] == '1') : ?>
                                <div class="badge badge-xs badge-warning">Rawat Inap</div>
                              <?php endif; ?>
                            </td>
                            <td class="text-center" width="100"><?= to_date($row['tgl_registrasi'], '', 'full_date', '<br>') ?></td>
                            <td class="text-left" width="150">
                              <?php if ($row['is_ranap'] == '0') : ?>
                                <?= (@$row['check_pendaftaran'] != null) ? '<i class="fas fa-check-circle text-success"></i>' : '<i class="fas fa-ban text-warning"></i>' ?> Pendaftaran</br>
                                <?= (@$row['check_poli'] != null) ? '<i class="fas fa-check-circle text-success"></i>' : '<i class="fas fa-ban text-warning"></i>' ?> Poliklinik</br>
                                <?php if (@$row['check_resep_exist'] == null) : ?>
                                  <i class="fas fa-times-circle text-disabled"></i> <del>Farmasi</del>
                                <?php else : ?>
                                  <?php if (@$row['check_resep']['resep_st'] == 2) : ?>
                                    <i class="fas fa-check-circle text-success"></i>
                                  <?php elseif (@$row['check_resep']['resep_st'] == 1) : ?>
                                    <i class="fas fa-sync-alt text-warning"></i>
                                  <?php else : ?>
                                    <i class="fas fa-ban text-warning"></i>
                                  <?php endif; ?>
                                  Farmasi
                                <?php endif; ?>
                              <?php elseif ($row['is_ranap'] == '1') : ?>
                                <div class="badge badge-xs badge-warning">Lihat di detail</div>
                              <?php endif; ?>
                            </td>
                            <td class="text-center" width="100">
                              <?php if ($row['bayar_st'] == '1') : ?>
                                <div class="badge badge-xs badge-primary">Sudah</div>
                              <?php elseif ($row['bayar_st'] == '2') : ?>
                                <div class="badge badge-xs badge-warning blink">Pending</div>
                              <?php else : ?>
                                <div class="badge badge-xs badge-danger blink">Belum</div>
                              <?php endif; ?>
                            </td>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                    <?php endif; ?>
                  </table>
                </form>
              </div>
            </div>
          </div><!-- /.row -->
          <div class="row mt-2">
            <div class="col-6 text-middle pt-1">
              <div class="row">
                <div class="col">
                  <div class="row mt-2 pagination-info">
                    <div class="col-md-8 pt-1">
                      <form id="form-paging" action="<?= site_url() . '/app/per_page/' . $nav['nav_id'] ?>" method="post">
                        <label><i class="fas fa-bookmark"></i> Perhalaman</label>
                        <select name="per_page" class="select-pagination" onchange="$('#form-paging').submit()">
                          <option value="10" <?php if ($cookie['per_page'] == 10) {
                                                echo 'selected';
                                              } ?>>10</option>
                          <option value="50" <?php if ($cookie['per_page'] == 50) {
                                                echo 'selected';
                                              } ?>>50</option>
                          <option value="100" <?php if ($cookie['per_page'] == 100) {
                                                echo 'selected';
                                              } ?>>100</option>
                        </select>
                        <label class="ml-2"><?= @$pagination_info ?></label>
                      </form>
                    </div>
                  </div><!-- /.row -->
                </div>
              </div>
            </div>
            <div class="col-6 text-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  $(document).ready(function() {
    $(".datepicker-empty").val("");
  });
</script>