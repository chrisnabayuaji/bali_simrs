<script type="text/javascript">
  $(document).ready(function() {
    var form = $("#form-data").validate({
      rules: {
        nik: {
          'number': true
        }
      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-n2 mb-2');
        } else if ($(element).hasClass('chosen-select-2')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-n2 mb-2');
        } else if ($(element).hasClass('form-input-group')) {
          error.insertAfter(element.next(".input-group-prepend"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    $("#update_box").hide();
    sebutancd();

    // select2
    $(".chosen-select-2").select2();
    $('.select2-container').css('width', '100%');

    $('.chosen-select').on('select2:select', function(e) {
      form.element(this);
    });

    <?php if (@$main['tgl_lahir'] == '') : ?>
      $('#tgl_lahir').val('');
    <?php endif; ?>
  });

  function sebutancd() {
    var val = $("#sebutan_cd").val();
    if (val == 'BY') {
      $("#box-anak-ke").removeClass('d-none');
    } else if (val == 'BY NY') {
      $("#box-anak-ke").removeClass('d-none');
    } else {
      $("#box-anak-ke").addClass('d-none');
    }

    if (val == 'NY' || val == 'NN') {
      $("#sex_cd").val('P').trigger('change');
    } else {
      $("#sex_cd").val('L').trigger('change');
    }
  }
</script>