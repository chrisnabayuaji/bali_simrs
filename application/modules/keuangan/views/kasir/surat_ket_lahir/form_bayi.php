<?php $this->load->view('_js_form_bayi'); ?>
<div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
<form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?= $form_action ?>" class="needs-validation" novalidate autocomplete="off">
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline col-title">
          <div class="col-back">
            <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
          </div>
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            <?= $nav['nav_nm'] ?>
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-user-injured"></i> Registrasi</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item active"><span>Form</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>

    <div class="row full-page mt-4">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <div class="row mt-1">
              <div class="col-md-7">
                <h6 class="text-primary">Identitas Pasien</h6>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Nama Bayi</label>
                  <div class="col-lg-7 col-md-6">
                    <input type="text" class="form-control" name="bayi_nm" id="bayi_nm" value="<?= @$main['bayi_nm'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Anak Ke <span class="text-danger">*<span></label>
                  <div class="col-lg-3 col-md-9">
                    <input type="text" class="form-control" name="anak_ke" id="anak_ke" value="<?= @$main['anak_ke'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Jns Kelamin <span class="text-danger">*<span></label>
                  <div class="col-lg-3 col-md-9">
                    <select class="form-control chosen-select" name="sex_cd" id="sex_cds" required>
                      <option value="">- Pilih -</option>
                      <?php foreach (get_parameter('sex_cd') as $r) : ?>
                        <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['sex_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Tempat Lahir</label>
                  <div class="col-lg-3 col-md-9">
                    <input type="text" class="form-control" name="tmp_lahir" id="tmp_lahir" value="<?= @$main['tmp_lahir'] ?>" placeholder="Tempat Lahir">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Tgl Jam Lahir <span class="text-danger">*</span></label>
                  <div class="col-lg-3 col-md-9">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datepicker" name="tgl_lahir" id="tgl_lahir" value="<?= @to_date(@$main['tgl_lahir']) ?>" placeholder="dd-mm-yyyy" required>
                    </div>
                  </div>
                  <div class="col-lg-2 col-md-9">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-clock"></i></span>
                      </div>
                      <input type="text" class="form-control" name="jam_lahir" id="jam_lahir" value="<?= @$main['jam_lahir'] ?>" placeholder="Lahir Jam" required>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Nama Ibu <span class="text-danger">*<span></label>
                  <div class="col-lg-5 col-md-9">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text" style="font-size: 12px;">NY</span>
                      </div>
                      <input type="text" class="form-control" name="ibu_nm" id="ibu_nm" value="<?= @$main['ibu_nm'] ?>" required>
                    </div>
                  </div>
                  <label class="col-lg-2 col-md-3 col-form-label">Umur Ibu <span class="text-danger">*<span></label>
                  <div class="col-lg-2 col-md-9">
                    <div class="input-group">
                      <input type="number" class="form-control form-input-group" name="ibu_umur" id="ibu_umur" value="<?= @$main['ibu_umur'] ?>" required>
                      <div class="input-group-prepend">
                        <span class="input-group-text" style="font-size: 12px;">Th</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Nama Ayah</label>
                  <div class="col-lg-5 col-md-9">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text" style="font-size: 12px;">TN</span>
                      </div>
                      <input type="text" class="form-control" name="ayah_nm" id="ayah_nm" value="<?= @$main['ayah_nm'] ?>">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Cara Persalinan <span class="text-danger">*<span></label>
                  <div class="col-lg-5 col-md-9">
                    <input type="text" class="form-control" name="persalinan" id="persalinan" value="<?= @$main['persalinan'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Berat <span class="text-danger">*<span></label>
                  <div class="col-lg-2 col-md-9">
                    <div class="input-group">
                      <input type="text" class="form-control form-input-group" name="bb" id="bb" value="<?= @$main['bb'] ?>" required>
                      <div class="input-group-prepend">
                        <span class="input-group-text" style="font-size: 12px;">Gram</span>
                      </div>
                    </div>
                  </div>
                  <label class="col-lg-2 col-md-3 col-form-label">Panjang <span class="text-danger">*<span></label>
                  <div class="col-lg-2 col-md-9">
                    <div class="input-group">
                      <input type="text" class="form-control form-input-group" name="pb" id="pb" value="<?= @$main['pb'] ?>" required>
                      <div class="input-group-prepend">
                        <span class="input-group-text" style="font-size: 12px;">Cm</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label mt-n3">Dokter Penolong Persalinan <span class="text-danger">*<span></label>
                  <div class="col-lg-5 col-md-9">
                    <select class="form-control chosen-select" name="dr_persalinan" id="dr_persalinan" required>
                      <?php foreach ($dokter as $r) : ?>
                        <option value="<?= $r['pegawai_id'] ?>" <?= (@$main['dr_persalinan'] == $r['pegawai_id']) ? 'selected' : ''; ?>><?= $r['pegawai_nm'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <input type="hidden" name="nama_kk" value="">
                <h6 class="text-primary">Domisili Pasien</h6>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Alamat</label>
                  <div class="col-lg-7 col-md-9">
                    <input type="text" class="form-control" name="alamat" id="alamat" value="<?= @$main['alamat'] ?>">
                  </div>
                  <input type="hidden" name="kode_pos" value="">
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Kabupaten <span class="text-danger">*</span></label>
                  <div class="col-lg-4 col-md-9">
                    <input type="text" class="form-control" name="kabupaten" id="kabupaten" value="<?= @$main['kabupaten'] ?>" required>
                  </div>
                </div>
              </div>
              <div class="col-lg-5">
                <div class="alert alert-info" role="alert">
                  <h4 class="alert-heading">Perhatian!</h4>
                  <p>Pastikan kolom identitas bayi sudah benar : </p>
                  <ol>
                    <li>Jenis Kelamin</li>
                    <li>Tempat Tanggal Lahir</li>
                    <li>Anak Ke</li>
                    <li>Nama Orang Tua Bayi (Nama Ibu & Ayah)</li>
                    <li>Cara Persalinan</li>
                    <li>Berat & Panjang Bayi</li>
                  </ol>
                  <hr>
                  <p class="mb-0 blink text-red font-weight-bold">*SEBELUM MENCETAK HARAP SIMPAN DATA TERLEBIH DAHULU !</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="btn-form">
    <div class="btn-form-action">
      <div class="btn-form-action-bottom w-100 small-text clearfix">
        <div class="col-lg-7 col-md-8 col-4">
        </div>
        <div class="col-lg-4 col-md-2 col-4 btn-form-clear">
          <a href="<?= site_url() . '/' . $nav['nav_url'] ?>" class="btn btn-xs btn-secondary mr-3"><i class="mdi mdi-keyboard-backspace"></i> Kembali</a>
          <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/cetak_modal/cetak_surat_kelahiran/' . $main['reg_id'] ?>" modal-title="Cetak Surat Keterangan Kelahiran" modal-size="lg" modal-content-top="-75px" class="btn btn-xs btn-primary modal-href"><i class="fas fa-print"></i> Cetak Surat Keterangan Kelahiran</a>
        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-save">
          <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>