<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline">
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          <?= $nav['nav_nm'] ?>
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item active"><span>Index</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>
  <!-- flash message -->
  <div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
  <!-- /flash message -->
  <div class="row full-page mt-4 mb-n2">
    <div class="col-lg-12 grid-margin-xl-0 stretch-card">
      <div class="card border-none">
        <div class="card-body">
          <form id="form-search" action="<?= site_url() . '/app/search/' . $nav['nav_id'] ?>" method="post" autocomplete="off">
            <h4 class="card-title border-bottom border-2 pb-2 mb-2">Filter Data
              <span class="float-right text-gray" data-toggle="collapse" data-target="#container-search" aria-expanded="false" aria-controls="container-search"><i class="fas fa-search"></i></span>
            </h4>
            <div class="row border-dotted collapse show multi-collapse mb-2" id="container-search">
              <div class="col-md-4">
                <!-- <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Tanggal Lahir</label>
                  <div class="col-lg-8 col-md-8">
                    <div class="row">
                      <div class="col-md-5" style="padding-right: 5px; flex: 0 0 46.4%; max-width: 46.4%;">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datepicker text-center" name="tgl_lahir_from" id="tgl_lahir_from" value="<?= @$cookie['search']['tgl_lahir_from'] ?>">
                        </div>
                      </div>
                      <div class="col-md-2 text-center-group">s.d</div>
                      <div class="col-md-5" style="padding-left: 5px; flex: 0 0 46.4%; max-width: 46.4%;">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datepicker text-center" name="tgl_lahir_to" id="tgl_lahir_to" value="<?= @$cookie['search']['tgl_lahir_to'] ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                </div> -->
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">No.RM/Nama Pasien</label>
                  <div class="col-lg-8 col-md-8">
                    <input type="text" class="form-control" name="no_rm_nm" id="no_rm_nm" value="<?= @$cookie['search']['no_rm_nm'] ?>">
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Lokasi</label>
                  <div class="col-lg-8 col-md-8">
                    <select class="form-control chosen-select" name="lokasi_id" id="lokasi_id">
                      <option value="">- Semua -</option>
                      <?php foreach ($lokasi as $r) : ?>
                        <?php if (in_array($r['lokasi_id'], $this->session->userdata('sess_lokasi'))) : ?>
                          <option value="<?= $r['lokasi_id'] ?>" <?= (@$cookie['search']['lokasi_id'] == $r['lokasi_id']) ? 'selected' : ''; ?>><?= $r['lokasi_nm'] ?></option>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Jenis Registrasi</label>
                  <div class="col-lg-5 col-md-8">
                    <select class="chosen-select custom-select w-100" name="jenisreg_st">
                      <option value="">- Semua -</option>
                      <option value="1" <?php if (@$cookie['search']['jenisreg_st'] == '1') echo 'selected' ?>>Rawat Jalan</option>
                      <option value="2" <?php if (@$cookie['search']['jenisreg_st'] == '2') echo 'selected' ?>>Rawat Inap</option>
                      <option value="3" <?php if (@$cookie['search']['jenisreg_st'] == '3') echo 'selected' ?>>Gawat Darurat</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-2">
                <button type="submit" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cari Data"><i class="fas fa-search"></i> Cari</button>
                <a class="btn btn-xs btn-default" href="<?= site_url() . '/app/reset/' . $nav['nav_id'] ?>" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-default" title="" data-original-title="Reset"><i class="fas fa-sync-alt"></i> Reset</a>
              </div>
            </div>
          </form>
          <div class="row">
            <div class="col-md-12 mt-2">
              <div class="table-responsive">
                <form id="form-multiple" action="<?= site_url() . '/' . $nav['nav_url'] . '/multiple/enable' ?>" method="post">
                  <table class="table table-hover table-striped table-bordered table-fixed">
                    <thead>
                      <tr>
                        <th class="text-center" width="36">No</th>
                        <th class="text-center" width="70">Aksi</th>
                        <th class="text-center" width="80"><?= table_sort($nav['nav_id'], 'No. RM', 'pasien_id', $cookie['order']) ?></th>
                        <th class="text-center"><?= table_sort($nav['nav_id'], 'Nama Pasien', 'pasien_nm', $cookie['order']) ?></th>
                        <th class="text-center">Alamat</th>
                        <th class="text-center" width="30">JK</th>
                        <th class="text-center" width="150"><?= table_sort($nav['nav_id'], 'Jenis Pasien', 'jenispasien_id', $cookie['order']) ?></th>
                        <th class="text-center" width="150">Lokasi & Kelas</th>
                        <th class="text-center" width="115">Tgl. / Jam Lahir</th>
                        <th class="text-center" width="70">Periksa</th>
                        <th class="text-center" width="70">Pulang</th>
                      </tr>
                    </thead>
                    <?php if (@$main == null) : ?>
                      <tbody>
                        <tr>
                          <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                        </tr>
                      </tbody>
                    <?php else : ?>
                      <tbody>
                        <?php $i = 1;
                        foreach ($main as $row) : ?>
                          <tr>
                            <td class="text-center" width="36"><?= $cookie['cur_page'] + ($i++) ?></td>
                            <td class="text-center" width="70">
                              <a href="<?= site_url() . '/' . $nav['nav_url'] . '/form_bayi/' . $row['reg_id'] ?>" class="btn btn-primary btn-table"><i class="fas fa-edit"></i> Edit</a>
                              <!-- <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/cetak_modal/cetak_surat_kelahiran/' . $row['reg_id'] ?>" modal-title="Cetak Surat Keterangan Kelahiran" modal-size="lg" modal-content-top="-75px" class="btn btn-primary btn-table modal-href"><i class="fas fa-print"></i> Cetak</a> -->
                            </td>
                            <td class="text-center" width="80"><?= $row['pasien_id'] ?></td>
                            <td class="text-left">
                              <?= $row['pasien_nm'] . ', ' . $row['sebutan_cd'] ?> <br>
                              <b>Anak Ke : <?= (@$row['anak_ke'] != '') ? @$row['anak_ke'] : '-' ?></b>
                            </td>
                            <td class="text-left">
                              <?= ($row['alamat'] != '') ? $row['alamat'] . ', ' : '' ?>
                              <?= ($row['kelurahan'] != '') ? ucwords(strtolower($row['kelurahan'])) . ', ' : '' ?>
                              <?= ($row['kecamatan'] != '') ? ucwords(strtolower($row['kecamatan'])) . ', ' : '' ?>
                              <?= ($row['kabupaten'] != '') ? ucwords(strtolower($row['kabupaten'])) . ', ' : '' ?>
                              <?= ($row['provinsi'] != '') ? ucwords(strtolower($row['provinsi'])) : '' ?>
                            </td>
                            <td class="text-center" width="30"><?= $row['sex_cd'] ?></td>
                            <td class="text-center" width="150"><?= $row['jenispasien_nm'] ?> <br> <?= $row['no_kartu'] ?> <?= ($row['sep_no'] != '') ? '<br>' . $row['sep_no'] : '' ?></td>
                            <td class="text-center" width="150"><?= $row['lokasi_nm'] ?> <br> <?= get_kelas_nm($row['kelas_nm']) ?></td>
                            <td class="text-center" width="115"><?= to_date($row['tgl_lahir']) ?> <br> <?= $row['jam_lahir'] ?> WIB</td>
                            <td class="text-center" width="70">
                              <?php if ($row['periksa_st'] == '2') : ?>
                                <div class="badge badge-xs badge-success">&nbsp; Selesai &nbsp;</div>
                              <?php elseif ($row['periksa_st'] == '1') : ?>
                                <div class="badge badge-xs badge-success">&nbsp; Sudah &nbsp;</div>
                              <?php else : ?>
                                <div class="badge badge-xs badge-danger">&nbsp; Belum &nbsp;</div>
                              <?php endif; ?>
                            </td>
                            <td class="text-center" width="70">
                              <?php if ($row['pulang_st'] == '1') : ?>
                                <div class="badge badge-xs badge-success">&nbsp; Sudah &nbsp;</div>
                              <?php else : ?>
                                <div class="badge badge-xs badge-danger">&nbsp; Belum &nbsp;</div>
                              <?php endif; ?>
                            </td>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                    <?php endif; ?>
                  </table>
                </form>
              </div>
            </div>
          </div><!-- /.row -->
          <div class="row mt-2">
            <div class="col-6 text-middle">
              <div class="row">
                <div class="col">
                  <div class="row pagination-info">
                    <div class="col-md-8" style="padding-top:3px !important">
                      <form id="form-paging" action="<?= site_url() . '/app/per_page/' . $nav['nav_id'] ?>" method="post">
                        <label><i class="fas fa-bookmark"></i> Perhalaman</label>
                        <select name="per_page" class="select-pagination" onchange="$('#form-paging').submit()">
                          <option value="10" <?php if ($cookie['per_page'] == 10) {
                                                echo 'selected';
                                              } ?>>10</option>
                          <option value="50" <?php if ($cookie['per_page'] == 50) {
                                                echo 'selected';
                                              } ?>>50</option>
                          <option value="100" <?php if ($cookie['per_page'] == 100) {
                                                echo 'selected';
                                              } ?>>100</option>
                        </select>
                        <label class="ml-2"><?= @$pagination_info ?></label>
                      </form>
                    </div>
                  </div><!-- /.row -->
                </div>
              </div>
            </div>
            <div class="col-6 text-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>