<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pengeluaran_bhp_alkes extends MY_Controller
{

  var $nav_id = '12.04.02', $nav, $cookie;

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'farmasi/m_pengeluaran_bhp_alkes',
      'master/m_lokasi',
    ));

    $this->nav = $this->m_app->_get_nav($this->nav_id);

    //cookie
    $this->cookie = get_cookie_nav($this->nav_id . '.pengeluaranbhpalkes');
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('bulan' => '', 'tahun' => '', 'jenisreg_st' => '', 'lokasi_id' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => '', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
  }

  public function index()
  {
    $this->authorize($this->nav, '_view');
    //main data
    $data['nav'] = $this->nav;
    if ($this->cookie['search']['bulan'] == '' && $this->cookie['search']['tahun'] == '') {
      $data['get_lokasi'] = array();
    } else {
      $data['get_lokasi'] = $this->m_lokasi->get_data(@$this->cookie['search']['lokasi_id']);
    }
    //cookie
    set_cookie_nav($this->nav_id . '.pengeluaranbhpalkes', $this->cookie);
    $data['cookie'] = $this->cookie;
    $data['bulan'] = list_bulan();
    //set pagination
    set_pagination($this->nav, $this->cookie);
    //render
    create_log('_view', $this->nav_id);
    $this->render('laporan/farmasi/pengeluaran_bhp_alkes/index', $data);
  }

  function list_data()
  {
    if ($this->cookie['search']['bulan'] == '' && $this->cookie['search']['tahun'] == '') {
      $data['main'] = array();
    } else {
      $data['main'] = $this->m_pengeluaran_bhp_alkes->list_data($this->cookie);
    }
    $data['cookie'] = $this->cookie;
    echo json_encode(array(
      'html' => $this->load->view('laporan/farmasi/pengeluaran_bhp_alkes/list_data', $data, true)
    ));
  }

  function ajax($type = null, $id = null)
  {
    if ($type == 'get_lokasi') {
      $jenisreg_st = $this->input->post('jenisreg_st');
      $lokasi_id = $this->input->post('lokasi_id');
      $list_lokasi = $this->m_lokasi->by_field('jenisreg_st', $jenisreg_st, 'result');
      //
      $html = '';
      $html .= '<select name="lokasi_id" id="lokasi_id" class="chosen-select custom-select w-100">';
      if (count($list_lokasi) > 1 || count($list_lokasi) == 0) {
        $html .= '<option value="">- Semua -</option>';
      }
      foreach ($list_lokasi as $kelas) {
        if (@$lokasi_id == $kelas['lokasi_id']) {
          $html .= '<option value="' . $kelas['lokasi_id'] . '" selected>' . $kelas['lokasi_nm'] . '</option>';
        } else {
          $html .= '<option value="' . $kelas['lokasi_id'] . '">' . $kelas['lokasi_nm'] . '</option>';
        }
      }
      $html .= '</select>';
      $html .= js_chosen();
      //
      echo json_encode(array(
        'html' => $html,
      ));
    }
  }

  function cetak_excel()
  {
    ini_set('memory_limit', '-1');
    // Header
    $main = $this->m_pengeluaran_bhp_alkes->list_data($this->cookie);
    if (@$this->cookie['search']['lokasi_id'] != '') {
      $get_lokasi = $this->m_lokasi->get_data(@$this->cookie['search']['lokasi_id']);
    }
    //
    //Configuration -------------------------------------------------------------------------
    $this->load->file(APPPATH . 'libraries/PHPExcel.php');
    $master_cetak = BASEPATH . 'master_cetak/pengeluaran_bhp_alkes.xlsx';
    $PHPExcel = PHPExcel_IOFactory::load($master_cetak);

    // sheet 1
    $PHPExcel->setActiveSheetIndex(0);

    //align center
    $align_center = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      )
    );
    //align left
    $align_left = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
      )
    );
    //align right
    $align_right = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
      )
    );
    //font bold
    $font_bold = array(
      'font' => array(
        'bold'  => 'bold'
      )
    );
    //font underline
    $font_underline = array(
      'font' => array(
        'underline'  => 'underline'
      )
    );
    //font size 12
    $font_size_12 = array(
      'font' => array(
        'size'  => 12
      )
    );
    //wrap text
    $wrap_text = array(
      'alignment' => array(
        'wrap' => 'wrap',
      )
    );
    //create border
    $styleArray = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('rgb' => '111111'),
        ),
      ),
      'font' => array(
        'size'  => 11,
        'name'  => 'Times New Roman'
      ),
    );
    //Header
    $PHPExcel->getActiveSheet()->setCellValue("A1", 'LAPORAN PENGELUARAN BHP/ALKES');
    $PHPExcel->getActiveSheet()->setCellValue("A2", 'BULAN ' . strtoupper(get_bulan($this->cookie['search']['bulan'])) . ' ' . $this->cookie['search']['tahun']);
    if (@$this->cookie['search']['jenisreg_st'] != '') {
      if (@$this->cookie['search']['lokasi_id'] != '') {
        $PHPExcel->getActiveSheet()->setCellValue("A3", strtoupper(get_parameter_value('jenisreg_st', $this->cookie['search']['jenisreg_st'])) . ' - ' . strtoupper($get_lokasi['lokasi_nm']));
      } else {
        $PHPExcel->getActiveSheet()->setCellValue("A3", strtoupper(get_parameter_value('jenisreg_st', $this->cookie['search']['jenisreg_st'])));
      }
    } else {
      $PHPExcel->getActiveSheet()->setCellValue("A3", 'SEMUA LOKASI REGISTRASI');
    }

    // Body
    $i = 7;
    $no = 1;
    $tot_harga_beli = 0;
    $tot_harga_jual = 0;
    $tot_laba = 0;
    foreach ($main as $row) {
      $tot_harga_beli += $row['harga_beli'];
      $tot_harga_jual += $row['harga_jual'];
      $tot_laba += $row['laba'];
      //
      $PHPExcel->getActiveSheet()->setCellValue("A$i", $no++);
      $PHPExcel->getActiveSheet()->setCellValue("B$i", $row['obat_nm']);
      $abj = 'C';
      for ($for_i = 1; $for_i <= 31; $for_i++) {
        $num = $for_i;
        $PHPExcel->getActiveSheet()->setCellValue("$abj$i", ' ' . num_id($row['tgl_' . $num]));
        $abj++;
      }
      $PHPExcel->getActiveSheet()->setCellValue("AH$i", ' ' . num_id($row['total']));
      $PHPExcel->getActiveSheet()->setCellValue("AI$i", ' ' . num_id($row['harga_beli']));
      $PHPExcel->getActiveSheet()->setCellValue("AJ$i", ' ' . num_id($row['harga_jual']));
      $PHPExcel->getActiveSheet()->setCellValue("AK$i", ' ' . num_id($row['laba']));
      //
      $i++;
    }

    // Total
    $PHPExcel->getActiveSheet()->setCellValue("A$i", 'TOTAL');
    $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($align_center);
    $PHPExcel->getActiveSheet()->mergeCells("A$i:AH$i");

    $PHPExcel->getActiveSheet()->setCellValue("AI$i", 'Rp. ' . num_id($tot_harga_beli));
    $PHPExcel->getActiveSheet()->getStyle("AI$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("AI$i")->applyFromArray($align_right);

    $PHPExcel->getActiveSheet()->setCellValue("AJ$i", 'Rp. ' . num_id($tot_harga_jual));
    $PHPExcel->getActiveSheet()->getStyle("AJ$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("AJ$i")->applyFromArray($align_right);

    $PHPExcel->getActiveSheet()->setCellValue("AK$i", 'Rp. ' . num_id($tot_laba));
    $PHPExcel->getActiveSheet()->getStyle("AK$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("AK$i")->applyFromArray($align_right);

    // Creating Border
    $last_cell = "AK" . ($i);
    $PHPExcel->getActiveSheet()->getStyle("A6:$last_cell")->applyFromArray($styleArray);

    // Save it as file ------------------------------------------------------------------
    $file_export = 'laporan-pengeluaran-bhp-alkes-' . strtolower(get_bulan($this->cookie['search']['bulan'])) . '-' . $this->cookie['search']['tahun'];
    //
    ob_end_clean();
    header('Content-Type: application/vnd.ms-excel2007');
    header('Content-Disposition: attachment; filename="' . $file_export . '.xlsx"');
    $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    //-----------------------------------------------------------------------------------
  }
}
