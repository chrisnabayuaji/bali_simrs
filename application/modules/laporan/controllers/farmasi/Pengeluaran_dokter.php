<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pengeluaran_dokter extends MY_Controller
{

  var $nav_id = '12.04.03', $nav, $cookie;

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'farmasi/m_pengeluaran_dokter',
      'master/m_lokasi',
      'master/m_pegawai',
    ));

    $this->nav = $this->m_app->_get_nav($this->nav_id);

    //cookie
    $this->cookie = get_cookie_nav($this->nav_id . '.pengeluarandokter');
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('bulan' => '', 'tahun' => '', 'dokter_id' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => '', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
  }

  public function index()
  {
    $this->authorize($this->nav, '_view');
    //main data
    $data['nav'] = $this->nav;
    //cookie
    set_cookie_nav($this->nav_id . '.pengeluarandokter', $this->cookie);
    $data['cookie'] = $this->cookie;
    $data['bulan'] = list_bulan();
    $data['dokter'] = $this->m_pegawai->list_dokter();
    $data['get_dokter'] = $this->m_pegawai->get_data(@$this->cookie['search']['dokter_id']);
    //set pagination
    set_pagination($this->nav, $this->cookie);
    //render
    create_log('_view', $this->nav_id);
    $this->render('laporan/farmasi/pengeluaran_dokter/index', $data);
  }

  function list_data()
  {
    if ($this->cookie['search']['bulan'] == '' && $this->cookie['search']['tahun'] == '') {
      $data['main'] = array();
    } else {
      $data['main'] = $this->m_pengeluaran_dokter->list_data($this->cookie);
    }
    $data['cookie'] = $this->cookie;
    $data['get_dokter'] = $this->m_pegawai->get_data(@$this->cookie['search']['dokter_id']);
    echo json_encode(array(
      'html' => $this->load->view('laporan/farmasi/pengeluaran_dokter/list_data', $data, true)
    ));
  }

  function cetak_excel()
  {
    ini_set('memory_limit', '-1');
    // Header
    $main = $this->m_pengeluaran_dokter->list_data($this->cookie);
    $get_dokter = $this->m_pegawai->get_data(@$this->cookie['search']['dokter_id']);
    //
    //Configuration -------------------------------------------------------------------------
    $this->load->file(APPPATH . 'libraries/PHPExcel.php');
    $master_cetak = BASEPATH . 'master_cetak/pengeluaran_dokter.xlsx';
    $PHPExcel = PHPExcel_IOFactory::load($master_cetak);

    // sheet 1
    $PHPExcel->setActiveSheetIndex(0);

    //align center
    $align_center = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      )
    );
    //align left
    $align_left = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
      )
    );
    //align right
    $align_right = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
      )
    );
    //font bold
    $font_bold = array(
      'font' => array(
        'bold'  => 'bold'
      )
    );
    //font underline
    $font_underline = array(
      'font' => array(
        'underline'  => 'underline'
      )
    );
    //font size 12
    $font_size_12 = array(
      'font' => array(
        'size'  => 12
      )
    );
    //wrap text
    $wrap_text = array(
      'alignment' => array(
        'wrap' => 'wrap',
      )
    );
    //create border
    $styleArray = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('rgb' => '111111'),
        ),
      ),
      'font' => array(
        'size'  => 11,
        'name'  => 'Times New Roman'
      ),
    );
    //Header
    $PHPExcel->getActiveSheet()->setCellValue("A1", 'LAPORAN PENGELUARAN OBAT/BHP/ALKES PER DOKTER');
    $PHPExcel->getActiveSheet()->setCellValue("A2", 'BULAN ' . strtoupper(get_bulan($this->cookie['search']['bulan'])) . ' ' . $this->cookie['search']['tahun']);
    $PHPExcel->getActiveSheet()->setCellValue("A3", 'DOKTER : ' . @$get_dokter['pegawai_nm']);

    $PHPExcel->getActiveSheet()->setCellValue("C5", 'Jumlah Pengeluaran Obat/BHP/ALKES Setiap Hari Dari ' . @$get_dokter['pegawai_nm']);
    // Body
    $i = 7;
    $no = 1;
    $tot_harga_beli = 0;
    $tot_harga_jual = 0;
    $tot_laba = 0;
    foreach ($main as $row) {
      $tot_harga_beli += $row['harga_beli'];
      $tot_harga_jual += $row['harga_jual'];
      $tot_laba += $row['laba'];
      //
      $PHPExcel->getActiveSheet()->setCellValue("A$i", $no++);
      $PHPExcel->getActiveSheet()->setCellValue("B$i", $row['obat_nm']);
      $abj = 'C';
      for ($for_i = 1; $for_i <= 31; $for_i++) {
        $num = $for_i;
        $PHPExcel->getActiveSheet()->setCellValue("$abj$i", ' ' . num_id($row['tgl_' . $num]));
        $abj++;
      }
      $PHPExcel->getActiveSheet()->setCellValue("AH$i", ' ' . num_id($row['total']));
      $PHPExcel->getActiveSheet()->setCellValue("AI$i", ' ' . num_id($row['harga_beli']));
      $PHPExcel->getActiveSheet()->setCellValue("AJ$i", ' ' . num_id($row['harga_jual']));
      $PHPExcel->getActiveSheet()->setCellValue("AK$i", ' ' . num_id($row['laba']));
      //
      $i++;
    }

    // Total
    $PHPExcel->getActiveSheet()->setCellValue("A$i", 'TOTAL');
    $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($align_center);
    $PHPExcel->getActiveSheet()->mergeCells("A$i:AH$i");

    $PHPExcel->getActiveSheet()->setCellValue("AI$i", 'Rp. ' . num_id($tot_harga_beli));
    $PHPExcel->getActiveSheet()->getStyle("AI$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("AI$i")->applyFromArray($align_right);

    $PHPExcel->getActiveSheet()->setCellValue("AJ$i", 'Rp. ' . num_id($tot_harga_jual));
    $PHPExcel->getActiveSheet()->getStyle("AJ$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("AJ$i")->applyFromArray($align_right);

    $PHPExcel->getActiveSheet()->setCellValue("AK$i", 'Rp. ' . num_id($tot_laba));
    $PHPExcel->getActiveSheet()->getStyle("AK$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("AK$i")->applyFromArray($align_right);

    // Creating Border
    $last_cell = "AK" . ($i);
    $PHPExcel->getActiveSheet()->getStyle("A6:$last_cell")->applyFromArray($styleArray);

    // Save it as file ------------------------------------------------------------------
    $file_export = 'laporan-pengeluaran-per-dokter-' . strtolower(get_bulan($this->cookie['search']['bulan'])) . '-' . $this->cookie['search']['tahun'];
    //
    ob_end_clean();
    header('Content-Type: application/vnd.ms-excel2007');
    header('Content-Disposition: attachment; filename="' . $file_export . '.xlsx"');
    $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    //-----------------------------------------------------------------------------------
  }
}
