<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Harian_rajal extends MY_Controller
{

  var $nav_id = '12.05.01', $nav, $cookie;

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'kasir/m_harian_rajal',
      'master/m_lokasi',
    ));

    $this->nav = $this->m_app->_get_nav($this->nav_id);

    //cookie
    $this->cookie = get_cookie_nav($this->nav_id . '.harianrajal');
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('tgl' => '', 'lokasi_id' => '', 'jenispasien_id' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => '', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
  }

  public function index()
  {
    $this->authorize($this->nav, '_view');
    //main data
    $data['nav'] = $this->nav;
    $data['list_lokasi'] = $this->m_lokasi->by_field('jenisreg_st', '1', 'result');
    $data['get_lokasi'] = $this->m_lokasi->by_field('lokasi_id', $this->cookie['search']['lokasi_id'], 'row');
    //cookie
    set_cookie_nav($this->nav_id . '.harianrajal', $this->cookie);
    $data['cookie'] = $this->cookie;
    $data['bulan'] = list_bulan();
    //set pagination
    set_pagination($this->nav, $this->cookie);
    //render
    create_log('_view', $this->nav_id);
    $this->render('laporan/kasir/harian_rajal/index', $data);
  }

  function list_data()
  {
    if ($this->cookie['search']['tgl'] == '') {
      $data['main'] = array();
    } else {
      $data['main'] = $this->m_harian_rajal->list_data($this->cookie);
    }
    $data['cookie'] = $this->cookie;
    echo json_encode(array(
      'html' => $this->load->view('laporan/kasir/harian_rajal/list_data', $data, true)
    ));
  }

  public function cetak_modal($type = null, $id = null)
  {
    $data['url'] = site_url() . '/laporan/kasir/harian_rajal/' . $type;

    echo json_encode(array(
      'html' => $this->load->view('laporan/kasir/harian_rajal/cetak_modal', $data, true)
    ));
  }

  function cetak_pdf()
  {
    $main = $this->m_harian_rajal->list_data($this->cookie);
    $get_lokasi = $this->m_lokasi->by_field('lokasi_id', $this->cookie['search']['lokasi_id'], 'row');
    //
    if (@$this->cookie['search']['jenispasien_id'] == '01') {
      $jenispasien = 'PASIEN UMUM';
    } elseif (@$this->cookie['search']['jenispasien_id'] == '02') {
      $jenispasien = 'PASIEN BPJS';
    } else {
      $jenispasien = '';
    }

    //
    $align_body_arr = array("C", "C", "L", "C", "L", "C", "C", "C", "R");
    $width_pot_arr = array("10", "18", "60", "11", "74", "27", "40", "40", "30");
    //
    $this->load->library('pdf');
    $pdf = new Pdf('l', 'mm', array(210, 330)); //F4
    $pdf->AliasNbPages();
    $pdf->SetTitle('Laporan Kasir Harian Rawat Jalan - ' . date('d-m-Y'));
    $pdf->AddPage();
    //
    $pdf->Cell(0, 5, '', 0, 1, 'C');
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(0, 5, 'LAPORAN KASIR HARIAN RAWAT JALAN', 0, 1, 'C');
    if (@$this->cookie['search']['lokasi_id'] != '') {
      if (@$this->cookie['search']['jenispasien_id'] != '') {
        $pdf->Cell(0, 5, 'TANGGAL ' . @$this->cookie['search']['tgl'] . ', ' . strtoupper(@$get_lokasi['lokasi_nm']) . ', ' . @$jenispasien, 0, 1, 'C');
      } else {
        $pdf->Cell(0, 5, 'TANGGAL ' . @$this->cookie['search']['tgl'] . ', ' . strtoupper(@$get_lokasi['lokasi_nm']) . ', SEMUA JENIS PASIEN', 0, 1, 'C');
      }
    } else {
      if (@$this->cookie['search']['jenispasien_id'] != '') {
        $pdf->Cell(0, 5, 'TANGGAL ' . @$this->cookie['search']['tgl'] . ', SEMUA POLI, ' . @$jenispasien, 0, 1, 'C');
      } else {
        $pdf->Cell(0, 5, 'TANGGAL ' . @$this->cookie['search']['tgl'] . ', SEMUA POLI, SEMUA JENIS PASIEN', 0, 1, 'C');
      }
    }
    $pdf->Cell(0, 5, '', 0, 1, 'L');
    //
    $pdf->SetFont('Arial', 'B', 10);
    $pdf->Cell(10, 7, 'No', 1, 0, 'C');
    $pdf->Cell(18, 7, 'No.RM', 1, 0, 'C');
    $pdf->Cell(60, 7, 'Nama Pasien', 1, 0, 'C');
    $pdf->Cell(11, 7, 'JK', 1, 0, 'C');
    $pdf->Cell(74, 7, 'Alamat', 1, 0, 'C');
    $pdf->Cell(27, 7, 'Jenis Pasien', 1, 0, 'C');
    $pdf->Cell(40, 7, 'Nama Poli', 1, 0, 'C');
    $pdf->Cell(40, 7, 'Tgl.Registrasi', 1, 0, 'C');
    $pdf->Cell(30, 7, 'Jumlah Tagihan', 1, 0, 'C');
    //
    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell(0, 7, '', 0, 1);
    //
    $pdf->SetWidths($width_pot_arr);
    $pdf->SetHeights('5');
    $pdf->SetAligns($align_body_arr);

    $no = 1;
    $tot_tagihan = 0;
    foreach ($main as $row) {
      $tot_tagihan += $row['grand_total_tagihan'];
      $list_data = array($no, $row['pasien_id'], $row['pasien_nm'], $row['sex_cd'], $row['alamat'], $row['jenispasien_nm'], $row['lokasi_nm'], to_date($row['tgl_registrasi'], '', 'full_date'), num_id($row['grand_total_tagihan']));
      //
      $pdf->Row($list_data);
      $no++;
    }
    if (count($main) == 0) {
      $pdf->Cell(310, 7, 'Data tidak ditemukan.', 1, 0, 'C');
    } else {
      $pdf->SetFont('Arial', 'B', 9);
      $pdf->SetWidths(array("280", "30"));
      $pdf->SetHeights('5');
      $pdf->SetAligns(array("C", "R"));
      $total = array('Total', 'Rp ' . num_id($tot_tagihan));
      $pdf->Row($total);
    }
    //

    $pdf->Output('I', 'laporan-kasir-harian-rawat-jalan-' . @$this->cookie['search']['tgl'] . '.pdf');
  }

  function cetak_excel()
  {
    ini_set('memory_limit', '-1');
    // Header
    $main = $this->m_harian_rajal->list_data($this->cookie);
    $get_lokasi = $this->m_lokasi->by_field('lokasi_id', $this->cookie['search']['lokasi_id'], 'row');
    //
    if (@$this->cookie['search']['jenispasien_id'] == '01') {
      $jenispasien = 'PASIEN UMUM';
    } elseif (@$this->cookie['search']['jenispasien_id'] == '02') {
      $jenispasien = 'PASIEN BPJS';
    } else {
      $jenispasien = '';
    }
    //
    //Configuration -------------------------------------------------------------------------
    $this->load->file(APPPATH . 'libraries/PHPExcel.php');
    $master_cetak = BASEPATH . 'master_cetak/harian_rajal.xlsx';
    $PHPExcel = PHPExcel_IOFactory::load($master_cetak);

    // sheet 1
    $PHPExcel->setActiveSheetIndex(0);

    //align center
    $align_center = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      )
    );
    //align left
    $align_left = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
      )
    );
    //align right
    $align_right = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
      )
    );
    //font bold
    $font_bold = array(
      'font' => array(
        'bold'  => 'bold'
      )
    );
    //font underline
    $font_underline = array(
      'font' => array(
        'underline'  => 'underline'
      )
    );
    //font size 12
    $font_size_12 = array(
      'font' => array(
        'size'  => 12
      )
    );
    //wrap text
    $wrap_text = array(
      'alignment' => array(
        'wrap' => 'wrap',
      )
    );
    //create border
    $styleArray = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('rgb' => '111111'),
        ),
      ),
      'font' => array(
        'size'  => 11,
        'name'  => 'Times New Roman'
      ),
    );
    //Header
    $PHPExcel->getActiveSheet()->setCellValue("A6", 'LAPORAN KASIR HARIAN RAWAT JALAN');
    if (@$this->cookie['search']['lokasi_id'] != '') {
      if (@$this->cookie['search']['jenispasien_id'] != '') {
        $PHPExcel->getActiveSheet()->setCellValue("A7", 'TANGGAL ' . @$this->cookie['search']['tgl'] . ', ' . strtoupper(@$get_lokasi['lokasi_nm']) . ', ' . @$jenispasien);
      } else {
        $PHPExcel->getActiveSheet()->setCellValue("A7", 'TANGGAL ' . @$this->cookie['search']['tgl'] . ', ' . strtoupper(@$get_lokasi['lokasi_nm']) . ', SEMUA JENIS PASIEN');
      }
    } else {
      if (@$this->cookie['search']['jenispasien_id'] != '') {
        $PHPExcel->getActiveSheet()->setCellValue("A7", 'TANGGAL ' . @$this->cookie['search']['tgl'] . ', SEMUA POLI, ' . @$jenispasien);
      } else {
        $PHPExcel->getActiveSheet()->setCellValue("A7", 'TANGGAL ' . @$this->cookie['search']['tgl'] . ', SEMUA POLI, SEMUA JENIS PASIEN');
      }
    }

    // Body
    $i = 10;
    $no = 1;
    $tot_tagihan = 0;
    foreach ($main as $row) {
      $tot_tagihan += $row['grand_total_tagihan'];

      $alamat = '';
      if ($row['alamat'] != '') {
        $alamat .= $row['alamat'] . ', ';
      } else {
        $alamat .= '';
      }

      if ($row['kelurahan'] != '') {
        $alamat .= ucwords(strtolower($row['kelurahan'])) . ', ';
      } else {
        $alamat .= '';
      }

      if ($row['kecamatan'] != '') {
        $alamat .= ucwords(strtolower($row['kecamatan'])) . ', ';
      } else {
        $alamat .= '';
      }

      if ($row['kabupaten'] != '') {
        $alamat .= ucwords(strtolower($row['kabupaten'])) . ', ';
      } else {
        $alamat .= '';
      }

      if ($row['provinsi'] != '') {
        $alamat .= ucwords(strtolower($row['provinsi']));
      } else {
        $alamat .= '';
      }

      $PHPExcel->getActiveSheet()->setCellValue("A$i", $no++);
      $PHPExcel->getActiveSheet()->setCellValue("B$i", ' ' . $row['pasien_id']);
      $PHPExcel->getActiveSheet()->setCellValue("C$i", str_replace("&#039;", "'", $row["pasien_nm"]) . ', ' . $row['sebutan_cd'], true);
      $PHPExcel->getActiveSheet()->setCellValue("D$i", $row['sex_cd']);
      $PHPExcel->getActiveSheet()->setCellValue("E$i", $alamat);
      $PHPExcel->getActiveSheet()->setCellValue("F$i", $row['jenispasien_nm']);
      $PHPExcel->getActiveSheet()->setCellValue("G$i", $row['lokasi_nm']);
      $PHPExcel->getActiveSheet()->setCellValue("H$i", to_date($row['tgl_registrasi'], '', 'full_date'));
      $PHPExcel->getActiveSheet()->setCellValue("I$i", ' ' . num_id($row['grand_total_tagihan']));
      //
      $i++;
    }

    // Total
    $PHPExcel->getActiveSheet()->setCellValue("A$i", 'TOTAL');
    $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($align_center);
    $PHPExcel->getActiveSheet()->mergeCells("A$i:H$i");

    $PHPExcel->getActiveSheet()->setCellValue("I$i", 'Rp. ' . num_id($tot_tagihan));
    $PHPExcel->getActiveSheet()->getStyle("I$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("I$i")->applyFromArray($align_right);

    // Creating Border
    $last_cell = "I" . ($i);
    $PHPExcel->getActiveSheet()->getStyle("A9:$last_cell")->applyFromArray($styleArray);

    // Save it as file ------------------------------------------------------------------
    $file_export = 'laporan-kasir-harian-rawat-jalan-' . @$this->cookie['search']['tgl'];
    //
    ob_end_clean();
    header('Content-Type: application/vnd.ms-excel2007');
    header('Content-Disposition: attachment; filename="' . $file_export . '.xlsx"');
    $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    //-----------------------------------------------------------------------------------
  }
}
