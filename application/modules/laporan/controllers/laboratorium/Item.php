<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Item extends MY_Controller
{

  var $nav_id = '12.03.02', $nav, $cookie;

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'laboratorium/m_item',
    ));

    $this->nav = $this->m_app->_get_nav($this->nav_id);

    //cookie
    $this->cookie = get_cookie_nav($this->nav_id . '.item');
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('bulan' => '', 'tahun' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => '', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
  }

  public function index()
  {
    $this->authorize($this->nav, '_view');
    //main data
    $data['nav'] = $this->nav;
    if ($this->cookie['search']['bulan'] == '' && $this->cookie['search']['tahun'] == '') {
      $data['main'] = array();
    } else {
      $data['main'] = $this->m_item->list_data($this->cookie);
    }
    //cookie
    set_cookie_nav($this->nav_id . '.item', $this->cookie);
    $data['cookie'] = $this->cookie;
    $data['bulan'] = list_bulan();
    //set pagination
    set_pagination($this->nav, $this->cookie);
    //render
    create_log('_view', $this->nav_id);
    $this->render('laporan/laboratorium/item/index', $data);
  }

  function cetak_excel()
  {
    ini_set('memory_limit', '-1');
    // Header
    $main = $this->m_item->list_data($this->cookie);
    //
    //Configuration -------------------------------------------------------------------------
    $this->load->file(APPPATH . 'libraries/PHPExcel.php');
    $master_cetak = BASEPATH . 'master_cetak/lab_item_pemeriksaan.xlsx';
    $PHPExcel = PHPExcel_IOFactory::load($master_cetak);

    // sheet 1
    $PHPExcel->setActiveSheetIndex(0);

    //align center
    $align_center = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      )
    );
    //align left
    $align_left = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
      )
    );
    //align right
    $align_right = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
      )
    );
    //font bold
    $font_bold = array(
      'font' => array(
        'bold'  => 'bold'
      )
    );
    //font underline
    $font_underline = array(
      'font' => array(
        'underline'  => 'underline'
      )
    );
    //font size 12
    $font_size_12 = array(
      'font' => array(
        'size'  => 12
      )
    );
    //wrap text
    $wrap_text = array(
      'alignment' => array(
        'wrap' => 'wrap',
      )
    );
    //create border
    $styleArray = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('rgb' => '111111'),
        ),
      ),
      'font' => array(
        'size'  => 11,
        'name'  => 'Times New Roman'
      ),
    );
    //Header
    $PHPExcel->getActiveSheet()->setCellValue("A6", 'REKAPITULASI LABORATORIUM ITEM PEMERIKSAAN');
    $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE DATA BULAN ' . strtoupper(get_bulan($this->cookie['search']['bulan'])) . ' ' . $this->cookie['search']['tahun']);

    // Body
    $i = 10;
    foreach ($main as $row) {
      $PHPExcel->getActiveSheet()->setCellValue("A$i", $row['itemlab_nm']);
      $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($align_left);
      $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($font_bold);
      $PHPExcel->getActiveSheet()->mergeCells("A$i:D$i");

      $is_item = $i + 1;
      $no = 1;
      foreach ($row['list_item'] as $item) {
        $PHPExcel->getActiveSheet()->setCellValue("A$is_item", $no++);
        $PHPExcel->getActiveSheet()->setCellValue("B$is_item", ' ' . $item['itemlab_id']);
        $PHPExcel->getActiveSheet()->setCellValue("C$is_item", $item['itemlab_nm']);
        $PHPExcel->getActiveSheet()->setCellValue("D$is_item", $item['jml_order']);
        //
        $is_item++;
      }
      //
      $i = $is_item;
    }

    // Creating Border
    $last_cell = "D" . ($i - 1);
    $PHPExcel->getActiveSheet()->getStyle("A9:$last_cell")->applyFromArray($styleArray);

    // Save it as file ------------------------------------------------------------------
    $file_export = 'rekapitulasi-laboratorium-item-pemeriksaan-' . strtolower(get_bulan($this->cookie['search']['bulan'])) . '-' . $this->cookie['search']['tahun'];
    //
    ob_end_clean();
    header('Content-Type: application/vnd.ms-excel2007');
    header('Content-Disposition: attachment; filename="' . $file_export . '.xlsx"');
    $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    //-----------------------------------------------------------------------------------
  }
}
