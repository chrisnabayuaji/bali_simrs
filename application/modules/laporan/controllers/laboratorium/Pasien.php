<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pasien extends MY_Controller
{

  var $nav_id = '12.03.01', $nav, $cookie;

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'laboratorium/m_pasien',
    ));

    $this->nav = $this->m_app->_get_nav($this->nav_id);

    //cookie
    $this->cookie = get_cookie_nav($this->nav_id . '.pasien');
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('bulan' => '', 'tahun' => '', 'asalpasien_st' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => '', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
  }

  public function index()
  {
    $this->authorize($this->nav, '_view');
    //main data
    $data['nav'] = $this->nav;
    if ($this->cookie['search']['bulan'] == '' && $this->cookie['search']['tahun'] == '') {
      $data['main'] = array();
    } else {
      $data['main'] = $this->m_pasien->list_data($this->cookie);
    }
    //cookie
    set_cookie_nav($this->nav_id . '.pasien', $this->cookie);
    $data['cookie'] = $this->cookie;
    $data['bulan'] = list_bulan();
    //set pagination
    set_pagination($this->nav, $this->cookie);
    //render
    create_log('_view', $this->nav_id);
    $this->render('laporan/laboratorium/pasien/index', $data);
  }

  function cetak_excel()
  {
    ini_set('memory_limit', '-1');
    // Header
    $main = $this->m_pasien->list_data($this->cookie);
    //
    //Configuration -------------------------------------------------------------------------
    $this->load->file(APPPATH . 'libraries/PHPExcel.php');
    $master_cetak = BASEPATH . 'master_cetak/lab_pemeriksaan_pasien.xlsx';
    $PHPExcel = PHPExcel_IOFactory::load($master_cetak);

    // sheet 1
    $PHPExcel->setActiveSheetIndex(0);

    //align center
    $align_center = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      )
    );
    //align left
    $align_left = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
      )
    );
    //align right
    $align_right = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
      )
    );
    //font bold
    $font_bold = array(
      'font' => array(
        'bold'  => 'bold'
      )
    );
    //font underline
    $font_underline = array(
      'font' => array(
        'underline'  => 'underline'
      )
    );
    //font size 12
    $font_size_12 = array(
      'font' => array(
        'size'  => 12
      )
    );
    //wrap text
    $wrap_text = array(
      'alignment' => array(
        'wrap' => 'wrap',
      )
    );
    //create border
    $styleArray = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('rgb' => '111111'),
        ),
      ),
      'font' => array(
        'size'  => 11,
        'name'  => 'Times New Roman'
      ),
    );
    //Header
    $PHPExcel->getActiveSheet()->setCellValue("A6", 'REKAPITULASI LABORATORIUM PEMERIKSAAN PASIEN');
    $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE DATA BULAN ' . strtoupper(get_bulan($this->cookie['search']['bulan'])) . ' ' . $this->cookie['search']['tahun']);

    // Body
    $i = 10;
    $no = 1;
    foreach ($main as $row) {

      if ($row['jenispasien_id'] == 02) {
        $no_kartu = '(' . $row['no_kartu'] . ')';
      } else {
        $no_kartu = '';
      }

      if ($row['periksa_st'] == 1) {
        $periksa = 'SUDAH';
      } else {
        $periksa = 'BELUM';
      }

      $item_pemeriksaan = '';
      $noitem = 1;
      foreach ($row['item_pemeriksaan'] as $item) {
        if ($noitem++ == count($row['item_pemeriksaan'])) {
          $enter_baris = "";
        } else {
          $enter_baris = "\n";
        }

        $item_pemeriksaan .= $item['itemlab_id'] . " - " . $item['itemlab_nm'] . $enter_baris;
      }


      $PHPExcel->getActiveSheet()->setCellValue("A$i", $no++);
      $PHPExcel->getActiveSheet()->setCellValue("B$i", ' ' . $row['pemeriksaan_id']);
      $PHPExcel->getActiveSheet()->setCellValue("C$i", to_date($row['tgl_order'], '', 'full_date'));
      $PHPExcel->getActiveSheet()->setCellValue("D$i", ' ' . $row['reg_id']);
      $PHPExcel->getActiveSheet()->setCellValue("E$i", ' ' . $row['pasien_id']);
      $PHPExcel->getActiveSheet()->setCellValue("F$i", $row['pasien_nm'] . ', ' . $row['sebutan_cd']);
      $PHPExcel->getActiveSheet()->setCellValue("G$i", $row['sex_cd']);
      $PHPExcel->getActiveSheet()->setCellValue("H$i", $row['umur_thn'] . ' th ' . $row['umur_bln'] . ' bl ' . $row['umur_hr'] . ' hr');
      $PHPExcel->getActiveSheet()->setCellValue("I$i", ' ' . $row['jenispasien_nm'] . $no_kartu);
      $PHPExcel->getActiveSheet()->setCellValue("J$i", $periksa);
      $PHPExcel->getActiveSheet()->setCellValue("K$i", $item_pemeriksaan);
      //
      $i++;
    }

    // Creating Border
    $last_cell = "K" . ($i - 1);
    $PHPExcel->getActiveSheet()->getStyle("A9:$last_cell")->applyFromArray($styleArray);

    // Save it as file ------------------------------------------------------------------
    $file_export = 'rekapitulasi-laboratorium-pemeriksaan-pasien-' . strtolower(get_bulan($this->cookie['search']['bulan'])) . '-' . $this->cookie['search']['tahun'];
    //
    ob_end_clean();
    header('Content-Type: application/vnd.ms-excel2007');
    header('Content-Disposition: attachment; filename="' . $file_export . '.xlsx"');
    $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    //-----------------------------------------------------------------------------------
  }
}
