<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class C_per_kelompokumur extends MY_Controller
{

  var $nav_id = '12.01.05', $nav, $cookie;

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'laporan/m_c_per_kelompokumur',
    ));

    $this->nav = $this->m_app->_get_nav($this->nav_id);

    //cookie
    $this->cookie = get_cookie_nav($this->nav_id . '.cperkelompokumur');
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('periode_data' => '', 'tgl' => '', 'tgl_awal' => '', 'tgl_akhir' => '', 'bulan' => '', 'tahun' => '', 'filter' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => '', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
  }

  public function index()
  {
    $this->authorize($this->nav, '_view');
    //main data
    $data['nav'] = $this->nav;
    if ($this->cookie['search']['periode_data'] == '' && $this->cookie['search']['filter'] == '') {
      $data['main'] = array();
    } else {
      $data['main'] = $this->m_c_per_kelompokumur->list_data($this->cookie);
    }
    //cookie
    set_cookie_nav($this->nav_id . '.cperkelompokumur', $this->cookie);
    $data['cookie'] = $this->cookie;
    $data['bulan'] = list_bulan();
    //set pagination
    set_pagination($this->nav, $this->cookie);
    //render
    create_log('_view', $this->nav_id);
    $this->render('laporan/laporan/c_per_kelompokumur/index', $data);
  }

  function cetak_excel()
  {
    ini_set('memory_limit', '-1');
    // Header
    $main = $this->m_c_per_kelompokumur->list_data($this->cookie);
    //
    //Configuration -------------------------------------------------------------------------
    $this->load->file(APPPATH . 'libraries/PHPExcel.php');
    $master_cetak = BASEPATH . 'master_cetak/c_per_kelompokumur.xlsx';
    $PHPExcel = PHPExcel_IOFactory::load($master_cetak);

    // sheet 1
    $PHPExcel->setActiveSheetIndex(0);

    //align center
    $align_center = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      )
    );
    //align left
    $align_left = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
      )
    );
    //align right
    $align_right = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
      )
    );
    //font bold
    $font_bold = array(
      'font' => array(
        'bold'  => 'bold'
      )
    );
    //font underline
    $font_underline = array(
      'font' => array(
        'underline'  => 'underline'
      )
    );
    //font size 12
    $font_size_12 = array(
      'font' => array(
        'size'  => 12
      )
    );
    //wrap text
    $wrap_text = array(
      'alignment' => array(
        'wrap' => 'wrap',
      )
    );
    //create border
    $styleArray = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('rgb' => '111111'),
        ),
      ),
      'font' => array(
        'size'  => 11,
        'name'  => 'Times New Roman'
      ),
    );
    //Header
    $PHPExcel->getActiveSheet()->setCellValue("A6", 'REKAPITULASI CAKUPAN KELOMPOK UMUR');
    if ($this->cookie['search']['periode_data'] == '01') {
      $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE TANGGAL ' . $this->cookie['search']['tgl']);
    } elseif ($this->cookie['search']['periode_data'] == '02') {
      $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE TANGGAL ' . $this->cookie['search']['tgl_awal'] . ' s/d ' . $this->cookie['search']['tgl_akhir']);
    } elseif ($this->cookie['search']['periode_data'] == '03') {
      $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE BULAN ' . strtoupper(get_bulan($this->cookie['search']['bulan'])) . ' ' . $this->cookie['search']['tahun']);
    } elseif ($this->cookie['search']['periode_data'] == '04') {
      $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE TAHUN ' . $this->cookie['search']['tahun']);
    }

    if ($this->cookie['search']['filter'] == '01') {
      $PHPExcel->getActiveSheet()->setCellValue("B9", 'Lokasi Pelayanan');
    } elseif ($this->cookie['search']['filter'] == '02') {
      $PHPExcel->getActiveSheet()->setCellValue("B9", 'Jenis Pasien');
    } elseif ($this->cookie['search']['filter'] == '03') {
      $PHPExcel->getActiveSheet()->setCellValue("B9", 'Asal Pasien');
    } elseif ($this->cookie['search']['filter'] == '04') {
      $PHPExcel->getActiveSheet()->setCellValue("B9", 'Dokter Penanggungjawab');
    }

    // Body
    $i = 12;
    $no = 1;
    for ($for_i = 1; $for_i <= 12; $for_i++) {
      $num = sprintf("%02d", $for_i);
      $tot_akhir_l_[$num] = 0;
      $tot_akhir_p_[$num] = 0;
    }
    $tot_akhir_total = 0;
    $tot_kunj_baru = 0;
    $tot_kunj_lama = 0;
    foreach ($main as $row) {
      for ($for_i = 1; $for_i <= 12; $for_i++) {
        $num = sprintf("%02d", $for_i);
        $tot_akhir_l_[$num] += $row['jml_kelompok_' . $num . '_l'];
        $tot_akhir_p_[$num] += $row['jml_kelompok_' . $num . '_p'];
      }
      $tot_akhir_total += $row['jml_kelompok_total'];
      $tot_kunj_baru += $row['jml_kunj_baru'];
      $tot_kunj_lama += $row['jml_kunj_lama'];

      $PHPExcel->getActiveSheet()->setCellValue("A$i", $no++);
      $PHPExcel->getActiveSheet()->setCellValue("B$i", $row['col_nm']);

      $ascii = ord('C');
      $awal_abj_1 = 0;
      $awal_abj_2 = 1;
      for ($for_i = 1; $for_i <= 12; $for_i++) {
        $cell_abj_1 = chr($ascii + $awal_abj_1);
        $cell_abj_2 = chr($ascii + $awal_abj_2);
        $num = sprintf("%02d", $for_i);

        $PHPExcel->getActiveSheet()->setCellValue("$cell_abj_1$i", ' ' . $row['jml_kelompok_' . $num . '_l']);
        $PHPExcel->getActiveSheet()->setCellValue("$cell_abj_2$i", ' ' . $row['jml_kelompok_' . $num . '_p']);

        $awal_abj_1 += 2;
        $awal_abj_2 += 2;
      }
      $PHPExcel->getActiveSheet()->setCellValue("AA$i", ' ' . $row['jml_kelompok_total']);
      $PHPExcel->getActiveSheet()->setCellValue("AB$i", ' ' . $row['jml_kunj_baru']);
      $PHPExcel->getActiveSheet()->setCellValue("AC$i", ' ' . $row['jml_kunj_lama']);
      //
      $i++;
    }

    $PHPExcel->getActiveSheet()->setCellValue("A$i", "Total Akhir");
    $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->mergeCells("A$i:B$i");

    $ascii = ord('C');
    $awal_abj_1 = 0;
    $awal_abj_2 = 1;
    for ($for_i = 1; $for_i <= 12; $for_i++) {
      $cell_abj_1 = chr($ascii + $awal_abj_1);
      $cell_abj_2 = chr($ascii + $awal_abj_2);
      $num = sprintf("%02d", $for_i);

      $PHPExcel->getActiveSheet()->setCellValue("$cell_abj_1$i", ' ' . $tot_akhir_l_[$num]);
      $PHPExcel->getActiveSheet()->getStyle("$cell_abj_1$i")->applyFromArray($font_bold);

      $PHPExcel->getActiveSheet()->setCellValue("$cell_abj_2$i", ' ' . $tot_akhir_p_[$num]);
      $PHPExcel->getActiveSheet()->getStyle("$cell_abj_2$i")->applyFromArray($font_bold);

      $awal_abj_1 += 2;
      $awal_abj_2 += 2;
    }
    $PHPExcel->getActiveSheet()->setCellValue("AA$i", ' ' . $tot_akhir_total);
    $PHPExcel->getActiveSheet()->setCellValue("AB$i", ' ' . $tot_kunj_baru);
    $PHPExcel->getActiveSheet()->setCellValue("AC$i", ' ' . $tot_kunj_lama);

    // Creating Border
    $last_cell = "AC" . ($i);
    $PHPExcel->getActiveSheet()->getStyle("A9:$last_cell")->applyFromArray($styleArray);

    // Save it as file ------------------------------------------------------------------
    $file_export = 'rekapitulasi-cakupan-kelompok-umur';
    //
    ob_end_clean();
    header('Content-Type: application/vnd.ms-excel2007');
    header('Content-Disposition: attachment; filename="' . $file_export . '.xlsx"');
    $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    //-----------------------------------------------------------------------------------
  }
}
