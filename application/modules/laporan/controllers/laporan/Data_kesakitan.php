<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Data_kesakitan extends MY_Controller
{

  var $nav_id = '12.01.08', $nav, $cookie;

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'laporan/m_data_kesakitan',
    ));

    $this->nav = $this->m_app->_get_nav($this->nav_id);

    //cookie
    $this->cookie = get_cookie_nav($this->nav_id . '.datakesakitan');
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('periode_data' => '', 'tgl' => '', 'tgl_awal' => '', 'tgl_akhir' => '', 'bulan' => '', 'tahun' => '', 'filter' => '', 'filter_lanjutan' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => '', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
  }

  public function index()
  {
    $this->authorize($this->nav, '_view');
    //main data
    $data['nav'] = $this->nav;
    if ($this->cookie['search']['periode_data'] == '' && $this->cookie['search']['filter'] == '') {
      $data['main'] = array();
    } else {
      if ($this->cookie['search']['filter'] == '01') {
        $data['main'] = $this->m_data_kesakitan->list_data_jenis_kelamin($this->cookie);
      } elseif ($this->cookie['search']['filter'] == '02') {
        $data['main'] = $this->m_data_kesakitan->list_data_jenis_kunjungan($this->cookie);
      }
    }
    //cookie
    set_cookie_nav($this->nav_id . '.datakesakitan', $this->cookie);
    $data['cookie'] = $this->cookie;
    $data['bulan'] = list_bulan();
    //set pagination
    set_pagination($this->nav, $this->cookie);
    //render
    create_log('_view', $this->nav_id);
    $this->render('laporan/laporan/data_kesakitan/index', $data);
  }

  function cetak_excel()
  {
    ini_set('memory_limit', '-1');
    // Header
    if ($this->cookie['search']['filter'] == '01') {
      $main = $this->m_data_kesakitan->list_data_jenis_kelamin($this->cookie);
    } elseif ($this->cookie['search']['filter'] == '02') {
      $main = $this->m_data_kesakitan->list_data_jenis_kunjungan($this->cookie);
    }
    //
    //Configuration -------------------------------------------------------------------------
    $this->load->file(APPPATH . 'libraries/PHPExcel.php');
    $master_cetak = BASEPATH . 'master_cetak/data_kesakitan.xlsx';
    $PHPExcel = PHPExcel_IOFactory::load($master_cetak);

    // sheet 1
    $PHPExcel->setActiveSheetIndex(0);

    //align center
    $align_center = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      )
    );
    //align left
    $align_left = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
      )
    );
    //align right
    $align_right = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
      )
    );
    //font bold
    $font_bold = array(
      'font' => array(
        'bold'  => 'bold'
      )
    );
    //font underline
    $font_underline = array(
      'font' => array(
        'underline'  => 'underline'
      )
    );
    //font size 12
    $font_size_12 = array(
      'font' => array(
        'size'  => 12
      )
    );
    //wrap text
    $wrap_text = array(
      'alignment' => array(
        'wrap' => 'wrap',
      )
    );
    //create border
    $styleArray = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('rgb' => '111111'),
        ),
      ),
      'font' => array(
        'size'  => 11,
        'name'  => 'Times New Roman'
      ),
    );
    //Header
    if ($this->cookie['search']['filter'] == '01') {
      $PHPExcel->getActiveSheet()->setCellValue("A6", 'REKAPITULASI DATA KESAKITAN PER JENIS KELAMIN');
    } elseif ($this->cookie['search']['filter'] == '02') {
      $PHPExcel->getActiveSheet()->setCellValue("A6", 'REKAPITULASI DATA KESAKITAN PER JENIS KUNJUNGAN');
    }

    if ($this->cookie['search']['periode_data'] == '01') {
      $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE TANGGAL ' . $this->cookie['search']['tgl']);
    } elseif ($this->cookie['search']['periode_data'] == '02') {
      $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE TANGGAL ' . $this->cookie['search']['tgl_awal'] . ' s/d ' . $this->cookie['search']['tgl_akhir']);
    } elseif ($this->cookie['search']['periode_data'] == '03') {
      $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE BULAN ' . strtoupper(get_bulan($this->cookie['search']['bulan'])) . ' ' . $this->cookie['search']['tahun']);
    } elseif ($this->cookie['search']['periode_data'] == '04') {
      $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE TAHUN ' . $this->cookie['search']['tahun']);
    }

    if ($this->cookie['search']['filter'] == '01') {
      $PHPExcel->getActiveSheet()->setCellValue("D10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("E10", 'P');
      $PHPExcel->getActiveSheet()->setCellValue("F10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("G10", 'P');
      $PHPExcel->getActiveSheet()->setCellValue("H10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("I10", 'P');
      $PHPExcel->getActiveSheet()->setCellValue("J10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("K10", 'P');
      $PHPExcel->getActiveSheet()->setCellValue("L10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("M10", 'P');
      $PHPExcel->getActiveSheet()->setCellValue("N10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("O10", 'P');
      $PHPExcel->getActiveSheet()->setCellValue("P10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("Q10", 'P');
      $PHPExcel->getActiveSheet()->setCellValue("R10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("S10", 'P');
      $PHPExcel->getActiveSheet()->setCellValue("T10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("U10", 'P');
      $PHPExcel->getActiveSheet()->setCellValue("V10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("W10", 'P');
      $PHPExcel->getActiveSheet()->setCellValue("X10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("Y10", 'P');
      $PHPExcel->getActiveSheet()->setCellValue("Z10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("AA10", 'P');
    } elseif ($this->cookie['search']['filter'] == '02') {
      $PHPExcel->getActiveSheet()->setCellValue("D10", 'B');
      $PHPExcel->getActiveSheet()->setCellValue("E10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("F10", 'B');
      $PHPExcel->getActiveSheet()->setCellValue("G10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("H10", 'B');
      $PHPExcel->getActiveSheet()->setCellValue("I10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("J10", 'B');
      $PHPExcel->getActiveSheet()->setCellValue("K10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("L10", 'B');
      $PHPExcel->getActiveSheet()->setCellValue("M10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("N10", 'B');
      $PHPExcel->getActiveSheet()->setCellValue("O10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("P10", 'B');
      $PHPExcel->getActiveSheet()->setCellValue("Q10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("R10", 'B');
      $PHPExcel->getActiveSheet()->setCellValue("S10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("T10", 'B');
      $PHPExcel->getActiveSheet()->setCellValue("U10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("V10", 'B');
      $PHPExcel->getActiveSheet()->setCellValue("W10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("X10", 'B');
      $PHPExcel->getActiveSheet()->setCellValue("Y10", 'L');
      $PHPExcel->getActiveSheet()->setCellValue("Z10", 'B');
      $PHPExcel->getActiveSheet()->setCellValue("AA10", 'L');
    }

    // Body
    $i = 11;
    $no = 1;
    foreach ($main as $row) {
      $PHPExcel->getActiveSheet()->setCellValue("A$i", $no++);
      $PHPExcel->getActiveSheet()->setCellValue("B$i", $row['icdx']);
      $PHPExcel->getActiveSheet()->setCellValue("C$i", $row['penyakit_nm']);

      if ($this->cookie['search']['filter'] == '01') {
        $PHPExcel->getActiveSheet()->setCellValue("D$i", ' ' . $row['jml_kelompok_01_l']);
        $PHPExcel->getActiveSheet()->setCellValue("E$i", ' ' . $row['jml_kelompok_01_p']);
        $PHPExcel->getActiveSheet()->setCellValue("F$i", ' ' . $row['jml_kelompok_02_l']);
        $PHPExcel->getActiveSheet()->setCellValue("G$i", ' ' . $row['jml_kelompok_02_p']);
        $PHPExcel->getActiveSheet()->setCellValue("H$i", ' ' . $row['jml_kelompok_03_l']);
        $PHPExcel->getActiveSheet()->setCellValue("I$i", ' ' . $row['jml_kelompok_03_p']);
        $PHPExcel->getActiveSheet()->setCellValue("J$i", ' ' . $row['jml_kelompok_04_l']);
        $PHPExcel->getActiveSheet()->setCellValue("K$i", ' ' . $row['jml_kelompok_04_p']);
        $PHPExcel->getActiveSheet()->setCellValue("L$i", ' ' . $row['jml_kelompok_05_l']);
        $PHPExcel->getActiveSheet()->setCellValue("M$i", ' ' . $row['jml_kelompok_05_p']);
        $PHPExcel->getActiveSheet()->setCellValue("N$i", ' ' . $row['jml_kelompok_06_l']);
        $PHPExcel->getActiveSheet()->setCellValue("O$i", ' ' . $row['jml_kelompok_06_p']);
        $PHPExcel->getActiveSheet()->setCellValue("P$i", ' ' . $row['jml_kelompok_07_l']);
        $PHPExcel->getActiveSheet()->setCellValue("Q$i", ' ' . $row['jml_kelompok_07_p']);
        $PHPExcel->getActiveSheet()->setCellValue("R$i", ' ' . $row['jml_kelompok_08_l']);
        $PHPExcel->getActiveSheet()->setCellValue("S$i", ' ' . $row['jml_kelompok_08_p']);
        $PHPExcel->getActiveSheet()->setCellValue("T$i", ' ' . $row['jml_kelompok_09_l']);
        $PHPExcel->getActiveSheet()->setCellValue("U$i", ' ' . $row['jml_kelompok_09_p']);
        $PHPExcel->getActiveSheet()->setCellValue("V$i", ' ' . $row['jml_kelompok_10_l']);
        $PHPExcel->getActiveSheet()->setCellValue("W$i", ' ' . $row['jml_kelompok_10_p']);
        $PHPExcel->getActiveSheet()->setCellValue("X$i", ' ' . $row['jml_kelompok_11_l']);
        $PHPExcel->getActiveSheet()->setCellValue("Y$i", ' ' . $row['jml_kelompok_11_p']);
        $PHPExcel->getActiveSheet()->setCellValue("Z$i", ' ' . $row['jml_kelompok_12_l']);
        $PHPExcel->getActiveSheet()->setCellValue("AA$i", ' ' . $row['jml_kelompok_12_p']);
      } elseif ($this->cookie['search']['filter'] == '02') {
        $PHPExcel->getActiveSheet()->setCellValue("D$i", ' ' . $row['jml_kelompok_01_b']);
        $PHPExcel->getActiveSheet()->setCellValue("E$i", ' ' . $row['jml_kelompok_01_l']);
        $PHPExcel->getActiveSheet()->setCellValue("F$i", ' ' . $row['jml_kelompok_02_b']);
        $PHPExcel->getActiveSheet()->setCellValue("G$i", ' ' . $row['jml_kelompok_02_l']);
        $PHPExcel->getActiveSheet()->setCellValue("H$i", ' ' . $row['jml_kelompok_03_b']);
        $PHPExcel->getActiveSheet()->setCellValue("I$i", ' ' . $row['jml_kelompok_03_l']);
        $PHPExcel->getActiveSheet()->setCellValue("J$i", ' ' . $row['jml_kelompok_04_b']);
        $PHPExcel->getActiveSheet()->setCellValue("K$i", ' ' . $row['jml_kelompok_04_l']);
        $PHPExcel->getActiveSheet()->setCellValue("L$i", ' ' . $row['jml_kelompok_05_b']);
        $PHPExcel->getActiveSheet()->setCellValue("M$i", ' ' . $row['jml_kelompok_05_l']);
        $PHPExcel->getActiveSheet()->setCellValue("N$i", ' ' . $row['jml_kelompok_06_b']);
        $PHPExcel->getActiveSheet()->setCellValue("O$i", ' ' . $row['jml_kelompok_06_l']);
        $PHPExcel->getActiveSheet()->setCellValue("P$i", ' ' . $row['jml_kelompok_07_b']);
        $PHPExcel->getActiveSheet()->setCellValue("Q$i", ' ' . $row['jml_kelompok_07_l']);
        $PHPExcel->getActiveSheet()->setCellValue("R$i", ' ' . $row['jml_kelompok_08_b']);
        $PHPExcel->getActiveSheet()->setCellValue("S$i", ' ' . $row['jml_kelompok_08_l']);
        $PHPExcel->getActiveSheet()->setCellValue("T$i", ' ' . $row['jml_kelompok_09_b']);
        $PHPExcel->getActiveSheet()->setCellValue("U$i", ' ' . $row['jml_kelompok_09_l']);
        $PHPExcel->getActiveSheet()->setCellValue("V$i", ' ' . $row['jml_kelompok_10_b']);
        $PHPExcel->getActiveSheet()->setCellValue("W$i", ' ' . $row['jml_kelompok_10_l']);
        $PHPExcel->getActiveSheet()->setCellValue("X$i", ' ' . $row['jml_kelompok_11_b']);
        $PHPExcel->getActiveSheet()->setCellValue("Y$i", ' ' . $row['jml_kelompok_11_l']);
        $PHPExcel->getActiveSheet()->setCellValue("Z$i", ' ' . $row['jml_kelompok_12_b']);
        $PHPExcel->getActiveSheet()->setCellValue("AA$i", ' ' . $row['jml_kelompok_12_l']);
      }

      $PHPExcel->getActiveSheet()->setCellValue("AB$i", ' ' . $row['jml_kelompok_total']);
      //
      $i++;
    }

    // Creating Border
    $last_cell = "AB" . ($i - 1);
    $PHPExcel->getActiveSheet()->getStyle("A9:$last_cell")->applyFromArray($styleArray);

    // Save it as file ------------------------------------------------------------------
    if ($this->cookie['search']['filter'] == '01') {
      $file_export = 'rekapitulasi-data-kesakitan-per-jenis-kelamin';
    } elseif ($this->cookie['search']['filter'] == '02') {
      $file_export = 'rekapitulasi-data-kesakitan-per-jenis-kunjungan';
    }
    //
    ob_end_clean();
    header('Content-Type: application/vnd.ms-excel2007');
    header('Content-Disposition: attachment; filename="' . $file_export . '.xlsx"');
    $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    //-----------------------------------------------------------------------------------
  }
}
