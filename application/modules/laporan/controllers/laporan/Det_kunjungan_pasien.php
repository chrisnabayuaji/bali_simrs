<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Det_kunjungan_pasien extends MY_Controller
{

  var $nav_id = '12.01.10', $nav, $cookie;

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'laporan/m_det_kunjungan_pasien',
      'master/m_lokasi',
    ));

    $this->nav = $this->m_app->_get_nav($this->nav_id);

    //cookie
    $this->cookie = get_cookie_nav($this->nav_id . '.detkunjunganpasien');
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('periode_data' => '', 'tgl' => '', 'tgl_awal' => '', 'tgl_akhir' => '', 'bulan' => '', 'tahun' => '', 'jenisreg_st' => '', 'lokasi_id' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => '', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
  }

  public function index()
  {
    $this->authorize($this->nav, '_view');
    //main data
    $data['nav'] = $this->nav;
    if ($this->cookie['search']['periode_data'] == '') {
      $data['main'] = array();
    } else {
      $data['main'] = $this->m_det_kunjungan_pasien->list_data($this->cookie);
    }
    //cookie
    set_cookie_nav($this->nav_id . '.detkunjunganpasien', $this->cookie);
    $data['cookie'] = $this->cookie;
    $data['bulan'] = list_bulan();
    //set pagination
    set_pagination($this->nav, $this->cookie);
    //render
    create_log('_view', $this->nav_id);
    $this->render('laporan/laporan/det_kunjungan_pasien/index', $data);
  }

  function ajax($type = null, $id = null)
  {
    if ($type == 'get_lokasi') {
      $jenisreg_st = $this->input->post('jenisreg_st');
      $lokasi_id = $this->input->post('lokasi_id');
      $list_lokasi = $this->m_lokasi->by_field('jenisreg_st', $jenisreg_st, 'result');
      //
      $html = '';
      $html .= '<select name="lokasi_id" id="lokasi_id" class="chosen-select custom-select w-100">';
      if (count($list_lokasi) > 1 || count($list_lokasi) == 0) {
        $html .= '<option value="">- Semua -</option>';
      }
      foreach ($list_lokasi as $kelas) {
        if (@$lokasi_id == $kelas['lokasi_id']) {
          $html .= '<option value="' . $kelas['lokasi_id'] . '" selected>' . $kelas['lokasi_nm'] . '</option>';
        } else {
          $html .= '<option value="' . $kelas['lokasi_id'] . '">' . $kelas['lokasi_nm'] . '</option>';
        }
      }
      $html .= '</select>';
      $html .= js_chosen();
      //
      echo json_encode(array(
        'html' => $html,
      ));
    }
  }

  function cetak_excel()
  {
    ini_set('memory_limit', '-1');
    // Header
    $main = $this->m_det_kunjungan_pasien->list_data($this->cookie);
    //
    //Configuration -------------------------------------------------------------------------
    $this->load->file(APPPATH . 'libraries/PHPExcel.php');
    $master_cetak = BASEPATH . 'master_cetak/det_kunjungan_pasien.xlsx';
    $PHPExcel = PHPExcel_IOFactory::load($master_cetak);

    // sheet 1
    $PHPExcel->setActiveSheetIndex(0);

    //align center
    $align_center = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      )
    );
    //align left
    $align_left = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
      )
    );
    //align right
    $align_right = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
      )
    );
    //font bold
    $font_bold = array(
      'font' => array(
        'bold'  => 'bold'
      )
    );
    //font underline
    $font_underline = array(
      'font' => array(
        'underline'  => 'underline'
      )
    );
    //font size 12
    $font_size_12 = array(
      'font' => array(
        'size'  => 12
      )
    );
    //wrap text
    $wrap_text = array(
      'alignment' => array(
        'wrap' => 'wrap',
      )
    );
    //create border
    $styleArray = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('rgb' => '111111'),
        ),
      ),
      'font' => array(
        'size'  => 11,
        'name'  => 'Times New Roman'
      ),
    );
    //Header
    $PHPExcel->getActiveSheet()->setCellValue("A6", 'REKAPITULASI CAKUPAN KELOMPOK UMUR');
    if ($this->cookie['search']['periode_data'] == '01') {
      $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE TANGGAL ' . $this->cookie['search']['tgl']);
    } elseif ($this->cookie['search']['periode_data'] == '02') {
      $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE TANGGAL ' . $this->cookie['search']['tgl_awal'] . ' s/d ' . $this->cookie['search']['tgl_akhir']);
    } elseif ($this->cookie['search']['periode_data'] == '03') {
      $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE BULAN ' . strtoupper(get_bulan($this->cookie['search']['bulan'])) . ' ' . $this->cookie['search']['tahun']);
    } elseif ($this->cookie['search']['periode_data'] == '04') {
      $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE TAHUN ' . $this->cookie['search']['tahun']);
    }

    // Body
    $i = 10;
    $no = 1;
    foreach ($main as $row) {
      $PHPExcel->getActiveSheet()->setCellValue("A$i", $no++);
      $PHPExcel->getActiveSheet()->setCellValue("B$i", ' ' . $row['reg_id']);
      $PHPExcel->getActiveSheet()->setCellValue("C$i", ' ' . $row['pasien_id']);
      $PHPExcel->getActiveSheet()->setCellValue("D$i", $row['pasien_nm']);
      $PHPExcel->getActiveSheet()->setCellValue("E$i", to_date($row['tgl_registrasi'], '', 'full_date'));
      $PHPExcel->getActiveSheet()->setCellValue("F$i", $row['alamat'] . ',' . ucwords(strtolower($row['kelurahan'])) . ',' . ucwords(strtolower($row['kecamatan'])) . ',' . ucwords(strtolower($row['kabupaten'])) . ',' . ucwords(strtolower($row['provinsi'])));
      $PHPExcel->getActiveSheet()->setCellValue("G$i", $row['sex_cd']);
      $PHPExcel->getActiveSheet()->setCellValue("H$i", $row['umur_thn'] . ' thn, ' . $row['umur_bln'] . ' bln, ' . $row['umur_hr'] . ' hr');
      $PHPExcel->getActiveSheet()->setCellValue("I$i", $row['jenispasien_nm']);
      $PHPExcel->getActiveSheet()->setCellValue("J$i", get_parameter_value('asalpasien_cd', $row['asalpasien_cd']));
      $PHPExcel->getActiveSheet()->setCellValue("K$i", $row['lokasi_nm']);
      $PHPExcel->getActiveSheet()->setCellValue("L$i", ' ' . $row['icdx']);
      $PHPExcel->getActiveSheet()->setCellValue("M$i", $row['penyakit_nm']);
      //
      $i++;
    }

    // Creating Border
    $last_cell = "M" . ($i - 1);
    $PHPExcel->getActiveSheet()->getStyle("A9:$last_cell")->applyFromArray($styleArray);

    // Save it as file ------------------------------------------------------------------
    $file_export = 'rekapitulasi-detail-kunjungan-pasien';
    //
    ob_end_clean();
    header('Content-Type: application/vnd.ms-excel2007');
    header('Content-Disposition: attachment; filename="' . $file_export . '.xlsx"');
    $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    //-----------------------------------------------------------------------------------
  }
}
