<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Duapuluh_penyakit extends MY_Controller
{

  var $nav_id = '12.01.09', $nav, $cookie;

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'laporan/m_duapuluh_penyakit',
    ));

    $this->nav = $this->m_app->_get_nav($this->nav_id);

    //cookie
    $this->cookie = get_cookie_nav($this->nav_id . '.duapuluhpenyakit');
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('periode_data' => '', 'tgl' => '', 'tgl_awal' => '', 'tgl_akhir' => '', 'bulan' => '', 'tahun' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => '', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
  }

  public function index()
  {
    $this->authorize($this->nav, '_view');
    //main data
    $data['nav'] = $this->nav;
    if ($this->cookie['search']['periode_data'] == '') {
      $data['main'] = array();
    } else {
      $data['main'] = $this->m_duapuluh_penyakit->list_data($this->cookie);
    }
    //cookie
    set_cookie_nav($this->nav_id . '.duapuluhpenyakit', $this->cookie);
    $data['cookie'] = $this->cookie;
    $data['bulan'] = list_bulan();
    //set pagination
    set_pagination($this->nav, $this->cookie);
    //render
    create_log('_view', $this->nav_id);
    $this->render('laporan/laporan/duapuluh_penyakit/index', $data);
  }

  function cetak_excel()
  {
    ini_set('memory_limit', '-1');
    // Header
    $main = $this->m_duapuluh_penyakit->list_data($this->cookie);
    //
    //Configuration -------------------------------------------------------------------------
    $this->load->file(APPPATH . 'libraries/PHPExcel.php');
    $master_cetak = BASEPATH . 'master_cetak/duapuluh_penyakit.xlsx';
    $PHPExcel = PHPExcel_IOFactory::load($master_cetak);

    // sheet 1
    $PHPExcel->setActiveSheetIndex(0);

    //align center
    $align_center = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      )
    );
    //align left
    $align_left = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
      )
    );
    //align right
    $align_right = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
      )
    );
    //font bold
    $font_bold = array(
      'font' => array(
        'bold'  => 'bold'
      )
    );
    //font underline
    $font_underline = array(
      'font' => array(
        'underline'  => 'underline'
      )
    );
    //font size 12
    $font_size_12 = array(
      'font' => array(
        'size'  => 12
      )
    );
    //wrap text
    $wrap_text = array(
      'alignment' => array(
        'wrap' => 'wrap',
      )
    );
    //create border
    $styleArray = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('rgb' => '111111'),
        ),
      ),
      'font' => array(
        'size'  => 11,
        'name'  => 'Times New Roman'
      ),
    );
    //Header
    $PHPExcel->getActiveSheet()->setCellValue("A6", 'REKAPITULASI 20 BESAR PENYAKIT');
    if ($this->cookie['search']['periode_data'] == '01') {
      $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE TANGGAL ' . $this->cookie['search']['tgl']);
    } elseif ($this->cookie['search']['periode_data'] == '02') {
      $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE TANGGAL ' . $this->cookie['search']['tgl_awal'] . ' s/d ' . $this->cookie['search']['tgl_akhir']);
    } elseif ($this->cookie['search']['periode_data'] == '03') {
      $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE BULAN ' . strtoupper(get_bulan($this->cookie['search']['bulan'])) . ' ' . $this->cookie['search']['tahun']);
    } elseif ($this->cookie['search']['periode_data'] == '04') {
      $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE TAHUN ' . $this->cookie['search']['tahun']);
    }

    // Body
    $i = 11;
    $no = 1;
    foreach ($main as $row) {
      $PHPExcel->getActiveSheet()->setCellValue("A$i", ' ' . $row['icdx']);
      $PHPExcel->getActiveSheet()->setCellValue("B$i", $row['penyakit_nm']);
      $PHPExcel->getActiveSheet()->setCellValue("L$i", ' ' . $row['jml_diagnosis']);
      //

      $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($align_left);
      $PHPExcel->getActiveSheet()->mergeCells("B$i:K$i");

      $PHPExcel->getActiveSheet()->getStyle("L$i")->applyFromArray($align_center);
      $PHPExcel->getActiveSheet()->mergeCells("L$i:M$i");

      $i++;
    }

    // Creating Border
    $last_cell = "M" . ($i - 1);
    $PHPExcel->getActiveSheet()->getStyle("A9:$last_cell")->applyFromArray($styleArray);

    // Save it as file ------------------------------------------------------------------
    $file_export = 'rekapitulasi-20-besar-penyakit';
    //
    ob_end_clean();
    header('Content-Type: application/vnd.ms-excel2007');
    header('Content-Disposition: attachment; filename="' . $file_export . '.xlsx"');
    $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    //-----------------------------------------------------------------------------------
  }
}
