<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Harian_obat extends MY_Controller
{

	var $nav_id = '12.01.15', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'laporan/m_harian_obat',
			'master/m_lokasi',
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id . '.harianobat');
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('bulan' => '', 'tahun' => '', 'jenisreg_st' => '', 'lokasi_id' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => '', 'type' => 'asc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//main data
		$data['nav'] = $this->nav;
		if ($this->cookie['search']['bulan'] == '' && $this->cookie['search']['tahun'] == '') {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_harian_obat->list_data($this->cookie);
		}
		//cookie
		set_cookie_nav($this->nav_id . '.harianobat', $this->cookie);
		$data['cookie'] = $this->cookie;
		$data['bulan'] = list_bulan();
		//set pagination
		set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('laporan/laporan/harian_obat/index', $data);
	}

	function ajax($type = null, $id = null)
	{
		if ($type == 'get_lokasi') {
			$jenisreg_st = $this->input->post('jenisreg_st');
			$lokasi_id = $this->input->post('lokasi_id');
			$list_lokasi = $this->m_lokasi->by_field('jenisreg_st', $jenisreg_st, 'result');
			//
			$html = '';
			$html .= '<select name="lokasi_id" id="lokasi_id" class="chosen-select custom-select w-100">';
			if (count($list_lokasi) > 1 || count($list_lokasi) == 0) {
				$html .= '<option value="">- Semua -</option>';
			}
			foreach ($list_lokasi as $kelas) {
				if (@$lokasi_id == $kelas['lokasi_id']) {
					$html .= '<option value="' . $kelas['lokasi_id'] . '" selected>' . $kelas['lokasi_nm'] . '</option>';
				} else {
					$html .= '<option value="' . $kelas['lokasi_id'] . '">' . $kelas['lokasi_nm'] . '</option>';
				}
			}
			$html .= '</select>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}
	}

	function cetak_excel()
	{
		ini_set('memory_limit', '-1');
		// Header
		$main = $this->m_harian_obat->list_data($this->cookie);
		//
		//Configuration -------------------------------------------------------------------------
		$this->load->file(APPPATH . 'libraries/PHPExcel.php');
		$master_cetak = BASEPATH . 'master_cetak/harian_obat.xlsx';
		$PHPExcel = PHPExcel_IOFactory::load($master_cetak);

		// sheet 1
		$PHPExcel->setActiveSheetIndex(0);

		//align center
		$align_center = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			)
		);
		//align left
		$align_left = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			)
		);
		//align right
		$align_right = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			)
		);
		//font bold
		$font_bold = array(
			'font' => array(
				'bold'  => 'bold'
			)
		);
		//font underline
		$font_underline = array(
			'font' => array(
				'underline'  => 'underline'
			)
		);
		//font size 12
		$font_size_12 = array(
			'font' => array(
				'size'  => 12
			)
		);
		//wrap text
		$wrap_text = array(
			'alignment' => array(
				'wrap' => 'wrap',
			)
		);
		//create border
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '111111'),
				),
			),
			'font' => array(
				'size'  => 11,
				'name'  => 'Times New Roman'
			),
		);
		//Header
		$PHPExcel->getActiveSheet()->setCellValue("A6", 'REKAPITULASI PEMAKAIAN HARIAN OBAT');
		$PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE DATA BULAN ' . strtoupper(get_bulan($this->cookie['search']['bulan'])) . ' ' . $this->cookie['search']['tahun']);

		// Body
		$i = 10;
		$no = 1;
		foreach ($main as $row) {
			$PHPExcel->getActiveSheet()->setCellValue("A$i", $no++);
			$PHPExcel->getActiveSheet()->setCellValue("B$i", $row['obat_nm']);
			$PHPExcel->getActiveSheet()->setCellValue("C$i", ' ' . $row['obat_id']);
			$PHPExcel->getActiveSheet()->setCellValue("D$i", $row['satuan_nm']);
			$abj = 'E';
			for ($for_i = 1; $for_i <= 31; $for_i++) {
				$num = sprintf("%02d", $for_i);
				$PHPExcel->getActiveSheet()->setCellValue("$abj$i", ' ' . $row['jml_tgl_' . $num]);
				$abj++;
			}
			$PHPExcel->getActiveSheet()->setCellValue("AJ$i", ' ' . $row['jml_tgl_total']);
			//
			$i++;
		}

		// Creating Border
		$last_cell = "AJ" . ($i - 1);
		$PHPExcel->getActiveSheet()->getStyle("A9:$last_cell")->applyFromArray($styleArray);

		// Save it as file ------------------------------------------------------------------
		$file_export = 'rekapitulasi-pemakaian-harian-obat-' . $this->cookie['search']['bulan'] . '-' . $this->cookie['search']['tahun'];
		//
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel2007');
		header('Content-Disposition: attachment; filename="' . $file_export . '.xlsx"');
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		//-----------------------------------------------------------------------------------
	}
}
