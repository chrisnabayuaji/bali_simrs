<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kunj_harian extends MY_Controller
{

	var $nav_id = '12.01.01', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'laporan/m_kunj_harian',
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id . '.kunjharian');
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('bulan' => '', 'tahun' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => '', 'type' => 'asc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//main data
		$data['nav'] = $this->nav;
		if ($this->cookie['search']['bulan'] == '' && $this->cookie['search']['tahun'] == '') {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_kunj_harian->list_data($this->cookie);
		}
		//cookie
		set_cookie_nav($this->nav_id . '.kunjharian', $this->cookie);
		$data['cookie'] = $this->cookie;
		$data['bulan'] = list_bulan();
		//set pagination
		set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('laporan/laporan/kunj_harian/index', $data);
	}

	function cetak_excel()
	{
		ini_set('memory_limit', '-1');
		// Header
		$main = $this->m_kunj_harian->list_data($this->cookie);
		//
		//Configuration -------------------------------------------------------------------------
		$this->load->file(APPPATH . 'libraries/PHPExcel.php');
		$master_cetak = BASEPATH . 'master_cetak/kunjungan_harian.xlsx';
		$PHPExcel = PHPExcel_IOFactory::load($master_cetak);

		// sheet 1
		$PHPExcel->setActiveSheetIndex(0);

		//align center
		$align_center = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			)
		);
		//align left
		$align_left = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
			)
		);
		//align right
		$align_right = array(
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
			)
		);
		//font bold
		$font_bold = array(
			'font' => array(
				'bold'  => 'bold'
			)
		);
		//font underline
		$font_underline = array(
			'font' => array(
				'underline'  => 'underline'
			)
		);
		//font size 12
		$font_size_12 = array(
			'font' => array(
				'size'  => 12
			)
		);
		//wrap text
		$wrap_text = array(
			'alignment' => array(
				'wrap' => 'wrap',
			)
		);
		//create border
		$styleArray = array(
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '111111'),
				),
			),
			'font' => array(
				'size'  => 11,
				'name'  => 'Times New Roman'
			),
		);
		//Header
		$PHPExcel->getActiveSheet()->setCellValue("A6", 'REKAPITULASI KUNJUNGAN HARIAN');
		$PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE DATA BULAN ' . strtoupper(get_bulan($this->cookie['search']['bulan'])) . ' ' . $this->cookie['search']['tahun']);

		// Body
		$i = 10;
		$no = 1;
		for ($for_i = 1; $for_i <= 31; $for_i++) {
			$num = sprintf("%02d", $for_i);
			$tot_akhir_[$num] = 0;
		}
		$tot_akhir_total = 0;
		foreach ($main as $row) {
			for ($for_i = 1; $for_i <= 31; $for_i++) {
				$num = sprintf("%02d", $for_i);
				$tot_akhir_[$num] += $row['jml_tgl_' . $num];
			}
			$tot_akhir_total += $row['jml_tgl_total'];

			$PHPExcel->getActiveSheet()->setCellValue("A$i", $no++);
			$PHPExcel->getActiveSheet()->setCellValue("B$i", $row['lokasi_nm']);
			$abj = 'C';
			for ($for_i = 1; $for_i <= 31; $for_i++) {
				$num = sprintf("%02d", $for_i);
				$PHPExcel->getActiveSheet()->setCellValue("$abj$i", ' ' . $row['jml_tgl_' . $num]);
				$abj++;
			}
			$PHPExcel->getActiveSheet()->setCellValue("AH$i", ' ' . $row['jml_tgl_total']);
			//
			$i++;
		}

		$PHPExcel->getActiveSheet()->setCellValue("A$i", "Total Akhir");
		$PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($font_bold);
		$PHPExcel->getActiveSheet()->mergeCells("A$i:B$i");
		$abj = 'C';
		for ($for_i = 1; $for_i <= 31; $for_i++) {
			$num = sprintf("%02d", $for_i);
			$PHPExcel->getActiveSheet()->setCellValue("$abj$i", ' ' . $tot_akhir_[$num]);
			$PHPExcel->getActiveSheet()->getStyle("$abj$i")->applyFromArray($font_bold);
			$abj++;
		}
		$PHPExcel->getActiveSheet()->setCellValue("AH$i", ' ' . $tot_akhir_total);

		// Creating Border
		$last_cell = "AH" . ($i);
		$PHPExcel->getActiveSheet()->getStyle("A9:$last_cell")->applyFromArray($styleArray);

		// Save it as file ------------------------------------------------------------------
		$file_export = 'laporan-kunjungan-harian-' . $this->cookie['search']['bulan'] . '-' . $this->cookie['search']['tahun'];
		//
		ob_end_clean();
		header('Content-Type: application/vnd.ms-excel2007');
		header('Content-Disposition: attachment; filename="' . $file_export . '.xlsx"');
		$objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		//-----------------------------------------------------------------------------------
	}
}
