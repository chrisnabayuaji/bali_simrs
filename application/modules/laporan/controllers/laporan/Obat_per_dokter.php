<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Obat_per_dokter extends MY_Controller
{

	var $nav_id = '12.01.14', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'laporan/m_obat_per_dokter',
			'master/m_pegawai',
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id . '.obatperdokter');
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('dokter_id' => '', 'tgl_awal' => '', 'tgl_akhir' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => '', 'type' => 'asc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//main data
		$data['nav'] = $this->nav;
		if ($this->cookie['search']['tgl_awal'] == '' || $this->cookie['search']['tgl_akhir'] == '') {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_obat_per_dokter->list_data($this->cookie);
		}
		//cookie
		set_cookie_nav($this->nav_id . '.obatperdokter', $this->cookie);
		$data['cookie'] = $this->cookie;
		$data['bulan'] = list_bulan();
		$data['dokter'] = $this->m_pegawai->list_dokter();
		//set pagination
		set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('laporan/laporan/obat_per_dokter/index', $data);
	}
}
