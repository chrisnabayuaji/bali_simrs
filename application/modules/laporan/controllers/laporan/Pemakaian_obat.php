<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pemakaian_obat extends MY_Controller
{

  var $nav_id = '12.01.13', $nav, $cookie;

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'laporan/m_pemakaian_obat',
      'master/m_lokasi',
      'master/m_jenis_pasien',
      'master/m_pegawai',
    ));

    $this->nav = $this->m_app->_get_nav($this->nav_id);

    //cookie
    $this->cookie = get_cookie_nav($this->nav_id . '.pemakaianobat');
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('periode_data' => '', 'tgl' => '', 'tgl_awal' => '', 'tgl_akhir' => '', 'bulan' => '', 'tahun' => '', 'jenisreg_st' => '', 'lokasi_id' => '', 'jenispasien_id' => 'jenispasien_id', 'petugas_id' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => '', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
  }

  public function index()
  {
    $this->authorize($this->nav, '_view');
    //main data
    $data['nav'] = $this->nav;
    if ($this->cookie['search']['periode_data'] == '') {
      $data['main'] = array();
    } else {
      $data['main'] = $this->m_pemakaian_obat->list_resep($this->cookie);
    }
    //cookie
    set_cookie_nav($this->nav_id . '.pemakaianobat', $this->cookie);
    $data['cookie'] = $this->cookie;
    $data['bulan'] = list_bulan();
    $data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
    $data['petugas'] = $this->m_pegawai->all_data('01');
    //set pagination
    set_pagination($this->nav, $this->cookie);
    //render
    create_log('_view', $this->nav_id);
    $this->render('laporan/laporan/pemakaian_obat/index', $data);
  }

  function ajax($type = null, $id = null)
  {
    if ($type == 'get_lokasi') {
      $jenisreg_st = $this->input->post('jenisreg_st');
      $lokasi_id = $this->input->post('lokasi_id');
      $list_lokasi = $this->m_lokasi->by_field('jenisreg_st', $jenisreg_st, 'result');
      //
      $html = '';
      $html .= '<select name="lokasi_id" id="lokasi_id" class="chosen-select custom-select w-100">';
      if (count($list_lokasi) > 1 || count($list_lokasi) == 0) {
        $html .= '<option value="">- Semua -</option>';
      }
      foreach ($list_lokasi as $kelas) {
        if (@$lokasi_id == $kelas['lokasi_id']) {
          $html .= '<option value="' . $kelas['lokasi_id'] . '" selected>' . $kelas['lokasi_nm'] . '</option>';
        } else {
          $html .= '<option value="' . $kelas['lokasi_id'] . '">' . $kelas['lokasi_nm'] . '</option>';
        }
      }
      $html .= '</select>';
      $html .= js_chosen();
      //
      echo json_encode(array(
        'html' => $html,
      ));
    }
  }
}
