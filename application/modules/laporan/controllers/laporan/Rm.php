<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Rm extends MY_Controller
{

  var $nav_id = '12.02.04', $nav, $cookie;

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'laporan/m_rm'
    ));

    $this->nav = $this->m_app->_get_nav($this->nav_id);

    //cookie
    $this->cookie = get_cookie_nav($this->nav_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('bulan' => '', 'tahun' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => '', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
  }

  public function index()
  {
    $this->authorize($this->nav, '_view');
    //main data
    $data['nav'] = $this->nav;
    //cookie
    set_cookie_nav($this->nav_id, $this->cookie);
    $data['bor'] = null;
    $data['los'] = null;
    $data['toi'] = null;
    $data['bto'] = null;
    if ($this->cookie['search']['bulan'] != '' && $this->cookie['search']['tahun'] != '') {
      $data['bor'] = $this->m_rm->bor($this->cookie);
      $data['los'] = $this->m_rm->los($this->cookie);
      $data['toi'] = $this->m_rm->toi($this->cookie);
      $data['bto'] = $this->m_rm->bto($this->cookie);
      $data['rekap'] = $this->m_rm->rekap($this->cookie);
      $bulan = $this->cookie['search']['bulan'];
      $start = $this->cookie['search']['tahun'] . '-' . $this->cookie['search']['bulan'] . '-01';
      $data['days'] = date('t', strtotime($start));
    }
    $data['cookie'] = $this->cookie;
    $data['bulan'] = list_bulan();
    //set pagination
    set_pagination($this->nav, $this->cookie);
    //render
    create_log('_view', $this->nav_id);
    $this->render('laporan/laporan/rm/index', $data);
  }
}
