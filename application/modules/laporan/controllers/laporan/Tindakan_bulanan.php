<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tindakan_bulanan extends MY_Controller
{

  var $nav_id = '12.01.12', $nav, $cookie;

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'laporan/m_tindakan_bulanan',
      'master/m_lokasi',
      'master/m_jenis_pasien',
      'master/m_pegawai',
    ));

    $this->nav = $this->m_app->_get_nav($this->nav_id);

    //cookie
    $this->cookie = get_cookie_nav($this->nav_id . '.tindakanbulanan');
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('tahun' => '', 'jenisreg_st' => '', 'lokasi_id' => '', 'jenispasien_id' => 'jenispasien_id', 'petugas_id' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => '', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
  }

  public function index()
  {
    $this->authorize($this->nav, '_view');
    //main data
    $data['nav'] = $this->nav;
    if ($this->cookie['search']['tahun'] == '') {
      $data['main'] = array();
    } else {
      $data['main'] = $this->m_tindakan_bulanan->list_data($this->cookie);
    }
    //cookie
    set_cookie_nav($this->nav_id . '.tindakanbulanan', $this->cookie);
    $data['cookie'] = $this->cookie;
    $data['bulan'] = list_bulan_singkat();
    $data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
    $data['petugas'] = $this->m_pegawai->all_data('01');
    //set pagination
    set_pagination($this->nav, $this->cookie);
    //render
    create_log('_view', $this->nav_id);
    $this->render('laporan/laporan/tindakan_bulanan/index', $data);
  }

  function ajax($type = null, $id = null)
  {
    if ($type == 'get_lokasi') {
      $jenisreg_st = $this->input->post('jenisreg_st');
      $lokasi_id = $this->input->post('lokasi_id');
      $list_lokasi = $this->m_lokasi->by_field('jenisreg_st', $jenisreg_st, 'result');
      //
      $html = '';
      $html .= '<select name="lokasi_id" id="lokasi_id" class="chosen-select custom-select w-100">';
      if (count($list_lokasi) > 1 || count($list_lokasi) == 0) {
        $html .= '<option value="">- Semua -</option>';
      }
      foreach ($list_lokasi as $kelas) {
        if (@$lokasi_id == $kelas['lokasi_id']) {
          $html .= '<option value="' . $kelas['lokasi_id'] . '" selected>' . $kelas['lokasi_nm'] . '</option>';
        } else {
          $html .= '<option value="' . $kelas['lokasi_id'] . '">' . $kelas['lokasi_nm'] . '</option>';
        }
      }
      $html .= '</select>';
      $html .= js_chosen();
      //
      echo json_encode(array(
        'html' => $html,
      ));
    }
  }

  function cetak_excel()
  {
    ini_set('memory_limit', '-1');
    // Header
    $main = $this->m_tindakan_bulanan->list_data($this->cookie);
    $bulan = list_bulan_singkat();
    //
    //Configuration -------------------------------------------------------------------------
    $this->load->file(APPPATH . 'libraries/PHPExcel.php');
    $master_cetak = BASEPATH . 'master_cetak/tindakan_bulanan.xlsx';
    $PHPExcel = PHPExcel_IOFactory::load($master_cetak);

    // sheet 1
    $PHPExcel->setActiveSheetIndex(0);

    //align center
    $align_center = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      )
    );
    //align left
    $align_left = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
      )
    );
    //align right
    $align_right = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
      )
    );
    //font bold
    $font_bold = array(
      'font' => array(
        'bold'  => 'bold'
      )
    );
    //font underline
    $font_underline = array(
      'font' => array(
        'underline'  => 'underline'
      )
    );
    //font size 12
    $font_size_12 = array(
      'font' => array(
        'size'  => 12
      )
    );
    //wrap text
    $wrap_text = array(
      'alignment' => array(
        'wrap' => 'wrap',
      )
    );
    //create border
    $styleArray = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('rgb' => '111111'),
        ),
      ),
      'font' => array(
        'size'  => 11,
        'name'  => 'Times New Roman'
      ),
    );
    //Header
    $PHPExcel->getActiveSheet()->setCellValue("A6", 'REKAPITULASI TOTAL TINDAKAN BULANAN');
    $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE DATA TAHUN ' . $this->cookie['search']['tahun']);

    // Body
    $i = 10;
    $no = 1;
    foreach ($bulan as $key => $val) {
      $tot_akhir_[$key] = 0;
    }
    $tot_akhir_total = 0;
    foreach ($main as $row) {
      foreach ($bulan as $key => $val) {
        $tot_akhir_[$key] += $row['jml_bln_' . $key];
      }
      $tot_akhir_total += $row['jml_bln_total'];

      $PHPExcel->getActiveSheet()->setCellValue("A$i", $no++);
      $PHPExcel->getActiveSheet()->setCellValue("B$i", $row['lokasi_nm']);
      $abj = 'C';
      foreach ($bulan as $key => $val) {
        $PHPExcel->getActiveSheet()->setCellValue("$abj$i", ' ' . num_id($row['jml_bln_' . $key]));
        $abj++;
      }
      $PHPExcel->getActiveSheet()->setCellValue("O$i", ' ' . num_id($row['jml_bln_total']));
      //
      $i++;
    }

    $PHPExcel->getActiveSheet()->setCellValue("A$i", "Total Akhir");
    $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->mergeCells("A$i:B$i");
    $abj = 'C';
    foreach ($bulan as $key => $val) {
      $PHPExcel->getActiveSheet()->setCellValue("$abj$i", ' ' . num_id($tot_akhir_[$key]));
      $PHPExcel->getActiveSheet()->getStyle("$abj$i")->applyFromArray($font_bold);
      $abj++;
    }
    $PHPExcel->getActiveSheet()->setCellValue("O$i", ' ' . num_id($tot_akhir_total));

    // Creating Border
    $last_cell = "O" . ($i);
    $PHPExcel->getActiveSheet()->getStyle("A9:$last_cell")->applyFromArray($styleArray);

    // Save it as file ------------------------------------------------------------------
    $file_export = 'rekapitulasi-total-tindakan-bulanan-' . $this->cookie['search']['tahun'];
    //
    ob_end_clean();
    header('Content-Type: application/vnd.ms-excel2007');
    header('Content-Disposition: attachment; filename="' . $file_export . '.xlsx"');
    $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    //-----------------------------------------------------------------------------------
  }
}
