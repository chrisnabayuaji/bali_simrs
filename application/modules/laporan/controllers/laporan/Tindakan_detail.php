<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Tindakan_detail extends MY_Controller
{

  var $nav_id = '12.01.11', $nav, $cookie;

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'laporan/m_tindakan_detail',
      'master/m_lokasi',
      'master/m_jenis_pasien',
      'master/m_pegawai',
    ));

    $this->nav = $this->m_app->_get_nav($this->nav_id);

    //cookie
    $this->cookie = get_cookie_nav($this->nav_id . '.tindakandetail');
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('periode_data' => '', 'tgl' => '', 'tgl_awal' => '', 'tgl_akhir' => '', 'bulan' => '', 'tahun' => '', 'jenisreg_st' => '', 'lokasi_id' => '', 'jenispasien_id' => 'jenispasien_id', 'petugas_id' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => '', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
  }

  public function index()
  {
    $this->authorize($this->nav, '_view');
    //main data
    $data['nav'] = $this->nav;
    if ($this->cookie['search']['periode_data'] == '') {
      $data['main'] = array();
    } else {
      $data['main'] = $this->m_tindakan_detail->list_data($this->cookie);
    }
    //cookie
    set_cookie_nav($this->nav_id . '.tindakandetail', $this->cookie);
    $data['cookie'] = $this->cookie;
    $data['bulan'] = list_bulan();
    $data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
    $data['petugas'] = $this->m_pegawai->all_data('01');
    //set pagination
    set_pagination($this->nav, $this->cookie);
    //render
    create_log('_view', $this->nav_id);
    $this->render('laporan/laporan/tindakan_detail/index', $data);
  }

  function ajax($type = null, $id = null)
  {
    if ($type == 'get_lokasi') {
      $jenisreg_st = $this->input->post('jenisreg_st');
      $lokasi_id = $this->input->post('lokasi_id');
      $list_lokasi = $this->m_lokasi->by_field('jenisreg_st', $jenisreg_st, 'result');
      //
      $html = '';
      $html .= '<select name="lokasi_id" id="lokasi_id" class="chosen-select custom-select w-100">';
      if (count($list_lokasi) > 1 || count($list_lokasi) == 0) {
        $html .= '<option value="">- Semua -</option>';
      }
      foreach ($list_lokasi as $kelas) {
        if (@$lokasi_id == $kelas['lokasi_id']) {
          $html .= '<option value="' . $kelas['lokasi_id'] . '" selected>' . $kelas['lokasi_nm'] . '</option>';
        } else {
          $html .= '<option value="' . $kelas['lokasi_id'] . '">' . $kelas['lokasi_nm'] . '</option>';
        }
      }
      $html .= '</select>';
      $html .= js_chosen();
      //
      echo json_encode(array(
        'html' => $html,
      ));
    }
  }

  function cetak_excel()
  {
    ini_set('memory_limit', '-1');
    // Header
    $main = $this->m_tindakan_detail->list_data($this->cookie);
    //
    //Configuration -------------------------------------------------------------------------
    $this->load->file(APPPATH . 'libraries/PHPExcel.php');
    $master_cetak = BASEPATH . 'master_cetak/tindakan_detail.xlsx';
    $PHPExcel = PHPExcel_IOFactory::load($master_cetak);

    // sheet 1
    $PHPExcel->setActiveSheetIndex(0);

    //align center
    $align_center = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      )
    );
    //align left
    $align_left = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
      )
    );
    //align right
    $align_right = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
      )
    );
    //font bold
    $font_bold = array(
      'font' => array(
        'bold'  => 'bold'
      )
    );
    //font underline
    $font_underline = array(
      'font' => array(
        'underline'  => 'underline'
      )
    );
    //font size 12
    $font_size_12 = array(
      'font' => array(
        'size'  => 12
      )
    );
    //wrap text
    $wrap_text = array(
      'alignment' => array(
        'wrap' => 'wrap',
      )
    );
    //create border
    $styleArray = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('rgb' => '111111'),
        ),
      ),
      'font' => array(
        'size'  => 11,
        'name'  => 'Times New Roman'
      ),
    );
    //Header
    $PHPExcel->getActiveSheet()->setCellValue("A6", 'REKAPITULASI TINDAKAN DETAIL');
    if ($this->cookie['search']['periode_data'] == '01') {
      $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE TANGGAL ' . $this->cookie['search']['tgl']);
    } elseif ($this->cookie['search']['periode_data'] == '02') {
      $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE TANGGAL ' . $this->cookie['search']['tgl_awal'] . ' s/d ' . $this->cookie['search']['tgl_akhir']);
    } elseif ($this->cookie['search']['periode_data'] == '03') {
      $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE BULAN ' . strtoupper(get_bulan($this->cookie['search']['bulan'])) . ' ' . $this->cookie['search']['tahun']);
    } elseif ($this->cookie['search']['periode_data'] == '04') {
      $PHPExcel->getActiveSheet()->setCellValue("A7", 'PERIODE TAHUN ' . $this->cookie['search']['tahun']);
    }

    // Body
    $i = 10;
    $no = 1;
    $total_akhir_js = 0;
    $total_akhir_jp = 0;
    $total_akhir_jb = 0;
    $total_akhir_biaya = 0;
    foreach ($main as $row) {

      $total_akhir_js += ($row['qty'] * $row['js']);
      $total_akhir_jp += ($row['qty'] * $row['jp']);
      $total_akhir_jb += ($row['qty'] * $row['jb']);
      $total_akhir_biaya += $row['jml_tagihan'];

      $pegawai_nm = '';
      if ($row['pegawai_nm_1'] != '') {
        $pegawai_nm .= $row['pegawai_nm_1'] . ', ';
      }
      if ($row['pegawai_nm_2'] != '') {
        $pegawai_nm .= $row['pegawai_nm_2'] . ', ';
      }
      if ($row['pegawai_nm_3'] != '') {
        $pegawai_nm .= $row['pegawai_nm_3'] . ', ';
      }
      if ($row['pegawai_nm_4'] != '') {
        $pegawai_nm .= $row['pegawai_nm_4'] . ', ';
      }
      if ($row['pegawai_nm_5'] != '') {
        $pegawai_nm .= $row['pegawai_nm_5'] . ', ';
      }

      $PHPExcel->getActiveSheet()->setCellValue("A$i", $no++);
      $PHPExcel->getActiveSheet()->setCellValue("B$i", ' ' . to_date($row['tgl_catat'], '', 'full_date'));
      $PHPExcel->getActiveSheet()->setCellValue("C$i", ' ' . $row['tindakan_id']);
      $PHPExcel->getActiveSheet()->setCellValue("D$i", $row['tarif_nm']);
      $PHPExcel->getActiveSheet()->setCellValue("E$i", $row['lokasi_nm']);
      $PHPExcel->getActiveSheet()->setCellValue("F$i", ' ' . $row['qty']);
      $PHPExcel->getActiveSheet()->setCellValue("G$i", ' ' . num_id($row['qty'] * $row['js']));
      $PHPExcel->getActiveSheet()->setCellValue("H$i", ' ' . num_id($row['qty'] * $row['jp']));
      $PHPExcel->getActiveSheet()->setCellValue("I$i", ' ' . num_id($row['qty'] * $row['jb']));
      $PHPExcel->getActiveSheet()->setCellValue("J$i", ' ' . num_id($row['jml_tagihan']));
      $PHPExcel->getActiveSheet()->setCellValue("K$i", ' ' . $pegawai_nm);
      $PHPExcel->getActiveSheet()->setCellValue("L$i", ' ' . $row['jenispasien_nm']);
      //
      $i++;
    }

    $PHPExcel->getActiveSheet()->setCellValue("A$i", "Total Akhir");
    $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->mergeCells("A$i:F$i");

    $PHPExcel->getActiveSheet()->setCellValue("G$i", ' ' . num_id($total_akhir_js));
    $PHPExcel->getActiveSheet()->getStyle("G$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("G$i")->applyFromArray($align_right);

    $PHPExcel->getActiveSheet()->setCellValue("H$i", ' ' . num_id($total_akhir_jp));
    $PHPExcel->getActiveSheet()->getStyle("H$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("H$i")->applyFromArray($align_right);

    $PHPExcel->getActiveSheet()->setCellValue("I$i", ' ' . num_id($total_akhir_jb));
    $PHPExcel->getActiveSheet()->getStyle("I$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("I$i")->applyFromArray($align_right);

    $PHPExcel->getActiveSheet()->setCellValue("J$i", ' ' . num_id($total_akhir_biaya));
    $PHPExcel->getActiveSheet()->getStyle("J$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("J$i")->applyFromArray($align_right);

    // Creating Border
    $last_cell = "L" . ($i);
    $PHPExcel->getActiveSheet()->getStyle("A9:$last_cell")->applyFromArray($styleArray);

    // Save it as file ------------------------------------------------------------------
    $file_export = 'rekapitulasi-tindakan-detail';
    //
    ob_end_clean();
    header('Content-Type: application/vnd.ms-excel2007');
    header('Content-Disposition: attachment; filename="' . $file_export . '.xlsx"');
    $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    //-----------------------------------------------------------------------------------
  }
}
