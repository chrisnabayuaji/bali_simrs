<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pemeriksaan_per_bulan extends MY_Controller
{

  var $nav_id = '12.07.03', $nav, $cookie;

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'radiologi/m_pemeriksaan_per_bulan',
      'master/m_lokasi',
    ));

    $this->nav = $this->m_app->_get_nav($this->nav_id);

    //cookie
    $this->cookie = get_cookie_nav($this->nav_id . '.radiologipemeriksaanperbulan');
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('bulan' => '', 'tahun' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => '', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
  }

  public function index()
  {
    $this->authorize($this->nav, '_view');
    //main data
    $data['nav'] = $this->nav;
    //cookie
    set_cookie_nav($this->nav_id . '.radiologipemeriksaanperbulan', $this->cookie);
    $data['cookie'] = $this->cookie;
    $data['bulan'] = list_bulan();
    //set pagination
    set_pagination($this->nav, $this->cookie);
    //render
    create_log('_view', $this->nav_id);
    $this->render('laporan/radiologi/pemeriksaan_per_bulan/index', $data);
  }

  function list_data()
  {
    if ($this->cookie['search']['bulan'] == '' && $this->cookie['search']['tahun'] == '') {
      $data['main'] = array();
    } else {
      $data['main'] = $this->m_pemeriksaan_per_bulan->list_data($this->cookie);
    }
    $data['cookie'] = $this->cookie;
    echo json_encode(array(
      'html' => $this->load->view('laporan/radiologi/pemeriksaan_per_bulan/list_data', $data, true)
    ));
  }

  function cetak_excel()
  {
    ini_set('memory_limit', '-1');
    // Header
    $main = $this->m_pemeriksaan_per_bulan->list_data($this->cookie);
    if (@$this->cookie['search']['lokasi_id'] != '') {
      $get_lokasi = $this->m_lokasi->get_data(@$this->cookie['search']['lokasi_id']);
    }
    //
    //Configuration -------------------------------------------------------------------------
    $this->load->file(APPPATH . 'libraries/PHPExcel.php');
    $master_cetak = BASEPATH . 'master_cetak/rad_pemeriksaan_per_bulan.xlsx';
    $PHPExcel = PHPExcel_IOFactory::load($master_cetak);

    // sheet 1
    $PHPExcel->setActiveSheetIndex(0);

    //align center
    $align_center = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      )
    );
    //align left
    $align_left = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
      )
    );
    //align right
    $align_right = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
      )
    );
    //font bold
    $font_bold = array(
      'font' => array(
        'bold'  => 'bold'
      )
    );
    //font underline
    $font_underline = array(
      'font' => array(
        'underline'  => 'underline'
      )
    );
    //font size 12
    $font_size_12 = array(
      'font' => array(
        'size'  => 12
      )
    );
    //wrap text
    $wrap_text = array(
      'alignment' => array(
        'wrap' => 'wrap',
      )
    );
    //create border
    $styleArray = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('rgb' => '111111'),
        ),
      ),
      'font' => array(
        'size'  => 11,
        'name'  => 'Times New Roman'
      ),
    );
    //Header
    $PHPExcel->getActiveSheet()->setCellValue("A1", 'LAPORAN PEMERIKSAAN RADIOLOGI PER BULAN');
    $PHPExcel->getActiveSheet()->setCellValue("A2", 'BULAN ' . strtoupper(get_bulan($this->cookie['search']['bulan'])) . ' ' . $this->cookie['search']['tahun']);

    // Body
    $i = 6;
    $tot_tagihan = 0;
    foreach ($main as $row) {
      $PHPExcel->getActiveSheet()->setCellValue("A$i", $row['itemrad_nm']);
      $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($align_left);
      $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($font_bold);
      $PHPExcel->getActiveSheet()->mergeCells("A$i:D$i");

      $is_item = $i + 1;
      $no = 1;
      foreach ($row['list_item'] as $item) {
        $tot_tagihan += $item['jml_tagihan'];

        $PHPExcel->getActiveSheet()->setCellValue("A$is_item", $no++);
        $PHPExcel->getActiveSheet()->setCellValue("B$is_item", $item['itemrad_nm']);
        $abj = 'C';
        for ($for_i = 1; $for_i <= 31; $for_i++) {
          $num = $for_i;
          $PHPExcel->getActiveSheet()->setCellValue("$abj$is_item", ' ' . num_id($item['tgl_' . $num]));
          $abj++;
        }
        $PHPExcel->getActiveSheet()->setCellValue("AH$is_item", ' ' . num_id($item['total']));
        $PHPExcel->getActiveSheet()->setCellValue("AI$is_item", ' ' . num_id($item['jml_tagihan']));
        //
        $is_item++;
      }
      //
      $i = $is_item;
    }

    // Total
    $PHPExcel->getActiveSheet()->setCellValue("A$i", 'TOTAL');
    $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($align_center);
    $PHPExcel->getActiveSheet()->mergeCells("A$i:AH$i");

    $PHPExcel->getActiveSheet()->setCellValue("AI$i", 'Rp. ' . num_id($tot_tagihan));
    $PHPExcel->getActiveSheet()->getStyle("AI$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("AI$i")->applyFromArray($align_right);

    // Creating Border
    $last_cell = "AI" . ($i);
    $PHPExcel->getActiveSheet()->getStyle("A5:$last_cell")->applyFromArray($styleArray);

    // Save it as file ------------------------------------------------------------------
    $file_export = 'laporan-pemeriksaan-radiologi-per-bulan-' . strtolower(get_bulan($this->cookie['search']['bulan'])) . '-' . $this->cookie['search']['tahun'];
    //
    ob_end_clean();
    header('Content-Type: application/vnd.ms-excel2007');
    header('Content-Disposition: attachment; filename="' . $file_export . '.xlsx"');
    $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    //-----------------------------------------------------------------------------------
  }
}
