<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pemeriksaan_per_hari extends MY_Controller
{

  var $nav_id = '12.07.02', $nav, $cookie;

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'radiologi/m_pemeriksaan_per_hari',
    ));

    $this->nav = $this->m_app->_get_nav($this->nav_id);

    //cookie
    $this->cookie = get_cookie_nav($this->nav_id . '.radiologipemeriksaanperhari');
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('tgl' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => '', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
  }

  public function index()
  {
    $this->authorize($this->nav, '_view');
    //main data
    $data['nav'] = $this->nav;
    if ($this->cookie['search']['tgl'] == '') {
      $data['main'] = array();
    } else {
      $data['main'] = $this->m_pemeriksaan_per_hari->list_data($this->cookie);
    }
    //cookie
    set_cookie_nav($this->nav_id . '.radiologipemeriksaanperhari', $this->cookie);
    $data['cookie'] = $this->cookie;
    //set pagination
    set_pagination($this->nav, $this->cookie);
    //render
    create_log('_view', $this->nav_id);
    $this->render('laporan/radiologi/pemeriksaan_per_hari/index', $data);
  }

  function cetak_excel()
  {
    ini_set('memory_limit', '-1');
    // Header
    $main = $this->m_pemeriksaan_per_hari->list_data($this->cookie);
    //
    //Configuration -------------------------------------------------------------------------
    $this->load->file(APPPATH . 'libraries/PHPExcel.php');
    $master_cetak = BASEPATH . 'master_cetak/rad_pemeriksaan_per_hari.xlsx';
    $PHPExcel = PHPExcel_IOFactory::load($master_cetak);

    // sheet 1
    $PHPExcel->setActiveSheetIndex(0);

    //align center
    $align_center = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      )
    );
    //align left
    $align_left = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
      )
    );
    //align right
    $align_right = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
      )
    );
    //font bold
    $font_bold = array(
      'font' => array(
        'bold'  => 'bold'
      )
    );
    //font underline
    $font_underline = array(
      'font' => array(
        'underline'  => 'underline'
      )
    );
    //font size 12
    $font_size_12 = array(
      'font' => array(
        'size'  => 12
      )
    );
    //wrap text
    $wrap_text = array(
      'alignment' => array(
        'wrap' => 'wrap',
      )
    );
    //create border
    $styleArray = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('rgb' => '111111'),
        ),
      ),
      'font' => array(
        'size'  => 11,
        'name'  => 'Times New Roman'
      ),
    );
    //Header
    $PHPExcel->getActiveSheet()->setCellValue("A6", 'REKAPITULASI RADIOLOGI ITEM PEMERIKSAAN');
    $PHPExcel->getActiveSheet()->setCellValue("A7", 'TGL. ' . $this->cookie['search']['tgl']);

    // Body
    $i = 10;
    $no = 1;
    $tot_tagihan = 0;
    foreach ($main as $row) {
      $tot_tagihan += $row['jml_tagihan'];

      $alamat = '';
      if ($row['alamat'] != '') {
        $alamat .= $row['alamat'] . ', ';
      } else {
        $alamat .= '';
      }

      if ($row['kelurahan'] != '') {
        $alamat .= ucwords(strtolower($row['kelurahan'])) . ', ';
      } else {
        $alamat .= '';
      }

      if ($row['kecamatan'] != '') {
        $alamat .= ucwords(strtolower($row['kecamatan'])) . ', ';
      } else {
        $alamat .= '';
      }

      if ($row['kabupaten'] != '') {
        $alamat .= ucwords(strtolower($row['kabupaten'])) . ', ';
      } else {
        $alamat .= '';
      }

      if ($row['provinsi'] != '') {
        $alamat .= ucwords(strtolower($row['provinsi']));
      } else {
        $alamat .= '';
      }

      if ($row['jenispasien_id'] == '02') {
        $no_kartu = $row['no_kartu'] . "\n";
      } else {
        $no_kartu = "";
      }

      $PHPExcel->getActiveSheet()->setCellValue("A$i", $no++);
      $PHPExcel->getActiveSheet()->setCellValue("B$i", ' ' . $row['pemeriksaan_id']);
      $PHPExcel->getActiveSheet()->setCellValue("C$i", to_date($row['tgl_order'], '', 'full_date'));
      $PHPExcel->getActiveSheet()->setCellValue("D$i", to_date($row['tgl_hasil'], '', 'full_date'));
      $PHPExcel->getActiveSheet()->setCellValue("E$i", $row['pasien_id']);
      $PHPExcel->getActiveSheet()->setCellValue("F$i", $row['pasien_nm'] . ", " . $row['sebutan_cd'] . " \n " . $row['umur_thn'] . ' th ' . $row['umur_bln'] . ' bl ' . $row['umur_hr'] . ' hr ');
      $PHPExcel->getActiveSheet()->setCellValue("G$i", $alamat);
      $PHPExcel->getActiveSheet()->setCellValue("H$i", $row['sex_cd']);
      $PHPExcel->getActiveSheet()->setCellValue("I$i", $row['jenispasien_nm'] . "\n" . $no_kartu . $row['lokasi_nm']);
      $PHPExcel->getActiveSheet()->setCellValue("J$i", ($row['periksa_st'] == 1) ? 'SUDAH' : 'BELUM');
      $PHPExcel->getActiveSheet()->setCellValue("K$i", ' ' . num_id($row['jml_tagihan']));

      $PHPExcel->getActiveSheet()->getStyle("C$i")->getAlignment()->setWrapText(true);
      $PHPExcel->getActiveSheet()->getStyle("D$i")->getAlignment()->setWrapText(true);
      $PHPExcel->getActiveSheet()->getStyle("F$i")->getAlignment()->setWrapText(true);
      $PHPExcel->getActiveSheet()->getStyle("G$i")->getAlignment()->setWrapText(true);
      $PHPExcel->getActiveSheet()->getStyle("I$i")->getAlignment()->setWrapText(true);

      $i++;
    }

    // Total
    $PHPExcel->getActiveSheet()->setCellValue("A$i", 'TOTAL');
    $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($align_center);
    $PHPExcel->getActiveSheet()->mergeCells("A$i:J$i");

    $PHPExcel->getActiveSheet()->setCellValue("K$i", 'Rp. ' . num_id($tot_tagihan));
    $PHPExcel->getActiveSheet()->getStyle("K$i")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("K$i")->applyFromArray($align_right);

    // Creating Border
    $last_cell = "K" . ($i);
    $PHPExcel->getActiveSheet()->getStyle("A9:$last_cell")->applyFromArray($styleArray);

    // Save it as file ------------------------------------------------------------------
    $file_export = 'laporan-pemeriksaan-per-hari-' . $this->cookie['search']['tgl'];
    //
    ob_end_clean();
    header('Content-Type: application/vnd.ms-excel2007');
    header('Content-Disposition: attachment; filename="' . $file_export . '.xlsx"');
    $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    //-----------------------------------------------------------------------------------
  }
}
