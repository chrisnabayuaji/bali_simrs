<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pengeluaran_dokter extends CI_Model
{

  public function list_data($cookie)
  {
    $where = "WHERE 1 = 1 ";
    if (@$cookie['search']['tahun'] != '') {
      $where .= "AND YEAR(x.tgl_catat) = '" . $this->db->escape_like_str($cookie['search']['tahun']) . "' ";
    }
    if (@$cookie['search']['bulan'] != '') {
      $where .= "AND MONTH(x.tgl_catat) = '" . $this->db->escape_like_str($cookie['search']['bulan']) . "' ";
    }
    if (@$cookie['search']['dokter_id'] != '') {
      $where .= "AND x.dokter_id = '" . $this->db->escape_like_str($cookie['search']['dokter_id']) . "' ";
    }
    $sql = "SELECT 
              a.obat_id, a.obat_nm, 
              IF(b.tgl_1 IS NULL, 0, tgl_1) AS tgl_1,
              IF(b.tgl_2 IS NULL, 0, tgl_2) AS tgl_2,
              IF(b.tgl_3 IS NULL, 0, tgl_3) AS tgl_3,
              IF(b.tgl_4 IS NULL, 0, tgl_4) AS tgl_4,
              IF(b.tgl_5 IS NULL, 0, tgl_5) AS tgl_5,
              IF(b.tgl_6 IS NULL, 0, tgl_6) AS tgl_6,
              IF(b.tgl_7 IS NULL, 0, tgl_7) AS tgl_7,
              IF(b.tgl_8 IS NULL, 0, tgl_8) AS tgl_8,
              IF(b.tgl_9 IS NULL, 0, tgl_9) AS tgl_9,
              IF(b.tgl_10 IS NULL, 0, tgl_10) AS tgl_10,
              IF(b.tgl_11 IS NULL, 0, tgl_11) AS tgl_11,
              IF(b.tgl_12 IS NULL, 0, tgl_12) AS tgl_12,
              IF(b.tgl_13 IS NULL, 0, tgl_13) AS tgl_13,
              IF(b.tgl_14 IS NULL, 0, tgl_14) AS tgl_14,
              IF(b.tgl_15 IS NULL, 0, tgl_15) AS tgl_15,
              IF(b.tgl_16 IS NULL, 0, tgl_16) AS tgl_16,
              IF(b.tgl_17 IS NULL, 0, tgl_17) AS tgl_17,
              IF(b.tgl_18 IS NULL, 0, tgl_18) AS tgl_18,
              IF(b.tgl_19 IS NULL, 0, tgl_19) AS tgl_19,
              IF(b.tgl_20 IS NULL, 0, tgl_20) AS tgl_20,
              IF(b.tgl_21 IS NULL, 0, tgl_21) AS tgl_21,
              IF(b.tgl_22 IS NULL, 0, tgl_22) AS tgl_22,
              IF(b.tgl_23 IS NULL, 0, tgl_23) AS tgl_23,
              IF(b.tgl_24 IS NULL, 0, tgl_24) AS tgl_24,
              IF(b.tgl_25 IS NULL, 0, tgl_25) AS tgl_25,
              IF(b.tgl_26 IS NULL, 0, tgl_26) AS tgl_26,
              IF(b.tgl_27 IS NULL, 0, tgl_27) AS tgl_27,
              IF(b.tgl_28 IS NULL, 0, tgl_28) AS tgl_28,
              IF(b.tgl_29 IS NULL, 0, tgl_29) AS tgl_29,
              IF(b.tgl_30 IS NULL, 0, tgl_30) AS tgl_30,
              IF(b.tgl_31 IS NULL, 0, tgl_31) AS tgl_31,
              IF(b.total IS NULL, 0, total) AS total, 
              IF((a.harga_beli * total) IS NULL, 0, (a.harga_beli * total)) AS harga_beli, 
              IF(b.jumlah_akhir IS NULL, 0, jumlah_akhir) AS harga_jual, 
	            (IF(b.jumlah_akhir IS NULL, 0, jumlah_akhir) - IF((a.harga_beli * total) IS NULL, 0, (a.harga_beli * total))) AS laba
            FROM mst_obat a 
            LEFT JOIN (
              SELECT
                x.obat_id,
                SUM(CASE WHEN DAY(x.tgl_catat) = '01' THEN x.qty ELSE 0 END) AS tgl_1,
                SUM(CASE WHEN DAY(x.tgl_catat) = '02' THEN x.qty ELSE 0 END) AS tgl_2,
                SUM(CASE WHEN DAY(x.tgl_catat) = '03' THEN x.qty ELSE 0 END) AS tgl_3,
                SUM(CASE WHEN DAY(x.tgl_catat) = '04' THEN x.qty ELSE 0 END) AS tgl_4,
                SUM(CASE WHEN DAY(x.tgl_catat) = '05' THEN x.qty ELSE 0 END) AS tgl_5,
                SUM(CASE WHEN DAY(x.tgl_catat) = '06' THEN x.qty ELSE 0 END) AS tgl_6,
                SUM(CASE WHEN DAY(x.tgl_catat) = '07' THEN x.qty ELSE 0 END) AS tgl_7,
                SUM(CASE WHEN DAY(x.tgl_catat) = '08' THEN x.qty ELSE 0 END) AS tgl_8,
                SUM(CASE WHEN DAY(x.tgl_catat) = '09' THEN x.qty ELSE 0 END) AS tgl_9,
                SUM(CASE WHEN DAY(x.tgl_catat) = '10' THEN x.qty ELSE 0 END) AS tgl_10,
                SUM(CASE WHEN DAY(x.tgl_catat) = '11' THEN x.qty ELSE 0 END) AS tgl_11,
                SUM(CASE WHEN DAY(x.tgl_catat) = '12' THEN x.qty ELSE 0 END) AS tgl_12,
                SUM(CASE WHEN DAY(x.tgl_catat) = '13' THEN x.qty ELSE 0 END) AS tgl_13,
                SUM(CASE WHEN DAY(x.tgl_catat) = '14' THEN x.qty ELSE 0 END) AS tgl_14,
                SUM(CASE WHEN DAY(x.tgl_catat) = '15' THEN x.qty ELSE 0 END) AS tgl_15,
                SUM(CASE WHEN DAY(x.tgl_catat) = '16' THEN x.qty ELSE 0 END) AS tgl_16,
                SUM(CASE WHEN DAY(x.tgl_catat) = '17' THEN x.qty ELSE 0 END) AS tgl_17,
                SUM(CASE WHEN DAY(x.tgl_catat) = '18' THEN x.qty ELSE 0 END) AS tgl_18,
                SUM(CASE WHEN DAY(x.tgl_catat) = '19' THEN x.qty ELSE 0 END) AS tgl_19,
                SUM(CASE WHEN DAY(x.tgl_catat) = '20' THEN x.qty ELSE 0 END) AS tgl_20,
                SUM(CASE WHEN DAY(x.tgl_catat) = '21' THEN x.qty ELSE 0 END) AS tgl_21,
                SUM(CASE WHEN DAY(x.tgl_catat) = '22' THEN x.qty ELSE 0 END) AS tgl_22,
                SUM(CASE WHEN DAY(x.tgl_catat) = '23' THEN x.qty ELSE 0 END) AS tgl_23,
                SUM(CASE WHEN DAY(x.tgl_catat) = '24' THEN x.qty ELSE 0 END) AS tgl_24,
                SUM(CASE WHEN DAY(x.tgl_catat) = '25' THEN x.qty ELSE 0 END) AS tgl_25,
                SUM(CASE WHEN DAY(x.tgl_catat) = '26' THEN x.qty ELSE 0 END) AS tgl_26,
                SUM(CASE WHEN DAY(x.tgl_catat) = '27' THEN x.qty ELSE 0 END) AS tgl_27,
                SUM(CASE WHEN DAY(x.tgl_catat) = '28' THEN x.qty ELSE 0 END) AS tgl_28,
                SUM(CASE WHEN DAY(x.tgl_catat) = '29' THEN x.qty ELSE 0 END) AS tgl_29,
                SUM(CASE WHEN DAY(x.tgl_catat) = '30' THEN x.qty ELSE 0 END) AS tgl_30,
                SUM(CASE WHEN DAY(x.tgl_catat) = '31' THEN x.qty ELSE 0 END) AS tgl_31,
                SUM(x.qty) AS total, 
                SUM(x.jumlah_akhir) AS jumlah_akhir, 
                x.tgl_catat 
              FROM dat_farmasi x
              LEFT JOIN mst_lokasi y ON x.lokasi_id = y.lokasi_id  
              $where
              GROUP BY x.obat_id
            ) b ON b.obat_id = a.obat_id";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
}
