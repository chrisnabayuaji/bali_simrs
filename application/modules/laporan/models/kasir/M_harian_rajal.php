<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_harian_rajal extends CI_Model
{

  public function list_data($cookie)
  {
    if (@$cookie['search']['tgl'] != '') {
      $sql_where = "AND DATE(a.tgl_registrasi) = '" . to_date($this->db->escape_like_str($cookie['search']['tgl'])) . "'";
    } else {
      $sql_where = "AND DATE(a.tgl_registrasi) = '" . date('Y-m-d') . "'";
    }

    if (@$cookie['search']['lokasi_id'] != '') {
      $sql_where .= " AND a.lokasi_id='" . $this->db->escape_like_str($cookie['search']['lokasi_id']) . "'";
    }

    if (@$cookie['search']['jenispasien_id'] != '') {
      $sql_where .= " AND a.jenispasien_id='" . $this->db->escape_like_str($cookie['search']['jenispasien_id']) . "'";
    }

    $sql = "SELECT
              a.pasien_id,
              a.pasien_nm,
              a.sex_cd,
              a.alamat,
              a.kelurahan,
              a.kecamatan,
              a.kabupaten,
              a.tgl_registrasi, 
              a.provinsi,
              a.sebutan_cd,
              b.lokasi_nm,
              c.jenispasien_nm,
              b.jenisreg_st, 
              d.grand_total_tagihan 
            FROM
              reg_pasien a
              LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
              LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
              INNER JOIN dat_billing d ON a.groupreg_id = d.groupreg_id 
            WHERE
              a.is_deleted = 0 
              AND b.jenisreg_st = '1' 
              AND a.bayar_st = 1 
              AND a.is_billing = 1 
              $sql_where 
            ORDER BY
              a.tgl_registrasi ASC ";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
}
