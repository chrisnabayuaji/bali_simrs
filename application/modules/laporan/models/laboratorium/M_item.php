<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_item extends CI_Model
{

  public function where($cookie)
  {
    $where = " ";
    if (@$cookie['search']['tahun'] != '') {
      $where .= "AND YEAR(c.tgl_order) = '" . $this->db->escape_like_str($cookie['search']['tahun']) . "' ";
    }
    if (@$cookie['search']['bulan'] != '') {
      $where .= "AND MONTH(c.tgl_order) = '" . $this->db->escape_like_str($cookie['search']['bulan']) . "' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT
              d.* 
            FROM
              lab_pemeriksaan_rinc a
              LEFT JOIN mst_item_lab b ON a.itemlab_id = b.itemlab_id
              LEFT JOIN lab_pemeriksaan c ON a.pemeriksaan_id = c.pemeriksaan_id
              LEFT JOIN mst_item_lab d ON b.parent_id = d.itemlab_id 
            WHERE
              d.parent_id = '' 
            $where
            GROUP BY
              d.itemlab_id";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    //
    foreach ($result as $key => $val) {
      $result[$key]['list_item'] = $this->list_item($val['itemlab_id'], $cookie);
    }
    return $result;
  }

  public function list_item($itemlab_id = null, $cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT
              a.itemlab_id,
              b.itemlab_nm,
              b.parent_id,
              c.pemeriksaan_id,
              c.reg_id,
              c.pasien_id,
              c.tgl_order,
              COUNT( a.pemeriksaanrinc_id ) AS jml_order 
            FROM
              lab_pemeriksaan_rinc a
              LEFT JOIN mst_item_lab b ON a.itemlab_id = b.itemlab_id
              LEFT JOIN lab_pemeriksaan c ON a.pemeriksaan_id = c.pemeriksaan_id 
            WHERE
              c.is_deleted = 0 
              AND b.parent_id = ?
              AND a.is_tagihan = 1
              $where
            GROUP BY
              a.itemlab_id";
    $query = $this->db->query($sql, $itemlab_id);
    $result = $query->result_array();
    return $result;
  }
}
