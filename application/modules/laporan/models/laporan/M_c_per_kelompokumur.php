<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_c_per_kelompokumur extends CI_Model {

  public function list_data($cookie)
  {
    if (@$cookie['search']['periode_data'] == '01') {
      $sql_where = "AND DATE(b.tgl_registrasi) = '".to_date($this->db->escape_like_str($cookie['search']['tgl']))."'";
    }elseif (@$cookie['search']['periode_data'] == '02') {
      $sql_where = "AND DATE(b.tgl_registrasi) BETWEEN '".to_date($this->db->escape_like_str($cookie['search']['tgl_awal']))."' AND '".to_date($this->db->escape_like_str($cookie['search']['tgl_akhir']))."'";
    }elseif (@$cookie['search']['periode_data'] == '03') {
      $sql_where = "AND YEAR(b.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['tahun'])."' AND MONTH(b.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['bulan'])."'";
    }elseif (@$cookie['search']['periode_data'] == '04') {
      $sql_where = "AND YEAR(b.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['tahun'])."'";
    }

    if($cookie['search']['filter'] == "01") {    // per lokasi pelayanan
      $tbl_field = " a.lokasi_id as col_id, a.lokasi_nm as col_nm, ";
      $tbl_join  = " b.lokasi_id=a.lokasi_id ";
      $tbl_join .= $sql_where;
      $tbl_from  = " FROM mst_lokasi a WHERE a.jenisreg_st IS NOT NULL ORDER BY a.lokasi_id ASC";
    } else if($cookie['search']['filter'] == "02") {  // per jenis pasien
      $tbl_field = " a.jenispasien_id as col_id, a.jenispasien_nm as col_nm, ";
      $tbl_join  = " b.jenispasien_id=a.jenispasien_id ";
      $tbl_join .= $sql_where;
      $tbl_from  = " FROM mst_jenis_pasien a ORDER BY a.jenispasien_id ASC";
    } else if($cookie['search']['filter'] == "03") {  // per asal pasien
      $tbl_field = " a.parameter_cd as col_id, a.parameter_val as col_nm, ";
      $tbl_join  = " b.asalpasien_cd=a.parameter_cd ";
      $tbl_join .= $sql_where;
      $tbl_from  = " FROM mst_parameter a WHERE a.parameter_field='asalpasien_cd' ORDER BY a.parameter_cd ASC";
    } else if($cookie['search']['filter'] == "04") {  // per dokter pj
      $tbl_field = " a.pegawai_id as col_id, a.pegawai_nm as col_nm, ";
      $tbl_join  = " b.dokter_id=a.pegawai_id ";
      $tbl_join .= $sql_where;
      $tbl_from  = " FROM mst_pegawai a WHERE a.jenisparamedis_cd='01' ORDER BY a.pegawai_nm ASC";
    }

    $sql = "SELECT 
             a.*,
             (jml_kelompok_01_l+jml_kelompok_01_p+jml_kelompok_02_l+jml_kelompok_02_p+jml_kelompok_03_l+jml_kelompok_03_p+jml_kelompok_04_l+jml_kelompok_04_p+jml_kelompok_05_l+jml_kelompok_05_p+jml_kelompok_06_l+jml_kelompok_06_p+jml_kelompok_07_l+jml_kelompok_07_p+jml_kelompok_08_l+jml_kelompok_08_p+jml_kelompok_09_l+jml_kelompok_09_p+jml_kelompok_10_l+jml_kelompok_10_p+jml_kelompok_11_l+jml_kelompok_11_p+jml_kelompok_12_l+jml_kelompok_12_p) as jml_kelompok_total
            FROM 
            (
             SELECT 
              $tbl_field
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='01' AND b.sex_cd='L') as jml_kelompok_01_l,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='01' AND b.sex_cd='P') as jml_kelompok_01_p,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='02' AND b.sex_cd='L') as jml_kelompok_02_l,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='02' AND b.sex_cd='P') as jml_kelompok_02_p,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='03' AND b.sex_cd='L') as jml_kelompok_03_l,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='03' AND b.sex_cd='P') as jml_kelompok_03_p,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='04' AND b.sex_cd='L') as jml_kelompok_04_l,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='04' AND b.sex_cd='P') as jml_kelompok_04_p,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='05' AND b.sex_cd='L') as jml_kelompok_05_l,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='05' AND b.sex_cd='P') as jml_kelompok_05_p,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='06' AND b.sex_cd='L') as jml_kelompok_06_l,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='06' AND b.sex_cd='P') as jml_kelompok_06_p,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='07' AND b.sex_cd='L') as jml_kelompok_07_l,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='07' AND b.sex_cd='P') as jml_kelompok_07_p,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='08' AND b.sex_cd='L') as jml_kelompok_08_l,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='08' AND b.sex_cd='P') as jml_kelompok_08_p,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='09' AND b.sex_cd='L') as jml_kelompok_09_l,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='09' AND b.sex_cd='P') as jml_kelompok_09_p,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='10' AND b.sex_cd='L') as jml_kelompok_10_l,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='10' AND b.sex_cd='P') as jml_kelompok_10_p,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='11' AND b.sex_cd='L') as jml_kelompok_11_l,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='11' AND b.sex_cd='P') as jml_kelompok_11_p,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='12' AND b.sex_cd='L') as jml_kelompok_12_l,
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.kelompokumur_id='12' AND b.sex_cd='P') as jml_kelompok_12_p,  
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.jeniskunjungan_cd='B') as jml_kunj_baru,  
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join AND b.jeniskunjungan_cd='L') as jml_kunj_lama 
             $tbl_from
            ) a";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
  
}