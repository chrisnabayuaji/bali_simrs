<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_c_per_variable extends CI_Model {

  public function list_data($cookie)
  {
    if (@$cookie['search']['periode_data'] == '01') {
      $sql_where = "AND DATE(b.tgl_registrasi) = '".to_date($this->db->escape_like_str($cookie['search']['tgl']))."'";
    }elseif (@$cookie['search']['periode_data'] == '02') {
      $sql_where = "AND DATE(b.tgl_registrasi) BETWEEN '".to_date($this->db->escape_like_str($cookie['search']['tgl_awal']))."' AND '".to_date($this->db->escape_like_str($cookie['search']['tgl_akhir']))."'";
    }elseif (@$cookie['search']['periode_data'] == '03') {
      $sql_where = "AND YEAR(b.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['tahun'])."' AND MONTH(b.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['bulan'])."'";
    }elseif (@$cookie['search']['periode_data'] == '04') {
      $sql_where = "AND YEAR(b.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['tahun'])."'";
    }

    if(@$cookie['search']['filter'] == "01") {    // lokasi pelayanan
      $tbl_field = " a.lokasi_id as col_id, a.lokasi_nm as col_nm, ";
      $tbl_join  = " b.lokasi_id=a.lokasi_id ";
      $tbl_join .= $sql_where;
      $tbl_from  = " FROM mst_lokasi a WHERE a.jenisreg_st IS NOT NULL ORDER BY a.lokasi_id ASC";
    } else if(@$cookie['search']['filter'] == "02") {  // jenis pasien
      $tbl_field = " a.jenispasien_id as col_id, a.jenispasien_nm as col_nm, ";
      $tbl_join  = " b.jenispasien_id=a.jenispasien_id ";
      $tbl_join .= $sql_where;
      $tbl_from  = " FROM mst_jenis_pasien a ORDER BY a.jenispasien_id ASC";
    } else if(@$cookie['search']['filter'] == "03") {  // asal pasien
      $tbl_field = " a.parameter_cd as col_id, a.parameter_val as col_nm, ";
      $tbl_join  = " b.asalpasien_cd=a.parameter_cd ";
      $tbl_join .= $sql_where;
      $tbl_from  = " FROM mst_parameter a WHERE a.parameter_field='asalpasien_cd' ORDER BY a.parameter_cd ASC";
    } else if(@$cookie['search']['filter'] == "04") {  // dokter pj
      $tbl_field = " a.pegawai_id as col_id, a.pegawai_nm as col_nm, ";
      $tbl_join  = " b.dokter_id=a.pegawai_id ";
      $tbl_join .= $sql_where;
      $tbl_from  = " FROM mst_pegawai a WHERE a.jenisparamedis_cd='01' ORDER BY a.pegawai_nm ASC";
    } else if(@$cookie['search']['filter'] == "05") {  // jenis kelamin
      $tbl_field = " a.parameter_cd as col_id, a.parameter_val as col_nm, ";
      $tbl_join  = " b.sex_cd=a.parameter_cd ";
      $tbl_join .= $sql_where;
      $tbl_from  = " FROM mst_parameter a WHERE a.parameter_field='sex_cd' ORDER BY a.parameter_cd ASC";
    } else if(@$cookie['search']['filter'] == "06") {  // jenis kunjungan
      $tbl_field = " a.parameter_cd as col_id, a.parameter_val as col_nm, ";
      $tbl_join  = " b.jeniskunjungan_cd=a.parameter_cd ";
      $tbl_join .= $sql_where;
      $tbl_from  = " FROM mst_parameter a WHERE a.parameter_field='jeniskunjungan_cd' ORDER BY a.parameter_cd ASC";
    }

    $sql = "SELECT 
             a.*,
            jml_data
            FROM 
            (
             SELECT 
              $tbl_field
              (SELECT COUNT(reg_id) FROM reg_pasien b WHERE $tbl_join) as jml_data
             $tbl_from
            ) a";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
  
}