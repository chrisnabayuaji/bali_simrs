<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_c_per_wilayah extends CI_Model {

  public function list_data($cookie)
  {
    if (@$cookie['search']['periode_data'] == '01') {
      $sql_where = "AND DATE(a.tgl_registrasi) = '".to_date($this->db->escape_like_str($cookie['search']['tgl']))."'";
    }elseif (@$cookie['search']['periode_data'] == '02') {
      $sql_where = "AND DATE(a.tgl_registrasi) BETWEEN '".to_date($this->db->escape_like_str($cookie['search']['tgl_awal']))."' AND '".to_date($this->db->escape_like_str($cookie['search']['tgl_akhir']))."'";
    }elseif (@$cookie['search']['periode_data'] == '03') {
      $sql_where = "AND YEAR(a.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['tahun'])."' AND MONTH(a.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['bulan'])."'";
    }elseif (@$cookie['search']['periode_data'] == '04') {
      $sql_where = "AND YEAR(a.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['tahun'])."'";
    }

    $sql = "SELECT 
      COUNT(reg_id) AS jml_data, 
        a.* 
      FROM reg_pasien a 
      WHERE 0 = 0 $sql_where 
      GROUP BY a.wilayah_id";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
  
}