<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_data_kesakitan extends CI_Model {

  public function list_data_jenis_kelamin($cookie)
  {
    if (@$cookie['search']['periode_data'] == '01') {
      $sql_where = "AND DATE(b.tgl_registrasi) = '".to_date($this->db->escape_like_str($cookie['search']['tgl']))."'";
    }elseif (@$cookie['search']['periode_data'] == '02') {
      $sql_where = "AND DATE(b.tgl_registrasi) BETWEEN '".to_date($this->db->escape_like_str($cookie['search']['tgl_awal']))."' AND '".to_date($this->db->escape_like_str($cookie['search']['tgl_akhir']))."'";
    }elseif (@$cookie['search']['periode_data'] == '03') {
      $sql_where = "AND YEAR(b.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['tahun'])."' AND MONTH(b.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['bulan'])."'";
    }elseif (@$cookie['search']['periode_data'] == '04') {
      $sql_where = "AND YEAR(b.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['tahun'])."'";
    }

    $sql_where_lanjutan = "";
    if ($cookie['search']['filter_lanjutan'] == 'menular') {
      $sql_where_lanjutan .= "AND a.menular = '1'";
    }elseif ($cookie['search']['filter_lanjutan'] == 'tidak_menular') {
      $sql_where_lanjutan .= "AND a.menular = '0'";
    }

    $sql = "SELECT * FROM 
            (
              SELECT 
               a.*,
               (jml_kelompok_01_l+jml_kelompok_01_p+jml_kelompok_02_l+jml_kelompok_02_p+jml_kelompok_03_l+jml_kelompok_03_p+jml_kelompok_04_l+jml_kelompok_04_p+jml_kelompok_05_l+jml_kelompok_05_p+jml_kelompok_06_l+jml_kelompok_06_p+jml_kelompok_07_l+jml_kelompok_07_p+jml_kelompok_08_l+jml_kelompok_08_p+jml_kelompok_09_l+jml_kelompok_09_p+jml_kelompok_10_l+jml_kelompok_10_p+jml_kelompok_11_l+jml_kelompok_11_p+jml_kelompok_12_l+jml_kelompok_12_p) as jml_kelompok_total
              FROM 
              (
               SELECT 
                a.penyakit_id,a.penyakit_nm,a.icdx,a.menular,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='01' AND b.sex_cd='L' $sql_where) as jml_kelompok_01_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='01' AND b.sex_cd='P' $sql_where) as jml_kelompok_01_p,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='02' AND b.sex_cd='L' $sql_where) as jml_kelompok_02_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='02' AND b.sex_cd='P' $sql_where) as jml_kelompok_02_p,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='03' AND b.sex_cd='L' $sql_where) as jml_kelompok_03_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='03' AND b.sex_cd='P' $sql_where) as jml_kelompok_03_p,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='04' AND b.sex_cd='L' $sql_where) as jml_kelompok_04_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='04' AND b.sex_cd='P' $sql_where) as jml_kelompok_04_p,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='05' AND b.sex_cd='L' $sql_where) as jml_kelompok_05_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='05' AND b.sex_cd='P' $sql_where) as jml_kelompok_05_p,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='06' AND b.sex_cd='L' $sql_where) as jml_kelompok_06_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='06' AND b.sex_cd='P' $sql_where) as jml_kelompok_06_p,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='07' AND b.sex_cd='L' $sql_where) as jml_kelompok_07_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='07' AND b.sex_cd='P' $sql_where) as jml_kelompok_07_p,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='08' AND b.sex_cd='L' $sql_where) as jml_kelompok_08_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='08' AND b.sex_cd='P' $sql_where) as jml_kelompok_08_p,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='09' AND b.sex_cd='L' $sql_where) as jml_kelompok_09_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='09' AND b.sex_cd='P' $sql_where) as jml_kelompok_09_p,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='10' AND b.sex_cd='L' $sql_where) as jml_kelompok_10_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='10' AND b.sex_cd='P' $sql_where) as jml_kelompok_10_p,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='11' AND b.sex_cd='L' $sql_where) as jml_kelompok_11_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='11' AND b.sex_cd='P' $sql_where) as jml_kelompok_11_p,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='12' AND b.sex_cd='L' $sql_where) as jml_kelompok_12_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='12' AND b.sex_cd='P' $sql_where) as jml_kelompok_12_p
               FROM mst_penyakit a WHERE 1 
               $sql_where_lanjutan
               ORDER BY a.icdx ASC 
              ) a 
            ) b WHERE b.jml_kelompok_total > 0
            ORDER BY b.jml_kelompok_total DESC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function list_data_jenis_kunjungan($cookie)
  {
    if (@$cookie['search']['periode_data'] == '01') {
      $sql_where = "AND DATE(b.tgl_registrasi) = '".to_date($this->db->escape_like_str($cookie['search']['tgl']))."'";
    }elseif (@$cookie['search']['periode_data'] == '02') {
      $sql_where = "AND DATE(b.tgl_registrasi) BETWEEN '".to_date($this->db->escape_like_str($cookie['search']['tgl_awal']))."' AND '".to_date($this->db->escape_like_str($cookie['search']['tgl_akhir']))."'";
    }elseif (@$cookie['search']['periode_data'] == '03') {
      $sql_where = "AND YEAR(b.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['tahun'])."' AND MONTH(b.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['bulan'])."'";
    }elseif (@$cookie['search']['periode_data'] == '04') {
      $sql_where = "AND YEAR(b.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['tahun'])."'";
    }

    $sql_where_lanjutan = "";
    if ($cookie['search']['filter_lanjutan'] == 'menular') {
      $sql_where_lanjutan .= "AND a.menular = '1'";
    }elseif ($cookie['search']['filter_lanjutan'] == 'tidak_menular') {
      $sql_where_lanjutan .= "AND a.menular = '0'";
    }

    $sql = "SELECT * FROM 
            (
              SELECT 
               a.*,
               (jml_kelompok_01_b+jml_kelompok_01_l+jml_kelompok_02_b+jml_kelompok_02_l+jml_kelompok_03_b+jml_kelompok_03_l+jml_kelompok_04_b+jml_kelompok_04_l+jml_kelompok_05_b+jml_kelompok_05_l+jml_kelompok_06_b+jml_kelompok_06_l+jml_kelompok_07_b+jml_kelompok_07_l+jml_kelompok_08_b+jml_kelompok_08_l+jml_kelompok_09_b+jml_kelompok_09_l+jml_kelompok_10_b+jml_kelompok_10_l+jml_kelompok_11_b+jml_kelompok_11_l+jml_kelompok_12_b+jml_kelompok_12_l) as jml_kelompok_total
              FROM 
              (
               SELECT 
                a.penyakit_id,a.penyakit_nm,a.icdx,a.menular,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='01' AND b.jeniskunjungan_cd='B' $sql_where) as jml_kelompok_01_b,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='01' AND b.jeniskunjungan_cd='L' $sql_where) as jml_kelompok_01_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='02' AND b.jeniskunjungan_cd='B' $sql_where) as jml_kelompok_02_b,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='02' AND b.jeniskunjungan_cd='L' $sql_where) as jml_kelompok_02_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='03' AND b.jeniskunjungan_cd='B' $sql_where) as jml_kelompok_03_b,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='03' AND b.jeniskunjungan_cd='L' $sql_where) as jml_kelompok_03_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='04' AND b.jeniskunjungan_cd='B' $sql_where) as jml_kelompok_04_b,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='04' AND b.jeniskunjungan_cd='L' $sql_where) as jml_kelompok_04_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='05' AND b.jeniskunjungan_cd='B' $sql_where) as jml_kelompok_05_b,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='05' AND b.jeniskunjungan_cd='L' $sql_where) as jml_kelompok_05_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='06' AND b.jeniskunjungan_cd='B' $sql_where) as jml_kelompok_06_b,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='06' AND b.jeniskunjungan_cd='L' $sql_where) as jml_kelompok_06_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='07' AND b.jeniskunjungan_cd='B' $sql_where) as jml_kelompok_07_b,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='07' AND b.jeniskunjungan_cd='L' $sql_where) as jml_kelompok_07_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='08' AND b.jeniskunjungan_cd='B' $sql_where) as jml_kelompok_08_b,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='08' AND b.jeniskunjungan_cd='L' $sql_where) as jml_kelompok_08_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='09' AND b.jeniskunjungan_cd='B' $sql_where) as jml_kelompok_09_b,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='09' AND b.jeniskunjungan_cd='L' $sql_where) as jml_kelompok_09_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='10' AND b.jeniskunjungan_cd='B' $sql_where) as jml_kelompok_10_b,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='10' AND b.jeniskunjungan_cd='L' $sql_where) as jml_kelompok_10_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='11' AND b.jeniskunjungan_cd='B' $sql_where) as jml_kelompok_11_b,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='11' AND b.jeniskunjungan_cd='L' $sql_where) as jml_kelompok_11_l,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='12' AND b.jeniskunjungan_cd='B' $sql_where) as jml_kelompok_12_b,
                (SELECT COUNT(reg_id) FROM reg_pasien b WHERE b.diagnosisakhir_penyakit_id=a.penyakit_id AND b.kelompokumur_id='12' AND b.jeniskunjungan_cd='L' $sql_where) as jml_kelompok_12_l
               FROM mst_penyakit a WHERE 1 
               $sql_where_lanjutan
               ORDER BY a.icdx ASC 
              ) a 
            ) b WHERE b.jml_kelompok_total > 0
            ORDER BY b.jml_kelompok_total DESC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
  
}