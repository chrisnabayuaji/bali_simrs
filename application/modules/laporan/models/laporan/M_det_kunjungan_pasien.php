<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_det_kunjungan_pasien extends CI_Model {

  public function list_data($cookie)
  {
    if (@$cookie['search']['periode_data'] == '01') {
      $sql_where = "AND DATE(b.tgl_registrasi) = '".to_date($this->db->escape_like_str($cookie['search']['tgl']))."'";
    }elseif (@$cookie['search']['periode_data'] == '02') {
      $sql_where = "AND DATE(b.tgl_registrasi) BETWEEN '".to_date($this->db->escape_like_str($cookie['search']['tgl_awal']))."' AND '".to_date($this->db->escape_like_str($cookie['search']['tgl_akhir']))."'";
    }elseif (@$cookie['search']['periode_data'] == '03') {
      $sql_where = "AND YEAR(b.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['tahun'])."' AND MONTH(b.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['bulan'])."'";
    }elseif (@$cookie['search']['periode_data'] == '04') {
      $sql_where = "AND YEAR(b.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['tahun'])."'";
    }

    if (@$cookie['search']['jenisreg_st'] !='') {
      $sql_where .= " AND d.jenisreg_st='".$this->db->escape_like_str($cookie['search']['jenisreg_st'])."'";
    }

    if (@$cookie['search']['lokasi_id'] !='') {
      $sql_where .= " AND b.lokasi_id='".$this->db->escape_like_str($cookie['search']['lokasi_id'])."'";
    }

    $sql = "SELECT b.*,c.icdx,c.penyakit_nm,e.jenispasien_nm,d.lokasi_nm 
            FROM reg_pasien b 
            LEFT JOIN mst_penyakit c ON b.diagnosisakhir_penyakit_id=c.penyakit_id
            INNER JOIN mst_lokasi d ON b.lokasi_id=d.lokasi_id
            LEFT JOIN mst_jenis_pasien e ON b.jenispasien_id=e.jenispasien_id
            WHERE 1 $sql_where
            ORDER BY b.reg_id ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
  
}