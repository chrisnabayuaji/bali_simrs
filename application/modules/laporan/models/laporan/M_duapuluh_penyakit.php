<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_duapuluh_penyakit extends CI_Model {

  public function list_data($cookie)
  {
    if (@$cookie['search']['periode_data'] == '01') {
      $sql_where = "AND DATE(b.tgl_registrasi) = '".to_date($this->db->escape_like_str($cookie['search']['tgl']))."'";
    }elseif (@$cookie['search']['periode_data'] == '02') {
      $sql_where = "AND DATE(b.tgl_registrasi) BETWEEN '".to_date($this->db->escape_like_str($cookie['search']['tgl_awal']))."' AND '".to_date($this->db->escape_like_str($cookie['search']['tgl_akhir']))."'";
    }elseif (@$cookie['search']['periode_data'] == '03') {
      $sql_where = "AND YEAR(b.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['tahun'])."' AND MONTH(b.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['bulan'])."'";
    }elseif (@$cookie['search']['periode_data'] == '04') {
      $sql_where = "AND YEAR(b.tgl_registrasi)='".$this->db->escape_like_str($cookie['search']['tahun'])."'";
    }

    $sql = "SELECT 
             a.penyakit_id,a.penyakit_nm,a.icdx,a.menular,
             b.jml_diagnosis 
            FROM mst_penyakit a 
            INNER JOIN 
            (
             SELECT b.diagnosisakhir_penyakit_id, COUNT(b.reg_id) as jml_diagnosis FROM reg_pasien b WHERE 1 $sql_where
            ) b ON b.diagnosisakhir_penyakit_id=a.penyakit_id
            WHERE b.jml_diagnosis > 0
            ORDER BY b.jml_diagnosis DESC LIMIT 20";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
  
}