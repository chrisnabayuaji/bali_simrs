<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_harian_obat extends CI_Model
{

  public function list_data($cookie)
  {
    $where = "WHERE 1 = 1 ";
    if (@$cookie['search']['tahun'] != '') {
      $where .= "AND YEAR(x.tgl_catat) = '" . $this->db->escape_like_str($cookie['search']['tahun']) . "' ";
    }
    if (@$cookie['search']['bulan'] != '') {
      $where .= "AND MONTH(x.tgl_catat) = '" . $this->db->escape_like_str($cookie['search']['bulan']) . "' ";
    }
    if (@$cookie['search']['jenisreg_st'] != '') {
      $where .= "AND x.jenisreg_st = '" . $this->db->escape_like_str($cookie['search']['jenisreg_st']) . "' ";
    }
    if (@$cookie['search']['lokasi_id'] != '') {
      $where .= "AND x.lokasi_id = '" . $this->db->escape_like_str($cookie['search']['lokasi_id']) . "' ";
    }
    $sql = "SELECT 
              a.obat_id, a.obat_nm, c.parameter_val as satuan_nm,
              IF(b.tgl_1 IS NULL, 0, tgl_1) as tgl_1,
              IF(b.tgl_2 IS NULL, 0, tgl_2) as tgl_2,
              IF(b.tgl_3 IS NULL, 0, tgl_3) as tgl_3,
              IF(b.tgl_4 IS NULL, 0, tgl_4) as tgl_4,
              IF(b.tgl_5 IS NULL, 0, tgl_5) as tgl_5,
              IF(b.tgl_6 IS NULL, 0, tgl_6) as tgl_6,
              IF(b.tgl_7 IS NULL, 0, tgl_7) as tgl_7,
              IF(b.tgl_8 IS NULL, 0, tgl_8) as tgl_8,
              IF(b.tgl_9 IS NULL, 0, tgl_9) as tgl_9,
              IF(b.tgl_10 IS NULL, 0, tgl_10) as tgl_10,
              IF(b.tgl_11 IS NULL, 0, tgl_11) as tgl_11,
              IF(b.tgl_12 IS NULL, 0, tgl_12) as tgl_12,
              IF(b.tgl_13 IS NULL, 0, tgl_13) as tgl_13,
              IF(b.tgl_14 IS NULL, 0, tgl_14) as tgl_14,
              IF(b.tgl_15 IS NULL, 0, tgl_15) as tgl_15,
              IF(b.tgl_16 IS NULL, 0, tgl_16) as tgl_16,
              IF(b.tgl_17 IS NULL, 0, tgl_17) as tgl_17,
              IF(b.tgl_18 IS NULL, 0, tgl_18) as tgl_18,
              IF(b.tgl_19 IS NULL, 0, tgl_19) as tgl_19,
              IF(b.tgl_20 IS NULL, 0, tgl_20) as tgl_20,
              IF(b.tgl_21 IS NULL, 0, tgl_21) as tgl_21,
              IF(b.tgl_22 IS NULL, 0, tgl_22) as tgl_22,
              IF(b.tgl_23 IS NULL, 0, tgl_23) as tgl_23,
              IF(b.tgl_24 IS NULL, 0, tgl_24) as tgl_24,
              IF(b.tgl_25 IS NULL, 0, tgl_25) as tgl_25,
              IF(b.tgl_26 IS NULL, 0, tgl_26) as tgl_26,
              IF(b.tgl_27 IS NULL, 0, tgl_27) as tgl_27,
              IF(b.tgl_28 IS NULL, 0, tgl_28) as tgl_28,
              IF(b.tgl_29 IS NULL, 0, tgl_29) as tgl_29,
              IF(b.tgl_30 IS NULL, 0, tgl_30) as tgl_30,
              IF(b.tgl_31 IS NULL, 0, tgl_31) as tgl_31,
              IF((b.tgl_1 + b.tgl_2 + b.tgl_3 + b.tgl_4 + b.tgl_5 + b.tgl_6 + b.tgl_7 + b.tgl_8 + b.tgl_9 + b.tgl_10 + b.tgl_11 + b.tgl_12 + b.tgl_13 + b.tgl_14 + b.tgl_15 + b.tgl_16 + b.tgl_17 + b.tgl_18 + b.tgl_19 + b.tgl_21 + b.tgl_22 + b.tgl_23 + b.tgl_24 + b.tgl_25 + b.tgl_26 + b.tgl_27 + b.tgl_28 + b.tgl_29 + b.tgl_30 + b.tgl_31) IS NULL, 0,  (b.tgl_1 + b.tgl_2 + b.tgl_3 + b.tgl_4 + b.tgl_5 + b.tgl_6 + b.tgl_7 + b.tgl_8 + b.tgl_9 + b.tgl_10 + b.tgl_11 + b.tgl_12 + b.tgl_13 + b.tgl_14 + b.tgl_15 + b.tgl_16 + b.tgl_17 + b.tgl_18 + b.tgl_19 + b.tgl_21 + b.tgl_22 + b.tgl_23 + b.tgl_24 + b.tgl_25 + b.tgl_26 + b.tgl_27 + b.tgl_28 + b.tgl_29 + b.tgl_30 + b.tgl_31)) as total
            FROM mst_obat a 
            LEFT JOIN (
              SELECT
                x.obat_id,
                SUM(CASE WHEN DAY(x.tgl_catat) = '01' THEN x.qty ELSE 0 END) as tgl_1,
                SUM(CASE WHEN DAY(x.tgl_catat) = '02' THEN x.qty ELSE 0 END) as tgl_2,
                SUM(CASE WHEN DAY(x.tgl_catat) = '03' THEN x.qty ELSE 0 END) as tgl_3,
                SUM(CASE WHEN DAY(x.tgl_catat) = '04' THEN x.qty ELSE 0 END) as tgl_4,
                SUM(CASE WHEN DAY(x.tgl_catat) = '05' THEN x.qty ELSE 0 END) as tgl_5,
                SUM(CASE WHEN DAY(x.tgl_catat) = '06' THEN x.qty ELSE 0 END) as tgl_6,
                SUM(CASE WHEN DAY(x.tgl_catat) = '07' THEN x.qty ELSE 0 END) as tgl_7,
                SUM(CASE WHEN DAY(x.tgl_catat) = '08' THEN x.qty ELSE 0 END) as tgl_8,
                SUM(CASE WHEN DAY(x.tgl_catat) = '09' THEN x.qty ELSE 0 END) as tgl_9,
                SUM(CASE WHEN DAY(x.tgl_catat) = '10' THEN x.qty ELSE 0 END) as tgl_10,
                SUM(CASE WHEN DAY(x.tgl_catat) = '11' THEN x.qty ELSE 0 END) as tgl_11,
                SUM(CASE WHEN DAY(x.tgl_catat) = '12' THEN x.qty ELSE 0 END) as tgl_12,
                SUM(CASE WHEN DAY(x.tgl_catat) = '13' THEN x.qty ELSE 0 END) as tgl_13,
                SUM(CASE WHEN DAY(x.tgl_catat) = '14' THEN x.qty ELSE 0 END) as tgl_14,
                SUM(CASE WHEN DAY(x.tgl_catat) = '15' THEN x.qty ELSE 0 END) as tgl_15,
                SUM(CASE WHEN DAY(x.tgl_catat) = '16' THEN x.qty ELSE 0 END) as tgl_16,
                SUM(CASE WHEN DAY(x.tgl_catat) = '17' THEN x.qty ELSE 0 END) as tgl_17,
                SUM(CASE WHEN DAY(x.tgl_catat) = '18' THEN x.qty ELSE 0 END) as tgl_18,
                SUM(CASE WHEN DAY(x.tgl_catat) = '19' THEN x.qty ELSE 0 END) as tgl_19,
                SUM(CASE WHEN DAY(x.tgl_catat) = '20' THEN x.qty ELSE 0 END) as tgl_20,
                SUM(CASE WHEN DAY(x.tgl_catat) = '21' THEN x.qty ELSE 0 END) as tgl_21,
                SUM(CASE WHEN DAY(x.tgl_catat) = '22' THEN x.qty ELSE 0 END) as tgl_22,
                SUM(CASE WHEN DAY(x.tgl_catat) = '23' THEN x.qty ELSE 0 END) as tgl_23,
                SUM(CASE WHEN DAY(x.tgl_catat) = '24' THEN x.qty ELSE 0 END) as tgl_24,
                SUM(CASE WHEN DAY(x.tgl_catat) = '25' THEN x.qty ELSE 0 END) as tgl_25,
                SUM(CASE WHEN DAY(x.tgl_catat) = '26' THEN x.qty ELSE 0 END) as tgl_26,
                SUM(CASE WHEN DAY(x.tgl_catat) = '27' THEN x.qty ELSE 0 END) as tgl_27,
                SUM(CASE WHEN DAY(x.tgl_catat) = '28' THEN x.qty ELSE 0 END) as tgl_28,
                SUM(CASE WHEN DAY(x.tgl_catat) = '29' THEN x.qty ELSE 0 END) as tgl_29,
                SUM(CASE WHEN DAY(x.tgl_catat) = '30' THEN x.qty ELSE 0 END) as tgl_30,
                SUM(CASE WHEN DAY(x.tgl_catat) = '31' THEN x.qty ELSE 0 END) as tgl_31,
                x.tgl_catat 
              FROM dat_farmasi x 
              $where
              GROUP BY x.obat_id
            ) b ON b.obat_id = a.obat_id 
            LEFT JOIN mst_parameter c ON a.satuan_cd = c.parameter_cd AND c.parameter_field='satuan_cd'";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
}
