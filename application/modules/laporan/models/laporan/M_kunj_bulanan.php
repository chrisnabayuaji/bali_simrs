<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kunj_bulanan extends CI_Model {

  public function where($cookie)
  {
    $where = "WHERE b.lokasi_id=a.lokasi_id ";
    if (@$cookie['search']['tahun'] != '') {
      $where .= "AND YEAR(b.tgl_registrasi) = '".$this->db->escape_like_str($cookie['search']['tahun'])."' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
             a.*,
             (jml_bln_01+jml_bln_02+jml_bln_03+jml_bln_04+jml_bln_05+jml_bln_06+jml_bln_07+jml_bln_08+jml_bln_09+jml_bln_10+jml_bln_11+jml_bln_12) as jml_bln_total
            FROM 
            (
             SELECT 
              a.lokasi_id,a.lokasi_nm,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND MONTH(b.tgl_registrasi)='01') as jml_bln_01,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND MONTH(b.tgl_registrasi)='02') as jml_bln_02,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND MONTH(b.tgl_registrasi)='03') as jml_bln_03,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND MONTH(b.tgl_registrasi)='04') as jml_bln_04,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND MONTH(b.tgl_registrasi)='05') as jml_bln_05,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND MONTH(b.tgl_registrasi)='06') as jml_bln_06,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND MONTH(b.tgl_registrasi)='07') as jml_bln_07,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND MONTH(b.tgl_registrasi)='08') as jml_bln_08,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND MONTH(b.tgl_registrasi)='09') as jml_bln_09,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND MONTH(b.tgl_registrasi)='10') as jml_bln_10,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND MONTH(b.tgl_registrasi)='11') as jml_bln_11,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND MONTH(b.tgl_registrasi)='12') as jml_bln_12
             FROM mst_lokasi a 
             WHERE a.jenisreg_st='1' ORDER BY a.lokasi_id ASC
            ) a";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
  
}