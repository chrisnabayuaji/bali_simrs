<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kunj_harian extends CI_Model {

  public function where($cookie)
  {
    $where = "WHERE b.lokasi_id=a.lokasi_id ";
    if (@$cookie['search']['tahun'] != '') {
      $where .= "AND YEAR(b.tgl_registrasi) = '".$this->db->escape_like_str($cookie['search']['tahun'])."' ";
    }
    if (@$cookie['search']['bulan'] != '') {
      $where .= "AND MONTH(b.tgl_registrasi) = '".$this->db->escape_like_str($cookie['search']['bulan'])."' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
            a.*,
            (jml_tgl_01+jml_tgl_02+jml_tgl_03+jml_tgl_04+jml_tgl_05+jml_tgl_06+jml_tgl_07+jml_tgl_08+jml_tgl_09+jml_tgl_10+jml_tgl_11+jml_tgl_12+jml_tgl_13+jml_tgl_14+jml_tgl_15+jml_tgl_16+jml_tgl_17+jml_tgl_18+jml_tgl_19+jml_tgl_20+jml_tgl_21+jml_tgl_22+jml_tgl_23+jml_tgl_24+jml_tgl_25+jml_tgl_26+jml_tgl_27+jml_tgl_28+jml_tgl_29+jml_tgl_30+jml_tgl_31) as jml_tgl_total
            FROM 
            (
             SELECT 
              a.lokasi_id,a.lokasi_nm,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='01') as jml_tgl_01,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='02') as jml_tgl_02,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='03') as jml_tgl_03,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='04') as jml_tgl_04,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='05') as jml_tgl_05,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='06') as jml_tgl_06,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='07') as jml_tgl_07,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='08') as jml_tgl_08,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='09') as jml_tgl_09,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='10') as jml_tgl_10,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='11') as jml_tgl_11,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='12') as jml_tgl_12,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='13') as jml_tgl_13,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='14') as jml_tgl_14,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='15') as jml_tgl_15,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='16') as jml_tgl_16,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='17') as jml_tgl_17,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='18') as jml_tgl_18,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='19') as jml_tgl_19,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='20') as jml_tgl_20,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='21') as jml_tgl_21,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='22') as jml_tgl_22,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='23') as jml_tgl_23,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='24') as jml_tgl_24,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='25') as jml_tgl_25,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='26') as jml_tgl_26,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='27') as jml_tgl_27,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='28') as jml_tgl_28,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='29') as jml_tgl_29,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='30') as jml_tgl_30,
              (SELECT COUNT(reg_id) FROM reg_pasien b $where AND DAY(b.tgl_registrasi)='31') as jml_tgl_31 
             FROM mst_lokasi a 
             WHERE a.jenisreg_st='1' 
             ORDER BY a.lokasi_id ASC
            ) a";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
  
}