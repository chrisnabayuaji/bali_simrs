<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_obat_per_dokter extends CI_Model {

  public function list_data($cookie)
  {
    $sql_where = '';
    if (@$cookie['search']['dokter_id'] !='') {
      $sql_where .= " AND c.dokter_id='".$this->db->escape_like_str($cookie['search']['dokter_id'])."'";
    }
    if (@$cookie['search']['tgl_awal'] !='' && @$cookie['search']['tgl_akhir'] !='') {
      $sql_where .= " AND DATE(a.tgl_catat) BETWEEN '".to_date($this->db->escape_like_str($cookie['search']['tgl_awal']))."' AND '".to_date($this->db->escape_like_str($cookie['search']['tgl_akhir']))."'";
    }

    $sql = "SELECT a.*,b.qty,a.grand_jml_tagihan as total_biaya_obat,c.pasien_nm,c.alamat,c.kelurahan,c.kecamatan,c.kabupaten,c.provinsi,d.lokasi_nm,e.pegawai_nm
            FROM dat_resep a 
            LEFT JOIN 
            (
             SELECT SUM(b.qty) as qty,b.resep_id FROM dat_farmasi b GROUP BY b.resep_id
            ) b ON a.resep_id=b.resep_id
            INNER JOIN reg_pasien c ON a.reg_id=c.reg_id
            INNER JOIN mst_lokasi d ON c.lokasi_id=d.lokasi_id
            INNER JOIN mst_pegawai e ON c.dokter_id=e.pegawai_id
            WHERE 1 $sql_where
            ORDER BY a.tgl_catat ASC, a.resep_id ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
  
}