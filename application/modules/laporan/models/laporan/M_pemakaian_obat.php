<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pemakaian_obat extends CI_Model {

  public function list_resep($cookie)
  {
    if (@$cookie['search']['periode_data'] == '01') {
      $sql_where = "AND DATE(a.tgl_catat) = '".to_date($this->db->escape_like_str($cookie['search']['tgl']))."'";
    }elseif (@$cookie['search']['periode_data'] == '02') {
      $sql_where = "AND DATE(a.tgl_catat) BETWEEN '".to_date($this->db->escape_like_str($cookie['search']['tgl_awal']))."' AND '".to_date($this->db->escape_like_str($cookie['search']['tgl_akhir']))."'";
    }elseif (@$cookie['search']['periode_data'] == '03') {
      $sql_where = "AND YEAR(a.tgl_catat)='".$this->db->escape_like_str($cookie['search']['tahun'])."' AND MONTH(a.tgl_catat)='".$this->db->escape_like_str($cookie['search']['bulan'])."'";
    }elseif (@$cookie['search']['periode_data'] == '04') {
      $sql_where = "AND YEAR(a.tgl_catat)='".$this->db->escape_like_str($cookie['search']['tahun'])."'";
    }

    if (@$cookie['search']['jenisreg_st'] !='') {
      $sql_where .= " AND b.jenisreg_st='".$this->db->escape_like_str($cookie['search']['jenisreg_st'])."'";
    }

    if (@$cookie['search']['lokasi_id'] !='') {
      $sql_where .= " AND a.lokasi_id='".$this->db->escape_like_str($cookie['search']['lokasi_id'])."'";
    }

    $sql = "SELECT a.*,b.lokasi_nm
            FROM dat_resep a 
            INNER JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id
            WHERE 1 $sql_where
            ORDER BY a.tgl_catat ASC, a.resep_id ASC";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    // 
    foreach($result as $key => $val) {
      $result[$key]['list_detail'] = $this->list_detail($cookie, $val['resep_id']);
    }

    return $result;
  }

  public function list_detail($cookie, $resep_id='')
  {
    if (@$cookie['search']['periode_data'] == '01') {
      $sql_where = "AND DATE(a.tgl_catat) = '".to_date($this->db->escape_like_str($cookie['search']['tgl']))."'";
    }elseif (@$cookie['search']['periode_data'] == '02') {
      $sql_where = "AND DATE(a.tgl_catat) BETWEEN '".to_date($this->db->escape_like_str($cookie['search']['tgl_awal']))."' AND '".to_date($this->db->escape_like_str($cookie['search']['tgl_akhir']))."'";
    }elseif (@$cookie['search']['periode_data'] == '03') {
      $sql_where = "AND YEAR(a.tgl_catat)='".$this->db->escape_like_str($cookie['search']['tahun'])."' AND MONTH(a.tgl_catat)='".$this->db->escape_like_str($cookie['search']['bulan'])."'";
    }elseif (@$cookie['search']['periode_data'] == '04') {
      $sql_where = "AND YEAR(a.tgl_catat)='".$this->db->escape_like_str($cookie['search']['tahun'])."'";
    }

    if (@$cookie['search']['lokasi_id'] !='') {
      $sql_where .= " AND a.lokasi_id='".$this->db->escape_like_str($cookie['search']['lokasi_id'])."'";
    }

    if (@$resep_id !='') {
      $sql_where .= " AND a.resep_id='".$resep_id."'";
    }

    $sql = "SELECT a.*, b.satuan_cd
            FROM dat_farmasi a
            LEFT JOIN mst_obat b ON a.obat_id=b.obat_id 
            WHERE 1 $sql_where
            ORDER BY a.tgl_catat ASC, a.resep_id ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
  
}