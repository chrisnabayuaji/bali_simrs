<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_rm extends CI_Model {

  public function bor($cookie)
  {
    $start = $cookie['search']['tahun'].'-'.$cookie['search']['bulan'].'-01';
    $days = date('t', strtotime($start));

    $hari = '';
    for ($i = 0; $i < $days; $i++) {
      $date = date('Y-m-d',strtotime($start . "+$i days"));
      if ($i != $days-1) {
        $hari .= "(SELECT COUNT(a.reg_id) as jml_pasien FROM reg_pasien a WHERE a.kamar_id IS NOT NULL AND '$date' BETWEEN DATE(a.tgl_registrasi) AND DATE(IFNULL(a.tgl_pulang,NOW()))) UNION ALL ";
      }else{
        $hari .= "(SELECT COUNT(a.reg_id) as jml_pasien FROM reg_pasien a WHERE a.kamar_id IS NOT NULL AND '$date' BETWEEN DATE(a.tgl_registrasi) AND DATE(IFNULL(a.tgl_pulang,NOW()))) ";
      }
    }

    $sql = "SELECT 
            b.*, 
            IFNULL(ROUND((b.hari_perawatan / (b.bed*b.periode) * 100),2),0) as nilai
          FROM 
          (
            SELECT 
            SUM(a.jml_pasien) as hari_perawatan,
            (
              SELECT SUM(jml_bed) FROM mst_kamar) as bed,
                DAY(LAST_DAY('$start')) as periode
              FROM 
              (
                $hari
              ) as a
            ) as b";
    
    $res = $this->db->query($sql);
    return $res->row_array();
  }
  
  public function los($cookie)
  {
    $tahun = $cookie['search']['tahun'];
    $bulan = $cookie['search']['bulan'];
    $start = $cookie['search']['tahun'].'-'.$cookie['search']['bulan'].'-01';
    $days = date('t', strtotime($start));

    $hari = '';
    for ($i = 0; $i < $days; $i++) {
      $date = date('Y-m-d',strtotime($start . "+$i days"));
      if ($i != $days-1) {
        $hari .= "(SELECT COUNT(a.reg_id) as jml_pasien FROM reg_pasien a WHERE a.kamar_id IS NOT NULL AND '$date' BETWEEN DATE(a.tgl_registrasi) AND DATE(IFNULL(a.tgl_pulang,NOW()))) UNION ALL ";
      }else{
        $hari .= "(SELECT COUNT(a.reg_id) as jml_pasien FROM reg_pasien a WHERE a.kamar_id IS NOT NULL AND '$date' BETWEEN DATE(a.tgl_registrasi) AND DATE(IFNULL(a.tgl_pulang,NOW()))) ";
      }
    }

    $sql = "SELECT 
            b.*,
            IFNULL(ROUND((b.hari_perawatan / b.pasien_keluar),2),0) as nilai
          FROM 
          (
            SELECT 
            SUM(a.jml_pasien) as hari_perawatan,
            (SELECT COUNT(a.reg_id) FROM reg_pasien a WHERE a.kamar_id IS NOT NULL AND a.pulang_st='1' AND YEAR(a.tgl_pulang)='$tahun' AND MONTH(a.tgl_pulang)='$bulan') as pasien_keluar
            FROM 
            (
              $hari
            ) as a
          ) as b";
    
    $res = $this->db->query($sql);
    return $res->row_array();
  }

  public function toi($cookie)
  {
    $tahun = $cookie['search']['tahun'];
    $bulan = $cookie['search']['bulan'];
    $start = $cookie['search']['tahun'].'-'.$cookie['search']['bulan'].'-01';
    $days = date('t', strtotime($start));

    $hari = '';
    for ($i = 0; $i < $days; $i++) {
      $date = date('Y-m-d',strtotime($start . "+$i days"));
      if ($i != $days-1) {
        $hari .= "(SELECT COUNT(a.reg_id) as jml_pasien FROM reg_pasien a WHERE a.kamar_id IS NOT NULL AND '$date' BETWEEN DATE(a.tgl_registrasi) AND DATE(IFNULL(a.tgl_pulang,NOW()))) UNION ALL ";
      }else{
        $hari .= "(SELECT COUNT(a.reg_id) as jml_pasien FROM reg_pasien a WHERE a.kamar_id IS NOT NULL AND '$date' BETWEEN DATE(a.tgl_registrasi) AND DATE(IFNULL(a.tgl_pulang,NOW()))) "; 
      }
    }

    $sql = "SELECT 
              b.*,
              IFNULL(ROUND((((b.bed * b.periode) - b.hari_perawatan) / b.pasien_keluar),2),0) as nilai
            FROM 
            (
              SELECT 
              SUM(a.jml_pasien) as hari_perawatan,
              (SELECT SUM(jml_bed) FROM mst_kamar) as bed,
              DAY(LAST_DAY('$tahun-$bulan-01')) as periode,
              (SELECT COUNT(a.reg_id) FROM reg_pasien a WHERE a.kamar_id IS NOT NULL AND a.pulang_st='1' AND YEAR(a.tgl_pulang)='$tahun' AND MONTH(a.tgl_pulang)='$bulan') as pasien_keluar
              FROM 
              (
                $hari
              ) as a
            ) as b";
    
    $res = $this->db->query($sql);
    return $res->row_array();
  }
  
  public function bto($cookie)
  {
    $tahun = $cookie['search']['tahun'];
    $bulan = $cookie['search']['bulan'];
    $start = $cookie['search']['tahun'].'-'.$cookie['search']['bulan'].'-01';
    $days = date('t', strtotime($start));

    $sql = "SELECT 
              a.*,
              IFNULL(ROUND((a.pasien_keluar/a.bed),2),0) as nilai 
            FROM 
            (
              SELECT 
              a.pasien_keluar,
              (SELECT SUM(jml_bed) FROM mst_kamar) as bed
              FROM 
              (
                (SELECT COUNT(a.reg_id) as pasien_keluar FROM reg_pasien a WHERE a.kamar_id IS NOT NULL AND a.pulang_st='1' 
                AND YEAR(a.tgl_pulang)='$tahun' AND MONTH(a.tgl_pulang)='$bulan')
              ) as a
            ) as a";
    
    $res = $this->db->query($sql);
    return $res->row_array();
  }
  
  public function rekap($cookie)
  {
    $tahun = $cookie['search']['tahun'];
    $bulan = $cookie['search']['bulan'];
    $start = $cookie['search']['tahun'].'-'.$cookie['search']['bulan'].'-01';
    $days = date('t', strtotime($start));

    $result = array();
    for ($i=0; $i < $days; $i++) { 
      $date = date('Y-m-d', strtotime($start. "+ $i days"));
      $result[$i]['tanggal'] = to_date($date);
      $diff = intval(date_diff(date_create(date('Y-m-d')), date_create($date))->format('%R%a'));
      
      if($diff <= 0){
        $pasien_awal = $this->db->query("SELECT COUNT(reg_id) as pasien_awal FROM reg_pasien WHERE DATE(tgl_registrasi) < '$date' AND tgl_pulang IS NULL AND kamar_id IS NOT NULL")->row_array();
        $result[$i]['pasien_awal'] = $pasien_awal['pasien_awal'];
      }else{
        $result[$i]['pasien_awal'] = null;
      }

      if($diff <= 0){
        $pasien_masuk = $this->db->query("SELECT COUNT(reg_id) as pasien_masuk FROM reg_pasien WHERE DATE(tgl_registrasi) = '$date' AND kamar_id IS NOT NULL")->row_array();
        $result[$i]['pasien_masuk'] = $pasien_masuk['pasien_masuk'];
      }else{
        $result[$i]['pasien_masuk'] = null;
      }

      if($diff <= 0){
        $pasien_pulang = $this->db->query("SELECT COUNT(reg_id) as pasien_pulang FROM reg_pasien WHERE DATE(tgl_pulang) = '$date' AND kamar_id IS NOT NULL")->row_array();
        $result[$i]['pasien_pulang'] = $pasien_pulang['pasien_pulang'];
      }else{
        $result[$i]['pasien_pulang'] = null;
      }

      if($diff <= 0){
        $result[$i]['pasien_dirawat'] = $pasien_awal['pasien_awal'] + $pasien_masuk['pasien_masuk'] - $pasien_pulang['pasien_pulang'];
      }else{
        $result[$i]['pasien_dirawat'] = null;
      }

      if($diff <= 0){
        $hari_perawatan = $this->db->query("SELECT IFNULL(SUM(DATEDIFF(tgl_registrasi, tgl_pulang)+1),0) as hari_perawatan FROM reg_pasien WHERE DATE(tgl_registrasi) <= '$date' ")->row_array();
        $result[$i]['hari_perawatan'] = $hari_perawatan['hari_perawatan'];
      }else{
        $result[$i]['hari_perawatan'] = null;
      }      

    }
    return $result;
  }
  
}