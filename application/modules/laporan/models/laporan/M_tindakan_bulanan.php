<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tindakan_bulanan extends CI_Model {

  public function list_data($cookie)
  {
    if (@$cookie['search']['tahun'] != '') {
      $sql_where = " AND YEAR(b.tgl_catat)='".$this->db->escape_like_str($cookie['search']['tahun'])."'";
    }
    if (@$cookie['search']['jenispasien_id'] !='') {
      $sql_where .= " AND c.jenispasien_id='".$this->db->escape_like_str($cookie['search']['jenispasien_id'])."'";
    }
    if (@$cookie['search']['petugas_id'] !='') {
      $sql_where .= " AND (b.petugas_id='".$this->db->escape_like_str($cookie['search']['petugas_id'])."' OR a.petugas_id_2='".$this->db->escape_like_str($cookie['search']['petugas_id'])."' OR a.petugas_id_3='".$this->db->escape_like_str($cookie['search']['petugas_id'])."' OR a.petugas_id_4='".$this->db->escape_like_str($cookie['search']['petugas_id'])."' OR a.petugas_id_5='".$this->db->escape_like_str($cookie['search']['petugas_id'])."')";
    }

    $sql_where_2 = '';
    if (@$cookie['search']['jenisreg_st'] != '') {
      $sql_where_2 .= " AND a.jenisreg_st='".$this->db->escape_like_str($cookie['search']['jenisreg_st'])."'";
    }
    if (@$cookie['search']['lokasi_id'] != '') {
      $sql_where_2 .= " AND a.lokasi_id='".$this->db->escape_like_str($cookie['search']['lokasi_id'])."'";
    }

    $sql = "SELECT 
             a.*,
             (jml_bln_01+jml_bln_02+jml_bln_03+jml_bln_04+jml_bln_05+jml_bln_06+jml_bln_07+jml_bln_08+jml_bln_09+jml_bln_10+jml_bln_11+jml_bln_12) as jml_bln_total
            FROM 
            (
             SELECT 
              a.lokasi_id,a.lokasi_nm,
              (SELECT CASE WHEN SUM(b.jml_tagihan) IS NOT NULL THEN SUM(b.jml_tagihan) ELSE 0 END FROM dat_tindakan b INNER JOIN reg_pasien c ON b.reg_id=c.reg_id WHERE b.lokasi_id=a.lokasi_id AND MONTH(b.tgl_catat)='01' $sql_where) as jml_bln_01,
              (SELECT CASE WHEN SUM(b.jml_tagihan) IS NOT NULL THEN SUM(b.jml_tagihan) ELSE 0 END FROM dat_tindakan b INNER JOIN reg_pasien c ON b.reg_id=c.reg_id WHERE b.lokasi_id=a.lokasi_id AND MONTH(b.tgl_catat)='02' $sql_where) as jml_bln_02,
              (SELECT CASE WHEN SUM(b.jml_tagihan) IS NOT NULL THEN SUM(b.jml_tagihan) ELSE 0 END FROM dat_tindakan b INNER JOIN reg_pasien c ON b.reg_id=c.reg_id WHERE b.lokasi_id=a.lokasi_id AND MONTH(b.tgl_catat)='03' $sql_where) as jml_bln_03,
              (SELECT CASE WHEN SUM(b.jml_tagihan) IS NOT NULL THEN SUM(b.jml_tagihan) ELSE 0 END FROM dat_tindakan b INNER JOIN reg_pasien c ON b.reg_id=c.reg_id WHERE b.lokasi_id=a.lokasi_id AND MONTH(b.tgl_catat)='04' $sql_where) as jml_bln_04,
              (SELECT CASE WHEN SUM(b.jml_tagihan) IS NOT NULL THEN SUM(b.jml_tagihan) ELSE 0 END FROM dat_tindakan b INNER JOIN reg_pasien c ON b.reg_id=c.reg_id WHERE b.lokasi_id=a.lokasi_id AND MONTH(b.tgl_catat)='05' $sql_where) as jml_bln_05,
              (SELECT CASE WHEN SUM(b.jml_tagihan) IS NOT NULL THEN SUM(b.jml_tagihan) ELSE 0 END FROM dat_tindakan b INNER JOIN reg_pasien c ON b.reg_id=c.reg_id WHERE b.lokasi_id=a.lokasi_id AND MONTH(b.tgl_catat)='06' $sql_where) as jml_bln_06,
              (SELECT CASE WHEN SUM(b.jml_tagihan) IS NOT NULL THEN SUM(b.jml_tagihan) ELSE 0 END FROM dat_tindakan b INNER JOIN reg_pasien c ON b.reg_id=c.reg_id WHERE b.lokasi_id=a.lokasi_id AND MONTH(b.tgl_catat)='07' $sql_where) as jml_bln_07,
              (SELECT CASE WHEN SUM(b.jml_tagihan) IS NOT NULL THEN SUM(b.jml_tagihan) ELSE 0 END FROM dat_tindakan b INNER JOIN reg_pasien c ON b.reg_id=c.reg_id WHERE b.lokasi_id=a.lokasi_id AND MONTH(b.tgl_catat)='08' $sql_where) as jml_bln_08,
              (SELECT CASE WHEN SUM(b.jml_tagihan) IS NOT NULL THEN SUM(b.jml_tagihan) ELSE 0 END FROM dat_tindakan b INNER JOIN reg_pasien c ON b.reg_id=c.reg_id WHERE b.lokasi_id=a.lokasi_id AND MONTH(b.tgl_catat)='09' $sql_where) as jml_bln_09,
              (SELECT CASE WHEN SUM(b.jml_tagihan) IS NOT NULL THEN SUM(b.jml_tagihan) ELSE 0 END FROM dat_tindakan b INNER JOIN reg_pasien c ON b.reg_id=c.reg_id WHERE b.lokasi_id=a.lokasi_id AND MONTH(b.tgl_catat)='10' $sql_where) as jml_bln_10,
              (SELECT CASE WHEN SUM(b.jml_tagihan) IS NOT NULL THEN SUM(b.jml_tagihan) ELSE 0 END FROM dat_tindakan b INNER JOIN reg_pasien c ON b.reg_id=c.reg_id WHERE b.lokasi_id=a.lokasi_id AND MONTH(b.tgl_catat)='11' $sql_where) as jml_bln_11,
              (SELECT CASE WHEN SUM(b.jml_tagihan) IS NOT NULL THEN SUM(b.jml_tagihan) ELSE 0 END FROM dat_tindakan b INNER JOIN reg_pasien c ON b.reg_id=c.reg_id WHERE b.lokasi_id=a.lokasi_id AND MONTH(b.tgl_catat)='12' $sql_where) as jml_bln_12
             FROM mst_lokasi a 
             WHERE 1
              $sql_where_2
              ORDER BY a.lokasi_id ASC
            ) a";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
  
}