<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tindakan_detail extends CI_Model {

  public function list_data($cookie)
  {
    if (@$cookie['search']['periode_data'] == '01') {
      $sql_where = "AND DATE(a.tgl_catat) = '".to_date($this->db->escape_like_str($cookie['search']['tgl']))."'";
    }elseif (@$cookie['search']['periode_data'] == '02') {
      $sql_where = "AND DATE(a.tgl_catat) BETWEEN '".to_date($this->db->escape_like_str($cookie['search']['tgl_awal']))."' AND '".to_date($this->db->escape_like_str($cookie['search']['tgl_akhir']))."'";
    }elseif (@$cookie['search']['periode_data'] == '03') {
      $sql_where = "AND YEAR(a.tgl_catat)='".$this->db->escape_like_str($cookie['search']['tahun'])."' AND MONTH(a.tgl_catat)='".$this->db->escape_like_str($cookie['search']['bulan'])."'";
    }elseif (@$cookie['search']['periode_data'] == '04') {
      $sql_where = "AND YEAR(a.tgl_catat)='".$this->db->escape_like_str($cookie['search']['tahun'])."'";
    }

    if (@$cookie['search']['jenisreg_st'] !='') {
      $sql_where .= " AND c.jenisreg_st='".$this->db->escape_like_str($cookie['search']['jenisreg_st'])."'";
    }

    if (@$cookie['search']['lokasi_id'] !='') {
      $sql_where .= " AND a.lokasi_id='".$this->db->escape_like_str($cookie['search']['lokasi_id'])."'";
    }

    if (@$cookie['search']['jenispasien_id'] !='') {
      $sql_where .= " AND c.jenispasien_id='".$this->db->escape_like_str($cookie['search']['jenispasien_id'])."'";
    }

    if (@$cookie['search']['petugas_id'] !='') {
      $sql_where .= " AND (a.petugas_id='".$this->db->escape_like_str($cookie['search']['petugas_id'])."' OR a.petugas_id_2='".$this->db->escape_like_str($cookie['search']['petugas_id'])."' OR a.petugas_id_3='".$this->db->escape_like_str($cookie['search']['petugas_id'])."' OR a.petugas_id_4='".$this->db->escape_like_str($cookie['search']['petugas_id'])."' OR a.petugas_id_5='".$this->db->escape_like_str($cookie['search']['petugas_id'])."')";
    }

    $sql = "SELECT 
              a.*,
              b.lokasi_nm,
              d.jenispasien_nm,
              e.pegawai_nm AS pegawai_nm_1, 
              f.pegawai_nm AS pegawai_nm_2, 
              g.pegawai_nm AS pegawai_nm_3, 
              h.pegawai_nm AS pegawai_nm_4, 
              i.pegawai_nm AS pegawai_nm_5 
            FROM dat_tindakan a 
            INNER JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id
            INNER JOIN reg_pasien c ON a.reg_id=c.reg_id
            INNER JOIN mst_jenis_pasien d ON c.jenispasien_id=d.jenispasien_id
            LEFT JOIN mst_pegawai e ON a.petugas_id = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_2 = f.pegawai_id
            LEFT JOIN mst_pegawai g ON a.petugas_id_3 = g.pegawai_id
            LEFT JOIN mst_pegawai h ON a.petugas_id_4 = h.pegawai_id
            LEFT JOIN mst_pegawai i ON a.petugas_id_5 = i.pegawai_id
            WHERE 1 $sql_where
            ORDER BY a.tgl_catat ASC, a.tindakan_id ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }
  
}