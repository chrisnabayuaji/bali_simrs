<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pasien extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['asalpasien_st'] == '1') {
      $where .= "AND d.src_reg_id IS NOT NULL ";
    } elseif (@$cookie['search']['asalpasien_st'] == '2') {
      $where .= "AND a.lokasi_id = '03.02' ";
    }
    if (@$cookie['search']['tahun'] != '') {
      if (@$cookie['search']['asalpasien_st'] == '1') {
        $where .= "AND YEAR(d.tgl_order) = '" . $this->db->escape_like_str($cookie['search']['tahun']) . "' ";
      } elseif (@$cookie['search']['asalpasien_st'] == '2') {
        $where .= "AND YEAR(a.tgl_registrasi) = '" . $this->db->escape_like_str($cookie['search']['tahun']) . "' ";
      } else {
        $where .= "AND (YEAR(d.tgl_order) = '" . $this->db->escape_like_str($cookie['search']['tahun']) . "' OR YEAR(a.tgl_registrasi) = '" . $this->db->escape_like_str($cookie['search']['tahun']) . "') ";
      }
    }
    if (@$cookie['search']['bulan'] != '') {
      if (@$cookie['search']['asalpasien_st'] == '1') {
        $where .= "AND MONTH(d.tgl_order) = '" . $this->db->escape_like_str($cookie['search']['bulan']) . "' ";
      } elseif (@$cookie['search']['asalpasien_st'] == '2') {
        $where .= "AND MONTH(a.tgl_registrasi) = '" . $this->db->escape_like_str($cookie['search']['bulan']) . "' ";
      } else {
        $where .= "AND (MONTH(d.tgl_order) = '" . $this->db->escape_like_str($cookie['search']['bulan']) . "' OR MONTH(a.tgl_registrasi) = '" . $this->db->escape_like_str($cookie['search']['bulan']) . "') ";
      }
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT
              a.*,
              b.lokasi_nm,
              c.jenispasien_nm,
              d.* 
            FROM
              reg_pasien a
              JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
              JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
              JOIN rad_pemeriksaan d ON a.reg_id = d.reg_id
            $where              
            ORDER BY
              d.tgl_order DESC";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    //
    foreach ($result as $key => $val) {
      $result[$key]['item_pemeriksaan'] = $this->list_item_pemeriksaan($val['pemeriksaan_id']);
    }
    return $result;
  }

  public function list_item_pemeriksaan($pemeriksaan_id = null)
  {
    $sql = "SELECT
              a.pemeriksaanrinc_id, a.pemeriksaan_id, a.itemrad_id, b.itemrad_nm
            FROM
              rad_pemeriksaan_rinc a    
            LEFT JOIN mst_item_rad b ON a.itemrad_id = b.itemrad_id        
            WHERE a.pemeriksaan_id = ?
            ORDER BY
              a.pemeriksaanrinc_id ASC";
    $query = $this->db->query($sql, $pemeriksaan_id);
    $result = $query->result_array();
    return $result;
  }
}
