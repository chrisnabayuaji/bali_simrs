<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pemeriksaan_per_hari extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['tgl'] != '') {
      $where = "AND DATE(d.tgl_order) = '" . to_date($this->db->escape_like_str($cookie['search']['tgl'])) . "'";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT
              a.*,
              b.lokasi_nm,
              c.jenispasien_nm,
              d.* 
            FROM
              reg_pasien a
              JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
              JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
              JOIN rad_pemeriksaan d ON a.reg_id = d.reg_id
            $where              
            ORDER BY
              d.tgl_order DESC";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    //
    foreach ($result as $key => $val) {
      $result[$key]['tgl_hasil'] = $this->get_tgl_hasil($val['pemeriksaan_id']);
      $result[$key]['jml_tagihan'] = $this->get_jml_tagihan($val['pemeriksaan_id']);
      // $result[$key]['item_pemeriksaan'] = $this->list_item_pemeriksaan($val['pemeriksaan_id']);
    }
    return $result;
  }

  function get_tgl_hasil($pemeriksaan_id)
  {
    $sql = "SELECT 
              a.tgl_hasil
            FROM rad_pemeriksaan_rinc a 
            WHERE a.pemeriksaan_id=?
            GROUP BY a.pemeriksaan_id";
    $query = $this->db->query($sql, array($pemeriksaan_id));
    $row = $query->row_array();
    return $row['tgl_hasil'];
  }

  function get_jml_tagihan($pemeriksaan_id)
  {
    $sql = "SELECT
              SUM(b.jml_tagihan) AS jml_tagihan 
            FROM
              rad_pemeriksaan_rinc a
              LEFT JOIN dat_tindakan b ON b.tarif_id = a.tarif_id 
              AND b.pemeriksaan_id = '$pemeriksaan_id' 
            WHERE
              a.pemeriksaan_id = '$pemeriksaan_id'";
    $query = $this->db->query($sql);
    $row = $query->row_array();
    return $row['jml_tagihan'];
  }

  // public function list_item_pemeriksaan($pemeriksaan_id = null)
  // {
  //   $sql = "SELECT
  //             a.pemeriksaanrinc_id, a.pemeriksaan_id, a.itemrad_id, b.itemrad_nm
  //           FROM
  //             rad_pemeriksaan_rinc a    
  //           LEFT JOIN mst_item_rad b ON a.itemrad_id = b.itemrad_id        
  //           WHERE a.pemeriksaan_id = ?
  //           ORDER BY
  //             a.pemeriksaanrinc_id ASC";
  //   $query = $this->db->query($sql, $pemeriksaan_id);
  //   $result = $query->result_array();
  //   return $result;
  // }
}
