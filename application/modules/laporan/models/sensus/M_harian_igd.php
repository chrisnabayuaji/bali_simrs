<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_harian_igd extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE 1 = 1 ";
    if (@$cookie['search']['tgl'] != '' && @$cookie['search']['lokasi_id'] != '') {
      $where .= "AND DATE(a.tgl_registrasi) = '" . to_date($cookie['search']['tgl']) . "' AND a.lokasi_id = '" . $cookie['search']['lokasi_id'] . "'";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
      a.*, 
      b.lokasi_nm, 
      c.jenispasien_nm, 
      d.parameter_val as jeniskunjungan_nm,
      e.*,
      f.pegawai_nm as dokter_nm
    FROM reg_pasien a
    LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
    LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
    JOIN mst_parameter d ON a.jeniskunjungan_cd = d.parameter_cd AND d.parameter_field = 'jeniskunjungan_cd'
    LEFT JOIN 
      ( 
        SELECT 
          x.reg_id AS reg_id_tindaklanjut, 
          x.subtindaklanjut_cd,
          y.lokasi_nm AS lokasi_nm_tindaklanjut,
          z.rsrujukan_nm
        FROM reg_pasien_tindaklanjut x
        LEFT JOIN mst_lokasi y ON x.lokasi_id = y.lokasi_id
        LEFT JOIN mst_rs_rujukan z ON x.rsrujukan_id = z.rsrujukan_id
      ) AS e ON a.reg_id = e.reg_id_tindaklanjut 
    LEFT JOIN mst_pegawai f ON a.dokter_id = f.pegawai_id
    $where";
    $query = $this->db->query($sql);
    $pasien = $query->result_array();
    foreach ($pasien as $key => $value) {
      $diagnosis = $this->db->query("SELECT a.icdx,b.penyakit_nm FROM dat_diagnosis a INNER JOIN mst_penyakit b ON a.penyakit_id = b.penyakit_id WHERE reg_id = '" . $pasien[$key]['reg_id'] . "' ORDER BY diagnosis_id DESC LIMIT 1")->row_array();
      if ($diagnosis == null) {
        $pasien[$key]['diagnosis'] = null;
        $pasien[$key]['icdx'] = null;
      } else {
        $pasien[$key]['diagnosis'] = $diagnosis['penyakit_nm'];
        $pasien[$key]['icdx'] = $diagnosis['icdx'];
      }
    }
    return $pasien;
  }

  public function get_lokasi()
  {
    return $this->db->where('jenisreg_st', 1)->get('mst_lokasi')->result_array();
  }
}
