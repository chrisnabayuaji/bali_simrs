<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_harian_ranap extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE 1 = 1 ";
    if (@$cookie['search']['tgl'] != '' && @$cookie['search']['lokasi_id'] != '') {
      $where .= "AND DATE(a.tgl_masuk) = '" . to_date($cookie['search']['tgl']) . "' AND a.lokasi_id = '" . $cookie['search']['lokasi_id'] . "'";
    }
    return $where;
  }

  public function pasien_masuk($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT *, b.pasien_nm, c.kelas_nm FROM reg_pasien_kamar a 
      JOIN reg_pasien b ON a.reg_id = b.reg_id
      LEFT JOIN mst_kelas c ON a.kelas_id = c.kelas_id
      $where AND lokasi_asal_id IS NULL AND kamar_asal_id IS NULL";
    $query = $this->db->query($sql);
    $pasien = $query->result_array();
    foreach ($pasien as $key => $value) {
      $diagnosis = $this->db->query("SELECT a.icdx,b.penyakit_nm FROM dat_diagnosis a INNER JOIN mst_penyakit b ON a.penyakit_id = b.penyakit_id WHERE reg_id = '" . $pasien[$key]['reg_id'] . "' ORDER BY diagnosis_id DESC LIMIT 1")->row_array();
      if ($diagnosis == null) {
        $pasien[$key]['diagnosis'] = null;
        $pasien[$key]['icdx'] = null;
      } else {
        $pasien[$key]['diagnosis'] = $diagnosis['penyakit_nm'];
        $pasien[$key]['icdx'] = $diagnosis['icdx'];
      }
    }
    return $pasien;
  }

  public function pasien_pindahan($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT *, b.pasien_nm, b.tindaklanjut_cd, c.kelas_nm, d.lokasi_nm AS lokasi_asal_nm
      FROM reg_pasien_kamar a 
      JOIN reg_pasien b ON a.reg_id = b.reg_id
      LEFT JOIN mst_kelas c ON a.kelas_id = c.kelas_id
      LEFT JOIN mst_lokasi d ON a.lokasi_asal_id = d.lokasi_id
      $where AND lokasi_asal_id IS NOT NULL AND kamar_asal_id IS NOT NULL";
    $query = $this->db->query($sql);
    $pasien = $query->result_array();
    foreach ($pasien as $key => $value) {
      $diagnosis = $this->db->query("SELECT a.icdx,b.penyakit_nm FROM dat_diagnosis a INNER JOIN mst_penyakit b ON a.penyakit_id = b.penyakit_id WHERE reg_id = '" . $pasien[$key]['reg_id'] . "' ORDER BY diagnosis_id DESC LIMIT 1")->row_array();
      if ($diagnosis == null) {
        $pasien[$key]['diagnosis'] = null;
        $pasien[$key]['icdx'] = null;
      } else {
        $pasien[$key]['diagnosis'] = $diagnosis['penyakit_nm'];
        $pasien[$key]['icdx'] = $diagnosis['icdx'];
      }
    }
    return $pasien;
  }

  public function pasien_keluar($cookie)
  {
    $sql = "SELECT *, b.pasien_nm, b.tindaklanjut_cd, c.kelas_nm, d.lokasi_nm as lokasi_tujuan_nm
      FROM reg_pasien_kamar a 
      JOIN reg_pasien b ON a.reg_id = b.reg_id
      LEFT JOIN mst_kelas c ON a.kelas_id = c.kelas_id
      LEFT JOIN mst_lokasi d ON a.lokasi_tujuan_id = d.lokasi_id
      WHERE DATE(a.tgl_keluar) = '" . to_date($cookie['search']['tgl']) . "' AND a.lokasi_id = '" . $cookie['search']['lokasi_id'] . "' 
        AND lokasi_tujuan_id IS NULL AND kamar_tujuan_id IS NULL AND b.tindaklanjut_cd IS NOT NULL AND b.tindaklanjut_cd != '04' AND b.tindaklanjut_cd != '05'";
    $query = $this->db->query($sql);
    $pasien = $query->result_array();
    foreach ($pasien as $key => $value) {
      $diagnosis = $this->db->query("SELECT a.icdx,b.penyakit_nm FROM dat_diagnosis a INNER JOIN mst_penyakit b ON a.penyakit_id = b.penyakit_id WHERE reg_id = '" . $pasien[$key]['reg_id'] . "' ORDER BY diagnosis_id DESC LIMIT 1")->row_array();
      if ($diagnosis == null) {
        $pasien[$key]['diagnosis'] = null;
        $pasien[$key]['icdx'] = null;
      } else {
        $pasien[$key]['diagnosis'] = $diagnosis['penyakit_nm'];
        $pasien[$key]['icdx'] = $diagnosis['icdx'];
      }
    }
    return $pasien;
  }

  public function pasien_dipindah($cookie)
  {
    $sql = "SELECT *, b.pasien_nm, b.tindaklanjut_cd, c.kelas_nm, d.lokasi_nm as lokasi_tujuan_nm
      FROM reg_pasien_kamar a 
      JOIN reg_pasien b ON a.reg_id = b.reg_id
      LEFT JOIN mst_kelas c ON a.kelas_id = c.kelas_id
      LEFT JOIN mst_lokasi d ON a.lokasi_tujuan_id = d.lokasi_id
      WHERE DATE(a.tgl_keluar) = '" . to_date($cookie['search']['tgl']) . "' AND a.lokasi_id = '" . $cookie['search']['lokasi_id'] . "' 
        AND lokasi_tujuan_id IS NOT NULL AND kamar_tujuan_id IS NOT NULL";
    $query = $this->db->query($sql);
    $pasien = $query->result_array();
    foreach ($pasien as $key => $value) {
      $diagnosis = $this->db->query("SELECT a.icdx,b.penyakit_nm FROM dat_diagnosis a INNER JOIN mst_penyakit b ON a.penyakit_id = b.penyakit_id WHERE reg_id = '" . $pasien[$key]['reg_id'] . "' ORDER BY diagnosis_id DESC LIMIT 1")->row_array();
      if ($diagnosis == null) {
        $pasien[$key]['diagnosis'] = null;
        $pasien[$key]['icdx'] = null;
      } else {
        $pasien[$key]['diagnosis'] = $diagnosis['penyakit_nm'];
        $pasien[$key]['icdx'] = $diagnosis['icdx'];
      }
    }
    return $pasien;
  }

  public function get_lokasi()
  {
    return $this->db->where('jenisreg_st', 2)->get('mst_lokasi')->result_array();
  }

  public function pasien_meninggal($cookie)
  {
    $sql = "SELECT *, b.pasien_nm, HOUR(TIMEDIFF(b.tgl_pulang, b.tgl_registrasi)) as jam, b.tindaklanjut_cd, c.kelas_nm, d.lokasi_nm as lokasi_tujuan_nm
      FROM reg_pasien_kamar a 
      JOIN reg_pasien b ON a.reg_id = b.reg_id
      LEFT JOIN mst_kelas c ON a.kelas_id = c.kelas_id
      LEFT JOIN mst_lokasi d ON a.lokasi_tujuan_id = d.lokasi_id
      WHERE DATE(a.tgl_keluar) = '" . to_date($cookie['search']['tgl']) . "' AND a.lokasi_id = '" . $cookie['search']['lokasi_id'] . "' 
        AND lokasi_tujuan_id IS NULL AND kamar_tujuan_id IS NULL AND b.tindaklanjut_cd IS NOT NULL AND b.tindaklanjut_cd = '04' ";
    $query = $this->db->query($sql);
    $pasien = $query->result_array();
    foreach ($pasien as $key => $value) {
      $diagnosis = $this->db->query("SELECT a.icdx,b.penyakit_nm FROM dat_diagnosis a INNER JOIN mst_penyakit b ON a.penyakit_id = b.penyakit_id WHERE reg_id = '" . $pasien[$key]['reg_id'] . "' ORDER BY diagnosis_id DESC LIMIT 1")->row_array();
      if ($diagnosis == null) {
        $pasien[$key]['diagnosis'] = null;
        $pasien[$key]['icdx'] = null;
      } else {
        $pasien[$key]['diagnosis'] = $diagnosis['penyakit_nm'];
        $pasien[$key]['icdx'] = $diagnosis['icdx'];
      }
    }
    return $pasien;
  }

  public function rekapitulasi($cookie)
  {
    $tgl = to_date($cookie['search']['tgl']);
    $lokasi = $cookie['search']['lokasi_id'];
    $jml_awal = $this->db->query("SELECT IFNULL(COUNT(regkamar_id),0) as jml FROM reg_pasien_kamar a WHERE DATE(a.tgl_masuk) < '$tgl' AND a.tgl_keluar IS NULL AND a.lokasi_id = '$lokasi'")->row_array();
    $pasien_masuk = $this->db->query("SELECT IFNULL(COUNT(regkamar_id),0) as jml FROM reg_pasien_kamar a WHERE DATE(a.tgl_masuk) = '$tgl' AND a.lokasi_id = '$lokasi' AND lokasi_asal_id IS NULL")->row_array();
    $pasien_pindahan = $this->db->query("SELECT IFNULL(COUNT(regkamar_id),0) as jml FROM reg_pasien_kamar a WHERE DATE(a.tgl_masuk) = '$tgl' AND a.lokasi_id = '$lokasi' AND lokasi_asal_id IS NOT NULL")->row_array();
    $pasien_pulang = $this->db->query(
      "SELECT 
        IFNULL(COUNT(regkamar_id),0) as jml 
      FROM reg_pasien_kamar a 
      JOIN reg_pasien b ON a.reg_id = b.reg_id
      WHERE DATE(a.tgl_masuk) = '$tgl' AND a.lokasi_id = '$lokasi' 
        AND a.tgl_keluar IS NOT NULL AND b.tindaklanjut_cd = '00'"
    )->row_array();

    $pasien_rujuk = $this->db->query(
      "SELECT 
        IFNULL(COUNT(regkamar_id),0) as jml 
      FROM reg_pasien_kamar a 
      JOIN reg_pasien b ON a.reg_id = b.reg_id
      WHERE DATE(a.tgl_masuk) = '$tgl' AND a.lokasi_id = '$lokasi' 
        AND a.tgl_keluar IS NOT NULL AND b.tindaklanjut_cd = '01' OR b.tindaklanjut_cd = '02' OR b.tindaklanjut_cd = '03'"
    )->row_array();

    $pasien_meninggal = $this->db->query(
      "SELECT 
        IFNULL(COUNT(regkamar_id),0) as jml 
      FROM reg_pasien_kamar a 
      JOIN reg_pasien b ON a.reg_id = b.reg_id AND HOUR(TIMEDIFF(b.tgl_pulang, b.tgl_registrasi)) < 48
      WHERE DATE(a.tgl_masuk) = '$tgl' AND a.lokasi_id = '$lokasi' 
        AND a.tgl_keluar IS NOT NULL AND b.tindaklanjut_cd = '04' OR b.tindaklanjut_cd = '05'"
    )->row_array();

    $pasien_meninggal_48 = $this->db->query(
      "SELECT 
        IFNULL(COUNT(regkamar_id),0) as jml 
      FROM reg_pasien_kamar a 
      JOIN reg_pasien b ON a.reg_id = b.reg_id AND HOUR(TIMEDIFF(b.tgl_pulang, b.tgl_registrasi)) >= 48
      WHERE DATE(a.tgl_masuk) = '$tgl' AND a.lokasi_id = '$lokasi' 
        AND a.tgl_keluar IS NOT NULL AND b.tindaklanjut_cd = '04' OR b.tindaklanjut_cd = '05'"
        )->row_array();

    $result = array(
      'jml_awal' => $jml_awal['jml'],
      'pasien_masuk' => $pasien_masuk['jml'],
      'pasien_pindahan' => $pasien_pindahan['jml'],
      'pasien_pulang' => $pasien_pulang['jml'],
      'pasien_rujuk' => $pasien_rujuk['jml'],
      'pasien_meninggal' => $pasien_meninggal['jml'],
      'pasien_meninggal_48' => $pasien_meninggal_48['jml'],
    );
    return $result;
  }
}
