<script>
  $(document).ready(function() {
    $(".table-responsive tbody").overlayScrollbars({});
  });
</script>
<div class="table-responsive">
  <table class="table table-hover table-striped table-bordered table-fixed" style="width: 1800px;">
    <thead>
      <tr>
        <th class="text-center text-middle" rowspan="2" width="36">No</th>
        <th class="text-center text-middle" rowspan="2" width="300">Nama Obat</th>
        <th class="text-center text-middle" colspan="31" width="1550">Jumlah Pengeluaran Setiap Hari</th>
        <th class="text-center text-middle" rowspan="2" width="70">Total</th>
        <th class="text-center text-middle" width="100">Harga</th>
        <th class="text-center text-middle" width="100">Harga</th>
        <th class="text-center text-middle" rowspan="2" width="100">Laba</th>
      </tr>
      <tr>
        <?php
        for ($i = 1; $i <= 31; $i++) :
          $num = sprintf("%02d", $i);
        ?>
          <th class="text-center text-middle" width="50"><?= $num ?></th>
        <?php endfor; ?>
        <th class="text-center text-middle" width="100">Beli</th>
        <th class="text-center text-middle" width="100">Jual</th>
      </tr>
    </thead>
    <?php if (@$main == null) : ?>
      <tbody>
        <tr>
          <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
        </tr>
      </tbody>
    <?php else : ?>
      <tbody>
        <?php
        $nomor = 1;
        $tot_harga_beli = 0;
        $tot_harga_jual = 0;
        $tot_laba = 0;
        foreach ($main as $row) :
          $tot_harga_beli += $row['harga_beli'];
          $tot_harga_jual += $row['harga_jual'];
          $tot_laba += $row['laba'];
        ?>
          <tr>
            <td class="text-center" width="36"><?= ($nomor++) ?></td>
            <td class="text-left" width="300"><?= $row['obat_nm'] ?></td>
            <?php
            for ($i = 1; $i <= 31; $i++) :
              $num = $i;
            ?>
              <td class="text-center" width="50"><?= num_id($row['tgl_' . $num]) ?></td>
            <?php endfor; ?>
            <td class="text-center" width="70"><?= num_id($row['total']) ?></td>
            <td class="text-right" width="100"><?= num_id($row['harga_beli']) ?></td>
            <td class="text-right" width="100"><?= num_id($row['harga_jual']) ?></td>
            <td class="text-right" width="100"><?= num_id($row['laba']) ?></td>
          </tr>
        <?php endforeach; ?>
        <tr>
          <td class="text-center" colspan="34" width="1956"><b>TOTAL</b></td>
          <td class="text-right" width="100"><b>Rp. <?= num_id($tot_harga_beli) ?></b></td>
          <td class="text-right" width="100"><b>Rp. <?= num_id($tot_harga_jual) ?></b></td>
          <td class="text-right" width="100"><b>Rp. <?= num_id($tot_laba) ?></b></td>
        </tr>
      </tbody>
    <?php endif; ?>
  </table>
</div>