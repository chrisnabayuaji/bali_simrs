<?php $this->load->view('_js_index'); ?>
<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline col-title">
        <div class="col-back">
          <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
        </div>
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          Laporan Pengeluaran Obat
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item"><a href="#"><i class="fas fa-notes-medical"></i> Laporan</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item active"><span>Index</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>
  <!-- flash message -->
  <div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
  <!-- /flash message -->
  <div class="row full-page mt-4 mb-n2">
    <div class="col-lg-12 grid-margin-xl-0 stretch-card">
      <div class="card border-none">
        <div class="card-body">
          <form id="form-search" action="<?= site_url() . '/app/search_redirect/' . $nav['nav_id'] . '.pengeluaranobat' ?>" method="post" autocomplete="off">
            <input type="hidden" name="redirect" value="laporan/farmasi/pengeluaran_obat">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Bulan</label>
                  <div class="col-lg-5 col-md-5">
                    <select class="form-control chosen-select" name="bulan" id="bulan">
                      <option value="">- Pilih -</option>
                      <?php foreach ($bulan as $key => $val) : ?>
                        <option value="<?= $key ?>" <?= (@$cookie['search']['bulan'] == $key) ? 'selected' : ''; ?>><?= $val ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Tahun</label>
                  <div class="col-lg-5 col-md-5">
                    <select class="form-control chosen-select" name="tahun" id="tahun">
                      <option value="">- Pilih -</option>
                      <?php foreach (list_tahun() as $key => $val) : ?>
                        <option value="<?= $key ?>" <?= (@$cookie['search']['tahun'] == $key) ? 'selected' : ''; ?>><?= $val ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Jenis Registrasi</label>
                  <div class="col-lg-6 col-md-6">
                    <select class="form-control chosen-select" name="jenisreg_st" id="jenisreg_st">
                      <option value="">- Semua -</option>
                      <?php foreach (get_parameter('jenisreg_st') as $r) : ?>
                        <option value="<?= $r['parameter_cd'] ?>" <?= (@$cookie['search']['jenisreg_st'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Lokasi Pelayanan</label>
                  <div class="col-lg-6 col-md-6">
                    <div id="box_lokasi">
                      <select class="form-control chosen-select" name="lokasi_id" id="lokasi_id">
                        <option value="">- Semua -</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <?php if (@$cookie['search']['bulan'] != '' && @$cookie['search']['tahun'] != '') : ?>
                <div class="col-md-8">
                  <div class="text-right font-weight-bold" style="font-size: 15px;">LAPORAN PENGELUARAN OBAT</div>
                  <div class="text-right font-weight-semibold mt-1" style="font-size: 14px;">BULAN <?= strtoupper(get_bulan(@$cookie['search']['bulan'])) ?> <?= @$cookie['search']['tahun'] ?></div>
                  <?php if (@$cookie['search']['jenisreg_st'] != '') : ?>
                    <div class="text-right font-weight-semibold mt-1" style="font-size: 14px;"><?= strtoupper(get_parameter_value('jenisreg_st', @$cookie['search']['jenisreg_st'])) ?>
                      <?= (@$cookie['search']['lokasi_id'] != '') ? ' - ' . strtoupper($get_lokasi['lokasi_nm']) : '' ?>
                    </div>
                  <?php else : ?>
                    <div class="text-right font-weight-semibold mt-1" style="font-size: 14px;">SEMUA LOKASI REGISTRASI</div>
                  <?php endif; ?>
                </div>
              <?php endif; ?>
            </div>
            <div class="row">
              <div class="col-md-4 row">
                <div class="col-md-4">
                  <?php if (@$cookie['search']['bulan'] != '' && @$cookie['search']['tahun'] != '') : ?>
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-print"></i> Cetak Data
                      </button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?= site_url('laporan/farmasi/pengeluaran_obat/cetak_excel') ?>" target="_blank"><i class="fas fa-file-excel"></i> Cetak Excel</a>
                      </div>
                    </div>
                  <?php endif; ?>
                </div>
                <div class="col-md-6 pl-2">
                  <button type="submit" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Tampilkan Data"><i class="fas fa-search"></i> Tampilkan</button>
                  <a class="btn btn-xs btn-default" href="<?= site_url() . '/app/reset_redirect/' . $nav['nav_id'] . '.pengeluaranobat/' . str_replace('/', '-', 'laporan/farmasi/pengeluaran_obat') ?>" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-default" title="" data-original-title="Reset"><i class="fas fa-sync-alt"></i> Reset</a>
                </div>
              </div>
            </div>
          </form>
          <div class="row mt-3">
            <div class="col-md-12">
              <div id="box-list-data"></div>
            </div>
          </div><!-- /.row -->
        </div>
      </div>
    </div>
  </div>
</div>