<script type="text/javascript">
  $(document).ready(function () {
    $("#form-search").validate( {
      rules: {
        periode_data: { valueNotEquals: "" },
        tgl: { valueNotEquals: "" },
        tgl_awal: { valueNotEquals: "" },
        tgl_akhir: { valueNotEquals: "" },
        bulan: { valueNotEquals: "" },
        tahun: { valueNotEquals: "" },
        filter: { valueNotEquals: "" },
      },
      messages: {
        periode_data: { valueNotEquals: "Silahkan Pilih Periode Data!" },
        tgl: { valueNotEquals: "Silahkan Pilih Tanggal!" },
        tgl_awal: { valueNotEquals: "Silahkan Pilih Tanggal Awal!" },
        tgl_akhir: { valueNotEquals: "Silahkan Pilih Tanggal Akhir!" },
        bulan: { valueNotEquals: "Silahkan Pilih Bulan!" },
        tahun: { valueNotEquals: "Silahkan Pilih Tahun!" },
        filter: { valueNotEquals: "Silahkan Pilih filter!" },
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container")).addClass('mt-n1');
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    <?php if (@$cookie['search']['tgl'] == ''): ?>
    $('#tgl').val('');
    <?php endif; ?>
    <?php if (@$cookie['search']['tgl_awal'] == ''): ?>
    $('#tgl_awal').val('');
    <?php endif; ?>
    <?php if (@$cookie['search']['tgl_akhir'] == ''): ?>
    $('#tgl_akhir').val('');
    <?php endif; ?>

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    $('#periode_data').bind('change',function(e) {
      e.preventDefault();
      var i = $(this).val();
      if (i == '01') {
        $('.box-hide').addClass('d-none');
        $('#box-tanggal').removeClass('d-none');
      }else if(i == '02') {
        $('.box-hide').addClass('d-none');
        $('#box-tanggal-awal').removeClass('d-none');
        $('#box-tanggal-akhir').removeClass('d-none');
      }else if(i == '03') {
        $('.box-hide').addClass('d-none');
        $('#box-bulan').removeClass('d-none');
        $('#box-tahun').removeClass('d-none');
      }else if(i == '04') {
        $('.box-hide').addClass('d-none');
        $('#box-tahun').removeClass('d-none');
      }
    })

    $('#filter').bind('change',function(e) {
      e.preventDefault();
      var i = $(this).val();
      if (i == '01') {
        $('#title').html('Lokasi Pelayanan');
      }else if(i == '02') {
        $('#title').html('Jenis Pasien');
      }else if(i == '03') {
        $('#title').html('Asal Pasien');
      }else if(i == '04') {
        $('#title').html('Dokter Penanggungjawab');
      }
    })
  })
</script>