<?php $this->load->view('_js_index'); ?>
<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline col-title">
        <div class="col-back">
          <a href="<?=site_url($nav['nav_module'].'/dashboard')?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
        </div>
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          Rekapitulasi Detail Kunjungan Pasien
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?=site_url('app/dashboard')?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url($module_nav['nav_url'])?>"><i class="fas fa-folder-open"></i> <?=$module_nav['nav_nm']?></a></li>
          <li class="breadcrumb-item"><a href="#"><i class="fas fa-notes-medical"></i> Laporan</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url($nav['nav_url'])?>"><i class="<?=$nav['nav_icon']?>"></i> <?=$nav['nav_nm']?></a></li>
          <li class="breadcrumb-item active"><span>Index</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>
  <!-- flash message -->
  <div class="flash-success" data-flashsuccess="<?=$this->session->flashdata('flash_success')?>"></div>
  <!-- /flash message -->
  <div class="row full-page mt-4 mb-n2">
    <div class="col-lg-12 grid-margin-xl-0 stretch-card">
      <div class="card border-none">
        <div class="card-body">
          <form id="form-search" action="<?=site_url().'/app/search_redirect/'.$nav['nav_id'].'.detkunjunganpasien'?>" method="post" autocomplete="off">
            <input type="hidden" name="redirect" value="laporan/laporan/det_kunjungan_pasien">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Periode Data</label>
                  <div class="col-lg-5 col-md-5">
                    <select class="form-control chosen-select" name="periode_data" id="periode_data">
                      <option value="">- Pilih -</option>
                      <option value="01" <?=(@$cookie['search']['periode_data'] == '01') ? 'selected' : '';?>>Harian</option>
                      <option value="02" <?=(@$cookie['search']['periode_data'] == '02') ? 'selected' : '';?>>Periode Harian</option>
                      <option value="03" <?=(@$cookie['search']['periode_data'] == '03') ? 'selected' : '';?>>Bulanan</option>
                      <option value="04" <?=(@$cookie['search']['periode_data'] == '04') ? 'selected' : '';?>>Tahunan</option>
                    </select>
                  </div>
                </div>
                <div class="form-group <?=(@$cookie['search']['periode_data'] == '01' && @$cookie['search']['tgl'] !='') ? '' : 'd-none'?> row box-hide" id="box-tanggal">
                  <label class="col-lg-4 col-md-4 col-form-label">Tanggal</label>
                  <div class="col-lg-5 col-md-5">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datepicker" name="tgl" id="tgl" value="<?=@$cookie['search']['tgl']?>">
                    </div>
                  </div>
                </div>
                <div class="form-group <?=(@$cookie['search']['periode_data'] == '02' && @$cookie['search']['tgl_awal'] !='') ? '' : 'd-none'?> row box-hide" id="box-tanggal-awal">
                  <label class="col-lg-4 col-md-4 col-form-label">Tanggal Awal</label>
                  <div class="col-lg-5 col-md-5">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datepicker" name="tgl_awal" id="tgl_awal" value="<?=@$cookie['search']['tgl_awal']?>">
                    </div>
                  </div>
                </div>
                <div class="form-group <?=(@$cookie['search']['periode_data'] == '02' && @$cookie['search']['tgl_akhir'] !='') ? '' : 'd-none'?> row box-hide" id="box-tanggal-akhir">
                  <label class="col-lg-4 col-md-4 col-form-label">Tanggal Akhir</label>
                  <div class="col-lg-5 col-md-5">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datepicker" name="tgl_akhir" id="tgl_akhir" value="<?=@$cookie['search']['tgl_akhir']?>">
                    </div>
                  </div>
                </div>
                <div class="form-group <?=(@$cookie['search']['periode_data'] == '03' && @$cookie['search']['bulan'] !='') ? '' : 'd-none'?> row box-hide" id="box-bulan">
                  <label class="col-lg-4 col-md-4 col-form-label">Bulan</label>
                  <div class="col-lg-5 col-md-5">
                    <select class="form-control chosen-select" name="bulan" id="bulan">
                      <option value="">- Pilih -</option>
                      <?php foreach($bulan as $key => $val): ?>
                        <option value="<?=$key?>" <?=(@$cookie['search']['bulan'] == $key) ? 'selected' : '';?>><?=$val?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div>
                <div class="form-group <?=(@$cookie['search']['periode_data'] == '03' || @$cookie['search']['periode_data'] == '04'  && @$cookie['search']['tahun'] !='') ? '' : 'd-none'?> row box-hide" id="box-tahun">
                  <label class="col-lg-4 col-md-4 col-form-label">Tahun</label>
                  <div class="col-lg-5 col-md-5">
                    <select class="form-control chosen-select" name="tahun" id="tahun">
                      <option value="">- Pilih -</option>
                      <?php foreach(list_tahun() as $key => $val): ?>
                        <option value="<?=$key?>" <?=(@$cookie['search']['tahun'] == $key) ? 'selected' : '';?>><?=$val?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Jenis Registrasi</label>
                  <div class="col-lg-6 col-md-6">
                    <select class="form-control chosen-select" name="jenisreg_st" id="jenisreg_st">
                      <option value="">- Semua -</option>
                      <?php foreach(get_parameter('jenisreg_st') as $r): ?>
                        <option value="<?=$r['parameter_cd']?>" <?=(@$cookie['search']['jenisreg_st'] == $r['parameter_cd']) ? 'selected' : '';?>><?=$r['parameter_val']?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Lokasi Pelayanan</label>
                  <div class="col-lg-6 col-md-6">
                    <div id="box_lokasi">
                      <select class="form-control chosen-select" name="lokasi_id" id="lokasi_id">
                        <option value="">- Semua -</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-8">
                <div class="text-right font-weight-bold" style="font-size: 15px;">REKAPITULASI DETAIL KUNJUNGAN PASIEN</div>
                <div class="text-right font-weight-semibold mt-1" style="font-size: 14px;">
                  <?php if (@$cookie['search']['periode_data'] == '01'): ?>
                    Periode Tanggal <?=@$cookie['search']['tgl']?>
                  <?php elseif (@$cookie['search']['periode_data'] == '02'): ?>
                    Periode Tanggal <?=@$cookie['search']['tgl_awal']?> s/d <?=@$cookie['search']['tgl_akhir']?>
                  <?php elseif (@$cookie['search']['periode_data'] == '03'): ?>
                    Periode Bulan <?=get_bulan(@$cookie['search']['bulan'])?> Tahun <?=@$cookie['search']['tahun']?>
                  <?php elseif (@$cookie['search']['periode_data'] == '04'): ?>
                    Periode Tahun <?=@$cookie['search']['tahun']?>
                  <?php endif; ?>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 row">
                <div class="col-md-4">
                  <?php if (@$cookie['search']['periode_data'] !=''): ?>
                  <div class="btn-group">
                    <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-print"></i> Cetak Data
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="<?=site_url('laporan/laporan/det_kunjungan_pasien/cetak_excel')?>" target="_blank"><i class="fas fa-file-excel"></i> Cetak Excel</a>
                    </div>
                  </div>
                  <?php endif; ?>
                </div>
                <div class="col-md-6 pl-2">
                  <button type="submit" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Tampilkan Data"><i class="fas fa-search"></i> Tampilkan</button>
                  <a class="btn btn-xs btn-default" href="<?=site_url().'/app/reset_redirect/'.$nav['nav_id'].'.detkunjunganpasien/'.str_replace('/','-','laporan/laporan/det_kunjungan_pasien')?>" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-default" title="" data-original-title="Reset"><i class="fas fa-sync-alt"></i> Reset</a>
                </div>
              </div>
            </div>
          </form>
          
          <div class="row mt-3">
            <div class="col-md-12">
              <div class="table-responsive">
                <form id="form-multiple" action="<?=site_url().'/'.$nav['nav_url'].'/multiple/enable'?>" method="post">
                  <table class="table table-hover table-striped table-bordered table-fixed">
                    <thead>
                      <tr>
                        <th class="text-center text-middle" width="36">No</th>
                        <th class="text-center text-middle" width="100">No.Reg</th>
                        <th class="text-center text-middle" width="100">No.RM</th>
                        <th class="text-center text-middle">Nama Pasien</th>
                        <th class="text-center text-middle" width="100">Tgl.Registrasi</th>
                        <th class="text-center text-middle">Alamat</th>
                        <th class="text-center text-middle" width="30">JK</th>
                        <th class="text-center text-middle" width="70">Umur</th>
                        <th class="text-center text-middle" width="100">Jenis Pasien</th>
                        <th class="text-center text-middle" width="100">Asal Pasien</th>
                        <th class="text-center text-middle" width="100">Lokasi pelayanan</th>
                        <th class="text-center text-middle" width="100">ICDX</th>
                        <th class="text-center text-middle" width="150">Diagnosis Akhir</th>
                      </tr>
                    </thead>
                    <?php if(@$main == null):?>
                      <tbody>
                        <tr>
                          <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                        </tr>
                      </tbody>
                    <?php else:?>
                      <tbody>
                        <?php 
                        $nomor=1;
                        foreach($main as $row):
                        ?>
                          <tr>
                            <td class="text-center" width="36"><?=($nomor++)?></td>
                            <td class="text-center" width="100"><?=$row['reg_id']?></td>
                            <td class="text-center" width="100"><?=$row['pasien_id']?></td>
                            <td class="text-left"><?=$row['pasien_nm']?></td>
                            <td class="text-center" width="100"><?=to_date($row['tgl_registrasi'], '', 'full_date', '<br>')?></td>
                            <td class="text-left">
                              <?=$row['alamat']?>, 
                              <?=ucwords(strtolower($row['kelurahan']))?>,
                              <?=ucwords(strtolower($row['kecamatan']))?>,
                              <?=ucwords(strtolower($row['kabupaten']))?>,
                              <?=ucwords(strtolower($row['provinsi']))?>
                            </td>
                            <td class="text-center" width="30"><?=$row['sex_cd']?></td>
                            <td class="text-center" width="70">
                              <?=$row['umur_thn']?> tahun <br>
                              <?=$row['umur_bln']?> bulan <br>
                              <?=$row['umur_hr']?> hari
                            </td>
                            <td class="text-center" width="100"><?=$row['jenispasien_nm']?></td>
                            <td class="text-center" width="100"><?=get_parameter_value('asalpasien_cd', $row['asalpasien_cd'])?></td>
                            <td class="text-center" width="100"><?=$row['lokasi_nm']?></td>
                            <td class="text-center" width="100"><?=$row['icdx']?></td>
                            <td class="text-center" width="150"><?=$row['penyakit_nm']?></td>
                          </tr>
                        <?php endforeach;?>
                      </tbody>
                    <?php endif;?>
                  </table>
                </form>
              </div>
            </div>
          </div><!-- /.row -->

        </div>
      </div>
    </div>
  </div>
</div>