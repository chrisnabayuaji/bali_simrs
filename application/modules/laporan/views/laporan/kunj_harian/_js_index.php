<script type="text/javascript">
  $(document).ready(function () {
    $("#form-search").validate( {
      rules: {
        bulan: { valueNotEquals: "" },
        tahun: { valueNotEquals: "" },
      },
      messages: {
        bulan: { valueNotEquals: "Silahkan Pilih Bulan!" },
        tahun: { valueNotEquals: "Silahkan Pilih Tahun!" },
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container")).addClass('mt-n1');
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });
  })
</script>