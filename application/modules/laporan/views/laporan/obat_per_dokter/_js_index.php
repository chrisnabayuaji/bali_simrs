<script type="text/javascript">
  $(document).ready(function () {
    $("#form-search").validate( {
      rules: {
        dokter: { valueNotEquals: "" },
        tgl_awal: { valueNotEquals: "" },
        tgl_akhir: { valueNotEquals: "" },
      },
      messages: {
        dokter: { valueNotEquals: "Silahkan Pilih Dokter!" },
        tgl_awal: { valueNotEquals: "Silahkan Pilih Tanggal Awal!" },
        tgl_akhir: { valueNotEquals: "Silahkan Pilih Tanggal Akhir!" },
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container")).addClass('mt-n1');
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    <?php if (@$cookie['search']['tgl_awal'] == ''): ?>
    $('#tgl_awal').val('');
    <?php endif; ?>
    <?php if (@$cookie['search']['tgl_akhir'] == ''): ?>
    $('#tgl_akhir').val('');
    <?php endif; ?>

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');
  })
</script>