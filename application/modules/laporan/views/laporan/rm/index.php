<?php $this->load->view('_js_index'); ?>
<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline col-title">
        <div class="col-back">
          <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
        </div>
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          Rekapitulasi BOR, AVLOS, TOI DAN BTO
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item"><a href="#"><i class="fas fa-notes-medical"></i> Laporan</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item active"><span>Index</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>
  <!-- flash message -->
  <div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
  <!-- /flash message -->
  <div class="row full-page mt-4 mb-n2">
    <div class="col-lg-12 grid-margin-xl-0 stretch-card">
      <div class="card border-none">
        <div class="card-body">
          <form id="form-search" action="<?= site_url() . '/app/search/' . $nav['nav_id'] ?>" method="post" autocomplete="off">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group row" id="box-bulan">
                  <label class="col-lg-4 col-md-4 col-form-label">Bulan</label>
                  <div class="col-lg-5 col-md-5">
                    <select class="form-control chosen-select" name="bulan" id="bulan">
                      <option value="">- Pilih -</option>
                      <?php foreach ($bulan as $key => $val) : ?>
                        <option value="<?= $key ?>" <?= (@$cookie['search']['bulan'] == $key) ? 'selected' : ''; ?>><?= $val ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row" id="box-tahun">
                  <label class="col-lg-4 col-md-4 col-form-label">Tahun</label>
                  <div class="col-lg-5 col-md-5">
                    <select class="form-control chosen-select" name="tahun" id="tahun">
                      <option value="">- Pilih -</option>
                      <?php foreach (list_tahun() as $key => $val) : ?>
                        <option value="<?= $key ?>" <?= (@$cookie['search']['tahun'] == $key) ? 'selected' : ''; ?>><?= $val ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-8">
                <div class="text-right font-weight-bold" style="font-size: 15px;">REKAP BOR, AVLOS, TOI DAN BTO</div>
                <div class="text-right font-weight-semibold mt-1" style="font-size: 14px;">
                  Periode Bulan <?= get_bulan(@$cookie['search']['bulan']) ?> Tahun <?= @$cookie['search']['tahun'] ?>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 row">
                <div class="col-md-4">
                  <?php if (@$cookie['search']['periode_data'] != '') : ?>
                    <!-- <div class="btn-group">
                    <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <i class="fas fa-print"></i> Cetak Data
                    </button>
                    <div class="dropdown-menu">
                      <a class="dropdown-item" href="<?= site_url('laporan/laporan/pemakaian_obat/cetak_excel') ?>" target="_blank"><i class="fas fa-file-excel"></i> Cetak Excel</a>
                    </div>
                  </div> -->
                  <?php endif; ?>
                </div>
                <div class="col-md-6 pl-2">
                  <button type="submit" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Tampilkan Data"><i class="fas fa-search"></i> Tampilkan</button>
                  <a class="btn btn-xs btn-default" href="<?= site_url() . '/app/reset/' . $nav['nav_id'] ?>" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-default" title="" data-original-title="Reset"><i class="fas fa-sync-alt"></i> Reset</a>
                </div>
              </div>
            </div>
          </form>
          <div class="row mt-3">
            <?php if ($cookie['search']['bulan'] != '' && $cookie['search']['tahun'] != '') : ?>
              <div class="col-8">
                <h6>Rekapitulasi Sensus Rawat Inap</h6>
                <table class="table table-sm table-bordered">
                  <thead>
                    <tr>
                      <th class="text-center" width="90">Tanggal</th>
                      <th class="text-center">Pasien Awal</th>
                      <th class="text-center">Pasien Masuk</th>
                      <th class="text-center">Pasien Keluar</th>
                      <th class="text-center">Pasien Dirawat</th>
                      <th class="text-center">&Sigma; Hari Perawatan</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($rekap as $r) : ?>
                      <tr>
                        <td class="text-center"><?= $r['tanggal'] ?></td>
                        <td class="text-center"><?= $r['pasien_awal'] ?></td>
                        <td class="text-center"><?= $r['pasien_masuk'] ?></td>
                        <td class="text-center"><?= $r['pasien_pulang'] ?></td>
                        <td class="text-center"><?= $r['pasien_dirawat'] ?></td>
                        <td class="text-center"><?= $r['hari_perawatan'] ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              </div>
            <?php endif; ?>
          </div>
          <div class="row mt-3">
            <div class="col-md-8">
              <?php if ($cookie['search']['bulan'] != '' && $cookie['search']['tahun'] != '') : ?>
                <div class="table-responsive mb-5">
                  <table class="table table-bordered table-fixed table-sm">
                    <thead>
                      <tr>
                        <th class="text-center" width="50">No</th>
                        <th class="text-center">Keterangan</th>
                        <th class="text-center" width="100">Nilai</th>
                      </tr>
                    </thead>
                    <tbody style="width: 100%; height: 100%;">
                      <tr>
                        <td class="text-center" width="50">1</td>
                        <td>BOR (Bed Occupancy Ratio = Angka penggunaan tempat tidur)</td>
                        <td class="text-center" width="100"><?= @$bor['nilai'] . ' %' ?></td>
                      </tr>
                      <tr>
                        <td class="text-center" width="50">2</td>
                        <td>AVLOS (Average Length of Stay = Rata-rata lamanya pasien dirawat)</td>
                        <td class="text-center" width="100"><?= @$los['nilai'] . ' hari' ?></td>
                      </tr>
                      <tr>
                        <td class="text-center" width="50">3</td>
                        <td>TOI (Turn Over Interval = Tenggang perputaran)</td>
                        <td class="text-center" width="100"><?= @$toi['nilai'] . ' hari' ?></td>
                      </tr>
                      <tr>
                        <td class="text-center" width="50">4</td>
                        <td>BTO (Bed Turn Over = Angka perputaran tempat tidur)</td>
                        <td class="text-center" width="100"><?= @$bto['nilai'] . ' hari' ?></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <h5>Rumus Perhitungan</h5>
                <div class="table-responsive">
                  <table class="table table-bordered table-fixed table-sm">
                    <tbody style="width: 100%; height: 100%;">
                      <!-- BOR -->
                      <tr style="background:#f5f5f5">
                        <th class="text-center" width="50">1</th>
                        <th colspan="2">BOR (Bed Occupancy Ratio = Angka penggunaan tempat tidur)</th>
                      </tr>
                      <tr>
                        <td class="text-center" width="50"></td>
                        <th style="background:yellow" colspan="2">Jumlah Hari Perawatan / (Jumlah TT x Jumlah Hari Persatuan Waktu) x 100%</th>
                      </tr>
                      <tr>
                        <td class="text-center" width="50"></td>
                        <td class="text-right" width="250">Jumlah Hari Perawatan : </td>
                        <td class="text-left"><?= @$bor['hari_perawatan'] ?></td>
                      </tr>
                      <tr>
                        <td class="text-center" width="50"></td>
                        <td class="text-right" width="250">Jumlah Tempat Tidur (TT) : </td>
                        <td class="text-left"><?= @$bor['bed'] ?></td>
                      </tr>
                      <tr>
                        <td class="text-center" width="50"></td>
                        <td class="text-right" width="250">Jumlah Hari Persatuan Waktu : </td>
                        <td class="text-left"><?= @$bor['periode'] ?></td>
                      </tr>
                      <tr>
                        <td class="text-center" width="50"></td>
                        <th class="text-right" width="250">Nilai BOR : </th>
                        <th class="text-left"><?= @$bor['nilai'] . ' %' ?></th>
                      </tr>
                      <!-- LOS -->
                      <tr style="background:#f5f5f5">
                        <th class="text-center" width="50">2</th>
                        <th colspan="2">AVLOS (Average Length of Stay = Rata-rata lamanya pasien dirawat)</th>
                      </tr>
                      <tr>
                        <td class="text-center" width="50"></td>
                        <th style="background:yellow" colspan="2">Jumlah Hari Perawatan / Jumlah Pasien Keluar</th>
                      </tr>
                      <tr>
                        <td class="text-center" width="50"></td>
                        <td class="text-right" width="250">Jumlah Hari Perawatan : </td>
                        <td class="text-left"><?= @$los['hari_perawatan'] ?></td>
                      </tr>
                      <tr>
                        <td class="text-center" width="50"></td>
                        <td class="text-right" width="250">Jumlah Pasien Keluar : </td>
                        <td class="text-left"><?= @$los['pasien_keluar'] ?></td>
                      </tr>
                      <tr>
                        <td class="text-center" width="50"></td>
                        <th class="text-right" width="250">Nilai AVELOS : </th>
                        <th class="text-left"><?= @$los['nilai'] . ' hari' ?></th>
                      </tr>
                      <!-- TOI -->
                      <tr style="background:#f5f5f5">
                        <th class="text-center" width="50">3</th>
                        <th colspan="2">TOI (Turn Over Interval = Tenggang perputaran)</th>
                      </tr>
                      <tr>
                        <td class="text-center" width="50"></td>
                        <th style="background:yellow" colspan="2">((Jumlah TT X Hari ) - Hari Perawatan RS) / Jumlah Pasien Keluar HidupMati</th>
                      </tr>
                      <tr>
                        <td class="text-center" width="50"></td>
                        <td class="text-right" width="250">Jumlah Tempat Tidur (TT) : </td>
                        <td class="text-left"><?= @$toi['bed'] ?></td>
                      </tr>
                      <tr>
                        <td class="text-center" width="50"></td>
                        <td class="text-right" width="250">Hari Perawatan RS : </td>
                        <td class="text-left"><?= @$toi['hari_perawatan'] ?></td>
                      </tr>
                      <tr>
                        <td class="text-center" width="50"></td>
                        <td class="text-right" width="250">Jumlah Pasien Keluar : </td>
                        <td class="text-left"><?= @$toi['pasien_keluar'] ?></td>
                      </tr>
                      <tr>
                        <td class="text-center" width="50"></td>
                        <th class="text-right" width="250">Nilai TOI : </th>
                        <th class="text-left"><?= @$toi['nilai'] . ' hari' ?></th>
                      </tr>
                      <!-- BTO -->
                      <tr style="background:#f5f5f5">
                        <th class="text-center" width="50">4</th>
                        <th colspan="2">BTO (Bed Turn Over = Angka perputaran tempat tidur)</th>
                      </tr>
                      <tr>
                        <td class="text-center" width="50"></td>
                        <th style="background:yellow" colspan="2">Jumlah Pasien Keluar HidupMati / Jumlah TT</th>
                      </tr>
                      <tr>
                        <td class="text-center" width="50"></td>
                        <td class="text-right" width="250">Jumlah Pasien Keluar : </td>
                        <td class="text-left"><?= @$bto['pasien_keluar'] ?></td>
                      </tr>
                      <tr>
                        <td class="text-center" width="50"></td>
                        <td class="text-right" width="250">Jumlah Tempat Tidur (TT) : </td>
                        <td class="text-left"><?= @$bto['bed'] ?></td>
                      </tr>
                      <tr>
                        <td class="text-center" width="50"></td>
                        <th class="text-right" width="250">Nilai BTO : </th>
                        <th class="text-left"><?= @$bto['nilai'] . ' hari' ?></th>
                      </tr>
                    </tbody>
                  </table>
                </div>
              <?php endif; ?>
            </div>
            <div class="col-md-7" class="height:45vh !important">
              <?php if ($cookie['search']['bulan'] != '' && $cookie['search']['tahun'] != '') : ?>
                <canvas id="myChart" height="220"></canvas>
              <?php endif; ?>
            </div>
          </div><!-- /.row -->
        </div>
      </div>
    </div>
  </div>
</div>
<script>
  var label = [];
  var bor_50 = [];
  var bor_70 = [];
  var bor_80 = [];
  var bor_90 = [];

  var max = 30;
  var step = 1;

  var bto_30 = [];
  var bto_20 = [];
  var bto_15 = [];
  var bto_125 = [];
  for (let i = 0; i <= max / step; i++) {
    var v = i * step;
    label.push(v.toFixed(1))
    bor_50.push(fbor_50(v))
    bor_70.push(fbor_70(v))
    bor_80.push(fbor_80(v))
    bor_90.push(fbor_90(v))

    bto_30.push(fbto_30(v))
    bto_20.push(fbto_20(v))
    bto_15.push(fbto_15(v))
    bto_125.push(fbto_125(v))
  }
  var ctx = document.getElementById('myChart');
  var mixedChart = new Chart(ctx, {
    height: 100,
    type: 'line',
    data: {
      datasets: [{
        label: 'BOR 50',
        data: bor_50,
        type: 'line',
        fill: false,
        borderColor: '#55efc4',
        pointRadius: 0,
      }, {
        label: 'BOR 70',
        data: bor_70,
        type: 'line',
        fill: false,
        borderColor: '#81ecec',
        pointRadius: 0,
      }, {
        label: 'BOR 80',
        data: bor_80,
        type: 'line',
        fill: false,
        borderColor: '#74b9ff',
        pointRadius: 0,
      }, {
        label: 'BOR 90',
        data: bor_90,
        type: 'line',
        fill: false,
        borderColor: '#a29bfe',
        pointRadius: 0,
      }, {
        label: 'BTO 30',
        data: bto_30,
        type: 'line',
        fill: false,
        borderColor: '#e84118',
        pointRadius: 0,
      }, {
        label: 'BTO 20',
        data: bto_20,
        type: 'line',
        fill: false,
        borderColor: '#0097e6',
        pointRadius: 0,
      }, {
        label: 'BTO 15',
        data: bto_15,
        type: 'line',
        fill: false,
        borderColor: '#e1b12c',
        pointRadius: 0,
      }, {
        label: 'BTO 12.5',
        data: bto_125,
        type: 'line',
        fill: false,
        borderColor: '#44bd32',
        pointRadius: 0,
      }, ],
      labels: label
    },
    options: {
      responsive: true,
      legend: {
        position: 'bottom',
      },
      hover: {
        mode: 'label'
      },
      scales: {
        xAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            stepSize: step
          },
        }],
        yAxes: [{
          display: true,
          ticks: {
            max: max,
            min: 0,
            stepSize: step
          }
        }]
      },
      title: {
        display: true,
        text: 'Chart.js Line Chart - Legend'
      }
    }
  });

  function fbor_50(x) {
    return x;
  }

  function fbor_70(x) {
    return (7 / 3) * x;
  }

  function fbor_80(x) {
    return 4 * x;
  }

  function fbor_90(x) {
    return 9 * x;
  }

  function fbto_30(x) {
    var days = 365;//<?= @$days ?>;
    var b = days / 30;

    return (-1 * x) + b;
  }

  function fbto_20(x) {
    var days = 365;//<?= @$days ?>;
    var b = days / 20;

    return (-1 * x) + b;
  }

  function fbto_15(x) {
    var days = 365;//<?= @$days ?>;
    var b = days / 15;

    return (-1 * x) + b;
  }

  function fbto_125(x) {
    var days = 365;//<?= @$days ?>;
    var b = days / 12.5;

    return (-1 * x) + b;
  }
</script>