<script type="text/javascript">
  $(document).ready(function () {
    $("#form-search").validate( {
      rules: {
        tahun: { valueNotEquals: "" },
      },
      messages: {
        tahun: { valueNotEquals: "Silahkan Pilih Tahun!" },
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container")).addClass('mt-n1');
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    $('#jenisreg_st').bind('change',function(e) {
      e.preventDefault();
      var i = $(this).val();
      _get_lokasi(i);
    })

    <?php if (@$cookie['search']['jenisreg_st']): ?>
      _get_lokasi('<?=@$cookie['search']['jenisreg_st']?>', '<?=@$cookie['search']['lokasi_id']?>');
    <?php endif; ?>

    function _get_lokasi(i, j) {
      $.post('<?=site_url($nav['nav_url'].'/ajax/get_lokasi')?>', {jenisreg_st: i, lokasi_id: j},function(data) {
        $('#box_lokasi').html(data.html);
      },'json');
    }
  })
</script>