<script>
  $(document).ready(function() {
    $(".table-responsive tbody").overlayScrollbars({});
  });
</script>
<div class="table-responsive">
  <table class="table table-hover table-striped table-bordered table-fixed">
    <thead>
      <tr>
        <th class="text-center text-middle" width="36">No</th>
        <th class="text-center text-middle" width="100">No.RM</th>
        <th class="text-center text-middle">Nama Pasien</th>
        <th class="text-center text-middle" width="30">JK</th>
        <th class="text-center text-middle">Alamat</th>
        <th class="text-center text-middle" width="100">Jenis Pasien</th>
        <th class="text-center text-middle" width="150">Nama Poli</th>
        <th class="text-center text-middle" width="150">Tgl.Registrasi</th>
      </tr>
    </thead>
    <?php if (@$main == null) : ?>
      <tbody>
        <tr>
          <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
        </tr>
      </tbody>
    <?php else : ?>
      <tbody>
        <?php
        $nomor = 1;
        foreach ($main as $row) :
        ?>
          <tr>
            <td class="text-center" width="36"><?= ($nomor++) ?></td>
            <td class="text-center" width="100"><?= $row['pasien_id'] ?></td>
            <td class="text-left"><?= $row['pasien_nm'] ?></td>
            <td class="text-center" width="30"><?= $row['sex_cd'] ?></td>
            <td class="text-left">
              <?= ($row['alamat'] != '') ? $row['alamat'] . ', ' : '' ?>
              <?= ($row['kelurahan'] != '') ? ucwords(strtolower($row['kelurahan'])) . ', ' : '' ?>
              <?= ($row['kecamatan'] != '') ? ucwords(strtolower($row['kecamatan'])) . ', ' : '' ?>
              <?= ($row['kabupaten'] != '') ? ucwords(strtolower($row['kabupaten'])) . ', ' : '' ?>
              <?= ($row['provinsi'] != '') ? ucwords(strtolower($row['provinsi'])) : '' ?>
            </td>
            <td class="text-center" width="100"><?= $row['jenispasien_nm'] ?></td>
            <td class="text-center" width="150"><?= $row['lokasi_nm'] ?></td>
            <td class="text-center" width="150"><?= to_date($row['tgl_registrasi'], '', 'full_date', '') ?></td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    <?php endif; ?>
  </table>
</div>