<script>
  $(document).ready(function() {
    $(".table-responsive tbody").overlayScrollbars({});
  });
</script>
<div class="table-responsive">
  <table class="table table-hover table-striped table-bordered table-fixed" style="width: 1800px;">
    <thead>
      <tr>
        <th class="text-center text-middle" rowspan="2" width="36">No</th>
        <th class="text-center text-middle" rowspan="2" width="300">Item Radiologi</th>
        <th class="text-center text-middle" colspan="31" width="1550">Jumlah Pengeluaran Setiap Hari</th>
        <th class="text-center text-middle" rowspan="2" width="70">Total</th>
        <th class="text-center text-middle" width="100">Jumlah</th>
      </tr>
      <tr>
        <?php
        for ($i = 1; $i <= 31; $i++) :
          $num = sprintf("%02d", $i);
        ?>
          <th class="text-center text-middle" width="50"><?= $num ?></th>
        <?php endfor; ?>
        <th class="text-center text-middle" width="100">Tagihan</th>
      </tr>
    </thead>
    <?php if (@$main == null) : ?>
      <tbody>
        <tr>
          <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
        </tr>
      </tbody>
    <?php else : ?>
      <tbody>
        <?php
        $tot_tagihan = 0;
        foreach ($main as $row) : ?>
          <tr>
            <td class="text-left text-top"><b><?= $row['itemrad_nm'] ?></b></td>
          </tr>
          <?php $nomor = 1;
          foreach ($row['list_item'] as $item) :
            $tot_tagihan += $item['jml_tagihan'];
          ?>
            <tr>
              <td class="text-center" width="36"><?= ($nomor++) ?></td>
              <td class="text-left" width="300"><?= $item['itemrad_nm'] ?></td>
              <?php
              for ($i = 1; $i <= 31; $i++) :
                $num = $i;
              ?>
                <td class="text-center" width="50"><?= num_id($item['tgl_' . $num]) ?></td>
              <?php endfor; ?>
              <td class="text-center" width="70"><?= num_id($item['total']) ?></td>
              <td class="text-right" width="100"><?= num_id($item['jml_tagihan']) ?></td>
            </tr>
          <?php endforeach; ?>
        <?php endforeach; ?>
        <tr>
          <td class="text-center" colspan="34" width="1956"><b>TOTAL</b></td>
          <td class="text-right" width="100"><b>Rp. <?= num_id($tot_tagihan) ?></b></td>
        </tr>
      </tbody>
    <?php endif; ?>
  </table>
</div>