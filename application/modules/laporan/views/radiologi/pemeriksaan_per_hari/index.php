<?php $this->load->view('_js_index'); ?>
<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline col-title">
        <div class="col-back">
          <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
        </div>
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          Laporan Pemeriksaan Radiologi Per Hari
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item"><a href="#"><i class="fas fa-flask"></i> Laporan Radiologi</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item active"><span>Index</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>
  <!-- flash message -->
  <div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
  <!-- /flash message -->
  <div class="row full-page mt-4 mb-n2">
    <div class="col-lg-12 grid-margin-xl-0 stretch-card">
      <div class="card border-none">
        <div class="card-body">
          <form id="form-search" action="<?= site_url() . '/app/search_redirect/' . $nav['nav_id'] . '.radiologipemeriksaanperhari' ?>" method="post" autocomplete="off">
            <input type="hidden" name="redirect" value="laporan/radiologi/pemeriksaan_per_hari">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Tanggal</label>
                  <div class="col-lg-5 col-md-5">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datepicker" name="tgl" id="tgl" value="<?= @$cookie['search']['tgl'] ?>">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 row">
                <div class="col-md-4">
                  <?php if (@$cookie['search']['tgl'] != '') : ?>
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-print"></i> Cetak Data
                      </button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?= site_url('laporan/radiologi/pemeriksaan_per_hari/cetak_excel') ?>" target="_blank"><i class="fas fa-file-excel"></i> Cetak Excel</a>
                      </div>
                    </div>
                  <?php endif; ?>
                </div>
                <div class="col-md-6 pl-2">
                  <button type="submit" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Tampilkan Data"><i class="fas fa-search"></i> Tampilkan</button>
                  <a class="btn btn-xs btn-default" href="<?= site_url() . '/app/reset_redirect/' . $nav['nav_id'] . '.radiologipemeriksaanperhari/' . str_replace('/', '-', 'laporan/radiologi/pemeriksaan_per_hari') ?>" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-default" title="" data-original-title="Reset"><i class="fas fa-sync-alt"></i> Reset</a>
                </div>
              </div>
            </div>
          </form>
          <div class="row mt-3">
            <div class="col-md-12">
              <div class="table-responsive">
                <form id="form-multiple" action="<?= site_url() . '/' . $nav['nav_url'] . '/multiple/enable' ?>" method="post">
                  <table class="table table-hover table-striped table-bordered table-fixed">
                    <thead>
                      <tr>
                        <th class="text-center text-middle" width="36">No</th>
                        <th class="text-center text-middle" width="100">Nomor Pemeriksaan</th>
                        <th class="text-center text-middle" width="100">Tgl.Order</th>
                        <th class="text-center text-middle" width="100">Tgl.Hasil</th>
                        <th class="text-center text-middle" width="100">No.RM</th>
                        <th class="text-center text-middle">Nama Pasien</th>
                        <th class="text-center text-middle">Alamat</th>
                        <th class="text-center text-middle" width="30">JK</th>
                        <th class="text-center text-middle" width="180">Jenis Pasien & Lokasi</th>
                        <th class="text-center text-middle" width="70">Periksa</th>
                        <th class="text-center text-middle" width="130">Jml.Tagihan</th>
                      </tr>
                    </thead>
                    <?php if (@$main == null) : ?>
                      <tbody>
                        <tr>
                          <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                        </tr>
                      </tbody>
                    <?php else : ?>
                      <tbody>
                        <?php $i = 1;
                        $tot_tagihan = 0;
                        foreach ($main as $row) :
                          $tot_tagihan += $row['jml_tagihan'];
                        ?>
                          <tr>
                            <td class="text-center text-top" width="36"><?= ($i++) ?></td>
                            <td class="text-center text-top" width="100"><?= $row['pemeriksaan_id'] ?></td>
                            <td class="text-center text-top" width="100"><?= to_date($row['tgl_order'], '', 'full_date') ?></td>
                            <td class="text-center text-top" width="100"><?= to_date($row['tgl_hasil'], '', 'full_date') ?></td>
                            <td class="text-center text-top" width="100"><?= $row['pasien_id'] ?></td>
                            <td class="text-left text-top"><?= $row['pasien_nm'] . ', ' . $row['sebutan_cd'] ?> <br> <?= $row['umur_thn'] ?> th <?= $row['umur_bln'] ?> bl <?= $row['umur_hr'] ?> hr</td>
                            <td class="text-left text-top">
                              <?= ($row['alamat'] != '') ? $row['alamat'] . ', ' : '' ?>
                              <?= ($row['kelurahan'] != '') ? ucwords(strtolower($row['kelurahan'])) . ', ' : '' ?>
                              <?= ($row['kecamatan'] != '') ? ucwords(strtolower($row['kecamatan'])) . ', ' : '' ?>
                              <?= ($row['kabupaten'] != '') ? ucwords(strtolower($row['kabupaten'])) . ', ' : '' ?>
                              <?= ($row['provinsi'] != '') ? ucwords(strtolower($row['provinsi'])) : '' ?>
                            </td>
                            <td class="text-center text-top" width="30"><?= $row['sex_cd'] ?></td>
                            <td class="text-center text-top" width="180">
                              <?= $row['jenispasien_nm'] ?> <br>
                              <?= ($row['jenispasien_id'] == '02') ? $row['no_kartu'] . '<br>' : '' ?>
                              <?= $row['lokasi_nm'] ?>
                            </td>
                            <td class="text-center text-top" width="70"><?= ($row['periksa_st'] == 1) ? 'SUDAH' : 'BELUM' ?></td>
                            <td class="text-right text-top" width="130"><?= num_id($row['jml_tagihan']) ?></td>
                          </tr>
                        <?php endforeach; ?>
                        <tr>
                          <td class="text-center text-top" colspan="10"><b>TOTAL</b></td>
                          <td class="text-right text-top" width="130"><b>Rp. <?= num_id($tot_tagihan) ?></b></td>
                        </tr>
                      </tbody>
                    <?php endif; ?>
                  </table>
                </form>
              </div>
            </div>
          </div><!-- /.row -->
        </div>
      </div>
    </div>
  </div>
</div>