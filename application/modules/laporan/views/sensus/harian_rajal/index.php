<?php $this->load->view('_js_index'); ?>
<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline col-title">
        <div class="col-back">
          <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
        </div>
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          Sensus Harian Rawat Jalan
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item"><a href="#"><i class="fas fa-notes-medical"></i> Laporan</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item active"><span>Index</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>
  <!-- flash message -->
  <div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
  <!-- /flash message -->
  <div class="row full-page mt-4 mb-n2">
    <div class="col-lg-12 grid-margin-xl-0 stretch-card">
      <div class="card border-none">
        <div class="card-body">
          <form id="form-search" action="<?= site_url() . '/app/search/' . $nav['nav_id'] ?>" method="post" autocomplete="off">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Tanggal</label>
                  <div class="col-lg-5 col-md-5">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datepicker" name="tgl" id="tgl" value="<?= @$cookie['search']['tgl'] ?>">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Lokasi</label>
                  <div class="col-lg-5 col-md-5">
                    <select class="form-control chosen-select" name="lokasi_id" id="lokasi_id" required>
                      <option value="">---</option>
                      <?php foreach ($lokasi as $r) : ?>
                        <option value="<?= $r['lokasi_id'] ?>" <?= (@$cookie['search']['lokasi_id'] == $r['lokasi_id']) ? 'selected' : '' ?>><?= $r['lokasi_nm'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-8">
                <div class="text-right font-weight-bold" style="font-size: 15px;">SENSUS HARIAN RAWAT JALAN</div>
                <div class="text-right font-weight-semibold mt-1" style="font-size: 14px;">
                  Periode Tanggal <?= @$cookie['search']['tgl'] ?>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 row">
                <div class="col-md-4">
                  <?php if (@$cookie['search']['periode_data'] != '') : ?>
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-print"></i> Cetak Data
                      </button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?= site_url('laporan/laporan/duapuluh_penyakit/cetak_excel') ?>" target="_blank"><i class="fas fa-file-excel"></i> Cetak Excel</a>
                      </div>
                    </div>
                  <?php endif; ?>
                </div>
                <div class="col-md-6 pl-2">
                  <button type="submit" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Tampilkan Data"><i class="fas fa-search"></i> Tampilkan</button>
                  <a class="btn btn-xs btn-default" href="<?= site_url() . '/app/reset/' . $nav['nav_id'] ?>" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-default" title="" data-original-title="Reset"><i class="fas fa-sync-alt"></i> Reset</a>
                </div>
              </div>
            </div>
          </form>

          <div class="row mt-3">
            <div class="col-md-12">
              <div class="table-responsive">
                <form id="form-multiple" action="<?= site_url() . '/' . $nav['nav_url'] . '/multiple/enable' ?>" method="post">
                  <table class="table table-hover table-striped table-bordered table-fixed">
                    <thead>
                      <tr>
                        <th class="text-center text-middle" rowspan="2" width="30">No</th>
                        <th class="text-center text-middle" rowspan="2" width="200">Nama Pasien</th>
                        <th class="text-center text-middle" rowspan="2" width="100">No. RM</th>
                        <th class="text-center text-middle" rowspan="2" width="100">Umur</th>
                        <th class="text-center text-middle" colspan="2" width="100">Cara Pembayaran</th>
                        <th class="text-center text-middle" colspan="2" width="100">Kunjungan</th>
                        <th class="text-center text-middle" colspan="2" width="150">Asal Pasien</th>
                        <th class="text-center text-middle" colspan="3" width="180">Keadaan Pasien</th>
                        <th class="text-center text-middle" rowspan="2">Diagnosa</th>
                        <th class="text-center text-middle" rowspan="2" width="50">ICD</th>
                        <th class="text-center text-middle" rowspan="2">Nama Dokter</th>
                      </tr>
                      <tr>
                        <td class="text-center text-middle" width="50">U</td>
                        <td class="text-center text-middle" width="50">As</td>
                        <td class="text-center text-middle" width="50">B</td>
                        <td class="text-center text-middle" width="50">L</td>
                        <td class="text-center text-middle" width="75">Dtg Sendiri</td>
                        <td class="text-center text-middle" width="75">Rujukan</td>
                        <td class="text-center text-middle" width="60">Dirawat</td>
                        <td class="text-center text-middle" width="60">Dirujuk</td>
                        <td class="text-center text-middle" width="60">Pulang</td>
                      </tr>
                    </thead>
                    <?php if (@$main == null) : ?>
                      <tbody>
                        <tr>
                          <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                        </tr>
                      </tbody>
                    <?php else : ?>
                      <tbody>
                        <?php
                        $i = 1;
                        foreach ($main as $row) :
                        ?>
                          <tr>
                            <td class="text-center" width="30"><?= $i++ ?></td>
                            <td class="text-left" width="200"><?= $row['pasien_nm'] . ', ' . $row['sebutan_cd'] ?></td>
                            <td class="text-center" width="100"><?= $row['pasien_id'] ?></td>
                            <td class="text-center text-middle" width="100">
                              <?= $row['umur_thn'] ?> th
                              <?= $row['umur_bln'] ?> bl
                              <?= $row['umur_hr'] ?> hr
                            </td>
                            <td class="text-center text-middle" width="50"><?= ($row['jenispasien_nm'] == 'UMUM') ? '<i class="fas fa-check"></i>' : '' ?></td>
                            <td class="text-center text-middle" width="50"><?= ($row['jenispasien_nm'] != 'UMUM') ? '<i class="fas fa-check"></i>' : '' ?></td>
                            <td class="text-center text-middle" width="50"><?= ($row['jeniskunjungan_cd'] == 'B') ? '<i class="fas fa-check"></i>' : '' ?></td>
                            <td class="text-center text-middle" width="50"><?= ($row['jeniskunjungan_cd'] == 'L') ? '<i class="fas fa-check"></i>' : '' ?></td>
                            <td class="text-center text-middle" width="75"><?= ($row['asalpasien_cd'] == '01') ? '<i class="fas fa-check"></i>' : '' ?></td>
                            <td class="text-center text-middle" width="75"><?= ($row['asalpasien_cd'] != '01') ? '<i class="fas fa-check"></i>' : '' ?></td>
                            <td class="text-center text-middle" width="60"><?= ($row['tindaklanjut_cd'] == '02') ? '<i class="fas fa-check"></i>' : '' ?></td>
                            <td class="text-center text-middle" width="60"><?= ($row['tindaklanjut_cd'] == '03') ? '<i class="fas fa-check"></i>' : '' ?></td>
                            <td class="text-center text-middle" width="60"><?= ($row['tindaklanjut_cd'] == '00') ? '<i class="fas fa-check"></i>' : '' ?></td>
                            <td><?=$row['diagnosis']?></td>
                            <td class="text-left text-middle" width="50"><?=$row['icdx']?></td>
                            <td><?=$row['dokter_nm']?></td>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                    <?php endif; ?>
                  </table>
                </form>
              </div>
            </div>
          </div><!-- /.row -->

        </div>
      </div>
    </div>
  </div>
</div>