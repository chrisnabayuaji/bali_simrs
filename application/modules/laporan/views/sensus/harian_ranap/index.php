<?php $this->load->view('_js_index'); ?>
<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline col-title">
        <div class="col-back">
          <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
        </div>
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          Sensus Harian Rawat Inap
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item"><a href="#"><i class="fas fa-notes-medical"></i> Laporan</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item active"><span>Index</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>
  <!-- flash message -->
  <div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
  <!-- /flash message -->
  <div class="row full-page mt-4 mb-n2">
    <div class="col-lg-12 grid-margin-xl-0 stretch-card">
      <div class="card border-none">
        <div class="card-body">
          <form id="form-search" action="<?= site_url() . '/app/search/' . $nav['nav_id'] ?>" method="post" autocomplete="off">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Tanggal</label>
                  <div class="col-lg-5 col-md-5">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datepicker" name="tgl" id="tgl" value="<?= @$cookie['search']['tgl'] ?>">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Lokasi</label>
                  <div class="col-lg-5 col-md-5">
                    <select class="form-control chosen-select" name="lokasi_id" id="lokasi_id" required>
                      <option value="">---</option>
                      <?php foreach ($lokasi as $r) : ?>
                        <option value="<?= $r['lokasi_id'] ?>" <?= (@$cookie['search']['lokasi_id'] == $r['lokasi_id']) ? 'selected' : '' ?>><?= $r['lokasi_nm'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-8">
                <div class="text-right font-weight-bold" style="font-size: 15px;">SENSUS HARIAN RAWAT INAP</div>
                <div class="text-right font-weight-semibold mt-1" style="font-size: 14px;">
                  Periode Tanggal <?= @$cookie['search']['tgl'] ?>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 row">
                <div class="col-md-4">
                  <?php if (@$cookie['search']['periode_data'] != '') : ?>
                    <div class="btn-group">
                      <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-print"></i> Cetak Data
                      </button>
                      <div class="dropdown-menu">
                        <a class="dropdown-item" href="<?= site_url('laporan/laporan/duapuluh_penyakit/cetak_excel') ?>" target="_blank"><i class="fas fa-file-excel"></i> Cetak Excel</a>
                      </div>
                    </div>
                  <?php endif; ?>
                </div>
                <div class="col-md-6 pl-2">
                  <button type="submit" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Tampilkan Data"><i class="fas fa-search"></i> Tampilkan</button>
                  <a class="btn btn-xs btn-default" href="<?= site_url() . '/app/reset/' . $nav['nav_id'] ?>" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-default" title="" data-original-title="Reset"><i class="fas fa-sync-alt"></i> Reset</a>
                </div>
              </div>
            </div>
          </form>
          <div class="row mt-3">
            <div class="col-md-8">
              <h6>Pasien Masuk</h6>
              <table class="table table-hover table-striped table-bordered">
                <thead>
                  <tr>
                    <th class="text-center text-middle" width="30">No</th>
                    <th class="text-center text-middle" width="100">Nomor RM</th>
                    <th class="text-center text-middle" width="100">Nomor Registrasi</th>
                    <th class="text-center text-middle" width="200">Nama Pasien</th>
                    <th class="text-center text-middle" width="100">Kelas</th>
                    <th class="text-center text-middle">Diagnosa</th>
                  </tr>
                </thead>
                <?php if (@$pasien_masuk == null) : ?>
                  <tbody>
                    <tr>
                      <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                    </tr>
                  </tbody>
                <?php else : ?>
                  <tbody>
                    <?php
                    $i = 1;
                    foreach ($pasien_masuk as $row) :
                    ?>
                      <tr>
                        <td class="text-center" width="30"><?= $i++ ?></td>
                        <td class="text-center" width="100"><?= $row['pasien_id'] ?></td>
                        <td class="text-center" width="100"><?= $row['reg_id'] ?></td>
                        <td class="text-left" width="200"><?= $row['pasien_nm'] ?>, <?= $row['sebutan_cd'] ?></td>
                        <td class="text-center" width="100"><?= $row['kelas_nm'] ?></td>
                        <td><?= $row['icdx'] ?> - <?= $row['diagnosis'] ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                <?php endif; ?>
              </table>
            </div>
          </div><!-- /.row -->
          <div class="row mt-3">
            <div class="col-md-9">
              <h6>Pasien Pindahan</h6>
              <table class="table table-hover table-striped table-bordered">
                <thead>
                  <tr>
                    <th class="text-center text-middle" width="30">No</th>
                    <th class="text-center text-middle" width="100">Nomor RM</th>
                    <th class="text-center text-middle" width="100">Nomor Registrasi</th>
                    <th class="text-center text-middle" width="200">Nama Pasien</th>
                    <th class="text-center text-middle" width="100">Kelas</th>
                    <th class="text-center text-middle" width="100">Pindahan Dari</th>
                    <th class="text-center text-middle">Diagnosa</th>
                  </tr>
                </thead>
                <?php if (@$pasien_pindahan == null) : ?>
                  <tbody>
                    <tr>
                      <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                    </tr>
                  </tbody>
                <?php else : ?>
                  <tbody>
                    <?php
                    $i = 1;
                    foreach ($pasien_pindahan as $row) :
                    ?>
                      <tr>
                        <td class="text-center" width="30"><?= $i++ ?></td>
                        <td class="text-center" width="100"><?= $row['pasien_id'] ?></td>
                        <td class="text-center" width="100"><?= $row['reg_id'] ?></td>
                        <td class="text-left" width="200"><?= $row['pasien_nm'] ?>, <?= $row['sebutan_cd'] ?></td>
                        <td class="text-center" width="100"><?= $row['kelas_nm'] ?></td>
                        <td class="text-center" width="100"><?= $row['lokasi_asal_nm'] ?></td>
                        <td><?= $row['icdx'] ?> - <?= $row['diagnosis'] ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                <?php endif; ?>
              </table>
            </div>
          </div><!-- /.row -->
          <div class="row mt-3">
            <div class="col-md-9">
              <h6>Pasien Keluar</h6>
              <table class="table table-hover table-striped table-bordered">
                <thead>
                  <tr>
                    <th class="text-center text-middle" rowspan="2" width="30">No</th>
                    <th class="text-center text-middle" rowspan="2" width="100">Nomor RM</th>
                    <th class="text-center text-middle" rowspan="2" width="100">Nomor Registrasi</th>
                    <th class="text-center text-middle" rowspan="2" width="200">Nama Pasien</th>
                    <th class="text-center text-middle" rowspan="2" width="100">Kelas</th>
                    <th class="text-center text-middle" colspan="2" width="100">Cara Pasien Keluar</th>
                    <th class="text-center text-middle">Diagnosa</th>
                  </tr>
                  <tr>
                    <th class="text-center text-middle" width="50">Pulang</th>
                    <th class="text-center text-middle" width="50">Dirujuk</th>
                  </tr>
                </thead>
                <?php if (@$pasien_keluar == null) : ?>
                  <tbody>
                    <tr>
                      <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                    </tr>
                  </tbody>
                <?php else : ?>
                  <tbody>
                    <?php
                    $i = 1;
                    foreach ($pasien_keluar as $row) :
                    ?>
                      <tr>
                        <td class="text-center" width="30"><?= $i++ ?></td>
                        <td class="text-center" width="100"><?= $row['pasien_id'] ?></td>
                        <td class="text-center" width="100"><?= $row['reg_id'] ?></td>
                        <td class="text-left" width="200"><?= $row['pasien_nm'] ?>, <?= $row['sebutan_cd'] ?></td>
                        <td class="text-center" width="100"><?= $row['kelas_nm'] ?></td>
                        <td class="text-center text-middle" width="60"><?= ($row['tindaklanjut_cd'] == '00') ? '<i class="fas fa-check"></i>' : '' ?></td>
                        <td class="text-center text-middle" width="60"><?= ($row['tindaklanjut_cd'] != '00') ? '<i class="fas fa-check"></i>' : '' ?></td>
                        <td><?= $row['icdx'] ?> - <?= $row['diagnosis'] ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                <?php endif; ?>
              </table>
            </div>
          </div><!-- /.row -->
          <div class="row mt-3">
            <div class="col-md-9">
              <h6>Pasien Dipindahkan</h6>
              <table class="table table-hover table-striped table-bordered">
                <thead>
                  <tr>
                    <th class="text-center text-middle" width="30">No</th>
                    <th class="text-center text-middle" width="100">Nomor RM</th>
                    <th class="text-center text-middle" width="100">Nomor Registrasi</th>
                    <th class="text-center text-middle" width="200">Nama Pasien</th>
                    <th class="text-center text-middle" width="100">Kelas</th>
                    <th class="text-center text-middle" width="50">Dipindah ke</th>
                    <th class="text-center text-middle" width="50">Lama Dirawat</th>
                    <th class="text-center text-middle">Diagnosa</th>
                  </tr>
                </thead>
                <?php if (@$pasien_dipindah == null) : ?>
                  <tbody>
                    <tr>
                      <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                    </tr>
                  </tbody>
                <?php else : ?>
                  <tbody>
                    <?php
                    $i = 1;
                    foreach ($pasien_dipindah as $row) :
                    ?>
                      <tr>
                        <td class="text-center" width="30"><?= $i++ ?></td>
                        <td class="text-center" width="100"><?= $row['pasien_id'] ?></td>
                        <td class="text-center" width="100"><?= $row['reg_id'] ?></td>
                        <td class="text-left" width="200"><?= $row['pasien_nm'] ?>, <?= $row['sebutan_cd'] ?></td>
                        <td class="text-center" width="100"><?= $row['kelas_nm'] ?></td>
                        <td class="text-center" width="100"><?= $row['lokasi_tujuan_nm'] ?></td>
                        <td class="text-center" width="100"><?= $row['jml_hari'] ?></td>
                        <td><?= $row['icdx'] ?> - <?= $row['diagnosis'] ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                <?php endif; ?>
              </table>
            </div>
          </div><!-- /.row -->
          <div class="row mt-3">
            <div class="col-md-9">
              <h6>Pasien Meninggal</h6>
              <table class="table table-hover table-striped table-bordered">
                <thead>
                  <tr>
                    <th class="text-center text-middle" rowspan="2" width="30">No</th>
                    <th class="text-center text-middle" rowspan="2" width="100">Nomor RM</th>
                    <th class="text-center text-middle" rowspan="2" width="100">Nomor Registrasi</th>
                    <th class="text-center text-middle" rowspan="2" width="200">Nama Pasien</th>
                    <th class="text-center text-middle" rowspan="2" width="100">Kelas</th>
                    <th class="text-center text-middle" colspan="2" width="100">Meninggal</th>
                    <th class="text-center text-middle">Diagnosa</th>
                  </tr>
                  <tr>
                    <th class="text-center text-middle" width="50">
                      < 48 j</th> <th class="text-center text-middle" width="50">> 48 j
                    </th>
                  </tr>
                </thead>
                <?php if (@$pasien_meninggal == null) : ?>
                  <tbody>
                    <tr>
                      <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                    </tr>
                  </tbody>
                <?php else : ?>
                  <tbody>
                    <?php
                    $i = 1;
                    foreach ($pasien_meninggal as $row) :
                    ?>
                      <tr>
                        <td class="text-center" width="30"><?= $i++ ?></td>
                        <td class="text-center" width="100"><?= $row['pasien_id'] ?></td>
                        <td class="text-center" width="100"><?= $row['reg_id'] ?></td>
                        <td class="text-left" width="200"><?= $row['pasien_nm'] ?>, <?= $row['sebutan_cd'] ?></td>
                        <td class="text-center" width="100"><?= $row['kelas_nm'] ?></td>
                        <td class="text-center" width="100"><?= ($row['jam'] < 48) ? '<i class="fas fa-check"></i>' : '' ?></td>
                        <td class="text-center" width="100"><?= ($row['jam'] >= 48) ? '<i class="fas fa-check"></i>' : '' ?></td>
                        <td><?= $row['icdx'] ?> - <?= $row['diagnosis'] ?></td>
                      </tr>
                    <?php endforeach; ?>
                  </tbody>
                <?php endif; ?>
              </table>
            </div>
          </div><!-- /.row -->
          <div class="row mt-3">
            <div class="col-md-12">
              <h6>Rekapitulasi Harian Sensus Rawat Inap</h6>
              <table class="table table-hover table-striped table-bordered">
                <thead>
                  <tr>
                    <th class="text-center text-middle" rowspan="3" width="30">Pasien Awal</th>
                    <th class="text-center text-middle" colspan="3" width="100">Pasien Masuk Ruangan</th>
                    <th class="text-center text-middle" colspan="6" width="100">Pasien Keluar Ruangan</th>
                    <th class="text-center text-middle" rowspan="3" width="100">Pasien Masih Dirawat</th>
                  </tr>
                  <tr>
                    <th class="text-center text-middle" rowspan="2" width="50">Pasien Masuk</th>
                    <th class="text-center text-middle" rowspan="2" width="50">Pasien Pindahan</th>
                    <th class="text-center text-middle" rowspan="2" width="50">Jumlah (1+2+3)</th>
                    <th class="text-center text-middle" colspan="2" width="50">Pasien Keluar</th>
                    <th class="text-center text-middle" colspan="3" width="50">Pasien Mati</th>
                    <th class="text-center text-middle" rowspan="2" width="50">Jml (5+6+7+8)</th>
                  </tr>
                  <tr>
                    <th class="text-center text-middle" width="50">Hidup</th>
                    <th class="text-center text-middle" width="50">Dirujuk</th>
                    <th class="text-center text-middle" width="50">
                      < 48 j</th> <th class="text-center text-middle" width="50">> 48 j
                    </th>
                    <th class="text-center text-middle" width="50">Jml (7+8)</th>
                  </tr>
                  <tr>
                    <th class="text-center text-middle">(1)</th>
                    <th class="text-center text-middle">(2)</th>
                    <th class="text-center text-middle">(3)</th>
                    <th class="text-center text-middle">(4)</th>
                    <th class="text-center text-middle">(5)</th>
                    <th class="text-center text-middle">(6)</th>
                    <th class="text-center text-middle">(7)</th>
                    <th class="text-center text-middle">(8)</th>
                    <th class="text-center text-middle">(9)</th>
                    <th class="text-center text-middle">(10)</th>
                    <th class="text-center text-middle">(11)</th>
                  </tr>
                </thead>
                <?php if (@$rekapitulasi == null) : ?>
                  <tbody>
                    <tr>
                      <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                    </tr>
                  </tbody>
                <?php else : ?>
                  <tbody>
                    <tr>
                      <td class="text-center"><?= $rekapitulasi['jml_awal'] ?></td>
                      <td class="text-center"><?= $rekapitulasi['pasien_masuk'] ?></td>
                      <td class="text-center"><?= $rekapitulasi['pasien_pindahan'] ?></td>
                      <td class="text-center"><?= $masuk = $rekapitulasi['jml_awal'] + $rekapitulasi['pasien_masuk'] + $rekapitulasi['pasien_pindahan'] ?></td>
                      <td class="text-center"><?= $rekapitulasi['pasien_pulang'] ?></td>
                      <td class="text-center"><?= $rekapitulasi['pasien_rujuk'] ?></td>
                      <td class="text-center"><?= $rekapitulasi['pasien_meninggal'] ?></td>
                      <td class="text-center"><?= $rekapitulasi['pasien_meninggal_48'] ?></td>
                      <td class="text-center"><?= $rekapitulasi['pasien_meninggal'] + $rekapitulasi['pasien_meninggal_48'] ?></td>
                      <td class="text-center"><?= $keluar = $rekapitulasi['pasien_pulang'] + $rekapitulasi['pasien_rujuk'] + $rekapitulasi['pasien_meninggal'] + $rekapitulasi['pasien_meninggal_48'] ?></td>
                      <td class="text-center"><?=$masuk - $keluar?></td>
                    </tr>
                  </tbody>
                <?php endif; ?>
              </table>
            </div>
          </div><!-- /.row -->
        </div>
      </div>
    </div>
  </div>
</div>