<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Berita extends MY_Controller
{

	var $nav_id = '02.05.07', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'm_berita'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id);
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'news_id', 'type' => 'asc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(4, 0);
		$this->cookie['total_rows'] = $this->m_berita->all_rows($this->cookie);
		set_cookie_nav($this->nav_id, $this->cookie);
		//main data
		$data['nav'] = $this->nav;
		$data['cookie'] = $this->cookie;
		$data['main'] = $this->m_berita->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
		//set pagination
		set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('master/berita/index', $data);
	}

	public function form($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_berita->get_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save/' . $id;

		$this->render('master/berita/form', $data);
	}

	public function save($id = null)
	{
		$this->m_berita->save($id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($id != '') ? '_update' : '_add', $this->nav_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	public function delete($id = null)
	{
		$this->m_berita->delete($id, true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log('_delete', $this->nav_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	public function multiple($type = null)
	{
		$data = $this->input->post();
		if (isset($data['checkitem'])) {
			foreach ($data['checkitem'] as $key) {
				switch ($type) {
					case 'delete':
						$this->authorize($this->nav, '_delete');
						$this->m_berita->delete($key, true);
						$flash = 'Data berhasil dihapus.';
						create_log('_delete', $this->nav_id);
						break;

					case 'enable':
						$this->authorize($this->nav, '_update');
						$this->m_berita->update($key, array('is_active' => 1));
						$flash = 'Data berhasil diaktifkan.';
						create_log('_update', $this->nav_id);
						break;

					case 'disable':
						$this->authorize($this->nav, '_update');
						$this->m_berita->update($key, array('is_active' => 0));
						$flash = 'Data berhasil dinonaktifkan.';
						create_log('_delete', $this->nav_id);
						break;
				}
			}
		}
		create_log($t, $this->this->menu['menu']);
		$this->session->set_flashdata('flash_success', $flash);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	function ajax($type = null, $id = null)
	{
		if ($type == 'cek_id') {
			$data = $this->input->post();
			$cek = $this->m_berita->get_data($data['news_id']);
			if ($id == null) {
				if ($cek == null) {
					echo 'true';
				} else {
					echo 'false';
				}
			} else {
				if ($id != $data['news_id'] && $cek != null) {
					echo 'false';
				} else {
					echo 'true';
				}
			}
		}

		if ($type == 'add_item_image') {
			$image_no = $this->input->get('image_no') + 1;
			$news_id = $this->input->get('news_id');
			//
			$html = '';
			//
			if ($news_id != '') {
				$list_image = $this->m_berita->get_image_by_news_id($news_id);
				//
				if (count($list_image) != '0') {
					foreach ($list_image as $li) {
						$html .= '<div class="form-group row box-image-' . $li['newsimage_id'] . '">
												<label class="col-lg-3 col-md-3 col-form-label">Gambar ' . $image_no . '</label>
												<div class="col-lg-6">
													<a href="' . base_url(@$li['image_path'] . @$li['image_name']) . '" title="Preview" target="_blank">
														<img src="' . base_url(@$li['image_path'] . @$li['image_name']) . '" style="width: 200px;">
													</a>	                            
													<a href="' . base_url(@$li['image_path'] . @$li['image_name']) . '" title="Preview" target="_blank"" class="btn btn-info btn-xs text-center mt-1"><i class="fas fa-eye"></i> View</a>
													<a href="javascript:void(0)" class="btn btn-danger btn-xs text-center mt-1 delete-image" data-id="' . $li['newsimage_id'] . '"><i class="fas fa-trash"></i> Delete</a>
												</div>
		                  </div>
											<input type="hidden" name="newsimage_id_' . $image_no . '" value="' . $li['newsimage_id'] . '">
											<div class="form-group row mt-2 box-image-' . $li['newsimage_id'] . '" style="border-bottom:1px solid #eee; margin-bottom: 10px;">
												<label class="col-lg-3 col-md-3 col-form-label mb-2">&nbsp;</label>
												<div class="col-lg-7 mb-2">
														<input type="file" class="form-control input-sm" name="image_source_' . $image_no . '" id="image_source_' . $image_no . '">
														<div class="text-danger small-text font-weight-semibold">* Upload gambar untuk mengubah</div>
												</div>

												<label class="col-lg-3 col-md-3 col-form-label">Deskripsi</label>
												<div class="col-lg-9">
													<input type="text" class="form-control input-sm" name="image_description_' . $image_no . '" placeholder="Masukan judul gambar disini ..." value="' . $li['image_description'] . '">
												</div>
											</div>';
						$image_no++;
					}
					$image_no = $image_no - 1;
				} else {
					$html .= '<div class="form-group row" style="border-bottom:1px solid #eee; margin-bottom: 10px;">
											<label class="col-lg-3 col-md-3 col-form-label">Gambar ' . $image_no . '</label>
											<div class="col-lg-7">
													<input type="file" class="form-control input-sm" name="image_source_' . $image_no . '" id="image_source_' . $image_no . '">
											</div>

											<label class="col-lg-3 col-md-3 col-form-label">Deskripsi</label>
												<div class="col-lg-9">
													<input type="text" class="form-control input-sm" name="image_description_' . $image_no . '" placeholder="Masukan deskripsi gambar disini ...">
												</div>
											</div>
										</div>';
				}
			} else {
				$html .= '<div class="form-group row" style="border-bottom:1px solid #eee; margin-bottom: 10px;">
										<label class="col-lg-3 col-md-3 col-form-label">Gambar ' . $image_no . '</label>
											<div class="col-lg-7">
													<input type="file" class="form-control input-sm" name="image_source_' . $image_no . '" id="image_source_' . $image_no . '">
											</div>

										<label class="col-lg-3 col-md-3 col-form-label">Deskripsi</label>
											<div class="col-lg-9">
												<input type="text" class="form-control input-sm" name="image_description_' . $image_no . '" placeholder="Masukan deskripsi gambar disini ...">
											</div>
										</div>
									</div>';
			}
			//
			$html .= '<script>
								$(function() {
									$(".delete-image").on("click", function (e) {
										e.preventDefault();
										Swal.fire({
											title: "Apakah Anda yakin?",
											text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
											type: "warning",
											showCancelButton: true,
											confirmButtonColor: "#eb3b5a",
											cancelButtonColor: "#b2bec3",
											confirmButtonText: "Hapus",
											cancelButtonText: "Batal",
											customClass: "swal-wide"
										}).then((result) => {
											if (result.value) {
												var i = $(this).attr("data-id");
												$.get("' . site_url('master/berita/ajax/delete_image') . '?newsimage_id="+i,null,function(data) {
													$(".box-image-"+i).fadeOut("fast");
												},"json");
												$.toast({
													heading: "Sukses",
													text: "Gambar berhasil dihapus",
													icon: "success",
													position: "top-right"
												})
											}
										})
									})
								});
								</script>';
			//			
			echo json_encode(array(
				'html' => $html,
				'image_no' => $image_no,
			));
		}

		if ($type == 'permalink') {
			$news_title = $this->input->post('news_title');
			$permalink = clean_url_news($news_title);
			//
			echo json_encode(array(
				'permalink'	=> $permalink
			));
		}

		if ($type == 'validate_news_url') {
			$news_url = $this->input->post('news_url');
			$news_url_exist = $this->input->post('news_url_exist');
			//
			if ($news_url != $news_url_exist) {
				$validate = $this->m_berita->validate_news_url($news_url);
			}
			//
			$result = 'true';
			if ($validate == true) $result = 'false';
			//
			echo json_encode(array(
				'result'	=> $result
			));
		}

		if ($type == 'delete_image') {
			$newsimage_id = $this->input->get('newsimage_id');
			//
			$result = $this->m_berita->delete_image($newsimage_id);
			$callback = 'false';
			if ($result) $callback = 'true';
			//
			echo json_encode(array(
				'callback' => $callback
			));
		}
	}
}
