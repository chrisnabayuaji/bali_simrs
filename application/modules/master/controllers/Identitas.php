<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Identitas extends MY_Controller
{

	var $nav_id = '02.01.01', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'm_identitas',
			'master/m_wilayah'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id);
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'role_id', 'type' => 'asc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/form');
	}

	public function form()
	{
		$this->authorize($this->nav, '_update');

		$data['nav'] = $this->nav;
		$data['main'] = $this->m_identitas->get_first();
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save';
		$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_parent('');
		$this->render('master/identitas/form', $data);
	}

	public function save($type = '', $val = '')
	{
		$this->m_identitas->update($type, $val);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log('_update', $this->nav_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	function ajax($id = null)
	{
		if ($id == 'get_wilayah_child') {
			$wilayah_parent = $this->input->get('wilayah_parent');
			$wilayah_prop = $this->input->get('wilayah_prop');
			$wilayah_kab = $this->input->get('wilayah_kab');
			$wilayah_kec = $this->input->get('wilayah_kec');
			$wilayah_kel = $this->input->get('wilayah_kel');
			//
			$wilayah_params = $this->m_wilayah->get_params($wilayah_parent);
			//
			$wilayah_selected = '';
			if ($wilayah_params['level'] == '2' && $wilayah_kab != '') { // kab
				$wilayah_selected = $wilayah_kab;
			} else if ($wilayah_params['level'] == '3' && $wilayah_kec != '') { // kec
				$wilayah_selected = $wilayah_kec;
			} else if ($wilayah_params['level'] == '4' && $wilayah_kel != '') { // kel
				$wilayah_selected = $wilayah_kel;
			}
			//
			$list_wilayah = $this->m_wilayah->list_wilayah_by_parent($wilayah_parent);
			//
			$html = '';
			$html .= '<select name="' . $wilayah_params['input_nm'] . '" id="' . $wilayah_params['input_nm'] . '" class="chosen-select custom-select w-100" required="">';
			$html .= '<option value="">- ' . $wilayah_params['level_nm'] . ' -</option>';
			foreach ($list_wilayah as $w) {
				if ($wilayah_selected == $w['wilayah_id']) {
					$html .= '<option value="' . $w['wilayah_id'] . '#' . $w['wilayah_nm'] . '" selected>' . $w['wilayah_id'] . ' - ' . $w['wilayah_nm'] . '</option>';
				} else {
					$html .= '<option value="' . $w['wilayah_id'] . '#' . $w['wilayah_nm'] . '">' . $w['wilayah_id'] . ' - ' . $w['wilayah_nm'] . '</option>';
				}
			}
			$html .= '</select>';
			$html .= '<script>
					  $(function() {
					  ';
			// autoload
			if ($wilayah_selected != '') {
				$html .= 	'_get_wilayah("' . @$wilayah_selected . '","' . @$wilayah_prop . '", "' . @$wilayah_kab . '", "' . @$wilayah_kec . '", "' . @$wilayah_kel . '"); ';
			}
			// 
			$html .= '	$("#' . $wilayah_params['input_nm'] . '").bind("change",function(e) {
					  		e.preventDefault();
					  		var i = $(this).val().split("#");
	    					_get_wilayah(i[0]);
					  	});
					  	//
					  	function _get_wilayah(i, prop="", kab="", kec="", kel="") {
					  		var ext_var = "wilayah_prop="+prop;
				                ext_var+= "&wilayah_kab="+kab;
				                ext_var+= "&wilayah_kec="+kec;
				                ext_var+= "&wilayah_kel="+kel;

					  		$.get("' . site_url('master/identitas/ajax/get_wilayah_child') . '?wilayah_parent="+i+"&"+ext_var,null,function(data) {
					  			$("#box_' . $wilayah_params['input_nm_next'] . '").html(data.html);
					  		},"json");
					  	}
					  });
					  </script>
					 ';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}
	}
}
