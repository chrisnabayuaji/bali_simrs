<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kamar extends MY_Controller
{

	var $nav_id = '02.01.08', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'm_kamar',
			'm_lokasi',
			'm_kelas',
			'm_kelas_lokasi',
			'bridging/m_konfigurasi'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id);
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '', 'lokasi_id' => '', 'kelas_id' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'kamar_id', 'type' => 'asc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(4, 0);
		$this->cookie['total_rows'] = $this->m_kamar->all_rows($this->cookie);
		set_cookie_nav($this->nav_id, $this->cookie);
		//main data
		$data['nav'] = $this->nav;
		$data['cookie'] = $this->cookie;
		$data['main'] = $this->m_kamar->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);

		$data['list_lokasi'] = $this->m_lokasi->all_data(array('is_bed' => '1'));
		$data['list_kelas'] = $this->m_kelas->all_data();
		//set pagination
		set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('master/kamar/index', $data);
	}

	public function form_modal($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_kamar->get_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save/' . $id;
		$data['list_lokasi'] = $this->m_lokasi->all_data(array('is_bed' => '1'));

		echo json_encode(array(
			'html' => $this->load->view('master/kamar/form_modal', $data, true)
		));
	}

	public function save($id = null)
	{
		$this->m_kamar->save($id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($id != '') ? '_update' : '_add', $this->nav_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	public function delete($id = null)
	{
		$this->m_kamar->delete($id, true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log('_delete', $this->nav_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	public function multiple($type = null)
	{
		$data = $this->input->post();
		if (isset($data['checkitem'])) {
			foreach ($data['checkitem'] as $key) {
				switch ($type) {
					case 'delete':
						$this->authorize($this->nav, '_delete');
						$this->m_kamar->delete($key, true);
						$flash = 'Data berhasil dihapus.';
						create_log('_delete', $this->nav_id);
						break;

					case 'enable':
						$this->authorize($this->nav, '_update');
						$this->m_kamar->update($key, array('is_active' => 1));
						$flash = 'Data berhasil diaktifkan.';
						create_log('_update', $this->nav_id);
						break;

					case 'disable':
						$this->authorize($this->nav, '_update');
						$this->m_kamar->update($key, array('is_active' => 0));
						$flash = 'Data berhasil dinonaktifkan.';
						create_log('_delete', $this->nav_id);
						break;
				}
			}
		}
		$this->session->set_flashdata('flash_success', $flash);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	public function ajax($type = null, $id = null)
	{
		if ($type == 'get_kamar_id') {
			$lokasi_id = $this->input->get('lokasi_id');
			$kelas_id = $this->input->get('kelas_id');
			$result = $this->m_kamar->get_kamar_id($lokasi_id, $kelas_id);
			echo json_encode(array(
				'result' => $result
			));
		}

		if ($type == 'cek_id') {
			$data = $this->input->post();
			$cek = $this->m_kamar->get_data($data['kamar_id']);
			if ($id == null) {
				if ($cek == null) {
					echo 'true';
				} else {
					echo 'false';
				}
			} else {
				if ($id != $data['kamar_id'] && $cek != null) {
					echo 'false';
				} else {
					echo 'true';
				}
			}
		}

		if ($type == 'get_kelas') {
			$lokasi_id = $this->input->get('lokasi_id');
			$kelas_id = $this->input->get('kelas_id');
			$tarif_id = $this->input->get('tarif_id');
			$list_kelas = $this->m_kelas_lokasi->all_data($lokasi_id);
			//
			$html = '';
			$html .= '<select name="kelas_id" id="kelas_id" class="chosen-select custom-select w-100" required="">';
			$html .= '<option value="">- Pilih -</option>';
			foreach ($list_kelas as $kelas) {
				if (@$kelas_id == $kelas['kelas_id']) {
					$html .= '<option value="' . $kelas['kelas_id'] . '" selected>' . $kelas['kelas_nm'] . '</option>';
				} else {
					$html .= '<option value="' . $kelas['kelas_id'] . '">' . $kelas['kelas_nm'] . '</option>';
				}
			}
			$html .= '</select>';
			$html .= '<script>
					  			$(function() {';
			if (@$tarif_id != '') {
				$html .= '			_get_tarif("' . @$kelas_id . '", "' . $tarif_id . '");';
			}
			$html .= '		$("#kelas_id").bind("change",function(e) {
										  e.preventDefault();
										  var lokasi_id = $("#lokasi_id").val();
										  var kelas_id = $("#kelas_id").val();

										  $.get("' . site_url("master/kamar/ajax/get_kamar_id") . '?lokasi_id="+lokasi_id+"&kelas_id="+kelas_id,null,function(data) {
										      $("#kamar_id").val(data.result);
										  },"json");

										  _get_tarif(kelas_id);
										});

										function _get_tarif(kelas_id, tarif_id) {
										  $.get("' . site_url("master/kamar/ajax/get_tarif") . '?kelas_id="+kelas_id+"&tarif_id="+tarif_id,null,function(data) {
										      $("#box_tarif").html(data.html);
										  },"json");
									  }';
			$html .= ' 	});
					  		</script>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}

		if ($type == 'get_tarif') {
			$kelas_id = $this->input->get('kelas_id');
			$tarif_id = $this->input->get('tarif_id');
			$list_tarif = $this->m_kamar->list_tarif_by_kelas_id($kelas_id);
			//
			$html = '';
			$html .= '<select name="tarif_id" id="tarif_id" class="chosen-select custom-select w-100" required="">';
			$html .= '<option value="">- Pilih -</option>';
			foreach ($list_tarif as $tarif) {
				if (@$tarif_id == $tarif['tarif_id']) {
					$html .= '<option value="' . $tarif['tarif_id'] . '" selected>' . $tarif['tarif_id'] . ' - ' . $tarif['tarif_nm'] . '</option>';
				} else {
					$html .= '<option value="' . $tarif['tarif_id'] . '">' . $tarif['tarif_id'] . ' - ' . $tarif['tarif_nm'] . '</option>';
				}
			}
			$html .= '</select>';
			$html .= '<script>
					  			$(function() {';
			$html .= '		$("#tarif_id").bind("change",function(e) {
										  e.preventDefault();
										  var kelas_id = $("#kelas_id").val();
										  var tarif_id = $("#tarif_id").val();
										  $.get("' . site_url("master/kamar/ajax/get_desc_tarif") . '?kelas_id="+kelas_id+"&tarif_id="+tarif_id,null,function(data) {
										      $("#tarif_nm").val(data.result.tarif_nm);
										      $("#nom_tarif").val(data.result.nominal);
										  },"json");
										});';
			$html .= ' 	});
					  		</script>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}

		if ($type == 'get_desc_tarif') {
			$kelas_id = $this->input->get('kelas_id');
			$tarif_id = $this->input->get('tarif_id');
			$result = $this->m_kamar->get_desc_tarif($kelas_id, $tarif_id);
			echo json_encode(array(
				'result' => $result
			));
		}
	}

	public function hitung_kamar()
	{
		//reset kamar
		$this->db->query("UPDATE mst_kamar SET jml_bed_terpakai = 0, jml_bed_kosong = jml_bed;");
		//cari pasien yang masih ranap
		$reg = $this->db->query("SELECT * FROM reg_pasien WHERE is_ranap = 1 AND pulang_st = 0 AND is_billing = 1")->result_array();
		foreach ($reg as $r) {
			$kamar = $this->db->where('kamar_id', $r['kamar_id'])->get('mst_kamar')->row_array();
			if ($kamar != null) {
				$dkamar = array(
					'jml_bed_terpakai' => $kamar['jml_bed_terpakai'] + 1,
					'jml_bed_kosong' => $kamar['jml_bed_kosong'] - 1,
					'updated_at' => date('Y-m-d H:i:s'),
					'updated_by' => @$this->session->userdata('sess_user_realname'),
				);
				$this->db->where('kamar_id', $r['kamar_id'])->update('mst_kamar', $dkamar);
				echo 'Kamar ' . $kamar['kamar_nm'] . ' updated @ ' . $dkamar['updated_at'] . '</br>';
			}
		}

		//update aplicare
		// string(160) "{"koderuang":"0009","kodekelas":"KL3","namaruang":"MUTIARA II","kapasitas":"10","tersedia":"6","tersediapria":"0","tersediawanita":"0","tersediapriawanita":"0"}"
		$kamar_all = $this->db->query(
			"SELECT 
				a.*, b.kelas_nm, b.kelas_singkatan, c.lokasi_nm 
			FROM mst_kamar a 
			JOIN mst_kelas b ON a.kelas_id = b.kelas_id 
			JOIN mst_lokasi c ON a.lokasi_id = c.lokasi_id
			WHERE bpjs_st = 1 AND bpjs_kd IS NOT NULL AND bpjs_kd != ''"
		)->result_array();

		$conf = $this->m_konfigurasi->get_first();

		foreach ($kamar_all as $r) {
			$data_arr = array(
				"koderuang" => $r['bpjs_kd'],
				"kodekelas" => $r['kelas_singkatan'],
				"namaruang" => $r['lokasi_nm'],
				"kapasitas" => $r['jml_bed'],
				"tersedia" => $r['jml_bed_kosong'],
				"tersediapria" => 0,
				"tersediawanita" => 0,
			);
			$data_str = json_encode($data_arr);

			$response = bpjs_service('aplicare', 'POST', "rest/bed/update/" . $conf['kode_ppk'], $data_str);
			var_dump($data_arr['koderuang'] . ' ' . $response . '</br>');
		}
	}
}
