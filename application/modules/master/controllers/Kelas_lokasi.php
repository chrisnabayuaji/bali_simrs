<?php 
if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Kelas_lokasi extends MY_Controller{

	var $nav_id = '02.01.07', $nav, $cookie;
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array(
			'm_kelas_lokasi',
			'm_lokasi',
			'm_kelas'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
    $this->cookie = get_cookie_nav($this->nav_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'lokasi_id','type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}
	
	public function index() {	
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(4, 0);
    $this->cookie['total_rows'] = $this->m_kelas_lokasi->all_rows($this->cookie);
    set_cookie_nav($this->nav_id, $this->cookie);
    //main data
    $data['nav'] = $this->nav;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_kelas_lokasi->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
    //set pagination
    set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('master/kelas_lokasi/index',$data);
	}

	public function form_modal($lokasi_id=null, $kelas_id=null) {
    $this->authorize($this->nav, ($lokasi_id != '' && $kelas_id !='' ) ? '_update' : '_add');
	
		if($lokasi_id == null && $kelas_id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_kelas_lokasi->get_data($lokasi_id, $kelas_id);
		}
		$data['lokasi_id'] = $lokasi_id;
		$data['kelas_id'] = $kelas_id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/save/'.$lokasi_id.'/'.$kelas_id;
		$data['list_lokasi'] = $this->m_lokasi->all_data();
		$data['list_kelas'] = $this->m_kelas->all_data();
			
		echo json_encode(array(
			'html' => $this->load->view('master/kelas_lokasi/form_modal', $data, true)
		));
	}
	
	public function save($lokasi_id = null, $kelas_id = null)
	{
		$this->m_kelas_lokasi->save($lokasi_id, $kelas_id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($lokasi_id != '' && $kelas_id !='' ) ? '_update' : '_add', $this->nav_id);
		redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	public function delete($lokasi_id = null, $kelas_id = null)
	{
		$this->m_kelas_lokasi->delete($lokasi_id, $kelas_id, true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log('_delete', $this->nav_id);
		redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	public function multiple($type = null)
	{
		$data = $this->input->post();
    if(isset($data['checkitem'])){
      foreach ($data['checkitem'] as $key) {
        switch ($type) {					
					case 'delete':
						$this->authorize($this->nav, '_delete');
            $this->m_kelas_lokasi->delete($key, true);
						$flash = 'Data berhasil dihapus.';
						create_log('_delete', $this->nav_id);
            break;

					case 'enable':
						$this->authorize($this->nav, '_update');
            $this->m_kelas_lokasi->update($key, array('is_active' => 1));
						$flash = 'Data berhasil diaktifkan.';
						create_log('_update', $this->nav_id);
            break;

					case 'disable':
						$this->authorize($this->nav, '_update');
            $this->m_kelas_lokasi->update($key, array('is_active' => 0));
						$flash = 'Data berhasil dinonaktifkan.';
						create_log('_delete', $this->nav_id);
            break;
        }
      }
    }
    create_log($t,$this->this->menu['menu']);
    $this->session->set_flashdata('flash_success', $flash);
    redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}
	
}