<?php 
if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Pegawai extends MY_Controller{

	var $nav_id = '02.02.03', $nav, $cookie;
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array(
			'm_pegawai',
			'app/m_parameter',
			'm_wilayah'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
    $this->cookie = get_cookie_nav($this->nav_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '', 'parameter_cd' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'pegawai_id','type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}
	
	public function index() {	
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(4, 0);
    $this->cookie['total_rows'] = $this->m_pegawai->all_rows($this->cookie);
    set_cookie_nav($this->nav_id, $this->cookie);
    //main data
    $data['nav'] = $this->nav;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_pegawai->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);

		$data['list_jenispegawai'] = $this->m_parameter->list_parameter_by_field('jenispegawai_cd');
    //set pagination
    set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('master/pegawai/index',$data);
	}

	public function form_modal($id=null) {
    $this->authorize($this->nav, ($id != '' ) ? '_update' : '_add');
		
		if($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_pegawai->get_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/save/'.$id;
		$data['list_spesialis'] = $this->m_parameter->list_parameter_by_field('spesialisasi_cd');
		$data['list_jenispegawai'] = $this->m_parameter->list_parameter_by_field('jenispegawai_cd');
		$data['list_sex'] = $this->m_parameter->list_parameter_by_field('sex_cd');
		$data['list_agama'] = $this->m_parameter->list_parameter_by_field('agama_cd');
		$data['list_pendidikan'] = $this->m_parameter->list_parameter_by_field('pendidikan_cd');
		$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_parent('');
			
		echo json_encode(array(
			'html' => $this->load->view('master/pegawai/form_modal', $data, true)
		));
	}
	
	public function save($id = null)
	{
		$this->m_pegawai->save($id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($id != '' ) ? '_update' : '_add', $this->nav_id);
		redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	public function delete($id = null)
	{
		$this->m_pegawai->delete($id, true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log('_delete', $this->nav_id);
		redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	public function multiple($type = null)
	{
		$data = $this->input->post();
    if(isset($data['checkitem'])){
      foreach ($data['checkitem'] as $key) {
        switch ($type) {					
					case 'delete':
						$this->authorize($this->nav, '_delete');
            $this->m_pegawai->delete($key, true);
						$flash = 'Data berhasil dihapus.';
						create_log('_delete', $this->nav_id);
            break;

					case 'enable':
						$this->authorize($this->nav, '_update');
            $this->m_pegawai->update($key, array('is_active' => 1));
						$flash = 'Data berhasil diaktifkan.';
						create_log('_update', $this->nav_id);
            break;

					case 'disable':
						$this->authorize($this->nav, '_update');
            $this->m_pegawai->update($key, array('is_active' => 0));
						$flash = 'Data berhasil dinonaktifkan.';
						create_log('_delete', $this->nav_id);
            break;
        }
      }
    }
    create_log($t,$this->this->menu['menu']);
    $this->session->set_flashdata('flash_success', $flash);
    redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	function ajax($type=null, $id=null) {
		if($type == 'get_pegawai_id') {
			$jenispegawai_cd = $this->input->get('jenispegawai_cd');
			$result = $this->m_pegawai->get_pegawai_id($jenispegawai_cd);
			echo json_encode(array(
				'result' => $result
			));
		}elseif ($type = 'cek_id') {
			$data = $this->input->post();
			$cek = $this->m_pegawai->get_data($data['pegawai_id']);
			if ($id == null) {
				if ($cek == null) {
					echo 'true';
				}else{
					echo 'false';
				}
			}else{
				if ($id != $data['pegawai_id'] && $cek != null) {
					echo 'false';
				}else{
					echo 'true';
				}
			}
		}
	}
	
}