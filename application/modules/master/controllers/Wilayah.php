<?php 
if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Wilayah extends MY_Controller{

	var $nav_id = '02.01.13', $nav, $cookie;
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array(
			'm_wilayah'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
    $this->cookie = get_cookie_nav($this->nav_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'wilayah_id','type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}
	
	public function index($id = 0, $page = 0) {	
		$this->authorize($this->nav, '_view');
		//get params
		$data['params'] = $this->m_wilayah->get_params($id);
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(5, 0);
    $this->cookie['total_rows'] = $this->m_wilayah->all_rows($this->cookie, $data['params']);
    set_cookie_nav($this->nav_id, $this->cookie);
    //main data
    $data['nav'] = $this->nav;
    $data['cookie'] = $this->cookie;
		$data['main'] = $this->m_wilayah->list_data($this->cookie, $data['params']);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
    //set pagination
    set_pagination($this->nav, $this->cookie, $id);
		//render
		create_log('_view', $this->nav_id);
		$this->render('master/wilayah/index',$data);
	}

	public function form_modal($wilayah_parent=null, $id=null) {
    $this->authorize($this->nav, ($id != '' ) ? '_update' : '_add');
		
		if($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_wilayah->get_data($id);
		}
		$data['id'] = $id;
		$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/save/'.$id;
		$data['params'] = $this->m_wilayah->get_params($wilayah_parent);
		$data['list_wilayah_parent'] = $this->m_wilayah->list_wilayah_by_parent($data['params']['wilayah_back']);
			
		echo json_encode(array(
			'html' => $this->load->view('master/wilayah/form_modal', $data, true)
		));
	}
	
	public function save($id = null)
	{
		$this->m_wilayah->save($id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($id != '' ) ? '_update' : '_add', $this->nav_id);
		redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	public function delete($id = null)
	{
		$this->m_wilayah->delete($id, true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log('_delete', $this->nav_id);
		redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	public function multiple($type = null)
	{
		$data = $this->input->post();
    if(isset($data['checkitem'])){
      foreach ($data['checkitem'] as $key) {
        switch ($type) {					
					case 'delete':
						$this->authorize($this->nav, '_delete');
            $this->m_wilayah->delete($key, true);
						$flash = 'Data berhasil dihapus.';
						create_log('_delete', $this->nav_id);
            break;

					case 'enable':
						$this->authorize($this->nav, '_update');
            $this->m_wilayah->update($key, array('is_active' => 1));
						$flash = 'Data berhasil diaktifkan.';
						create_log('_update', $this->nav_id);
            break;

					case 'disable':
						$this->authorize($this->nav, '_update');
            $this->m_wilayah->update($key, array('is_active' => 0));
						$flash = 'Data berhasil dinonaktifkan.';
						create_log('_delete', $this->nav_id);
            break;
        }
      }
    }
    create_log($t,$this->this->menu['menu']);
    $this->session->set_flashdata('flash_success', $flash);
    redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	function ajax($id=null) {
		if($id == 'get_wilayah_child') {
			$wilayah_parent = $this->input->get('wilayah_parent');
			$wilayah_prop = $this->input->get('wilayah_prop');
			$wilayah_kab = $this->input->get('wilayah_kab');
			$wilayah_kec = $this->input->get('wilayah_kec');
			$wilayah_kel = $this->input->get('wilayah_kel');
			//
			$wilayah_params = $this->m_wilayah->get_params($wilayah_parent);
			//
			$wilayah_selected = '';
			if($wilayah_params['level'] == '2' && $wilayah_kab != '') { // kab
				$wilayah_selected = $wilayah_kab;
			} else if($wilayah_params['level'] == '3' && $wilayah_kec != '') { // kec
				$wilayah_selected = $wilayah_kec;
			} else if($wilayah_params['level'] == '4' && $wilayah_kel != '') { // kel
				$wilayah_selected = $wilayah_kel;
			}
			//
			$list_wilayah = $this->m_wilayah->list_wilayah_by_parent($wilayah_parent);
			//
			$html = '';
			$html .= '<select name="'.$wilayah_params['input_nm'].'" id="'.$wilayah_params['input_nm'].'" class="chosen-select custom-select w-100">';
			$html .= '<option value="">- '.$wilayah_params['level_nm'].' -</option>';
			foreach($list_wilayah as $w) {
				if($wilayah_selected == $w['wilayah_id']) {
					$html .= '<option value="'.$w['wilayah_id'].'" selected>'.$w['wilayah_id'].' - '.$w['wilayah_nm'].'</option>';
				} else {
					$html .= '<option value="'.$w['wilayah_id'].'">'.$w['wilayah_id'].' - '.$w['wilayah_nm'].'</option>';
				}				
			}
			$html .= '</select>';
			$html .= '<script>
					  $(function() {
					  ';
					  // autoload
					  if($wilayah_selected != '') {
			$html .= 	'_get_wilayah("'.@$wilayah_selected.'","'.@$wilayah_prop.'", "'.@$wilayah_kab.'", "'.@$wilayah_kec.'", "'.@$wilayah_kel.'"); ';					  	
					  } 
					  // 
			$html .= '	$("#'.$wilayah_params['input_nm'].'").bind("change",function(e) {
					  		e.preventDefault();
					  		var i = $(this).val();
					  		_get_wilayah(i);
					  	});
					  	//
					  	function _get_wilayah(i, prop="", kab="", kec="", kel="") {
					  		var ext_var = "wilayah_prop="+prop;
				                ext_var+= "&wilayah_kab="+kab;
				                ext_var+= "&wilayah_kec="+kec;
				                ext_var+= "&wilayah_kel="+kel;

					  		$.get("'.site_url('master/wilayah/ajax/get_wilayah_child').'?wilayah_parent="+i+"&"+ext_var,null,function(data) {
					  			$("#box_'.$wilayah_params['input_nm_next'].'").html(data.html);
					  		},"json");
					  	}
					  });
					  </script>
					 ';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}else if($id == 'get_kantor_wilayah_child') {
			$kantor_wilayah_parent = $this->input->get('kantor_wilayah_parent');
			$kantor_wilayah_prop = $this->input->get('kantor_wilayah_prop');
			$kantor_wilayah_kab = $this->input->get('kantor_wilayah_kab');
			$kantor_wilayah_kec = $this->input->get('kantor_wilayah_kec');
			$kantor_wilayah_kel = $this->input->get('kantor_wilayah_kel');
			//
			$wilayah_params = $this->m_wilayah->get_params($kantor_wilayah_parent);
			//
			$wilayah_selected = '';
			if($wilayah_params['level'] == '2' && $kantor_wilayah_kab != '') { // kab
				$wilayah_selected = $kantor_wilayah_kab;
			} else if($wilayah_params['level'] == '3' && $kantor_wilayah_kec != '') { // kec
				$wilayah_selected = $kantor_wilayah_kec;
			} else if($wilayah_params['level'] == '4' && $kantor_wilayah_kel != '') { // kel
				$wilayah_selected = $kantor_wilayah_kel;
			}
			//
			$list_wilayah = $this->m_wilayah->list_wilayah_by_parent($kantor_wilayah_parent);
			//
			$html = '';
			$html .= '<select name="kantor_'.$wilayah_params['input_nm'].'" id="kantor_'.$wilayah_params['input_nm'].'" class="chosen-select custom-select w-100">';
			$html .= '<option value="">- '.$wilayah_params['level_nm'].' -</option>';
			foreach($list_wilayah as $w) {
				if($wilayah_selected == $w['wilayah_id']) {
					$html .= '<option value="'.$w['wilayah_id'].'" selected>'.$w['wilayah_id'].' - '.$w['wilayah_nm'].'</option>';
				} else {
					$html .= '<option value="'.$w['wilayah_id'].'">'.$w['wilayah_id'].' - '.$w['wilayah_nm'].'</option>';
				}				
			}
			$html .= '</select>';
			$html .= '<div class="invalid-tooltip tooltip-select"><i class="mdi mdi-alert-circle-outline"></i> Kecamatan belum diisi</div>';
			$html .= '<script>
					  $(function() {
					  ';
					  // autoload
					  if($wilayah_selected != '') {
			$html .= 	'_get_kantor_wilayah("'.@$wilayah_selected.'","'.@$kantor_wilayah_prop.'", "'.@$kantor_wilayah_kab.'", "'.@$kantor_wilayah_kec.'", "'.@$kantor_wilayah_kel.'"); ';					  	
					  } 
					  // 
			$html .= '	$("#kantor_'.$wilayah_params['input_nm'].'").bind("change",function(e) {
					  		e.preventDefault();
					  		var i = $(this).val();
					  		_get_kantor_wilayah(i);
					  	});
					  	//
					  	function _get_kantor_wilayah(i, prop="", kab="", kec="", kel="") {
					  		var ext_var = "kantor_wilayah_prop="+prop;
				                ext_var+= "&kantor_wilayah_kab="+kab;
				                ext_var+= "&kantor_wilayah_kec="+kec;
				                ext_var+= "&kantor_wilayah_kel="+kel;

					  		$.get("'.site_url('master/wilayah/ajax/get_kantor_wilayah_child').'?kantor_wilayah_parent="+i+"&"+ext_var,null,function(data) {
					  			$("#box_kantor_'.$wilayah_params['input_nm_next'].'").html(data.html);
					  		},"json");
					  	}
					  });
					  </script>
					 ';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}else if($id == 'get_wilayah_id_name') {
			$wilayah_parent = $this->input->get('wilayah_parent');
			$wilayah_prop = $this->input->get('wilayah_prop');
			$wilayah_kab = $this->input->get('wilayah_kab');
			$wilayah_kec = $this->input->get('wilayah_kec');
			$wilayah_kel = $this->input->get('wilayah_kel');
			//
			$wilayah_params = $this->m_wilayah->get_params($wilayah_parent);
			//
			$wilayah_selected = '';
			if($wilayah_params['level'] == '2' && $wilayah_kab != '') { // kab
				$wilayah_selected = $wilayah_kab;
			} else if($wilayah_params['level'] == '3' && $wilayah_kec != '') { // kec
				$wilayah_selected = $wilayah_kec;
			} else if($wilayah_params['level'] == '4' && $wilayah_kel != '') { // kel
				$wilayah_selected = $wilayah_kel;
			}
			//
			$list_wilayah = $this->m_wilayah->list_wilayah_by_parent($wilayah_parent);
			//
			$html = '';
			if ($wilayah_parent == '' && $wilayah_prop == '' && $wilayah_kab == '' && $wilayah_kec == '' && $wilayah_kel == '') {
				$html .= '<select class="chosen-select custom-select w-100" name="wilayah_kab" id="wilayah_kab">
                    <option value="">- Kab/Kota -</option>
                  </select>';
			}else{
				$html .= '<select name="'.$wilayah_params['input_nm'].'" id="'.$wilayah_params['input_nm'].'" class="chosen-select custom-select w-100">';
				$html .= '<option value="">- '.$wilayah_params['level_nm'].' -</option>';
				foreach($list_wilayah as $w) {
					if($wilayah_selected == $w['wilayah_id']) {
						$html .= '<option value="'.$w['wilayah_id'].'#'.$w['wilayah_nm'].'" selected>'.$w['wilayah_id'].' - '.$w['wilayah_nm'].'</option>';
					} else {
						$html .= '<option value="'.$w['wilayah_id'].'#'.$w['wilayah_nm'].'">'.$w['wilayah_id'].' - '.$w['wilayah_nm'].'</option>';
					}				
				}
				$html .= '</select>';
				$html .= '<script>
						  $(function() {
						  ';
						  // autoload
						  if($wilayah_selected != '') {
				$html .= 	'_get_wilayah("'.@$wilayah_selected.'","'.@$wilayah_prop.'", "'.@$wilayah_kab.'", "'.@$wilayah_kec.'", "'.@$wilayah_kel.'"); ';					  	
						  } 
						  // 
				$html .= '	$("#'.$wilayah_params['input_nm'].'").bind("change",function(e) {
						  		e.preventDefault();
						  		var i = $(this).val().split("#");
		    					_get_wilayah(i[0]);
						  	});
						  	//
						  	function _get_wilayah(i, prop="", kab="", kec="", kel="") {
						  		var ext_var = "wilayah_prop="+prop;
					                ext_var+= "&wilayah_kab="+kab;
					                ext_var+= "&wilayah_kec="+kec;
					                ext_var+= "&wilayah_kel="+kel;

						  		$.get("'.site_url('master/wilayah/ajax/get_wilayah_id_name').'?wilayah_parent="+i+"&"+ext_var,null,function(data) {
						  			$("#box_'.$wilayah_params['input_nm_next'].'").html(data.html);
						  		},"json");
						  	}
						  });
						  </script>
						 ';
			}
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}else if($id == 'get_provinsi_by_st') {
			$wilayah_st = $this->input->get('wilayah_st');
			if ($wilayah_st == 'D') {
				$list_wilayah_prop = $this->m_wilayah->list_wilayah_by_profile();
			}else{
				$list_wilayah_prop = $this->m_wilayah->list_wilayah_by_parent('');
			}
			//
			$html = '';
			$html .= '<select name="wilayah_prop" id="wilayah_prop" class="chosen-select custom-select w-100">';
			$html .= '<option value="">- Propinsi -</option>';
			foreach($list_wilayah_prop as $w) {
				$html .= '<option value="'.$w['wilayah_id'].'#'.$w['wilayah_nm'].'">'.$w['wilayah_id'].' - '.$w['wilayah_nm'].'</option>';			
			}
			$html .= '</select>';
			$html .= '<script>
					  $(function() {
					  '; 
					  // 
			$html .= ' $("#wilayah_prop").bind("change",function(e) {
				      e.preventDefault();
				      var i = $(this).val().split("#");
				      _get_wilayah(i[0]);
				    })
				    //
				    function _get_wilayah(i, prop="", kab="", kec="", kel="") {
				      var ext_var = "wilayah_prop="+prop;
				          ext_var+= "&wilayah_kab="+kab;
				          ext_var+= "&wilayah_kec="+kec;
				          ext_var+= "&wilayah_kel="+kel;
				      $.get("'.site_url("master/wilayah/ajax/get_wilayah_id_name").'?wilayah_parent="+i+"&"+ext_var,null,function(data) {
				          $("#box_wilayah_kab").html(data.html);
				      },"json");
				    }
					  });
					  </script>
					 ';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}else if($id == 'empty_kabupaten') {
			$html = '';
			$html .= '<select name="wilayah_kab" id="wilayah_kab" class="chosen-select custom-select w-100">';
			$html .= '<option value="">- Kab/Kota -</option>';
			$html .= '</select>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}else if($id == 'empty_kecamatan') {
			$html = '';
			$html .= '<select name="wilayah_kec" id="wilayah_kec" class="chosen-select custom-select w-100">';
			$html .= '<option value="">- Kecamatan -</option>';
			$html .= '</select>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}else if($id == 'empty_kelurahan') {
			$html = '';
			$html .= '<select name="wilayah_kel" id="wilayah_kel" class="chosen-select custom-select w-100">';
			$html .= '<option value="">- Desa/Kelurahan -</option>';
			$html .= '</select>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}
	}
	
}