<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Itemoperasi extends MY_Controller
{

  var $nav_id = '02.03.03', $nav, $cookie;

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'm_itemoperasi',
      'm_tarif'
    ));

    $this->nav = $this->m_app->_get_nav($this->nav_id);

    //cookie
    $this->cookie = get_cookie_nav($this->nav_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'itemoperasi_id', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
  }

  public function index()
  {
    $this->authorize($this->nav, '_view');
    //cookie
    $this->cookie['cur_page'] = $this->uri->segment(4, 0);
    $this->cookie['total_rows'] = $this->m_itemoperasi->all_rows($this->cookie);
    set_cookie_nav($this->nav_id, $this->cookie);
    //main data
    $data['nav'] = $this->nav;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_itemoperasi->list_data($this->cookie);
    $data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
    //set pagination
    set_pagination($this->nav, $this->cookie);
    //render
    create_log('_view', $this->nav_id);
    $this->render('master/itemoperasi/index', $data);
  }

  public function form_modal($id = null)
  {
    $this->authorize($this->nav, ($id != '') ? '_update' : '_add');

    if ($id == null) {
      $data['main'] = array();
    } else {
      $data['main'] = $this->m_itemoperasi->get_data($id);
    }
    $data['id'] = $id;
    $data['nav'] = $this->nav;
    $data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save/' . $id;
    $data['list_itemoperasi'] = $this->m_itemoperasi->all_data();
    $data['list_tarif'] = $this->m_tarif->all_data();

    echo json_encode(array(
      'html' => $this->load->view('master/itemoperasi/form_modal', $data, true)
    ));
  }

  public function save($id = null)
  {
    $this->m_itemoperasi->save($id);
    $this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
    create_log(($id != '') ? '_update' : '_add', $this->nav_id);
    redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
  }

  public function delete($id = null)
  {
    $this->m_itemoperasi->delete($id, true);
    $this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
    create_log('_delete', $this->nav_id);
    redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
  }

  public function multiple($type = null)
  {
    $data = $this->input->post();
    if (isset($data['checkitem'])) {
      foreach ($data['checkitem'] as $key) {
        switch ($type) {
          case 'delete':
            $this->authorize($this->nav, '_delete');
            $this->m_itemoperasi->delete($key, true);
            $flash = 'Data berhasil dihapus.';
            create_log('_delete', $this->nav_id);
            break;

          case 'enable':
            $this->authorize($this->nav, '_update');
            $this->m_itemoperasi->update($key, array('is_active' => 1));
            $flash = 'Data berhasil diaktifkan.';
            create_log('_update', $this->nav_id);
            break;

          case 'disable':
            $this->authorize($this->nav, '_update');
            $this->m_itemoperasi->update($key, array('is_active' => 0));
            $flash = 'Data berhasil dinonaktifkan.';
            create_log('_delete', $this->nav_id);
            break;
        }
      }
    }
    create_log($t, $this->this->menu['menu']);
    $this->session->set_flashdata('flash_success', $flash);
    redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
  }

  function ajax($type = null, $id = null)
  {
    if ($type == 'get_itemoperasi_id') {
      $parent_id = $this->input->get('parent_id');
      $result = $this->m_itemoperasi->get_itemoperasi_id($parent_id);
      echo json_encode(array(
        'result' => $result
      ));
    } elseif ($type = 'cek_id') {
      $data = $this->input->post();
      $cek = $this->m_itemoperasi->get_data($data['itemoperasi_id']);
      if ($id == null) {
        if ($cek == null) {
          echo 'true';
        } else {
          echo 'false';
        }
      } else {
        if ($id != $data['itemoperasi_id'] && @$cek != null) {
          echo 'false';
        } else {
          echo 'true';
        }
      }
    }
  }
}
