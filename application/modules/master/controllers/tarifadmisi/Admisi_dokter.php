<?php 
if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Admisi_dokter extends MY_Controller{

	var $nav_id = '02.01.12', $nav, $cookie;
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array(
			'm_admisi_dokter',
			'm_lokasi',
			'm_kelas',
			'm_tarifkelas',
			'm_pegawai'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
    $this->cookie = get_cookie_nav($this->nav_id.'.admisi_dokter');
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'tarifadmisi_id','type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}
	
	public function index() {	
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(5, 0);
    $this->cookie['total_rows'] = $this->m_admisi_dokter->all_rows($this->cookie);
    set_cookie_nav($this->nav_id, $this->cookie);
    //main data
    $data['nav'] = $this->nav;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_admisi_dokter->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
    //set pagination
    set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('master/tarifadmisi/admisi_dokter/index',$data);
	}

	public function form_modal($id=null) {
    $this->authorize($this->nav, ($id != '' ) ? '_update' : '_add');
	
		if($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_admisi_dokter->get_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['lokasi'] = $this->m_lokasi->all_data();
		$data['kelas'] = $this->m_kelas->all_data();
		$data['dokter'] = $this->m_pegawai->all_data('01');
		$data['form_action'] = site_url().'/master/tarifadmisi/admisi_dokter/save/'.$id;
			
		echo json_encode(array(
			'html' => $this->load->view('master/tarifadmisi/admisi_dokter/form_modal', $data, true)
		));
	}
	
	public function save($id = null)
	{
		$this->m_admisi_dokter->save($id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($id != '' ) ? '_update' : '_add', $this->nav_id);
		redirect(site_url().'/master/tarifadmisi/admisi_dokter/index/'.$this->cookie['cur_page']);
	}

	public function delete($id = null)
	{
		$this->m_admisi_dokter->delete($id, true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log('_delete', $this->nav_id);
		redirect(site_url().'/master/tarifadmisi/admisi_dokter/index/'.$this->cookie['cur_page']);
	}

	function ajax($type=null, $id=null) {
		if ($type == 'cek_id') {
			$data = $this->input->post();
			$cek = $this->m_admisi_dokter->get_data($data['tarifadmisi_id']);
			if ($id == null) {
				if ($cek == null) {
					echo 'true';
				}else{
					echo 'false';
				}
			}else{
				if ($id != $data['tarifadmisi_id'] && $cek != null) {
					echo 'false';
				}else{
					echo 'true';
				}
			}
		}

		if($type == 'get_tarif') {
			$kelas_id = $this->input->post('kelas_id');
			$tarif_id = $this->input->post('tarif_id');
			$list_tarif = $this->m_tarifkelas->all_data_by_kelas_id($kelas_id);
			//
			$html = '';
			$html .= '<select name="tarif_id" id="tarif_id" class="chosen-select custom-select w-100">';
			$html .= '<option value="">- Pilih -</option>';
			foreach($list_tarif as $tarif) {
				if ($tarif_id == $tarif['tarif_id']) {
					$html .= '<option value="'.$tarif['tarif_id'].'" selected>'.$tarif['tarif_id'].' - '.$tarif['tarif_nm'].'</option>';
				}else{
					$html .= '<option value="'.$tarif['tarif_id'].'">'.$tarif['tarif_id'].' - '.$tarif['tarif_nm'].'</option>';
				}
			}
			$html .= '</select>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html
			));
		}
	}
	
}