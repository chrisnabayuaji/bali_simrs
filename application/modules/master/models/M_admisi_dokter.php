<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_admisi_dokter extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 AND dokter_id IS NOT NULL ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND (b.lokasi_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' OR d.tarif_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%') ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT *, b.lokasi_nm, c.kelas_nm, d.tarif_nm, e.nominal, f.pegawai_nm  
            FROM mst_tarif_admisi a 
            LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
            LEFT JOIN mst_kelas c ON a.kelas_id = c.kelas_id
            LEFT JOIN mst_tarif d ON a.tarif_id = d.tarif_id
            LEFT JOIN mst_tarif_kelas e ON a.tarif_id = e.tarif_id AND a.kelas_id = e.kelas_id
            LEFT JOIN mst_pegawai f ON a.dokter_id = f.pegawai_id
            $where
            ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM mst_tarif_admisi a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total 
            FROM mst_tarif_admisi a 
            LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
            LEFT JOIN mst_kelas c ON a.kelas_id = c.kelas_id
            LEFT JOIN mst_tarif d ON a.tarif_id = d.tarif_id
            LEFT JOIN mst_tarif_kelas e ON a.tarif_id = e.tarif_id AND a.kelas_id = e.kelas_id
            $where ";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id)
  {
    $sql = "SELECT * FROM mst_tarif_admisi WHERE tarifadmisi_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  function get_check_data($lokasi_id = null, $kelas_id = null)
  {
    $sql = "SELECT
              MAX(tarifadmisi_id) AS tarifadmisi_id, lokasi_id, kelas_id, dokter_id, tarif_id
            FROM
              mst_tarif_admisi 
            WHERE
              lokasi_id=? AND kelas_id=?";
    $query = $this->db->query($sql, array($lokasi_id, $kelas_id));
    $row = $query->row_array();
    return $row;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    if ($id == null) {
      $get_data = $this->get_check_data($data['lokasi_id'], $data['kelas_id']);
      if ($get_data['tarifadmisi_id'] != '') {
        $exp_id = explode('.', $get_data['tarifadmisi_id']);
        $no_increment = zerofill($exp_id[2] + 1, '2');
      } else {
        $no_increment = '01';
      }

      $data['tarifadmisi_id'] = str_replace('.', '', $data['lokasi_id']) . '.' . str_replace('.', '', $data['kelas_id']) . '.' . $no_increment;
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('mst_tarif_admisi', $data);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('tarifadmisi_id', $id)->update('mst_tarif_admisi', $data);
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('tarifadmisi_id', $id)->update('mst_tarif_admisi', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('tarifadmisi_id', $id)->delete('mst_tarif_admisi');
    } else {
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('tarifadmisi_id', $id)->update('mst_tarif_admisi', $data);
    }
  }
}
