<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_berita extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND a.news_title LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT * FROM web_news a 
      $where
      ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM web_news a $where ORDER BY news_id ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM web_news a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id)
  {
    $sql = "SELECT * FROM web_news WHERE news_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    $data_news['news_title']    = $data['news_title'];
    $data_news['news_url']      = $data['news_url'];
    $data_news['news_content']  = $_POST['news_content'];
    $data_news['author_nm']     = $data['author_nm'];
    $data_news['news_st']       = $data['news_st'];
    if ($id == null) {
      $data_news['created_at'] = to_date($data['created_at'], '-', 'full_date');
      $data_news['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('web_news', $data_news);
      $news_id = $this->db->insert_id();
    } else {
      $data_news['updated_at'] = to_date($data['created_at'], '-', 'full_date');
      $data_news['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('news_id', $id)->update('web_news', $data_news);
      $news_id = $id;
    }

    $this->images_save($news_id);
  }

  function images_save($news_id)
  {
    $image_no = $this->input->post('image_no');
    $path_dir = "assets/images/news/";
    $date = date('dmy');
    //
    $result = '';
    for ($n = 1; $n <= $image_no; $n++) {
      $tmp_name = @$_FILES['image_source_' . $n]['tmp_name'];
      $newsimage_id = @$_POST['newsimage_id_' . $n];
      $description = @$_POST['image_description_' . $n];
      //
      if ($tmp_name != '') {
        if ($newsimage_id == '') {
          $image_no_max = $this->get_image_no($date);
          $image_name = $this->upload_image($path_dir, $tmp_name, @$_FILES['image_source_' . $n]['name'], $n);
        } else {
          $image = $this->get_image($newsimage_id);
          $image_no_max = $image['image_no'];
          $image_name = $this->upload_image($path_dir, $tmp_name, @$_FILES['image_source_' . $n]['name'], $n, $image['image_name']);
        }
        //
        $data['news_id'] = @$news_id;
        $data['image_path'] = $path_dir;
        $data['image_date'] = $date;
        $data['image_no'] = $image_no_max;
        $data['image_name'] = $image_name;
        $data['image_description'] = @$_POST['image_description_' . $n];
        $data['image_size'] = @$_FILES['image_source_' . $n]['size'];
        $data['image_tp'] = @$_FILES['image_source_' . $n]['type'];
        //
        if ($image_name != 'error_img') {
          if ($newsimage_id != '') {
            $this->db->where('newsimage_id', $newsimage_id);
            $result = $this->db->update('web_news_image', $data);
          } else {
            $result = $this->db->insert('web_news_image', $data);
          }
        }
      }
      // only description
      else if ($newsimage_id != '' && $description != '') {
        $data['image_description'] = $description;
        $this->db->where('newsimage_id', $newsimage_id);
        $result = $this->db->update('web_news_image', $data);
      }
    }
    return $result;
  }

  function upload_image($path_dir = null, $tmp_name = null, $fupload_name = null, $n, $old_file = null)
  {
    //
    $this->load->library('upload');
    $this->load->library('image_lib');
    //
    $name_img = create_title_img($path_dir, $tmp_name, $fupload_name, $old_file);
    $src_file_name = 'image_source_' . $n;
    //
    $config['file_name'] = $name_img;
    $config['upload_path'] = $path_dir; //path folder
    $config['allowed_types'] = 'jpg|png|jpeg|gif'; //type yang dapat diakses bisa anda sesuaikan

    $this->upload->initialize($config);
    if (!empty($fupload_name)) {
      if ($this->upload->do_upload($src_file_name)) {
        $gbr = array('upload_data' => $this->upload->data());
        // cek resolusi gambar
        $nama_gambar = $path_dir . $gbr['upload_data']['file_name'];
        $data = getimagesize($nama_gambar);
        $width = $data[0];
        $height = $data[1];
        $bagi_width = round($width / 2);
        $bagi_height = round($height / 2);
        //Compress Image
        $config['image_library'] = 'gd2';
        $config['source_image'] = $gbr['upload_data']['full_path'];
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['quality'] = '100%';
        $config['width'] = $bagi_width;
        $config['height'] = $bagi_height;
        $config['new_image'] = $path_dir . $gbr['upload_data']['file_name'];
        $this->image_lib->initialize($config);
        // $this->load->library('image_lib', $config);
        $this->image_lib->resize();
        $this->image_lib->clear();
        //
        // $gambar_1 = $gbr['upload_data']['file_name'];
        $result = $name_img;
      } else {
        $result = 'error_img';
      }
      //
      return $result;
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('news_id', $id)->update('web_news', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('news_id', $id)->delete('web_news');
    } else {
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('news_id', $id)->update('web_news', $data);
    }
    // delete images
    $arr_image = $this->get_image_by_news_id($id);
    if (count($arr_image) > 0) {
      foreach ($arr_image as $image) {
        if (@$image['image_name'] != '') {
          unlink($image['image_path'] . $image['image_name']);
        }
      }
    }
    $this->db->where('news_id', $id)->delete('web_news_image');
  }

  public function get_image($newsimage_id = null)
  {
    $sql = "SELECT * FROM web_news_image WHERE newsimage_id=?";
    $query = $this->db->query($sql, $newsimage_id);
    return $query->row_array();
  }

  public function get_image_by_news_id($news_id = null)
  {
    $sql = "SELECT * FROM web_news_image WHERE news_id=?";
    $query = $this->db->query($sql, $news_id);
    return $query->result_array();
  }

  public function validate_news_url($news_url = null, $news_st = null)
  {
    $sql = "SELECT * FROM web_news a WHERE a.news_url=?";
    if ($news_st != "") $sql .= " AND a.news_st='$news_st'";
    $query = $this->db->query($sql, $news_url);
    if ($query->num_rows() > 0) {
      return true;
    } else {
      return false;
    }
  }

  public function get_image_no($date)
  {
    $sql = "SELECT 
                MAX(a.image_no) as image_no
            FROM web_news_image a 
            WHERE a.image_date=?";
    $query = $this->db->query($sql, array($date));
    if ($query->num_rows() > 0) {
      $row = $query->row_array();
      return ($row['image_no'] + 1);
    } else {
      return '1';
    }
  }

  public function delete_image($newsimage_id = null)
  {
    $image = $this->get_image($newsimage_id);
    if (@$image['image_name'] != '') {
      unlink($image['image_path'] . $image['image_name']);
    }
    //
    $sql = "DELETE FROM web_news_image WHERE newsimage_id=?";
    $query = $this->db->query($sql, $newsimage_id);
    return $query;
  }
}
