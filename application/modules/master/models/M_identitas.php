<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_identitas extends CI_Model
{

  public function get_first()
  {
    return $this->db->get('mst_identitas')->row_array();
  }

  // Diagnosis
  public function penyakit_autocomplete($penyakit_nm = null)
  {
    $sql = "SELECT 
              *
            FROM mst_penyakit a   
            WHERE (a.penyakit_nm LIKE '%$penyakit_nm%' OR a.icdx LIKE '%$penyakit_nm%')
            ORDER BY a.icdx";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['penyakit_id'] . "#" . $row['icdx'],
        'text' => $row['icdx'] . ' - ' . $row['penyakit_nm']
      );
    }
    return $res;
  }

  public function diagnosis_row($id)
  {
    $sql = "SELECT * FROM mst_penyakit WHERE penyakit_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  public function update($type, $val)
  {
    $data = html_escape($this->input->post());
    if ($type != '' && $val != '') {
      $data[$type] = urldecode($val);
    } else {
      // upload img
      $logo_rumah_sakit = $this->upload_file_process($data, 'logo_rumah_sakit', 'logo-rs');
      if ($logo_rumah_sakit != '') {
        $data['logo_rumah_sakit'] = $logo_rumah_sakit;
      }
      $img_rumah_sakit = $this->upload_file_process($data, 'img_rumah_sakit', 'image-rs');
      if ($img_rumah_sakit != '') {
        $data['img_rumah_sakit'] = $img_rumah_sakit;
      }
      $exp_wilayah_prop = explode("#", @$data['wilayah_prop']);
      $exp_wilayah_kab = explode("#", @$data['wilayah_kab']);
      $exp_wilayah_kec = explode("#", @$data['wilayah_kec']);
      $exp_wilayah_kel = explode("#", @$data['wilayah_kel']);
      $data['propinsi'] = $exp_wilayah_prop[1];
      $data['kabupaten'] = $exp_wilayah_kab[1];
      $data['kecamatan'] = $exp_wilayah_kec[1];
      $data['kelurahan'] = $exp_wilayah_kel[1];
      $data['wilayah_prop'] = $exp_wilayah_prop[0];
      $data['wilayah_kab'] = $exp_wilayah_kab[0];
      $data['wilayah_kec'] = $exp_wilayah_kec[0];
      $data['wilayah_kel'] = $exp_wilayah_kel[0];
    }
    $this->db->update('mst_identitas', $data);
  }

  function upload_file_process($data = null, $name = null, $name_upload = null)
  {
    $result   = '';
    if (@$_FILES[$name]['tmp_name'] != '') {
      $path_dir       = "assets/images/icon/";
      $tmp_name       = @$_FILES[$name]['tmp_name'];
      $fupload_name   = @$_FILES[$name]['name'];
      //
      $identitas = $this->get_first();
      $result = $this->_upload_img($path_dir, $tmp_name, $fupload_name, $identitas[$name], $name, $name_upload);
    }
    return $result;
  }

  function _upload_img($path_dir = null, $tmp_name = null, $fupload_name = null, $old_file = null, $name = null, $name_upload = null)
  {
    //
    $this->load->library('upload');
    $this->load->library('image_lib');
    //
    $name_img = create_title_img($path_dir, $tmp_name, $fupload_name, $old_file, $name_upload);
    $src_file_name = $name;
    //
    $config['file_name'] = $name_img;
    $config['upload_path'] = $path_dir; //path folder
    $config['allowed_types'] = 'jpg|png|jpeg|gif'; //type yang dapat diakses bisa anda sesuaikan

    $this->upload->initialize($config);
    if (!empty($fupload_name)) {
      if ($this->upload->do_upload($src_file_name)) {
        $gbr = array('upload_data' => $this->upload->data());
        // cek resolusi gambar
        $nama_gambar = $path_dir . $gbr['upload_data']['file_name'];
        $data = getimagesize($nama_gambar);
        $width = $data[0];
        $height = $data[1];
        // pembagian
        // $bagi_width = $width / 3;
        // $bagi_height = $height / 3;
        $bagi_width = $width;
        $bagi_height = $height;
        // Compress Image
        $config['image_library'] = 'gd2';
        $config['source_image'] = $gbr['upload_data']['full_path'];
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = FALSE;
        $config['quality'] = '20%';
        $config['width'] = round($bagi_width);
        $config['height'] = round($bagi_height);
        $config['new_image'] = $path_dir . $gbr['upload_data']['file_name'];
        $this->image_lib->initialize($config);
        // $this->load->library('image_lib', $config);
        $this->image_lib->resize();
        $this->image_lib->clear();
        //
        // $gambar_1 = $gbr['upload_data']['file_name'];
        $result = $name_img;
      } else {
        $result = 'error_img';
      }
      //
      return $result;
    }
  }
}
