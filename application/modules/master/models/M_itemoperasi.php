<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_itemoperasi extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND a.itemoperasi_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
              a.*, b.tarif_nm  
            FROM mst_item_operasi a 
            LEFT JOIN mst_tarif b ON a.tarif_id=b.tarif_id 
            $where
            ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM mst_item_operasi a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM mst_item_operasi a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id)
  {
    $sql = "SELECT * FROM mst_item_operasi WHERE itemoperasi_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  function get_itemoperasi_id($parent_id = null)
  {
    $len = strlen($parent_id);
    $sql = "SELECT MAX(RIGHT(itemoperasi_id,4)) as itemoperasi_id FROM mst_item_operasi WHERE LEFT(itemoperasi_id,$len)=? AND LENGTH(itemoperasi_id) > $len";
    $query = $this->db->query($sql, $parent_id);
    if ($query->num_rows() > 0) {
      $row = $query->row_array();
      $itemoperasi_id = abs($row['itemoperasi_id']) + 1;
      $itemoperasi_id = zerofill($itemoperasi_id, 4);
      $result = $parent_id . '.' . $itemoperasi_id;
    } else {
      $result = '';
    }
    return $result;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    if ($id == null) {
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('mst_item_operasi', $data);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('itemoperasi_id', $id)->update('mst_item_operasi', $data);
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('itemoperasi_id', $id)->update('mst_item_operasi', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('itemoperasi_id', $id)->delete('mst_item_operasi');
    } else {
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('itemoperasi_id', $id)->update('mst_item_operasi', $data);
    }
  }
}
