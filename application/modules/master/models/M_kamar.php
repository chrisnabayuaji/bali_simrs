<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kamar extends CI_Model {

	public function where($cookie)
	{
		$where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND a.kamar_nm LIKE '%".$this->db->escape_like_str($cookie['search']['term'])."%' ";
		}
    if (@$cookie['search']['lokasi_id'] != '') {
      $where .= "AND a.lokasi_id = '".$this->db->escape_like_str($cookie['search']['lokasi_id'])."' ";
    }
    if (@$cookie['search']['kelas_id'] != '') {
      $where .= "AND a.kelas_id = '".$this->db->escape_like_str($cookie['search']['kelas_id'])."' ";
    }
		return $where;
	}

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
              a.*,
              b.lokasi_nm, 
              c.kelas_nm 
            FROM mst_kamar a 
            LEFT JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id
            LEFT JOIN mst_kelas c ON a.kelas_id=c.kelas_id
            $where
            ORDER BY "
              .$cookie['order']['field']." ".$cookie['order']['type'].
            " LIMIT ".$cookie['cur_page'].",".$cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data($lokasi_id = null)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if ($lokasi_id != null) {
      $where.= "AND a.lokasi_id = '$lokasi_id'";
    }

    $sql = "SELECT 
              a.*, b.lokasi_nm, c.kelas_nm 
            FROM mst_kamar a 
            LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
            LEFT JOIN mst_kelas c ON a.kelas_id = c.kelas_id
            $where
            ORDER BY a.kelas_id ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM mst_kamar a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id) {
    $sql = "SELECT * FROM mst_kamar WHERE kamar_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }
  
  function by_field($field, $val, $type) {
    $sql = "SELECT * FROM mst_kamar WHERE $field=?";
    $query = $this->db->query($sql, array($val));
    if ($type == 'row') {
      $row = $query->row_array();
    }else{
      $row = $query->result_array();
    }
    return $row;
  }

  function get_kamar_id($lokasi_id=null,$kelas_id=null) {
    $merge_id = $lokasi_id.'.'.$kelas_id;
    //
    $sql = "SELECT MAX(RIGHT(kamar_id,2)) as kamar_id FROM mst_kamar WHERE LEFT(kamar_id,8)=?";
    $query = $this->db->query($sql, $merge_id);
    if($query->num_rows() > 0) {
        $row = $query->row_array();
        $kamar_id = abs($row['kamar_id'])+1;
        $kamar_id = zerofill($kamar_id,2);
        $result = $merge_id.'.'.$kamar_id;
    } else {
        $result = '';
    }
    return $result;
  }

  function list_tarif_by_kelas_id($kelas_id) {
    $sql = "SELECT 
              a.*, b.tarif_nm 
            FROM mst_tarif_kelas a 
            LEFT JOIN mst_tarif b ON a.tarif_id = b.tarif_id
            WHERE a.kelas_id=?";
    $query = $this->db->query($sql, $kelas_id);
    $row = $query->result_array();
    return $row;
  }

  function get_desc_tarif($kelas_id, $tarif_id) {
    $sql = "SELECT 
              a.*, b.tarif_nm 
            FROM mst_tarif_kelas a 
            LEFT JOIN mst_tarif b ON a.tarif_id = b.tarif_id
            WHERE a.kelas_id=? AND a.tarif_id=?";
    $query = $this->db->query($sql, array($kelas_id, $tarif_id));
    $row = $query->row_array();
    return $row;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    if ($id == null) {
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('mst_kamar', $data);
    }else{
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('kamar_id', $id)->update('mst_kamar', $data);
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('kamar_id',$id)->update('mst_kamar', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('kamar_id', $id)->delete('mst_kamar');
    }else{
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('kamar_id', $id)->update('mst_kamar', $data);
    }
  }
  
}