<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_kelas extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND a.kelas_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT * FROM mst_kelas a 
      $where
      ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 AND a.is_active = 1 ";

    $sql = "SELECT * FROM mst_kelas a $where ORDER BY kelas_id ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM mst_kelas a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id)
  {
    $sql = "SELECT * FROM mst_kelas WHERE kelas_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  function list_kelas()
  {
    $sql = "SELECT * FROM mst_kelas WHERE kelas_id != '00'";
    $query = $this->db->query($sql);
    $row = $query->result_array();
    return $row;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    if ($id == null) {
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('mst_kelas', $data);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('kelas_id', $id)->update('mst_kelas', $data);
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('kelas_id', $id)->update('mst_kelas', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('kelas_id', $id)->delete('mst_kelas');
    } else {
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('kelas_id', $id)->update('mst_kelas', $data);
    }
  }
}
