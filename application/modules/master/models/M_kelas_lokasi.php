<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_kelas_lokasi extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
              a.*, b.lokasi_nm, c.kelas_nm, c.kelas_singkatan 
            FROM mst_lokasi_kelas a 
            LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
            LEFT JOIN mst_kelas c ON a.kelas_id = c.kelas_id
            $where
            ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data($lokasi_id = null)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if ($lokasi_id != null) {
      $where .= "AND a.lokasi_id = '$lokasi_id'";
    }

    $sql = "SELECT 
              a.*, b.lokasi_nm, c.kelas_nm 
            FROM mst_lokasi_kelas a 
            LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
            LEFT JOIN mst_kelas c ON a.kelas_id = c.kelas_id
            $where
            ORDER BY a.kelas_id ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM mst_lokasi_kelas a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($lokasi_id, $kelas_id)
  {
    $sql = "SELECT * FROM mst_lokasi_kelas WHERE lokasi_id=? AND kelas_id=?";
    $query = $this->db->query($sql, array($lokasi_id, $kelas_id));
    $row = $query->row_array();
    return $row;
  }

  public function save($lokasi_id = null, $kelas_id = null)
  {
    $data = html_escape($this->input->post());
    if ($lokasi_id == null && $kelas_id == null) {
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('mst_lokasi_kelas', $data);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('lokasi_id', $lokasi_id)->where('kelas_id', $kelas_id)->update('mst_lokasi_kelas', $data);
    }
  }

  public function update($lokasi_id, $kelas_id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('lokasi_id', $lokasi_id)->where('kelas_id', $kelas_id)->update('mst_lokasi_kelas', $data);
  }

  public function delete($lokasi_id, $kelas_id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('lokasi_id', $lokasi_id)->where('kelas_id', $kelas_id)->delete('mst_lokasi_kelas');
    } else {
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('lokasi_id', $lokasi_id)->where('kelas_id', $kelas_id)->update('mst_lokasi_kelas', $data);
    }
  }
}
