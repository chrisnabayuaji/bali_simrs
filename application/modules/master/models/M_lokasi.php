<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_lokasi extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND a.lokasi_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT a.*, b.lokasi_nm AS lokasi_depo, c.lokasi_nm AS lokasi_kasir
            FROM mst_lokasi a
            LEFT JOIN mst_lokasi b ON a.map_lokasi_depo = b.lokasi_id
            LEFT JOIN mst_lokasi c ON a.map_lokasi_kasir = c.lokasi_id
            $where
            ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data($data = null)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$data['is_bed'] != '') {
      $where .= " AND a.is_bed = '" . $data['is_bed'] . "'";
    }
    if (@$data['is_loket_antrian'] != '') {
      $where .= " AND a.is_loket_antrian = '" . $data['is_loket_antrian'] . "'";
    }
    if (@$data['is_depo_farmasi'] != '') {
      $where .= " AND a.is_depo_farmasi = '" . $data['is_depo_farmasi'] . "'";
    }
    if (@$data['is_kasir'] != '') {
      $where .= " AND a.is_kasir = '" . $data['is_kasir'] . "'";
    }

    $sql = "SELECT a.*, b.lokasi_nm as lokasi_depo FROM 
      mst_lokasi a
      LEFT JOIN mst_lokasi b ON a.map_lokasi_depo = b.lokasi_id
      $where ORDER BY lokasi_id";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM mst_lokasi a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id)
  {
    $sql = "SELECT * FROM mst_lokasi WHERE lokasi_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  function by_field($field, $val, $type = 'row')
  {
    $sql = "SELECT * FROM mst_lokasi WHERE $field=?";
    $query = $this->db->query($sql, array($val));
    if ($type == 'row') {
      $res = $query->row_array();
    } else if ($type == 'result') {
      $res = $query->result_array();
    }
    return $res;
  }

  public function list_lokasi_pelayanan($lokasi_kasir = null)
  {
    $sql = "SELECT a.* 
            FROM mst_lokasi a
            WHERE a.map_lokasi_kasir=?
            ORDER BY lokasi_id ASC";
    $query = $this->db->query($sql, $lokasi_kasir);
    return $query->result_array();
  }

  function get_lokasi_id($parent_id = null)
  {
    $len = strlen($parent_id);
    $len_next = $len + 3;
    $sql = "SELECT MAX(RIGHT(lokasi_id,2)) as lokasi_id FROM mst_lokasi WHERE LEFT(lokasi_id,$len)=? AND LENGTH(lokasi_id)=?";
    $query = $this->db->query($sql, array($parent_id, $len_next));
    if ($query->num_rows() > 0) {
      $row = $query->row_array();
      $lokasi_id = abs($row['lokasi_id']) + 1;
      $lokasi_id = zerofill($lokasi_id, 2);
      $result = $parent_id . '.' . $lokasi_id;
    } else {
      $result = '01';
    }
    return $result;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    if ($id == null) {
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('mst_lokasi', $data);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('lokasi_id', $id)->update('mst_lokasi', $data);
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('lokasi_id', $id)->update('mst_lokasi', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('lokasi_id', $id)->delete('mst_lokasi');
    } else {
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('lokasi_id', $id)->update('mst_lokasi', $data);
    }
  }

  public function bpjs_poliklinik()
  {
    return $this->db->get('bpjs_ref_poliklinik')->result_array();
  }
}
