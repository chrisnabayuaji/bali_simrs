<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_obat extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND a.obat_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' ";
    }
    if (@$cookie['search']['jenisbarang_cd'] != '') {
      $where .= "AND a.jenisbarang_cd = '" . $this->db->escape_like_str($cookie['search']['jenisbarang_cd']) . "' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT * FROM mst_obat a 
            $where
            ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM mst_obat a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM mst_obat a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id)
  {
    $sql = "SELECT * FROM mst_obat WHERE obat_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  function get_obat_id($jenisbarang_cd = null)
  {
    $len = strlen($jenisbarang_cd);
    $sql = "SELECT MAX(RIGHT(obat_id,4)) as obat_id FROM mst_obat WHERE LEFT(obat_id,$len)='$jenisbarang_cd' AND LENGTH(obat_id) > $len";
    $query = $this->db->query($sql, $jenisbarang_cd);
    if ($query->num_rows() > 0) {
      $row = $query->row_array();
      $obat_id = abs($row['obat_id']) + 1;
      $obat_id = zerofill($obat_id, 2);
      $result = $jenisbarang_cd . '.' . $obat_id;
    } else {
      $result = '';
    }
    return $result;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    $data['tgl_catat'] = date('Y-m-d H:i:s');
    $data['harga_beli'] = clear_numeric($data['harga_beli']);
    $data['harga_jual'] = clear_numeric($data['harga_jual']);
    $data['harga_akhir'] = clear_numeric($data['harga_akhir']);
    if ($id == null) {
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('mst_obat', $data);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('obat_id', $id)->update('mst_obat', $data);
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('obat_id', $id)->update('mst_obat', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('obat_id', $id)->delete('mst_obat');
    } else {
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('obat_id', $id)->update('mst_obat', $data);
    }
  }
}
