<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pasien extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND a.pasien_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' ";
      $where .= "OR a.pasien_id LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
              a.*, b.jenispasien_nm 
            FROM mst_pasien a
            LEFT JOIN mst_jenis_pasien b ON a.jenispasien_id = b.jenispasien_id 
            $where
            ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM mst_pasien a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM mst_pasien a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id)
  {
    $sql = "SELECT * FROM mst_pasien WHERE pasien_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());

    $data['provinsi'] = get_wilayah($data['wilayah_prop'], 'name');
    $data['kabupaten'] = get_wilayah($data['wilayah_kab'], 'name');
    $data['kecamatan'] = get_wilayah($data['wilayah_kec'], 'name');
    $data['kelurahan'] = get_wilayah($data['wilayah_kel'], 'name');
    $data['wilayah_id'] = get_wilayah($data['wilayah_kel'], 'id');
    $data['tgl_lahir'] = to_date($data['tgl_lahir']);
    $data['tgl_catat'] = to_date($data['tgl_catat']);

    // unset
    unset($data['wilayah_prop'], $data['wilayah_kab'], $data['wilayah_kec'], $data['wilayah_kel']);

    if ($id == null) {
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('mst_pasien', $data);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('pasien_id', $id)->update('mst_pasien', $data);
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('pasien_id', $id)->update('mst_pasien', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('pasien_id', $id)->delete('mst_pasien');
    } else {
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('pasien_id', $id)->update('mst_pasien', $data);
    }
  }
}
