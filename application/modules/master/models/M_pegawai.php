<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pegawai extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND a.pegawai_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' ";
    }
    if (@$cookie['search']['parameter_cd'] != '') {
      $where .= "AND b.jenispegawai_cd = '" . $this->db->escape_like_str($cookie['search']['parameter_cd']) . "' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
                a.*, b.jenispegawai_nm  
            FROM mst_pegawai a 
            LEFT JOIN 
            (
                SELECT p.parameter_cd as jenispegawai_cd, p.parameter_val as jenispegawai_nm 
                FROM mst_parameter p 
                WHERE p.parameter_field = 'jenispegawai_cd'
            ) b ON a.jenispegawai_cd = b.jenispegawai_cd 
            $where
            ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data($jenispegawai_cd = '')
  {
    $where = "WHERE a.is_deleted = 0 ";
    if ($jenispegawai_cd != '') {
      $where .= "AND a.jenispegawai_cd='$jenispegawai_cd'";
    }

    $sql = "SELECT * FROM mst_pegawai a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function list_dokter()
  {
    $sql = "SELECT * 
            FROM mst_pegawai a 
            WHERE a.is_deleted = 0 
              AND a.jenispegawai_cd = '02'
            ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT 
              COUNT(1) as total
            FROM mst_pegawai a 
            LEFT JOIN 
            (
                SELECT p.parameter_cd as jenispegawai_cd, p.parameter_val as jenispegawai_nm 
                FROM mst_parameter p 
                WHERE p.parameter_field = 'jenispegawai_cd'
            ) b ON a.jenispegawai_cd = b.jenispegawai_cd 
            $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id)
  {
    $sql = "SELECT * FROM mst_pegawai WHERE pegawai_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  function by_field($field, $val, $type = 'row')
  {
    $sql = "SELECT * FROM mst_pegawai WHERE $field=?";
    $query = $this->db->query($sql, array($val));
    if ($type == 'row') {
      $res = $query->row_array();
    } else if ($type == 'result') {
      $res = $query->result_array();
    }
    return $res;
  }

  function get_pegawai_id($parent_id = null)
  {
    $len = strlen($parent_id);
    $sql = "SELECT MAX(RIGHT(pegawai_id,4)) as pegawai_id FROM mst_pegawai WHERE LEFT(pegawai_id,$len)=?";
    $query = $this->db->query($sql, $parent_id);
    if ($query->num_rows() > 0) {
      $row = $query->row_array();
      $pegawai_id = abs($row['pegawai_id']) + 1;
      $pegawai_id = zerofill($pegawai_id, 4);
      $result = $parent_id . '.' . $pegawai_id;
    } else {
      $result = '';
    }
    return $result;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    if ($id == null) {
      $data['tgl_lahir'] = to_date($data['tgl_lahir']);
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('mst_pegawai', $data);
    } else {
      $data['tgl_lahir'] = to_date($data['tgl_lahir']);
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('pegawai_id', $id)->update('mst_pegawai', $data);
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('pegawai_id', $id)->update('mst_pegawai', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('pegawai_id', $id)->delete('mst_pegawai');
    } else {
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('pegawai_id', $id)->update('mst_pegawai', $data);
    }
  }
}
