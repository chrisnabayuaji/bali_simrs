<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_tarif extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND (a.tarif_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' OR a.tarif_id LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%') ";
    }
    if (@$cookie['search']['tarif_induk'] != '' && @$cookie['search']['sub_tarif_induk'] != '') {
      $where .= "AND (a.parent_id = '" . $this->db->escape_like_str($cookie['search']['tarif_induk']) . "' OR a.parent_id = '" . $this->db->escape_like_str($cookie['search']['sub_tarif_induk']) . "' )";
    } else {
      if (@$cookie['search']['tarif_induk'] != '') {
        $where .= "AND a.parent_id = '" . $this->db->escape_like_str($cookie['search']['tarif_induk']) . "' ";
      }
      if (@$cookie['search']['sub_tarif_induk'] != '') {
        $where .= "AND a.parent_id = '" . $this->db->escape_like_str($cookie['search']['sub_tarif_induk']) . "' ";
      }
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT a.*, b.akun_nm  
            FROM mst_tarif a 
            LEFT JOIN mst_akun b ON a.akun_id=b.akun_id
            $where
            ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data($data = null)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$data['tarif_id'] != '') {
      $where .= " AND a.tarif_id = '" . $data['tarif_id'] . "'";
    }

    $sql = "SELECT * FROM mst_tarif a $where ORDER BY tarif_id ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM mst_tarif a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id)
  {
    $sql = "SELECT * FROM mst_tarif WHERE tarif_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  function get_tarif_id($parent_id = null)
  {
    $len = strlen($parent_id);
    $sql = "SELECT MAX(RIGHT(tarif_id,2)) as tarif_id FROM mst_tarif WHERE LEFT(tarif_id,$len)=?";
    $query = $this->db->query($sql, $parent_id);
    if ($query->num_rows() > 0) {
      $row = $query->row_array();
      $tarif_id = abs($row['tarif_id']) + 1;
      $tarif_id = zerofill($tarif_id, 2);
      $result = $parent_id . '.' . $tarif_id;
    } else {
      $result = '';
    }
    return $result;
  }

  function list_tarif_induk()
  {
    $sql = "SELECT * FROM mst_tarif where LENGTH(tarif_id) = 2";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function list_sub_tarif_induk($tarif_induk = '')
  {
    $sql_where = "";
    if ($tarif_induk != '') {
      $sql_where = "AND parent_id='$tarif_induk'";
    }
    $sql = "SELECT * FROM mst_tarif WHERE LENGTH(tarif_id) = 5 $sql_where";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    if ($id == null) {
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('mst_tarif', $data);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('tarif_id', $id)->update('mst_tarif', $data);
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('tarif_id', $id)->update('mst_tarif', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('tarif_id', $id)->delete('mst_tarif');
    } else {
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('tarif_id', $id)->update('mst_tarif', $data);
    }
  }
}
