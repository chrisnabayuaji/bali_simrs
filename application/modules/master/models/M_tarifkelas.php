<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_tarifkelas extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND (a.tarif_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' OR a.tarif_id LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%') ";
    }
    if (@$cookie['search']['tarif_induk'] != '' && @$cookie['search']['sub_tarif_induk'] != '') {
      $where .= "AND (a.parent_id = '" . $this->db->escape_like_str($cookie['search']['tarif_induk']) . "' OR a.parent_id = '" . $this->db->escape_like_str($cookie['search']['sub_tarif_induk']) . "' )";
    } else {
      if (@$cookie['search']['tarif_induk'] != '') {
        $where .= "AND a.parent_id = '" . $this->db->escape_like_str($cookie['search']['tarif_induk']) . "' ";
      }
      if (@$cookie['search']['sub_tarif_induk'] != '') {
        $where .= "AND a.parent_id = '" . $this->db->escape_like_str($cookie['search']['sub_tarif_induk']) . "' ";
      }
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT * FROM mst_tarif a 
            $where
            ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    $result = $query->result_array();
    // 
    foreach ($result as $key => $val) {
      $result[$key]['tarif_kelas'] = $this->_item_tarifkelas($result[$key]['tarif_id']);
    }
    return $result;
  }

  function _item_tarifkelas($tarif_id = null)
  {
    $sql = "SELECT * FROM mst_tarif_kelas WHERE tarif_id=? ORDER BY kelas_id ASC";
    $query = $this->db->query($sql, $tarif_id);
    $result = $query->result_array();
    $set_result = array();
    foreach ($result as $key => $val) {
      $set_key = $val['kelas_id'];
      $set_result[$set_key]['nominal'] = $val['nominal'];
    }
    return $set_result;
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM mst_tarif a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data_by_kelas_id($kelas_id = null)
  {
    $sql = "SELECT a.*, b.tarif_nm 
            FROM mst_tarif_kelas a
            LEFT JOIN mst_tarif b ON a.tarif_id = b.tarif_id 
            WHERE a.is_deleted = 0 AND a.kelas_id = ?
            ORDER BY created_at";
    $query = $this->db->query($sql, $kelas_id);
    return $query->result_array();
  }

  public function all_data_by_tarif_id($tarif_id = null)
  {
    $sql = "SELECT
              a.tarifkelas_id,
              a.tarif_id,
              a.kelas_id,
              b.kelas_nm, 
              a.js,
              a.jp,
              a.jb,
              a.nominal 
            FROM
              mst_tarif_kelas a 
              LEFT JOIN mst_kelas b ON a.kelas_id = b.kelas_id
            WHERE
              a.tarif_id = ?
              AND a.is_deleted = 0
            ORDER BY b.kelas_nm ASC";
    $query = $this->db->query($sql, $tarif_id);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM mst_tarif a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id)
  {
    $sql = "SELECT * FROM mst_tarif WHERE tarif_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  function list_kelas_all($tarif_id = null)
  {
    $sql = "SELECT a.*, b.js, b.jp, b.jb, b.nominal, b.tarifkelas_id, b.tarif_id   
            FROM mst_kelas a 
            LEFT JOIN mst_tarif_kelas b ON a.kelas_id=b.kelas_id AND b.tarif_id=?
            ORDER BY a.kelas_id ASC";
    $query = $this->db->query($sql, $tarif_id);
    $result = $query->result_array();
    return $result;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    $this->db->where('tarif_id', $data['tarif_id'])->delete('mst_tarif_kelas');
    foreach ($data['kelas_id'] as $key => $val) {
      if (@$data['nominal'][$key] != '') {
        $tarifkelas_id = @$data['tarifkelas_id'][$key];
        //
        $data_tarif['tarif_id'] = $data['tarif_id'];
        $data_tarif['kelas_id'] = $data['kelas_id'][$key];
        $data_tarif['js'] = clear_numeric($data['js'][$key]);
        $data_tarif['jp'] = clear_numeric($data['jp'][$key]);
        $data_tarif['jb'] = clear_numeric($data['jb'][$key]);
        $data_tarif['nominal'] = clear_numeric($data['nominal'][$key]);
        //
        $cek = $this->db->where('tarif_id', $data_tarif['tarif_id'])->where('kelas_id', $data_tarif['kelas_id'])->get('mst_tarif_kelas')->row_array();
        if ($cek != null) {
          $data_tarif['updated_at'] = date('Y-m-d H:i:s');
          $data_tarif['updated_by'] = $this->session->userdata('sess_user_realname');
          $this->db->where('tarifkelas_id', $tarifkelas_id);
          $this->db->update('mst_tarif_kelas', $data_tarif);
        } else {
          $data_tarif['tarifkelas_id'] = $data['tarif_id'] . '.' . $data['kelas_id'][$key];
          $data_tarif['created_at'] = date('Y-m-d H:i:s');
          $data_tarif['created_by'] = $this->session->userdata('sess_user_realname');
          $this->db->insert('mst_tarif_kelas', $data_tarif);
        }

        //update tarif di lain tempat
        //kamar
        $this->db->where('tarif_id', $data_tarif['tarif_id'])->where('kelas_id', $data_tarif['kelas_id'])->update('mst_kamar', array('nom_tarif' => $data_tarif['nominal']));
      }
    }
  }

  public function delete($id, $tarifkelas_id = null)
  {
    if ($tarifkelas_id != '') {
      $this->db->where('tarifkelas_id', $tarifkelas_id)->delete('mst_tarif_kelas');
    } else {
      $this->db->where('tarif_id', $id)->delete('mst_tarif_kelas');
    }
  }
}
