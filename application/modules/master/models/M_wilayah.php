<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_wilayah extends CI_Model {

	public function where($cookie)
	{
		$where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND a.wilayah_nm LIKE '%".$this->db->escape_like_str($cookie['search']['term'])."%' ";
		}
		return $where;
	}

  public function where_level($params) {
      $sql_where = "";
      if($params['level'] == '1') { // propinsi
          $sql_where .= " AND LENGTH(a.wilayah_id)='2'";
      }
      if($params['wilayah_parent'] != 0) { 
          $sql_where .= " AND a.wilayah_parent='".$this->db->escape_str($params['wilayah_parent'])."'";
      }
      return $sql_where;
  }

  public function list_data($cookie, $params=null)
  {
    $where = $this->where($cookie);
    $where.= $this->where_level($params);

    $sql = "SELECT * FROM mst_wilayah a 
            $where
            ORDER BY "
              .$cookie['order']['field']." ".$cookie['order']['type'].
            " LIMIT ".$cookie['cur_page'].",".$cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM mst_wilayah a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie, $params=null)
  {
    $where = $this->where($cookie);
    $where.= $this->where_level($params);

    $sql = "SELECT COUNT(1) as total FROM mst_wilayah a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id) {
    $sql = "SELECT * FROM mst_wilayah WHERE wilayah_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  function get_params($wilayah_parent=null) {
    $params = array(
        'level' => $this->get_wilayah_level($wilayah_parent),    
        'level_nm' => $this->get_wilayah_level_nm($wilayah_parent),  
        'input_nm' => $this->get_wilayah_input_nm($wilayah_parent),  
        'wilayah_parent' => $wilayah_parent,
        'wilayah_back' => $this->get_wilayah_back($wilayah_parent),
    );
    $params['input_nm_next'] = $this->get_wilayah_input_nm('',($params['level']+1));
    return $params;
  }

  function get_wilayah_level($wilayah_parent=null) {
    $len = strlen($wilayah_parent);
    if($len == 0 || $len == 1) $level = '1';
    elseif($len == 2) $level = '2';
    elseif($len == 5) $level = '3';
    elseif($len == 8) $level = '4';
    return @$level;
  }

  function get_wilayah_level_nm($wilayah_parent=null) {
    $level = $this->get_wilayah_level($wilayah_parent);
    //
    $result = '';
    if($level == '1') $result = 'Propinsi';
    elseif($level == '2') $result = 'Kab/Kota';
    elseif($level == '3') $result = 'Kecamatan';
    elseif($level == '4') $result = 'Desa/Kelurahan';
    return $result;
  }

  function get_wilayah_input_nm($wilayah_parent=null,$level=null) {
    if($level == '') $level = $this->get_wilayah_level($wilayah_parent);
    //
    $result = '';
    if($level == '1') $result = 'wilayah_prop';
    elseif($level == '2') $result = 'wilayah_kab';
    elseif($level == '3') $result = 'wilayah_kec';
    elseif($level == '4') $result = 'wilayah_kel';
    return $result;
  }

  function get_wilayah_back($wilayah_parent=null) {
    $level = $this->get_wilayah_level($wilayah_parent);
    //
    $arr = explode('.', $wilayah_parent);
    //
    $result = '';
    if($level == '4') $result = $arr[0].'.'.$arr[1];
    elseif($level == '3') $result = $arr[0];
    return $result;
  }

  public function get_profile() {
    return $this->db->get('mst_identitas')->row_array();
  }

  public function list_wilayah_by_profile() {
    $wilayah_id = $this->get_profile();
    $sql = "SELECT * FROM mst_wilayah WHERE wilayah_id=? ORDER BY wilayah_id ASC";
    $query = $this->db->query($sql, $wilayah_id['wilayah_prop']);
    return $query->result_array();
  }

  public function list_wilayah_by_id($wilayah_id=null) {
    $sql = "SELECT * FROM mst_wilayah WHERE wilayah_id=? ORDER BY wilayah_id ASC";
    $query = $this->db->query($sql, $wilayah_id);
    return $query->result_array();
  }

  function list_wilayah_by_parent($wilayah_parent=null) {
    $sql = "SELECT * FROM mst_wilayah WHERE wilayah_parent=? ORDER BY wilayah_id ASC";
    $query = $this->db->query($sql, $wilayah_parent);
    return $query->result_array();
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    if ($id == null) {
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('mst_wilayah', $data);
    }else{
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('wilayah_id', $id)->update('mst_wilayah', $data);
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('wilayah_id',$id)->update('mst_wilayah', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('wilayah_id', $id)->delete('mst_wilayah');
    }else{
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('wilayah_id', $id)->update('mst_wilayah', $data);
    }
  }
  
}