<!-- js -->
<?php $this->load->view('_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Akun Parent</label>
        <div class="col-lg-9 col-md-9">
          <select class="chosen-select custom-select w-100" name="akun_parent" id="akun_parent">
            <option value="">None</option>
            <?php foreach ($list_akun as $row): ?>
              <option value="<?=$row['akun_id']?>" <?php if(@$main['akun_parent'] == $row['akun_id']) echo 'selected'?>><?=$row['akun_id']?> - <?=$row['akun_nm']?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Kode <span class="text-danger">*</span></label>
        <div class="col-lg-4 col-md-4">
          <input type="text" class="form-control" name="akun_id" id="akun_id" value="<?=@$main['akun_id']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Nama Akun <span class="text-danger">*</span></label>
        <div class="col-lg-7 col-md-7">
          <input type="text" class="form-control" name="akun_nm" id="akun_nm" value="<?=@$main['akun_nm']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Tipe Akun</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="tp_akun">
            <option value="G" <?php if(@$main['tp_akun'] == 'G') echo 'selected'?>>Group</option>
            <option value="D" <?php if(@$main['tp_akun'] == 'D') echo 'selected'?>>Detail</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Tipe Saldo</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="tp_saldo">
            <option value="K" <?php if(@$main['tp_saldo'] == 'K') echo 'selected'?>>Kredit</option>
            <option value="D" <?php if(@$main['tp_saldo'] == 'D') echo 'selected'?>>Debet</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Tipe Laporan</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="tp_laporan">
            <option value="LR" <?php if(@$main['tp_laporan'] == 'LR') echo 'selected'?>>Laba Rugi</option>
            <option value="NR" <?php if(@$main['tp_laporan'] == 'NR') echo 'selected'?>>Neraca</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Tipe Laporan Sub</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="tp_laporan_sub">
            <option value="PND" <?php if(@$main['tp_laporan_sub'] == 'PND') echo 'selected'?>>Pendapatan</option>
            <option value="PNG" <?php if(@$main['tp_laporan_sub'] == 'PNG') echo 'selected'?>>Pengeluaran</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Status</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_active">
            <option value="1" <?php if(@$main['is_active'] == '1') echo 'selected'?>>Aktif</option>
            <option value="0" <?php if(@$main['is_active'] == '0') echo 'selected'?>>Tidak Aktif</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>