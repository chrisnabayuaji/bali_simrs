<!-- js -->
<?php $this->load->view('master/barang/_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Kode <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="barang_id" id="barang_id" value="<?=@$main['barang_id']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Nama Barang <span class="text-danger">*</span></label>
        <div class="col-lg-8 col-md-8">
          <input type="text" class="form-control" name="barang_nm" id="barang_nm" value="<?=@$main['barang_nm']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Harga Beli <span class="text-danger">*</span></label>
        <div class="col-lg-4 col-md-4">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text text-small">Rp</span>
            </div>
            <input type="text" class="form-control text-right autonumeric" name="harga_beli" value="<?=@$main['harga_beli']?>" required="">
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Harga Jual <span class="text-danger">*</span></label>
        <div class="col-lg-4 col-md-4">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text text-small">Rp</span>
            </div>
            <input type="text" class="form-control text-right autonumeric" name="harga_jual" value="<?=@$main['harga_jual']?>" required="">
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Harga Akhir <span class="text-danger">*</span></label>
        <div class="col-lg-4 col-md-4">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text text-small">Rp</span>
            </div>
            <input type="text" class="form-control text-right autonumeric" name="harga_akhir" value="<?=@$main['harga_akhir']?>" required="">
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Stok Minimal <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="stok_minimal" id="stok_minimal" value="<?=@$main['stok_minimal']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Satuan <span class="text-danger">*<span></label>
        <div class="col-lg-4 col-md-4">
          <select class="form-control chosen-select" name="satuan_cd" id="satuan_cd" required="">
            <option value="">- Pilih -</option>
            <?php foreach(get_parameter('satuan_cd') as $r): ?>
              <option value="<?=$r['parameter_cd']?>" <?=(@$main['satuan_cd'] == $r['parameter_cd']) ? 'selected' : '';?>><?=$r['parameter_val']?></option>
            <?php endforeach;?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Jenis Barang <span class="text-danger">*<span></label>
        <div class="col-lg-4 col-md-4">
          <select class="form-control chosen-select" name="jenisbarang_cd" id="jenisbarang_cd" required="">
            <option value="">- Pilih -</option>
            <?php foreach(get_parameter('jenisbarang_cd') as $r): ?>
              <option value="<?=$r['parameter_cd']?>" <?=(@$main['jenisbarang_cd'] == $r['parameter_cd']) ? 'selected' : '';?>><?=$r['parameter_val']?></option>
            <?php endforeach;?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Sub Jenis Barang</label>
        <div class="col-lg-4 col-md-4">
          <select class="form-control chosen-select" name="subjenisbarang_cd" id="subjenisbarang_cd" required="">
            <option value="">- Pilih -</option>
            <?php foreach(get_parameter('subjenisbarang_cd') as $r): ?>
              <option value="<?=$r['parameter_cd']?>" <?=(@$main['subjenisbarang_cd'] == $r['parameter_cd']) ? 'selected' : '';?>><?=$r['parameter_val']?></option>
            <?php endforeach;?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Keterangan Barang <span class="text-danger">*</span></label>
        <div class="col-lg-7 col-md-7">
          <textarea rows="3" class="form-control" name="keterangan_barang"><?=@$main['keterangan_barang']?></textarea>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Status</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_active">
            <option value="1" <?php if(@$main['is_active'] == '1') echo 'selected'?>>Aktif</option>
            <option value="0" <?php if(@$main['is_active'] == '0') echo 'selected'?>>Tidak Aktif</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>