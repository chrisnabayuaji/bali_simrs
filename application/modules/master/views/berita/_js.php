<script type="text/javascript">
  $(document).ready(function() {
    var form = $("#form-data").validate({
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    $('.chosen-select').on('select2:select', function(e) {
      form.element(this);
    });
    //
    $('input[name="news_title"]').bind('keyup', function(e) {
      e.preventDefault();
      $.post('<?= site_url($nav['nav_url'] . '/ajax/permalink') ?>', {
        news_title: $(this).val()
      }, function(data) {
        $('input[name="news_url"]').val(data.permalink);
      }, 'json');
    }).bind('change', function(e) {
      //e.preventDefault();
      var news_url = $('input[name="news_url"]').val();
      var news_url_exist = "<?= @$main['news_url'] ?>";
      $.post('<?= site_url($nav['nav_url'] . '/ajax/validate_news_url') ?>', {
        news_url: news_url,
        news_url_exist: news_url_exist
      }, function(data) {
        if (data.result == 'false') {
          alert('Maaf, Judul sudah digunakan. Silahkan mengganti dengan judul yang lain !');
          $('input[name="news_title"]').val('').focus();
          $('input[name="news_url"]').val('');
          return false;
        }
      }, 'json');
    });
    // image
    $('#add_item_image').bind('click', function(e) {
      e.preventDefault();
      var no = $('#image_no').val();
      add_item_image(no, '');
    });
    //
    add_item_image('0', '<?= @$main["news_id"] ?>');

    function add_item_image(no, news_id) {
      $.get('<?= site_url("master/berita/ajax/add_item_image") ?>?image_no=' + no + '&news_id=' + news_id, null, function(data) {
        $('#box_image').append(data.html);
        $('#image_no').val(data.image_no);
      }, 'json');
    }
  })
</script>