<link href="<?= base_url() ?>assets/plugins/summernote/summernote-bs4.min.css" rel="stylesheet">
<script src="<?= base_url() ?>assets/plugins/summernote/summernote-bs4.min.js"></script>

<?php $this->load->view('master/berita/_js'); ?>
<form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?= $form_action ?>" class="needs-validation" novalidate autocomplete="off">
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline col-title">
          <div class="col-back">
            <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
          </div>
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            <?= $nav['nav_nm'] ?>
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-folder-open"></i> Data Induk</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><?= $nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item active"><span>Index</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>

    <div class="row full-page mt-4">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <h4 class="card-title border-bottom border-2 pb-2 mb-3"><?= $nav['nav_nm'] ?></h4>
            <div class="row mt-1">
              <div class="col-md-8">
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Judul Berita <span class="text-danger">*<span></label>
                  <div class="col-lg-9 col-md-9">
                    <input type="text" class="form-control" name="news_title" id="news_title" value="<?= @$main['news_title'] ?>" required>
                    <input type="hidden" class="form-control" name="news_url" value="<?= @$main['news_url'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Isi Konten Berita <span class="text-danger">*<span></label>
                  <div class="col-lg-9 col-md-9">
                    <textarea name="news_content" id="summernote" class="form-control" required><?= @$main['news_content'] ?></textarea>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Penulis <span class="text-danger">*<span></label>
                  <div class="col-lg-7 col-md-9">
                    <input type="text" class="form-control" name="author_nm" id="author_nm" value="<?= @$main['author_nm'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Tgl Terbit <span class="text-danger">*<span></label>
                  <div class="col-lg-6 col-md-9">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datetimepicker" name="created_at" id="created_at" value="<?php if (@$main) {
                                                                                                                        echo to_date(@$main['created_at'], '-', 'full_date');
                                                                                                                      } else {
                                                                                                                        echo date('d-m-Y H:i:s');
                                                                                                                      } ?>" required aria-invalid="false">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Status</label>
                  <div class="col-lg-5 col-md-9">
                    <select class="form-control chosen-select" name="news_st" id="news_st">
                      <option value="1" <?= (@$main['news_st'] == '1') ? 'selected' : ''; ?>>Publish</option>
                      <option value="2" <?= (@$main['news_st'] == '2') ? 'selected' : ''; ?>>Draft</option>
                      <option value="0" <?= (@$main['news_st'] == '0') ? 'selected' : ''; ?>>Not Publish</option>
                    </select>
                  </div>
                </div>
                <br>
                <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-images"></i> Upload Gambar</h4>
                <div id="box_image"></div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label"></label>
                  <div class="col-lg-5 col-md-9">
                    <input type="hidden" name="image_no" id="image_no" value="0">
                    <a href="javascript:void(0)" class="btn btn-primary btn-xs" id="add_item_image"><i class="fa fa-plus"></i> Tambah Item Gambar</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="btn-form">
    <div class="btn-form-action">
      <div class="btn-form-action-bottom w-100 small-text clearfix">
        <div class="col-lg-10 col-md-8 col-4">
          <a href="<?= site_url() . '/' . $nav['nav_url'] ?>" class="btn btn-xs btn-success"><i class="fas fa-arrow-left"></i> Kembali</a>
        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-clear">
        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-save">
          <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script>
  $('#summernote').summernote({
    height: 700, // set editor height
    minHeight: null, // set minimum height of editor
    maxHeight: null,
    toolbar: [
      // [groupName, [list of button]]
      ['style', ['bold', 'italic', 'underline']],
      //['font', ['strikethrough', 'superscript']],
      ['fontsize', ['fontsize']],
      ['color', ['color']],
      ['para', ['ul', 'ol', 'paragraph']],
      ['table', ['table']],
      ['insert', ['link', 'picture', 'video', 'help']]
    ]
  });
</script>