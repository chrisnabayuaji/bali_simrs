<script type="text/javascript">
  $(document).ready(function () {
  	$("#form-data").validate( {
	    rules: {
	      
	    },
	    messages: {
	      
	    },
	    errorElement: "em",
	    errorPlacement: function (error,element) {
	      error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container"));
        }else{
          error.insertAfter(element);
        }
	    },
	    highlight: function (element,errorClass,validClass) {
	      $(element).addClass("is-invalid").removeClass("is-valid");
	    },
	    unhighlight: function (element, errorClass, validClass) {
	      $(element).addClass("is-valid").removeClass("is-invalid");
	    },
	    submitHandler: function (form) {
	      $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
	      $(".btn-submit").attr("disabled", "disabled");
	      $(".btn-cancel").attr("disabled", "disabled");
	      form.submit();
	    }
	  });
	  // wilayah
   	 <?php if(@$main['wilayah_prop'] != ''):?>
	  _get_wilayah('<?=@$main["wilayah_prop"]?>', '<?=@$main["wilayah_prop"]?>', '<?=@$main["wilayah_kab"]?>', '<?=@$main["wilayah_kec"]?>', '<?=@$main["wilayah_kel"]?>');
	  <?php endif;?>
	  //
	  $('#wilayah_prop').bind('change',function(e) {
	    e.preventDefault();
	    var i = $(this).val().split('#');
	    _get_wilayah(i[0]);
	  })
	  //
	  function _get_wilayah(i, prop='', kab='', kec='', kel='') {
	    var ext_var = 'wilayah_prop='+prop;
	        ext_var+= '&wilayah_kab='+kab;
	        ext_var+= '&wilayah_kec='+kec;
	        ext_var+= '&wilayah_kel='+kel;
	    $.get('<?=site_url('master/identitas/ajax/get_wilayah_child')?>?wilayah_parent='+i+'&'+ext_var,null,function(data) {
	        $('#box_wilayah_kab').html(data.html);
	    },'json');
	  }
  })
  function save(type, val) {
    window.location.href = "<?=site_url().'/'.$nav['nav_url'].'/save/'?>"+type+"/"+val;
  }
</script>