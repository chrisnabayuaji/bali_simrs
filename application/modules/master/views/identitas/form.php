<!-- js -->
<?php $this->load->view('_js') ?>
<!-- / -->
<form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?= $form_action ?>" class="needs-validation" novalidate>
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline col-title">
          <div class="col-back">
            <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
          </div>
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            <?= $nav['nav_nm'] ?>
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-hospital-alt"></i> Data Dasar RS</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><?= $nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item active"><span>Form</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>

    <div class="row full-page mt-4">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <div class="row mt-1">
              <div class="col-md-6">
                <h4 class="card-title border-bottom border-2 pb-2 mb-3">Konfigurasi Rumah Sakit</h4>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Kode Rumah Sakit <span class="text-danger">*<span></label>
                  <div class="col-lg-2 col-md-2">
                    <input type="text" class="form-control" name="kode" value="<?= @$main['kode'] ?>" required="">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Kode Faskes BPJS <span class="text-danger">*<span></label>
                  <div class="col-lg-2 col-md-2">
                    <input type="text" class="form-control" name="kode_faskes_bpjs" value="<?= @$main['kode_faskes_bpjs'] ?>" required="">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Nama Rumah Sakit <span class="text-danger">*<span></label>
                  <div class="col-lg-8 col-md-8">
                    <input type="text" class="form-control" name="rumah_sakit" value="<?= @$main['rumah_sakit'] ?>" required="">
                  </div>
                </div>
                <div class="form-group row mb-2">
                  <label class="col-lg-3 col-md-3 col-form-label mt-n3">Kepanjangan<br>Unit Kerja <span class="text-danger">*<span></label>
                  <div class="col-lg-8 col-md-8">
                    <input type="text" class="form-control" name="kepanjangan_unit_kerja" value="<?= @$main['kepanjangan_unit_kerja'] ?>" required="">
                    <small class="text-danger">Contoh : Rumah Sakit Umum Daerah</small>
                  </div>
                </div>
                <div class="form-group row mb-n2">
                  <label class="col-lg-3 col-md-3 col-form-label mt-n3">Nama RS Tanpa Unit Kerja <span class="text-danger">*<span></label>
                  <div class="col-lg-8 col-md-8">
                    <input type="text" class="form-control" name="rs_tanpa_unit_kerja" value="<?= @$main['rs_tanpa_unit_kerja'] ?>" required="">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Propinsi <span class="text-danger">*<span></label>
                  <div class="col-lg-5 col-md-5">
                    <select class="chosen-select custom-select w-100" name="wilayah_prop" id="wilayah_prop" required="">
                      <option value="">- Propinsi -</option>
                      <?php foreach ($list_wilayah_prop as $wp) : ?>
                        <option value="<?= $wp['wilayah_id'] ?>#<?= $wp['wilayah_nm'] ?>" <?php if ($wp['wilayah_id'] == @$main['wilayah_prop']) echo 'selected' ?>><?= $wp['wilayah_id'] ?> - <?= $wp['wilayah_nm'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Kab/Kota <span class="text-danger">*<span></label>
                  <div class="col-lg-5 col-md-5">
                    <div id="box_wilayah_kab">
                      <select class="chosen-select custom-select w-100" name="wilayah_kab" id="wilayah_kab" required="">
                        <option value="">- Kab/Kota -</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Kecamatan <span class="text-danger">*<span></label>
                  <div class="col-lg-5 col-md-5">
                    <div id="box_wilayah_kec">
                      <select class="chosen-select custom-select w-100" name="wilayah_kec" id="wilayah_kec" required="">
                        <option value="">- Kecamatan -</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Desa/Kelurahan <span class="text-danger">*<span></label>
                  <div class="col-lg-5 col-md-5">
                    <div id="box_wilayah_kel">
                      <select class="chosen-select custom-select w-100" name="wilayah_kel" id="wilayah_kel" required="">
                        <option value="">- Desa/Kelurahan -</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Jalan</label>
                  <div class="col-lg-8 col-md-8">
                    <input type="text" class="form-control" name="jalan" value="<?= @$main['jalan'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Kepala Rumah Sakit</label>
                  <div class="col-lg-5 col-md-5">
                    <input type="text" class="form-control" name="kepala" value="<?= @$main['kepala'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">NIP Kepala Rumah Sakit</label>
                  <div class="col-lg-5 col-md-5">
                    <input type="text" class="form-control" name="nip_kepala" value="<?= @$main['nip_kepala'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">No Telpon</label>
                  <div class="col-lg-5 col-md-5">
                    <input type="text" class="form-control" name="telp" value="<?= @$main['telp'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Fax</label>
                  <div class="col-lg-5 col-md-5">
                    <input type="text" class="form-control" name="fax" value="<?= @$main['fax'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Logo Rumah Sakit <span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-9">
                    <input type="file" name="logo_rumah_sakit" class="form-control" id="logo_rumah_sakit">
                    <?php if (@$main['logo_rumah_sakit'] != '') : ?>
                      <div id="box_img">
                        <div class="text-danger small-text font-weight-semibold">* Upload gambar untuk mengubah</div>
                        <div class="card mb-2 mt-1">
                          <div class="card-body">
                            <div class="text-center">
                              <img src="<?= base_url() ?>assets/images/icon/<?= @$main['logo_rumah_sakit'] ?>" class="img-lg rounded">
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php endif; ?>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Gambar Rumah Sakit <span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-9">
                    <input type="file" name="img_rumah_sakit" class="form-control" id="img_rumah_sakit">
                    <?php if (@$main['img_rumah_sakit'] != '') : ?>
                      <div id="box_img">
                        <div class="text-danger small-text font-weight-semibold">* Upload gambar untuk mengubah</div>
                        <div class="card mb-2 mt-1">
                          <div class="card-body">
                            <div class="text-center">
                              <img src="<?= base_url() ?>assets/images/icon/<?= @$main['img_rumah_sakit'] ?>" class="img-lg rounded">
                            </div>
                          </div>
                        </div>
                      </div>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="btn-form">
    <div class="btn-form-action">
      <div class="btn-form-action-bottom w-100 small-text clearfix">
        <div class="col-lg-10 col-md-8 col-4">
        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-clear">
          <button type="reset" onClick="window.location.reload();" class="btn btn-xs btn-primary"><i class="mdi mdi-refresh"></i> Clear</button>
        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-save">
          <button type="submit" class="btn btn-xs btn-primary"><i class="mdi mdi-file-check"></i> Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>