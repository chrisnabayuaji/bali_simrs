<script type="text/javascript">
  $(document).ready(function () {
    $("#form-data").validate( {
      rules: {
        
      },
      messages: {
        
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        form.submit();
      }
    });
    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');
    //
  	$('#parent_id').bind('change',function(e) {
      e.preventDefault();
      var parent_id = $('#parent_id').val();
      $.get('<?=site_url("master/itemlab/ajax/get_itemlab_id")?>?parent_id='+parent_id,null,function(data) {
          $('#itemlab_id').val(data.result);
      },'json');
    });
  });
</script>