<!-- js -->
<?php $this->load->view('master/itemlab/_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <input type="hidden" name="itemlab_id" value="<?=@$main['itemlab_id']?>">
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Item Induk</label>
        <div class="col-lg-8 col-md-5">
          <select class="chosen-select custom-select w-100" name="parent_id" id="parent_id">
            <option value="">- None -</option>
            <?php foreach($list_itemlab as $li):?>
            <option value="<?=$li['itemlab_id']?>" <?php if($li['itemlab_id'] == @$main['parent_id']) echo 'selected'?>><?=$li['itemlab_id']?> - <?=$li['itemlab_nm']?></option>
            <?php endforeach;?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Kode</label>
        <div class="col-lg-4 col-md-5">
          <input type="text" class="form-control" name="itemlab_id" id="itemlab_id" value="<?=@$main['itemlab_id']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Item Laboratorium</label>
        <div class="col-lg-7 col-md-9">
          <input type="text" class="form-control" name="itemlab_nm" value="<?=@$main['itemlab_nm']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Nilai Normal</label>
        <div class="col-lg-4 col-md-9">
          <input type="text" class="form-control" name="nilai_normal" value="<?=@$main['nilai_normal']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Paket</label>
        <div class="col-lg-4 col-md-9">
          <input type="text" class="form-control" name="paket" value="<?=@$main['paket']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Kesesuaian Tarif</label>
        <div class="col-lg-8 col-md-9">
          <select class="chosen-select custom-select w-100" name="tarif_id">
            <option value="">- None -</option>
            <?php foreach($list_tarif as $lt):?>
            <option value="<?=$lt['tarif_id']?>" <?php if($lt['tarif_id'] == @$main['tarif_id']) echo 'selected'?>><?=$lt['tarif_id']?> - <?=$lt['tarif_nm']?></option>
            <?php endforeach;?>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>