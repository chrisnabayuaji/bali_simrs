<script type="text/javascript">
  $(document).ready(function() {
    $("#form-data").validate({
      // onfocusout: true,
      onkeyup: false,
      rules: {
        itemlab_id: {
          remote: {
            url: '<?= site_url() . '/' . $nav['nav_url'] ?>/ajax/cek_id/<?= @$main['itemlab_id'] ?>',
            type: 'post',
            data: {
              itemlab_id: function() {
                return $("#itemlab_id").val();
              }
            }
          }
        },
      },
      messages: {
        itemlab_id: {
          remote: 'Kode sudah digunakan!'
        }
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });
    $(".autonumeric").autoNumeric({
      aSep: ".",
      aDec: ",",
      vMax: "999999999999999",
      vMin: "0",
    });
    $(".autonumeric-float").autoNumeric({
      aSep: ".",
      aDec: ",",
      aForm: true,
      vMax: "999999999999999999.99999999",
      vMin: "-999999999999999999.99999999",
      aPad: false
    });
    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');
    //
    $('#parent_id').bind('change', function(e) {
      e.preventDefault();
      var parent_id = $('#parent_id').val();
      $.get('<?= site_url("master/itemlab/ajax/get_itemlab_id") ?>?parent_id=' + parent_id, null, function(data) {
        $('#itemlab_id').val(data.result);
      }, 'json');
    });

    $("#tipe_rujukan").bind('change', function() {
      var tipe_rujukan = $(this).val();
      change_tipe_rujukan(tipe_rujukan);
    })

    $("#is_periksa").bind('change', function() {
      var is_periksa = $(this).val();

      if (is_periksa == '0') {
        $("#tipe_rujukan_box").hide();
        change_tipe_rujukan(0);
      } else {
        $("#tipe_rujukan_box").show();
        <?php if ($id == null) : ?>
          change_tipe_rujukan('1');
        <?php else : ?>
          change_tipe_rujukan('<?= @$main['tipe_rujukan'] ?>');
        <?php endif; ?>
      }
    })
    $("#is_tagihan").bind('change', function() {
      var is_tagihan = $(this).val();

      if (is_tagihan == '0') {
        $("#tarif_id_box").hide();
      } else {
        $("#tarif_id_box").show();
      }
    })

    <?php if ($id == null) : ?>
      if ($("#is_periksa").val() == '0') {
        $("#tipe_rujukan_box").hide();
        change_tipe_rujukan(0);
      } else {
        $("#tipe_rujukan_box").show();
        change_tipe_rujukan('1');
      }
    <?php else : ?>
      if ($("#is_periksa").val() == '0') {
        $("#tipe_rujukan_box").hide();
        change_tipe_rujukan(0);
      } else {
        $("#tipe_rujukan_box").show();
        change_tipe_rujukan('<?= @$main['tipe_rujukan'] ?>');
      }
      if ('<?= @$main['is_tagihan'] ?>' == '0') {
        $("#tarif_id_box").hide();
      } else {
        $("#tarif_id_box").show();
      }
    <?php endif; ?>
  });

  function change_tipe_rujukan(i) {

    $("#konfirmasi_box").hide();

    $("#rentang_box").hide();
    $("#rentang_l_box").hide();
    $("#rentang_p_box").hide();

    $("#kurang_box").hide();
    $("#kurang_l_box").hide();
    $("#kurang_p_box").hide();

    $("#lebih_box").hide();
    $("#lebih_l_box").hide();
    $("#lebih_p_box").hide();

    $("#satuan_box").hide();

    $("#informasi_box").hide();

    switch (i) {
      case '1':
        $("#konfirmasi_box").show();
        break;

      case '2':
        $("#rentang_box").show();
        $("#satuan_box").show();
        break;

      case '3':
        $("#rentang_l_box").show();
        $("#rentang_p_box").show();
        $("#satuan_box").show();
        break;

      case '4':
        $("#kurang_box").show();
        $("#satuan_box").show();
        break;

      case '5':
        $("#kurang_l_box").show();
        $("#kurang_p_box").show();
        $("#satuan_box").show();
        break;

      case '6':
        $("#lebih_box").show();
        $("#satuan_box").show();
        break;

      case '7':
        $("#lebih_l_box").show();
        $("#lebih_p_box").show();
        $("#satuan_box").show();
        break;

      case '8':
        $("#informasi_box").show();
        break;
    }
  }
</script>