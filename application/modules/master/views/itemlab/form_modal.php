<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?= $form_action ?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Item Induk</label>
        <div class="col-lg-8 col-md-5">
          <select class="chosen-select custom-select w-100" name="parent_id" id="parent_id">
            <option value="">- None -</option>
            <?php foreach ($list_itemlab as $li) : ?>
              <option value="<?= $li['itemlab_id'] ?>" <?php if ($li['itemlab_id'] == @$main['parent_id']) echo 'selected' ?>><?= $li['itemlab_id'] ?> - <?= $li['itemlab_nm'] ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Kode <span class="text-danger">*<span></label>
        <div class="col-lg-4 col-md-5">
          <input type="text" class="form-control" name="itemlab_id" id="itemlab_id" value="<?= @$main['itemlab_id'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Item Laboratorium <span class="text-danger">*<span></label>
        <div class="col-lg-7 col-md-9">
          <input type="text" class="form-control" name="itemlab_nm" value="<?= @$main['itemlab_nm'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Periksa?</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" id="is_periksa" name="is_periksa">
            <option value="1" <?php if (@$main['is_periksa'] == '1') echo 'selected' ?>>Ya</option>
            <option value="0" <?php if (@$main['is_periksa'] == '0') echo 'selected' ?>>Tidak</option>
          </select>
        </div>
      </div>
      <div id="tipe_rujukan_box" class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Tipe rujukan</label>
        <div class="col-lg-4 col-md-9">
          <select class="form-control chosen-select" name="tipe_rujukan" id="tipe_rujukan">
            <option value="1" <?= (@$main['tipe_rujukan'] == 1) ? 'selected' : '' ?>>Konfirmasi</option>
            <option value="2" <?= (@$main['tipe_rujukan'] == 2) ? 'selected' : '' ?>>Rentang Nilai (Umum)</option>
            <option value="3" <?= (@$main['tipe_rujukan'] == 3) ? 'selected' : '' ?>>Rentang Nilai (Gender)</option>
            <option value="4" <?= (@$main['tipe_rujukan'] == 4) ? 'selected' : '' ?>>Kurang dari (Umum)</option>
            <option value="5" <?= (@$main['tipe_rujukan'] == 5) ? 'selected' : '' ?>>Kurang dari (Gender)</option>
            <option value="6" <?= (@$main['tipe_rujukan'] == 6) ? 'selected' : '' ?>>Lebih dari (Umum)</option>
            <option value="7" <?= (@$main['tipe_rujukan'] == 7) ? 'selected' : '' ?>>Lebih dari (Gender)</option>
            <option value="8" <?= (@$main['tipe_rujukan'] == 8) ? 'selected' : '' ?>>Informasi</option>
          </select>
        </div>
      </div>
      <div id="konfirmasi_box" class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Nilai Normal <span class="text-danger">*<span></label>
        <div class="col-lg-4 col-md-9">
          <select class="form-control chosen-select" name="konfirmasi" id="konfirmasi">
            <option value="-1" <?= (@$main['konfirmasi'] == -1) ? 'selected' : ''; ?>>-</option>
            <option value="0" <?= (@$main['konfirmasi'] == 0) ? 'selected' : ''; ?>>Negatif</option>
            <option value="1" <?= (@$main['konfirmasi'] == 1) ? 'selected' : ''; ?>>Positif</option>
          </select>
        </div>
      </div>
      <div id="rentang_box" class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Nilai Normal <span class="text-danger">*<span></label>
        <label class="col-lg-1 col-md-4 col-form-label"><b>Min</b></label>
        <div class="col-lg-3 col-md-2">
          <input type="text" class="form-control autonumeric-float" name="rentang_min" value="<?= @$main['rentang_min'] ?>" required="">
        </div>
        <label class="col-lg-1 col-md-4 col-form-label"><b>Max</b></label>
        <div class="col-lg-3 col-md-2">
          <input type="text" class="form-control autonumeric-float" name="rentang_max" value="<?= @$main['rentang_max'] ?>" required="">
        </div>
      </div>
      <div id="rentang_l_box" class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Nilai Normal (Laki-laki) <span class="text-danger">*<span></label>
        <label class="col-lg-1 col-md-4 col-form-label"><b>Min</b></label>
        <div class="col-lg-3 col-md-2">
          <input type="text" class="form-control autonumeric-float" name="rentang_l_min" value="<?= @$main['rentang_l_min'] ?>" required="">
        </div>
        <label class="col-lg-1 col-md-4 col-form-label"><b>Max</b></label>
        <div class="col-lg-3 col-md-2">
          <input type="text" class="form-control autonumeric-float" name="rentang_l_max" value="<?= @$main['rentang_l_max'] ?>" required="">
        </div>
      </div>
      <div id="rentang_p_box" class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Nilai Normal (Perempuan) <span class="text-danger">*<span></label>
        <label class="col-lg-1 col-md-4 col-form-label"><b>Min</b></label>
        <div class="col-lg-3 col-md-2">
          <input type="text" class="form-control autonumeric-float" name="rentang_p_min" value="<?= @$main['rentang_p_min'] ?>" required="">
        </div>
        <label class="col-lg-1 col-md-4 col-form-label"><b>Max</b></label>
        <div class="col-lg-3 col-md-2">
          <input type="text" class="form-control autonumeric-float" name="rentang_p_max" value="<?= @$main['rentang_p_max'] ?>" required="">
        </div>
      </div>
      <div id="kurang_box" class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Nilai Normal <span class="text-danger">*<span></label>
        <label class="col-lg-1 col-md-3 col-form-label"><b>
            <</b> </label> <div class="col-lg-3 col-md-9">
              <input type="text" class="form-control" name="kurang" value="<?= @$main['kurang'] ?>" required="">
      </div>
    </div>
    <div id="kurang_l_box" class="form-group row">
      <label class="col-lg-4 col-md-4 col-form-label">Nilai Normal (Laki-laki) <span class="text-danger">*<span></label>
      <label class="col-lg-1 col-md-3 col-form-label"><b>
          <</b> </label> <div class="col-lg-3 col-md-9">
            <input type="text" class="form-control" name="kurang_l" value="<?= @$main['kurang_l'] ?>" required="">
    </div>
  </div>
  <div id="kurang_p_box" class="form-group row">
    <label class="col-lg-4 col-md-4 col-form-label">Nilai Normal (Perempuan) <span class="text-danger">*<span></label>
    <label class="col-lg-1 col-md-3 col-form-label"><b>
        <</b> </label> <div class="col-lg-3 col-md-9">
          <input type="text" class="form-control" name="kurang_p" value="<?= @$main['kurang_p'] ?>" required="">
  </div>
  </div>
  </div>
  </div>
  <div id="lebih_box" class="form-group row">
    <label class="col-lg-4 col-md-4 col-form-label">Nilai Normal <span class="text-danger">*<span></label>
    <label class="col-lg-1 col-md-4 col-form-label"><b>></b></label>
    <div class="col-lg-3 col-md-9">
      <input type="text" class="form-control" name="lebih" value="<?= @$main['lebih'] ?>" required="">
    </div>
  </div>
  <div id="lebih_l_box" class="form-group row">
    <label class="col-lg-4 col-md-4 col-form-label">Nilai Normal (Laki-laki) <span class="text-danger">*<span></label>
    <label class="col-lg-1 col-md-4 col-form-label"><b>></b></label>
    <div class="col-lg-3 col-md-9">
      <input type="text" class="form-control" name="lebih_l" value="<?= @$main['lebih_l'] ?>" required="">
    </div>
  </div>
  <div id="lebih_p_box" class="form-group row">
    <label class="col-lg-4 col-md-4 col-form-label">Nilai Normal (Perempuan) <span class="text-danger">*<span></label>
    <label class="col-lg-1 col-md-4 col-form-label"><b>></b></label>
    <div class="col-lg-3 col-md-9">
      <input type="text" class="form-control" name="lebih_p" value="<?= @$main['lebih_p'] ?>" required="">
    </div>
  </div>
  <div id="satuan_box" class="form-group row">
    <label class="col-lg-4 col-md-4 col-form-label">Satuan <span class="text-danger">*<span></label>
    <div class="col-lg-4 col-md-9">
      <input type="text" class="form-control" name="satuan" value="<?= @$main['satuan'] ?>" required="">
    </div>
  </div>
  <div id="informasi_box" class="form-group row">
    <label class="col-lg-4 col-md-4 col-form-label">Nilai Normal <span class="text-danger">*<span></label>
    <div class="col-lg-8 col-md-2">
      <input type="text" class="form-control" name="informasi" value="<?= @$main['informasi'] ?>" required="">
    </div>
  </div>
  <div class="form-group row">
    <label class="col-lg-4 col-md-3 col-form-label">Tagihan?</label>
    <div class="col-lg-4 col-md-5">
      <select class="chosen-select custom-select w-100" id="is_tagihan" name="is_tagihan">
        <option value="1" <?php if (@$main['is_tagihan'] == '1') echo 'selected' ?>>Ya</option>
        <option value="0" <?php if (@$main['is_tagihan'] == '0') echo 'selected' ?>>Tidak</option>
      </select>
    </div>
  </div>
  <div id="tarif_id_box" class="form-group row">
    <label class="col-lg-4 col-md-4 col-form-label">Kesesuaian Tarif</label>
    <div class="col-lg-8 col-md-9">
      <select class="chosen-select custom-select w-100" name="tarif_id">
        <option value="">- None -</option>
        <?php foreach ($list_tarif as $lt) : ?>
          <option value="<?= $lt['tarif_id'] ?>" <?php if ($lt['tarif_id'] == @$main['tarif_id']) echo 'selected' ?>><?= $lt['tarif_id'] ?> - <?= $lt['tarif_nm'] ?></option>
        <?php endforeach; ?>
      </select>
    </div>
  </div>
  </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>
<!-- js -->
<?php $this->load->view('master/itemlab/_js') ?>
<!-- / -->
<script>

</script>