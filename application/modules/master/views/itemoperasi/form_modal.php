<!-- js -->
<?php $this->load->view('master/itemoperasi/_js') ?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?= $form_action ?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Item Induk</label>
        <div class="col-lg-8 col-md-5">
          <select class="chosen-select custom-select w-100" name="parent_id" id="parent_id">
            <option value="">- None -</option>
            <?php foreach ($list_itemoperasi as $li) : ?>
              <option value="<?= $li['itemoperasi_id'] ?>" <?php if ($li['itemoperasi_id'] == @$main['parent_id']) echo 'selected' ?>><?= $li['itemoperasi_id'] ?> - <?= $li['itemoperasi_nm'] ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Kode <span class="text-danger">*<span></label>
        <div class="col-lg-4 col-md-5">
          <input type="text" class="form-control" name="itemoperasi_id" id="itemoperasi_id" value="<?= @$main['itemoperasi_id'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Item Operasi <span class="text-danger">*<span></label>
        <div class="col-lg-7 col-md-9">
          <input type="text" class="form-control" name="itemoperasi_nm" value="<?= @$main['itemoperasi_nm'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Tagihan</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_tagihan">
            <option value="1" <?php if (@$main['is_tagihan'] == '1') echo 'selected' ?>>Ya</option>
            <option value="0" <?php if (@$main['is_tagihan'] == '0') echo 'selected' ?>>Tidak</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Periksa</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_periksa">
            <option value="1" <?php if (@$main['is_periksa'] == '1') echo 'selected' ?>>Ya</option>
            <option value="0" <?php if (@$main['is_periksa'] == '0') echo 'selected' ?>>Tidak</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Kesesuaian Tarif</label>
        <div class="col-lg-8 col-md-9">
          <select class="chosen-select custom-select w-100" name="tarif_id">
            <option value="">- None -</option>
            <?php foreach ($list_tarif as $lt) : ?>
              <option value="<?= $lt['tarif_id'] ?>" <?php if ($lt['tarif_id'] == @$main['tarif_id']) echo 'selected' ?>><?= $lt['tarif_id'] ?> - <?= $lt['tarif_nm'] ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>