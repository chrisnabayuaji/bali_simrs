<!-- js -->
<?php $this->load->view('master/jamgizi/_js') ?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?= $form_action ?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Kode <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="jamgizi_id" id="jamgizi_id" value="<?= @$main['jamgizi_id'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Nama Jam Gizi <span class="text-danger">*</span></label>
        <div class="col-lg-7 col-md-7">
          <input type="text" class="form-control" name="jamgizi_nm" id="jamgizi_nm" value="<?= @$main['jamgizi_nm'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Jam Pemberian <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-7">
          <input type="text" class="form-control timepicker" name="jam_pemberian" id="jam_pemberian" value="<?= @$main['jam_pemberian'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Status</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_active">
            <option value="1" <?php if (@$main['is_active'] == '1') echo 'selected' ?>>Aktif</option>
            <option value="0" <?php if (@$main['is_active'] == '0') echo 'selected' ?>>Tidak Aktif</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>