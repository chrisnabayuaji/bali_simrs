<script type="text/javascript">
$(document).ready(function () {
	$("#form-data").validate( {
    rules: {
      kamar_id:{
        remote: {
          url: '<?=site_url().'/'.$nav['nav_url']?>/ajax/cek_id/<?=@$main['kamar_id']?>',
          type: 'post',
          data: {
            kamar_id: function() {
              return $("#kamar_id").val();
            }
          }
        }
      },
      jml_bed : {
        number : 'digits'
      }
    },
    messages: {
      kamar_id : {
        remote : 'Kode sudah digunakan!'
      }
    },
    errorElement: "em",
    errorPlacement: function (error,element) {
      error.addClass("invalid-feedback");
      if (element.prop("type") === "checkbox") {
        error.insertAfter(element.next("label"));
      } else if($(element).hasClass('chosen-select')){
        error.insertAfter(element.next(".select2-container"));
      }else{
        error.insertAfter(element);
      }
    },
    highlight: function (element,errorClass,validClass) {
      $(element).addClass("is-invalid").removeClass("is-valid");
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).addClass("is-valid").removeClass("is-invalid");
    },
    submitHandler: function (form) {
      $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
      $(".btn-submit").attr("disabled", "disabled");
      $(".btn-cancel").attr("disabled", "disabled");
      form.submit();
    }
  });
  // select2
  $(".chosen-select").select2();
  $('.select2-container').css('width', '100%');
  //

  $('#lokasi_id').bind('change',function(e) {
    e.preventDefault();
    var i = $(this).val();
    _get_kelas(i);
  })

  <?php if (@$id !=''): ?>
    _get_kelas('<?=@$main['lokasi_id']?>', '<?=@$main['kelas_id']?>', '<?=@$main['tarif_id']?>');
  <?php endif; ?>

  function _get_kelas(i, j, k) {
    $.get('<?=site_url('master/kamar/ajax/get_kelas')?>?lokasi_id='+i+'&kelas_id='+j+'&tarif_id='+k,null,function(data) {
        $('#box_kelas').html(data.html);
    },'json');
  }
});
</script>