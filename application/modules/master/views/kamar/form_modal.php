<!-- js -->
<?php $this->load->view('master/kamar/_js') ?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?= $form_action ?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Lokasi</label>
        <div class="col-lg-8 col-md-5">
          <select class="chosen-select custom-select w-100" name="lokasi_id" id="lokasi_id">
            <option value="">- Pilih -</option>
            <?php foreach ($list_lokasi as $ll) : ?>
              <option value="<?= $ll['lokasi_id'] ?>" <?php if ($ll['lokasi_id'] == @$main['lokasi_id']) echo 'selected' ?>><?= $ll['lokasi_id'] ?> - <?= $ll['lokasi_nm'] ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Kelas</label>
        <div class="col-lg-5 col-md-5">
          <div id="box_kelas">
            <select class="chosen-select custom-select w-100" name="kelas_id" id="kelas_id">
              <option value="">- Pilih -</option>
            </select>
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Kode <span class="text-danger">*<span></label>
        <div class="col-lg-5 col-md-5">
          <input type="text" class="form-control" name="kamar_id" id="kamar_id" value="<?= @$main['kamar_id'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Kode BPJS</label>
        <div class="col-lg-5 col-md-5">
          <input type="text" class="form-control" name="bpjs_kd" id="bpjs_kd" value="<?= @$main['bpjs_kd'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">BPJS Tampil?</label>
        <div class="col-lg-3 col-md-5">
          <select class="chosen-select custom-select w-100" name="bpjs_st">
            <option value="1" <?php if (@$main['bpjs_st'] == '1') echo 'selected' ?>>Ya</option>
            <option value="0" <?php if (@$main['bpjs_st'] == '0') echo 'selected' ?>>Tidak</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Nama Kamar <span class="text-danger">*<span></label>
        <div class="col-lg-8 col-md-8">
          <input type="text" class="form-control" name="kamar_nm" value="<?= @$main['kamar_nm'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Jml Bed <span class="text-danger">*<span></label>
        <div class="col-lg-3 col-md-8">
          <input type="text" class="form-control" name="jml_bed" value="<?= @$main['jml_bed'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Jml Bed Kosong<span class="text-danger">*<span></label>
        <div class="col-lg-3 col-md-9">
          <input type="text" class="form-control" name="jml_bed_kosong" value="<?= @$main['jml_bed_kosong'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Jml Bed Terpakai<span class="text-danger">*<span></label>
        <div class="col-lg-3 col-md-9">
          <input type="text" class="form-control" name="jml_bed_terpakai" value="<?= @$main['jml_bed_terpakai'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Tarif</label>
        <div class="col-lg-8 col-md-8">
          <div id="box_tarif">
            <select class="chosen-select custom-select w-100" name="tarif_id" id="tarif_id" required="">
              <option value="">- Pilih -</option>
            </select>
          </div>
          <input type="hidden" name="tarif_nm" id="tarif_nm" value="<?= @$main['tarif_nm'] ?>">
          <input type="hidden" name="nom_tarif" id="nom_tarif" value="<?= @$main['nom_tarif'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Status</label>
        <div class="col-lg-3 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_active">
            <option value="1" <?php if (@$main['is_active'] == '1') echo 'selected' ?>>Aktif</option>
            <option value="0" <?php if (@$main['is_active'] == '0') echo 'selected' ?>>Tidak Aktif</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>