<!-- js -->
<?php $this->load->view('master/kelas/_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Kode <span class="text-danger">*<span></label>
        <div class="col-lg-3 col-md-5">
          <input type="text" class="form-control" name="kelas_id" id="kelas_id" value="<?=@$main['kelas_id']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Nama Kelas <span class="text-danger">*<span></label>
        <div class="col-lg-7 col-md-9">
          <input type="text" class="form-control" name="kelas_nm" value="<?=@$main['kelas_nm']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Singkatan <span class="text-danger">*<span></label>
        <div class="col-lg-4 col-md-9">
          <input type="text" class="form-control" name="kelas_singkatan" value="<?=@$main['kelas_singkatan']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Keterangan</label>
        <div class="col-lg-7 col-md-9">
          <input type="text" class="form-control" name="kelas_desc" value="<?=@$main['kelas_desc']?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Status</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_active">
            <option value="1" <?php if(@$main['is_active'] == '1') echo 'selected'?>>Aktif</option>
            <option value="0" <?php if(@$main['is_active'] == '0') echo 'selected'?>>Tidak Aktif</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>