<!-- js -->
<?php $this->load->view('master/kelas_lokasi/_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Lokasi</label>
        <div class="col-lg-9 col-md-6">
          <select class="chosen-select custom-select w-100" name="lokasi_id" id="lokasi_id">
            <option value="">- Pilih -</option>
            <?php foreach ($list_lokasi as $row): ?>
              <option value="<?=$row['lokasi_id']?>" <?php if($row['lokasi_id'] == @$main['lokasi_id']) echo 'selected'?>><?=$row['lokasi_id'].' - '.$row['lokasi_nm']?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Kelas</label>
        <div class="col-lg-6 col-md-6">
          <select class="chosen-select custom-select w-100" name="kelas_id" id="kelas_id">
            <option value="">- Pilih -</option>
            <?php foreach ($list_kelas as $row): ?>
              <option value="<?=$row['kelas_id']?>" <?php if($row['kelas_id'] == @$main['kelas_id']) echo 'selected'?>><?=$row['kelas_id'].' - '.$row['kelas_nm'].' ['.$row['kelas_singkatan'].']'?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>