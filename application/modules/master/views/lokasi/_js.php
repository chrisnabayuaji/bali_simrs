<script type="text/javascript">
$(document).ready(function () {
	// validate
	$("#form-data").validate( {
    rules: {
      lokasi_id:{
        remote: {
          url: '<?=site_url().'/'.$nav['nav_url']?>/ajax/cek_id/<?=@$main['lokasi_id']?>',
          type: 'post',
          data: {
            lokasi_id: function() {
              return $("#lokasi_id").val();
            }
          }
        }
      },
    },
    messages: {
      lokasi_id : {
        remote : 'Kode sudah digunakan!'
      }
    },
    errorElement: "em",
    errorPlacement: function (error,element) {
      error.addClass("invalid-feedback");
      if (element.prop("type") === "checkbox") {
        error.insertAfter(element.next("label"));
      } else if($(element).hasClass('chosen-select')){
        error.insertAfter(element.next(".select2-container"));
      }else{
        error.insertAfter(element);
      }
    },
    highlight: function (element,errorClass,validClass) {
      $(element).addClass("is-invalid").removeClass("is-valid");
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).addClass("is-valid").removeClass("is-invalid");
    },
    submitHandler: function (form) {
      $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
      $(".btn-submit").attr("disabled", "disabled");
      $(".btn-cancel").attr("disabled", "disabled");
      form.submit();
    }
  });
  // select2
  $(".chosen-select").select2();
  $('.select2-container').css('width', '100%');
  //
	$('#parent_id').bind('change',function(e) {
		e.preventDefault();
		var i = $(this).val();
		$.get('<?=site_url("master/lokasi/ajax/get_lokasi_id")?>?parent_id='+i,null,function(data) {
			$('#lokasi_id').val(data.result);
		},'json');
	});

  show_antrian_cd();
});

function show_antrian_cd() {
  var val = $("#is_loket_antrian").val();
  console.log(val);
  if (val == 1) {
    $(".antrian_cd_container").show();
  }else{
    $(".antrian_cd_container").hide();
  }
}
</script>