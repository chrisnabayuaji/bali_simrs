<!-- js -->
<?php $this->load->view('master/lokasi/_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Item Induk</label>
        <div class="col-lg-8 col-md-5">
          <select class="chosen-select custom-select w-100" name="parent_id" id="parent_id">
            <option value="">- None -</option>
            <?php foreach($list_lokasi as $ll):?>
            <option value="<?=$ll['lokasi_id']?>" <?php if($ll['lokasi_id'] == @$main['parent_id']) echo 'selected'?>><?=$ll['lokasi_id']?> - <?=$ll['lokasi_nm']?></option>
            <?php endforeach;?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Kode <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-5">
          <input type="text" class="form-control" name="lokasi_id" id="lokasi_id" value="<?=@$main['lokasi_id']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Nama Lokasi <span class="text-danger">*</span></label>
        <div class="col-lg-7 col-md-9">
          <input type="text" class="form-control" name="lokasi_nm" value="<?=@$main['lokasi_nm']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Kode Poli BPJS</label>
        <div class="col-lg-7 col-md-5">
          <select class="form-control chosen-select" name="kd_poli_bpjs" id="kd_poli_bpjs">
            <option value=""> --- </option>
            <?php foreach($list_bpjs_poliklinik as $r): ?>
              <option value="<?=$r['poli_kd']?>" <?=(@$main['kd_poli_bpjs'] == $r['poli_kd']) ? 'selected' : '';?>><?=$r['poli_kd'].' - '.$r['poli_nm']?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Eksekutif ?</label>
        <div class="col-lg-3 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_eksekutif">
            <option value="0" <?php if(@$main['is_eksekutif'] == '0') echo 'selected'?>>Tidak</option>
            <option value="1" <?php if(@$main['is_eksekutif'] == '1') echo 'selected'?>>Iya</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Jenis Registrasi</label>
        <div class="col-lg-3 col-md-5">
          <select class="chosen-select custom-select w-100" name="jenisreg_st">
            <option value="0" <?php if(@$main['jenisreg_st'] == '0') echo 'selected'?>>Tidak</option>
            <option value="1" <?php if(@$main['jenisreg_st'] == '1') echo 'selected'?>>Rawat Jalan</option>
            <option value="2" <?php if(@$main['jenisreg_st'] == '2') echo 'selected'?>>Rawat Inap</option>
            <option value="3" <?php if(@$main['jenisreg_st'] == '3') echo 'selected'?>>Gawat Darurat</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Untuk Pendaftaran Online ?</label>
        <div class="col-lg-3 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_reg_online">
            <option value="0" <?php if(@$main['is_reg_online'] == '0') echo 'selected'?>>Tidak</option>
            <option value="1" <?php if(@$main['is_reg_online'] == '1') echo 'selected'?>>Iya</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Ada Bed ?</label>
        <div class="col-lg-3 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_bed">
            <option value="0" <?php if(@$main['is_bed'] == '0') echo 'selected'?>>Tidak</option>
            <option value="1" <?php if(@$main['is_bed'] == '1') echo 'selected'?>>Ada</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Ada Antrian</label>
        <div class="col-lg-3 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_loket_antrian" id="is_loket_antrian" onchange="show_antrian_cd()">
            <option value="0" <?php if(@$main['is_loket_antrian'] == '0') echo 'selected'?>>Tidak</option>
            <option value="1" <?php if(@$main['is_loket_antrian'] == '1') echo 'selected'?>>Ada</option>
          </select>
        </div>
      </div>
      <div class="form-group row antrian_cd_container">
        <label class="col-lg-3 col-md-3 col-form-label">Kode Antrian</label>
        <div class="col-lg-3 col-md-9">
          <input type="text" class="form-control" name="antrian_cd" value="<?=@$main['antrian_cd']?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Ada Jadwal Dokter</label>
        <div class="col-lg-3 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_jadwal_dokter" id="is_jadwal_dokter" onchange="show_antrian_cd()">
            <option value="0" <?php if(@$main['is_jadwal_dokter'] == '0') echo 'selected'?>>Tidak</option>
            <option value="1" <?php if(@$main['is_jadwal_dokter'] == '1') echo 'selected'?>>Ada</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Sebagai Depo Farmasi</label>
        <div class="col-lg-3 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_depo_farmasi" id="is_depo_farmasi" onchange="show_antrian_cd()">
            <option value="0" <?php if(@$main['is_depo_farmasi'] == '0') echo 'selected'?>>Tidak</option>
            <option value="1" <?php if(@$main['is_depo_farmasi'] == '1') echo 'selected'?>>Ya</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Lokasi Depo Farmasi</label>
        <div class="col-lg-8 col-md-5">
          <select class="chosen-select custom-select w-100" name="map_lokasi_depo" id="map_lokasi_depo">
            <option value="">- None -</option>
            <?php foreach($list_depo_farmasi as $ldf):?>
            <option value="<?=$ldf['lokasi_id']?>" <?php if($ldf['lokasi_id'] == @$main['map_lokasi_depo']) echo 'selected'?>><?=$ldf['lokasi_id']?> - <?=$ldf['lokasi_nm']?></option>
            <?php endforeach;?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Sebagai Kasir</label>
        <div class="col-lg-3 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_kasir" id="is_kasir" onchange="show_antrian_cd()">
            <option value="0" <?php if(@$main['is_kasir'] == '0') echo 'selected'?>>Tidak</option>
            <option value="1" <?php if(@$main['is_kasir'] == '1') echo 'selected'?>>Ya</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Lokasi Kasir</label>
        <div class="col-lg-8 col-md-5">
          <select class="chosen-select custom-select w-100" name="map_lokasi_kasir" id="map_lokasi_kasir">
            <option value="">- None -</option>
            <?php foreach($list_kasir as $lk):?>
            <option value="<?=$lk['lokasi_id']?>" <?php if($lk['lokasi_id'] == @$main['map_lokasi_kasir']) echo 'selected'?>><?=$lk['lokasi_id']?> - <?=$lk['lokasi_nm']?></option>
            <?php endforeach;?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Sebagai Depo Kerumahtanggaan</label>
        <div class="col-lg-3 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_depo_kerumahtanggaan" id="is_depo_kerumahtanggaan" onchange="show_antrian_cd()">
            <option value="0" <?php if(@$main['is_depo_kerumahtanggaan'] == '0') echo 'selected'?>>Tidak</option>
            <option value="1" <?php if(@$main['is_depo_kerumahtanggaan'] == '1') echo 'selected'?>>Ya</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Status</label>
        <div class="col-lg-3 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_active">
            <option value="1" <?php if(@$main['is_active'] == '1') echo 'selected'?>>Aktif</option>
            <option value="0" <?php if(@$main['is_active'] == '0') echo 'selected'?>>Tidak Aktif</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-9 offset-md-9">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>