<script type="text/javascript">
  $(document).ready(function() {
    $("#form-data").validate({
      rules: {
        obat_id: {
          remote: {
            url: '<?= site_url() . '/' . $nav['nav_url'] ?>/ajax/cek_id/<?= @$main['obat_id'] ?>',
            type: 'post',
            data: {
              obat_id: function() {
                return $("#obat_id").val();
              }
            }
          }
        },
      },
      messages: {
        obat_id: {
          remote: 'Kode sudah digunakan!'
        }
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');
    //
    $(".autonumeric").autoNumeric({
      aSep: ".",
      aDec: ",",
      vMax: "999999999999999",
      vMin: "0"
    });

    $('#jenisbarang_cd').bind('change', function(e) {
      e.preventDefault();
      var jenisbarang_cd = $('#jenisbarang_cd').val();
      $.get('<?= site_url("master/obat/ajax/get_obat_id") ?>?jenisbarang_cd=' + jenisbarang_cd, null, function(data) {
        $('#obat_id').val(data.result);
      }, 'json');
    });
  })
</script>