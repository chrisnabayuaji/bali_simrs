<!-- js -->
<?php $this->load->view('_js') ?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?= $form_action ?>" autocomplete="off">
  <div class="row">
    <div class="col">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Jenis Barang <span class="text-danger">*<span></label>
        <div class="col-lg-3 col-md-4">
          <select class="form-control chosen-select" name="jenisbarang_cd" id="jenisbarang_cd">
            <option value="">- Pilih -</option>
            <?php foreach (get_parameter('jenisbarang_cd') as $r) : ?>
              <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['jenisbarang_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Kode <span class="text-danger">*<span></label>
        <div class="col-lg-3 col-md-4">
          <input type="text" class="form-control" name="obat_id" id="obat_id" value="<?= @$main['obat_id'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Nama Obat <span class="text-danger">*<span></label>
        <div class="col-lg-9 col-md-9">
          <input type="text" class="form-control" name="obat_nm" value="<?= @$main['obat_nm'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">No Batch</label>
        <div class="col-lg-4 col-md-4">
          <input type="text" class="form-control" name="no_batch" value="<?= @$main['no_batch'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Harga Beli</label>
        <div class="col-lg-3 col-md-4">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text text-small">Rp</span>
            </div>
            <input type="text" class="form-control text-right autonumeric" name="harga_beli" value="<?= num_id(@$main['harga_beli']) ?>">
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Harga Jual</label>
        <div class="col-lg-3 col-md-4">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text text-small">Rp</span>
            </div>
            <input type="text" class="form-control text-right autonumeric" name="harga_jual" value="<?= num_id(@$main['harga_jual']) ?>">
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Harga Akhir</label>
        <div class="col-lg-3 col-md-4">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text text-small">Rp</span>
            </div>
            <input type="text" class="form-control text-right autonumeric" name="harga_akhir" value="<?= num_id(@$main['harga_akhir']) ?>">
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Stok Minimal</label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="stok_minimal" value="<?= @$main['stok_minimal'] ?>">
        </div>
      </div>
    </div>
    <div class="col">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Satuan <span class="text-danger">*<span></label>
        <div class="col-lg-4 col-md-4">
          <select class="form-control chosen-select" name="satuan_cd" id="satuan_cd">
            <?php foreach (get_parameter('satuan_cd') as $r) : ?>
              <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['satuan_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Obat Indikator <span class="text-danger">*<span></label>
        <div class="col-lg-3 col-md-3">
          <select class="form-control chosen-select" name="is_obat_indikator" id="is_obat_indikator">
            <option value="0" <?= (@$main['is_obat_indikator'] == 0) ? 'selected' : ''; ?>>Tidak</option>
            <option value="1" <?= (@$main['is_obat_indikator'] == 1) ? 'selected' : ''; ?>>Ya</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Napza <span class="text-danger">*<span></label>
        <div class="col-lg-3 col-md-3">
          <select class="form-control chosen-select" name="is_napza" id="is_napza">
            <option value="0" <?= (@$main['is_napza'] == 0) ? 'selected' : ''; ?>>Tidak</option>
            <option value="1" <?= (@$main['is_napza'] == 1) ? 'selected' : ''; ?>>Ya</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Generik <span class="text-danger">*<span></label>
        <div class="col-lg-3 col-md-3">
          <select class="form-control chosen-select" name="is_generik" id="is_generik">
            <option value="0" <?= (@$main['is_generik'] == 0) ? 'selected' : ''; ?>>Tidak</option>
            <option value="1" <?= (@$main['is_generik'] == 1) ? 'selected' : ''; ?>>Ya</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Antibiotik <span class="text-danger">*<span></label>
        <div class="col-lg-3 col-md-3">
          <select class="form-control chosen-select" name="is_antibiotik" id="is_antibiotik">
            <option value="0" <?= (@$main['is_antibiotik'] == 0) ? 'selected' : ''; ?>>Tidak</option>
            <option value="1" <?= (@$main['is_antibiotik'] == 1) ? 'selected' : ''; ?>>Ya</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Injeksi <span class="text-danger">*<span></label>
        <div class="col-lg-3 col-md-3">
          <select class="form-control chosen-select" name="is_injeksi" id="is_injeksi">
            <option value="0" <?= (@$main['is_injeksi'] == 0) ? 'selected' : ''; ?>>Tidak</option>
            <option value="1" <?= (@$main['is_injeksi'] == 1) ? 'selected' : ''; ?>>Ya</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Keterangan <span class="text-danger">*<span></label>
        <div class="col-lg-9 col-md-9">
          <textarea class="form-control" name="keterangan_obat" rows="4"><?= @$main['keterangan_obat'] ?></textarea>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>