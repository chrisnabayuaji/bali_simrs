<script type="text/javascript">
  $(document).ready(function () {
    var form = $("#form-data").validate( {
      rules: {
        
      },
      messages: {
        
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container"));
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    $('.chosen-select').on('select2:select', function (e) {
      form.element(this);
    });

    $('#wilayah_st').bind('change',function(e) {
      e.preventDefault();
      var i = $(this).val();
      _get_provinsi(i);
    })
    function _get_provinsi(i) {
      $.get('<?=site_url('master/wilayah/ajax/get_provinsi_by_st')?>?wilayah_st='+i,null,function(data) {
          $('#box_wilayah_prop').html(data.html);
      },'json');
    }

    <?php if(@$id != ''):?>
    _get_wilayah('<?=get_wilayah_id(@$main["wilayah_id"], 'provinsi')?>', '<?=get_wilayah_id(@$main["wilayah_id"], 'provinsi')?>', '<?=get_wilayah_id(@$main["wilayah_id"], 'kabupaten')?>', '<?=get_wilayah_id(@$main["wilayah_id"], 'kecamatan')?>', '<?=get_wilayah_id(@$main["wilayah_id"], 'kelurahan')?>');
    <?php endif;?>
    //
    $('#wilayah_prop').bind('change',function(e) {
      e.preventDefault();
      var i = $(this).val();
      _get_wilayah(i);
      $(this).on('select2:select', function (e) {
        form.element(this);
      });
    })
    //
    function _get_wilayah(i, prop='', kab='', kec='', kel='') {
      var ext_var = 'wilayah_prop='+prop;
          ext_var+= '&wilayah_kab='+kab;
          ext_var+= '&wilayah_kec='+kec;
          ext_var+= '&wilayah_kel='+kel;
      $.get('<?=site_url('master/wilayah/ajax/get_wilayah_id_name')?>?wilayah_parent='+i+'&'+ext_var,null,function(data) {
          $('#box_wilayah_kab').html(data.html);
      },'json');
    }
    //
    jenispasien();
  })

  function jenispasien() {
    var val = $("#jenispasien_id").val();
    if (val != '01') {
      $('#label-no-kartu').removeClass('d-none');
      $('#col-no-kartu').removeClass('d-none');
    }else{
      $('#label-no-kartu').addClass('d-none');
      $('#col-no-kartu').addClass('d-none');
    }
  }
</script>