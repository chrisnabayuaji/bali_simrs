<?php $this->load->view('master/pasien/_js'); ?>
<form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?=$form_action?>" class="needs-validation" novalidate autocomplete="off">
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline col-title">
          <div class="col-back">
            <a href="<?=site_url($nav['nav_module'].'/dashboard')?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
          </div>
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            <?=$nav['nav_nm']?>
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?=site_url('app/dashboard')?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?=site_url($module_nav['nav_url'])?>"><i class="fas fa-folder-open"></i> <?=$module_nav['nav_nm']?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-folder-open"></i> Data Induk</a></li>
            <li class="breadcrumb-item"><a href="<?=site_url($nav['nav_url'])?>"><?=$nav['nav_nm']?></a></li>
            <li class="breadcrumb-item active"><span>Index</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>

    <div class="row full-page mt-4">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <h4 class="card-title border-bottom border-2 pb-2 mb-3"><?=$nav['nav_nm']?></h4>
            <div class="row mt-1">
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">No. Rekam Medis <span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-9">
                    <input type="text" class="form-control" name="pasien_id" id="pasien_id" value="<?=@$main['pasien_id']?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Nama Pasien <span class="text-danger">*<span></label>
                  <div class="col-lg-2 col-md-2">
                    <select class="form-control chosen-select" name="sebutan_cd" id="sebutan_cd">
                      <?php foreach(get_parameter('sebutan_cd') as $r): ?>
                        <option value="<?=$r['parameter_cd']?>" <?=(@$main['sebutan_cd'] == $r['parameter_cd']) ? 'selected' : '';?>><?=$r['parameter_cd']?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                  <div class="col-lg-5 col-md-9">
                    <input type="text" class="form-control" name="pasien_nm" id="pasien_nm" value="<?=@$main['pasien_nm']?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">NIK <span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-9">
                    <input type="text" class="form-control" name="nik" id="nik" value="<?=@$main['nik']?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Nama KK</label>
                  <div class="col-lg-5 col-md-9">
                    <input type="text" class="form-control" name="nama_kk" id="nama_kk" value="<?=@$main['nama_kk']?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">No KK</label>
                  <div class="col-lg-4 col-md-9">
                    <input type="text" class="form-control" name="no_kk" id="no_kk" value="<?=@$main['no_kk']?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Alamat <span class="text-danger">*<span></label>
                  <div class="col-lg-8 col-md-9">
                    <input type="text" class="form-control" name="alamat" id="alamat" value="<?=@$main['alamat']?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Kode Pos <span class="text-danger">*<span></label>
                  <div class="col-lg-3 col-md-9">
                    <input type="text" class="form-control" name="kode_pos" id="kode_pos" value="<?=@$main['kode_pos']?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Status Wilayah <span class="text-danger">*<span></label>
                  <div class="col-lg-3 col-md-9">
                    <select class="form-control chosen-select" name="wilayah_st" id="wilayah_st">
                      <option value="D" <?php if('D' == @$main['wilayah_st']) echo 'selected'?>>Dalam Wilayah</option>
                      <option value="L" <?php if('L' == @$main['wilayah_st']) echo 'selected'?>>Luar Wilayah</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Propinsi <span class="text-danger">*<span></label>
                  <div class="col-lg-5 col-md-5">
                    <div id="box_wilayah_prop">
                      <select class="chosen-select custom-select w-100" name="wilayah_prop" id="wilayah_prop" required="">
                        <option value="">- Propinsi -</option>
                        <?php foreach($list_wilayah_prop as $wp):?>
                        <option value="<?=$wp['wilayah_id']?>#<?=$wp['wilayah_nm']?>" <?php if($wp['wilayah_id'] == get_wilayah_id(@$main['wilayah_id'], 'provinsi')) echo 'selected'?>><?=$wp['wilayah_id']?> - <?=$wp['wilayah_nm']?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Kab/Kota <span class="text-danger">*<span></label>
                  <div class="col-lg-5 col-md-5">
                    <div id="box_wilayah_kab">
                      <select class="chosen-select custom-select w-100" name="wilayah_kab" id="wilayah_kab" required="">
                        <option value="">- Kab/Kota -</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Kecamatan <span class="text-danger">*<span></label>
                  <div class="col-lg-5 col-md-5">
                    <div id="box_wilayah_kec">
                      <select class="chosen-select custom-select w-100" name="wilayah_kec" id="wilayah_kec" required="">
                        <option value="">- Kecamatan -</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Desa/Kelurahan <span class="text-danger">*<span></label>
                  <div class="col-lg-5 col-md-5">
                    <div id="box_wilayah_kel">
                      <select class="chosen-select custom-select w-100" name="wilayah_kel" id="wilayah_kel" required="">
                        <option value="">- Desa/Kelurahan -</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Tempat Tgl Lahir <span class="text-danger">*<span></label>
                  <div class="col-lg-3 col-md-9">
                    <input type="text" class="form-control" name="tmp_lahir" id="tmp_lahir" value="<?=@$main['tmp_lahir']?>" required>
                  </div>
                  <div class="col-lg-3 col-md-9">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datepicker text-center" name="tgl_lahir" value="<?=to_date(@$main['tgl_lahir'])?>" required="">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Jenis Kelamin <span class="text-danger">*<span></label>
                  <div class="col-lg-3 col-md-9">
                    <select class="form-control chosen-select" name="sex_cd" id="sex_cd">
                      <?php foreach(get_parameter('sex_cd') as $r): ?>
                        <option value="<?=$r['parameter_cd']?>" <?php if($r['parameter_cd'] == @$main['sex_cd']) echo 'selected'?>><?=$r['parameter_val']?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                  <label class="col-lg-3 col-md-3 col-form-label">Golongan Darah <span class="text-danger">*<span></label>
                  <div class="col-lg-2 col-md-9">
                    <select class="form-control chosen-select" name="goldarah_cd" id="goldarah_cd">
                      <option value="">---</option>
                      <?php foreach(get_parameter('goldarah_cd') as $r): ?>
                        <option value="<?=$r['parameter_cd']?>" <?php if($r['parameter_cd'] == @$main['goldarah_cd']) echo 'selected'?>><?=$r['parameter_val']?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Pendidikan Terakhir <span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-9">
                    <select class="form-control chosen-select" name="pendidikan_cd" id="pendidikan_cd">
                      <?php foreach(get_parameter('pendidikan_cd') as $r): ?>
                        <option value="<?=$r['parameter_cd']?>" <?php if($r['parameter_cd'] == @$main['pendidikan_cd']) echo 'selected'?>><?=$r['parameter_val']?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Pekerjaan <span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-9">
                    <select class="form-control chosen-select" name="pekerjaan_cd" id="pekerjaan_cd">
                      <?php foreach(get_parameter('pekerjaan_cd') as $r): ?>
                        <option value="<?=$r['parameter_cd']?>" <?php if($r['parameter_cd'] == @$main['pekerjaan_cd']) echo 'selected'?>><?=$r['parameter_val']?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Agama <span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-9">
                    <select class="form-control chosen-select" name="agama_cd" id="agama_cd">
                      <?php foreach(get_parameter('agama_cd') as $r): ?>
                        <option value="<?=$r['parameter_cd']?>" <?php if($r['parameter_cd'] == @$main['agama_cd']) echo 'selected'?>><?=$r['parameter_val']?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">No. Telp <span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-9">
                    <input type="text" class="form-control" name="telp" id="telp" value="<?=@$main['telp']?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Jenis Pasien <span class="text-danger">*<span></label>
                  <div class="col-lg-3 col-md-9">
                    <select class="form-control chosen-select" name="jenispasien_id" id="jenispasien_id" onchange="jenispasien()">
                      <?php foreach($jenis_pasien as $r): ?>
                        <option value="<?=$r['jenispasien_id']?>" <?php if($r['jenispasien_id'] == @$main['jenispasien_id']) echo 'selected'?>><?=$r['jenispasien_nm']?></option>
                      <?php endforeach;?>
                    </select>
                  </div>
                  <label class="col-lg-2 col-md-3 col-form-label d-none" id="label-no-kartu">No.Kartu <span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-9 d-none" id="col-no-kartu">
                    <input type="text" class="form-control" name="no_kartu" id="no_kartu" value="<?=@$main['no_kartu']?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Status Pasien <span class="text-danger">*<span></label>
                  <div class="col-lg-3 col-md-9">
                    <select class="form-control chosen-select" name="pasien_st" id="pasien_st">
                      <option value="1" <?php if('1' == @$main['pasien_st']) echo 'selected'?>>Aktif</option>
                      <option value="0" <?php if('0' == @$main['pasien_st']) echo 'selected'?>>Tidak Aktif</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Tgl.Catat</label>
                  <div class="col-lg-3 col-md-9">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datepicker text-center" name="tgl_catat" value="<?=to_date(@$main['tgl_catat'])?>" required="">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Keterangan</label>
                  <div class="col-lg-6 col-md-9">
                    <textarea class="form-control" rows="5" name="keterangan"><?=@$main['keterangan']?></textarea>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="btn-form">
    <div class="btn-form-action">
      <div class="btn-form-action-bottom w-100 small-text clearfix">
        <div class="col-lg-10 col-md-8 col-4">
          <a href="<?=site_url().'/'.$nav['nav_url']?>" class="btn btn-xs btn-success"><i class="fas fa-arrow-left"></i> Kembali</a>
        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-clear">
          <button type="reset" onClick="window.location.reload();" class="btn btn-xs btn-secondary btn-cancel"><i class="fas fa-sync-alt"></i> Reset</button>
        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-save">
          <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>