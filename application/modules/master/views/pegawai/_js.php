<script type="text/javascript">
$(function() {
  $(document).ready(function () {
    $("#form-data").validate( {
      rules: {
        pegawai_id:{
          remote: {
            url: '<?=site_url().'/'.$nav['nav_url']?>/ajax/cek_id/<?=@$main['pegawai_id']?>',
            type: 'post',
            data: {
              pegawai_id: function() {
                return $("#pegawai_id").val();
              }
            }
          }
        },
      },
      messages: {
        pegawai_id : {
          remote : 'Kode sudah digunakan!'
        }
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container"));
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    <?php if (@$main['tgl_lahir'] == '0000-00-00' || @$main['tgl_lahir'] == null || @$main['tgl_lahir'] == ''): ?>
      $("#tgl_lahir").val("");
    <?php endif; ?>
  })
  // select2
  $(".chosen-select").select2();
  $('.select2-container').css('width', '100%');
  //datepicker
  $('.datepicker').daterangepicker({
    singleDatePicker: true,
    showDropdowns: true,
    locale: {
      cancelLabel: 'Clear',
      format: 'DD-MM-YYYY'
    },
    isInvalidDate: function(date) {
      return '';
    }
  })
  //
	$('#jenispegawai_cd').bind('change',function(e) {
    e.preventDefault();
    var jenispegawai_cd = $('#jenispegawai_cd').val();
    $.get('<?=site_url("master/pegawai/ajax/get_pegawai_id")?>?jenispegawai_cd='+jenispegawai_cd,null,function(data) {
        $('#pegawai_id').val(data.result);
    },'json');
  });
  //
  <?php if(@$main['pegawai_id'] != '' && @$main['wilayah_prop'] !=''):?>
  _get_wilayah('<?=@$main["wilayah_prop"]?>', '<?=@$main["wilayah_prop"]?>', '<?=@$main["wilayah_kab"]?>', '<?=@$main["wilayah_kec"]?>', '<?=@$main["wilayah_kel"]?>');
  <?php endif;?>
  //
  $('#wilayah_prop').bind('change',function(e) {
    e.preventDefault();
    var i = $(this).val();
    _get_wilayah(i);
  })
  //
  function _get_wilayah(i, prop='', kab='', kec='', kel='') {
    var ext_var = 'wilayah_prop='+prop;
        ext_var+= '&wilayah_kab='+kab;
        ext_var+= '&wilayah_kec='+kec;
        ext_var+= '&wilayah_kel='+kel;
    $.get('<?=site_url('master/wilayah/ajax/get_wilayah_child')?>?wilayah_parent='+i+'&'+ext_var,null,function(data) {
        $('#box_wilayah_kab').html(data.html);
    },'json');
  }
});
</script>