<!-- js -->
<?php $this->load->view('master/penyakit/_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Kode <span class="text-danger">*</span></label>
        <div class="col-lg-4 col-md-5">
          <input type="text" class="form-control" name="penyakit_id" id="penyakit_id" value="<?=@$main['penyakit_id']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Nama Penyakit <span class="text-danger">*</span></label>
        <div class="col-lg-9 col-md-9">
          <input type="text" class="form-control" name="penyakit_nm" value="<?=@$main['penyakit_nm']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">ICDX <span class="text-danger">*</span></label>
        <div class="col-lg-4 col-md-5">
          <input type="text" class="form-control" name="icdx" value="<?=@$main['icdx']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">LB 1</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="lb1">
            <option value="1" <?php if(@$main['lb1'] == '1') echo 'selected'?>>Ya</option>
            <option value="0" <?php if(@$main['lb1'] == '0') echo 'selected'?>>Tidak</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">RL</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="rl">
            <option value="1" <?php if(@$main['rl'] == '1') echo 'selected'?>>Ya</option>
            <option value="0" <?php if(@$main['rl'] == '0') echo 'selected'?>>Tidak</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Menular</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="menular">
            <option value="0" <?php if(@$main['menular'] == '0') echo 'selected'?>>Tidak</option>
            <option value="1" <?php if(@$main['menular'] == '1') echo 'selected'?>>Ya</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Kelompok Penyakit</label>
        <div class="col-lg-8 col-md-8">
          <select class="chosen-select custom-select w-100" name="kelompokpenyakit_id">
            <option value="">-- Pilih --</option>
            <?php foreach($kelompok_penyakit as $r):?>
              <option value="<?=$r['kelompokpenyakit_id']?>" <?php if(@$main['kelompokpenyakit_id'] == $r['kelompokpenyakit_id'])echo 'selected';?>><?=$r['kelompokpenyakit_nm']?></option>              
            <?php endforeach;?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Status</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_active">
            <option value="1" <?php if(@$main['is_active'] == '1') echo 'selected'?>>Aktif</option>
            <option value="0" <?php if(@$main['is_active'] == '0') echo 'selected'?>>Tidak Aktif</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="col-5 offset-md-7">
        <div class="float-right">
          <button type="button" class="btn btn-xs btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
          <button type="submit" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>