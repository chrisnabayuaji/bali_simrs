<!-- js -->
<?php $this->load->view('master/rsrujukan/_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Kode <span class="text-danger">*<span></label>
        <div class="col-lg-3 col-md-5">
          <input type="text" class="form-control" name="rsrujukan_id" id="rsrujukan_id" value="<?=@$main['rsrujukan_id']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Nama RS Rujukan <span class="text-danger">*<span></label>
        <div class="col-lg-7 col-md-9">
          <input type="text" class="form-control" name="rsrujukan_nm" value="<?=@$main['rsrujukan_nm']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Alamat <span class="text-danger">*<span></label>
        <div class="col-lg-8 col-md-8">
          <input type="text" class="form-control" name="rsrujukan_alamat" value="<?=@$main['rsrujukan_alamat']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Status</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="wilayah_st">
            <option value="D" <?php if(@$main['wilayah_st'] == 'D') echo 'selected'?>>Dalam Wilayah</option>
            <option value="L" <?php if(@$main['wilayah_st'] == 'L') echo 'selected'?>>Luar Wilayah</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>