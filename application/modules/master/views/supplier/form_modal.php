<!-- js -->
<?php $this->load->view('master/supplier/_js') ?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?= $form_action ?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Kode <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="supplier_id" id="supplier_id" value="<?= @$main['supplier_id'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Nama Supplier <span class="text-danger">*</span></label>
        <div class="col-lg-7 col-md-7">
          <input type="text" class="form-control" name="supplier_nm" id="supplier_nm" value="<?= @$main['supplier_nm'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Bentuk Usaha</label>
        <div class="col-lg-3 col-md-7">
          <input type="text" class="form-control" name="bentuk_usaha" id="bentuk_usaha" value="<?= @$main['bentuk_usaha'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">No. Telepon</label>
        <div class="col-lg-4 col-md-7">
          <input type="text" class="form-control" name="telp" id="telp" value="<?= @$main['telp'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Alamat</label>
        <div class="col-lg-9 col-md-7">
          <input type="text" class="form-control" name="alamat" id="alamat" value="<?= @$main['alamat'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Kabupaten</label>
        <div class="col-lg-4 col-md-7">
          <input type="text" class="form-control" name="kabupaten" id="kabupaten" value="<?= @$main['kabupaten'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Nama Bank</label>
        <div class="col-lg-6 col-md-7">
          <input type="text" class="form-control" name="bank_nm" id="bank_nm" value="<?= @$main['bank_nm'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Rekening Bank</label>
        <div class="col-lg-6 col-md-7">
          <input type="text" class="form-control" name="bank_rek" id="bank_rek" value="<?= @$main['bank_rek'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Nama Pemilik</label>
        <div class="col-lg-6 col-md-7">
          <input type="text" class="form-control" name="pemilik_nm" id="pemilik_nm" value="<?= @$main['pemilik_nm'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">NPWP Pemilik</label>
        <div class="col-lg-6 col-md-7">
          <input type="text" class="form-control" name="pemilik_npwp" id="pemilik_npwp" value="<?= @$main['pemilik_npwp'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Sebagai Supplier Farmasi</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_supplier_farmasi">
            <option value="0" <?php if (@$main['is_supplier_farmasi'] == '0') echo 'selected' ?>>Tidak</option>
            <option value="1" <?php if (@$main['is_supplier_farmasi'] == '1') echo 'selected' ?>>Ya</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Sebagai Supplier Kerumahtanggaan</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_supplier_rumahtangga">
            <option value="0" <?php if (@$main['is_supplier_rumahtangga'] == '0') echo 'selected' ?>>Tidak</option>
            <option value="1" <?php if (@$main['is_supplier_rumahtangga'] == '1') echo 'selected' ?>>Ya</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Status</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_active">
            <option value="1" <?php if (@$main['is_active'] == '1') echo 'selected' ?>>Aktif</option>
            <option value="0" <?php if (@$main['is_active'] == '0') echo 'selected' ?>>Tidak Aktif</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>