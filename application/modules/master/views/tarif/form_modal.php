<!-- js -->
<?php $this->load->view('master/tarif/_js') ?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?= $form_action ?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Tarif Induk</label>
        <div class="col-lg-8 col-md-5">
          <select class="chosen-select custom-select w-100" name="parent_id" id="parent_id">
            <option value="">None</option>
            <?php foreach ($list_tarif as $lt) : ?>
              <option value="<?= $lt['tarif_id'] ?>" <?php if ($lt['tarif_id'] == @$main['parent_id']) echo 'selected' ?>><?= $lt['tarif_id'] ?> - <?= $lt['tarif_nm'] ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Kode <span class="text-danger">*<span></label>
        <div class="col-lg-4 col-md-5">
          <input type="text" class="form-control" name="tarif_id" id="tarif_id" value="<?= @$main['tarif_id'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Invoice Order</label>
        <div class="col-lg-3 col-md-5">
          <input type="text" class="form-control" name="invoice_order" id="invoice_order" value="<?= @$main['invoice_order'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Nama Tarif <span class="text-danger">*<span></label>
        <div class="col-lg-9 col-md-9">
          <input type="text" class="form-control" name="tarif_nm" value="<?= @$main['tarif_nm'] ?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Tipe</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="tarif_tp">
            <option value="G" <?php if (@$main['tarif_tp'] == 'G') echo 'selected' ?>>Group</option>
            <option value="D" <?php if (@$main['tarif_tp'] == 'D') echo 'selected' ?>>Detail</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Akun</label>
        <div class="col-lg-8 col-md-8 mt-1">
          <select class="chosen-select custom-select w-100" name="akun_id">
            <option value="">None</option>
            <?php foreach ($list_akun as $row) : ?>
              <option value="<?= $row['akun_id'] ?>" <?php if (@$main['akun_id'] == $row['akun_id']) echo 'selected' ?>><?= $row['akun_id'] ?> - <?= $row['akun_nm'] ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Muncul Di Kwitansi</label>
        <div class="col-lg-4 col-md-5 mt-1">
          <select class="chosen-select custom-select w-100" name="is_kwitansi">
            <option value="0" <?php if (@$main['is_kwitansi'] == '0') echo 'selected' ?>>Tidak</option>
            <option value="1" <?php if (@$main['is_kwitansi'] == '1') echo 'selected' ?>>Ya</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Komponen Biaya Kamar</label>
        <div class="col-lg-4 col-md-5 mt-1">
          <select class="chosen-select custom-select w-100" name="is_data_kamar">
            <option value="0" <?php if (@$main['is_data_kamar'] == '0') echo 'selected' ?>>Tidak</option>
            <option value="1" <?php if (@$main['is_data_kamar'] == '1') echo 'selected' ?>>Ya</option>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Status</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="is_active">
            <option value="1" <?php if (@$main['is_active'] == '1') echo 'selected' ?>>Aktif</option>
            <option value="0" <?php if (@$main['is_active'] == '0') echo 'selected' ?>>Tidak Aktif</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>