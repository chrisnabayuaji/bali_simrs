<script type="text/javascript">
  $(document).ready(function () {
    $("#form-data").validate( {
      rules: {
      },
      messages: {
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container")).addClass('mt-n2 mb-2');
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    $('#kelas_id').bind('change',function(e) {
      e.preventDefault();
      var i = $(this).val();
      _get_tarif(i);
    })

    <?php if (@$main['tarif_id'] !=''): ?>
      _get_tarif('<?=@$main['kelas_id']?>', '<?=@$main['tarif_id']?>');
    <?php endif; ?>

    function _get_tarif(i, j) {
      $.post('<?=site_url('master/tarifadmisi/admisi_dokter/ajax/get_tarif')?>', {kelas_id: i, tarif_id: j},function(data) {
        $('#box_tarif').html(data.html);
      },'json');
    }

  })
</script>