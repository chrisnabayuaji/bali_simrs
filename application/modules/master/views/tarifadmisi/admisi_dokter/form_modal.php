<!-- js -->
<?php $this->load->view('master/tarifadmisi/admisi_dokter/_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Lokasi</label>
        <div class="col-lg-8 col-md-8">
          <select class="chosen-select custom-select w-100" name="lokasi_id" id="lokasi_id" required="">
            <option value="">- Pilih -</option>
            <?php foreach ($lokasi as $row): ?>
              <option value="<?=$row['lokasi_id']?>" <?php if(@$main['lokasi_id'] == $row['lokasi_id']) echo 'selected'?>><?=$row['lokasi_nm']?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Kelas</label>
        <div class="col-lg-6 col-md-6">
          <select class="chosen-select custom-select w-100" name="kelas_id" id="kelas_id" required="">
            <option value="">- Pilih -</option>
            <?php foreach ($kelas as $row): ?>
              <option value="<?=$row['kelas_id']?>" <?php if(@$main['kelas_id'] == $row['kelas_id']) echo 'selected'?>><?=$row['kelas_nm']?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Dokter</label>
        <div class="col-lg-6 col-md-6">
          <select class="chosen-select custom-select w-100" name="dokter_id" id="dokter_id">
            <option value="">---</option>
            <?php foreach ($dokter as $row): ?>
              <option value="<?=$row['pegawai_id']?>" <?php if(@$main['dokter_id'] == $row['pegawai_id']) echo 'selected'?>><?=$row['pegawai_nm']?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Tarif</label>
        <div class="col-lg-8 col-md-8">
          <div id="box_tarif">
            <select class="chosen-select custom-select w-100" name="tarif_id" id="tarif_id">
              <option value="">- Pilih -</option>
            </select>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>