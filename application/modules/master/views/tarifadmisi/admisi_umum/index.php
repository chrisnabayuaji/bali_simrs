<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline col-title">
        <div class="col-back">
          <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
        </div>
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          <?= $nav['nav_nm'] ?>
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item"><a href="#"><i class="fas fa-hospital-alt"></i> Data Dasar RS</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><?= $nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item active"><span>Index</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>
  <!-- flash message -->
  <div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
  <!-- /flash message -->
  <div class="row full-page mt-4 mb-n2">
    <div class="col-lg-12 grid-margin-xl-0 stretch-card">
      <div class="card border-none">
        <div class="card-body">
          <div class="row mt-n3 mb-1">
            <div class="col-md-12 mt-3">
              <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link tab-menu active" href="<?= site_url('master/tarifadmisi/admisi_umum') ?>"><i class="fas fa-users"></i> TARIF ADMISI UMUM</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link tab-menu" href="<?= site_url('master/tarifadmisi/admisi_dokter') ?>"><i class="fas fa-user-md"></i> TARIF ADMISI DOKTER</a>
                </li>
              </ul>
            </div>
          </div>
          <form id="form-search" action="<?= site_url() . '/app/search_redirect/' . $nav['nav_id'] . '.admisi_umum' ?>" method="post" autocomplete="off">
            <input type="hidden" name="redirect" value="master/tarifadmisi/admisi_umum">
            <div class="row">
              <div class="col-md-2">
                <?php if ($nav['_add']) : ?>
                  <a href="#" data-href="<?= site_url('master/tarifadmisi/admisi_umum/form_modal') ?>" modal-title="Tambah Data" modal-size="md" class="btn btn-xs btn-primary modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Tambah Data"><i class="fas fa-plus-circle"></i> Tambah</a>
                <?php endif; ?>
              </div>
              <div class="col-md-3 offset-md-7">
                <div class="input-group">
                  <input type="text" name="term" value="<?= @$cookie['search']['term'] ?>" class="form-control form-control-sm" placeholder="Pencarian...">
                  <div class="input-group-append">
                    <button type="submit" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cari Data"><i class="fas fa-search"></i></button>
                    <a class="btn btn-xs btn-default" href="<?= site_url() . '/app/reset_redirect/' . $nav['nav_id'] . '.admisi_umum/' . str_replace('/', '-', 'master/tarifadmisi/admisi_umum') ?>" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-default" title="" data-original-title="Reset"><i class="fas fa-sync-alt"></i></a>
                  </div>
                </div>
              </div>
            </div><!-- /.row -->
          </form>
          <div class="row mt-2 pagination-info">
            <div class="col-md-6">
              <form id="form-paging" action="<?= site_url() . '/app/per_page/' . $nav['nav_id'] . '.admisi_umum' ?>" method="post">
                <label><i class="mdi mdi-bookmark"></i> Tampilkan</label>
                <select name="per_page" class="select-pagination" onchange="$('#form-paging').submit()">
                  <option value="10" <?php if ($cookie['per_page'] == 10) {
                                        echo 'selected';
                                      } ?>>10</option>
                  <option value="50" <?php if ($cookie['per_page'] == 50) {
                                        echo 'selected';
                                      } ?>>50</option>
                  <option value="100" <?php if ($cookie['per_page'] == 100) {
                                        echo 'selected';
                                      } ?>>100</option>
                </select>
                <label>data per halaman.</label>
              </form>
            </div>
            <div class="col-md-6 text-right">
              <div class="pt-1"><?= @$pagination_info ?></div>
            </div>
          </div><!-- /.row -->
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                <form id="form-multiple" action="<?= site_url() . '/' . $nav['nav_url'] . '/multiple/enable' ?>" method="post">
                  <table class="table table-hover table-striped table-bordered table-fixed">
                    <thead>
                      <tr>
                        <th class="text-center" width="36">No</th>
                        <th class="text-center" width="60">Aksi</th>
                        <th class="text-center">Lokasi</th>
                        <th class="text-center" width="200">Kelas</th>
                        <th class="text-center">Nama Tarif</th>
                        <th class="text-center" width="200">Nominal</th>
                      </tr>
                    </thead>
                    <?php if (@$main == null) : ?>
                      <tbody>
                        <tr>
                          <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                        </tr>
                      </tbody>
                    <?php else : ?>
                      <tbody>
                        <?php $i = 1;
                        foreach ($main as $row) : ?>
                          <tr>
                            <td class="text-center" width="36"><?= $cookie['cur_page'] + ($i++) ?></td>
                            <td class="text-center" width="60">
                              <?php if ($nav['_update'] || $nav['_delete']) : ?>
                                <?php if ($nav['_update']) : ?>
                                  <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/form_modal/' . $row['tarifadmisi_id'] ?>" modal-title="Ubah Data" modal-size="md" class="btn btn-primary btn-table modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Ubah Data"><i class="fas fa-pencil-alt"></i></a>
                                <?php endif; ?>
                                <?php if ($nav['_delete']) : ?>
                                  <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/delete/' . $row['tarifadmisi_id'] ?>" class="btn btn-danger btn-table btn-delete" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-danger" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
                                <?php endif; ?>
                              <?php endif; ?>
                            </td>
                            <td><?= $row['lokasi_id'] . ' - ' . $row['lokasi_nm'] ?></td>
                            <td class="text-center" width="200"><?= $row['kelas_nm'] ?></td>
                            <td><?= $row['tarif_id'] ?> - <?= $row['tarif_nm'] ?></td>
                            <td class="text-right" width="200"><?= num_id($row['nominal']) ?></td>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                    <?php endif; ?>
                  </table>
                </form>
              </div>
            </div>
          </div><!-- /.row -->
        </div>
      </div>
    </div>
  </div>
</div>