<script type="text/javascript">
$(document).ready(function () {
  $("#form-data").validate( {
    rules: {
      
    },
    messages: {
      
    },
    errorElement: "em",
    errorPlacement: function (error,element) {
      error.addClass("invalid-feedback");
      if (element.prop("type") === "checkbox") {
        error.insertAfter(element.next("label"));
      } else if($(element).hasClass('chosen-select')){
        error.insertAfter(element.next(".select2-container"));
      }else{
        error.insertAfter(element);
      }
    },
    highlight: function (element,errorClass,validClass) {
      $(element).addClass("is-invalid").removeClass("is-valid");
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).addClass("is-valid").removeClass("is-invalid");
    },
    submitHandler: function (form) {
      $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
      $(".btn-submit").attr("disabled", "disabled");
      $(".btn-cancel").attr("disabled", "disabled");
      form.submit();
    }
  });
  // select2
  $(".chosen-select").select2();
  $('.select2-container').css('width', '100%');
  //
	$('.js,.jp,.jb').bind('keyup',function() {
    $(this).each(function() {
      var n = $(this).attr('data-no');
      var js = ($('#js_'+n).val() != '' ? $('#js_'+n).val().replace(/\./g, '') : '0');
      var jp = ($('#jp_'+n).val() != '' ? $('#jp_'+n).val().replace(/\./g, '') : '0');
      var jb = ($('#jb_'+n).val() != '' ? $('#jb_'+n).val().replace(/\./g, '') : '0');
      var nominal = parseFloat(js) + parseFloat(jp) + parseFloat(jb);
      $('#nominal_'+n).autoNumericSet(nominal);
    });
  });
  //
  var i = '<?=@$id?>';
  // loading
  $('#table-kelas').html('<tr><td class="text-center" colspan="99"><i class="fas fa-2x fa-spin fa-spinner"></i><br>Loading</td></tr>');
  $.get('<?=site_url("master/tarifkelas/ajax/list_tarif_kelas_id")?>?tarif_id='+i,null,function(data) {
    $('#table-kelas').html(data.html);
  },'json');
  //
  $('#tarif_id').bind('change',function(e) {
    e.preventDefault();
    var i = $(this).val();
    // loading
    $('#table-kelas').html('<tr><td class="text-center" colspan="99"><i class="fas fa-2x fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.get('<?=site_url("master/tarifkelas/ajax/list_tarif_kelas_id")?>?tarif_id='+i,null,function(data) {
      $('#table-kelas').html(data.html);
    },'json');
  });
});
</script>