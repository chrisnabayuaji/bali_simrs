<?php $i=1;foreach($list_kelas as $lk):?>
<tr>
  <td class="text-center">
    <?=$i++?>
    <input type="hidden" name="kelas_id[]" value="<?=@$lk['kelas_id']?>">
    <input type="hidden" name="tarifkelas_id[]" value="<?=@$lk['tarifkelas_id']?>">
  </td>
  <td><?=$lk['kelas_nm']?></td>
  <td><input type="text" name="js[]" class="js form-control input-sm autonumeric text-right" id="js_<?=$i?>" data-no="<?=$i?>" value="<?=(@$lk['js'] != '' ? num_id($lk['js']) : '')?>"></td>
  <td><input type="text" name="jp[]" class="jp form-control input-sm autonumeric text-right" id="jp_<?=$i?>" data-no="<?=$i?>" value="<?=(@$lk['jp'] != '' ? num_id($lk['jp']) : '')?>"></td>
  <td><input type="text" name="jb[]" class="jb form-control input-sm autonumeric text-right" id="jb_<?=$i?>" data-no="<?=$i?>" value="<?=(@$lk['jb'] != '' ? num_id($lk['jb']) : '')?>"></td>
  <td><input type="text" name="nominal[]" class="nominal form-control input-sm autonumeric text-right" id="nominal_<?=$i?>" data-no="<?=$i?>" value="<?=(@$lk['nominal'] != '' ? num_id($lk['nominal']) : '')?>"></td>
  <td class="text-center">
    <?php if(@$lk['tarifkelas_id'] != ''):?>
    <a href="#" data-tarif_id="<?=$tarif_id?>" data-tarifkelas_id="<?=$lk['tarifkelas_id']?>" class="btn btn-xs btn-danger btn-table btn-delete" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-danger" title="Hapus Data"><i class="far fa-trash-alt"></i> Hapus</a>
    <?php endif;?>
  </td>
</tr>
<?php endforeach;?>

<script type="text/javascript">
$(document).ready(function () {
  $('.btn-delete').on('click', function (e) {
    e.preventDefault();

    const tarif_id = $(this).data('tarif_id');
    const tarifkelas_id = $(this).data('tarifkelas_id');

    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#eb3b5a',
      cancelButtonColor: '#b2bec3',
      confirmButtonText: 'Hapus',
      cancelButtonText: 'Batal',
      customClass: 'swal-wide'
    }).then((result) => {
      if (result.value) {
        DeleteItem(tarif_id, tarifkelas_id);
      }
    })
  })
  //
  $('.js,.jp,.jb').bind('keyup',function() {
    $(this).each(function() {
      var n = $(this).attr('data-no');
      var js = ($('#js_'+n).val() != '' ? $('#js_'+n).val().replace(/\./g, '') : '0');
      var jp = ($('#jp_'+n).val() != '' ? $('#jp_'+n).val().replace(/\./g, '') : '0');
      var jb = ($('#jb_'+n).val() != '' ? $('#jb_'+n).val().replace(/\./g, '') : '0');
      var nominal = parseFloat(js) + parseFloat(jp) + parseFloat(jb);
      $('#nominal_'+n).autoNumericSet(nominal);
    });
  });
  //
  $(".autonumeric").autoNumeric({ aSep: ".", aDec: ",", vMax: "999999999999999", vMin: "0" });
  // delete item
  function DeleteItem(tarif_id, tarifkelas_id) {
    $('#table-kelas').html('<tr><td class="text-center" colspan="99"><i class="fas fa-2x fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.get('<?=site_url("master/tarifkelas/ajax/delete_item_tarifkelas")?>?tarif_id='+tarif_id+'&tarifkelas_id='+tarifkelas_id,null,function(data) {
    $('#table-kelas').html(data.html);
    $.toast({
      heading: 'Sukses',
      text: 'Data berhasil dihapus.',
      icon: 'success',
      position: 'top-right'
    })
  },'json');
  }
});
</script>