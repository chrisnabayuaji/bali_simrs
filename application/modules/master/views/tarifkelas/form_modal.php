<!-- js -->
<?php $this->load->view('master/tarifkelas/_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <input type="hidden" name="tarif_id" value="<?=@$main['tarif_id']?>">
      <div class="form-group row">
        <label class="col-lg-2 col-md-3 col-form-label">Tarif</label>
        <div class="col-lg-5 col-md-5">
          <select class="chosen-select custom-select w-100" name="tarif_id" id="tarif_id" required="">
            <?php if (@$id == ''): ?>
              <option value="">- Pilih -</option>
            <?php endif; ?>
            <?php foreach($list_tarif as $lt):?>
            <option value="<?=$lt['tarif_id']?>" <?php if($lt['tarif_id'] == @$main['tarif_id']) echo 'selected'?>><?=$lt['tarif_id']?> - <?=$lt['tarif_nm']?></option>
            <?php endforeach;?>
          </select>
        </div>
      </div>
      <div class="form-group" style="margin-top: 20px;">
        <div class="table-responsive">
          <table class="table table-hover table-striped table-bordered">
            <thead>
              <tr>
                <th width="2%" rowspan="2" class="text-center align-middle">No</th>
                <th rowspan="2" class="text-center align-middle">Kelas</th>
                <th colspan="3" class="text-center">Nominal Tarif (Rp)</th>
                <th width="15%" rowspan="2" class="text-center align-middle">Total (Rp)</th>
                <th width="8%" rowspan="2" class="text-center align-middle">Aksi</th>
              </tr>
              <tr>
                <th width="15%" class="text-center">Jasa Sarana</th>
                <th width="15%" class="text-center">Jasa Pelayanan</th>
                <th width="15%" class="text-center">Jasa Barang</th>
              </tr>
            </thead>
            <tbody id="table-kelas">
              <tr>
                <td class="text-center" colspan="99">Silahkan pilih Tarif terlebih dahulu</td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-10 offset-md-10">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>