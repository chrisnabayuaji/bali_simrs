<!-- js -->
<?php $this->load->view('master/wilayah/_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Induk Wilayah</label>
        <div class="col-lg-7 col-md-5">
          <select class="chosen-select custom-select w-100" name="wilayah_parent">
            <option value="">- None -</option>
            <?php foreach($list_wilayah_parent as $wp):?>
            <option value="<?=$wp['wilayah_id']?>" <?php if($wp['wilayah_id'] == @$main['wilayah_parent']) echo 'selected'?>><?=$wp['wilayah_id']?> - <?=$wp['wilayah_nm']?></option>
            <?php endforeach;?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Kode Wilayah <span class="text-danger">*<span></label>
        <div class="col-lg-3 col-md-5">
          <input type="text" class="form-control" name="wilayah_id" value="<?=@$main['wilayah_id']?>" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Nama Wilayah <span class="text-danger">*<span></label>
        <div class="col-lg-7 col-md-9">
          <input type="text" class="form-control" name="wilayah_nm" value="<?=@$main['wilayah_nm']?>" required="">
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>