<?php 
if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Jadwal extends MY_Controller{

	var $nav_id = '03.05.01', $nav, $cookie;
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array(
			'm_jadwal_dokter',
			'master/m_pegawai',
			'master/m_lokasi'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
    $this->cookie = get_cookie_nav($this->nav_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'lokasi_id','type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}
	
	public function index() {	
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(4, 0);
    $this->cookie['total_rows'] = $this->m_jadwal_dokter->all_rows($this->cookie);
    set_cookie_nav($this->nav_id, $this->cookie);
    //main data
    $data['nav'] = $this->nav;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_jadwal_dokter->list_data($this->cookie);
    $data['dokter'] = $this->m_pegawai->all_data();
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
    //set pagination
    set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('pelayanan/jadwal_dokter/index',$data);
	}

	public function form_modal($id=null) {
    $this->authorize($this->nav, ($id != '' ) ? '_update' : '_add');
		
		if($id == null) {
			$data['main'] = array();
			$data['count_senin'] = 0;
			$data['count_selasa'] = 0;
			$data['count_rabu'] = 0;
			$data['count_kamis'] = 0;
			$data['count_jumat'] = 0;
			$data['count_sabtu'] = 0;
			$data['count_minggu'] = 0;
		} else {
			$data['main'] = $this->m_jadwal_dokter->get_data($id);
			$data['count_senin'] = $this->m_jadwal_dokter->count_jadwal_dokter_rinc($id, 'senin');
			$data['count_selasa'] = $this->m_jadwal_dokter->count_jadwal_dokter_rinc($id, 'selasa');
			$data['count_rabu'] = $this->m_jadwal_dokter->count_jadwal_dokter_rinc($id, 'rabu');
			$data['count_kamis'] = $this->m_jadwal_dokter->count_jadwal_dokter_rinc($id, 'kamis');
			$data['count_jumat'] = $this->m_jadwal_dokter->count_jadwal_dokter_rinc($id, 'jumat');
			$data['count_sabtu'] = $this->m_jadwal_dokter->count_jadwal_dokter_rinc($id, 'sabtu');
			$data['count_minggu'] = $this->m_jadwal_dokter->count_jadwal_dokter_rinc($id, 'minggu');
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['lokasi'] = $this->m_lokasi->all_data(array('is_loket_antrian' => 1));
		$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/save/'.$id;
			
		echo json_encode(array(
			'html' => $this->load->view('pelayanan/jadwal_dokter/form_modal', $data, true)
		));
	}
	
	public function save($id = null)
	{
		$this->m_jadwal_dokter->save($id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($id != '' ) ? '_update' : '_add', $this->nav_id);
		redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	public function delete($id = null)
	{
		$this->m_jadwal_dokter->delete($id, true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log('_delete', $this->nav_id);
		redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	public function multiple($type = null)
	{
		$data = $this->input->post();
    if(isset($data['checkitem'])){
      foreach ($data['checkitem'] as $key) {
        switch ($type) {					
					case 'delete':
						$this->authorize($this->nav, '_delete');
            $this->m_jadwal_dokter->delete($key, true);
						$flash = 'Data berhasil dihapus.';
						create_log('_delete', $this->nav_id);
            break;

					case 'enable':
						$this->authorize($this->nav, '_update');
            $this->m_jadwal_dokter->update($key, array('is_active' => 1));
						$flash = 'Data berhasil diaktifkan.';
						create_log('_update', $this->nav_id);
            break;

					case 'disable':
						$this->authorize($this->nav, '_update');
            $this->m_jadwal_dokter->update($key, array('is_active' => 0));
						$flash = 'Data berhasil dinonaktifkan.';
						create_log('_delete', $this->nav_id);
            break;
        }
      }
    }
    create_log($t,$this->this->menu['menu']);
    $this->session->set_flashdata('flash_success', $flash);
    redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	function ajax($type=null, $id=null) {
		if ($type == 'cek_id') {
			$data = $this->input->post();
			$cek = $this->m_jadwal_dokter->check_by_lokasi_id($data['lokasi_id']);
			if ($id == null) {
				if ($cek == null) {
					echo 'true';
				}else{
					echo 'false';
				}
			}else{
				if ($id != $data['lokasi_id'] && $cek != null) {
					echo 'false';
				}else{
					echo 'true';
				}
			}
		}

		if ($type == 'get_antrian_cd') {
			$data = $this->input->post();
			$res = $this->m_lokasi->get_data($data['lokasi_id']);
			echo json_encode($res);
		}

		if($type == 'add_item') {
			$data['nav'] = $this->nav;
			$data['dokter'] = $this->m_pegawai->by_field('jenispegawai_cd', '02', 'result');
			
			$html = '';
			$data['hari'] = $this->input->get('hari');
			$data['jadwal_id'] = $this->input->get('jadwal_id');
			if($data['jadwal_id'] == '') {
				$data['list_rinc'] = array();
			} else {
				$data['list_rinc'] = $this->m_jadwal_dokter->jadwal_dokter_rinc($data['jadwal_id'], $data['hari']);
			} 
			$item_no = $this->input->get('item_no')+1;
			$data['item_no'] = $item_no;

			$html.= $this->load->view('pelayanan/jadwal_dokter/add_item', $data, true);
					
      echo json_encode(array(
      	'html' => $html,
      	'item_no' => $item_no,
      ));
		}

		if ($type == 'delete_item') {
			$jadwalrinc_id = $this->input->post('jadwalrinc_id');
			//
			$this->m_jadwal_dokter->delete_item($jadwalrinc_id);
			$callback = 'true';
			//
			echo json_encode(array(
				'callback' => $callback
			));
		}

	}
	
}