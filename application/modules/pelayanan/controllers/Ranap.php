<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Ranap extends MY_Controller
{

	var $nav_id = '03.03', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'm_ranap',
			'master/m_lokasi',
			'master/m_jenis_pasien',
			'm_dt_diagnosis',
			'm_dt_tarifkelas',
			'm_dt_petugas',
			'm_dt_obat',
			'm_dt_obat_master',
			'm_dt_bhp',
			'm_dt_alkes',
			'm_dt_laboratorium',
			'm_dt_radiologi',
			'm_dt_bedah_sentral',
			'master/m_rsrujukan',
			'master/m_jenisgizi',
			'master/m_jamgizi',
			'master/m_kamar',
			'm_dt_kamar',
			'keuangan/kasir/m_piutang'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id);
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('tgl_registrasi_from' => '', 'tgl_registrasi_to' => '', 'no_rm_nm' => '', 'lokasi_id' => '', 'jenispasien_id' => '', 'periksa_st' => '', 'pulang_st' => '0');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'a.periksa_st', 'type' => 'asc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(4, 0);
		$this->cookie['total_rows'] = $this->m_ranap->all_rows($this->cookie);
		set_cookie_nav($this->nav_id, $this->cookie);
		//main data
		$data['nav'] = $this->nav;
		$data['cookie'] = $this->cookie;
		$data['main'] = $this->m_ranap->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
		$data['lokasi'] = $this->m_lokasi->by_field('jenisreg_st', 2, 'result');
		$data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
		//set pagination
		set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('pelayanan/ranap/index', $data);
	}

	public function form($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
			$data['alergi'] = array();
		} else {
			$data['main'] = $this->m_ranap->get_data($id);
			$data['alergi'] = $this->m_ranap->get_alergi($data['main']['pasien_id']);
			$this->m_ranap->update_periksa_st($id, $data['main']['periksa_st']);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save/' . $id;

		$this->render('pelayanan/ranap/form', $data);
	}

	public function pindah_bangsal($reg_id = null, $pasien_id = null, $kelas_id = null, $lokasi_id = null, $kamar_id = null)
	{
		$this->authorize($this->nav, ($reg_id != '') ? '_update' : '_add');

		if ($reg_id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_ranap->get_data_pindah_bangsal($reg_id, $pasien_id, $kelas_id, $lokasi_id, $kamar_id);
		}
		$data['reg_id'] = $reg_id;
		$data['nav'] = $this->nav;
		$data['lokasi_bangsal'] = $this->m_lokasi->by_field('jenisreg_st', 2, 'result');
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save_pindah_bangsal/' . $reg_id;

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/ranap/pindah_bangsal', $data, true)
		));
	}

	public function riwayat_bangsal($reg_id = null)
	{
		$this->authorize($this->nav, ($reg_id != '') ? '_update' : '_add');

		$data['reg_id'] = $reg_id;
		$data['nav'] = $this->nav;
		$data['main'] = $this->m_ranap->list_riwayat_bangsal($reg_id);

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/ranap/riwayat_bangsal', $data, true)
		));
	}

	public function save_pindah_bangsal($reg_id = null)
	{
		$this->m_ranap->save_pindah_bangsal($reg_id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($reg_id != '') ? '_update' : '_add', $this->nav_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index');
	}

	function ajax($type = null, $id = null)
	{
		if ($type == 'view_tab_menu') {
			$view_name = $this->input->post('view_name');
			$id = $this->input->post('id');
			$data['nav'] = $this->nav;
			$data['reg'] = $this->m_ranap->get_data($id);

			// Tindak Lanjut
			if ($view_name == 'tindak_lanjut') {
				$data['lokasi_jenisreg_1'] = $this->m_lokasi->by_field('jenisreg_st', 1, 'result');
				$data['lokasi_jenisreg_2'] = $this->m_lokasi->by_field('jenisreg_st', 2, 'result');
				$data['rujukan'] = $this->m_rsrujukan->all_data();
			}

			if ($view_name == 'gizi') {
				$data['jenisgizi'] = $this->m_jenisgizi->all_data();
				$data['jamgizi'] = $this->m_jamgizi->all_data();
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/' . $view_name, $data, true)
			));
		}

		if ($type == 'get_kamar') {
			$lokasi_id = $this->input->post('lokasi_id');
			$list_kamar = $this->m_kamar->all_data($lokasi_id);
			//
			$html = '';
			$html .= '<select name="kamar_id" id="kamar_id" class="chosen-select custom-select w-100" required>';
			$html .= '<option value="">---</option>';
			foreach ($list_kamar as $kamar) {
				$html .= '<option value="' . $kamar['kamar_id'] . '" data-kelas-id="' . $kamar['kelas_id'] . '" data-nom-tarif="' . $kamar['nom_tarif'] . '">' . $kamar['kamar_nm'] . '</option>';
			}
			$html .= '</select>';
			$html .= js_chosen();

			$html .= '<script>
					  			$(function() {';
			$html .= '		$("#kamar_id").bind("change",function(e) {
											e.preventDefault();
											var i = $(this).val();
											var kelas_id = $(this).find(":selected").attr("data-kelas-id");
											var nom_tarif = $(this).find(":selected").attr("data-nom-tarif");
											var reg_id = $("#reg_id").val();
											$("#kelas_id").val(kelas_id);
											$("#nom_tarif").val(nom_tarif);
											_cek_kamar(i, reg_id);
										})

										function _cek_kamar(id, reg_id) {
								      $.post("' . site_url('pelayanan/ranap/ajax/cek_kamar') . '", {kamar_id: id, reg_id: reg_id},function(data) {
								        if (data.status === false) {
								          _get_kamar($("#lokasi_bangsal_id").val());
								          $("#kelas_id").val("");
													$("#nom_tarif").val("");
								          $.toast({
								            heading: "Error",
								            text: "Kamar tidak tersedia",
								            icon: "error",
								            position: "top-right"
								          })
								        }
								      },"json");
								    }

								    function _get_kamar(i, j) {
								      $.post("' . site_url('pelayanan/ranap/ajax/get_kamar') . '", {lokasi_id: i},function(data) {
								        $("#box_kamar").html(data.html);
								      },"json");
								    }';
			$html .= ' 	});
					  		</script>';

			$html .= "<script></script>";
			//
			echo json_encode(array(
				'html' => $html,
			));
		}

		if ($type == 'cek_kamar') {
			$reg_id = $this->input->post('reg_id');
			$kamar_id = $this->input->post('kamar_id');
			$reg_pasien = $this->m_kamar->get_reg_pasien($reg_id);
			$cek = $this->m_kamar->by_field('kamar_id', $kamar_id, 'row');
			if ($reg_pasien['jns_rawat_cd'] == '01') {
				$status = true;
			} else {
				if ($cek['jml_bed_kosong'] == 0 || $cek['jml_bed_kosong'] == NULL) {
					$status = false;
				} else {
					$status = true;
				}
			}
			//
			echo json_encode(array(
				'status' => $status,
			));
		}

		if ($type == 'data_kamar') {
			$list = $this->m_dt_kamar->get_datatables();
			$no = 0;
			$data = array();
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['lokasi_nm'];
				$row[] = $field['kamar_nm'];
				$row[] = $field['jml_bed_kosong'];

				$data[] = $row;
			}

			$output = array(
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}
	}

	//CATATAN MEDIS
	public function ajax_catatan_medis($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_ranap->catatan_medis_save();
		} elseif ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->catatan_medis_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/catatan_medis_data', $data, true)
			));
		} elseif ($type == 'history') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->catatan_medis_history($pasien_id, $reg_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/catatan_medis_history', $data, true)
			));
		} elseif ($type == 'get_data') {
			$catatanmedis_id = $this->input->post('catatanmedis_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');

			$main = $this->m_ranap->catatan_medis_get($catatanmedis_id, $reg_id, $pasien_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$catatanmedis_id = $this->input->post('catatanmedis_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_ranap->catatan_medis_delete($catatanmedis_id, $reg_id, $pasien_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->catatan_medis_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/catatan_medis_data', $data, true)
			));
		} elseif ($type == 'autocomplete') {
			$pegawai_nm = $this->input->get('pegawai_nm');
			$res = $this->m_ranap->petugas_autocomplete($pegawai_nm);
			echo json_encode($res);
		} elseif ($type == 'search_petugas') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/search_petugas_single', $data, true)
			));
		} elseif ($type == 'search_data') {
			$list = $this->m_dt_petugas->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['pegawai_id'];
				$row[] = $field['pegawai_nm'];
				$row[] = $field['pegawai_nip'];
				$row[] = get_parameter_value('jenispegawai_cd', $field['jenispegawai_cd']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="petugas_fill(' . "'" . $field['pegawai_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_petugas->count_all(),
				"recordsFiltered" => $this->m_dt_petugas->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'petugas_fill') {
			$data = $this->input->post();
			$res = $this->m_ranap->petugas_row($data['pegawai_id']);
			echo json_encode($res);
		}
	}

	// Pemeriksaan fisik
	public function ajax_pemeriksaan_fisik($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_ranap->pemeriksaan_fisik_save();
		} elseif ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->pemeriksaan_fisik_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/pemeriksaan_fisik_data', $data, true)
			));
		} elseif ($type == 'history') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->pemeriksaan_fisik_history($pasien_id, $reg_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/pemeriksaan_fisik_history', $data, true)
			));
		} elseif ($type == 'get_data') {
			$anamnesis_id = $this->input->post('anamnesis_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');

			$main = $this->m_ranap->pemeriksaan_fisik_get($anamnesis_id, $reg_id, $pasien_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$anamnesis_id = $this->input->post('anamnesis_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_ranap->pemeriksaan_fisik_delete($anamnesis_id, $reg_id, $pasien_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->pemeriksaan_fisik_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/pemeriksaan_fisik_data', $data, true),
			));
		} elseif ($type == 'autocomplete') {
			$pegawai_nm = $this->input->get('pegawai_nm');
			$res = $this->m_ranap->petugas_autocomplete($pegawai_nm);
			echo json_encode($res);
		} elseif ($type == 'search_petugas') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/search_petugas_single', $data, true)
			));
		} elseif ($type == 'search_data') {
			$list = $this->m_dt_petugas->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['pegawai_id'];
				$row[] = $field['pegawai_nm'];
				$row[] = $field['pegawai_nip'];
				$row[] = get_parameter_value('jenispegawai_cd', $field['jenispegawai_cd']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="petugas_fill(' . "'" . $field['pegawai_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_petugas->count_all(),
				"recordsFiltered" => $this->m_dt_petugas->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'petugas_fill') {
			$data = $this->input->post();
			$res = $this->m_ranap->petugas_row($data['pegawai_id']);
			echo json_encode($res);
		} else if ($type == 'grafik_tekanan_darah') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['main'] = $this->m_ranap->pemeriksaan_fisik_data($pasien_id, $reg_id, $lokasi_id);
			if (count($data['main']) > 0) {
				$html = $this->load->view('pelayanan/ranap/grafik_tekanan_darah', $data, true);
			} else {
				$html = 'Tidak ada data';
			}

			//
			echo json_encode(array(
				'html' => $html
			));
		} else if ($type == 'grafik_suhu_tubuh') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['main'] = $this->m_ranap->pemeriksaan_fisik_data($pasien_id, $reg_id, $lokasi_id);
			if (count($data['main']) > 0) {
				$html = $this->load->view('pelayanan/ranap/grafik_suhu_tubuh', $data, true);
			} else {
				$html = 'Tidak ada data';
			}

			//
			echo json_encode(array(
				'html' => $html
			));
		}
	}

	// Diagnosis
	public function ajax_diagnosis($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_ranap->diagnosis_save();
		} elseif ($type == 'autocomplete') {
			$penyakit_nm = $this->input->get('penyakit_nm');
			$res = $this->m_ranap->penyakit_autocomplete($penyakit_nm);
			echo json_encode($res);
		} elseif ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->diagnosis_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/diagnosis_data', $data, true)
			));
		} elseif ($type == 'history') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->diagnosis_history($pasien_id, $reg_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/diagnosis_history', $data, true)
			));
		} elseif ($type == 'get_data') {
			$diagnosis_id = $this->input->post('diagnosis_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');

			$main = $this->m_ranap->diagnosis_get($diagnosis_id, $reg_id, $pasien_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$diagnosis_id = $this->input->post('diagnosis_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_ranap->diagnosis_delete($diagnosis_id, $reg_id, $pasien_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->diagnosis_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/diagnosis_data', $data, true)
			));
		} elseif ($type == 'search_diagnosis') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/search_diagnosis', $data, true)
			));
		} elseif ($type == 'search_data') {
			$list = $this->m_dt_diagnosis->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['penyakit_id'];
				$row[] = $field['icdx'];
				$row[] = $field['penyakit_nm'];
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="diagnosis_fill(' . "'" . $field['penyakit_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_diagnosis->count_all(),
				"recordsFiltered" => $this->m_dt_diagnosis->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'diagnosis_fill') {
			$data = $this->input->post();
			$res = $this->m_ranap->diagnosis_row($data['penyakit_id']);
			echo json_encode($res);
		}
	}

	// Tindakan
	public function ajax_tindakan($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_ranap->tindakan_save();
		} elseif ($type == 'tarifkelas_autocomplete') {
			$tarif_nm = $this->input->get('tarif_nm');
			$res = $this->m_ranap->tarifkelas_autocomplete($tarif_nm, $id);
			echo json_encode($res);
		} elseif ($type == 'prosedur_autocomplete') {
			$prosedur_nm = $this->input->get('prosedur_nm');
			$res = $this->m_ranap->prosedur_autocomplete($prosedur_nm);
			echo json_encode($res);
		} elseif ($type == 'petugas_autocomplete') {
			$petugas_nm = $this->input->get('petugas_nm');
			$res = $this->m_ranap->petugas_autocomplete($petugas_nm);
			echo json_encode($res);
		} elseif ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->tindakan_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/tindakan_data', $data, true)
			));
		} elseif ($type == 'history') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->tindakan_history($pasien_id, $reg_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/tindakan_history', $data, true)
			));
		} elseif ($type == 'get_data') {
			$tindakan_id = $this->input->post('tindakan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');

			$main = $this->m_ranap->tindakan_get($tindakan_id, $reg_id, $pasien_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$tindakan_id = $this->input->post('tindakan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_ranap->tindakan_delete($tindakan_id, $reg_id, $pasien_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->tindakan_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/tindakan_data', $data, true)
			));
		} elseif ($type == 'search_tarifkelas') {
			$data['nav'] = $this->nav;
			$data['kelas_id'] = $id;
			$data['get_kelas'] = $this->m_ranap->get_kelas($id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/search_tarifkelas', $data, true)
			));
		} elseif ($type == 'search_tarifkelas_data') {
			$list = $this->m_dt_tarifkelas->get_datatables($id);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['tarifkelas_id'];
				$row[] = $field['tarif_nm'];
				$row[] = $field['kelas_nm'];
				$row[] = num_id($field['nominal']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="tarifkelas_fill(' . "'" . $field['tarifkelas_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_tarifkelas->count_all($id),
				"recordsFiltered" => $this->m_dt_tarifkelas->count_filtered($id),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'tarifkelas_fill') {
			$data = $this->input->post();
			$res = $this->m_ranap->tarifkelas_row($data['tarifkelas_id']);
			echo json_encode($res);
		} elseif ($type == 'search_petugas') {
			$data['nav'] = $this->nav;
			$data['form_name'] = @$id;

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/search_petugas', $data, true)
			));
		} elseif ($type == 'search_petugas_data') {
			$list = $this->m_dt_petugas->get_datatables();
			$form_name = $this->input->post('form_name');
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['pegawai_id'];
				$row[] = $field['pegawai_nm'];
				$row[] = $field['pegawai_nip'];
				$row[] = get_parameter_value('jenispegawai_cd', $field['jenispegawai_cd']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="petugas_fill(' . "'" . $field['pegawai_id'] . "'" . ',' . "'" . $form_name . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_petugas->count_all(),
				"recordsFiltered" => $this->m_dt_petugas->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'petugas_fill') {
			$data = $this->input->post();
			$res = $this->m_ranap->petugas_row($data['pegawai_id']);
			echo json_encode($res);
		} else if ($type == 'add_petugas') {
			$tindakan_id = $this->input->get('tindakan_id');
			$data['nav'] = $this->nav;

			$html = '';
			if ($tindakan_id != '') {
				$petugas_no = $this->input->get('petugas_no');
				$get_tindakan = $this->m_ranap->get_tindakan($tindakan_id);
				$data['tindakan_id'] = $tindakan_id;
				$data['petugas_no_post'] = $petugas_no;
				$data['type'] = 'edit';

				if ($get_tindakan['petugas_id'] != '') {
					$data['petugas_no'] = '';
					$data['form_name'] = 'petugas_id';
					$data['pegawai_id'] = $get_tindakan['petugas_id'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_1'];

					$html .= $this->load->view('pelayanan/ranap/add_petugas', $data, true);
				}
				if ($get_tindakan['petugas_id_2'] != '') {
					$data['petugas_no'] = '2';
					$data['form_name'] = 'petugas_id_2';
					$data['pegawai_id'] = $get_tindakan['petugas_id_2'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_2'];

					$html .= $this->load->view('pelayanan/ranap/add_petugas', $data, true);
				}
				if ($get_tindakan['petugas_id_3'] != '') {
					$data['petugas_no'] = '3';
					$data['form_name'] = 'petugas_id_3';
					$data['pegawai_id'] = $get_tindakan['petugas_id_3'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_3'];

					$html .= $this->load->view('pelayanan/ranap/add_petugas', $data, true);
				}
				if ($get_tindakan['petugas_id_4'] != '') {
					$data['petugas_no'] = '4';
					$data['form_name'] = 'petugas_id_4';
					$data['pegawai_id'] = $get_tindakan['petugas_id_4'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_4'];

					$html .= $this->load->view('pelayanan/ranap/add_petugas', $data, true);
				}
				if ($get_tindakan['petugas_id_5'] != '') {
					$data['petugas_no'] = '5';
					$data['form_name'] = 'petugas_id_5';
					$data['pegawai_id'] = $get_tindakan['petugas_id_5'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_5'];

					$html .= $this->load->view('pelayanan/ranap/add_petugas', $data, true);
				}
			} else {
				$petugas_no = $this->input->get('petugas_no') + 1;
				$data['petugas_no'] = ($petugas_no == '1') ? '' : $petugas_no;
				$data['form_name'] = ($petugas_no == '1') ? 'petugas_id' : 'petugas_id_' . $petugas_no;
				$data['petugas_no_post'] = $petugas_no;
				$data['type'] = 'add';

				$html .= $this->load->view('pelayanan/ranap/add_petugas', $data, true);
			}

			echo json_encode(array(
				'html' => $html,
				'petugas_no' => $petugas_no,
			));
		} elseif ($type == 'delete_petugas') {
			$tindakan_id = $this->input->post('tindakan_id');
			$form_name = $this->input->post('form_name');
			//
			$callback = 'true';
			//
			echo json_encode(array(
				'callback' => $callback
			));
		}
	}

	// Pemberian Obat
	public function ajax_pemberian_obat($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_ranap->pemberian_obat_save();
		} elseif ($type == 'autocomplete') {
			$obat_nm = $this->input->get('obat_nm');
			$map_lokasi_depo = $this->input->get('map_lokasi_depo');
			$res = $this->m_ranap->obat_autocomplete($obat_nm, $map_lokasi_depo);
			echo json_encode($res);
		} elseif ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->pemberian_obat_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/igd/pemberian_obat_data', $data, true)
			));
		} elseif ($type == 'history') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->pemberian_obat_history($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/igd/pemberian_obat_history', $data, true)
			));
		} elseif ($type == 'get_data') {
			$farmasi_id = $this->input->post('farmasi_id');
			$resep_id = $this->input->post('resep_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_ranap->pemberian_obat_get($farmasi_id, $resep_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$resep_id = $this->input->post('resep_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$farmasi_id = $this->input->post('farmasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$this->m_ranap->pemberian_obat_delete($resep_id, $reg_id, $farmasi_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->pemberian_obat_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/igd/pemberian_obat_data', $data, true)
			));
		} elseif ($type == 'search_obat') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/igd/search_obat', $data, true)
			));
		} elseif ($type == 'search_data') {
			$map_lokasi_depo = $this->input->post('map_lokasi_depo');
			$list = $this->m_dt_obat->get_datatables($map_lokasi_depo);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['no_batch'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = to_date($field['tgl_expired'], '-', 'date', ' ');
				$row[] = num_id($field['stok_akhir']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="obat_fill(' . "'" . $field['obat_id'] . "'" . ',' . "'" . $field['stokdepo_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_obat->count_all($map_lokasi_depo),
				"recordsFiltered" => $this->m_dt_obat->count_filtered($map_lokasi_depo),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} elseif ($type == 'search_data_master') {
			$list = $this->m_dt_obat_master->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="obat_fill_master(' . "'" . $field['obat_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_obat_master->count_all(),
				"recordsFiltered" => $this->m_dt_obat_master->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'obat_fill') {
			$data = $this->input->post();
			$res = $this->m_ranap->obat_row($data);
			echo json_encode($res);
		} else if ($type == 'obat_fill_master') {
			$data = $this->input->post();
			$res = $this->m_ranap->obat_row_master($data['obat_id']);
			echo json_encode($res);
		} elseif ($type == 'get_no_resep') {
			$reg_id = $this->input->post('reg_id');
			$res = $this->m_ranap->get_no_resep($reg_id);
			echo json_encode($res);
		} elseif ($type == 'dosis_autocomplete') {
			$res = $this->m_poliklinik->dosis_autocomplete();
			echo json_encode($res);
		} elseif ($type == 'peringatan_khusus_autocomplete') {
			$res = $this->m_poliklinik->peringatan_khusus_autocomplete();
			echo json_encode($res);
		} elseif ($type == 'jam_minum_autocomplete') {
			$res = $this->m_poliklinik->jam_minum_autocomplete();
			echo json_encode($res);
		}
	}

	// BHP
	public function ajax_bhp($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_ranap->bhp_save();
		} elseif ($type == 'autocomplete') {
			$obat_nm = $this->input->get('obat_nm');
			$map_lokasi_depo = $this->input->get('map_lokasi_depo');
			$res = $this->m_ranap->bhp_autocomplete($obat_nm, $map_lokasi_depo);
			echo json_encode($res);
		} elseif ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->bhp_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/bhp_data', $data, true)
			));
		} elseif ($type == 'history') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->bhp_history($pasien_id, $reg_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/bhp_history', $data, true)
			));
		} elseif ($type == 'get_data') {
			$bhp_id = $this->input->post('bhp_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_ranap->bhp_get($bhp_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$bhp_id = $this->input->post('bhp_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_ranap->bhp_delete($bhp_id, $reg_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->bhp_data($reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/bhp_data', $data, true)
			));
		} elseif ($type == 'search_bhp') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/search_bhp', $data, true)
			));
		} elseif ($type == 'search_data') {
			$map_lokasi_depo = $this->input->post('map_lokasi_depo');
			$list = $this->m_dt_bhp->get_datatables($map_lokasi_depo);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['no_batch'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = to_date($field['tgl_expired'], '-', 'date', ' ');
				$row[] = num_id($field['stok_akhir']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="bhp_fill(' . "'" . $field['obat_id'] . "'" . ',' . "'" . $field['stokdepo_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_bhp->count_all($map_lokasi_depo),
				"recordsFiltered" => $this->m_dt_bhp->count_filtered($map_lokasi_depo),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'bhp_fill') {
			$data = $this->input->post();
			$res = $this->m_ranap->bhp_row($data);
			echo json_encode($res);
		}
	}

	// ALKES
	public function ajax_alkes($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_ranap->alkes_save();
		} elseif ($type == 'autocomplete') {
			$obat_nm = $this->input->get('obat_nm');
			$map_lokasi_depo = $this->input->get('map_lokasi_depo');
			$res = $this->m_ranap->alkes_autocomplete($obat_nm, $map_lokasi_depo);
			echo json_encode($res);
		} elseif ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->alkes_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/alkes_data', $data, true)
			));
		} elseif ($type == 'history') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->alkes_history($pasien_id, $reg_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/alkes_history', $data, true)
			));
		} elseif ($type == 'get_data') {
			$alkes_id = $this->input->post('alkes_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_ranap->alkes_get($alkes_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$alkes_id = $this->input->post('alkes_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_ranap->alkes_delete($alkes_id, $reg_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->alkes_data($reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/alkes_data', $data, true)
			));
		} elseif ($type == 'search_alkes') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/search_alkes', $data, true)
			));
		} elseif ($type == 'search_data') {
			$map_lokasi_depo = $this->input->post('map_lokasi_depo');
			$list = $this->m_dt_alkes->get_datatables($map_lokasi_depo);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['no_batch'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = to_date($field['tgl_expired'], '-', 'date', ' ');
				$row[] = num_id($field['stok_akhir']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="alkes_fill(' . "'" . $field['obat_id'] . "'" . ',' . "'" . $field['stokdepo_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_alkes->count_all($map_lokasi_depo),
				"recordsFiltered" => $this->m_dt_alkes->count_filtered($map_lokasi_depo),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'alkes_fill') {
			$data = $this->input->post();
			$res = $this->m_ranap->alkes_row($data);
			echo json_encode($res);
		}
	}

	//  Penunjang
	public function ajax_penunjang_laboratorium($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'modal') {
			$data['reg'] = $this->m_ranap->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainlab'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_laboratorium/save/';
			} else {
				$data['mainlab'] = $this->m_ranap->laboratorium_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_laboratorium/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/laboratorium_modal', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['reg'] = $this->m_ranap->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainlab'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_laboratorium/save/';
			} else {
				$data['mainlab'] = $this->m_ranap->laboratorium_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_laboratorium/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/laboratorium_detail', $data, true)
			));
		}

		if ($type == 'laboratorium_pemeriksaan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->laboratorium_pemeriksaan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/laboratorium_pemeriksaan_data', $data, true)
			));
		}

		if ($type == 'laboratorium_tarif_tindakan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->laboratorium_tarif_tindakan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/laboratorium_tarif_tindakan_data', $data, true)
			));
		}

		if ($type == 'laboratorium_bhp_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->laboratorium_bhp_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/laboratorium_bhp_data', $data, true)
			));
		}

		if ($type == 'laboratorium_alkes_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->laboratorium_alkes_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/laboratorium_alkes_data', $data, true)
			));
		}

		if ($type == 'search_data') {
			$list = $this->m_dt_laboratorium->get_datatables($id);
			$no = 0;
			$data = array();
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				if ($field['parent_id'] != '') {
					$dash = '--';
				} else {
					$dash = '';
				};
				$row[] = $field['itemlab_id'];
				$row[] = $dash . $field['itemlab_nm'];
				if ($field['parent_id'] != '') {
					if (@$field['pemeriksaanrinc_id'] != null) {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemlab_id'].'" checked="true">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="' . $field['itemlab_id'] . '" checked="true">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>';
					} else {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemlab_id'].'">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="' . $field['itemlab_id'] . '">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>';
					}
				} else {
					$row[] = '';
				}

				$data[] = $row;
			}

			$output = array(
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'save') {
			$this->m_ranap->laboratorium_save(@$reg_id, @$id);
		}

		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->laboratorium_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/laboratorium_data', $data, true)
			));
		}

		if ($type == 'history') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->laboratorium_history($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/laboratorium_history', $data, true)
			));
		}

		if ($type == 'delete_data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_ranap->laboratorium_delete($pemeriksaan_id, $reg_id, $pasien_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->laboratorium_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/laboratorium_data', $data, true)
			));
		}
	}

	public function ajax_penunjang_radiologi($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'modal') {
			$data['reg'] = $this->m_ranap->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			$data['diagnosis'] = $this->m_ranap->get_diagnosis($reg_id);
			if ($id == null) {
				$data['mainrad'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_radiologi/save/';
			} else {
				$data['mainrad'] = $this->m_ranap->radiologi_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_radiologi/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/radiologi_modal', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['reg'] = $this->m_ranap->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			$data['diagnosis'] = $this->m_ranap->get_diagnosis($reg_id);
			if ($id == null) {
				$data['mainrad'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_radiologi/save/';
			} else {
				$data['mainrad'] = $this->m_ranap->radiologi_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_radiologi/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/radiologi_detail', $data, true)
			));
		}

		if ($type == 'radiologi_pemeriksaan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->radiologi_pemeriksaan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/radiologi_pemeriksaan_data', $data, true)
			));
		}

		if ($type == 'radiologi_tarif_tindakan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->radiologi_tarif_tindakan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/radiologi_tarif_tindakan_data', $data, true)
			));
		}

		if ($type == 'radiologi_bhp_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->radiologi_bhp_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/radiologi_bhp_data', $data, true)
			));
		}

		if ($type == 'radiologi_alkes_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->radiologi_alkes_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/radiologi_alkes_data', $data, true)
			));
		}

		if ($type == 'search_data') {
			$list = $this->m_dt_radiologi->get_datatables($id);
			$no = 0;
			$data = array();
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				if ($field['parent_id'] != '') {
					$dash = '--';
				} else {
					$dash = '';
				};
				$row[] = $field['itemrad_id'];
				$row[] = $dash . $field['itemrad_nm'];
				if ($field['parent_id'] != '') {
					if (@$field['pemeriksaanrinc_id'] != null) {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemrad_id'].'" checked="true">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="' . $field['itemrad_id'] . '" checked="true">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>';
					} else {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemrad_id'].'">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="' . $field['itemrad_id'] . '">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>';
					}
				} else {
					$row[] = '';
				}

				$data[] = $row;
			}

			$output = array(
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'save') {
			$this->m_ranap->radiologi_save(@$reg_id, @$id);
		}

		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->radiologi_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/radiologi_data', $data, true)
			));
		}

		if ($type == 'history') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->radiologi_history($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/radiologi_history', $data, true)
			));
		}

		if ($type == 'delete_data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_ranap->radiologi_delete($pemeriksaan_id, $reg_id, $pasien_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->radiologi_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/radiologi_data', $data, true)
			));
		}
	}

	public function ajax_penunjang_bedah_sentral($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'modal') {
			$data['reg'] = $this->m_ranap->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainbs'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_bedah_sentral/save/';
			} else {
				$data['mainbs'] = $this->m_ranap->bedah_sentral_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_bedah_sentral/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/bedah_sentral_modal', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['reg'] = $this->m_ranap->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainbs'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_bedah_sentral/save/';
			} else {
				$data['mainbs'] = $this->m_ranap->bedah_sentral_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_bedah_sentral/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/bedah_sentral_detail', $data, true)
			));
		}

		if ($type == 'bedah_sentral_tindakan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->bedah_sentral_tindakan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/bedah_sentral_tindakan_data', $data, true)
			));
		}

		if ($type == 'bedah_sentral_bhp_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->bedah_sentral_bhp_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/bedah_sentral_bhp_data', $data, true)
			));
		}

		if ($type == 'bedah_sentral_alkes_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->bedah_sentral_alkes_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/bedah_sentral_alkes_data', $data, true)
			));
		}

		if ($type == 'search_data') {
			$list = $this->m_dt_bedah_sentral->get_datatables($id);
			$no = 0;
			$data = array();
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				if ($field['parent_id'] != '') {
					$dash = '--';
				} else {
					$dash = '';
				};
				$row[] = $field['itemoperasi_id'];
				$row[] = $dash . $field['itemoperasi_nm'];
				if ($field['parent_id'] != '') {
					if (@$field['pemeriksaanrinc_id'] != null) {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemoperasi_id'].'" checked="true">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="' . $field['itemoperasi_id'] . '" checked="true">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>';
					} else {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemoperasi_id'].'">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="' . $field['itemoperasi_id'] . '">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>';
					}
				} else {
					$row[] = '';
				}

				$data[] = $row;
			}

			$output = array(
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'save') {
			$this->m_ranap->bedah_sentral_save(@$reg_id, @$id);
		}

		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->bedah_sentral_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/bedah_sentral_data', $data, true)
			));
		}

		if ($type == 'history') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->bedah_sentral_history($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/bedah_sentral_history', $data, true)
			));
		}

		if ($type == 'delete_data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_ranap->bedah_sentral_delete($pemeriksaan_id, $reg_id, $pasien_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->bedah_sentral_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/bedah_sentral_data', $data, true)
			));
		}
	}

	public function ajax_penunjang_vk($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'modal') {
			$data['reg'] = $this->m_ranap->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainvk'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_vk/save/';
			} else {
				$data['mainvk'] = $this->m_ranap->vk_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_vk/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/vk_modal', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['reg'] = $this->m_ranap->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainvk'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_vk/save/';
			} else {
				$data['mainvk'] = $this->m_ranap->vk_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_vk/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/vk_detail', $data, true)
			));
		}

		if ($type == 'vk_tindakan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->vk_tindakan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/vk_tindakan_data', $data, true)
			));
		}

		if ($type == 'vk_bhp_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->vk_bhp_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/vk_bhp_data', $data, true)
			));
		}

		if ($type == 'vk_alkes_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->vk_alkes_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/vk_alkes_data', $data, true)
			));
		}

		if ($type == 'save') {
			$this->m_ranap->vk_save(@$reg_id, @$id);
		}

		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->vk_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/vk_data', $data, true)
			));
		}

		if ($type == 'history') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->vk_history($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/vk_history', $data, true)
			));
		}

		if ($type == 'delete_data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_ranap->vk_delete($pemeriksaan_id, $reg_id, $pasien_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->vk_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/vk_data', $data, true)
			));
		}
	}

	public function ajax_penunjang_icu($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'modal') {
			$data['reg'] = $this->m_ranap->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainicu'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_icu/save/';
			} else {
				$data['mainicu'] = $this->m_ranap->icu_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_icu/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/icu_modal', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['reg'] = $this->m_ranap->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainicu'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_icu/save/';
			} else {
				$data['mainicu'] = $this->m_ranap->icu_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_icu/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/icu_detail', $data, true)
			));
		}

		if ($type == 'icu_tindakan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->icu_tindakan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/icu_tindakan_data', $data, true)
			));
		}

		if ($type == 'icu_bhp_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->icu_bhp_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/icu_bhp_data', $data, true)
			));
		}

		if ($type == 'icu_alkes_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->icu_alkes_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/icu_alkes_data', $data, true)
			));
		}

		if ($type == 'save') {
			$this->m_ranap->icu_save(@$reg_id, @$id);
		}

		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->icu_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/icu_data', $data, true)
			));
		}

		if ($type == 'history') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->icu_history($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/icu_history', $data, true)
			));
		}

		if ($type == 'delete_data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_ranap->icu_delete($pemeriksaan_id, $reg_id, $pasien_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->icu_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/icu_data', $data, true)
			));
		}
	}

	public function ajax_penunjang_hemodialisa($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'modal') {
			$data['reg'] = $this->m_ranap->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainhemo'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_hemodialisa/save/';
			} else {
				$data['mainhemo'] = $this->m_ranap->hemodialisa_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_hemodialisa/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/hemodialisa_modal', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['reg'] = $this->m_ranap->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainhemo'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_hemodialisa/save/';
			} else {
				$data['mainhemo'] = $this->m_ranap->hemodialisa_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_hemodialisa/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/hemodialisa_detail', $data, true)
			));
		}

		if ($type == 'hemodialisa_tindakan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->hemodialisa_tindakan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/hemodialisa_tindakan_data', $data, true)
			));
		}

		if ($type == 'hemodialisa_bhp_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->hemodialisa_bhp_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/hemodialisa_bhp_data', $data, true)
			));
		}

		if ($type == 'hemodialisa_alkes_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->hemodialisa_alkes_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/hemodialisa_alkes_data', $data, true)
			));
		}

		if ($type == 'save') {
			$this->m_ranap->hemodialisa_save(@$reg_id, @$id);
		}

		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->hemodialisa_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/hemodialisa_data', $data, true)
			));
		}

		if ($type == 'history') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->hemodialisa_history($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/hemodialisa_history', $data, true)
			));
		}

		if ($type == 'delete_data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_ranap->hemodialisa_delete($pemeriksaan_id, $reg_id, $pasien_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->hemodialisa_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/hemodialisa_data', $data, true)
			));
		}
	}

	//  Tindak Lanjut
	public function ajax_tindak_lanjut($type = null, $id = null)
	{
		if ($type == 'form_rs_rujukan') {
			$data['nav'] = $this->nav;
			$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_tindakan/save/' . $id;

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/form_rs_rujukan', $data, true)
			));
		} else if ($type == 'cek_id') {
			$data = $this->input->post();
			$data['rsrujukan_id'] = $data['rsrujukan_kode'];
			$cek = $this->m_rsrujukan->get_data($data['rsrujukan_id']);
			if ($id == null) {
				if ($cek == null) {
					echo 'true';
				} else {
					echo 'false';
				}
			} else {
				if ($id != $data['rsrujukan_id'] && $cek != null) {
					echo 'false';
				} else {
					echo 'true';
				}
			}
		} else if ($type == 'save_rs_rujukan') {
			$data = $this->input->post();
			$this->m_ranap->save_rs_rujukan();

			echo json_encode(array(
				'main' => $data
			));
		} else if ($type == 'save_tindak_lanjut') {
			$data = $this->input->post();
			$this->m_ranap->save_tindak_lanjut();
		} elseif ($type == 'tindak_lanjut_data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_ranap->tindak_lanjut_data($pasien_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'reg_pasien_data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_ranap->reg_pasien_data_pulang_st($pasien_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		} else if ($type == 'save_status_pulang') {
			$data = $this->input->post();
			$this->m_ranap->save_status_pulang();
		} elseif ($type == 'status_pulang_data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_ranap->status_pulang_data($pasien_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		}
	}

	// History
	public function ajax_history($type = null, $id = null)
	{
		if ($type == 'pasien_history') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->pasien_history($pasien_id, $reg_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/history_pasien', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['nav'] = $this->nav;
			$data['reg_id'] = $id;
			$data['main'] = $this->m_ranap->get_data($id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/history_detail', $data, true)
			));
		}

		if ($type == 'history_detail_pemeriksaan_fisik') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->history_detail_pemeriksaan_fisik($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/history_detail_pemeriksaan_fisik', $data, true)
			));
		}

		if ($type == 'history_detail_catatan_medis') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->history_detail_catatan_medis($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/history_detail_catatan_medis', $data, true)
			));
		}

		if ($type == 'history_detail_diagnosis') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->history_detail_diagnosis($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/history_detail_diagnosis', $data, true)
			));
		}

		if ($type == 'history_detail_tindakan') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->history_detail_tindakan($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/history_detail_tindakan', $data, true)
			));
		}

		if ($type == 'history_detail_pemberian_obat') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->history_detail_pemberian_obat($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/history_detail_pemberian_obat', $data, true)
			));
		}

		if ($type == 'history_detail_resep') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->history_detail_resep($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/history_detail_resep', $data, true)
			));
		}

		if ($type == 'history_detail_resep_racik') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->history_detail_resep_racik($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/history_detail_resep_racik', $data, true)
			));
		}

		if ($type == 'history_detail_bhp') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->history_detail_bhp($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/history_detail_bhp', $data, true)
			));
		}

		if ($type == 'history_detail_alkes') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->history_detail_alkes($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/history_detail_alkes', $data, true)
			));
		}

		if ($type == 'history_detail_laboratorium') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['reg_id'] = $reg_id;
			$data['main'] = $this->m_ranap->history_detail_laboratorium($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/history_detail_laboratorium', $data, true)
			));
		}

		if ($type == 'history_detail_radiologi') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['reg_id'] = $reg_id;
			$data['main'] = $this->m_ranap->history_detail_radiologi($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/history_detail_radiologi', $data, true)
			));
		}

		if ($type == 'history_detail_bedah_sentral') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['reg_id'] = $reg_id;
			$data['main'] = $this->m_ranap->history_detail_bedah_sentral($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/history_detail_bedah_sentral', $data, true)
			));
		}

		if ($type == 'history_detail_vk') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['reg_id'] = $reg_id;
			$data['main'] = $this->m_ranap->history_detail_vk($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/history_detail_vk', $data, true)
			));
		}

		if ($type == 'history_detail_icu') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['reg_id'] = $reg_id;
			$data['main'] = $this->m_ranap->history_detail_icu($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/history_detail_icu', $data, true)
			));
		}

		if ($type == 'history_detail_hemodialisa') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['reg_id'] = $reg_id;
			$data['main'] = $this->m_ranap->history_detail_hemodialisa($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/history_detail_hemodialisa', $data, true)
			));
		}
	}

	public function save_tindak_lanjut()
	{
		$this->m_ranap->save_tindak_lanjut();
		$this->m_ranap->save_status_pulang();
		$this->m_ranap->save_status_pulang_asal_tindaklanjut();
		$this->m_ranap->save_reg_pasien_kamar();
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		redirect(site_url() . '/' . $this->nav['nav_url']);
	}

	public function piutang_modal($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');
		$groupreg_in = $this->input->get('groupreg_in');
		if ($id == null) {
			$data['reg'] = null;
			$data['tindakan'] = null;
			$data['obat'] = null;
			$data['alkes'] = null;
			$data['bhp'] = null;
			$data['kamar'] = null;
		} else {
			$data['reg'] = $this->m_piutang->piutang_data($id);
			$data['tindakan'] = $this->m_piutang->piutang_tindakan($groupreg_in);
			$data['obat'] = $this->m_piutang->piutang_obat($groupreg_in);
			$data['alkes'] = $this->m_piutang->piutang_alkes($groupreg_in);
			$data['bhp'] = $this->m_piutang->piutang_bhp($groupreg_in);
			$data['kamar'] = $this->m_piutang->piutang_kamar($groupreg_in);
		}

		$data['id'] = $id;
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('keuangan/kasir/piutang/piutang_modal', $data, true)
		));
	}

	// Resep Non Racik
	public function ajax_resep($type)
	{
		if ($type == 'save') {
			$this->m_ranap->resep_save();
		}

		if ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->resep_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/resep_data', $data, true)
			));
		}

		if ($type == 'get_data') {
			$rincian_id = $this->input->post('rincian_id');
			$resep_id = $this->input->post('resep_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_ranap->resep_get($resep_id, $rincian_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		}

		if ($type == 'delete_data') {
			$pasien_id = $this->input->post('pasien_id');
			$resep_id = $this->input->post('resep_id');
			$rincian_id = $this->input->post('rincian_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_ranap->resep_delete($resep_id, $rincian_id, $reg_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->resep_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/resep_data', $data, true)
			));
		}

		if ($type == 'delete_resep') {
			$resep_id = $this->input->post('resep_id');
			$pasien_id = $this->input->post('pasien_id');
			$resep_id = $this->input->post('resep_id');
			$reg_id = $this->input->post('reg_id');

			$this->m_ranap->resep_delete_all($resep_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->resep_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/resep_data', $data, true)
			));
		}
	}

	//resep racik
	public function ajax_racik($type)
	{
		if ($type == 'save') {
			$this->m_ranap->racik_save();
		}

		if ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->racik_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/racik_data', $data, true)
			));
		}

		if ($type == 'get_data') {
			$rincian_id = $this->input->post('rincian_id');
			$resep_id = $this->input->post('resep_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_ranap->racik_get($resep_id, $rincian_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		}

		if ($type == 'delete_data') {
			$pasien_id = $this->input->post('pasien_id');
			$resep_id = $this->input->post('resep_id');
			$rincian_id = $this->input->post('rincian_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_ranap->racik_delete($resep_id, $rincian_id, $reg_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->racik_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/racik_data', $data, true)
			));
		}

		if ($type == 'delete_resep') {
			$resep_id = $this->input->post('resep_id');
			$pasien_id = $this->input->post('pasien_id');
			$resep_id = $this->input->post('resep_id');
			$reg_id = $this->input->post('reg_id');

			$this->m_ranap->resep_delete_all($resep_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->racik_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/racik_data', $data, true)
			));
		}
	}

	public function prosedur_detail($prosedur_id)
	{
		$data['nav'] = $this->nav;
		$data['prosedur'] = $this->db->where('prosedur_id', $prosedur_id)->get('mst_prosedur')->row_array();
		echo json_encode(array(
			'html' => $this->load->view('pelayanan/ranap/prosedur_detail', $data, true)
		));
	}

	//GIZI
	public function ajax_gizi($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_ranap->gizi_save();
		} elseif ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->gizi_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/gizi_data', $data, true)
			));
		} elseif ($type == 'history') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->gizi_history($pasien_id, $reg_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/gizi_history', $data, true)
			));
		} elseif ($type == 'get_data') {
			$gizi_id = $this->input->post('gizi_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');

			$main = $this->m_ranap->gizi_get($gizi_id, $reg_id, $pasien_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$gizi_id = $this->input->post('gizi_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_ranap->gizi_delete($gizi_id, $reg_id, $pasien_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_ranap->gizi_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/gizi_data', $data, true)
			));
		} elseif ($type == 'autocomplete') {
			$pegawai_nm = $this->input->get('pegawai_nm');
			$res = $this->m_ranap->petugas_autocomplete($pegawai_nm);
			echo json_encode($res);
		} elseif ($type == 'search_petugas') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/ranap/search_petugas_single', $data, true)
			));
		} elseif ($type == 'search_data') {
			$list = $this->m_dt_petugas->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['pegawai_id'];
				$row[] = $field['pegawai_nm'];
				$row[] = $field['pegawai_nip'];
				$row[] = get_parameter_value('jenispegawai_cd', $field['jenispegawai_cd']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="petugas_fill(' . "'" . $field['pegawai_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_petugas->count_all(),
				"recordsFiltered" => $this->m_dt_petugas->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'petugas_fill') {
			$data = $this->input->post();
			$res = $this->m_ranap->petugas_row($data['pegawai_id']);
			echo json_encode($res);
		}
	}
}
