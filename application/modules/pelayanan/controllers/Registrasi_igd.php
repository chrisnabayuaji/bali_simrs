<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Registrasi_igd extends MY_Controller
{

	var $nav_id = '03.01.04', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'm_registrasi_igd',
			'master/m_lokasi',
			'master/m_wilayah',
			'master/m_kelas',
			'master/m_jenis_pasien',
			'master/m_pegawai',
			'm_dt_registrasi_online',
			'm_dt_mst_pasien',
			'm_caller_antrian',
			'm_dt_diagnosis',
			'm_dt_tarifkelas',
			'm_dt_petugas',
			'm_dt_obat',
			'm_dt_bhp',
			'm_dt_alkes',
			'm_dt_laboratorium',
			'm_dt_radiologi',
			'm_dt_registrasi_ibu',
			'master/m_rsrujukan',
			'master/m_kelas_lokasi'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id);
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('tgl_registrasi_from' => '', 'tgl_registrasi_to' => '', 'no_rm_nm' => '', 'lokasi_id' => '03.01', 'jenispasien_id' => '', 'periksa_st' => '', 'pulang_st' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'tgl_registrasi', 'type' => 'desc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(4, 0);
		$this->cookie['total_rows'] = $this->m_registrasi_igd->all_rows($this->cookie);
		set_cookie_nav($this->nav_id, $this->cookie);
		//main data
		$data['nav'] = $this->nav;
		$data['cookie'] = $this->cookie;
		$data['main'] = $this->m_registrasi_igd->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
		$data['lokasi'] = $this->m_lokasi->by_field('lokasi_id', '03.01', 'result');
		$data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
		//set pagination
		set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('pelayanan/registrasi_igd/index', $data);
	}

	public function form_modal($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_registrasi_igd->get_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save/' . $id;

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/registrasi_igd/form_modal', $data, true)
		));
	}

	public function form($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
			$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_profile();
		} else {
			$data['main'] = $this->m_registrasi_igd->get_data($id);
			if ($data['main']['wilayah_st'] == 'L') {
				$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_parent('');
			} else {
				$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_profile();
			}
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		// $data['kelas'] = $this->m_kelas->all_data();
		$data['kelas'] = $this->m_kelas_lokasi->all_data('03.01');
		// $data['lokasi'] = $this->m_lokasi->by_field('jenisreg_st', 1, 'result');
		$data['lokasi'] = $this->m_lokasi->by_field('lokasi_id', '03.01', 'result');
		$data['dokter'] = $this->m_pegawai->by_field('jenispegawai_cd', '02', 'result');
		$data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save/' . $id;

		$this->render('pelayanan/registrasi_igd/form', $data);
	}

	public function save($id = null)
	{
		$this->m_registrasi_igd->save($id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($id != '') ? '_update' : '_add', $this->nav_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index');
	}

	public function delete($id = null)
	{
		$this->m_registrasi_igd->delete($id, true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log('_delete', $this->nav_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	public function multiple($type = null)
	{
		$data = $this->input->post();
		if (isset($data['checkitem'])) {
			foreach ($data['checkitem'] as $key) {
				switch ($type) {
					case 'delete':
						$this->authorize($this->nav, '_delete');
						$this->m_registrasi_igd->delete($key, true);
						$flash = 'Data berhasil dihapus.';
						create_log('_delete', $this->nav_id);
						break;

					case 'enable':
						$this->authorize($this->nav, '_update');
						$this->m_registrasi_igd->update($key, array('is_active' => 1));
						$flash = 'Data berhasil diaktifkan.';
						create_log('_update', $this->nav_id);
						break;

					case 'disable':
						$this->authorize($this->nav, '_update');
						$this->m_registrasi_igd->update($key, array('is_active' => 0));
						$flash = 'Data berhasil dinonaktifkan.';
						create_log('_delete', $this->nav_id);
						break;
				}
			}
		}
		$this->session->set_flashdata('flash_success', $flash);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	function ajax($type = null, $id = null)
	{
		if ($type == 'cek_id') {
			$data = $this->input->post();
			$cek = $this->m_registrasi_igd->get_data($data['lokasi_id']);
			if ($id == null) {
				if ($cek == null) {
					echo 'true';
				} else {
					echo 'false';
				}
			} else {
				if ($id != $data['lokasi_id'] && $cek != null) {
					echo 'false';
				} else {
					echo 'true';
				}
			}
		} else if ($type == 'get_antrian_cd') {
			$data = $this->input->post();
			$res = $this->m_lokasi->get_data($data['lokasi_id']);
			echo json_encode($res);
		} else if ($type == 'online_fill') {
			$data = $this->input->post();
			$res = $this->m_registrasi_igd->online_row($data['regonline_id']);
			echo json_encode($res);
		}
		if ($type == 'ibu_fill') {
			$data = $this->input->post();
			$res = $this->m_registrasi_igd->ibu_row($data['reg_id']);
			echo json_encode($res);
		} else if ($type == 'pasien_fill') {
			$data = $this->input->post();
			$res = $this->m_registrasi_igd->pasien_row($data['pasien_id']);
			echo json_encode($res);
		} else if ($type == 'no_rm_fill') {
			$data = $this->input->post();
			$res = $this->m_registrasi_igd->no_rm_row($data['pasien_id']);
			echo json_encode($res);
		}

		if ($type == 'status_antrian') {
			$data = $this->input->post();
			$res = $this->m_caller_antrian->get_data($data['lokasi_id']);
			echo json_encode($res);
		}

		if ($type == 'call_antrian') {
			$data = $this->input->post();
			$res = $this->m_caller_antrian->call_antrian($data);
			echo json_encode($res);
		}

		if ($type == 'get_kelas') {
			$lokasi_id = $this->input->post('lokasi_id');
			$kelas_id = $this->input->post('kelas_id');
			$list_kelas = $this->m_kelas_lokasi->all_data($lokasi_id);
			//
			$html = '';
			$html .= '<select name="kelas_id" id="kelas_id" class="chosen-select custom-select w-100" required>';
			foreach ($list_kelas as $kelas) {
				if (@$kelas_id == $kelas['kelas_id']) {
					$html .= '<option value="' . $kelas['kelas_id'] . '" selected>' . $kelas['kelas_nm'] . '</option>';
				} else {
					$html .= '<option value="' . $kelas['kelas_id'] . '">' . $kelas['kelas_nm'] . '</option>';
				}
			}
			$html .= '</select>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}

		if ($type == 'check_nik') {
			$nik = $this->input->post('nik');
			$cek = $this->m_registrasi_igd->check_nik($nik);
			if ($cek != '') {
				$status = "true";
			} else {
				$status = "false";
			}
			//
			echo json_encode(array(
				'status' => $status,
				'pasien_id' => $cek['pasien_id']
			));
		}

		if ($type == 'search-history') {
			$data = $this->m_registrasi_igd->history();
			echo json_encode($data);
		}
	}

	public function ibu_modal()
	{
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/registrasi_igd/ibu_modal', $data, true)
		));
	}

	public function get_data_ibu()
	{
		$list = $this->m_dt_registrasi_ibu->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field['pasien_id'];
			$row[] = $field['pasien_nm'] . ', ' . $field['sebutan_cd'];
			$row[] = $field['alamat'];
			$row[] = ($field['tmp_lahir'] != '') ? $field['tmp_lahir'] . ', ' : '' . @to_date($field['tgl_lahir']);
			$row[] = $field['jenispasien_nm'];
			$row[] = $field['lokasi_nm'];
			$row[] = $field['tgl_registrasi'];
			$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="ibu_fill(' . "'" . $field['reg_id'] . "'" . ')">Pilih >></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_dt_registrasi_ibu->count_all(),
			"recordsFiltered" => $this->m_dt_registrasi_ibu->count_filtered(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function online_modal()
	{
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/registrasi_igd/online_modal', $data, true)
		));
	}

	public function get_data_online()
	{
		$list = $this->m_dt_registrasi_online->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field['regonline_id'];
			$row[] = $field['tgl_periksa'];
			$row[] = $field['pasien_id'];
			$row[] = $field['pasien_nm'];
			$row[] = $field['alamat'];
			$row[] = $field['sex_cd'];
			$row[] = $field['tmp_lahir'] . ', ' . @to_date($field['tgl_lahir']);
			$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="online_fill(' . "'" . $field['regonline_id'] . "'" . ')">Pilih >></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_dt_registrasi_online->count_all(),
			"recordsFiltered" => $this->m_dt_registrasi_online->count_filtered(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function pasien_modal()
	{
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/registrasi_igd/pasien_modal', $data, true)
		));
	}

	public function get_data_pasien()
	{
		$list = $this->m_dt_mst_pasien->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field['pasien_id'];
			$row[] = $field['pasien_nm'];
			$row[] = $field['alamat'];
			$row[] = $field['sex_cd'];
			$row[] = $field['tmp_lahir'] . ', ' . @to_date($field['tgl_lahir']);
			$row[] = $field['jenispasien_nm'];
			$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="pasien_fill(' . "'" . $field['pasien_id'] . "'" . ')">Pilih >></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_dt_mst_pasien->count_all(),
			"recordsFiltered" => $this->m_dt_mst_pasien->count_filtered(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function caller_modal()
	{
		$data['nav'] = $this->nav;
		$data['caller'] = $this->m_registrasi_igd->get_caller();

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/registrasi_igd/caller_modal', $data, true)
		));
	}

	public function get_nik($nik)
	{
		$this->load->library('xmlrpc');
		$this->load->library('xmlrpcs');
		$url = "https://ayokitakerja.kemnaker.go.id/tools/check_nik/" . $nik;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		$data = curl_exec($ch);

		if (curl_errno($ch)) {
			$res = array('STATUS' => -1);
			echo json_encode($res);
		} else {
			$data = json_decode($data, TRUE);
			curl_close($ch);
			echo json_encode($data);
		}
	}
}
