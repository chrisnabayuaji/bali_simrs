<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Registrasi_offline extends MY_Controller
{

	var $nav_id = '03.01.03', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'm_registrasi_offline',
			'master/m_lokasi',
			'master/m_wilayah',
			'master/m_kelas',
			'master/m_jenis_pasien',
			'master/m_pegawai',
			'm_dt_registrasi_online',
			'm_dt_registrasi_ibu',
			'm_dt_mst_pasien',
			'm_caller_antrian',
			'master/m_kelas_lokasi',
			'master/m_identitas',
			'm_dt_diagnosis'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id);
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('tgl_registrasi_from' => '', 'tgl_registrasi_to' => '', 'no_rm_nm' => '', 'lokasi_id' => '', 'jenispasien_id' => '', 'periksa_st' => '', 'pulang_st' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'tgl_registrasi', 'type' => 'desc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(4, 0);
		$this->cookie['total_rows'] = $this->m_registrasi_offline->all_rows($this->cookie);
		set_cookie_nav($this->nav_id, $this->cookie);
		//main data
		$data['nav'] = $this->nav;
		$data['cookie'] = $this->cookie;
		$data['main'] = $this->m_registrasi_offline->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
		$data['lokasi'] = $this->m_lokasi->by_field('jenisreg_st', 1, 'result');
		$data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
		//set pagination
		set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('pelayanan/registrasi_offline/index', $data);
	}

	public function form_modal($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_registrasi_offline->get_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save/' . $id;

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/registrasi_offline/form_modal', $data, true)
		));
	}

	public function form($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
			$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_profile();
		} else {
			$data['main'] = $this->m_registrasi_offline->get_data($id);
			if ($data['main']['wilayah_st'] == 'L') {
				$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_parent('');
			} else {
				$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_profile();
			}
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['lokasi'] = $this->m_lokasi->by_field('jenisreg_st', 1, 'result');
		$data['dokter'] = $this->m_pegawai->by_field('jenispegawai_cd', '02', 'result');
		$data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save/' . $id;

		$this->render('pelayanan/registrasi_offline/form', $data);
	}

	public function pembuatan_sep($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_registrasi_offline->get_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['lokasi'] = $this->m_lokasi->by_field('jenisreg_st', 1, 'result');
		$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_parent('');
		$data['identitas_rs'] = $this->m_identitas->get_first();
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/insert_sep/' . $id;

		$this->render('pelayanan/registrasi_offline/pembuatan_sep', $data);
	}

	public function ubah_sep($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_registrasi_offline->get_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['lokasi'] = $this->m_lokasi->by_field('jenisreg_st', 1, 'result');
		$data['request_vclaim'] = json_decode($data['main']['request_vclaim'], true);
		$data['diagnosis'] = $this->m_dt_diagnosis->get_data_by_icdx($data['request_vclaim']['request']['t_sep']['diagAwal']);
		$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_parent('');
		$data['identitas_rs'] = $this->m_identitas->get_first();
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/update_sep/' . $id;

		$this->render('pelayanan/registrasi_offline/ubah_sep', $data);
	}

	public function cetak_sep($no_kartu = null)
	{
		$data['no_kartu'] = @$no_kartu;
		$data['nav'] = $this->nav;
		//render
		$this->render('pelayanan/registrasi_offline/cetak_sep', $data);
	}

	public function save($id = null)
	{
		$this->m_registrasi_offline->save($id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($id != '') ? '_update' : '_add', $this->nav_id);
		// redirect(site_url() . '/' . $this->nav['nav_url'] . '/form/');
		redirect(site_url() . '/' . $this->nav['nav_url']);
	}

	// Diagnosis
	public function ajax_diagnosis($type = null, $id = null)
	{
		if ($type == 'autocomplete') {
			$penyakit_nm = $this->input->get('penyakit_nm');
			$res = $this->m_identitas->penyakit_autocomplete($penyakit_nm);
			echo json_encode($res);
		} elseif ($type == 'search_diagnosis') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/registrasi_offline/search_diagnosis', $data, true)
			));
		} elseif ($type == 'search_data') {
			$list = $this->m_dt_diagnosis->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['penyakit_id'];
				$row[] = $field['icdx'];
				$row[] = $field['penyakit_nm'];
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="diagnosis_fill(' . "'" . $field['penyakit_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_diagnosis->count_all(),
				"recordsFiltered" => $this->m_dt_diagnosis->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'diagnosis_fill') {
			$data = $this->input->post();
			$res = $this->m_identitas->diagnosis_row($data['penyakit_id']);
			echo json_encode($res);
		}
	}

	function ajax_wilayah($id = null)
	{
		if ($id == 'get_wilayah_id_name') {
			$wilayah_parent = $this->input->get('wilayah_parent');
			$wilayah_prop = $this->input->get('wilayah_prop');
			$wilayah_kab = $this->input->get('wilayah_kab');
			$wilayah_kec = $this->input->get('wilayah_kec');
			$wilayah_kel = $this->input->get('wilayah_kel');
			//
			$wilayah_params = $this->m_wilayah->get_params($wilayah_parent);
			//
			$wilayah_selected = '';
			if ($wilayah_params['level'] == '2' && $wilayah_kab != '') { // kab
				$wilayah_selected = $wilayah_kab;
			} else if ($wilayah_params['level'] == '3' && $wilayah_kec != '') { // kec
				$wilayah_selected = $wilayah_kec;
			} else if ($wilayah_params['level'] == '4' && $wilayah_kel != '') { // kel
				$wilayah_selected = $wilayah_kel;
			}
			//
			$list_wilayah = $this->m_wilayah->list_wilayah_by_parent($wilayah_parent);
			//
			$html = '';
			if ($wilayah_parent == '' && $wilayah_prop == '' && $wilayah_kab == '' && $wilayah_kec == '' && $wilayah_kel == '') {
				$html .= '<select class="chosen-select custom-select w-100" name="wilayah_kab" id="wilayah_kab">
                    <option value="">- Kab/Kota -</option>
                  </select>';
			} else {
				$html .= '<select name="' . $wilayah_params['input_nm'] . '" id="' . $wilayah_params['input_nm'] . '" class="chosen-select custom-select w-100">';
				$html .= '<option value="">- ' . $wilayah_params['level_nm'] . ' -</option>';
				foreach ($list_wilayah as $w) {
					if ($wilayah_selected == $w['wilayah_id']) {
						$html .= '<option value="' . $w['wilayah_id'] . '#' . $w['wilayah_nm'] . '#' . $w['wilayah_id_bpjs'] . '" selected>' . $w['wilayah_id'] . ' - ' . $w['wilayah_nm'] . '</option>';
					} else {
						$html .= '<option value="' . $w['wilayah_id'] . '#' . $w['wilayah_nm'] . '#' . $w['wilayah_id_bpjs'] . '">' . $w['wilayah_id'] . ' - ' . $w['wilayah_nm'] . '</option>';
					}
				}
				$html .= '</select>';
				$html .= '<script>
						  $(function() {
						  ';
				// autoload
				if ($wilayah_selected != '') {
					$html .= 	'_get_wilayah("' . @$wilayah_selected . '","' . @$wilayah_prop . '", "' . @$wilayah_kab . '", "' . @$wilayah_kec . '", "' . @$wilayah_kel . '"); ';
				}
				// 
				$html .= '	$("#' . $wilayah_params['input_nm'] . '").bind("change",function(e) {
						  		e.preventDefault();
						  		var i = $(this).val().split("#");
		    					_get_wilayah(i[0]);
						  	});
						  	//
						  	function _get_wilayah(i, prop="", kab="", kec="", kel="") {
						  		var ext_var = "wilayah_prop="+prop;
					                ext_var+= "&wilayah_kab="+kab;
					                ext_var+= "&wilayah_kec="+kec;
					                ext_var+= "&wilayah_kel="+kel;

						  		$.get("' . site_url('pelayanan/registrasi_offline/ajax_wilayah/get_wilayah_id_name') . '?wilayah_parent="+i+"&"+ext_var,null,function(data) {
						  			$("#box_' . $wilayah_params['input_nm_next'] . '").html(data.html);
						  		},"json");
						  	}
						  });
						  </script>
						 ';
			}
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		} else if ($id == 'get_provinsi_by_st') {
			$wilayah_st = $this->input->get('wilayah_st');
			if ($wilayah_st == 'D') {
				$list_wilayah_prop = $this->m_wilayah->list_wilayah_by_profile();
			} else {
				$list_wilayah_prop = $this->m_wilayah->list_wilayah_by_parent('');
			}
			//
			$html = '';
			$html .= '<select name="wilayah_prop" id="wilayah_prop" class="chosen-select custom-select w-100">';
			$html .= '<option value="">- Propinsi -</option>';
			foreach ($list_wilayah_prop as $w) {
				$html .= '<option value="' . $w['wilayah_id'] . '#' . $w['wilayah_nm'] . '#' . $w['wilayah_id_bpjs'] . '">' . $w['wilayah_id'] . ' - ' . $w['wilayah_nm'] . '</option>';
			}
			$html .= '</select>';
			$html .= '<script>
					  $(function() {
					  ';
			// 
			$html .= ' $("#wilayah_prop").bind("change",function(e) {
				      e.preventDefault();
				      var i = $(this).val().split("#");
				      _get_wilayah(i[0]);
				    })
				    //
				    function _get_wilayah(i, prop="", kab="", kec="", kel="") {
				      var ext_var = "wilayah_prop="+prop;
				          ext_var+= "&wilayah_kab="+kab;
				          ext_var+= "&wilayah_kec="+kec;
				          ext_var+= "&wilayah_kel="+kel;
				      $.get("' . site_url("pelayanan/registrasi_offline/ajax_wilayah/get_wilayah_id_name") . '?wilayah_parent="+i+"&"+ext_var,null,function(data) {
				          $("#box_wilayah_kab").html(data.html);
				      },"json");
				    }
					  });
					  </script>
					 ';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		} else if ($id == 'empty_kabupaten') {
			$html = '';
			$html .= '<select name="wilayah_kab" id="wilayah_kab" class="chosen-select custom-select w-100">';
			$html .= '<option value="">- Kab/Kota -</option>';
			$html .= '</select>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		} else if ($id == 'empty_kecamatan') {
			$html = '';
			$html .= '<select name="wilayah_kec" id="wilayah_kec" class="chosen-select custom-select w-100">';
			$html .= '<option value="">- Kecamatan -</option>';
			$html .= '</select>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		} else if ($id == 'empty_kelurahan') {
			$html = '';
			$html .= '<select name="wilayah_kel" id="wilayah_kel" class="chosen-select custom-select w-100">';
			$html .= '<option value="">- Desa/Kelurahan -</option>';
			$html .= '</select>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}
	}

	public function insert_sep($id = null)
	{
		$data = $this->input->post(null, true);
		//
		$expld_diagawal = explode("#", $data['diagAwal']);
		//
		$provinsi = get_wilayah_id_bpjs(@$data['wilayah_prop']);
		$kabupaten = get_wilayah_id_bpjs(@$data['wilayah_kab']);
		$kecamatan = get_wilayah_id_bpjs(@$data['wilayah_kec']);
		// request
		$request = array(
			'request' =>
			array(
				't_sep' =>
				array(
					'noKartu' => $data['noKartu'],
					'tglSep' => to_date($data['tglSep']),
					'ppkPelayanan' => $data['ppkPelayanan'],
					'jnsPelayanan' => $data['jnsPelayanan'],
					'klsRawat' => $data['klsRawat'],
					'noMR' => $data['noMR'],
					'rujukan' =>
					array(
						'asalRujukan' => $data['asalRujukan'],
						'tglRujukan' => to_date($data['tglRujukan']),
						'noRujukan' => $data['noRujukan'],
						'ppkRujukan' => $data['ppkRujukan'],
					),
					'catatan' => $data['catatan'],
					'diagAwal' => $expld_diagawal[1],
					'poli' =>
					array(
						'tujuan' => $data['tujuan'],
						'eksekutif' => $data['eksekutif'],
					),
					'cob' =>
					array(
						'cob' => $data['cob'],
					),
					'katarak' =>
					array(
						'katarak' => $data['katarak'],
					),
					'jaminan' =>
					array(
						'lakaLantas' => $data['lakaLantas'],
						'penjamin' =>
						array(
							'penjamin' => $data['penjamin'],
							'tglKejadian' => to_date($data['tglKejadian']),
							'keterangan' => $data['keterangan'],
							'suplesi' =>
							array(
								'suplesi' => $data['suplesi'],
								'noSepSuplesi' => $data['noSepSuplesi'],
								'lokasiLaka' =>
								array(
									'kdPropinsi' => $provinsi,
									'kdKabupaten' => $kabupaten,
									'kdKecamatan' => $kecamatan,
								),
							),
						),
					),
					'skdp' =>
					array(
						'noSurat' => $data['noSurat'],
						'kodeDPJP' => $data['kodeDPJP'],
					),
					'noTelp' => $data['noTelp'],
					'user' => $data['user'],
				),
			),
		);
		//
		// $service_bpjs = $this->m_service_bpjs->get_first();
		$vclaim_conf = [
			'cons_id' => '26659',
			'secret_key' => '9lI684EDC2',
			'base_url' => 'https://dvlp.bpjs-kesehatan.go.id',
			'service_name' => 'vclaim-rest'
		];

		$peserta = new Nsulistiyawan\Bpjs\VClaim\Sep($vclaim_conf);
		$result = $peserta->insertSEP($request);
		// insert request & response
		$activity_vclaim = [
			'request_vclaim' => json_encode(@$request),
			'response_vclaim' => json_encode(@$result)
		];
		$this->m_registrasi_offline->insert_activity_vclaim($id, $activity_vclaim);
		// insert no sep jika sukses
		if ($result['metaData']['code'] == '200') {
			$this->m_registrasi_offline->insert_no_sep($id, @$result['response']['sep']['noSep']);
		}

		echo json_encode($result);
	}

	public function update_sep($id = null)
	{
		$data = $this->input->post(null, true);
		//
		$expld_diagawal = explode("#", $data['diagAwal']);
		//
		$provinsi = get_wilayah_id_bpjs(@$data['wilayah_prop']);
		$kabupaten = get_wilayah_id_bpjs(@$data['wilayah_kab']);
		$kecamatan = get_wilayah_id_bpjs(@$data['wilayah_kec']);
		// request
		$request = array(
			'request' =>
			array(
				't_sep' =>
				array(
					'noSep' => $data['noSep'],
					'klsRawat' => $data['klsRawat'],
					'noMR' => $data['noMR'],
					'rujukan' =>
					array(
						'asalRujukan' => $data['asalRujukan'],
						'tglRujukan' => to_date($data['tglRujukan']),
						'noRujukan' => $data['noRujukan'],
						'ppkRujukan' => $data['ppkRujukan'],
					),
					'catatan' => $data['catatan'],
					'diagAwal' => $expld_diagawal[1],
					'poli' =>
					array(
						'eksekutif' => $data['eksekutif'],
					),
					'cob' =>
					array(
						'cob' => $data['cob'],
					),
					'katarak' =>
					array(
						'katarak' => $data['katarak'],
					),
					'skdp' =>
					array(
						'noSurat' => $data['noSurat'],
						'kodeDPJP' => $data['kodeDPJP'],
					),
					'jaminan' =>
					array(
						'lakaLantas' => $data['lakaLantas'],
						'penjamin' =>
						array(
							'penjamin' => $data['penjamin'],
							'tglKejadian' => to_date($data['tglKejadian']),
							'keterangan' => $data['keterangan'],
							'suplesi' =>
							array(
								'suplesi' => $data['suplesi'],
								'noSepSuplesi' => $data['noSepSuplesi'],
								'lokasiLaka' =>
								array(
									'kdPropinsi' => $provinsi,
									'kdKabupaten' => $kabupaten,
									'kdKecamatan' => $kecamatan,
								),
							),
						),
					),
					'noTelp' => $data['noTelp'],
					'user' => $data['user'],
				),
			),
		);
		//
		// $service_bpjs = $this->m_service_bpjs->get_first();
		$vclaim_conf = [
			'cons_id' => '26659',
			'secret_key' => '9lI684EDC2',
			'base_url' => 'https://dvlp.bpjs-kesehatan.go.id',
			'service_name' => 'vclaim-rest'
		];

		$peserta = new Nsulistiyawan\Bpjs\VClaim\Sep($vclaim_conf);
		$result = $peserta->updateSEP($request);
		// insert request & response
		if ($result['metaData']['code'] == '200') {
			$activity_vclaim = [
				'request_vclaim' => json_encode(@$request),
				'response_vclaim' => json_encode(@$result)
			];
			$this->m_registrasi_offline->insert_activity_vclaim($id, $activity_vclaim);
		}

		echo json_encode($result);
	}

	public function delete($id = null)
	{
		$this->m_registrasi_offline->delete($id, true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log('_delete', $this->nav_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	public function multiple($type = null)
	{
		$data = $this->input->post();
		if (isset($data['checkitem'])) {
			foreach ($data['checkitem'] as $key) {
				switch ($type) {
					case 'delete':
						$this->authorize($this->nav, '_delete');
						$this->m_registrasi_offline->delete($key, true);
						$flash = 'Data berhasil dihapus.';
						create_log('_delete', $this->nav_id);
						break;

					case 'enable':
						$this->authorize($this->nav, '_update');
						$this->m_registrasi_offline->update($key, array('is_active' => 1));
						$flash = 'Data berhasil diaktifkan.';
						create_log('_update', $this->nav_id);
						break;

					case 'disable':
						$this->authorize($this->nav, '_update');
						$this->m_registrasi_offline->update($key, array('is_active' => 0));
						$flash = 'Data berhasil dinonaktifkan.';
						create_log('_delete', $this->nav_id);
						break;
				}
			}
		}
		// create_log($t,$this->this->menu['menu']);
		$this->session->set_flashdata('flash_success', $flash);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index/' . $this->cookie['cur_page']);
	}

	function ajax($type = null, $id = null)
	{
		if ($type == 'cek_id') {
			$data = $this->input->post();
			$cek = $this->m_registrasi_offline->get_data($data['lokasi_id']);
			if ($id == null) {
				if ($cek == null) {
					echo 'true';
				} else {
					echo 'false';
				}
			} else {
				if ($id != $data['lokasi_id'] && $cek != null) {
					echo 'false';
				} else {
					echo 'true';
				}
			}
		} else if ($type == 'get_antrian_cd') {
			$data = $this->input->post();
			$res = $this->m_lokasi->get_data($data['lokasi_id']);
			echo json_encode($res);
		} else if ($type == 'online_fill') {
			$data = $this->input->post();
			$res = $this->m_registrasi_offline->online_row($data['regonline_id']);
			echo json_encode($res);
		} else if ($type == 'pasien_fill') {
			$data = $this->input->post();
			$res = $this->m_registrasi_offline->pasien_row($data['pasien_id']);
			echo json_encode($res);
		} else if ($type == 'ibu_fill') {
			$data = $this->input->post();
			$res = $this->m_registrasi_offline->ibu_row($data['reg_id']);
			echo json_encode($res);
		} else if ($type == 'no_rm_fill') {
			$data = $this->input->post();
			$res = $this->m_registrasi_offline->no_rm_row($data['pasien_id']);
			echo json_encode($res);
		}

		if ($type == 'status_antrian') {
			$data = $this->input->post();
			$res = $this->m_caller_antrian->get_data($data['lokasi_id']);
			echo json_encode($res);
		}

		if ($type == 'call_antrian') {
			$data = $this->input->post();
			$res = $this->m_caller_antrian->call_antrian($data);
			echo json_encode($res);
		}

		if ($type == 'get_kelas') {
			$lokasi_id = $this->input->post('lokasi_id');
			$kelas_id = $this->input->post('kelas_id');
			// $jenispasien_id = $this->input->post('jenispasien_id');
			$list_kelas = $this->m_kelas_lokasi->all_data($lokasi_id);
			//
			$html = '';
			$html .= '<select name="kelas_id" id="kelas_id" class="chosen-select custom-select w-100">';
			// if (count($list_kelas) > 1 || count($list_kelas) == 0) {
			// }
			foreach ($list_kelas as $kelas) {
				if (@$kelas_id == $kelas['kelas_id']) {
					$html .= '<option value="' . $kelas['kelas_id'] . '" selected>' . $kelas['kelas_nm'] . '</option>';
				} else {
					$html .= '<option value="' . $kelas['kelas_id'] . '">' . $kelas['kelas_nm'] . '</option>';
				}
			}
			$html .= '</select>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}

		if ($type == 'check_nik') {
			$nik = $this->input->post('nik');
			$cek = $this->m_registrasi_offline->check_nik($nik);
			if ($cek != '') {
				$status = "true";
			} else {
				$status = "false";
			}
			//
			echo json_encode(array(
				'status' => $status,
				'pasien_id' => $cek['pasien_id']
			));
		}

		if ($type == 'list_rujukan') {
			$no_kartu = $this->input->post('no_kartu');
			//
			$vclaim_conf = [
				'cons_id' => '26659',
				'secret_key' => '9lI684EDC2',
				'base_url' => 'https://dvlp.bpjs-kesehatan.go.id',
				'service_name' => 'vclaim-rest'
			];

			$peserta = new Nsulistiyawan\Bpjs\VClaim\Rujukan($vclaim_conf);
			$result = $peserta->cariByNoKartu('', $no_kartu, true);

			$data['nav'] = $this->nav;
			$data['no_kartu'] = @$no_kartu;
			$data['code'] = @$result['metaData']['code'];
			$data['message'] = @$result['metaData']['message'];
			if (@$result['metaData']['code'] == 200) {
				$data['main'] = $result['response']['rujukan'];
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/registrasi_offline/list_rujukan', $data, true)
			));
		}

		if ($type == 'detail_rujukan') {
			$no_rujukan = $id;
			//
			$vclaim_conf = [
				'cons_id' => '26659',
				'secret_key' => '9lI684EDC2',
				'base_url' => 'https://dvlp.bpjs-kesehatan.go.id',
				'service_name' => 'vclaim-rest'
			];

			$peserta = new Nsulistiyawan\Bpjs\VClaim\Rujukan($vclaim_conf);
			$result = $peserta->cariByNoRujukan('', $no_rujukan);

			$data['nav'] = $this->nav;
			$data['no_rujukan'] = @$no_rujukan;
			$data['code'] = @$result['metaData']['code'];
			$data['message'] = @$result['metaData']['message'];
			if (@$result['metaData']['code'] == 200) {
				$data['main'] = $result['response']['rujukan'];
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/registrasi_offline/detail_rujukan', $data, true)
			));
		}

		if ($type == 'cari_sep') {
			$sep_no = $this->input->post('sep_no');
			//
			$vclaim_conf = [
				'cons_id' => '26659',
				'secret_key' => '9lI684EDC2',
				'base_url' => 'https://dvlp.bpjs-kesehatan.go.id',
				'service_name' => 'vclaim-rest'
			];

			$peserta = new Nsulistiyawan\Bpjs\VClaim\Sep($vclaim_conf);
			$result = $peserta->cariSEP($sep_no);

			$data['nav'] = $this->nav;
			$data['sep_no'] = @$sep_no;
			$data['code'] = @$result['metaData']['code'];
			$data['message'] = @$result['metaData']['message'];
			if (@$result['metaData']['code'] == 200) {
				$data['main'] = $result['response'];
			}

			echo json_encode($data);
		}

		if ($type == 'search-history') {
			$data = $this->m_registrasi_offline->history();
			echo json_encode($data);
		}
	}

	public function ibu_modal()
	{
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/registrasi_offline/ibu_modal', $data, true)
		));
	}

	public function get_data_ibu()
	{
		$list = $this->m_dt_registrasi_ibu->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field['pasien_id'];
			$row[] = $field['pasien_nm'] . ', ' . $field['sebutan_cd'];
			$row[] = $field['alamat'];
			$row[] = ($field['tmp_lahir'] != '') ? $field['tmp_lahir'] . ', ' : '' . @to_date($field['tgl_lahir']);
			$row[] = $field['jenispasien_nm'];
			$row[] = $field['lokasi_nm'];
			$row[] = $field['tgl_registrasi'];
			$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="ibu_fill(' . "'" . $field['reg_id'] . "'" . ')">Pilih >></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_dt_registrasi_ibu->count_all(),
			"recordsFiltered" => $this->m_dt_registrasi_ibu->count_filtered(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function online_modal()
	{
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/registrasi_offline/online_modal', $data, true)
		));
	}

	public function get_data_online()
	{
		$list = $this->m_dt_registrasi_online->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field['regonline_id'];
			$row[] = $field['tgl_periksa'];
			$row[] = $field['pasien_id'];
			$row[] = $field['pasien_nm'];
			$row[] = $field['alamat'];
			$row[] = $field['sex_cd'];
			$row[] = $field['tmp_lahir'] . ', ' . @to_date($field['tgl_lahir']);
			$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="online_fill(' . "'" . $field['regonline_id'] . "'" . ')">Pilih >></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_dt_registrasi_online->count_all(),
			"recordsFiltered" => $this->m_dt_registrasi_online->count_filtered(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function pasien_modal()
	{
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/registrasi_offline/pasien_modal', $data, true)
		));
	}

	public function get_data_pasien()
	{
		$list = $this->m_dt_mst_pasien->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field['pasien_id'];
			$row[] = $field['pasien_nm'] . ', ' . $field['sebutan_cd'];
			$row[] = $field['alamat'];
			$row[] = $field['sex_cd'];
			$row[] = $field['tmp_lahir'] . ', ' . @to_date($field['tgl_lahir']);
			$row[] = $field['jenispasien_nm'];
			$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="pasien_fill(' . "'" . $field['pasien_id'] . "'" . ')">Pilih >></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_dt_mst_pasien->count_all(),
			"recordsFiltered" => $this->m_dt_mst_pasien->count_filtered(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function caller_modal()
	{
		$data['nav'] = $this->nav;
		$data['caller'] = $this->m_registrasi_offline->get_caller();

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/registrasi_offline/caller_modal', $data, true)
		));
	}

	public function get_nik($nik)
	{
		$this->load->library('xmlrpc');
		$this->load->library('xmlrpcs');
		$url = "https://ayokitakerja.kemnaker.go.id/tools/check_nik/" . $nik;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		$data = curl_exec($ch);

		if (curl_errno($ch)) {
			$res = array('STATUS' => -1);
			echo json_encode($res);
		} else {
			$data = json_decode($data, TRUE);
			curl_close($ch);
			echo json_encode($data);
		}
	}

	public function cetak_modal($type = null, $reg_id = null, $sep_no = null)
	{
		$data['reg_id'] = @$reg_id;
		$data['sep_no'] = @$sep_no;
		$data['nav'] = $this->nav;
		$data['url'] = site_url() . '/pelayanan/registrasi_offline/' . $type . '/' . @$reg_id . '/' . @$sep_no;

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/registrasi_offline/cetak_modal', $data, true)
		));
	}

	public function cetak_sep_pdf($reg_id = null, $sep_no = null)
	{
		$date_now = date('Y-m-d H:i:s');
		$vclaim_conf = [
			// 'cons_id' => '26659',
			// 'secret_key' => '9lI684EDC2',
			// 'base_url' => 'https://dvlp.bpjs-kesehatan.go.id',
			// 'service_name' => 'vclaim-rest',
			'cons_id' => '24940',
			'secret_key' => '0qN6CB85B0',
			'base_url' => 'https://new-api.bpjs-kesehatan.go.id:8080',
			'service_name' => 'new-vclaim-rest'
		];

		$pasien = $this->m_registrasi_offline->get_data($reg_id);
		$request_vclaim = json_decode($pasien['request_vclaim'], true);
		// Cari SEP
		$peserta = new Nsulistiyawan\Bpjs\VClaim\Sep($vclaim_conf);
		$result = $peserta->cariSEP($sep_no);
		if (@$result['metaData']['code'] == 200) {
			$main = $result['response'];
		}
		// Car Fasilitas Kesehata/FASKES
		$kd_faskes = @$request_vclaim['request']['t_sep']['rujukan']['ppkRujukan'];
		$jns_faskes = '2'; // Faskes 1
		$referensi = new Nsulistiyawan\Bpjs\VClaim\Referensi($vclaim_conf);
		$res_referensi = $referensi->faskes($kd_faskes, $jns_faskes);
		if (@$res_referensi['metaData']['code'] == 200) {
			$faskes = $res_referensi['response'];
		}

		if (@$main['peserta']['kelamin'] == 'L') {
			$jns_kelamin = 'Laki-Laki';
		} elseif (@$main['peserta']['kelamin'] == 'P') {
			$jns_kelamin = 'Perempuan';
		} else {
			$jns_kelamin = '';
		}

		if (@$pasien['no_telp'] != '') {
			$no_telp = @$pasien['no_telp'];
		} else {
			$no_telp = '-';
		}

		if ($request_vclaim['request']['t_sep']['cob']['cob'] != '0') {
			$cob = $request_vclaim['request']['t_sep']['cob']['cob'];
		} else {
			$cob = '-';
		}

		if (@$main['penjamin'] != '') {
			$penjamin = @$main['penjamin'];
		} else {
			$penjamin = '-';
		}

		if (@$main['poli'] != '') {
			$poli = @$main['poli'];
		} else {
			$poli = @$pasien['lokasi_nm'];
		}

		if (@$faskes['faskes'][0]['nama'] != '') {
			$faskes_perujuk = @$faskes['faskes'][0]['nama'];
		} else {
			$faskes_perujuk = '-';
		}


		//generate pdf
		$this->load->library('pdf');
		$pdf = new Pdf('p', 'mm', array(210, 297));
		$pdf->AliasNbPages();
		$pdf->SetTitle('Cetak SEP ' . @$sep_no);
		$pdf->SetMargins(2, 2, 2);
		$pdf->AddPage();

		$pdf->Image(FCPATH . 'assets/images/icon/bpjs.png', 3, 3, 50, 8);
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(60, 2, '', 0, 1, 'L');
		$pdf->Cell(60, 6, '', 0, 0, 'L');
		$pdf->Cell(0, 3, 'SURAT ELEGIBILITAS PESERTA', 0, 1, 'L');
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(60, 6, '', 0, 0, 'L');
		$pdf->Cell(0, 5, 'RSUD DR. SOEDIRMAN KEBUMEN', 0, 1, 'L');
		$pdf->SetFont('Arial', '', 12);
		$pdf->Cell(0, 5, '', 0, 1, 'C');

		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(30, 5, 'No.SEP', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$main['noSep'], 0, 1, 'L');
		$pdf->Cell(30, 5, 'Tgl.SEP', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$main['tglSep'], 0, 0, 'L');
		$pdf->Cell(30, 5, 'Peserta', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(73, 5, @$main['peserta']['jnsPeserta'], 0, 1, 'L');
		$pdf->Cell(30, 5, 'No.Kartu', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$main['peserta']['noKartu'] . ' ( MR. ' . @$main['peserta']['noMr'] . ' )', 0, 0, 'L');
		$pdf->Cell(30, 5, 'COB', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(73, 5, @$cob, 0, 1, 'L');
		$pdf->Cell(30, 5, 'Nama Peserta', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$main['peserta']['nama'], 0, 0, 'L');
		$pdf->Cell(30, 5, 'Jns.Rawat', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(73, 5, @$main['jnsPelayanan'], 0, 1, 'L');
		$pdf->Cell(30, 5, 'Tgl.Lahir', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$main['peserta']['tglLahir'] . ' Kelamin : ' . @$jns_kelamin, 0, 0, 'L');
		$pdf->Cell(30, 5, 'Kls.Rawat', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(73, 5, 'Kelas ' . @$main['kelasRawat'], 0, 1, 'L');
		$pdf->Cell(30, 5, 'No.Telepon', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$no_telp, 0, 0, 'L');
		$pdf->Cell(30, 5, 'Penjamin', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$penjamin, 0, 1, 'L');
		$pdf->Cell(30, 5, 'Sub/Spesialis', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$poli, 0, 1, 'L');
		$pdf->Cell(30, 5, 'Faskes Perujuk', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$faskes_perujuk, 0, 1, 'L');
		$pdf->Cell(30, 5, 'Diagnosa Awal', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$main['diagnosa'], 0, 1, 'L');
		$pdf->Cell(30, 5, 'Catatan', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$main['catatan'], 0, 1, 'L');
		$pdf->Cell(90, 3, '', 0, 1, 'L');

		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(150, 3.5, '*Saya menyetujui BPJS Kesehatan menggunakan informasi medis pasien jika diperlukan.', 0, 0, 'L');
		$pdf->Cell(60, 3.5, 'Pasien/Keluarga Pasien', 0, 1, 'C');
		$pdf->Cell(150, 3.5, '*SEP Bukan sebagai bukti penjaminan peserta', 0, 1, 'L');

		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(90, 5, '', 0, 1, 'L');
		$pdf->Cell(150, 3.5, 'Cetakan ke 1 ' . to_date(@$date_now, '', 'full_date') . ' WIB', 0, 1, 'L');
		$pdf->Cell(90, 5, '', 0, 1, 'L');

		$pdf->Output('I', 'SEP_' . @$main['noSep'] . '.pdf');
	}
}
