<?php 
if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Registrasi_online extends MY_Controller{

	var $nav_id = '03.01.02', $nav, $cookie;
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array(
			'm_registrasi_online',
			'master/m_lokasi',
			'master/m_jenis_pasien'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
    $this->cookie = get_cookie_nav($this->nav_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('tgl_periksa_from' => '', 'tgl_periksa_to' => '', 'no_rm_nm' => '', 'lokasi_id' => '', 'jenispasien_id' => '', 'is_have_norm' => '', 'is_have_rujukan' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'tgl_periksa','type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}
	
	public function index() {	
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(4, 0);
    $this->cookie['total_rows'] = $this->m_registrasi_online->all_rows($this->cookie);
    set_cookie_nav($this->nav_id, $this->cookie);
    //main data
    $data['nav'] = $this->nav;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_registrasi_online->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
		$data['lokasi'] = $this->m_lokasi->by_field('jenisreg_st', 1, 'result');
		$data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
    //set pagination
    set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('pelayanan/registrasi_online/index',$data);
	}

	public function detail($id=null) {
		$data['main'] = $this->m_registrasi_online->get_data($id);
		$data['id'] = $id;
		$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/save/'.$id;
			
		echo json_encode(array(
			'html' => $this->load->view('pelayanan/registrasi_online/detail', $data, true)
		));
	}

	public function delete($id = null)
	{
		$this->m_registrasi_online->delete($id, true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log('_delete', $this->nav_id);
		redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	public function multiple($type = null)
	{
		$data = $this->input->post();
    if(isset($data['checkitem'])){
      foreach ($data['checkitem'] as $key) {
        switch ($type) {					
					case 'delete':
						$this->authorize($this->nav, '_delete');
            $this->m_registrasi_online->delete($key, true);
						$flash = 'Data berhasil dihapus.';
						create_log('_delete', $this->nav_id);
            break;

					case 'enable':
						$this->authorize($this->nav, '_update');
            $this->m_registrasi_online->update($key, array('is_active' => 1));
						$flash = 'Data berhasil diaktifkan.';
						create_log('_update', $this->nav_id);
            break;

					case 'disable':
						$this->authorize($this->nav, '_update');
            $this->m_registrasi_online->update($key, array('is_active' => 0));
						$flash = 'Data berhasil dinonaktifkan.';
						create_log('_delete', $this->nav_id);
            break;
        }
      }
    }
    create_log($t,$this->this->menu['menu']);
    $this->session->set_flashdata('flash_success', $flash);
    redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}
	
}