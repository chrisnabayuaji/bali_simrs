<?php 
if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Video_display extends MY_Controller{

	var $nav_id = '03.05.04', $nav, $cookie;
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array(
			'm_video_display'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
    $this->cookie = get_cookie_nav($this->nav_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'info_id','type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}
	
	public function index() {	
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(4, 0);
    $this->cookie['total_rows'] = $this->m_video_display->all_rows($this->cookie);
    set_cookie_nav($this->nav_id, $this->cookie);
    //main data
    $data['nav'] = $this->nav;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_video_display->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
    //set pagination
    set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('pelayanan/video_display/index',$data);
	}

	public function form_modal($id=null) {
    $this->authorize($this->nav, ($id != '' ) ? '_update' : '_add');
		
		if($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_video_display->get_data($id);
		}
		if ($id !='') {
			$data['id'] = $id;
		}else{
			$data['id'] = $this->m_video_display->get_id();
		}
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/save/'.$id;
			
		echo json_encode(array(
			'html' => $this->load->view('pelayanan/video_display/form_modal', $data, true)
		));
	}
	
	public function save($id = null)
	{
		$this->m_video_display->save($id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($id != '' ) ? '_update' : '_add', $this->nav_id);
		redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	function upload_video($name_file='', $info_id='') {
		$this->m_video_display->insert_video($name_file, $info_id);
	}

	public function delete($id = null)
	{
		$this->m_video_display->delete($id, true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log('_delete', $this->nav_id);
		redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	public function multiple($type = null)
	{
		$data = $this->input->post();
    if(isset($data['checkitem'])){
      foreach ($data['checkitem'] as $key) {
        switch ($type) {					
					case 'delete':
						$this->authorize($this->nav, '_delete');
            $this->m_video_display->delete($key, true);
						$flash = 'Data berhasil dihapus.';
						create_log('_delete', $this->nav_id);
            break;

					case 'enable':
						$this->authorize($this->nav, '_update');
            $this->m_video_display->update($key, array('is_active' => 1));
						$flash = 'Data berhasil diaktifkan.';
						create_log('_update', $this->nav_id);
            break;

					case 'disable':
						$this->authorize($this->nav, '_update');
            $this->m_video_display->update($key, array('is_active' => 0));
						$flash = 'Data berhasil dinonaktifkan.';
						create_log('_delete', $this->nav_id);
            break;
        }
      }
    }
    create_log($t,$this->this->menu['menu']);
    $this->session->set_flashdata('flash_success', $flash);
    redirect(site_url().'/'.$this->nav['nav_url'].'/index/'.$this->cookie['cur_page']);
	}

	function ajax($id=null) {
		if($id == 'get_video_01') {
			$info_id = $this->input->get('id');
			$data['nav'] = $this->nav;
			//
			$data['get_data'] = $this->m_video_display->get_data($info_id);
			$data['file_name'] = 'file_media_01';
			//
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/video_display/_button', $data, true)
			));
		}elseif($id == 'get_video_02') {
			$info_id = $this->input->get('id');
			$data['nav'] = $this->nav;
			//
			$data['get_data'] = $this->m_video_display->get_data($info_id);
			$data['file_name'] = 'file_media_02';
			//
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/video_display/_button', $data, true)
			));
		}elseif($id == 'get_video_03') {
			$info_id = $this->input->get('id');
			$data['nav'] = $this->nav;
			//
			$data['get_data'] = $this->m_video_display->get_data($info_id);
			$data['file_name'] = 'file_media_03';
			//
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/video_display/_button', $data, true)
			));
		}elseif($id == 'get_video_04') {
			$info_id = $this->input->get('id');
			$data['nav'] = $this->nav;
			//
			$data['get_data'] = $this->m_video_display->get_data($info_id);
			$data['file_name'] = 'file_media_04';
			//
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/video_display/_button', $data, true)
			));
		}elseif($id == 'get_video_05') {
			$info_id = $this->input->get('id');
			$data['nav'] = $this->nav;
			//
			$data['get_data'] = $this->m_video_display->get_data($info_id);
			$data['file_name'] = 'file_media_05';
			//
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/video_display/_button', $data, true)
			));
		}elseif($id == 'delete_video') {
			$name = $this->input->get('name');
			$info_id = $this->input->get('info_id');
			$data['nav'] = $this->nav;
			// delete video
			$this->m_video_display->delete_video($name, $info_id);
			//
			$data['get_data'] = $this->m_video_display->get_data($info_id);
			//
			$html = '<div class="mt-n2"><small class="text-danger font-weight-semibold">Tidak ada video</small></div>';
			echo json_encode(array(
				'html' => $html
			));
		}
	}
	
}