<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Kunjungan extends MY_Controller
{

	var $nav_id = '03.01.05', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'm_reg_ranap_kunjungan',
			'master/m_lokasi',
			'master/m_jenis_pasien',
			'master/m_lokasi',
			'master/m_wilayah',
			'master/m_kelas',
			'master/m_jenis_pasien',
			'master/m_pegawai',
			'master/m_kelas_lokasi',
			'master/m_kamar',
			'm_dt_registrasi_ibu',
			'm_dt_mst_pasien',
			'm_dt_kamar',
			'master/m_identitas',
			'm_dt_diagnosis'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id . '.kunjungan');
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('tgl_registrasi_from' => '', 'tgl_registrasi_to' => '', 'no_rm_nm' => '', 'lokasi_id' => '', 'jenispasien_id' => '', 'periksa_st_lab' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'tgl_registrasi', 'type' => 'desc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(5, 0);
		$this->cookie['total_rows'] = $this->m_reg_ranap_kunjungan->all_rows($this->cookie);
		set_cookie_nav($this->nav_id . '.kunjungan', $this->cookie);
		//main data
		$data['nav'] = $this->nav;
		$data['cookie'] = $this->cookie;
		$data['main'] = $this->m_reg_ranap_kunjungan->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
		$data['lokasi'] = $this->m_lokasi->by_field('jenisreg_st', 1, 'result');
		$data['lokasi_bangsal'] = $this->m_lokasi->by_field('jenisreg_st', 2, 'result');
		$data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
		$data['total_verifikasi'] = $this->m_reg_ranap_kunjungan->total_verifikasi();
		//set pagination
		set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('pelayanan/registrasi_ranap/kunjungan/index', $data);
	}

	public function form_modal()
	{
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/insert_bayi/';

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/registrasi_ranap/kunjungan/form_modal', $data, true)
		));
	}

	public function form($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
			$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_profile();
		} else {
			$data['main'] = $this->m_reg_ranap_kunjungan->get_data($id);
			if ($data['main']['wilayah_st'] == 'L') {
				$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_parent('');
			} else {
				$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_profile();
			}
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['lokasi_asal'] = $this->m_lokasi->by_field('is_active', 1, 'result');
		$data['lokasi_bangsal'] = $this->m_lokasi->by_field('jenisreg_st', 2, 'result');
		$data['dokter'] = $this->m_pegawai->by_field('jenispegawai_cd', '02', 'result');
		$data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
		$data['form_action'] = site_url() . '/' . 'pelayanan/registrasi_ranap/kunjungan' . '/save/' . $id;

		$this->render('pelayanan/registrasi_ranap/kunjungan/form', $data);
	}

	public function form_edit($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		$data['main'] = $this->m_reg_ranap_kunjungan->get_data($id);
		if ($data['main']['wilayah_st'] == 'L') {
			$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_parent('');
		} else {
			$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_profile();
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['lokasi_asal'] = $this->m_lokasi->by_field('is_active', 1, 'result');
		$data['lokasi_bangsal'] = $this->m_lokasi->by_field('jenisreg_st', 2, 'result');
		$data['dokter'] = $this->m_pegawai->by_field('jenispegawai_cd', '02', 'result');
		$data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
		$data['form_action'] = site_url() . '/' . 'pelayanan/registrasi_ranap/kunjungan' . '/save_edit/' . $id;

		$this->render('pelayanan/registrasi_ranap/kunjungan/form_edit', $data);
	}

	public function form_bayi($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		$data['main'] = $this->m_reg_ranap_kunjungan->get_data($id);
		if ($data['main']['wilayah_st'] == 'L') {
			$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_parent('');
		} else {
			$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_profile();
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['lokasi_asal'] = $this->m_lokasi->by_field('is_active', 1, 'result');
		$data['lokasi_bangsal'] = $this->m_lokasi->by_field('jenisreg_st', 2, 'result');
		$data['kelas'] = $this->m_kelas->list_kelas();
		$data['dokter'] = $this->m_pegawai->by_field('jenispegawai_cd', '02', 'result');
		$data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
		$data['form_action'] = site_url() . '/' . 'pelayanan/registrasi_ranap/kunjungan' . '/save/' . $id;

		$this->render('pelayanan/registrasi_ranap/kunjungan/form_bayi', $data);
	}

	public function pembuatan_sep($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_reg_ranap_kunjungan->get_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['lokasi'] = $this->m_lokasi->by_field('jenisreg_st', 1, 'result');
		$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_parent('');
		$data['identitas_rs'] = $this->m_identitas->get_first();
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/insert_sep/' . $id;

		$this->render('pelayanan/registrasi_ranap/kunjungan/pembuatan_sep', $data);
	}

	public function ubah_sep($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_reg_ranap_kunjungan->get_data($id);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['lokasi'] = $this->m_lokasi->by_field('jenisreg_st', 1, 'result');
		$data['request_vclaim'] = json_decode($data['main']['request_vclaim'], true);
		$data['diagnosis'] = $this->m_dt_diagnosis->get_data_by_icdx($data['request_vclaim']['request']['t_sep']['diagAwal']);
		$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_parent('');
		$data['identitas_rs'] = $this->m_identitas->get_first();
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/update_sep/' . $id;

		$this->render('pelayanan/registrasi_ranap/kunjungan/ubah_sep', $data);
	}

	public function info_kamar()
	{
		$data['main'] = $this->m_kamar->all_data();
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/registrasi_ranap/kunjungan/info_kamar', $data, true)
		));
	}

	public function pindah_bangsal($reg_id = null, $pasien_id = null, $kelas_id = null, $lokasi_id = null, $kamar_id = null)
	{
		$this->authorize($this->nav, ($reg_id != '') ? '_update' : '_add');

		if ($reg_id == null) {
			$data['main'] = array();
		} else {
			$data['main'] = $this->m_reg_ranap_kunjungan->get_data_pindah_bangsal($reg_id, $pasien_id, $kelas_id, $lokasi_id, $kamar_id);
		}
		$data['reg_pasien'] = $this->m_reg_ranap_kunjungan->get_reg_pasien($reg_id);
		$data['reg_id'] = $reg_id;
		$data['nav'] = $this->nav;
		$data['lokasi_bangsal'] = $this->m_lokasi->by_field('jenisreg_st', 2, 'result');
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save_pindah_bangsal/' . $reg_id;

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/registrasi_ranap/kunjungan/pindah_bangsal', $data, true)
		));
	}

	public function riwayat_bangsal($reg_id = null)
	{
		$this->authorize($this->nav, ($reg_id != '') ? '_update' : '_add');

		$data['reg_id'] = $reg_id;
		$data['nav'] = $this->nav;
		$data['main'] = $this->m_reg_ranap_kunjungan->list_riwayat_bangsal($reg_id);

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/registrasi_ranap/kunjungan/riwayat_bangsal', $data, true)
		));
	}

	public function save_pindah_bangsal($reg_id = null)
	{
		$this->m_reg_ranap_kunjungan->save_pindah_bangsal($reg_id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($reg_id != '') ? '_update' : '_add', $this->nav_id);
		redirect(site_url() . '/' . $this->nav['nav_url'] . '/index');
	}

	public function save($id = null)
	{
		$this->m_reg_ranap_kunjungan->save($id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($id != '') ? '_update' : '_add', $this->nav_id);
		redirect(site_url() . '/' . 'pelayanan/registrasi_ranap/kunjungan' . '/index/');
	}

	public function save_edit($id = null)
	{
		$this->m_reg_ranap_kunjungan->save_edit($id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($id != '') ? '_update' : '_add', $this->nav_id);
		redirect(site_url() . '/' . 'pelayanan/registrasi_ranap/kunjungan' . '/index/');
	}

	public function delete($id)
	{
		$this->m_reg_ranap_kunjungan->delete($id, true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log(($id != '') ? '_update' : '_add', $this->nav_id);
		redirect(site_url() . '/' . 'pelayanan/registrasi_ranap/kunjungan' . '/index/');
	}

	public function insert_sep($id = null)
	{
		$data = $this->input->post(null, true);
		//
		$expld_diagawal = explode("#", $data['diagAwal']);
		//
		$provinsi = get_wilayah_id_bpjs(@$data['wilayah_prop']);
		$kabupaten = get_wilayah_id_bpjs(@$data['wilayah_kab']);
		$kecamatan = get_wilayah_id_bpjs(@$data['wilayah_kec']);
		// request
		$request = array(
			'request' =>
			array(
				't_sep' =>
				array(
					'noKartu' => $data['noKartu'],
					'tglSep' => to_date($data['tglSep']),
					'ppkPelayanan' => $data['ppkPelayanan'],
					'jnsPelayanan' => $data['jnsPelayanan'],
					'klsRawat' => $data['klsRawat'],
					'noMR' => $data['noMR'],
					'rujukan' =>
					array(
						'asalRujukan' => $data['asalRujukan'],
						'tglRujukan' => to_date($data['tglRujukan']),
						'noRujukan' => $data['noRujukan'],
						'ppkRujukan' => $data['ppkRujukan'],
					),
					'catatan' => $data['catatan'],
					'diagAwal' => $expld_diagawal[1],
					'poli' =>
					array(
						'tujuan' => $data['tujuan'],
						'eksekutif' => $data['eksekutif'],
					),
					'cob' =>
					array(
						'cob' => $data['cob'],
					),
					'katarak' =>
					array(
						'katarak' => $data['katarak'],
					),
					'jaminan' =>
					array(
						'lakaLantas' => $data['lakaLantas'],
						'penjamin' =>
						array(
							'penjamin' => $data['penjamin'],
							'tglKejadian' => to_date($data['tglKejadian']),
							'keterangan' => $data['keterangan'],
							'suplesi' =>
							array(
								'suplesi' => $data['suplesi'],
								'noSepSuplesi' => $data['noSepSuplesi'],
								'lokasiLaka' =>
								array(
									'kdPropinsi' => $provinsi,
									'kdKabupaten' => $kabupaten,
									'kdKecamatan' => $kecamatan,
								),
							),
						),
					),
					'skdp' =>
					array(
						'noSurat' => $data['noSurat'],
						'kodeDPJP' => $data['kodeDPJP'],
					),
					'noTelp' => $data['noTelp'],
					'user' => $data['user'],
				),
			),
		);
		//
		// $service_bpjs = $this->m_service_bpjs->get_first();
		$vclaim_conf = [
			'cons_id' => '26659',
			'secret_key' => '9lI684EDC2',
			'base_url' => 'https://dvlp.bpjs-kesehatan.go.id',
			'service_name' => 'vclaim-rest'
		];

		$peserta = new Nsulistiyawan\Bpjs\VClaim\Sep($vclaim_conf);
		$result = $peserta->insertSEP($request);
		// insert request & response
		$activity_vclaim = [
			'request_vclaim' => json_encode(@$request),
			'response_vclaim' => json_encode(@$result)
		];
		$this->m_reg_ranap_kunjungan->insert_activity_vclaim($id, $activity_vclaim);
		// insert no sep jika sukses
		if ($result['metaData']['code'] == '200') {
			$this->m_reg_ranap_kunjungan->insert_no_sep($id, @$result['response']['sep']['noSep']);
		}

		echo json_encode($result);
	}

	public function update_sep($id = null)
	{
		$data = $this->input->post(null, true);
		//
		$expld_diagawal = explode("#", $data['diagAwal']);
		//
		$provinsi = get_wilayah_id_bpjs(@$data['wilayah_prop']);
		$kabupaten = get_wilayah_id_bpjs(@$data['wilayah_kab']);
		$kecamatan = get_wilayah_id_bpjs(@$data['wilayah_kec']);
		// request
		$request = array(
			'request' =>
			array(
				't_sep' =>
				array(
					'noSep' => $data['noSep'],
					'klsRawat' => $data['klsRawat'],
					'noMR' => $data['noMR'],
					'rujukan' =>
					array(
						'asalRujukan' => $data['asalRujukan'],
						'tglRujukan' => to_date($data['tglRujukan']),
						'noRujukan' => $data['noRujukan'],
						'ppkRujukan' => $data['ppkRujukan'],
					),
					'catatan' => $data['catatan'],
					'diagAwal' => $expld_diagawal[1],
					'poli' =>
					array(
						'eksekutif' => $data['eksekutif'],
					),
					'cob' =>
					array(
						'cob' => $data['cob'],
					),
					'katarak' =>
					array(
						'katarak' => $data['katarak'],
					),
					'skdp' =>
					array(
						'noSurat' => $data['noSurat'],
						'kodeDPJP' => $data['kodeDPJP'],
					),
					'jaminan' =>
					array(
						'lakaLantas' => $data['lakaLantas'],
						'penjamin' =>
						array(
							'penjamin' => $data['penjamin'],
							'tglKejadian' => to_date($data['tglKejadian']),
							'keterangan' => $data['keterangan'],
							'suplesi' =>
							array(
								'suplesi' => $data['suplesi'],
								'noSepSuplesi' => $data['noSepSuplesi'],
								'lokasiLaka' =>
								array(
									'kdPropinsi' => $provinsi,
									'kdKabupaten' => $kabupaten,
									'kdKecamatan' => $kecamatan,
								),
							),
						),
					),
					'noTelp' => $data['noTelp'],
					'user' => $data['user'],
				),
			),
		);
		//
		// $service_bpjs = $this->m_service_bpjs->get_first();
		$vclaim_conf = [
			'cons_id' => '26659',
			'secret_key' => '9lI684EDC2',
			'base_url' => 'https://dvlp.bpjs-kesehatan.go.id',
			'service_name' => 'vclaim-rest'
		];

		$peserta = new Nsulistiyawan\Bpjs\VClaim\Sep($vclaim_conf);
		$result = $peserta->updateSEP($request);
		// insert request & response
		if ($result['metaData']['code'] == '200') {
			$activity_vclaim = [
				'request_vclaim' => json_encode(@$request),
				'response_vclaim' => json_encode(@$result)
			];
			$this->m_reg_ranap_kunjungan->insert_activity_vclaim($id, $activity_vclaim);
		}

		echo json_encode($result);
	}

	// List Data Ibu
	public function ajax_list_ibu($type = null, $id = null)
	{
		if ($type == 'search_ibu') {
			$nav = $this->nav;
			$list = $this->m_dt_registrasi_ibu->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$alamat = '';
				$alamat .= ($field['alamat'] != '') ? $field['alamat'] . ', <br>' : '';
				$alamat .= ($field['kelurahan'] != '') ? ucwords(strtolower($field['kelurahan'])) . ', ' : '';
				$alamat .= ($field['kecamatan'] != '') ? ucwords(strtolower($field['kecamatan'])) . ', ' : '';
				$alamat .= ($field['kabupaten'] != '') ? ucwords(strtolower($field['kabupaten'])) . ', ' : '';
				$alamat .= ($field['provinsi'] != '') ? ucwords(strtolower($field['provinsi'])) : '';
				$row = array();
				$row[] = $no;
				$row[] = $field['pasien_id'];
				$row[] = '<b>' . $field['pasien_nm'] . ', ' . $field['sebutan_cd'] . '</b><br>';
				$row[] = $field['sex_cd'];
				$row[] = $alamat;
				$row[] = $field['lokasi_nm'] . '<br><b>' . $field['jenispasien_nm'] . '</b>';
				$row[] = to_date($field['tgl_registrasi'], '', 'full_date');
				$row[] = '<a href="' . site_url() . '/' . $nav['nav_url'] . '/form_bayi/' . $field['reg_id'] . '" type="button" class="btn btn-xs btn-primary pt-1 pb-1">Pilih Ibu >></a>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_registrasi_ibu->count_all(),
				"recordsFiltered" => $this->m_dt_registrasi_ibu->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}
	}

	// Diagnosis
	public function ajax_diagnosis($type = null, $id = null)
	{
		if ($type == 'autocomplete') {
			$penyakit_nm = $this->input->get('penyakit_nm');
			$res = $this->m_identitas->penyakit_autocomplete($penyakit_nm);
			echo json_encode($res);
		} elseif ($type == 'search_diagnosis') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/registrasi_ranap/kunjungan/search_diagnosis', $data, true)
			));
		} elseif ($type == 'search_data') {
			$list = $this->m_dt_diagnosis->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['penyakit_id'];
				$row[] = $field['icdx'];
				$row[] = $field['penyakit_nm'];
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="diagnosis_fill(' . "'" . $field['penyakit_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_diagnosis->count_all(),
				"recordsFiltered" => $this->m_dt_diagnosis->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'diagnosis_fill') {
			$data = $this->input->post();
			$res = $this->m_identitas->diagnosis_row($data['penyakit_id']);
			echo json_encode($res);
		}
	}

	function ajax_wilayah($id = null)
	{
		if ($id == 'get_wilayah_id_name') {
			$wilayah_parent = $this->input->get('wilayah_parent');
			$wilayah_prop = $this->input->get('wilayah_prop');
			$wilayah_kab = $this->input->get('wilayah_kab');
			$wilayah_kec = $this->input->get('wilayah_kec');
			$wilayah_kel = $this->input->get('wilayah_kel');
			//
			$wilayah_params = $this->m_wilayah->get_params($wilayah_parent);
			//
			$wilayah_selected = '';
			if ($wilayah_params['level'] == '2' && $wilayah_kab != '') { // kab
				$wilayah_selected = $wilayah_kab;
			} else if ($wilayah_params['level'] == '3' && $wilayah_kec != '') { // kec
				$wilayah_selected = $wilayah_kec;
			} else if ($wilayah_params['level'] == '4' && $wilayah_kel != '') { // kel
				$wilayah_selected = $wilayah_kel;
			}
			//
			$list_wilayah = $this->m_wilayah->list_wilayah_by_parent($wilayah_parent);
			//
			$html = '';
			if ($wilayah_parent == '' && $wilayah_prop == '' && $wilayah_kab == '' && $wilayah_kec == '' && $wilayah_kel == '') {
				$html .= '<select class="chosen-select custom-select w-100" name="wilayah_kab" id="wilayah_kab">
                    <option value="">- Kab/Kota -</option>
                  </select>';
			} else {
				$html .= '<select name="' . $wilayah_params['input_nm'] . '" id="' . $wilayah_params['input_nm'] . '" class="chosen-select custom-select w-100">';
				$html .= '<option value="">- ' . $wilayah_params['level_nm'] . ' -</option>';
				foreach ($list_wilayah as $w) {
					if ($wilayah_selected == $w['wilayah_id']) {
						$html .= '<option value="' . $w['wilayah_id'] . '#' . $w['wilayah_nm'] . '" selected>' . $w['wilayah_id'] . ' - ' . $w['wilayah_nm'] . '</option>';
					} else {
						$html .= '<option value="' . $w['wilayah_id'] . '#' . $w['wilayah_nm'] . '">' . $w['wilayah_id'] . ' - ' . $w['wilayah_nm'] . '</option>';
					}
				}
				$html .= '</select>';
				$html .= '<script>
						  $(function() {
						  ';
				// autoload
				if ($wilayah_selected != '') {
					$html .= 	'_get_wilayah("' . @$wilayah_selected . '","' . @$wilayah_prop . '", "' . @$wilayah_kab . '", "' . @$wilayah_kec . '", "' . @$wilayah_kel . '"); ';
				}
				// 
				$html .= '	$("#' . $wilayah_params['input_nm'] . '").bind("change",function(e) {
						  		e.preventDefault();
						  		var i = $(this).val().split("#");
		    					_get_wilayah(i[0]);
						  	});
						  	//
						  	function _get_wilayah(i, prop="", kab="", kec="", kel="") {
						  		var ext_var = "wilayah_prop="+prop;
					                ext_var+= "&wilayah_kab="+kab;
					                ext_var+= "&wilayah_kec="+kec;
					                ext_var+= "&wilayah_kel="+kel;

						  		$.get("' . site_url('pelayanan/registrasi_ranap/kunjungan/ajax_wilayah/get_wilayah_id_name') . '?wilayah_parent="+i+"&"+ext_var,null,function(data) {
						  			$("#box_' . $wilayah_params['input_nm_next'] . '").html(data.html);
						  		},"json");
						  	}
						  });
						  </script>
						 ';
			}
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		} else if ($id == 'get_provinsi_by_st') {
			$wilayah_st = $this->input->get('wilayah_st');
			if ($wilayah_st == 'D') {
				$list_wilayah_prop = $this->m_wilayah->list_wilayah_by_profile();
			} else {
				$list_wilayah_prop = $this->m_wilayah->list_wilayah_by_parent('');
			}
			//
			$html = '';
			$html .= '<select name="wilayah_prop" id="wilayah_prop" class="chosen-select custom-select w-100">';
			$html .= '<option value="">- Propinsi -</option>';
			foreach ($list_wilayah_prop as $w) {
				$html .= '<option value="' . $w['wilayah_id'] . '#' . $w['wilayah_nm'] . '">' . $w['wilayah_id'] . ' - ' . $w['wilayah_nm'] . '</option>';
			}
			$html .= '</select>';
			$html .= '<script>
					  $(function() {
					  ';
			// 
			$html .= ' $("#wilayah_prop").bind("change",function(e) {
				      e.preventDefault();
				      var i = $(this).val().split("#");
				      _get_wilayah(i[0]);
				    })
				    //
				    function _get_wilayah(i, prop="", kab="", kec="", kel="") {
				      var ext_var = "wilayah_prop="+prop;
				          ext_var+= "&wilayah_kab="+kab;
				          ext_var+= "&wilayah_kec="+kec;
				          ext_var+= "&wilayah_kel="+kel;
				      $.get("' . site_url("pelayanan/registrasi_ranap/kunjungan/ajax_wilayah/get_wilayah_id_name") . '?wilayah_parent="+i+"&"+ext_var,null,function(data) {
				          $("#box_wilayah_kab").html(data.html);
				      },"json");
				    }
					  });
					  </script>
					 ';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		} else if ($id == 'empty_kabupaten') {
			$html = '';
			$html .= '<select name="wilayah_kab" id="wilayah_kab" class="chosen-select custom-select w-100">';
			$html .= '<option value="">- Kab/Kota -</option>';
			$html .= '</select>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		} else if ($id == 'empty_kecamatan') {
			$html = '';
			$html .= '<select name="wilayah_kec" id="wilayah_kec" class="chosen-select custom-select w-100">';
			$html .= '<option value="">- Kecamatan -</option>';
			$html .= '</select>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		} else if ($id == 'empty_kelurahan') {
			$html = '';
			$html .= '<select name="wilayah_kel" id="wilayah_kel" class="chosen-select custom-select w-100">';
			$html .= '<option value="">- Desa/Kelurahan -</option>';
			$html .= '</select>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}
	}

	function ajax($type = null, $id = null)
	{
		if ($type == 'get_kelas') {
			$lokasi_id = $this->input->post('lokasi_id');
			$kelas_id = $this->input->post('kelas_id');
			$list_kelas = $this->m_kelas_lokasi->all_data($lokasi_id);
			//
			$html = '';
			$html .= '<select name="src_kelas_id" id="src_kelas_id" class="chosen-select custom-select w-100" required="">';
			if (count($list_kelas) > 1 || count($list_kelas) == 0) {
				$html .= '<option value="">-- Pilih --</option>';
			}
			foreach ($list_kelas as $kelas) {
				if (@$kelas_id == $kelas['kelas_id']) {
					$html .= '<option value="' . $kelas['kelas_id'] . '" selected>' . $kelas['kelas_nm'] . '</option>';
				} else {
					$html .= '<option value="' . $kelas['kelas_id'] . '">' . $kelas['kelas_nm'] . '</option>';
				}
			}
			$html .= '</select>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}

		if ($type == 'get_kamar') {
			$lokasi_id = $this->input->post('lokasi_id');
			$kamar_id = $this->input->post('kamar_id');
			$list_kamar = $this->m_kamar->all_data($lokasi_id);
			//
			$html = '';
			$html .= '<select name="kamar_id" id="kamar_id" class="chosen-select custom-select w-100" required="">';
			$html .= '<option value="">---</option>';
			foreach ($list_kamar as $kamar) {
				if ($kamar['kamar_id'] == $kamar_id) {
					$html .= '<option value="' . $kamar['kamar_id'] . '" selected>' . $kamar['kamar_nm'] . ' (' . $kamar['jml_bed_kosong'] . ')</option>';
				} else {
					$html .= '<option value="' . $kamar['kamar_id'] . '">' . $kamar['kamar_nm'] . ' (' . $kamar['jml_bed_kosong'] . ')</option>';
				}
			}
			$html .= '</select>';
			$html .= js_chosen();
			$html .= "<script>$('#kamar_id').bind('change',function(e) {
				e.preventDefault();
				var i = $(this).val();
				var jns_rawat_cd = $('#jns_rawat_cd').val();
				_cek_kamar(i, jns_rawat_cd);
			})</script>";
			//
			echo json_encode(array(
				'html' => $html,
			));
		}

		if ($type == 'cek_kamar') {
			$jns_rawat_cd = $this->input->post('jns_rawat_cd');
			$kamar_id = $this->input->post('kamar_id');
			$cek = $this->m_kamar->by_field('kamar_id', $kamar_id, 'row');
			if ($jns_rawat_cd == '01') {
				$status = true;
			} else {
				if ($cek['jml_bed_kosong'] == 0 || $cek['jml_bed_kosong'] == NULL) {
					$status = false;
				} else {
					$status = true;
				}
			}
			//
			echo json_encode(array(
				'status' => $status,
			));
		}

		if ($type == 'get_kamar_pindah_bangsal') {
			$lokasi_id = $this->input->post('lokasi_id');
			$list_kamar = $this->m_kamar->all_data($lokasi_id);
			//
			$html = '';
			$html .= '<select name="kamar_id" id="kamar_id" class="chosen-select custom-select w-100" required>';
			$html .= '<option value="">---</option>';
			foreach ($list_kamar as $kamar) {
				$html .= '<option value="' . $kamar['kamar_id'] . '" data-kelas-id="' . $kamar['kelas_id'] . '" data-nom-tarif="' . $kamar['nom_tarif'] . '">' . $kamar['kamar_nm'] . '</option>';
			}
			$html .= '</select>';
			$html .= js_chosen();

			$html .= '<script>
					  			$(function() {';
			$html .= '		$("#kamar_id").bind("change",function(e) {
											e.preventDefault();
											var i = $(this).val();
											var kelas_id = $(this).find(":selected").attr("data-kelas-id");
											var nom_tarif = $(this).find(":selected").attr("data-nom-tarif");
											var jns_rawat_cd = $("#jns_rawat_cd").val();
											$("#kelas_id").val(kelas_id);
											$("#nom_tarif").val(nom_tarif);
											_cek_kamar(i, jns_rawat_cd);
										})

										function _cek_kamar(id, jns_rawat_cd) {
								      $.post("' . site_url('pelayanan/registrasi_ranap/kunjungan/ajax/cek_kamar') . '", {kamar_id: id, jns_rawat_cd: jns_rawat_cd},function(data) {
								        if (data.status === false) {
								          _get_kamar($("#lokasi_bangsal_id").val());
								          $("#kelas_id").val("");
													$("#nom_tarif").val("");
								          $.toast({
								            heading: "Error",
								            text: "Kamar tidak tersedia",
								            icon: "error",
								            position: "top-right"
								          })
								        }
								      },"json");
								    }

								    function _get_kamar(i, j) {
								      $.post("' . site_url('pelayanan/registrasi_ranap/kunjungan/ajax/get_kamar_pindah_bangsal') . '", {lokasi_id: i},function(data) {
								        $("#box_kamar").html(data.html);
								      },"json");
								    }';
			$html .= ' 	});
					  		</script>';

			$html .= "<script></script>";
			//
			echo json_encode(array(
				'html' => $html,
			));
		}

		if ($type == 'data_kamar') {
			$list = $this->m_dt_kamar->get_datatables();
			$no = 0;
			$data = array();
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['lokasi_nm'];
				$row[] = $field['kamar_nm'];
				$row[] = $field['jml_bed_kosong'];

				$data[] = $row;
			}

			$output = array(
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'ibu_fill') {
			$data = $this->input->post();
			$res = $this->m_reg_ranap_kunjungan->ibu_row($data['reg_id']);
			echo json_encode($res);
		}

		if ($type == 'pasien_fill') {
			$data = $this->input->post();
			$res = $this->m_reg_ranap_kunjungan->pasien_row($data['pasien_id']);
			echo json_encode($res);
		}

		if ($type == 'no_rm_fill') {
			$data = $this->input->post();
			$res = $this->m_reg_ranap_kunjungan->no_rm_row($data['pasien_id']);
			echo json_encode($res);
		}

		if ($type == 'cari_sep') {
			$sep_no = $this->input->post('sep_no');
			//
			$vclaim_conf = [
				'cons_id' => '26659',
				'secret_key' => '9lI684EDC2',
				'base_url' => 'https://dvlp.bpjs-kesehatan.go.id',
				'service_name' => 'vclaim-rest'
			];

			$peserta = new Nsulistiyawan\Bpjs\VClaim\Sep($vclaim_conf);
			$result = $peserta->cariSEP($sep_no);

			$data['nav'] = $this->nav;
			$data['sep_no'] = @$sep_no;
			$data['code'] = @$result['metaData']['code'];
			$data['message'] = @$result['metaData']['message'];
			if (@$result['metaData']['code'] == 200) {
				$data['main'] = $result['response'];
			}

			echo json_encode($data);
		}
	}

	public function ibu_modal()
	{
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/registrasi_ranap/kunjungan/ibu_modal', $data, true)
		));
	}

	public function get_data_ibu()
	{
		$list = $this->m_dt_registrasi_ibu->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field['pasien_id'];
			$row[] = $field['pasien_nm'] . ', ' . $field['sebutan_cd'];
			$row[] = $field['alamat'];
			$row[] = ($field['tmp_lahir'] != '') ? $field['tmp_lahir'] . ', ' : '' . @to_date($field['tgl_lahir']);
			$row[] = $field['jenispasien_nm'];
			$row[] = $field['lokasi_nm'];
			$row[] = $field['tgl_registrasi'];
			$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="ibu_fill(' . "'" . $field['reg_id'] . "'" . ')">Pilih >></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_dt_registrasi_ibu->count_all(),
			"recordsFiltered" => $this->m_dt_registrasi_ibu->count_filtered(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function pasien_modal()
	{
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/registrasi_ranap/kunjungan/pasien_modal', $data, true)
		));
	}

	public function get_data_pasien()
	{
		$list = $this->m_dt_mst_pasien->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field['pasien_id'];
			$row[] = $field['pasien_nm'] . ', ' . $field['sebutan_cd'];
			$row[] = $field['alamat'];
			$row[] = $field['sex_cd'];
			$row[] = $field['tmp_lahir'] . ', ' . @to_date($field['tgl_lahir']);
			$row[] = $field['jenispasien_nm'];
			$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="pasien_fill(' . "'" . $field['pasien_id'] . "'" . ')">Pilih >></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_dt_mst_pasien->count_all(),
			"recordsFiltered" => $this->m_dt_mst_pasien->count_filtered(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function cetak_modal($type = null, $reg_id = null, $sep_no = null)
	{
		$data['reg_id'] = @$reg_id;
		$data['sep_no'] = @$sep_no;
		$data['nav'] = $this->nav;
		$data['url'] = site_url() . '/pelayanan/registrasi_ranap/kunjungan/' . $type . '/' . @$reg_id . '/' . @$sep_no;

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/registrasi_ranap/kunjungan/cetak_modal', $data, true)
		));
	}

	public function cetak_sep_pdf($reg_id = null, $sep_no = null)
	{
		$date_now = date('Y-m-d H:i:s');
		$vclaim_conf = [
			'cons_id' => '26659',
			'secret_key' => '9lI684EDC2',
			'base_url' => 'https://dvlp.bpjs-kesehatan.go.id',
			'service_name' => 'vclaim-rest'
		];

		$pasien = $this->m_reg_ranap_kunjungan->get_data($reg_id);
		$request_vclaim = json_decode($pasien['request_vclaim'], true);
		// Cari SEP
		$peserta = new Nsulistiyawan\Bpjs\VClaim\Sep($vclaim_conf);
		$result = $peserta->cariSEP($sep_no);
		if (@$result['metaData']['code'] == 200) {
			$main = $result['response'];
		}
		// Car Fasilitas Kesehata/FASKES
		$kd_faskes = @$request_vclaim['request']['t_sep']['rujukan']['ppkRujukan'];
		$jns_faskes = '2'; // Faskes 1
		$referensi = new Nsulistiyawan\Bpjs\VClaim\Referensi($vclaim_conf);
		$res_referensi = $referensi->faskes($kd_faskes, $jns_faskes);
		if (@$res_referensi['metaData']['code'] == 200) {
			$faskes = $res_referensi['response'];
		}

		if (@$main['peserta']['kelamin'] == 'L') {
			$jns_kelamin = 'Laki-Laki';
		} elseif (@$main['peserta']['kelamin'] == 'P') {
			$jns_kelamin = 'Perempuan';
		} else {
			$jns_kelamin = '';
		}

		if (@$pasien['no_telp'] != '') {
			$no_telp = @$pasien['no_telp'];
		} else {
			$no_telp = '-';
		}

		if ($request_vclaim['request']['t_sep']['cob']['cob'] != '0') {
			$cob = $request_vclaim['request']['t_sep']['cob']['cob'];
		} else {
			$cob = '-';
		}

		if (@$main['penjamin'] != '') {
			$penjamin = @$main['penjamin'];
		} else {
			$penjamin = '-';
		}

		if (@$main['poli'] != '') {
			$poli = @$main['poli'];
		} else {
			$poli = @$pasien['lokasi_nm'];
		}

		if (@$faskes['faskes'][0]['nama'] != '') {
			$faskes_perujuk = @$faskes['faskes'][0]['nama'];
		} else {
			$faskes_perujuk = '-';
		}


		//generate pdf
		$this->load->library('pdf');
		$pdf = new Pdf('p', 'mm', array(210, 297));
		$pdf->AliasNbPages();
		$pdf->SetTitle('Cetak SEP ' . @$sep_no);
		$pdf->SetMargins(2, 2, 2);
		$pdf->AddPage();

		$pdf->Image(FCPATH . 'assets/images/icon/bpjs.png', 3, 3, 50, 8);
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(60, 2, '', 0, 1, 'L');
		$pdf->Cell(60, 6, '', 0, 0, 'L');
		$pdf->Cell(0, 3, 'SURAT ELEGIBILITAS PESERTA', 0, 1, 'L');
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(60, 6, '', 0, 0, 'L');
		$pdf->Cell(0, 5, 'RSUD DR. SOEDIRMAN KEBUMEN', 0, 1, 'L');
		$pdf->SetFont('Arial', '', 12);
		$pdf->Cell(0, 5, '', 0, 1, 'C');

		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(30, 5, 'No.SEP', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$main['noSep'], 0, 1, 'L');
		$pdf->Cell(30, 5, 'Tgl.SEP', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$main['tglSep'], 0, 0, 'L');
		$pdf->Cell(30, 5, 'Peserta', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(73, 5, @$main['peserta']['jnsPeserta'], 0, 1, 'L');
		$pdf->Cell(30, 5, 'No.Kartu', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$main['peserta']['noKartu'] . ' ( MR. ' . @$main['peserta']['noMr'] . ' )', 0, 0, 'L');
		$pdf->Cell(30, 5, 'COB', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(73, 5, @$cob, 0, 1, 'L');
		$pdf->Cell(30, 5, 'Nama Peserta', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$main['peserta']['nama'], 0, 0, 'L');
		$pdf->Cell(30, 5, 'Jns.Rawat', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(73, 5, @$main['jnsPelayanan'], 0, 1, 'L');
		$pdf->Cell(30, 5, 'Tgl.Lahir', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$main['peserta']['tglLahir'] . ' Kelamin : ' . @$jns_kelamin, 0, 0, 'L');
		$pdf->Cell(30, 5, 'Kls.Rawat', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(73, 5, 'Kelas ' . @$main['kelasRawat'], 0, 1, 'L');
		$pdf->Cell(30, 5, 'No.Telepon', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$no_telp, 0, 0, 'L');
		$pdf->Cell(30, 5, 'Penjamin', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$penjamin, 0, 1, 'L');
		$pdf->Cell(30, 5, 'Sub/Spesialis', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$poli, 0, 1, 'L');
		$pdf->Cell(30, 5, 'Faskes Perujuk', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$faskes_perujuk, 0, 1, 'L');
		$pdf->Cell(30, 5, 'Diagnosa Awal', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$main['diagnosa'], 0, 1, 'L');
		$pdf->Cell(30, 5, 'Catatan', 0, 0, 'L');
		$pdf->Cell(2, 5, ':', 0, 0, 'C');
		$pdf->Cell(90, 5, @$main['catatan'], 0, 1, 'L');
		$pdf->Cell(90, 3, '', 0, 1, 'L');

		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(150, 3.5, '*Saya menyetujui BPJS Kesehatan menggunakan informasi medis pasien jika diperlukan.', 0, 0, 'L');
		$pdf->Cell(60, 3.5, 'Pasien/Keluarga Pasien', 0, 1, 'C');
		$pdf->Cell(150, 3.5, '*SEP Bukan sebagai bukti penjaminan peserta', 0, 1, 'L');

		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(90, 5, '', 0, 1, 'L');
		$pdf->Cell(150, 3.5, 'Cetakan ke 1 ' . to_date(@$date_now, '', 'full_date') . ' WIB', 0, 1, 'L');
		$pdf->Cell(90, 5, '', 0, 1, 'L');

		$pdf->Output('I', 'SEP_' . @$main['noSep'] . '.pdf');
	}
}
