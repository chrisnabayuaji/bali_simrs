<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Verifikasi extends MY_Controller
{

	var $nav_id = '03.01.05', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'm_reg_ranap_verifikasi',
			'master/m_lokasi',
			'master/m_jenis_pasien',
			'master/m_lokasi',
			'master/m_wilayah',
			'master/m_kelas',
			'master/m_jenis_pasien',
			'master/m_pegawai',
			'master/m_kelas_lokasi',
			'master/m_kamar'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id . '.verifikasi');
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('tgl_registrasi_from' => '', 'tgl_registrasi_to' => '', 'no_rm_nm' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'tgl_registrasi', 'type' => 'desc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(5, 0);
		$this->cookie['total_rows'] = $this->m_reg_ranap_verifikasi->all_rows($this->cookie);
		set_cookie_nav($this->nav_id . '.verifikasi', $this->cookie);
		//main data
		$data['nav'] = $this->nav;
		$data['cookie'] = $this->cookie;
		$data['main'] = $this->m_reg_ranap_verifikasi->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
		$data['lokasi'] = $this->m_lokasi->by_field('jenisreg_st', 1, 'result');
		$data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
		$data['total_verifikasi'] = $this->m_reg_ranap_verifikasi->total_verifikasi();
		//set pagination
		$nav['nav_url'] = 'pelayanan/registrasi_ranap/verifikasi';
		set_pagination($nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('pelayanan/registrasi_ranap/verifikasi/index', $data);
	}

	public function form($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
			$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_profile();
		} else {
			$data['main'] = $this->m_reg_ranap_verifikasi->get_data($id);
			if ($data['main']['wilayah_st'] == 'L') {
				$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_parent('');
			} else {
				$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_profile();
			}
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['lokasi_asal'] = $this->m_lokasi->by_field('is_deleted', 0, 'result');
		$data['lokasi_bangsal'] = $this->m_lokasi->by_field('jenisreg_st', 2, 'result');
		$data['kelas'] = $this->m_kelas->list_kelas();
		$data['dokter'] = $this->m_pegawai->by_field('jenispegawai_cd', '02', 'result');
		$data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
		$data['form_action'] = site_url() . '/' . 'pelayanan/registrasi_ranap/verifikasi' . '/save/' . $id;

		$this->render('pelayanan/registrasi_ranap/verifikasi/form', $data);
	}

	public function info_kamar()
	{
		$data['main'] = $this->m_kamar->all_data();
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('pelayanan/registrasi_ranap/verifikasi/info_kamar', $data, true)
		));
	}

	public function save($id = null)
	{
		$this->m_reg_ranap_verifikasi->save($id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($id != '') ? '_update' : '_add', $this->nav_id);
		redirect(site_url() . '/' . 'pelayanan/registrasi_ranap/verifikasi' . '/index/');
	}

	function ajax($type = null, $id = null)
	{
		if ($type == 'get_kelas') {
			$lokasi_id = $this->input->post('lokasi_id');
			$kelas_id = $this->input->post('kelas_id');
			$list_kelas = $this->m_kelas_lokasi->all_data($lokasi_id);
			//
			$html = '';
			$html .= '<select name="src_kelas_id" id="src_kelas_id" class="chosen-select custom-select w-100">';
			$html .= '<option value="">---</option>';
			foreach ($list_kelas as $kelas) {
				if (@$kelas_id == $kelas['kelas_id']) {
					$html .= '<option value="' . $kelas['kelas_id'] . '" selected>' . $kelas['kelas_nm'] . '</option>';
				} else {
					$html .= '<option value="' . $kelas['kelas_id'] . '">' . $kelas['kelas_nm'] . '</option>';
				}
			}
			$html .= '</select>';
			$html .= js_chosen();
			//
			echo json_encode(array(
				'html' => $html,
			));
		}

		if ($type == 'get_kamar') {
			$lokasi_id = $this->input->post('lokasi_id');
			$list_kamar = $this->m_kamar->all_data($lokasi_id);
			//
			$html = '';
			$html .= '<select name="kamar_id" id="kamar_id" class="chosen-select custom-select w-100" required>';
			$html .= '<option value="">- Pilih -</option>';
			foreach ($list_kamar as $kamar) {
				$html .= '<option value="' . $kamar['kamar_id'] . '">' . $kamar['kamar_nm'] . ' (' . $kamar['jml_bed_kosong'] . ')</option>';
			}
			$html .= '</select>';
			$html .= js_chosen();
			$html .= "<script>$('#kamar_id').bind('change',function(e) {
				e.preventDefault();
				var i = $(this).val();
				_cek_kamar(i);
			})</script>";
			//
			echo json_encode(array(
				'html' => $html,
			));
		}

		if ($type == 'detail_kamar') {
			$kamar_id = $this->input->post('kamar_id');
			$kamar = $this->db->where('kamar_id', $kamar_id)->get('mst_kamar')->row_array();
			echo json_encode($kamar);
		}

		if ($type == 'cek_kamar') {
			$kamar_id = $this->input->post('kamar_id');
			$cek = $this->m_kamar->by_field('kamar_id', $kamar_id, 'row');
			if ($cek['jml_bed_kosong'] == 0 || $cek['jml_bed_kosong'] == NULL) {
				$status = false;
			} else {
				$status = true;
			}
			//
			echo json_encode(array(
				'status' => $status,
			));
		}
	}
}
