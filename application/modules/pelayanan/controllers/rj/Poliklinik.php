<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Poliklinik extends MY_Controller
{

	var $nav_id = '03.02', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'm_poliklinik',
			'master/m_lokasi',
			'master/m_jenis_pasien',
			'm_dt_diagnosis',
			'm_dt_tarifkelas',
			'm_dt_petugas',
			'm_dt_obat',
			'm_dt_obat_master',
			'm_dt_bhp',
			'm_dt_alkes',
			'm_dt_laboratorium',
			'm_dt_radiologi',
			'm_dt_bedah_sentral',
			'master/m_rsrujukan',
			'keuangan/kasir/m_piutang',
			'app/m_profile'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id);
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('tgl_registrasi_from' => '', 'tgl_registrasi_to' => '', 'no_rm_nm' => '', 'lokasi_id' => '', 'jenispasien_id' => '', 'periksa_st' => '', 'pulang_st' => '0');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'a.tgl_registrasi', 'type' => 'desc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(5, 0);
		$this->cookie['total_rows'] = $this->m_poliklinik->all_rows($this->cookie);
		set_cookie_nav($this->nav_id, $this->cookie);
		//main data
		$data['nav'] = $this->nav;
		$data['cookie'] = $this->cookie;
		$data['main'] = $this->m_poliklinik->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
		$data['lokasi'] = $this->m_lokasi->by_field('jenisreg_st', 1, 'result');
		$data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
		//set pagination
		set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('pelayanan/poliklinik/index', $data);
	}

	public function form($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
			$data['alergi'] = array();
		} else {
			$data['main'] = $this->m_poliklinik->get_data($id);
			$data['alergi'] = $this->m_poliklinik->get_alergi($data['main']['pasien_id']);
			$this->m_poliklinik->update_periksa_st($id, $data['main']['periksa_st']);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save/' . $id;

		$this->render('pelayanan/poliklinik/form', $data);
	}

	function ajax($type = null, $id = null)
	{
		if ($type == 'view_tab_menu') {
			$view_name = $this->input->post('view_name');
			$id = $this->input->post('id');
			$data['nav'] = $this->nav;
			$data['reg'] = $this->m_poliklinik->get_data($id);

			// Tindak Lanjut
			if ($view_name == 'tindak_lanjut') {
				$data['lokasi_jenisreg_1'] = $this->m_lokasi->by_field('jenisreg_st', 1, 'result');
				$data['lokasi_jenisreg_2'] = $this->m_lokasi->by_field('jenisreg_st', 2, 'result');
				$data['rujukan'] = $this->m_rsrujukan->all_data();
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/' . $view_name, $data, true)
			));
		}
	}

	//CATATAN MEDIS
	public function ajax_catatan_medis($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_poliklinik->catatan_medis_save();
		} elseif ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->catatan_medis_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/catatan_medis_data', $data, true)
			));
		} elseif ($type == 'history') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->catatan_medis_history($pasien_id, $reg_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/catatan_medis_history', $data, true)
			));
		} elseif ($type == 'get_data') {
			$catatanmedis_id = $this->input->post('catatanmedis_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');

			$main = $this->m_poliklinik->catatan_medis_get($catatanmedis_id, $reg_id, $pasien_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$catatanmedis_id = $this->input->post('catatanmedis_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_poliklinik->catatan_medis_delete($catatanmedis_id, $reg_id, $pasien_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->catatan_medis_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/catatan_medis_data', $data, true)
			));
		} elseif ($type == 'autocomplete') {
			$pegawai_nm = $this->input->get('pegawai_nm');
			$res = $this->m_poliklinik->petugas_autocomplete($pegawai_nm);
			echo json_encode($res);
		} elseif ($type == 'search_petugas') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/search_petugas_single', $data, true)
			));
		} elseif ($type == 'search_data') {
			$list = $this->m_dt_petugas->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['pegawai_id'];
				$row[] = $field['pegawai_nm'];
				$row[] = $field['pegawai_nip'];
				$row[] = get_parameter_value('jenispegawai_cd', $field['jenispegawai_cd']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="petugas_fill(' . "'" . $field['pegawai_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_petugas->count_all(),
				"recordsFiltered" => $this->m_dt_petugas->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'petugas_fill') {
			$data = $this->input->post();
			$res = $this->m_poliklinik->petugas_row($data['pegawai_id']);
			echo json_encode($res);
		}
	}

	// Pemeriksaan fisik
	public function ajax_pemeriksaan_fisik($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_poliklinik->pemeriksaan_fisik_save();
		} elseif ($type == 'data') {
			$res = $this->m_poliklinik->pemeriksaan_fisik_data();
			echo json_encode($res);
		} elseif ($type == 'history') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->pemeriksaan_fisik_history($pasien_id, $reg_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/pemeriksaan_fisik_history', $data, true)
			));
		} elseif ($type == 'autocomplete') {
			$pegawai_nm = $this->input->get('pegawai_nm');
			$res = $this->m_poliklinik->petugas_autocomplete($pegawai_nm);
			echo json_encode($res);
		} elseif ($type == 'search_petugas') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/search_petugas_single', $data, true)
			));
		} elseif ($type == 'search_data') {
			$list = $this->m_dt_petugas->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['pegawai_id'];
				$row[] = $field['pegawai_nm'];
				$row[] = $field['pegawai_nip'];
				$row[] = get_parameter_value('jenispegawai_cd', $field['jenispegawai_cd']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="petugas_fill(' . "'" . $field['pegawai_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_petugas->count_all(),
				"recordsFiltered" => $this->m_dt_petugas->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'petugas_fill') {
			$data = $this->input->post();
			$res = $this->m_poliklinik->petugas_row($data['pegawai_id']);
			echo json_encode($res);
		} else if ($type == 'web_camera') {
			$data['main'] = array();
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/web_camera', $data, true)
			));
		}
	}

	// Diagnosis
	public function ajax_diagnosis($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_poliklinik->diagnosis_save();
		} elseif ($type == 'autocomplete') {
			$penyakit_nm = $this->input->get('penyakit_nm');
			$res = $this->m_poliklinik->penyakit_autocomplete($penyakit_nm);
			echo json_encode($res);
		} elseif ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->diagnosis_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/diagnosis_data', $data, true)
			));
		} elseif ($type == 'history') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->diagnosis_history($pasien_id, $reg_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/diagnosis_history', $data, true)
			));
		} elseif ($type == 'get_data') {
			$diagnosis_id = $this->input->post('diagnosis_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');

			$main = $this->m_poliklinik->diagnosis_get($diagnosis_id, $reg_id, $pasien_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$diagnosis_id = $this->input->post('diagnosis_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_poliklinik->diagnosis_delete($diagnosis_id, $reg_id, $pasien_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->diagnosis_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/diagnosis_data', $data, true)
			));
		} elseif ($type == 'search_diagnosis') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/search_diagnosis', $data, true)
			));
		} elseif ($type == 'search_data') {
			$list = $this->m_dt_diagnosis->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['penyakit_id'];
				$row[] = $field['icdx'];
				$row[] = $field['penyakit_nm'];
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="diagnosis_fill(' . "'" . $field['penyakit_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_diagnosis->count_all(),
				"recordsFiltered" => $this->m_dt_diagnosis->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'diagnosis_fill') {
			$data = $this->input->post();
			$res = $this->m_poliklinik->diagnosis_row($data['penyakit_id']);
			echo json_encode($res);
		}
	}

	// Tindakan
	public function ajax_tindakan($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_poliklinik->tindakan_save();
		} elseif ($type == 'tarifkelas_autocomplete') {
			$tarif_nm = $this->input->get('tarif_nm');
			$res = $this->m_poliklinik->tarifkelas_autocomplete($tarif_nm, $id);
			echo json_encode($res);
		} elseif ($type == 'prosedur_autocomplete') {
			$prosedur_nm = $this->input->get('prosedur_nm');
			$res = $this->m_poliklinik->prosedur_autocomplete($prosedur_nm);
			echo json_encode($res);
		} elseif ($type == 'petugas_autocomplete') {
			$petugas_nm = $this->input->get('petugas_nm');
			$res = $this->m_poliklinik->petugas_autocomplete($petugas_nm);
			echo json_encode($res);
		} elseif ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->tindakan_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/tindakan_data', $data, true)
			));
		} elseif ($type == 'history') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->tindakan_history($pasien_id, $reg_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/tindakan_history', $data, true)
			));
		} elseif ($type == 'get_data') {
			$tindakan_id = $this->input->post('tindakan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');

			$main = $this->m_poliklinik->tindakan_get($tindakan_id, $reg_id, $pasien_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$tindakan_id = $this->input->post('tindakan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_poliklinik->tindakan_delete($tindakan_id, $reg_id, $pasien_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->tindakan_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/tindakan_data', $data, true)
			));
		} elseif ($type == 'search_tarifkelas') {
			$data['nav'] = $this->nav;
			$data['kelas_id'] = $id;
			$data['get_kelas'] = $this->m_poliklinik->get_kelas($id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/search_tarifkelas', $data, true)
			));
		} elseif ($type == 'search_tarifkelas_data') {
			$list = $this->m_dt_tarifkelas->get_datatables($id);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['tarifkelas_id'];
				$row[] = $field['tarif_nm'];
				$row[] = $field['kelas_nm'];
				$row[] = num_id($field['nominal']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="tarifkelas_fill(' . "'" . $field['tarifkelas_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_tarifkelas->count_all($id),
				"recordsFiltered" => $this->m_dt_tarifkelas->count_filtered($id),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'tarifkelas_fill') {
			$data = $this->input->post();
			$res = $this->m_poliklinik->tarifkelas_row($data['tarifkelas_id']);
			echo json_encode($res);
		} elseif ($type == 'search_petugas') {
			$data['nav'] = $this->nav;
			$data['form_name'] = @$id;

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/search_petugas', $data, true)
			));
		} elseif ($type == 'search_petugas_data') {
			$list = $this->m_dt_petugas->get_datatables();
			$form_name = $this->input->post('form_name');
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['pegawai_id'];
				$row[] = $field['pegawai_nm'];
				$row[] = $field['pegawai_nip'];
				$row[] = get_parameter_value('jenispegawai_cd', $field['jenispegawai_cd']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="petugas_fill(' . "'" . $field['pegawai_id'] . "'" . ',' . "'" . $form_name . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_petugas->count_all(),
				"recordsFiltered" => $this->m_dt_petugas->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'petugas_fill') {
			$data = $this->input->post();
			$res = $this->m_poliklinik->petugas_row($data['pegawai_id']);
			echo json_encode($res);
		} else if ($type == 'add_petugas') {
			$tindakan_id = $this->input->get('tindakan_id');
			$data['nav'] = $this->nav;

			$html = '';
			if ($tindakan_id != '') {
				$petugas_no = $this->input->get('petugas_no');
				$get_tindakan = $this->m_poliklinik->get_tindakan($tindakan_id);
				$data['tindakan_id'] = $tindakan_id;
				$data['petugas_no_post'] = $petugas_no;
				$data['type'] = 'edit';

				if ($get_tindakan['petugas_id'] != '') {
					$data['petugas_no'] = '';
					$data['form_name'] = 'petugas_id';
					$data['pegawai_id'] = $get_tindakan['petugas_id'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_1'];

					$html .= $this->load->view('pelayanan/poliklinik/add_petugas', $data, true);
				}
				if ($get_tindakan['petugas_id_2'] != '') {
					$data['petugas_no'] = '2';
					$data['form_name'] = 'petugas_id_2';
					$data['pegawai_id'] = $get_tindakan['petugas_id_2'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_2'];

					$html .= $this->load->view('pelayanan/poliklinik/add_petugas', $data, true);
				}
				if ($get_tindakan['petugas_id_3'] != '') {
					$data['petugas_no'] = '3';
					$data['form_name'] = 'petugas_id_3';
					$data['pegawai_id'] = $get_tindakan['petugas_id_3'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_3'];

					$html .= $this->load->view('pelayanan/poliklinik/add_petugas', $data, true);
				}
				if ($get_tindakan['petugas_id_4'] != '') {
					$data['petugas_no'] = '4';
					$data['form_name'] = 'petugas_id_4';
					$data['pegawai_id'] = $get_tindakan['petugas_id_4'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_4'];

					$html .= $this->load->view('pelayanan/poliklinik/add_petugas', $data, true);
				}
				if ($get_tindakan['petugas_id_5'] != '') {
					$data['petugas_no'] = '5';
					$data['form_name'] = 'petugas_id_5';
					$data['pegawai_id'] = $get_tindakan['petugas_id_5'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_5'];

					$html .= $this->load->view('pelayanan/poliklinik/add_petugas', $data, true);
				}
			} else {
				$petugas_no = $this->input->get('petugas_no') + 1;
				$data['petugas_no'] = ($petugas_no == '1') ? '' : $petugas_no;
				$data['form_name'] = ($petugas_no == '1') ? 'petugas_id' : 'petugas_id_' . $petugas_no;
				$data['petugas_no_post'] = $petugas_no;
				$data['type'] = 'add';

				$html .= $this->load->view('pelayanan/poliklinik/add_petugas', $data, true);
			}

			echo json_encode(array(
				'html' => $html,
				'petugas_no' => $petugas_no,
			));
		} elseif ($type == 'delete_petugas') {
			$tindakan_id = $this->input->post('tindakan_id');
			$form_name = $this->input->post('form_name');
			//
			$callback = 'true';
			//
			echo json_encode(array(
				'callback' => $callback
			));
		}
	}

	// Pemberian Obat
	public function ajax_pemberian_obat($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_poliklinik->pemberian_obat_save();
		} elseif ($type == 'autocomplete') {
			$obat_nm = $this->input->get('obat_nm');
			$map_lokasi_depo = $this->input->get('map_lokasi_depo');
			$res = $this->m_poliklinik->obat_autocomplete($obat_nm, $map_lokasi_depo);
			echo json_encode($res);
		} elseif ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->pemberian_obat_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/igd/pemberian_obat_data', $data, true)
			));
		} elseif ($type == 'history') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->pemberian_obat_history($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/igd/pemberian_obat_history', $data, true)
			));
		} elseif ($type == 'get_data') {
			$farmasi_id = $this->input->post('farmasi_id');
			$resep_id = $this->input->post('resep_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_poliklinik->pemberian_obat_get($farmasi_id, $resep_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$resep_id = $this->input->post('resep_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$farmasi_id = $this->input->post('farmasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$this->m_poliklinik->pemberian_obat_delete($resep_id, $reg_id, $farmasi_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->pemberian_obat_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/igd/pemberian_obat_data', $data, true)
			));
		} elseif ($type == 'search_obat') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/igd/search_obat', $data, true)
			));
		} elseif ($type == 'search_data') {
			$map_lokasi_depo = $this->input->post('map_lokasi_depo');
			$list = $this->m_dt_obat->get_datatables($map_lokasi_depo);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['no_batch'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = to_date($field['tgl_expired'], '-', 'date', ' ');
				$row[] = num_id($field['stok_akhir']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="obat_fill(' . "'" . $field['obat_id'] . "'" . ',' . "'" . $field['stokdepo_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_obat->count_all($map_lokasi_depo),
				"recordsFiltered" => $this->m_dt_obat->count_filtered($map_lokasi_depo),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} elseif ($type == 'search_data_master') {
			$list = $this->m_dt_obat_master->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="obat_fill_master(' . "'" . $field['obat_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_obat_master->count_all(),
				"recordsFiltered" => $this->m_dt_obat_master->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'obat_fill') {
			$data = $this->input->post();
			$res = $this->m_poliklinik->obat_row($data);
			echo json_encode($res);
		} else if ($type == 'obat_fill_master') {
			$data = $this->input->post();
			$res = $this->m_poliklinik->obat_row_master($data['obat_id']);
			echo json_encode($res);
		} elseif ($type == 'get_no_resep') {
			$reg_id = $this->input->post('reg_id');
			$res = $this->m_poliklinik->get_no_resep($reg_id);
			echo json_encode($res);
		} elseif ($type == 'dosis_autocomplete') {
			$res = $this->m_poliklinik->dosis_autocomplete();
			echo json_encode($res);
		} elseif ($type == 'peringatan_khusus_autocomplete') {
			$res = $this->m_poliklinik->peringatan_khusus_autocomplete();
			echo json_encode($res);
		} elseif ($type == 'jam_minum_autocomplete') {
			$res = $this->m_poliklinik->jam_minum_autocomplete();
			echo json_encode($res);
		}
	}

	// BHP
	public function ajax_bhp($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_poliklinik->bhp_save();
		} elseif ($type == 'autocomplete') {
			$obat_nm = $this->input->get('obat_nm');
			$map_lokasi_depo = $this->input->get('map_lokasi_depo');
			$res = $this->m_poliklinik->bhp_autocomplete($obat_nm, $map_lokasi_depo);
			echo json_encode($res);
		} elseif ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->bhp_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/bhp_data', $data, true)
			));
		} elseif ($type == 'history') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->bhp_history($pasien_id, $reg_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/bhp_history', $data, true)
			));
		} elseif ($type == 'get_data') {
			$bhp_id = $this->input->post('bhp_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_poliklinik->bhp_get($bhp_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$bhp_id = $this->input->post('bhp_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_poliklinik->bhp_delete($bhp_id, $reg_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->bhp_data($reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/bhp_data', $data, true)
			));
		} elseif ($type == 'search_bhp') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/search_bhp', $data, true)
			));
		} elseif ($type == 'search_data') {
			$map_lokasi_depo = $this->input->post('map_lokasi_depo');
			$list = $this->m_dt_bhp->get_datatables($map_lokasi_depo);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['no_batch'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = to_date($field['tgl_expired'], '-', 'date', ' ');
				$row[] = num_id($field['stok_akhir']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="bhp_fill(' . "'" . $field['obat_id'] . "'" . ',' . "'" . $field['stokdepo_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_bhp->count_all($map_lokasi_depo),
				"recordsFiltered" => $this->m_dt_bhp->count_filtered($map_lokasi_depo),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'bhp_fill') {
			$data = $this->input->post();
			$res = $this->m_poliklinik->bhp_row($data);
			echo json_encode($res);
		}
	}

	// ALKES
	public function ajax_alkes($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_poliklinik->alkes_save();
		} elseif ($type == 'autocomplete') {
			$obat_nm = $this->input->get('obat_nm');
			$map_lokasi_depo = $this->input->get('map_lokasi_depo');
			$res = $this->m_poliklinik->alkes_autocomplete($obat_nm, $map_lokasi_depo);
			echo json_encode($res);
		} elseif ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->alkes_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/alkes_data', $data, true)
			));
		} elseif ($type == 'history') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->alkes_history($pasien_id, $reg_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/alkes_history', $data, true)
			));
		} elseif ($type == 'get_data') {
			$alkes_id = $this->input->post('alkes_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_poliklinik->alkes_get($alkes_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$alkes_id = $this->input->post('alkes_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_poliklinik->alkes_delete($alkes_id, $reg_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->alkes_data($reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/alkes_data', $data, true)
			));
		} elseif ($type == 'search_alkes') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/search_alkes', $data, true)
			));
		} elseif ($type == 'search_data') {
			$map_lokasi_depo = $this->input->post('map_lokasi_depo');
			$list = $this->m_dt_alkes->get_datatables($map_lokasi_depo);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['no_batch'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = to_date($field['tgl_expired'], '-', 'date', ' ');
				$row[] = num_id($field['stok_akhir']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="alkes_fill(' . "'" . $field['obat_id'] . "'" . ',' . "'" . $field['stokdepo_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_alkes->count_all($map_lokasi_depo),
				"recordsFiltered" => $this->m_dt_alkes->count_filtered($map_lokasi_depo),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'alkes_fill') {
			$data = $this->input->post();
			$res = $this->m_poliklinik->alkes_row($data);
			echo json_encode($res);
		}
	}

	//  Penunjang
	public function ajax_penunjang_laboratorium($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'modal') {
			$data['reg'] = $this->m_poliklinik->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainlab'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_laboratorium/save/';
			} else {
				$data['mainlab'] = $this->m_poliklinik->laboratorium_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_laboratorium/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/laboratorium_modal', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['reg'] = $this->m_poliklinik->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainlab'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_laboratorium/save/';
			} else {
				$data['mainlab'] = $this->m_poliklinik->laboratorium_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_laboratorium/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/laboratorium_detail', $data, true)
			));
		}

		if ($type == 'laboratorium_pemeriksaan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->laboratorium_pemeriksaan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/laboratorium_pemeriksaan_data', $data, true)
			));
		}

		if ($type == 'laboratorium_tarif_tindakan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->laboratorium_tarif_tindakan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/laboratorium_tarif_tindakan_data', $data, true)
			));
		}

		if ($type == 'laboratorium_bhp_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->laboratorium_bhp_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/laboratorium_bhp_data', $data, true)
			));
		}

		if ($type == 'laboratorium_alkes_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->laboratorium_alkes_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/laboratorium_alkes_data', $data, true)
			));
		}

		if ($type == 'search_data') {
			$list = $this->m_dt_laboratorium->get_datatables($id);
			$no = 0;
			$data = array();
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				if ($field['parent_id'] != '') {
					$dash = '--';
				} else {
					$dash = '';
				};
				$row[] = $field['itemlab_id'];
				$row[] = $dash . $field['itemlab_nm'];
				if ($field['parent_id'] != '') {
					if (@$field['pemeriksaanrinc_id'] != null) {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemlab_id'].'" checked="true">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="' . $field['itemlab_id'] . '" checked="true">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>';
					} else {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemlab_id'].'">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="' . $field['itemlab_id'] . '">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>';
					}
				} else {
					$row[] = '';
				}

				$data[] = $row;
			}

			$output = array(
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'save') {
			$this->m_poliklinik->laboratorium_save(@$reg_id, @$id);
		}

		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->laboratorium_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/laboratorium_data', $data, true)
			));
		}

		if ($type == 'history') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->laboratorium_history($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/laboratorium_history', $data, true)
			));
		}

		if ($type == 'delete_data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_poliklinik->laboratorium_delete($pemeriksaan_id, $reg_id, $pasien_id, $lokasi_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->laboratorium_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/laboratorium_data', $data, true)
			));
		}
	}

	public function ajax_penunjang_radiologi($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'modal') {
			$data['reg'] = $this->m_poliklinik->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			$data['diagnosis'] = $this->m_poliklinik->get_diagnosis($reg_id);
			if ($id == null) {
				$data['mainrad'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_radiologi/save/';
			} else {
				$data['mainrad'] = $this->m_poliklinik->radiologi_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_radiologi/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/radiologi_modal', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['reg'] = $this->m_poliklinik->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			$data['diagnosis'] = $this->m_poliklinik->get_diagnosis($reg_id);
			if ($id == null) {
				$data['mainrad'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_radiologi/save/';
			} else {
				$data['mainrad'] = $this->m_poliklinik->radiologi_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_radiologi/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/radiologi_detail', $data, true)
			));
		}

		if ($type == 'radiologi_pemeriksaan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->radiologi_pemeriksaan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/radiologi_pemeriksaan_data', $data, true)
			));
		}

		if ($type == 'radiologi_tarif_tindakan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->radiologi_tarif_tindakan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/radiologi_tarif_tindakan_data', $data, true)
			));
		}

		if ($type == 'radiologi_bhp_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->radiologi_bhp_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/radiologi_bhp_data', $data, true)
			));
		}

		if ($type == 'radiologi_alkes_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->radiologi_alkes_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/radiologi_alkes_data', $data, true)
			));
		}

		if ($type == 'search_data') {
			$list = $this->m_dt_radiologi->get_datatables($id);
			$no = 0;
			$data = array();
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				if ($field['parent_id'] != '') {
					$dash = '--';
				} else {
					$dash = '';
				};
				$row[] = $field['itemrad_id'];
				$row[] = $dash . $field['itemrad_nm'];
				if ($field['parent_id'] != '') {
					if (@$field['pemeriksaanrinc_id'] != null) {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemrad_id'].'" checked="true">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="' . $field['itemrad_id'] . '" checked="true">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>';
					} else {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemrad_id'].'">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="' . $field['itemrad_id'] . '">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>';
					}
				} else {
					$row[] = '';
				}

				$data[] = $row;
			}

			$output = array(
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'save') {
			$this->m_poliklinik->radiologi_save(@$reg_id, @$id);
		}

		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->radiologi_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/radiologi_data', $data, true)
			));
		}

		if ($type == 'history') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->radiologi_history($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/radiologi_history', $data, true)
			));
		}

		if ($type == 'delete_data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_poliklinik->radiologi_delete($pemeriksaan_id, $reg_id, $pasien_id, $lokasi_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->radiologi_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/radiologi_data', $data, true)
			));
		}
	}

	public function ajax_penunjang_bedah_sentral($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'modal') {
			$data['reg'] = $this->m_poliklinik->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainbs'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_bedah_sentral/save/';
			} else {
				$data['mainbs'] = $this->m_poliklinik->bedah_sentral_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_bedah_sentral/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/bedah_sentral_modal', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['reg'] = $this->m_poliklinik->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainbs'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_bedah_sentral/save/';
			} else {
				$data['mainbs'] = $this->m_poliklinik->bedah_sentral_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_bedah_sentral/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/bedah_sentral_detail', $data, true)
			));
		}

		if ($type == 'bedah_sentral_tindakan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->bedah_sentral_tindakan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/bedah_sentral_tindakan_data', $data, true)
			));
		}

		if ($type == 'bedah_sentral_bhp_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->bedah_sentral_bhp_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/bedah_sentral_bhp_data', $data, true)
			));
		}

		if ($type == 'bedah_sentral_alkes_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->bedah_sentral_alkes_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/bedah_sentral_alkes_data', $data, true)
			));
		}

		if ($type == 'search_data') {
			$list = $this->m_dt_bedah_sentral->get_datatables($id);
			$no = 0;
			$data = array();
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				if ($field['parent_id'] != '') {
					$dash = '--';
				} else {
					$dash = '';
				};
				$row[] = $field['itemoperasi_id'];
				$row[] = $dash . $field['itemoperasi_nm'];
				if ($field['parent_id'] != '') {
					if (@$field['pemeriksaanrinc_id'] != null) {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemoperasi_id'].'" checked="true">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="' . $field['itemoperasi_id'] . '" checked="true">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>';
					} else {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemoperasi_id'].'">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="' . $field['itemoperasi_id'] . '">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>';
					}
				} else {
					$row[] = '';
				}

				$data[] = $row;
			}

			$output = array(
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'save') {
			$this->m_poliklinik->bedah_sentral_save(@$reg_id, @$id);
		}

		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->bedah_sentral_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/bedah_sentral_data', $data, true)
			));
		}

		if ($type == 'history') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->bedah_sentral_history($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/bedah_sentral_history', $data, true)
			));
		}

		if ($type == 'delete_data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_poliklinik->bedah_sentral_delete($pemeriksaan_id, $reg_id, $pasien_id, $lokasi_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->bedah_sentral_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/bedah_sentral_data', $data, true)
			));
		}
	}

	public function ajax_penunjang_vk($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'modal') {
			$data['reg'] = $this->m_poliklinik->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainvk'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_vk/save/';
			} else {
				$data['mainvk'] = $this->m_poliklinik->vk_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_vk/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/vk_modal', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['reg'] = $this->m_poliklinik->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainvk'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_vk/save/';
			} else {
				$data['mainvk'] = $this->m_poliklinik->vk_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_vk/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/vk_detail', $data, true)
			));
		}

		if ($type == 'vk_tindakan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->vk_tindakan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/vk_tindakan_data', $data, true)
			));
		}

		if ($type == 'vk_bhp_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->vk_bhp_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/vk_bhp_data', $data, true)
			));
		}

		if ($type == 'vk_alkes_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->vk_alkes_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/vk_alkes_data', $data, true)
			));
		}

		if ($type == 'save') {
			$this->m_poliklinik->vk_save(@$reg_id, @$id);
		}

		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->vk_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/vk_data', $data, true)
			));
		}

		if ($type == 'history') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->vk_history($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/vk_history', $data, true)
			));
		}

		if ($type == 'delete_data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_poliklinik->vk_delete($pemeriksaan_id, $reg_id, $pasien_id, $lokasi_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->vk_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/vk_data', $data, true)
			));
		}
	}

	public function ajax_penunjang_icu($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'modal') {
			$data['reg'] = $this->m_poliklinik->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainicu'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_icu/save/';
			} else {
				$data['mainicu'] = $this->m_poliklinik->icu_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_icu/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/icu_modal', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['reg'] = $this->m_poliklinik->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainicu'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_icu/save/';
			} else {
				$data['mainicu'] = $this->m_poliklinik->icu_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_icu/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/icu_detail', $data, true)
			));
		}

		if ($type == 'icu_tindakan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->icu_tindakan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/icu_tindakan_data', $data, true)
			));
		}

		if ($type == 'icu_bhp_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->icu_bhp_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/icu_bhp_data', $data, true)
			));
		}

		if ($type == 'icu_alkes_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->icu_alkes_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/icu_alkes_data', $data, true)
			));
		}

		if ($type == 'save') {
			$this->m_poliklinik->icu_save(@$reg_id, @$id);
		}

		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->icu_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/icu_data', $data, true)
			));
		}

		if ($type == 'history') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->icu_history($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/icu_history', $data, true)
			));
		}

		if ($type == 'delete_data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_poliklinik->icu_delete($pemeriksaan_id, $reg_id, $pasien_id, $lokasi_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->icu_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/icu_data', $data, true)
			));
		}
	}

	public function ajax_penunjang_hemodialisa($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'modal') {
			$data['reg'] = $this->m_poliklinik->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainhemo'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_hemodialisa/save/';
			} else {
				$data['mainhemo'] = $this->m_poliklinik->hemodialisa_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_hemodialisa/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/hemodialisa_modal', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['reg'] = $this->m_poliklinik->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainhemo'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_hemodialisa/save/';
			} else {
				$data['mainhemo'] = $this->m_poliklinik->hemodialisa_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_hemodialisa/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/hemodialisa_detail', $data, true)
			));
		}

		if ($type == 'hemodialisa_tindakan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->hemodialisa_tindakan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/hemodialisa_tindakan_data', $data, true)
			));
		}

		if ($type == 'hemodialisa_bhp_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->hemodialisa_bhp_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/hemodialisa_bhp_data', $data, true)
			));
		}

		if ($type == 'hemodialisa_alkes_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->hemodialisa_alkes_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/hemodialisa_alkes_data', $data, true)
			));
		}

		if ($type == 'save') {
			$this->m_poliklinik->hemodialisa_save(@$reg_id, @$id);
		}

		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->hemodialisa_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/hemodialisa_data', $data, true)
			));
		}

		if ($type == 'history') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->hemodialisa_history($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/hemodialisa_history', $data, true)
			));
		}

		if ($type == 'delete_data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_poliklinik->hemodialisa_delete($pemeriksaan_id, $reg_id, $pasien_id, $lokasi_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->hemodialisa_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/hemodialisa_data', $data, true)
			));
		}
	}

	//  Tindak Lanjut
	public function ajax_tindak_lanjut($type = null, $id = null)
	{
		if ($type == 'form_rs_rujukan') {
			$data['nav'] = $this->nav;
			$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_tindakan/save/' . $id;

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/form_rs_rujukan', $data, true)
			));
		} else if ($type == 'cek_id') {
			$data = $this->input->post();
			$data['rsrujukan_id'] = $data['rsrujukan_kode'];
			$cek = $this->m_rsrujukan->get_data($data['rsrujukan_id']);
			if ($id == null) {
				if ($cek == null) {
					echo 'true';
				} else {
					echo 'false';
				}
			} else {
				if ($id != $data['rsrujukan_id'] && $cek != null) {
					echo 'false';
				} else {
					echo 'true';
				}
			}
		} else if ($type == 'save_rs_rujukan') {
			$data = $this->input->post();
			$this->m_poliklinik->save_rs_rujukan();

			echo json_encode(array(
				'main' => $data
			));
		} else if ($type == 'save_tindak_lanjut') {
			$data = $this->input->post();
			$this->m_poliklinik->save_tindak_lanjut();
		} elseif ($type == 'tindak_lanjut_data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_poliklinik->tindak_lanjut_data($pasien_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'reg_pasien_data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_poliklinik->reg_pasien_data_pulang_st($pasien_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		} else if ($type == 'save_status_pulang') {
			$data = $this->input->post();
			$this->m_poliklinik->save_status_pulang();
		} elseif ($type == 'status_pulang_data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_poliklinik->status_pulang_data($pasien_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		}
	}

	// History
	public function ajax_history($type = null, $id = null)
	{
		if ($type == 'pasien_history') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->pasien_history($pasien_id, $reg_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/history_pasien', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['nav'] = $this->nav;
			$data['reg_id'] = $id;
			$data['main'] = $this->m_poliklinik->get_data($id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/history_detail', $data, true)
			));
		}

		if ($type == 'history_detail_pemeriksaan_fisik') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->history_detail_pemeriksaan_fisik($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/history_detail_pemeriksaan_fisik', $data, true)
			));
		}

		if ($type == 'history_detail_catatan_medis') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->history_detail_catatan_medis($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/history_detail_catatan_medis', $data, true)
			));
		}

		if ($type == 'history_detail_diagnosis') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->history_detail_diagnosis($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/history_detail_diagnosis', $data, true)
			));
		}

		if ($type == 'history_detail_tindakan') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->history_detail_tindakan($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/history_detail_tindakan', $data, true)
			));
		}

		if ($type == 'history_detail_resep') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->history_detail_resep($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/history_detail_resep', $data, true)
			));
		}

		if ($type == 'history_detail_resep_racik') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->history_detail_resep_racik($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/history_detail_resep_racik', $data, true)
			));
		}

		if ($type == 'history_detail_bhp') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->history_detail_bhp($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/history_detail_bhp', $data, true)
			));
		}

		if ($type == 'history_detail_alkes') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->history_detail_alkes($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/history_detail_alkes', $data, true)
			));
		}

		if ($type == 'history_detail_laboratorium') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['reg_id'] = $reg_id;
			$data['main'] = $this->m_poliklinik->history_detail_laboratorium($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/history_detail_laboratorium', $data, true)
			));
		}

		if ($type == 'history_detail_radiologi') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['reg_id'] = $reg_id;
			$data['main'] = $this->m_poliklinik->history_detail_radiologi($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/history_detail_radiologi', $data, true)
			));
		}

		if ($type == 'history_detail_bedah_sentral') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['reg_id'] = $reg_id;
			$data['main'] = $this->m_poliklinik->history_detail_bedah_sentral($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/history_detail_bedah_sentral', $data, true)
			));
		}

		if ($type == 'history_detail_vk') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['reg_id'] = $reg_id;
			$data['main'] = $this->m_poliklinik->history_detail_vk($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/history_detail_vk', $data, true)
			));
		}

		if ($type == 'history_detail_icu') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['reg_id'] = $reg_id;
			$data['main'] = $this->m_poliklinik->history_detail_icu($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/history_detail_icu', $data, true)
			));
		}

		if ($type == 'history_detail_hemodialisa') {
			$reg_id = $this->input->post('reg_id');

			$data['nav'] = $this->nav;
			$data['reg_id'] = $reg_id;
			$data['main'] = $this->m_poliklinik->history_detail_hemodialisa($reg_id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/history_detail_hemodialisa', $data, true)
			));
		}
	}

	public function save_tindak_lanjut()
	{
		$this->m_poliklinik->save_tindak_lanjut();
		$this->m_poliklinik->save_status_pulang();
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		redirect(site_url() . '/' . $this->nav['nav_url']);
	}


	public function piutang_modal($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');
		$groupreg_in = $this->input->get('groupreg_in');
		if ($id == null) {
			$data['main'] = null;
			$data['tindakan'] = null;
			$data['obat'] = null;
			$data['alkes'] = null;
			$data['bhp'] = null;
			$data['kamar'] = null;
		} else {
			$data['main'] = $this->m_piutang->piutang_data($id);
			$data['tindakan'] = $this->m_piutang->piutang_tindakan($groupreg_in);
			$data['obat'] = $this->m_piutang->piutang_obat($groupreg_in);
			$data['alkes'] = $this->m_piutang->piutang_alkes($groupreg_in);
			$data['bhp'] = $this->m_piutang->piutang_bhp($groupreg_in);
			$data['kamar'] = $this->m_piutang->piutang_kamar($groupreg_in);
		}

		// var_dump($data['reg']);die;

		$data['id'] = $id;
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('keuangan/kasir/piutang/piutang_modal', $data, true)
		));
	}

	// Resep Non Racik
	public function ajax_resep($type)
	{
		if ($type == 'save') {
			$this->m_poliklinik->resep_save();
		}

		if ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->resep_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/resep_data', $data, true)
			));
		}

		if ($type == 'get_data') {
			$rincian_id = $this->input->post('rincian_id');
			$resep_id = $this->input->post('resep_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_poliklinik->resep_get($resep_id, $rincian_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		}

		if ($type == 'delete_data') {
			$pasien_id = $this->input->post('pasien_id');
			$resep_id = $this->input->post('resep_id');
			$rincian_id = $this->input->post('rincian_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_poliklinik->resep_delete($resep_id, $rincian_id, $reg_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->resep_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/resep_data', $data, true)
			));
		}

		if ($type == 'delete_resep') {
			$resep_id = $this->input->post('resep_id');
			$pasien_id = $this->input->post('pasien_id');
			$resep_id = $this->input->post('resep_id');
			$reg_id = $this->input->post('reg_id');

			$this->m_poliklinik->resep_delete_all($resep_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->resep_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/resep_data', $data, true)
			));
		}
	}

	//resep racik
	public function ajax_racik($type)
	{
		if ($type == 'save') {
			$this->m_poliklinik->racik_save();
		}

		if ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->racik_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/racik_data', $data, true)
			));
		}

		if ($type == 'get_data') {
			$rincian_id = $this->input->post('rincian_id');
			$resep_id = $this->input->post('resep_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_poliklinik->racik_get($resep_id, $rincian_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		}

		if ($type == 'delete_data') {
			$pasien_id = $this->input->post('pasien_id');
			$resep_id = $this->input->post('resep_id');
			$rincian_id = $this->input->post('rincian_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_poliklinik->racik_delete($resep_id, $rincian_id, $reg_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->racik_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/racik_data', $data, true)
			));
		}

		if ($type == 'delete_resep') {
			$resep_id = $this->input->post('resep_id');
			$pasien_id = $this->input->post('pasien_id');
			$resep_id = $this->input->post('resep_id');
			$reg_id = $this->input->post('reg_id');

			$this->m_poliklinik->resep_delete_all($resep_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->resep_data($reg_id, $pasien_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/resep_data', $data, true)
			));
		}
	}

	public function print_hasil_pemeriksaan($reg_id = '', $pemeriksaan_id = '')
	{
		$main = $this->m_poliklinik->laboratorium_get($pemeriksaan_id);
		$identitas_pasien = $this->m_poliklinik->laboratorium_identitas_pasien_get($reg_id, @$main['src_lokasi_id'], @$main['pasien_id']);
		$laboratorium_pemeriksaan_data = $this->m_poliklinik->laboratorium_pemeriksaan_data($reg_id, $pemeriksaan_id);
		$config = $this->m_app->_get_config();
		$identitas = $this->m_app->_get_identitas();

		$nav = $this->nav;
		//generate pdf
		$profile = $this->m_profile->get_first();
		$this->load->library('pdf');
		$pdf = new Pdf('p', 'mm', array(210, 297));
		$pdf->AliasNbPages();
		$pdf->SetTitle('Cetak Hasil Pemeriksaan Laboratorium ' . $pemeriksaan_id);
		$pdf->AddPage();

		$pdf->Image(FCPATH . 'assets/images/icon/' . $identitas['logo_rumah_sakit'], 10, 10, 15, 15);
		$pdf->SetFont('Arial', '', 11);
		$pdf->Cell(18, 6, '', 0, 0, 'C');
		$pdf->Cell(0, 4, 'INSTALASI LABORATORIUM', 0, 1, 'C');
		$pdf->Cell(18, 6, '', 0, 0, 'C');
		$pdf->Cell(0, 6, @$profile['title_logo_login'] . ' ' . @$profile['sub_title_logo_login'], 0, 1, 'C');
		$pdf->SetFont('Arial', '', 9);
		$pdf->Cell(18, 6, '', 0, 0, 'C');
		$pdf->Cell(0, 4, 'Alamat : ' . @$identitas['jalan'] . ', ' . ucfirst(strtolower(clear_kab_kota(@$identitas['kabupaten']))) . ', ' . ucfirst(strtolower(@$identitas['propinsi'])) . ' ' . @$identitas['telp'], 0, 1, 'C');
		$pdf->Line(10, 28, 200, 28);
		$pdf->Line(10, 28, 200, 28);
		$pdf->Line(10, 28, 200, 28);
		$pdf->Line(10, 28, 200, 28);
		$pdf->Line(10, 28, 200, 28);

		$pdf->Cell(0, 6, '', 0, 1, 'L');
		$pdf->SetFont('Arial', '', 11);
		$pdf->Cell(0, 4, 'HASIL PEMERIKSAAN LABORATORIUM', 0, 1, 'C');
		$pdf->Cell(0, 3, '', 0, 1, 'C');

		//Identitas Pasien
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(40, 4, 'No. RM', 0, 0, 'L');
		$pdf->Cell(120, 4, ':  ' . @$identitas_pasien['pasien_id'], 0, 1, 'L');

		$pdf->Cell(40, 4, 'Nama Pasien', 0, 0, 'L');
		$pdf->Cell(120, 4, ':  ' . strtoupper(@$identitas_pasien['pasien_nm'] . @$identitas_pasien['sebutan_cd']), 0, 1, 'L');

		$pdf->Cell(40, 4, 'Umur / Tempat, Tgl.Lahir', 0, 0, 'L');
		$pdf->Cell(120, 4, ':  ' . @$identitas_pasien['umur_thn'] . ' thn / ' . @$identitas_pasien['tmp_lahir'] . ', ' . to_date(@$identitas_pasien['tgl_lahir']), 0, 0, 'L');
		$pdf->Cell(30, 4, (@$identitas_pasien['sex_cd'] == 'L') ? 'Laki-laki' : 'Perempuan', 0, 1, 'L');

		$pdf->Cell(40, 4, 'Alamat', 0, 0, 'L');
		$pdf->Cell(120, 4, (@$identitas_pasien['alamat'] != '') ? ':  ' . strtoupper(@$identitas_pasien['alamat']) . ' ' . strtoupper(@$identitas_pasien['kelurahan']) . ' ' . strtoupper(@$identitas_pasien['kecamatan']) . ' ' . strtoupper(@$identitas_pasien['kabupaten']) : ':  ' . strtoupper(@$identitas_pasien['kelurahan']) . ' ' . strtoupper(@$identitas_pasien['kecamatan']) . ' ' . strtoupper(@$identitas_pasien['kabupaten']), 0, 1, 'L');

		$pdf->Cell(40, 4, 'Dokter Pengirim', 0, 0, 'L');
		$pdf->Cell(120, 4, ':  ' . @$main['dokter_nm'], 0, 1, 'L');

		$pdf->Cell(40, 4, 'Tanggal Terima', 0, 0, 'L');
		$pdf->Cell(120, 4, ':  ' . to_date(@$main['tgl_order'], '-', 'full_date'), 0, 1, 'L');

		$pdf->Cell(40, 4, 'Tanggal Pelaporan', 0, 0, 'L');
		$pdf->Cell(120, 4, ':  ' . to_date(@$main['tgl_diperiksa'], '-', 'full_date'), 0, 1, 'L');

		// generate tabel
		$pdf->Cell(0, 5, '', 0, 1, 'C');
		$no = 1;
		foreach ($laboratorium_pemeriksaan_data as $row) {
			$pdf->SetFont('Arial', 'B', 10);
			$pdf->Cell(0, 6, $row['itemlab_nm'], 0, 1, 'C');

			$pdf->SetFont('Arial', 'B', 9);
			$pdf->Cell(10, 5, 'No.', 1, 0, 'C');
			$pdf->Cell(80, 5, 'Pemeriksaan', 1, 0, 'C');
			$pdf->Cell(50, 5, 'Hasil', 1, 0, 'C');
			$pdf->Cell(50, 5, 'Nilai Normal', 1, 0, 'C');
			$pdf->Cell(0, 5, '', 0, 1);
			//
			if (count($row['rinc']) > 0) {
				$pdf->SetFont('Arial', '', 9);
				$pdf->SetWidths(array("10", "80", "50", "50"));
				$pdf->SetHeights('5');
				$pdf->SetAligns(array("C", "L", "L", "L"));

				foreach ($row['rinc'] as $row2) {
					$pdf->Row(array(
						$no++,
						$row2['itemlab_nm'],
						$row2['hasil_lab'],
						$row2['nilai_normal']
					));
				}
			}
			$pdf->Cell(310, 4, '', 0, 1, 'C');
		}

		// petugas
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(0, 10, '', 0, 1, 'C');
		$pdf->Cell(60, 4, 'Mengetahui', 0, 0, 'C');
		$pdf->Cell(60, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, 'Pemeriksa,', 0, 1, 'C');
		$pdf->Cell(60, 4, 'Penanggungjawab Laboratorium', 0, 0, 'C');
		$pdf->Cell(0, 15, '', 0, 1, 'C');
		$pdf->Cell(60, 4, @$main['dokterpj_nm'], 0, 0, 'C');
		$pdf->Cell(60, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, '(                                    )', 0, 1, 'C');

		$pdf->Output('I', 'Hasil_Pemeriksaan_Laboratorium_' . $reg_id . '_' . $pemeriksaan_id . '_' . date('Ymdhis') . '.pdf');
	}

	public function prosedur_detail($prosedur_id)
	{
		$data['nav'] = $this->nav;
		$data['prosedur'] = $this->db->where('prosedur_id', $prosedur_id)->get('mst_prosedur')->row_array();
		echo json_encode(array(
			'html' => $this->load->view('pelayanan/poliklinik/prosedur_detail', $data, true)
		));
	}
}
