<?php

class M_dt_bedah_sentral extends CI_Model
{

  function get_datatables($pemeriksaan_id = null)
  {
    if ($pemeriksaan_id != null) {
      $where = "WHERE b.pemeriksaan_id = '$pemeriksaan_id'";
      $query = $this->db->query(
        "SELECT 
          a.*,
          c.pemeriksaanrinc_id,
          c.tgl_hasil,
          c.hasil_operasi,
          c.catatan_operasi 
        FROM mst_item_operasi a 
        LEFT JOIN (
          SELECT b.* FROM ibs_pemeriksaan_rinc b 
          $where
        ) as c ON a.itemoperasi_id = c.itemoperasi_id 
        WHERE a.is_tagihan = 1 
        ORDER BY a.itemoperasi_id;"
      );
    } else {
      $query = $this->db->query(
        "SELECT 
          a.*
        FROM mst_item_operasi a 
        WHERE a.is_tagihan = 1 
        ORDER BY a.itemoperasi_id;"
      );
    };
    return $query->result_array();
  }
}
