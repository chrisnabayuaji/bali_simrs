<?php 

class M_dt_kamar extends CI_Model {
 
  function get_datatables()
  {
    $where = "WHERE a.is_deleted = 0 ";
    $query = $this->db->query(
      "SELECT 
        a.*, b.lokasi_nm
      FROM mst_kamar a 
      LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
      ORDER BY a.kamar_id ASC;"
    );
    return $query->result_array();
  }

}