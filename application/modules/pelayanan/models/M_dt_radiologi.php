<?php

class M_dt_radiologi extends CI_Model
{

  function get_datatables($pemeriksaan_id = null)
  {
    if ($pemeriksaan_id != null) {
      $where = "WHERE b.pemeriksaan_id = '$pemeriksaan_id'";
      $query = $this->db->query(
        "SELECT 
          a.*,
          c.pemeriksaanrinc_id,
          c.tgl_hasil,
          c.hasil_rad,
          c.catatan_rad 
        FROM mst_item_rad a 
        LEFT JOIN (
          SELECT b.* FROM rad_pemeriksaan_rinc b 
          $where
        ) as c ON a.itemrad_id = c.itemrad_id 
        WHERE a.is_tagihan = 1 
        ORDER BY a.itemrad_id;"
      );
    } else {
      $query = $this->db->query(
        "SELECT 
          a.*
        FROM mst_item_rad a 
        WHERE a.is_tagihan = 1 
        ORDER BY a.itemrad_id;"
      );
    };
    return $query->result_array();
  }
}
