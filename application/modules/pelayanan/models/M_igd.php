<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_igd extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 AND a.lokasi_id = '03.01' ";
    if (@$cookie['search']['tgl_registrasi_from'] != '' && @$cookie['search']['tgl_registrasi_to']) {
      $where .= "AND (DATE(a.tgl_registrasi) BETWEEN '" . to_date(@$cookie['search']['tgl_registrasi_from']) . "' AND '" . to_date(@$cookie['search']['tgl_registrasi_to']) . "')";
    }
    if (@$cookie['search']['no_rm_nm'] != '') {
      $where .= "AND (a.pasien_id LIKE '%" . $this->db->escape_like_str($cookie['search']['no_rm_nm']) . "%' OR a.pasien_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['no_rm_nm']) . "%' ) ";
    }
    if (@$cookie['search']['lokasi_id'] != '') {
      $where .= "AND a.lokasi_id = '" . $this->db->escape_like_str($cookie['search']['lokasi_id']) . "' ";
    }
    if (@$cookie['search']['jenispasien_id'] != '') {
      $where .= "AND a.jenispasien_id = '" . $this->db->escape_like_str($cookie['search']['jenispasien_id']) . "' ";
    }
    if (@$cookie['search']['periksa_st'] != '') {
      $where .= "AND a.periksa_st = '" . $this->db->escape_like_str($cookie['search']['periksa_st']) . "' ";
    }
    if (@$cookie['search']['pulang_st'] != '') {
      $where .= "AND a.pulang_st = '" . $this->db->escape_like_str($cookie['search']['pulang_st']) . "' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
              a.*, 
              b.lokasi_nm, 
              c.jenispasien_nm, 
              d.parameter_val as jeniskunjungan_nm,
              e.*,
              f.kelas_nm  
            FROM reg_pasien a
            LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
            JOIN mst_parameter d ON a.jeniskunjungan_cd = d.parameter_cd AND d.parameter_field = 'jeniskunjungan_cd'
            LEFT JOIN mst_kelas f ON a.kelas_id = f.kelas_id 
            LEFT JOIN 
              ( 
                SELECT 
                  x.reg_id AS reg_id_tindaklanjut, 
                  x.subtindaklanjut_cd,
                  y.lokasi_nm AS lokasi_nm_tindaklanjut,
                  z.rsrujukan_nm
                FROM reg_pasien_tindaklanjut x
                LEFT JOIN mst_lokasi y ON x.lokasi_id = y.lokasi_id
                LEFT JOIN mst_rs_rujukan z ON x.rsrujukan_id = z.rsrujukan_id
              ) AS e ON a.reg_id = e.reg_id_tindaklanjut 
            $where
            ORDER BY a.tgl_registrasi DESC, "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM reg_pasien a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT a.*, b.lokasi_nm, c.jenispasien_nm, d.parameter_val as jeniskunjungan_nm FROM reg_pasien a
    LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
    LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
    JOIN mst_parameter d ON a.jeniskunjungan_cd = d.parameter_cd AND d.parameter_field = 'jeniskunjungan_cd'
    $where";
    $query = $this->db->query($sql);
    return $query->num_rows();
  }

  function get_data($id)
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm, c.jenispasien_nm, d.lokasi_nm, d.map_lokasi_depo, e.kelas_nm
            FROM reg_pasien a 
            LEFT JOIN mst_pegawai b ON a.dokter_id=b.pegawai_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id=c.jenispasien_id
            LEFT JOIN mst_lokasi d ON a.lokasi_id = d.lokasi_id
            LEFT JOIN mst_kelas e ON a.kelas_id = e.kelas_id
            WHERE a.reg_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function get_alergi($pasien_id)
  {
    return $this->db->where('pasien_id', $pasien_id)->get('dat_catatanmedis')->result_array();
  }

  public function get_diagnosis($reg_id)
  {
    $sql = "SELECT 
              a.diagnosis_id, a.reg_id, a.pasien_id, a.penyakit_id, a.icdx, b.penyakit_nm
            FROM dat_diagnosis a 
            LEFT JOIN mst_penyakit b ON a.penyakit_id=b.penyakit_id
            WHERE a.reg_id=?";
    $query = $this->db->query($sql, array($reg_id));
    $result = $query->result_array();
    return $result;
  }

  public function update_periksa_st($id, $periksa_st)
  {
    if ($periksa_st != '2') {
      $data['periksa_st'] = 1;
      $this->db->where('reg_id', $id)->update('reg_pasien', $data);
    }
  }


  // CATATAN MEDIS --------------------------------------
  public function catatan_medis_data($pasien_id = '', $reg_id = '', $lokasi_id = '')
  {
    $sql = "SELECT
              a.*,
              b.pegawai_nm AS petugas_nm 
            FROM
              dat_catatanmedis a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            WHERE
              a.is_deleted = 0 
              AND a.pasien_id =? 
              AND a.reg_id =? 
              AND a.lokasi_id =? 
            ORDER BY
              a.catatanmedis_id";
    $query = $this->db->query($sql, array($pasien_id, $reg_id, $lokasi_id));
    return $query->result_array();
  }

  public function catatan_medis_history($pasien_id = '', $reg_id = '')
  {
    $sql = "SELECT a.*, b.lokasi_nm 
            FROM dat_catatanmedis a 
            LEFT JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id 
            WHERE a.pasien_id = ? AND a.reg_id != ? 
            ORDER BY a.tgl_catat DESC";
    $query = $this->db->query($sql, array($pasien_id, $reg_id));
    return $query->result_array();
  }

  public function catatan_medis_get($catatanmedis_id = '', $reg_id = '', $pasien_id = '')
  {
    $sql = "SELECT
              a.*,
              DATE_FORMAT(a.tgl_catat, '%Y-%m-%d %H:%i:%s' ) AS tgl_catat, 
              b.pegawai_id AS petugas_id,
              b.pegawai_nm AS petugas_nm 
            FROM
              dat_catatanmedis a
              LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            WHERE
              a.catatanmedis_id =? 
              AND a.reg_id =? 
              AND a.pasien_id =?";
    $query = $this->db->query($sql, array($catatanmedis_id, $reg_id, $pasien_id));
    return $query->row_array();
  }

  public function catatan_medis_save()
  {
    $data = $this->input->post();
    $data['tgl_catat'] = to_date($data['tgl_catat'], '', 'full_date');
    $data['user_cd'] = $this->session->userdata('sess_user_cd');
    if ($data['catatanmedis_id'] == null) {
      $data['catatanmedis_id'] = get_id('dat_catatanmedis');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_catatanmedis', $data);
      update_id('dat_catatanmedis', $data['catatanmedis_id']);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('catatanmedis_id', $data['catatanmedis_id'])->update('dat_catatanmedis', $data);
    }
  }

  public function catatan_medis_delete($catatanmedis_id = '', $reg_id = '', $pasien_id = '')
  {
    trash('dat_catatanmedis', array(
      'catatanmedis_id' => $catatanmedis_id,
      'reg_id' => $reg_id,
      'pasien_id' => $pasien_id
    ));
    $this->db->where('catatanmedis_id', $catatanmedis_id)->where('reg_id', $reg_id)->where('pasien_id', $pasien_id)->delete('dat_catatanmedis');
  }

  // Pemeriksaan Medis
  public function pemeriksaan_fisik_data()
  {
    $data = $this->input->post();
    // return $this->db->where('reg_id', $data['reg_id'])->where('pasien_id', $data['pasien_id'])->get('dat_anamnesis')->row_array();
    $reg_id = @$data['reg_id'];
    $pasien_id = @$data['pasien_id'];
    $sql = "SELECT
              a.*,
              DATE_FORMAT(a.tgl_catat, '%Y-%m-%d %H:%i:%s' ) AS tgl_catat, 
              b.pegawai_id AS petugas_id,
              b.pegawai_nm AS petugas_nm 
            FROM
              dat_anamnesis a
              LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            WHERE
              a.reg_id = '$reg_id' 
              AND a.pasien_id = '$pasien_id'";
    $query = $this->db->query($sql);
    return $query->row_array();
  }

  public function pemeriksaan_fisik_history($pasien_id = '', $reg_id = '')
  {
    $sql = "SELECT a.*, b.lokasi_nm 
            FROM dat_anamnesis a 
            LEFT JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id
            WHERE a.pasien_id = ? AND a.reg_id != ? 
            ORDER BY a.tgl_catat DESC";
    $query = $this->db->query($sql, array($pasien_id, $reg_id));
    return $query->result_array();
  }

  public function pemeriksaan_fisik_save()
  {
    $data = $this->input->post();
    $data['suhu'] = convert_suhu($data['suhu']);
    $cek = $this->db->where('reg_id', $data['reg_id'])->where('pasien_id', $data['pasien_id'])->get('dat_anamnesis')->row_array();
    if ($cek == null) {
      $data['anamnesis_id'] = get_id('dat_anamnesis');
      $data['tgl_catat'] = date('Y-m-d H:i:s');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_anamnesis', $data);
      update_id('dat_anamnesis', $data['anamnesis_id']);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('reg_id', $data['reg_id'])->where('pasien_id', $data['pasien_id'])->update('dat_anamnesis', $data);
    }
  }

  // Diagnosis
  public function diagnosis_data($pasien_id = '', $reg_id = '', $lokasi_id = '')
  {
    $sql = "SELECT 
              a.*, b.penyakit_nm 
            FROM dat_diagnosis a
            LEFT JOIN mst_penyakit b ON a.penyakit_id=b.penyakit_id 
            WHERE a.is_deleted=0 AND a.pasien_id=? AND a.reg_id=? AND a.lokasi_id=? 
            ORDER BY a.diagnosis_id";
    $query = $this->db->query($sql, array($pasien_id, $reg_id, $lokasi_id));
    return $query->result_array();
  }

  public function diagnosis_history($pasien_id = '', $reg_id = '')
  {
    $sql = "SELECT 
              a.*, b.penyakit_nm, c.lokasi_nm 
            FROM dat_diagnosis a
            LEFT JOIN mst_penyakit b ON a.penyakit_id=b.penyakit_id 
            LEFt JOIN mst_lokasi c ON a.lokasi_id=c.lokasi_id
            WHERE a.pasien_id =? AND a.reg_id != ? 
            ORDER BY a.tgl_catat DESC";
    $query = $this->db->query($sql, array($pasien_id, $reg_id));
    return $query->result_array();
  }

  public function penyakit_autocomplete($penyakit_nm = null)
  {
    $sql = "SELECT 
              *
            FROM mst_penyakit a   
            WHERE (a.penyakit_nm LIKE '%$penyakit_nm%' OR a.icdx LIKE '%$penyakit_nm%')
            ORDER BY a.icdx";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['penyakit_id'] . "#" . $row['icdx'],
        'text' => $row['icdx'] . ' - ' . $row['penyakit_nm']
      );
    }
    return $res;
  }

  public function diagnosis_get($diagnosis_id = '', $reg_id = '', $pasien_id = '')
  {
    $sql = "SELECT 
              a.*, b.penyakit_nm, DATE_FORMAT(a.tgl_catat, '%Y-%m-%d %H:%i:%s') AS tgl_catat 
            FROM dat_diagnosis a 
            LEFT JOIN mst_penyakit b ON a.penyakit_id=b.penyakit_id
            WHERE a.diagnosis_id=? AND a.reg_id=? AND a.pasien_id=?";
    $query = $this->db->query($sql, array($diagnosis_id, $reg_id, $pasien_id));
    return $query->row_array();
  }

  public function diagnosis_save()
  {
    $data = $this->input->post();
    $penyakit_id = explode("#", $data['penyakit_id']);
    $data['penyakit_id'] = $penyakit_id[0];
    $data['icdx'] = $penyakit_id[1];
    $data['tgl_catat'] = to_date($data['tgl_catat'], '', 'full_date');
    $data['user_cd'] = $this->session->userdata('sess_user_cd');

    if ($data['diagnosis_id'] == null) {
      $data['diagnosis_id'] = get_id('dat_diagnosis');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_diagnosis', $data);
      update_id('dat_diagnosis', $data['diagnosis_id']);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('diagnosis_id', $data['diagnosis_id'])->update('dat_diagnosis', $data);
    }
  }

  public function diagnosis_delete($diagnosis_id = '', $reg_id = '', $pasien_id = '')
  {
    trash('dat_diagnosis', array(
      'diagnosis_id' => $diagnosis_id,
      'reg_id' => $reg_id,
      'pasien_id' => $pasien_id
    ));
    $this->db->where('diagnosis_id', $diagnosis_id)->where('reg_id', $reg_id)->where('pasien_id', $pasien_id)->delete('dat_diagnosis');
  }

  public function diagnosis_row($id)
  {
    $sql = "SELECT * FROM mst_penyakit WHERE penyakit_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  //Tindakan 
  public function tindakan_data($pasien_id = '', $reg_id = '', $lokasi_id = '')
  {
    $sql = "SELECT 
              a.*, 
              b.pegawai_nm AS pegawai_nm_1, 
              c.pegawai_nm AS pegawai_nm_2, 
              d.pegawai_nm AS pegawai_nm_3, 
              e.pegawai_nm AS pegawai_nm_4, 
              f.pegawai_nm AS pegawai_nm_5, 
              g.kelas_nm
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            LEFT JOIN mst_pegawai c ON a.petugas_id_2 = c.pegawai_id
            LEFT JOIN mst_pegawai d ON a.petugas_id_3 = d.pegawai_id
            LEFT JOIN mst_pegawai e ON a.petugas_id_4 = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_5 = f.pegawai_id
            LEFT JOIN mst_kelas g ON a.kelas_id = g.kelas_id 
            WHERE a.is_deleted = 0 AND a.pasien_id=? AND a.reg_id=? AND a.lokasi_id=? 
            ORDER BY a.tindakan_id";
    $query = $this->db->query($sql, array($pasien_id, $reg_id, $lokasi_id));
    return $query->result_array();
  }

  public function tindakan_history($pasien_id = '', $reg_id = '')
  {
    $sql = "SELECT 
              a.*, 
              b.pegawai_nm AS pegawai_nm_1, 
              c.pegawai_nm AS pegawai_nm_2, 
              d.pegawai_nm AS pegawai_nm_3, 
              e.pegawai_nm AS pegawai_nm_4, 
              f.pegawai_nm AS pegawai_nm_5, 
              g.kelas_nm,
              h.lokasi_nm 
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            LEFT JOIN mst_pegawai c ON a.petugas_id_2 = c.pegawai_id
            LEFT JOIN mst_pegawai d ON a.petugas_id_3 = d.pegawai_id
            LEFT JOIN mst_pegawai e ON a.petugas_id_4 = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_5 = f.pegawai_id
            LEFT JOIN mst_kelas g ON a.kelas_id = g.kelas_id 
            LEFT JOIN mst_lokasi h ON a.lokasi_id = h.lokasi_id 
            WHERE a.pasien_id =? AND a.reg_id != ? 
            ORDER BY a.tgl_catat DESC";
    $query = $this->db->query($sql, array($pasien_id, $reg_id));
    return $query->result_array();
  }

  public function tarifkelas_autocomplete($tarif_nm = null, $kelas_id = '')
  {
    if ($kelas_id != '') {
      $where = "AND c.kelas_id = '$kelas_id'";
    } else {
      $where = "";
    }

    $sql = "SELECT 
              a.*,b.tarif_nm,c.kelas_nm
            FROM mst_tarif_kelas a
            JOIN mst_tarif b ON a.tarif_id = b.tarif_id
            JOIN mst_kelas c ON a.kelas_id = c.kelas_id
            WHERE b.tarif_nm LIKE '%$tarif_nm%' $where ";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['tarifkelas_id'],
        'text' => $row['tarifkelas_id'] . ' - ' . $row['tarif_nm'] . ' - ' . $row['kelas_nm']
      );
    }
    return $res;
  }
  public function prosedur_autocomplete($prosedur_nm = null)
  {
    $sql = "SELECT 
              a.*
            FROM mst_prosedur a
            WHERE a.prosedur_nm LIKE '%$prosedur_nm%' ";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['prosedur_id'],
        'text' => $row['prosedur_id'] . ' - ' . $row['prosedur_nm']
      );
    }
    return $res;
  }

  public function petugas_autocomplete($petugas_nm = null)
  {
    $sql = "SELECT 
              a.*
            FROM mst_pegawai a
            WHERE a.pegawai_nm LIKE '%$petugas_nm%' ";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['pegawai_id'],
        'text' => $row['pegawai_nm']
      );
    }
    return $res;
  }

  function get_kelas($kelas_id = null)
  {
    $sql = "SELECT 
              a.kelas_nm
            FROM mst_kelas a
            WHERE a.kelas_id=?";
    $query = $this->db->query($sql, array($kelas_id));
    $row = $query->row_array();
    return $row;
  }

  public function tindakan_save()
  {
    $data = $this->input->post();
    unset($data['petugas_no']);
    $data['tgl_catat'] = to_date($data['tgl_catat'], '', 'full_date');
    $data['user_cd'] = $this->session->userdata('sess_user_cd');
    $tk = $this->db
      ->select('a.*, b.tarif_nm')
      ->join('mst_tarif b', 'a.tarif_id = b.tarif_id', 'left')
      ->where('a.tarifkelas_id', $data['tarifkelas_id'])
      ->get('mst_tarif_kelas a')->row_array();
    unset($data['tarifkelas_id']);
    $data['tarif_id'] = $tk['tarif_id'];
    $data['kelas_id'] = $tk['kelas_id'];
    $data['tarif_nm'] = $tk['tarif_nm'];
    $data['js'] = $tk['js'];
    $data['jp'] = $tk['jp'];
    $data['jb'] = $tk['jb'];
    $data['nom_tarif'] = $tk['nominal'];
    $data['jml_awal'] = $tk['nominal'] * $data['qty'];
    $data['jml_tagihan'] = $tk['nominal'] * $data['qty'];

    // petugas
    $d = $data;
    unset($d['petugas_id'], $d['petugas_id_2'], $d['petugas_id_3'], $d['petugas_id_4'], $d['petugas_id_5']);
    $petugas = array();
    for ($i = 1; $i <= 5; $i++) {
      if ($i == '1') {
        $petugas_id = $data['petugas_id'];
      } else {
        $petugas_id = $data['petugas_id_' . $i];
      }

      if ($petugas_id != '0') {
        array_push($petugas, $petugas_id);
      }
    }

    for ($j = 0; $j < count($petugas); $j++) {
      $d['petugas_id_' . ($j + 1)] = $petugas[$j];
    }

    unset($data['petugas_id'], $data['petugas_id_2'], $data['petugas_id_3'], $data['petugas_id_4'], $data['petugas_id_5']);

    $data['petugas_id'] = @$d['petugas_id_1'];
    $data['petugas_id_2'] = @$d['petugas_id_2'];
    $data['petugas_id_3'] = @$d['petugas_id_3'];
    $data['petugas_id_4'] = @$d['petugas_id_4'];
    $data['petugas_id_5'] = @$d['petugas_id_5'];

    //prosedur  
    $prosedur = $this->db->where('prosedur_id', $data['prosedur_id'])->get('mst_prosedur')->row_array();
    $data['prosedur_nm'] = $prosedur['prosedur_nm'];

    if ($data['tindakan_id'] == null) {
      $data['tindakan_id'] = get_id('dat_tindakan');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_tindakan', $data);
      update_id('dat_tindakan', $data['tindakan_id']);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('tindakan_id', $data['tindakan_id'])->update('dat_tindakan', $data);
    }
  }

  public function tindakan_get($tindakan_id = '', $reg_id = '', $pasien_id = '')
  {
    $sql = "SELECT 
              a.*, b.kelas_nm, c.pegawai_nm, DATE_FORMAT(a.tgl_catat, '%Y-%m-%d %H:%i:%s') AS tgl_catat
            FROM dat_tindakan a
            LEFT JOIN mst_kelas b ON a.kelas_id = b.kelas_id
            LEFT JOIN mst_pegawai c ON a.petugas_id = c.pegawai_id 
            WHERE a.tindakan_id=? AND a.reg_id=? AND a.pasien_id=?";
    $query = $this->db->query($sql, array($tindakan_id, $reg_id, $pasien_id));
    $row = $query->row_array();
    $jml_petugas = 0;
    if ($row['petugas_id'] != '') {
      $jml_petugas += 1;
    }
    if ($row['petugas_id_2'] != '') {
      $jml_petugas += 1;
    }
    if ($row['petugas_id_3'] != '') {
      $jml_petugas += 1;
    }
    if ($row['petugas_id_4'] != '') {
      $jml_petugas += 1;
    }
    if ($row['petugas_id_5'] != '') {
      $jml_petugas += 1;
    }
    $row['jml_petugas'] = $jml_petugas;
    return $row;
  }

  public function tindakan_delete($tindakan_id = '', $reg_id = '', $pasien_id = '')
  {
    trash('dat_tindakan', array(
      'tindakan_id' => $tindakan_id,
      'reg_id' => $reg_id,
      'pasien_id' => $pasien_id
    ));
    $this->db->where('tindakan_id', $tindakan_id)->where('reg_id', $reg_id)->where('pasien_id', $pasien_id)->delete('dat_tindakan');
  }

  public function tarifkelas_row($id)
  {
    $sql = "SELECT a.*, b.tarif_nm, c.kelas_nm FROM mst_tarif_kelas a
      LEFT JOIN mst_tarif b ON a.tarif_id = b.tarif_id
      LEFT JOIN mst_kelas c ON a.kelas_id = c.kelas_id
      WHERE tarifkelas_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  public function petugas_row($id)
  {
    $sql = "SELECT * FROM mst_pegawai WHERE pegawai_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  public function get_tindakan($tindakan_id = null)
  {
    $sql = "SELECT 
              a.*, 
              b.pegawai_nm AS pegawai_nm_1, 
              c.pegawai_nm AS pegawai_nm_2, 
              d.pegawai_nm AS pegawai_nm_3, 
              e.pegawai_nm AS pegawai_nm_4, 
              f.pegawai_nm AS pegawai_nm_5
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            LEFT JOIN mst_pegawai c ON a.petugas_id_2 = c.pegawai_id
            LEFT JOIN mst_pegawai d ON a.petugas_id_3 = d.pegawai_id
            LEFT JOIN mst_pegawai e ON a.petugas_id_4 = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_5 = f.pegawai_id
            WHERE a.tindakan_id=?";
    $query = $this->db->query($sql, $tindakan_id);
    return $query->row_array();
  }

  public function delete_petugas($tindakan_id = null, $form_name = null)
  {
    $data[$form_name] = '';
    $query = $this->db->where('tindakan_id', $tindakan_id)->update('dat_tindakan', $data);
    return $query;
  }


  // Pemberian Obat
  public function pemberian_obat_data($reg_id = '', $pasien_id = '', $lokasi_id = '')
  {
    $sql = "SELECT 
              a.* 
            FROM dat_resep a
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pasien_id=? AND a.src_resep_st = 1 AND a.lokasi_id =?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pasien_id, $lokasi_id));
    $result = $query->result_array();
    // 
    foreach ($result as $key => $val) {
      $result[$key]['list_dat_resep'] = $this->list_dat_resep($val['resep_id'], $reg_id, $pasien_id);
    }
    return $result;
  }

  public function list_dat_resep($resep_id = '', $reg_id = '', $pasien_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_farmasi a
            LEFT JOIN mst_obat b ON a.obat_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.resep_id=? AND a.reg_id=? AND a.pasien_id=?
            ORDER BY a.reg_id ASC";
    $query = $this->db->query($sql, array($resep_id, $reg_id, $pasien_id));
    return $query->result_array();
  }

  public function pemberian_obat_history($reg_id = '', $pasien_id = '')
  {
    $sql = "SELECT 
              a.*, b.lokasi_nm 
            FROM dat_resep_group a
            LEFT JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id
            WHERE a.is_deleted=0 AND a.reg_id != ? AND a.pasien_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pasien_id));
    $result = $query->result_array();
    // 
    foreach ($result as $key => $val) {
      $result[$key]['list_dat_resep'] = $this->list_dat_resep_history($val['resepgroup_id'], $reg_id, $pasien_id);
    }
    return $result;
  }

  public function list_dat_resep_history($resepgroup_id = '', $reg_id = '', $pasien_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_resep a
            LEFT JOIN mst_obat b ON a.obat_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.resepgroup_id=? AND a.reg_id != ? AND a.pasien_id=? 
            ORDER BY a.reg_id ASC";
    $query = $this->db->query($sql, array($resepgroup_id, $reg_id, $pasien_id));
    return $query->result_array();
  }

  public function obat_autocomplete($obat_nm = null, $map_lokasi_depo = null)
  {
    $sql = "SELECT 
              *
            FROM far_stok_depo a   
            WHERE 
              (a.obat_nm LIKE '%$obat_nm%' OR a.obat_id LIKE '%$obat_nm%') AND
              a.stok_st = 1 AND a.stok_akhir > 0 AND a.lokasi_id = '$map_lokasi_depo'
            ORDER BY a.obat_id";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['obat_id'] . "#" . $row['obat_nm'] . "#" . $row['stokdepo_id'],
        'text' => $row['obat_id'] . ' - ' . $row['obat_nm']
      );
    }
    return $res;
  }

  public function pemberian_obat_get($farmasi_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm, DATE_FORMAT(a.tgl_catat, '%Y-%m-%d %H:%i:%s') AS tgl_catat 
            FROM dat_farmasi a 
            LEFT JOIN mst_obat b ON a.obat_id=b.obat_id
            WHERE a.farmasi_id=?";
    $query = $this->db->query($sql, array($farmasi_id));
    return $query->row_array();
  }

  public function pemberian_obat_save()
  {
    $data = $this->input->post();

    $reg = $this->db->where('reg_id', $data['reg_id'])
      ->get('reg_pasien')->row_array();

    $tgl = substr($data['tgl_catat'], 0, 2);
    $bln = substr($data['tgl_catat'], 3, 2);
    $thn = substr($data['tgl_catat'], 8, 2);

    $obat_raw = explode("#", $data['obat_id']);
    $obat_id = $obat_raw[0];
    $obat_nm = $obat_raw[1];
    $stokdepo_id = @$obat_raw[2];

    $obat = $this->db->where('obat_id', $obat_id)->get('mst_obat')->row_array();
    $stokdepo = $this->db->where('stokdepo_id', $stokdepo_id)->get('far_stok_depo')->row_array();

    $resep_prefix = $thn . $bln . $tgl;
    $resep_id = $data['resep_id'];
    $resep = array();
    //penentuan resep
    if ($resep_id == 'otomatis') {
      // get resep 
      $last_resep = $this->db->query(
        "SELECT * FROM dat_resep 
        WHERE src_resep_st = 1 
          AND reg_id = '" . $data['reg_id'] . "' 
          AND resep_id LIKE '$resep_prefix%' ORDER BY resep_id DESC"
      )->row_array();

      if ($last_resep == null) {
        //membuat resep baru
        $data_resep = array(
          'resep_id' => get_id('dat_resep'),
          'reg_id' => $data['reg_id'],
          'src_resep_st' => 1,
          'dokter_id' => $reg['dokter_id'],
          'pasien_id' => $reg['pasien_id'],
          'lokasi_id' => $reg['lokasi_id'],
          'tgl_catat' => date('Y-m-d H:i:s'),
          'created_at' => date('Y-m-d H:i:s'),
          'created_by' => $this->session->userdata('sess_user_realname'),
        );
        $this->db->insert("dat_resep", $data_resep);
        update_id('dat_resep', $data_resep['resep_id']);
        $resep = $this->db->where('resep_id', $data_resep['resep_id'])->get('dat_resep')->row_array();
      } else {
        $resep = $this->db->where('resep_id', $last_resep['resep_id'])->get('dat_resep')->row_array();
      }
      //insert obat
      $dfarmasi = array(
        'farmasi_id' => get_id('dat_farmasi'),
        'resep_id' => $resep['resep_id'],
        'reg_id' => $data['reg_id'],
        'dokter_id' => $reg['dokter_id'],
        'pasien_id' => $reg['pasien_id'],
        'lokasi_id' => $reg['lokasi_id'],
        'stokdepo_id' => $stokdepo_id,
        'obat_id' => $obat_id,
        'obat_nm' => $obat_nm,
        'harga' => @clear_numeric($stokdepo['harga_jual']),
        'qty' => @clear_numeric($data['qty']),
        'jumlah_awal' => @clear_numeric($stokdepo['harga_jual']) * @clear_numeric($data['qty']),
        'potongan' => 0,
        'jumlah_akhir' => @clear_numeric($stokdepo['harga_jual']) * @clear_numeric($data['qty']),
        'aturan_pakai' => $data['aturan_pakai'],
        'jadwal' => $data['jadwal'],
        'aturan_tambahan' => $data['aturan_tambahan'],
        'tgl_catat' => date('Y-m-d H:i:s'),
        'user_cd' => $this->session->userdata('sess_user_cd'),
        'created_by' => $this->session->userdata('sess_user_realname'),
        'created_at' => date('Y-m-d H:i:s'),
      );
      update_id('dat_farmasi', $dfarmasi['farmasi_id']);
      $this->db->insert('dat_farmasi', $dfarmasi);
    } else {
      //update
      $resep = $this->db->where('resep_id', $resep_id)->get('dat_resep')->row_array();
      $dfarmasi = array(
        'resep_id' => $resep['resep_id'],
        'reg_id' => $data['reg_id'],
        'dokter_id' => $reg['dokter_id'],
        'pasien_id' => $reg['pasien_id'],
        'lokasi_id' => $reg['lokasi_id'],
        'stokdepo_id' => $stokdepo_id,
        'obat_id' => $obat_id,
        'obat_nm' => $obat_nm,
        'harga' => @clear_numeric($stokdepo['harga_jual']),
        'qty' => @clear_numeric($data['qty']),
        'jumlah_awal' => @clear_numeric($stokdepo['harga_jual']) * @clear_numeric($data['qty']),
        'potongan' => 0,
        'jumlah_akhir' => @clear_numeric($stokdepo['harga_jual']) * @clear_numeric($data['qty']),
        'aturan_pakai' => $data['aturan_pakai'],
        'jadwal' => $data['jadwal'],
        'aturan_tambahan' => $data['aturan_tambahan'],
        'tgl_catat' => date('Y-m-d H:i:s'),
        'user_cd' => $this->session->userdata('sess_user_cd'),
        'created_by' => $this->session->userdata('sess_user_realname'),
        'created_at' => date('Y-m-d H:i:s'),
      );
      $this->db->where('farmasi_id', $data['farmasi_id'])->update('dat_farmasi', $dfarmasi);
    }

    $this->farmasi_grand($resep['resep_id']);
  }

  public function farmasi_grand($resep_id)
  {
    $farmasi = $this->db->where('resep_id', $resep_id)->get('dat_farmasi')->result_array();
    $resep = $this->db->where('resep_id', $resep_id)->get('dat_resep')->row_array();
    $grand_jml_bruto = 0;
    foreach ($farmasi as $row) {
      $grand_jml_bruto += $row['jumlah_akhir'];
    }
    $grand_nom_ppn = $resep['grand_prs_ppn'] * $grand_jml_bruto;
    $grand_jml_tagihan = $grand_jml_bruto + $grand_nom_ppn + $resep['grand_embalace'] - $resep['grand_jml_potongan'] + $resep['grand_pembulatan'];
    $d = array(
      'grand_jml_bruto' => $grand_jml_bruto,
      'grand_nom_ppn' => $grand_nom_ppn,
      'grand_jml_tagihan' => $grand_jml_tagihan
    );
    $this->db->where('resep_id', $resep_id)->update('dat_resep', $d);
  }

  public function get_jml($resepgroup_id = '')
  {
    $sql = "SELECT 
              SUM(jml_awal) as group_jml_awal,
              SUM(jml_potongan) as group_jml_potongan,
              SUM(jml_tagihan) as group_jml_tagihan
            FROM dat_resep
            WHERE resepgroup_id=?";
    $query = $this->db->query($sql, $resepgroup_id);
    return $query->row_array();
  }

  public function cek_by_reg_id($reg_id = '', $tgl_catat = '')
  {
    $tgl_catat = to_date($tgl_catat, '', 'date');
    $sql = "SELECT *
            FROM dat_resep
            WHERE reg_id=? AND DATE(tgl_catat)='$tgl_catat'
            ORDER BY resepgroup_id DESC";
    $query = $this->db->query($sql, $reg_id);
    return $query->row_array();
  }

  public function pemberian_obat_delete($resep_id = '', $reg_id = '', $farmasi_id = '')
  {
    $this->db->where('farmasi_id', $farmasi_id)->delete('dat_farmasi');

    $this->farmasi_grand($resep_id);
  }

  public function obat_row($data)
  {
    $sql = "SELECT * FROM far_stok_depo WHERE stokdepo_id=?";
    $query = $this->db->query($sql, array($data['stokdepo_id']));
    return $query->row_array();
  }

  public function obat_row_master($id)
  {
    $sql = "SELECT * FROM mst_obat WHERE obat_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  public function get_no_resep($reg_id)
  {
    $sql = "SELECT * FROM dat_resep WHERE reg_id=? ORDER BY resep_id DESC LIMIT 1";
    $query = $this->db->query($sql, $reg_id);
    $row = $query->row_array();
    if ($row['resepgroup_id'] != '') {
      $row['resepgroup_id'] = $row['resepgroup_id'];
    } else {
      $row['resepgroup_id'] = 'otomatis';
    }
    return $row;
  }

  public function count_dat_resep($resepgroup_id = '')
  {
    $sql = "SELECT 
              COUNT(*) AS jml_data
            FROM dat_resep
            WHERE resepgroup_id=?";
    $query = $this->db->query($sql, $resepgroup_id);
    $row = $query->row_array();
    return $row['jml_data'];
  }

  public function dosis_autocomplete()
  {
    $search = $_GET['term'];
    $sql = "SELECT dosis FROM dat_resep WHERE dosis LIKE '%" . $search . "%' GROUP BY dosis ORDER BY resep_id ASC";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    foreach ($result as $row) {
      $data[] = $row['dosis'];
    }
    return $data;
  }

  public function peringatan_khusus_autocomplete()
  {
    $search = $_GET['term'];
    $sql = "SELECT peringatan_khusus FROM dat_resep WHERE peringatan_khusus LIKE '%" . $search . "%' GROUP BY peringatan_khusus ORDER BY resep_id ASC";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    foreach ($result as $row) {
      $data[] = $row['peringatan_khusus'];
    }
    return $data;
  }

  public function jam_minum_autocomplete()
  {
    $search = $_GET['term'];
    $sql = "SELECT jam_minum FROM dat_resep WHERE jam_minum LIKE '%" . $search . "%' GROUP BY jam_minum ORDER BY resep_id ASC";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    foreach ($result as $row) {
      $data[] = $row['jam_minum'];
    }
    return $data;
  }

  // BHP
  public function bhp_data($pasien_id = '', $reg_id = '', $lokasi_id = '')
  {
    $sql = "SELECT 
              a.*
            FROM dat_bhp a
            WHERE a.is_deleted=0 AND a.pasien_id=? AND a.reg_id=? AND a.lokasi_id=? 
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($pasien_id, $reg_id, $lokasi_id));
    return $query->result_array();
  }

  public function bhp_history($pasien_id = '', $reg_id = '')
  {
    $sql = "SELECT a.*, b.lokasi_nm 
            FROM dat_bhp a
            LEFT JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id 
            WHERE a.pasien_id=? AND a.reg_id != ? 
            ORDER BY a.tgl_catat DESC";
    $query = $this->db->query($sql, array($pasien_id, $reg_id));
    return $query->result_array();
  }

  public function bhp_autocomplete($obat_nm = null, $map_lokasi_depo = null)
  {
    $sql = "SELECT 
            *
          FROM far_stok_depo a
          WHERE 
            (a.obat_nm LIKE '%$obat_nm%' OR a.obat_id LIKE '%$obat_nm%') AND
            a.stok_st = 1 AND a.stok_akhir > 0 AND a.lokasi_id = '$map_lokasi_depo' 
            AND a.jenisbarang_cd = '03'
          ORDER BY a.obat_id";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['obat_id'] . "#" . $row['obat_nm'] . "#" . $row['stokdepo_id'],
        'text' => $row['obat_id'] . ' - ' . $row['obat_nm']
      );
    }
    return $res;
  }

  public function bhp_row($data)
  {
    $sql = "SELECT * FROM far_stok_depo WHERE stokdepo_id=?";
    $query = $this->db->query($sql, array($data['stokdepo_id']));
    return $query->row_array();
  }

  public function bhp_get($bhp_id = '', $reg_id = '')
  {
    $sql = "SELECT 
              a.*, DATE_FORMAT(a.tgl_catat, '%Y-%m-%d %H:%i:%s') AS tgl_catat
            FROM dat_bhp a 
            WHERE a.bhp_id=? AND a.reg_id=?";
    $query = $this->db->query($sql, array($bhp_id, $reg_id));
    return $query->row_array();
  }

  public function bhp_save()
  {
    $data = $this->input->post();
    $barang_id = explode("#", $data['barang_id']);
    $data['barang_id'] = $barang_id[0];
    $data['barang_nm'] = $barang_id[1];
    $data['stokdepo_id'] = @$barang_id[2];

    $far_stok_depo = $this->db->where('stokdepo_id', @$barang_id[2])->get('far_stok_depo')->row_array();

    $data['stokdepo_id'] = $far_stok_depo['stokdepo_id'];
    $data['harga'] = $far_stok_depo['harga_akhir'];
    $data['jml_awal'] = $data['harga'] * $data['qty'];
    $data['jml_potongan'] = null;
    $data['jml_tagihan'] = $data['jml_awal'] - $data['jml_potongan'];
    $data['tgl_catat'] = to_date($data['tgl_catat'], '', 'full_date');
    $data['user_cd'] = $this->session->userdata('sess_user_cd');

    $qty_sebelum = $data['qty_sebelum'];
    $stokdepo_id_sebelum = $data['stokdepo_id_sebelum'];
    unset($data['qty_sebelum'], $data['stokdepo_id_sebelum']);

    if ($data['bhp_id'] == null) {
      $data['bhp_id'] = get_id('dat_bhp');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_bhp', $data);
      update_id('dat_bhp', $data['bhp_id']);

      // update far_stok_depo
      $sql = "UPDATE far_stok_depo 
              SET stok_terpakai=ifnull(stok_terpakai,0) + '" . $data['qty'] . "', 
                  stok_akhir=ifnull(stok_akhir,0) - '" . $data['qty'] . "' 
              WHERE stokdepo_id='" . $far_stok_depo['stokdepo_id'] . "'";
      $this->db->query($sql);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('bhp_id', $data['bhp_id'])->update('dat_bhp', $data);

      // update far_stok_depo
      if ($stokdepo_id_sebelum != $far_stok_depo['stokdepo_id']) {
        $sql1 = "UPDATE far_stok_depo 
              SET stok_terpakai=ifnull(stok_terpakai,0) - '" . $qty_sebelum . "', 
                  stok_akhir=ifnull(stok_akhir,0) + '" . $qty_sebelum . "' 
              WHERE stokdepo_id='" . $stokdepo_id_sebelum . "'";
        $this->db->query($sql1);

        $sql2 = "UPDATE far_stok_depo 
              SET stok_terpakai=ifnull(stok_terpakai,0) + '" . $data['qty'] . "', 
                  stok_akhir=ifnull(stok_akhir,0) - '" . $data['qty'] . "' 
              WHERE stokdepo_id='" . $far_stok_depo['stokdepo_id'] . "'";
        $this->db->query($sql2);
      } else {
        $sql = "UPDATE far_stok_depo 
                SET stok_terpakai=ifnull(stok_terpakai,0) - '" . $qty_sebelum . "' + '" . $data['qty'] . "', 
                    stok_akhir=ifnull(stok_akhir,0) + '" . $qty_sebelum . "' - '" . $data['qty'] . "' 
                WHERE stokdepo_id='" . $far_stok_depo['stokdepo_id'] . "'";
        $this->db->query($sql);
      }
    }
  }

  public function bhp_delete($bhp_id = '', $reg_id = '')
  {
    trash('dat_bhp', array(
      'bhp_id' => $bhp_id,
      'reg_id' => $reg_id
    ));

    $get_bhp = $this->db->where('bhp_id', $bhp_id)->where('reg_id', $reg_id)->get('dat_bhp')->row_array();

    $this->db->where('bhp_id', $bhp_id)->where('reg_id', $reg_id)->delete('dat_bhp');

    // update far_stok_depo
    $sql = "UPDATE far_stok_depo 
          SET stok_terpakai=ifnull(stok_terpakai,0) - '" . $get_bhp['qty'] . "', 
              stok_akhir=ifnull(stok_akhir,0) + '" . $get_bhp['qty'] . "' 
          WHERE stokdepo_id='" . $get_bhp['stokdepo_id'] . "'";
    $this->db->query($sql);
  }


  // ALKES
  public function alkes_data($pasien_id = '', $reg_id = '', $lokasi_id = '')
  {
    $sql = "SELECT 
              a.*
            FROM dat_alkes a
            WHERE a.is_deleted=0 AND a.pasien_id=? AND a.reg_id=? AND a.lokasi_id=? 
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($pasien_id, $reg_id, $lokasi_id));
    return $query->result_array();
  }

  public function alkes_history($pasien_id = '', $reg_id = '')
  {
    $sql = "SELECT a.*, b.lokasi_nm 
            FROM dat_alkes a
            LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id 
            WHERE a.pasien_id =? AND a.reg_id != ? 
            ORDER BY a.tgl_catat DESC";
    $query = $this->db->query($sql, array($pasien_id, $reg_id));
    return $query->result_array();
  }

  public function alkes_autocomplete($obat_nm = null, $map_lokasi_depo = null)
  {
    $sql = "SELECT 
            *
          FROM far_stok_depo a   
          WHERE 
            (a.obat_nm LIKE '%$obat_nm%' OR a.obat_id LIKE '%$obat_nm%') AND
            a.stok_st = 1 AND a.stok_akhir > 0 AND a.lokasi_id = '$map_lokasi_depo' 
            AND a.jenisbarang_cd = '02'
          ORDER BY a.obat_id";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['obat_id'] . "#" . $row['obat_nm'] . "#" . $row['stokdepo_id'],
        'text' => $row['obat_id'] . ' - ' . $row['obat_nm']
      );
    }
    return $res;
  }

  public function alkes_row($data)
  {
    $sql = "SELECT * FROM far_stok_depo WHERE stokdepo_id=?";
    $query = $this->db->query($sql, array($data['stokdepo_id']));
    return $query->row_array();
  }

  public function alkes_get($alkes_id = '', $reg_id = '')
  {
    $sql = "SELECT 
              a.*, DATE_FORMAT(a.tgl_catat, '%Y-%m-%d %H:%i:%s') AS tgl_catat
            FROM dat_alkes a 
            WHERE a.alkes_id=? AND a.reg_id=?";
    $query = $this->db->query($sql, array($alkes_id, $reg_id));
    return $query->row_array();
  }

  public function alkes_save()
  {
    $data = $this->input->post();
    $barang_id = explode("#", $data['barang_id']);
    $data['barang_id'] = $barang_id[0];
    $data['barang_nm'] = $barang_id[1];
    $data['stokdepo_id'] = @$barang_id[2];

    $far_stok_depo = $this->db->where('stokdepo_id', @$barang_id[2])->get('far_stok_depo')->row_array();

    $data['stokdepo_id'] = $far_stok_depo['stokdepo_id'];
    $data['harga'] = $far_stok_depo['harga_akhir'];
    $data['jml_awal'] = $data['harga'] * $data['qty'];
    $data['jml_potongan'] = null;
    $data['jml_tagihan'] = $data['jml_awal'] - $data['jml_potongan'];
    $data['tgl_catat'] = to_date($data['tgl_catat'], '', 'full_date');
    $data['user_cd'] = $this->session->userdata('sess_user_cd');

    $qty_sebelum = $data['qty_sebelum'];
    $stokdepo_id_sebelum = $data['stokdepo_id_sebelum'];
    unset($data['qty_sebelum'], $data['stokdepo_id_sebelum']);

    if ($data['alkes_id'] == null) {
      $data['alkes_id'] = get_id('dat_alkes');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_alkes', $data);
      update_id('dat_alkes', $data['alkes_id']);

      // update far_stok_depo
      $sql = "UPDATE far_stok_depo 
              SET stok_terpakai=ifnull(stok_terpakai,0) + '" . $data['qty'] . "', 
                  stok_akhir=ifnull(stok_akhir,0) - '" . $data['qty'] . "' 
              WHERE stokdepo_id='" . $far_stok_depo['stokdepo_id'] . "'";
      $this->db->query($sql);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('alkes_id', $data['alkes_id'])->update('dat_alkes', $data);

      // update far_stok_depo
      if ($stokdepo_id_sebelum != $far_stok_depo['stokdepo_id']) {
        $sql1 = "UPDATE far_stok_depo 
              SET stok_terpakai=ifnull(stok_terpakai,0) - '" . $qty_sebelum . "', 
                  stok_akhir=ifnull(stok_akhir,0) + '" . $qty_sebelum . "' 
              WHERE stokdepo_id='" . $stokdepo_id_sebelum . "'";
        $this->db->query($sql1);

        $sql2 = "UPDATE far_stok_depo 
              SET stok_terpakai=ifnull(stok_terpakai,0) + '" . $data['qty'] . "', 
                  stok_akhir=ifnull(stok_akhir,0) - '" . $data['qty'] . "' 
              WHERE stokdepo_id='" . $far_stok_depo['stokdepo_id'] . "'";
        $this->db->query($sql2);
      } else {
        $sql = "UPDATE far_stok_depo 
                SET stok_terpakai=ifnull(stok_terpakai,0) - '" . $qty_sebelum . "' + '" . $data['qty'] . "', 
                    stok_akhir=ifnull(stok_akhir,0) + '" . $qty_sebelum . "' - '" . $data['qty'] . "' 
                WHERE stokdepo_id='" . $far_stok_depo['stokdepo_id'] . "'";
        $this->db->query($sql);
      }
    }
  }

  public function alkes_delete($alkes_id = '', $reg_id = '')
  {
    trash('dat_alkes', array(
      'alkes_id' => $alkes_id,
      'reg_id' => $reg_id
    ));

    $get_alkes = $this->db->where('alkes_id', $alkes_id)->where('reg_id', $reg_id)->get('dat_alkes')->row_array();

    $this->db->where('alkes_id', $alkes_id)->where('reg_id', $reg_id)->delete('dat_alkes');

    // update far_stok_depo
    $sql = "UPDATE far_stok_depo 
          SET stok_terpakai=ifnull(stok_terpakai,0) - '" . $get_alkes['qty'] . "', 
              stok_akhir=ifnull(stok_akhir,0) + '" . $get_alkes['qty'] . "' 
          WHERE stokdepo_id='" . $get_alkes['stokdepo_id'] . "'";
    $this->db->query($sql);
  }

  //Laboratorium -------------------------------------------------------------------------------------
  public function laboratorium_data($reg_id = '', $pasien_id = '', $lokasi_id = '')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM lab_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE src_reg_id=? AND pasien_id=? AND src_lokasi_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pasien_id, $lokasi_id));
    return $query->result_array();
  }

  public function laboratorium_history($reg_id = '', $pasien_id = '')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM lab_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE a.reg_id != ? AND pasien_id=?
            ORDER BY a.tgl_order DESC";
    $query = $this->db->query($sql, array($reg_id, $pasien_id));
    return $query->result_array();
  }

  public function laboratorium_save($reg_id = null, $id = null)
  {
    $d = $this->input->post();
    $data = $d;

    unset($data['checkitem']);
    unset($data['laboratorium_table_length']);
    unset($data['kelas_id']);

    // insert into pemeriksaan lab
    $data['tgl_order'] = to_date($data['tgl_order'], '-', 'full_date');
    if ($id == null) {
      $data['pemeriksaan_id'] = get_id('lab_pemeriksaan');
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('lab_pemeriksaan', $data);
      update_id('lab_pemeriksaan', $data['pemeriksaan_id']);
    } else {
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('pemeriksaan_id', $id)->update('lab_pemeriksaan', $data);
    }

    // $reg = $this->db->where('reg_id', $reg_id)->get('reg_pasien')->row_array();
    // //delete all rincian
    // // $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->delete('lab_pemeriksaan_rinc');
    // $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->where('lokasi_id', '03.02')->delete('dat_tindakan');
    // if (isset($d['checkitem'])) {
    //   $rinc_raw = $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->get('lab_pemeriksaan_rinc')->result_array();
    //   $rinc = array();
    //   foreach ($rinc_raw as $v) {
    //     array_push($rinc, $v['itemlab_id']);
    //   }

    //   foreach ($d['checkitem'] as $v) {
    //     // if checkitem not in $rinc insert to pemeriksaan_rinc
    //     if (!in_array($v, $rinc)) {
    //       $item = $this->db->query("SELECT * FROM mst_item_lab WHERE itemlab_id LIKE '$v%' ")->result_array();
    //       foreach ($item as $row) {
    //         $dr = array(
    //           'pemeriksaanrinc_id' => get_id('lab_pemeriksaan_rinc'),
    //           'pemeriksaan_id' => $data['pemeriksaan_id'],
    //           'itemlab_id' => $row['itemlab_id'],
    //           'kelas_id' => $d['kelas_id'],
    //           'is_tagihan' => $row['is_tagihan'],
    //           'is_periksa' => $row['is_periksa'],
    //           'tarif_id' => $row['tarif_id'],
    //           'user_cd' => $this->session->userdata('sess_user_cd'),
    //           'created_at' => date('Y-m-d H:i:s'),
    //           'created_by' => $this->session->userdata('sess_user_realname'),
    //         );
    //         $this->db->insert('lab_pemeriksaan_rinc', $dr);
    //         update_id('lab_pemeriksaan_rinc', $dr['pemeriksaanrinc_id']);
    //       }
    //     }
    //   }

    //   foreach ($rinc as $v) {
    //     if (!in_array($v, $d['checkitem'])) {
    //       // if rinc not in check item then delete
    //       $this->db->query("DELETE FROM mst_item_lab WHERE itemlab_id LIKE '$v%' ");
    //     }
    //   }
    // }

    $old = $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->get('lab_pemeriksaan_rinc')->result_array();
    $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->delete('lab_pemeriksaan_rinc');
    if (isset($d['checkitem'])) {
      foreach ($d['checkitem'] as $v) {
        $item = $this->db->query("SELECT * FROM mst_item_lab WHERE itemlab_id LIKE '$v%' ")->result_array();
        foreach ($item as $row) {
          $dr = array(
            'pemeriksaanrinc_id' => get_id('lab_pemeriksaan_rinc'),
            'pemeriksaan_id' => $data['pemeriksaan_id'],
            'itemlab_id' => $row['itemlab_id'],
            'kelas_id' => $d['kelas_id'],
            'is_tagihan' => $row['is_tagihan'],
            'is_periksa' => $row['is_periksa'],
            'tarif_id' => $row['tarif_id'],
            'user_cd' => $this->session->userdata('sess_user_cd'),
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->session->userdata('sess_user_realname'),
          );
          $this->db->insert('lab_pemeriksaan_rinc', $dr);
          update_id('lab_pemeriksaan_rinc', $dr['pemeriksaanrinc_id']);
        }
      }
    }

    //update rincian
    foreach ($old as $row) {
      $check = $this->db->where('itemlab_id', $row['itemlab_id'])->where('pemeriksaan_id', $data['pemeriksaan_id'])->get('lab_pemeriksaan_rinc')->row_array();
      if ($check != null) {
        $do = array(
          'tgl_hasil' => $row['tgl_hasil'],
          'hasil_lab' => $row['hasil_lab'],
          'catatan_lab' => $row['catatan_lab'],
          'user_cd' => $row['user_cd'],
          'created_at' => $row['created_at'],
          'created_by' => $row['created_by'],
          'updated_at' => date('Y-m-d H:i:s'),
          'updated_by' => $this->session->userdata('sess_user_realname'),
        );
        $this->db->where('itemlab_id', $row['itemlab_id'])->where('pemeriksaan_id', $data['pemeriksaan_id'])->update('lab_pemeriksaan_rinc', $do);
      }
    }

    //insert dat_tindakan
    $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->where('lokasi_id', '03.02')->delete('dat_tindakan');
    $reg = $this->db->where('reg_id', $reg_id)->get('reg_pasien')->row_array();

    //insert into rincian
    if (isset($d['checkitem'])) {
      foreach ($d['checkitem'] as $v) {
        // $item = $this->db->query("SELECT * FROM mst_item_lab WHERE itemlab_id LIKE '$v%' ")->result_array();
        // foreach ($item as $row) {
        //   $dr = array(
        //     'pemeriksaanrinc_id' => get_id('lab_pemeriksaan_rinc'),
        //     'pemeriksaan_id' => $data['pemeriksaan_id'],
        //     'itemlab_id' => $row['itemlab_id'],
        //     'kelas_id' => $d['kelas_id'],
        //     'is_tagihan' => $row['is_tagihan'],
        //     'is_periksa' => $row['is_periksa'],
        //     'tarif_id' => $row['tarif_id'],
        //     'user_cd' => $this->session->userdata('sess_user_cd'),
        //     'created_at' => date('Y-m-d H:i:s'),
        //     'created_by' => $this->session->userdata('sess_user_realname'),
        //   );
        //   $this->db->insert('lab_pemeriksaan_rinc', $dr);
        //   update_id('lab_pemeriksaan_rinc', $dr['pemeriksaanrinc_id']);
        // }

        $lab = $this->db->where('itemlab_id', $v)->get('mst_item_lab')->row_array();
        // get mst_tarif_kelas
        $get_tarif_kelas = $this->get_tarif_kelas($lab['tarif_id'], $reg['kelas_id']);
        // insert dat_tindakan
        $data_tindakan['tindakan_id'] = get_id('dat_tindakan');
        $data_tindakan['reg_id'] = $reg['reg_id'];
        $data_tindakan['pemeriksaan_id'] = $data['pemeriksaan_id'];
        $data_tindakan['pasien_id'] = $reg['pasien_id'];
        $data_tindakan['lokasi_id'] = '03.02';
        $data_tindakan['kelas_id'] = $reg['kelas_id'];
        $data_tindakan['tarif_id'] = $lab['tarif_id'];
        $data_tindakan['tarif_nm'] = $get_tarif_kelas['tarif_nm'];
        $data_tindakan['js'] = $get_tarif_kelas['js'];
        $data_tindakan['jp'] = $get_tarif_kelas['jp'];
        $data_tindakan['jb'] = $get_tarif_kelas['jb'];
        $data_tindakan['nom_tarif'] = $get_tarif_kelas['nominal'];
        $data_tindakan['qty'] = 1;
        $data_tindakan['jml_awal'] = $get_tarif_kelas['nominal'];
        $data_tindakan['jml_tagihan'] = $get_tarif_kelas['nominal'];
        $data_tindakan['user_cd'] = $this->session->userdata('sess_user_cd');
        $data_tindakan['tgl_catat'] = date('Y-m-d H:i:s');
        $data_tindakan['created_at'] = date('Y-m-d H:i:s');
        $data_tindakan['created_by'] = $this->session->userdata('sess_user_realname');

        $this->db->insert('dat_tindakan', $data_tindakan);
        update_id('dat_tindakan', $data_tindakan['tindakan_id']);
      }
    }
  }

  public function get_tarif_kelas($tarif_id = '', $kelas_id = '')
  {
    $sql = "SELECT
              a.*,
              b.tarifkelas_id,
              b.js,
              b.jp,
              b.jb,
              b.nominal 
            FROM
              mst_tarif a
              LEFT JOIN mst_tarif_kelas b ON a.tarif_id = b.tarif_id AND b.kelas_id = '$kelas_id'
            WHERE
              a.tarif_id=?";
    $query = $this->db->query($sql, $tarif_id);
    return $query->row_array();
  }

  public function laboratorium_delete($pemeriksaan_id = '', $reg_id = '', $pasien_id = '', $lokasi_id = '')
  {
    trash('lab_pemeriksaan_rinc', array('pemeriksaan_id' => $pemeriksaan_id));

    trash('lab_pemeriksaan', array(
      'pemeriksaan_id' => $pemeriksaan_id,
      'src_reg_id' => $reg_id,
      'pasien_id' => $pasien_id,
      'src_lokasi_id' => $lokasi_id,
    ));
    $this->db
      ->where('pemeriksaan_id', $pemeriksaan_id)
      ->delete('lab_pemeriksaan_rinc');
    $this->db
      ->where('pemeriksaan_id', $pemeriksaan_id)
      ->where('src_reg_id', $reg_id)
      ->where('pasien_id', $pasien_id)
      ->where('src_lokasi_id', $lokasi_id)
      ->delete('lab_pemeriksaan');
    $this->db
      ->where('pemeriksaan_id', $pemeriksaan_id)
      ->delete('dat_tindakan');
  }

  public function laboratorium_get($pemeriksaan_id = '')
  {
    $sql = "SELECT 
            a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
          FROM lab_pemeriksaan a
          LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
          LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
          WHERE pemeriksaan_id=?";
    $query = $this->db->query($sql, array($pemeriksaan_id));
    return $query->row_array();
  }

  public function laboratorium_pemeriksaan_data($reg_id, $pemeriksaan_id)
  {
    $query = $this->db->query(
      "SELECT b.parent_id FROM lab_pemeriksaan_rinc a
      JOIN mst_item_lab b ON a.itemlab_id = b.itemlab_id
      WHERE a.pemeriksaan_id = '$pemeriksaan_id'
      GROUP BY b.parent_id
      ORDER BY b.parent_id"
    )->result_array();
    $res = null;
    if ($query != null) {
      $res = array();
      foreach ($query as $k => $v) {
        $res[$k] = $this->db->query(
          "SELECT * FROM mst_item_lab a
          LEFT JOIN lab_pemeriksaan_rinc b ON a.itemlab_id = b.itemlab_id AND b.pemeriksaan_id = '$pemeriksaan_id'
          WHERE a.itemlab_id='" . $v['parent_id'] . "'"
        )->row_array();
        $res[$k]['rinc'] = $this->db->query(
          "SELECT a.*, 
            b.itemlab_nm, 
            b.tipe_rujukan,
            b.konfirmasi, 
            b.rentang_min, b.rentang_max,
            b.rentang_l_min, b.rentang_l_max, 
            b.rentang_p_min, b.rentang_p_max,
            b.kurang, b.kurang_l, b.kurang_p,
            b.lebih, b.lebih_l, b.lebih_p,
            b.satuan,
            b.informasi 
          FROM lab_pemeriksaan_rinc a
          JOIN mst_item_lab b ON a.itemlab_id = b.itemlab_id
          WHERE a.is_periksa = 1 AND b.parent_id = '" . $v['parent_id'] . "' AND a.pemeriksaan_id = '" . $pemeriksaan_id . "'
          ORDER BY a.itemlab_id ASC"
        )->result_array();
      }
    }
    return $res;
  }

  public function laboratorium_tarif_tindakan_data($reg_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT a.*, b.pegawai_nm 
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            WHERE a.is_deleted=0 AND a.lokasi_id = '03.02'
            AND a.reg_id=? AND a.pemeriksaan_id=?";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function laboratorium_bhp_data($reg_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_bhp a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.02'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function laboratorium_alkes_data($reg_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_alkes a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.02'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  //Radiologi -------------------------------------------------------------------------------------
  public function radiologi_data($reg_id = '', $pasien_id = '', $lokasi_id = '')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM rad_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE src_reg_id=? AND pasien_id=? AND src_lokasi_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pasien_id, $lokasi_id));
    return $query->result_array();
  }

  public function radiologi_history($reg_id = '', $pasien_id = '')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM rad_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE a.reg_id != ? AND a.pasien_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pasien_id));
    return $query->result_array();
  }

  public function radiologi_save($reg_id = null, $id = null)
  {
    $d = $this->input->post();
    $data = $d;

    unset($data['checkitem']);
    unset($data['radiologi_table_length']);
    unset($data['kelas_id']);

    // insert into pemeriksaan rad
    $data['tgl_order'] = to_date($data['tgl_order'], '-', 'full_date');
    if ($id == null) {
      $data['pemeriksaan_id'] = get_id('rad_pemeriksaan');
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('rad_pemeriksaan', $data);
      update_id('rad_pemeriksaan', $data['pemeriksaan_id']);
    } else {
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('pemeriksaan_id', $id)->update('rad_pemeriksaan', $data);
    }

    // $reg = $this->db->where('reg_id', $reg_id)->get('reg_pasien')->row_array();
    // //delete all rincian
    // // $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->delete('rad_pemeriksaan_rinc');
    // $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->where('lokasi_id', '03.03')->delete('dat_tindakan');
    // if (isset($d['checkitem'])) {
    //   $rinc_raw = $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->get('rad_pemeriksaan_rinc')->result_array();
    //   $rinc = array();
    //   foreach ($rinc_raw as $v) {
    //     array_push($rinc, $v['itemrad_id']);
    //   }

    //   foreach ($d['checkitem'] as $v) {
    //     // if checkitem not in $rinc insert to pemeriksaan_rinc
    //     if (!in_array($v, $rinc)) {
    //       $item = $this->db->query("SELECT * FROM mst_item_rad WHERE itemrad_id LIKE '$v%' ")->result_array();
    //       foreach ($item as $row) {
    //         $dr = array(
    //           'pemeriksaanrinc_id' => get_id('rad_pemeriksaan_rinc'),
    //           'pemeriksaan_id' => $data['pemeriksaan_id'],
    //           'itemrad_id' => $row['itemrad_id'],
    //           'kelas_id' => $d['kelas_id'],
    //           'is_tagihan' => $row['is_tagihan'],
    //           'is_periksa' => $row['is_periksa'],
    //           'tarif_id' => $row['tarif_id'],
    //           'user_cd' => $this->session->userdata('sess_user_cd'),
    //           'created_at' => date('Y-m-d H:i:s'),
    //           'created_by' => $this->session->userdata('sess_user_realname'),
    //         );
    //         $this->db->insert('rad_pemeriksaan_rinc', $dr);
    //         update_id('rad_pemeriksaan_rinc', $dr['pemeriksaanrinc_id']);
    //       }
    //     }
    //   }

    //   foreach ($rinc as $v) {
    //     if (!in_array($v, $d['checkitem'])) {
    //       // if rinc not in check item then delete
    //       $this->db->query("DELETE FROM mst_item_rad WHERE itemrad_id LIKE '$v%' ");
    //     }
    //   }
    // }

    $old = $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->get('rad_pemeriksaan_rinc')->result_array();
    $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->delete('rad_pemeriksaan_rinc');
    if (isset($d['checkitem'])) {
      foreach ($d['checkitem'] as $v) {
        $item = $this->db->query("SELECT * FROM mst_item_rad WHERE itemrad_id LIKE '$v%' ")->result_array();
        foreach ($item as $row) {
          $dr = array(
            'pemeriksaanrinc_id' => get_id('rad_pemeriksaan_rinc'),
            'pemeriksaan_id' => $data['pemeriksaan_id'],
            'itemrad_id' => $row['itemrad_id'],
            'kelas_id' => $d['kelas_id'],
            'is_tagihan' => $row['is_tagihan'],
            'is_periksa' => $row['is_periksa'],
            'tarif_id' => $row['tarif_id'],
            'user_cd' => $this->session->userdata('sess_user_cd'),
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->session->userdata('sess_user_realname'),
          );
          $this->db->insert('rad_pemeriksaan_rinc', $dr);
          update_id('rad_pemeriksaan_rinc', $dr['pemeriksaanrinc_id']);
        }
      }
    }

    //update rincian
    foreach ($old as $row) {
      $check = $this->db->where('itemrad_id', $row['itemrad_id'])->where('pemeriksaan_id', $data['pemeriksaan_id'])->get('rad_pemeriksaan_rinc')->row_array();
      if ($check != null) {
        $do = array(
          'tgl_hasil' => $row['tgl_hasil'],
          'hasil_rad' => $row['hasil_rad'],
          'catatan_rad' => $row['catatan_rad'],
          'user_cd' => $row['user_cd'],
          'created_at' => $row['created_at'],
          'created_by' => $row['created_by'],
          'updated_at' => date('Y-m-d H:i:s'),
          'updated_by' => $this->session->userdata('sess_user_realname'),
        );
        $this->db->where('itemrad_id', $row['itemrad_id'])->where('pemeriksaan_id', $data['pemeriksaan_id'])->update('rad_pemeriksaan_rinc', $do);
      }
    }

    //insert dat_tindakan
    $reg = $this->db->where('reg_id', $reg_id)->get('reg_pasien')->row_array();
    $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->where('lokasi_id', '03.03')->delete('dat_tindakan');

    //insert into rincian
    if (isset($d['checkitem'])) {
      foreach ($d['checkitem'] as $v) {
        // $item = $this->db->query("SELECT * FROM mst_item_rad WHERE itemrad_id LIKE '$v%' ")->result_array();
        // foreach ($item as $row) {
        //   $dr = array(
        //     'pemeriksaanrinc_id' => get_id('rad_pemeriksaan_rinc'),
        //     'pemeriksaan_id' => $data['pemeriksaan_id'],
        //     'itemrad_id' => $row['itemrad_id'],
        //     'kelas_id' => $d['kelas_id'],
        //     'is_tagihan' => $row['is_tagihan'],
        //     'is_periksa' => $row['is_periksa'],
        //     'tarif_id' => $row['tarif_id'],
        //     'user_cd' => $this->session->userdata('sess_user_cd'),
        //     'created_at' => date('Y-m-d H:i:s'),
        //     'created_by' => $this->session->userdata('sess_user_realname'),
        //   );
        //   $this->db->insert('rad_pemeriksaan_rinc', $dr);
        //   update_id('rad_pemeriksaan_rinc', $dr['pemeriksaanrinc_id']);
        // }

        //insert dat_tindakan
        $rad = $this->db->where('itemrad_id', $v)->get('mst_item_rad')->row_array();
        // get mst_tarif_kelas
        $get_tarif_kelas = $this->get_tarif_kelas($rad['tarif_id'], $reg['kelas_id']);
        // insert dat_tindakan
        $data_tindakan['tindakan_id'] = get_id('dat_tindakan');
        $data_tindakan['reg_id'] = $reg['reg_id'];
        $data_tindakan['pemeriksaan_id'] = $data['pemeriksaan_id'];
        $data_tindakan['pasien_id'] = $reg['pasien_id'];
        $data_tindakan['lokasi_id'] = '03.03';
        $data_tindakan['kelas_id'] = $reg['kelas_id'];
        $data_tindakan['tarif_id'] = $rad['tarif_id'];
        $data_tindakan['tarif_nm'] = $get_tarif_kelas['tarif_nm'];
        $data_tindakan['js'] = $get_tarif_kelas['js'];
        $data_tindakan['jp'] = $get_tarif_kelas['jp'];
        $data_tindakan['jb'] = $get_tarif_kelas['jb'];
        $data_tindakan['nom_tarif'] = $get_tarif_kelas['nominal'];
        $data_tindakan['qty'] = 1;
        $data_tindakan['jml_awal'] = $get_tarif_kelas['nominal'];
        $data_tindakan['jml_tagihan'] = $get_tarif_kelas['nominal'];
        $data_tindakan['tgl_catat'] = date('Y-m-d H:i:s');
        $data_tindakan['user_cd'] = $this->session->userdata('sess_user_cd');
        $data_tindakan['created_at'] = date('Y-m-d H:i:s');
        $data_tindakan['created_by'] = $this->session->userdata('sess_user_realname');

        $this->db->insert('dat_tindakan', $data_tindakan);
        update_id('dat_tindakan', $data_tindakan['tindakan_id']);
      }
    }
  }

  public function radiologi_delete($pemeriksaan_id = '', $reg_id = '', $pasien_id = '', $lokasi_id = '')
  {
    trash('rad_pemeriksaan_rinc', array('pemeriksaan_id' => $pemeriksaan_id));

    trash('rad_pemeriksaan', array(
      'pemeriksaan_id' => $pemeriksaan_id,
      'src_reg_id' => $reg_id,
      'pasien_id' => $pasien_id,
      'src_lokasi_id' => $lokasi_id,
    ));
    $this->db
      ->where('pemeriksaan_id', $pemeriksaan_id)
      ->delete('rad_pemeriksaan_rinc');
    $this->db
      ->where('pemeriksaan_id', $pemeriksaan_id)
      ->where('src_reg_id', $reg_id)
      ->where('pasien_id', $pasien_id)
      ->where('src_lokasi_id', $lokasi_id)
      ->delete('rad_pemeriksaan');
    $this->db
      ->where('pemeriksaan_id', $pemeriksaan_id)
      ->delete('dat_tindakan');
  }

  public function radiologi_get($pemeriksaan_id = '')
  {
    $sql = "SELECT 
            a.*, b.pegawai_nm as dokter_nm, c.lokasi_nm, d.pegawai_nm AS dokterpj_nm 
          FROM rad_pemeriksaan a
          LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
          LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
          LEFT JOIN mst_pegawai d ON a.dokterpj_id = d.pegawai_id
          WHERE pemeriksaan_id=?";
    $query = $this->db->query($sql, array($pemeriksaan_id));
    return $query->row_array();
  }

  public function radiologi_pemeriksaan_data($reg_id, $pemeriksaan_id)
  {
    $query = $this->db->query(
      "SELECT b.parent_id FROM rad_pemeriksaan_rinc a
      JOIN mst_item_rad b ON a.itemrad_id = b.itemrad_id
      GROUP BY b.parent_id
      ORDER BY b.parent_id"
    )->result_array();
    $res = null;
    if ($query != null) {
      $res = array();
      foreach ($query as $k => $v) {
        $res[$k] = $this->db->query(
          "SELECT * FROM mst_item_rad WHERE itemrad_id='" . $v['parent_id'] . "'"
        )->row_array();
        $res[$k]['rinc'] = $this->db->query(
          "SELECT a.*, b.itemrad_nm FROM rad_pemeriksaan_rinc a
          JOIN mst_item_rad b ON a.itemrad_id = b.itemrad_id
          WHERE a.is_periksa = 1 AND b.parent_id = '" . $v['parent_id'] . "' AND a.pemeriksaan_id = '" . $pemeriksaan_id . "'
          ORDER BY a.itemrad_id ASC"
        )->result_array();
      }
    }
    return $res;
  }

  public function radiologi_tarif_tindakan_data($reg_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT a.*, b.pegawai_nm 
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            WHERE a.is_deleted=0 AND a.lokasi_id = '03.03'
            AND a.reg_id=? AND a.pemeriksaan_id=?";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function radiologi_bhp_data($reg_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_bhp a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.03'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function radiologi_alkes_data($reg_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_alkes a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.03'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  //Bedah Sentral -------------------------------------------------------------------------------------
  public function bedah_sentral_data($reg_id = '', $pasien_id = '', $lokasi_id = '')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM ibs_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE src_reg_id=? AND pasien_id=? AND src_lokasi_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pasien_id, $lokasi_id));
    return $query->result_array();
  }

  public function bedah_sentral_history($reg_id = '', $pasien_id = '')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM ibs_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE a.reg_id != ? AND a.pasien_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pasien_id));
    return $query->result_array();
  }

  public function bedah_sentral_save($reg_id = null, $id = null)
  {
    $d = $this->input->post();
    $data = $d;

    unset($data['checkitem']);
    unset($data['bedah_sentral_table_length']);
    unset($data['kelas_id']);

    $data['tgl_order'] = to_date($data['tgl_order'], '-', 'full_date');
    if ($id == null) {
      $data['pemeriksaan_id'] = get_id('ibs_pemeriksaan');
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('ibs_pemeriksaan', $data);
      update_id('ibs_pemeriksaan', $data['pemeriksaan_id']);
    } else {
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('pemeriksaan_id', $id)->update('ibs_pemeriksaan', $data);
    }

    $reg = $this->db->where('reg_id', $reg_id)->get('reg_pasien')->row_array();
    //delete all rincian
    $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->delete('ibs_pemeriksaan_rinc');
    $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->delete('dat_tindakan');
    //insert into rincian
    if (isset($d['checkitem'])) {
      foreach ($d['checkitem'] as $v) {
        $item = $this->db->query("SELECT * FROM mst_item_operasi WHERE itemoperasi_id LIKE '$v%' ")->result_array();
        foreach ($item as $row) {
          $dr = array(
            'pemeriksaanrinc_id' => get_id('ibs_pemeriksaan_rinc'),
            'pemeriksaan_id' => $data['pemeriksaan_id'],
            'itemoperasi_id' => $row['itemoperasi_id'],
            'kelas_id' => $d['kelas_id'],
            'is_tagihan' => $row['is_tagihan'],
            'is_periksa' => $row['is_periksa'],
            'tarif_id' => $row['tarif_id'],
            'user_cd' => $this->session->userdata('sess_user_cd'),
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->session->userdata('sess_user_realname'),
          );
          $this->db->insert('ibs_pemeriksaan_rinc', $dr);
          update_id('ibs_pemeriksaan_rinc', $dr['pemeriksaanrinc_id']);
        }

        //insert dat_tindakan
        $operasi = $this->db->where('itemoperasi_id', $v)->get('mst_item_operasi')->row_array();
        // get mst_tarif_kelas
        $get_tarif_kelas = $this->get_tarif_kelas($operasi['tarif_id'], $reg['kelas_id']);
        // insert dat_tindakan
        $data_tindakan['tindakan_id'] = get_id('dat_tindakan');
        $data_tindakan['reg_id'] = $reg['reg_id'];
        $data_tindakan['pemeriksaan_id'] = $data['pemeriksaan_id'];
        $data_tindakan['pasien_id'] = $reg['pasien_id'];
        // $data_tindakan['lokasi_id'] = $reg['lokasi_id'];
        $data_tindakan['lokasi_id'] = '03.04'; // Instalasi Bedah Sentral
        $data_tindakan['src_lokasi_id'] = $reg['src_lokasi_id'];
        $data_tindakan['kelas_id'] = $reg['kelas_id'];
        $data_tindakan['tarif_id'] = $operasi['tarif_id'];
        $data_tindakan['tarif_nm'] = $get_tarif_kelas['tarif_nm'];
        $data_tindakan['js'] = $get_tarif_kelas['js'];
        $data_tindakan['jp'] = $get_tarif_kelas['jp'];
        $data_tindakan['jb'] = $get_tarif_kelas['jb'];
        $data_tindakan['nom_tarif'] = $get_tarif_kelas['nominal'];
        $data_tindakan['qty'] = 1;
        $data_tindakan['jml_awal'] = $get_tarif_kelas['nominal'];
        $data_tindakan['jml_tagihan'] = $get_tarif_kelas['nominal'];
        $data_tindakan['user_cd'] = $this->session->userdata('sess_user_cd');
        $data_tindakan['tgl_catat'] = date('Y-m-d H:i:s');
        $data_tindakan['created_at'] = date('Y-m-d H:i:s');
        $data_tindakan['created_by'] = $this->session->userdata('sess_user_realname');

        $this->db->insert('dat_tindakan', $data_tindakan);
        update_id('dat_tindakan', $data_tindakan['tindakan_id']);
      }
    }
  }

  public function bedah_sentral_delete($pemeriksaan_id = '', $reg_id = '', $pasien_id = '', $lokasi_id = '')
  {
    trash('ibs_pemeriksaan_rinc', array('pemeriksaan_id' => $pemeriksaan_id));

    trash('ibs_pemeriksaan', array(
      'pemeriksaan_id' => $pemeriksaan_id,
      'src_reg_id' => $reg_id,
      'pasien_id' => $pasien_id,
      'src_lokasi_id' => $lokasi_id,
    ));

    $this->db
      ->where('pemeriksaan_id', $pemeriksaan_id)
      ->delete('ibs_pemeriksaan_rinc');
    $this->db
      ->where('pemeriksaan_id', $pemeriksaan_id)
      ->where('src_reg_id', $reg_id)
      ->where('pasien_id', $pasien_id)
      ->where('src_lokasi_id', $lokasi_id)
      ->delete('ibs_pemeriksaan');
    $this->db
      ->where('pemeriksaan_id', $pemeriksaan_id)
      ->delete('dat_tindakan');
  }

  public function bedah_sentral_get($pemeriksaan_id = '')
  {
    $sql = "SELECT 
            a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
          FROM ibs_pemeriksaan a
          LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
          LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
          WHERE pemeriksaan_id=?";
    $query = $this->db->query($sql, array($pemeriksaan_id));
    return $query->row_array();
  }

  public function bedah_sentral_tindakan_data($reg_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, 
              b.pegawai_nm AS pegawai_nm_1, 
              c.pegawai_nm AS pegawai_nm_2, 
              d.pegawai_nm AS pegawai_nm_3, 
              e.pegawai_nm AS pegawai_nm_4, 
              f.pegawai_nm AS pegawai_nm_5, 
              g.kelas_nm
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            LEFT JOIN mst_pegawai c ON a.petugas_id_2 = c.pegawai_id
            LEFT JOIN mst_pegawai d ON a.petugas_id_3 = d.pegawai_id
            LEFT JOIN mst_pegawai e ON a.petugas_id_4 = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_5 = f.pegawai_id
            LEFT JOIN mst_kelas g ON a.kelas_id = g.kelas_id 
            WHERE a.is_deleted = 0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id='03.04' 
            ORDER BY a.tindakan_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function bedah_sentral_bhp_data($reg_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_bhp a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.04'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function bedah_sentral_alkes_data($reg_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_alkes a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.04'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  //VK -------------------------------------------------------------------------------------
  public function vk_data($reg_id = '', $pasien_id = '', $lokasi_id = '')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM vk_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE src_reg_id=? AND pasien_id=? AND src_lokasi_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pasien_id, $lokasi_id));
    return $query->result_array();
  }

  public function vk_history($reg_id = '', $pasien_id = '')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM vk_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE a.reg_id != ? AND a.pasien_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pasien_id));
    return $query->result_array();
  }

  public function vk_save($reg_id = null, $id = null)
  {
    $d = $this->input->post();
    $data = $d;

    unset($data['checkitem']);
    unset($data['vk_table_length']);

    $data['tgl_order'] = to_date($data['tgl_order'], '-', 'full_date');
    if ($id == null) {
      $data['pemeriksaan_id'] = get_id('vk_pemeriksaan');
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('vk_pemeriksaan', $data);
      update_id('vk_pemeriksaan', $data['pemeriksaan_id']);
    } else {
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('pemeriksaan_id', $id)->update('vk_pemeriksaan', $data);
    }
  }

  public function vk_delete($pemeriksaan_id = '', $reg_id = '', $pasien_id = '', $lokasi_id = '')
  {
    trash('vk_pemeriksaan', array(
      'pemeriksaan_id' => $pemeriksaan_id,
      'src_reg_id' => $reg_id,
      'pasien_id' => $pasien_id,
      'src_lokasi_id' => $lokasi_id,
    ));
    $this->db
      ->where('pemeriksaan_id', $pemeriksaan_id)
      ->where('src_reg_id', $reg_id)
      ->where('pasien_id', $pasien_id)
      ->where('src_lokasi_id', $lokasi_id)
      ->delete('vk_pemeriksaan');
  }

  public function vk_get($pemeriksaan_id = '')
  {
    $sql = "SELECT 
            a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
          FROM vk_pemeriksaan a
          LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
          LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
          WHERE pemeriksaan_id=?";
    $query = $this->db->query($sql, array($pemeriksaan_id));
    return $query->row_array();
  }

  public function vk_tindakan_data($reg_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, 
              b.pegawai_nm AS pegawai_nm_1, 
              c.pegawai_nm AS pegawai_nm_2, 
              d.pegawai_nm AS pegawai_nm_3, 
              e.pegawai_nm AS pegawai_nm_4, 
              f.pegawai_nm AS pegawai_nm_5, 
              g.kelas_nm
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            LEFT JOIN mst_pegawai c ON a.petugas_id_2 = c.pegawai_id
            LEFT JOIN mst_pegawai d ON a.petugas_id_3 = d.pegawai_id
            LEFT JOIN mst_pegawai e ON a.petugas_id_4 = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_5 = f.pegawai_id
            LEFT JOIN mst_kelas g ON a.kelas_id = g.kelas_id 
            WHERE a.is_deleted = 0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.05' 
            ORDER BY a.tindakan_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function vk_bhp_data($reg_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_bhp a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.05'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function vk_alkes_data($reg_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_alkes a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.05'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  //ICU -------------------------------------------------------------------------------------
  public function icu_data($reg_id = '', $pasien_id = '', $lokasi_id = '')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM icu_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE src_reg_id=? AND pasien_id=? AND src_lokasi_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pasien_id, $lokasi_id));
    return $query->result_array();
  }

  public function icu_history($reg_id = '', $pasien_id = '')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM icu_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE a.reg_id != ? AND a.pasien_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pasien_id));
    return $query->result_array();
  }

  public function icu_save($reg_id = null, $id = null)
  {
    $d = $this->input->post();
    $data = $d;

    unset($data['checkitem']);
    unset($data['icu_table_length']);

    $data['tgl_order'] = to_date($data['tgl_order'], '-', 'full_date');
    if ($id == null) {
      $data['pemeriksaan_id'] = get_id('icu_pemeriksaan');
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('icu_pemeriksaan', $data);
      update_id('icu_pemeriksaan', $data['pemeriksaan_id']);
    } else {
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('pemeriksaan_id', $id)->update('icu_pemeriksaan', $data);
    }
  }

  public function icu_delete($pemeriksaan_id = '', $reg_id = '', $pasien_id = '', $lokasi_id = '')
  {
    trash('icu_pemeriksaan', array(
      'pemeriksaan_id' => $pemeriksaan_id,
      'src_reg_id' => $reg_id,
      'pasien_id' => $pasien_id,
      'src_lokasi_id' => $lokasi_id,
    ));
    $this->db
      ->where('pemeriksaan_id', $pemeriksaan_id)
      ->where('src_reg_id', $reg_id)
      ->where('pasien_id', $pasien_id)
      ->where('src_lokasi_id', $lokasi_id)
      ->delete('icu_pemeriksaan');
  }

  public function icu_get($pemeriksaan_id = '')
  {
    $sql = "SELECT 
            a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
          FROM icu_pemeriksaan a
          LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
          LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
          WHERE pemeriksaan_id=?";
    $query = $this->db->query($sql, array($pemeriksaan_id));
    return $query->row_array();
  }

  public function icu_tindakan_data($reg_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, 
              b.pegawai_nm AS pegawai_nm_1, 
              c.pegawai_nm AS pegawai_nm_2, 
              d.pegawai_nm AS pegawai_nm_3, 
              e.pegawai_nm AS pegawai_nm_4, 
              f.pegawai_nm AS pegawai_nm_5, 
              g.kelas_nm
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            LEFT JOIN mst_pegawai c ON a.petugas_id_2 = c.pegawai_id
            LEFT JOIN mst_pegawai d ON a.petugas_id_3 = d.pegawai_id
            LEFT JOIN mst_pegawai e ON a.petugas_id_4 = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_5 = f.pegawai_id
            LEFT JOIN mst_kelas g ON a.kelas_id = g.kelas_id 
            WHERE a.is_deleted = 0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.06'
            ORDER BY a.tindakan_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function icu_bhp_data($reg_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_bhp a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.06'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function icu_alkes_data($reg_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_alkes a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.06'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  //Hemodialisa -------------------------------------------------------------------------------------
  public function hemodialisa_data($reg_id = '', $pasien_id = '', $lokasi_id = '')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM hd_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE src_reg_id=? AND pasien_id=? AND src_lokasi_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pasien_id, $lokasi_id));
    return $query->result_array();
  }

  public function hemodialisa_history($reg_id = '', $pasien_id = '')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM hd_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE a.reg_id != ? AND a.pasien_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pasien_id));
    return $query->result_array();
  }

  public function hemodialisa_save($reg_id = null, $id = null)
  {
    $d = $this->input->post();
    $data = $d;

    unset($data['checkitem']);
    unset($data['hemodialisa_table_length']);

    $data['tgl_order'] = to_date($data['tgl_order'], '-', 'full_date');
    if ($id == null) {
      $data['pemeriksaan_id'] = get_id('hd_pemeriksaan');
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('hd_pemeriksaan', $data);
      update_id('hd_pemeriksaan', $data['pemeriksaan_id']);
    } else {
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('pemeriksaan_id', $id)->update('hd_pemeriksaan', $data);
    }
  }

  public function hemodialisa_delete($pemeriksaan_id = '', $reg_id = '', $pasien_id = '', $lokasi_id = '')
  {
    trash('hd_pemeriksaan', array(
      'pemeriksaan_id' => $pemeriksaan_id,
      'src_reg_id' => $reg_id,
      'pasien_id' => $pasien_id,
      'src_lokasi_id' => $lokasi_id,
    ));
    $this->db
      ->where('pemeriksaan_id', $pemeriksaan_id)
      ->where('src_reg_id', $reg_id)
      ->where('pasien_id', $pasien_id)
      ->where('src_lokasi_id', $lokasi_id)
      ->delete('hd_pemeriksaan');
  }

  public function hemodialisa_get($pemeriksaan_id = '')
  {
    $sql = "SELECT 
            a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
          FROM hd_pemeriksaan a
          LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
          LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
          WHERE pemeriksaan_id=?";
    $query = $this->db->query($sql, array($pemeriksaan_id));
    return $query->row_array();
  }

  public function hemodialisa_tindakan_data($reg_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, 
              b.pegawai_nm AS pegawai_nm_1, 
              c.pegawai_nm AS pegawai_nm_2, 
              d.pegawai_nm AS pegawai_nm_3, 
              e.pegawai_nm AS pegawai_nm_4, 
              f.pegawai_nm AS pegawai_nm_5, 
              g.kelas_nm
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            LEFT JOIN mst_pegawai c ON a.petugas_id_2 = c.pegawai_id
            LEFT JOIN mst_pegawai d ON a.petugas_id_3 = d.pegawai_id
            LEFT JOIN mst_pegawai e ON a.petugas_id_4 = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_5 = f.pegawai_id
            LEFT JOIN mst_kelas g ON a.kelas_id = g.kelas_id 
            WHERE a.is_deleted = 0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.07'
            ORDER BY a.tindakan_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function hemodialisa_bhp_data($reg_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_bhp a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.07'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function hemodialisa_alkes_data($reg_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_alkes a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.07'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  // Tindak Lanjut
  public function save_rs_rujukan()
  {
    $data = html_escape($this->input->post());
    $data['rsrujukan_id'] = $data['rsrujukan_kode'];
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['created_by'] = $this->session->userdata('sess_user_realname');
    unset($data['rsrujukan_kode']);
    $this->db->insert('mst_rs_rujukan', $data);
  }

  public function save_tindak_lanjut()
  {
    $data = $this->input->post();
    if ($data['tindaklanjut_cd'] == '01') {
      if ($data['lokasi_jenisreg_1'] != '') {
        $data_tindak_lanjut['lokasi_id'] = $data['lokasi_jenisreg_1'];
        $data_tindak_lanjut['subtindaklanjut_cd'] = $data['subtindaklanjut_cd'];
      } else if ($data['lokasi_jenisreg_2'] != '') {
        $data_tindak_lanjut['lokasi_id'] = $data['lokasi_jenisreg_2'];
        $data_tindak_lanjut['subtindaklanjut_cd'] = '';
      }
    } elseif ($data['tindaklanjut_cd'] == '02') {
      if ($data['lokasi_jenisreg_1'] != '') {
        $data_tindak_lanjut['lokasi_id'] = $data['lokasi_jenisreg_1'];
        $data_tindak_lanjut['subtindaklanjut_cd'] = $data['subtindaklanjut_cd'];
      } else if ($data['lokasi_jenisreg_2'] != '') {
        $data_tindak_lanjut['lokasi_id'] = $data['lokasi_jenisreg_2'];
        $data_tindak_lanjut['subtindaklanjut_cd'] = '';
      }
    } elseif ($data['tindaklanjut_cd'] == '03') {
      $data_tindak_lanjut['lokasi_id'] = '';
      $data_tindak_lanjut['rsrujukan_id'] = $data['rsrujukan_id'];
    } else {
      $data_tindak_lanjut['lokasi_id'] = '';
      $data_tindak_lanjut['subtindaklanjut_cd'] = '';
      $data_tindak_lanjut['rsrujukan_id'] = '';
    }
    $data_tindak_lanjut['reg_id'] = $data['reg_id'];
    $data_tindak_lanjut['pasien_id'] = $data['pasien_id'];
    $data_tindak_lanjut['tindaklanjut_cd'] = $data['tindaklanjut_cd'];
    $data_tindak_lanjut['catatan_tindaklanjut'] = $data['catatan_tindaklanjut'];
    $data_tindak_lanjut['user_cd'] = $this->session->userdata('sess_user_cd');
    $data_tindak_lanjut['tgl_tindaklanjut'] = date('Y-m-d H:i:s');
    // unset data
    unset($data['lokasi_jenisreg_1'], $data['lokasi_jenisreg_2']);
    $cek = $this->db->where('reg_id', $data['reg_id'])->where('pasien_id', $data['pasien_id'])->get('reg_pasien_tindaklanjut')->row_array();
    if ($cek == null) {
      $data_tindak_lanjut['created_at'] = date('Y-m-d H:i:s');
      $data_tindak_lanjut['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('reg_pasien_tindaklanjut', $data_tindak_lanjut);
    } else {
      $data_tindak_lanjut['updated_at'] = date('Y-m-d H:i:s');
      $data_tindak_lanjut['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('reg_id', $data['reg_id'])->where('pasien_id', $data['pasien_id'])->update('reg_pasien_tindaklanjut', $data_tindak_lanjut);
    }
  }

  public function tindak_lanjut_data($pasien_id = '', $reg_id = '')
  {
    $sql = "SELECT 
              a.* 
            FROM reg_pasien_tindaklanjut a
            WHERE a.is_deleted=0 AND a.pasien_id=? AND a.reg_id=?";
    $query = $this->db->query($sql, array($pasien_id, $reg_id));
    return $query->row_array();
  }

  public function save_status_pulang()
  {
    $data = $this->input->post();

    // update reg_pasien
    if ($data['pulang_st'] == '1') {
      $data_reg_pasien['periksa_st'] = 2;
      $data_reg_pasien['pulang_st'] = 1;
      $data_reg_pasien['tgl_pulang'] = to_date($data['tgl_pulang'], '-', 'full_date');
    }
    $data_reg_pasien['tindaklanjut_cd'] = $data['tindaklanjut_cd'];
    $data_reg_pasien['updated_at'] = date('Y-m-d H:i:s');
    $data_reg_pasien['updated_by'] = $this->session->userdata('sess_user_realname');
    $this->db->where('reg_id', $data['reg_id'])->where('pasien_id', $data['pasien_id'])->update('reg_pasien', $data_reg_pasien);

    if ($data['pulang_st'] == '1') {
      $data_status_pulang['reg_id'] = $data['reg_id'];
      $data_status_pulang['pasien_id'] = $data['pasien_id'];
      $data_status_pulang['tgl_pulang'] = to_date($data['tgl_pulang'], '-', 'full_date');
      $data_status_pulang['tgl_kontrol'] = to_date($data['tgl_kontrol']);
      $data_status_pulang['keadaan_cd'] = $data['keadaan_cd'];
      $data_status_pulang['carapulang_cd'] = $data['carapulang_cd'];
      $data_status_pulang['catatan_dokter'] = $data['catatan_dokter'];
      $data_status_pulang['catatan_keperawatan'] = $data['catatan_keperawatan'];
      $data_status_pulang['catatan_farmasi'] = $data['catatan_farmasi'];
      $data_status_pulang['user_cd'] = $this->session->userdata('sess_user_cd');
      //
      $cek = $this->db->where('reg_id', $data['reg_id'])->where('pasien_id', $data['pasien_id'])->get('reg_pasien_pulang')->row_array();
      if ($cek == null) {
        $data_status_pulang['created_at'] = date('Y-m-d H:i:s');
        $data_status_pulang['created_by'] = $this->session->userdata('sess_user_realname');
        $this->db->insert('reg_pasien_pulang', $data_status_pulang);
      } else {
        $data_status_pulang['updated_at'] = date('Y-m-d H:i:s');
        $data_status_pulang['updated_by'] = $this->session->userdata('sess_user_realname');
        $this->db->where('reg_id', $data['reg_id'])->where('pasien_id', $data['pasien_id'])->update('reg_pasien_pulang', $data_status_pulang);
      }
    }
  }

  public function status_pulang_data($pasien_id = '', $reg_id = '')
  {
    $sql = "SELECT 
              a.* 
            FROM reg_pasien_pulang a
            WHERE a.is_deleted=0 AND a.pasien_id=? AND a.reg_id=?";
    $query = $this->db->query($sql, array($pasien_id, $reg_id));
    $row = $query->row_array();
    $row['tgl_pulang'] = to_date($row['tgl_pulang'], '-', 'full_date');
    $row['tgl_kontrol'] = to_date($row['tgl_kontrol']);
    return $row;
  }

  public function reg_pasien_data_pulang_st($pasien_id = '', $reg_id = '')
  {
    $sql = "SELECT 
              a.pulang_st 
            FROM reg_pasien a
            WHERE a.is_deleted=0 AND a.pasien_id=? AND a.reg_id=?";
    $query = $this->db->query($sql, array($pasien_id, $reg_id));
    return $query->row_array();
  }

  // History
  public function pasien_history($pasien_id = '', $reg_id = '')
  {
    $sql = "SELECT
              a.*,
              b.lokasi_nm,
              c.jenispasien_nm,
              d.pegawai_nm AS dokter_pj
            FROM
              reg_pasien a
              LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id 
              LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
              LEFT JOIN mst_pegawai d ON a.dokter_id = d.pegawai_id
            WHERE
              a.pasien_id=? AND a.reg_id != ?
            ORDER BY
              a.tgl_registrasi DESC";
    $query = $this->db->query($sql, array($pasien_id, $reg_id));
    return $query->result_array();
  }

  public function history_detail_pemeriksaan_fisik($reg_id = '')
  {
    $sql = "SELECT
              a.*,
              b.lokasi_nm,
              c.pegawai_nm AS petugas_nm 
            FROM
              dat_anamnesis a
              LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id 
              LEFT JOIN mst_pegawai c ON a.petugas_id = c.pegawai_id
            WHERE
              a.reg_id = ?  
            ORDER BY
              a.tgl_catat DESC";
    $query = $this->db->query($sql, array($reg_id));
    return $query->row_array();
  }

  public function history_detail_catatan_medis($reg_id = '')
  {
    $sql = "SELECT
              a.*,
              b.lokasi_nm,
              c.pegawai_nm AS petugas_nm 
            FROM
              dat_catatanmedis a
              LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id 
              LEFT JOIN mst_pegawai c ON a.petugas_id = c.pegawai_id
            WHERE
              a.reg_id = ? 
            ORDER BY
              a.tgl_catat DESC";
    $query = $this->db->query($sql, array($reg_id));
    return $query->result_array();
  }

  public function history_detail_diagnosis($reg_id = '')
  {
    $sql = "SELECT 
              a.*, b.penyakit_nm, c.lokasi_nm 
            FROM dat_diagnosis a
            LEFT JOIN mst_penyakit b ON a.penyakit_id=b.penyakit_id 
            LEFt JOIN mst_lokasi c ON a.lokasi_id=c.lokasi_id
            WHERE a.reg_id = ? 
            ORDER BY a.tgl_catat DESC";
    $query = $this->db->query($sql, array($reg_id));
    return $query->result_array();
  }

  public function history_detail_tindakan($reg_id = '')
  {
    $sql = "SELECT 
              a.*, 
              b.pegawai_nm AS pegawai_nm_1, 
              c.pegawai_nm AS pegawai_nm_2, 
              d.pegawai_nm AS pegawai_nm_3, 
              e.pegawai_nm AS pegawai_nm_4, 
              f.pegawai_nm AS pegawai_nm_5, 
              g.kelas_nm,
              h.lokasi_nm 
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            LEFT JOIN mst_pegawai c ON a.petugas_id_2 = c.pegawai_id
            LEFT JOIN mst_pegawai d ON a.petugas_id_3 = d.pegawai_id
            LEFT JOIN mst_pegawai e ON a.petugas_id_4 = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_5 = f.pegawai_id
            LEFT JOIN mst_kelas g ON a.kelas_id = g.kelas_id 
            LEFT JOIN mst_lokasi h ON a.lokasi_id = h.lokasi_id 
            WHERE a.reg_id = ? 
            ORDER BY a.tgl_catat DESC";
    $query = $this->db->query($sql, array($reg_id));
    return $query->result_array();
  }

  public function history_detail_pemberian_obat($reg_id = '')
  {
    $sql = "SELECT 
              a.*, b.lokasi_nm 
            FROM dat_resep_group a
            LEFT JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id
            WHERE a.is_deleted=0 AND a.reg_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id));
    $result = $query->result_array();
    // 
    foreach ($result as $key => $val) {
      $result[$key]['list_dat_resep'] = $this->history_detail_list_dat_resep_history($val['resepgroup_id'], $reg_id);
    }
    return $result;
  }

  public function history_detail_resep($reg_id)
  {
    $resep = $this->db->query(
      "SELECT 
        a.*, b.lokasi_nm 
      FROM dat_resep a
      LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
      WHERE a.is_deleted = 0 AND a.reg_id = '$reg_id'
      ORDER BY a.reg_id 
      "
    )->result_array();
    foreach ($resep as $key => $val) {
      $resep[$key]['rinc'] = $this->db->query(
        "SELECT 
          a.*,
          b.parameter_val AS rute_nm,
          c.parameter_val AS satuan_nm,
          d.parameter_val AS waktu_nm
        FROM dat_resep_rinc a 
        INNER JOIN mst_parameter b ON b.parameter_field = 'rute_cd' AND a.rute_cd = b.parameter_cd
        INNER JOIN mst_parameter c ON c.parameter_field = 'satuan_cd' AND a.satuan_cd = c.parameter_cd
        INNER JOIN mst_parameter d ON d.parameter_field = 'waktu_cd' AND a.waktu_cd = d.parameter_cd
        WHERE a.is_racik = 0 AND a.resep_id = '" . $val['resep_id'] . "'"
      )->result_array();
    }
    return $resep;
  }

  public function history_detail_resep_racik($reg_id)
  {
    $resep = $this->db->query(
      "SELECT 
        a.*, b.lokasi_nm 
      FROM dat_resep a
      LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
      WHERE a.is_deleted = 0 AND a.reg_id = '$reg_id'
      ORDER BY a.reg_id 
      "
    )->result_array();
    foreach ($resep as $key => $val) {
      $resep[$key]['rinc'] = $this->db->query(
        "SELECT 
          a.*,
          b.parameter_val AS rute_nm,
          c.parameter_val AS satuan_nm,
          d.parameter_val AS waktu_nm
        FROM dat_resep_rinc a 
        INNER JOIN mst_parameter b ON b.parameter_field = 'rute_cd' AND a.rute_cd = b.parameter_cd
        INNER JOIN mst_parameter c ON c.parameter_field = 'satuan_cd' AND a.satuan_cd = c.parameter_cd
        INNER JOIN mst_parameter d ON d.parameter_field = 'waktu_cd' AND a.waktu_cd = d.parameter_cd
        WHERE a.is_racik = 1 AND a.resep_id = '" . $val['resep_id'] . "'"
      )->result_array();
      foreach ($resep[$key]['rinc'] as $k => $v) {
        $resep[$key]['rinc'][$k]['komposisi'] = $this->db->query(
          "SELECT * FROM dat_resep_racik"
        )->result_array();
      }
    }
    return $resep;
  }

  public function history_detail_list_dat_resep_history($resepgroup_id = '', $reg_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_resep a
            LEFT JOIN mst_obat b ON a.obat_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.resepgroup_id=? AND a.reg_id=?
            ORDER BY a.reg_id ASC";
    $query = $this->db->query($sql, array($resepgroup_id, $reg_id));
    return $query->result_array();
  }

  public function history_detail_bhp($reg_id = '')
  {
    $sql = "SELECT a.*, b.lokasi_nm 
            FROM dat_bhp a
            LEFT JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id 
            WHERE a.reg_id=? 
            ORDER BY a.tgl_catat DESC";
    $query = $this->db->query($sql, array($reg_id));
    return $query->result_array();
  }

  public function history_detail_alkes($reg_id = '')
  {
    $sql = "SELECT a.*, b.lokasi_nm 
            FROM dat_alkes a
            LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id 
            WHERE a.reg_id=? 
            ORDER BY a.tgl_catat DESC";
    $query = $this->db->query($sql, array($reg_id));
    return $query->result_array();
  }

  public function history_detail_laboratorium($reg_id = '')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM lab_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE a.reg_id=?
            ORDER BY a.tgl_order ASC";
    $query = $this->db->query($sql, array($reg_id));
    return $query->result_array();
  }

  public function history_detail_radiologi($reg_id = '')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM rad_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE a.reg_id=?
            ORDER BY a.tgl_order ASC";
    $query = $this->db->query($sql, array($reg_id));
    return $query->result_array();
  }

  public function history_detail_bedah_sentral($reg_id = '')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM ibs_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE a.reg_id=?
            ORDER BY a.tgl_order ASC";
    $query = $this->db->query($sql, array($reg_id));
    return $query->result_array();
  }

  public function history_detail_vk($reg_id = '')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM vk_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE a.reg_id = ?
            ORDER BY a.tgl_order ASC";
    $query = $this->db->query($sql, array($reg_id));
    return $query->result_array();
  }

  public function history_detail_icu($reg_id = '')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM icu_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE a.reg_id=?
            ORDER BY a.tgl_order ASC";
    $query = $this->db->query($sql, array($reg_id));
    return $query->result_array();
  }

  public function history_detail_hemodialisa($reg_id = '')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM hd_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE a.reg_id=?
            ORDER BY a.tgl_order ASC";
    $query = $this->db->query($sql, array($reg_id));
    return $query->result_array();
  }

  // Resep Non Racik
  public function resep_save()
  {
    $data = $this->input->post();
    //cek jika resep sudah ada atau belum
    if ($data['resep_id'] == '') {
      $cek = $this->db->query(
        "SELECT * FROM dat_resep 
        WHERE reg_id = '" . $data['reg_id'] . "' 
        AND is_racik = 0 AND src_resep_st = 0 AND DATE(tgl_catat) = DATE(NOW())"
      )->row_array();

      $resep_id = '';

      if ($cek == null) {
        //create baru
        $resep_id = get_id('dat_resep');
        $dr = array(
          'resep_id' => $resep_id,
          'reg_id' => $data['reg_id'],
          'is_racik' => 0,
          'pasien_id' => $data['pasien_id'],
          'lokasi_id' => $data['lokasi_id'],
          'dokter_id' => $data['dokter_id'],
          'tgl_catat' => date('Y-m-d H:i:s'),
          'created_at' => date('Y-m-d H:i:s'),
          'created_by' => $this->session->userdata('sess_user_realname'),
          'user_cd' => $this->session->userdata('sess_user_cd')
        );
        $this->db->insert('dat_resep', $dr);
        update_id('dat_resep', $resep_id);
      } else {
        $resep_id = $cek['resep_id'];
      }
    } else {
      $resep_id = $data['resep_id'];
    }

    // save to dat_resep_rinc
    if ($data['rincian_id'] == '') {
      $detail = array(
        'rincian_id' => get_id('dat_resep_rinc'),
        'resep_id' => $resep_id,
        'reg_id' => $data['reg_id'],
        'lokasi_id' => $data['lokasi_id'],
        'pasien_id' => $data['pasien_id'],
        'obat_nm' => $data['obat_nm'],
        'obat_alias' => $data['obat_alias'],
        'rute_cd' => $data['rute_cd'],
        'qty' => $data['qty'],
        'aturan_jml' => $data['aturan_jml'],
        'satuan_cd' => $data['satuan_cd'],
        'aturan_waktu' => $data['aturan_waktu'],
        'waktu_cd' => $data['waktu_cd'],
        'jadwal' => $data['jadwal'],
        'aturan_tambahan' => $data['aturan_tambahan'],
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $this->session->userdata('sess_user_realname'),
        'tgl_catat' => date('Y-m-d H:i:s'),
        'user_cd' => $this->session->userdata('sess_user_cd')
      );
      $this->db->insert('dat_resep_rinc', $detail);
      update_id('dat_resep_rinc', $detail['rincian_id']);
    } else {
      $detail = array(
        'rincian_id' => $data['rincian_id'],
        'resep_id' => $data['resep_id'],
        'reg_id' => $data['reg_id'],
        'lokasi_id' => $data['lokasi_id'],
        'pasien_id' => $data['pasien_id'],
        'obat_nm' => $data['obat_nm'],
        'obat_alias' => $data['obat_alias'],
        'rute_cd' => $data['rute_cd'],
        'qty' => $data['qty'],
        'aturan_jml' => $data['aturan_jml'],
        'satuan_cd' => $data['satuan_cd'],
        'aturan_waktu' => $data['aturan_waktu'],
        'waktu_cd' => $data['waktu_cd'],
        'jadwal' => $data['jadwal'],
        'aturan_tambahan' => $data['aturan_tambahan'],
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $this->session->userdata('sess_user_realname'),
        'tgl_catat' => date('Y-m-d H:i:s'),
        'user_cd' => $this->session->userdata('sess_user_cd')
      );
      $this->db->where('rincian_id', $data['rincian_id'])->update('dat_resep_rinc', $detail);
    }
  }

  public function resep_data($reg_id = '', $pasien_id = '')
  {
    $resep = $this->db
      ->where('reg_id', $reg_id)
      ->where('src_resep_st', 0)
      ->where('is_racik', 0)
      ->where('pasien_id', $pasien_id)
      ->order_by('tgl_catat', 'desc')
      ->get('dat_resep')
      ->result_array();
    if ($resep != null) {
      foreach ($resep as $k => $v) {
        $resep[$k]['rinc'] = $this->db->query(
          "SELECT 
            a.*,
            b.parameter_val AS rute_nm,
            c.parameter_val AS satuan_nm,
            d.parameter_val AS waktu_nm
          FROM dat_resep_rinc a 
          INNER JOIN mst_parameter b ON b.parameter_field = 'rute_cd' AND a.rute_cd = b.parameter_cd
          INNER JOIN mst_parameter c ON c.parameter_field = 'satuan_cd' AND a.satuan_cd = c.parameter_cd
          INNER JOIN mst_parameter d ON d.parameter_field = 'waktu_cd' AND a.waktu_cd = d.parameter_cd
          WHERE a.reg_id = '$reg_id' AND a.pasien_id = '$pasien_id' 
          AND a.is_racik = 0 AND a.resep_id = '" . $v['resep_id'] . "'"
        )->result_array();
      }
    }

    return $resep;
  }

  public function resep_get($resep_id = '', $rincian_id = '', $reg_id = '')
  {
    $sql = "SELECT *
            FROM dat_resep_rinc a
            WHERE a.resep_id=? AND rincian_id = ? AND a.reg_id=?";
    $query = $this->db->query($sql, array($resep_id, $rincian_id, $reg_id));
    return $query->row_array();
  }

  public function resep_delete($resep_id = '', $rincian_id = '', $reg_id = '')
  {
    $this->db->where('rincian_id', $rincian_id)->delete('dat_resep_rinc');
  }

  public function resep_delete_all($resep_id)
  {
    $this->db->where('resep_id', $resep_id)->delete('dat_resep');
    $this->db->where('resep_id', $resep_id)->delete('dat_resep_rinc');
  }

  // public function racik_save()
  // {
  //   $data = $this->input->post();
  //   var_dump($data);
  // }

  // Resep Racik
  public function racik_save()
  {
    $data = $this->input->post();
    //cek jika resep sudah ada atau belum

    if ($data['resep_id'] == '') {

      $cek = $this->db->query(
        "SELECT * FROM dat_resep 
        WHERE reg_id = '" . $data['reg_id'] . "' 
        AND is_racik = 1 AND src_resep_st = 0 AND DATE(tgl_catat) = DATE(NOW())"
      )->row_array();

      $resep_id = '';

      if ($cek == null) {
        //create baru
        $resep_id = get_id('dat_resep');
        $dr = array(
          'resep_id' => $resep_id,
          'reg_id' => $data['reg_id'],
          'is_racik' => 1,
          'pasien_id' => $data['pasien_id'],
          'lokasi_id' => $data['lokasi_id'],
          'dokter_id' => $data['dokter_id'],
          'tgl_catat' => date('Y-m-d H:i:s'),
          'created_at' => date('Y-m-d H:i:s'),
          'created_by' => $this->session->userdata('sess_user_realname'),
          'user_cd' => $this->session->userdata('sess_user_cd')
        );
        $this->db->insert('dat_resep', $dr);
        update_id('dat_resep', $resep_id);
      } else {
        $resep_id = $cek['resep_id'];
      }
    } else {
      $resep_id = $data['resep_id'];
    }

    // save to dat_resep_rinc -> obat racikan
    if ($data['rincian_id'] == '') {
      $detail = array(
        'rincian_id' => get_id('dat_resep_rinc'),
        'resep_id' => $resep_id,
        'reg_id' => $data['reg_id'],
        'lokasi_id' => $data['lokasi_id'],
        'is_racik' => 1,
        'pasien_id' => $data['pasien_id'],
        'obat_nm' => $data['obat_nm'],
        'sediaan_cd' => $data['sediaan_cd'],
        'rute_cd' => $data['rute_cd'],
        'qty' => $data['qty'],
        'aturan_jml' => $data['aturan_jml'],
        'satuan_cd' => $data['satuan_cd'],
        'aturan_waktu' => $data['aturan_waktu'],
        'waktu_cd' => $data['waktu_cd'],
        'jadwal' => $data['jadwal'],
        'aturan_tambahan' => $data['aturan_tambahan'],
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $this->session->userdata('sess_user_realname'),
        'tgl_catat' => date('Y-m-d H:i:s'),
        'user_cd' => $this->session->userdata('sess_user_cd')
      );
      $this->db->insert('dat_resep_rinc', $detail);
      update_id('dat_resep_rinc', $detail['rincian_id']);

      //insert to data racikan
      foreach (@$data['komposisi'] as $key => $val) {
        $racik = array(
          'komposisi_id' => get_id('dat_resep_racik'),
          'rincian_id' => $detail['rincian_id'],
          'komposisi' => $data['komposisi'][$key],
          'komposisi_alias' => $data['komposisi_alias'][$key],
          'komposisi_qty' => $data['komposisi_qty'][$key],
        );
        $this->db->insert('dat_resep_racik', $racik);
        update_id('dat_resep_racik', $racik['komposisi_id']);
      }
    } else {
      $detail = array(
        'rincian_id' => $data['rincian_id'],
        'resep_id' => $resep_id,
        'reg_id' => $data['reg_id'],
        'lokasi_id' => $data['lokasi_id'],
        'is_racik' => 1,
        'pasien_id' => $data['pasien_id'],
        'obat_nm' => $data['obat_nm'],
        'sediaan_cd' => $data['sediaan_cd'],
        'rute_cd' => $data['rute_cd'],
        'qty' => $data['qty'],
        'aturan_jml' => $data['aturan_jml'],
        'satuan_cd' => $data['satuan_cd'],
        'aturan_waktu' => $data['aturan_waktu'],
        'waktu_cd' => $data['waktu_cd'],
        'jadwal' => $data['jadwal'],
        'aturan_tambahan' => $data['aturan_tambahan'],
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $this->session->userdata('sess_user_realname'),
        'tgl_catat' => date('Y-m-d H:i:s'),
        'user_cd' => $this->session->userdata('sess_user_cd')
      );
      $this->db->where('rincian_id', $data['rincian_id'])->update('dat_resep_rinc', $detail);
      $this->db->where('rincian_id', $data['rincian_id'])->delete('dat_resep_racik');
      //insert to data racikan
      foreach ($data['komposisi'] as $key => $val) {
        $racik = array(
          'komposisi_id' => get_id('dat_resep_racik'),
          'rincian_id' => $detail['rincian_id'],
          'komposisi' => $data['komposisi'][$key],
          'komposisi_alias' => $data['komposisi_alias'][$key],
          'komposisi_qty' => $data['komposisi_qty'][$key],
        );
        $this->db->insert('dat_resep_racik', $racik);
        update_id('dat_resep_racik', $racik['komposisi_id']);
      }
    }
  }

  public function racik_data($reg_id = '', $pasien_id = '')
  {
    $resep = $this->db
      ->where('reg_id', $reg_id)
      ->where('src_resep_st', 0)
      ->where('is_racik', 1)
      ->where('pasien_id', $pasien_id)
      ->order_by('tgl_catat', 'desc')
      ->get('dat_resep')->result_array();
    if ($resep != null) {
      foreach ($resep as $k => $v) {
        $resep[$k]['rinc'] = $this->db->query(
          "SELECT 
            a.*,
            b.parameter_val AS rute_nm,
            c.parameter_val AS satuan_nm,
            d.parameter_val AS waktu_nm,
            e.parameter_val AS sediaan_nm
          FROM dat_resep_rinc a 
          INNER JOIN mst_parameter b ON b.parameter_field = 'rute_cd' AND a.rute_cd = b.parameter_cd
          INNER JOIN mst_parameter c ON c.parameter_field = 'satuan_cd' AND a.satuan_cd = c.parameter_cd
          INNER JOIN mst_parameter d ON d.parameter_field = 'waktu_cd' AND a.waktu_cd = d.parameter_cd
          INNER JOIN mst_parameter e ON e.parameter_field = 'sediaan_cd' AND a.sediaan_cd = e.parameter_cd
          WHERE a.reg_id = '$reg_id' AND a.pasien_id = '$pasien_id' 
          AND is_racik = 1 AND a.resep_id = '" . $v['resep_id'] . "'"
        )->result_array();
        foreach ($resep[$k]['rinc'] as $key => $value) {
          $resep[$k]['rinc'][$key]['komposisi'] = $this->db->where('rincian_id', $resep[$k]['rinc'][$key]['rincian_id'])->get('dat_resep_racik')->result_array();
        }
      }
    }

    return $resep;
  }

  public function racik_get($resep_id = '', $rincian_id = '', $reg_id = '')
  {
    $sql = "SELECT *
            FROM dat_resep_rinc a
            WHERE a.resep_id=? AND rincian_id = ? AND a.reg_id=?";
    $query = $this->db->query($sql, array($resep_id, $rincian_id, $reg_id));

    $data = $query->row_array();

    if ($data != null) {
      $data['komposisi'] = $this->db->where('rincian_id', $data['rincian_id'])->get('dat_resep_racik')->result_array();
    }

    return $data;
  }

  public function racik_delete($resep_id = '', $rincian_id = '', $reg_id = '')
  {
    $this->db->where('rincian_id', $rincian_id)->delete('dat_resep_rinc');
    $this->db->where('rincian_id', $rincian_id)->delete('dat_resep_racik');
  }
}
