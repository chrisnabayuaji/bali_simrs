<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jadwal_dokter extends CI_Model {

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
  //   if (@$cookie['search']['term'] != '') {
  //     $where .= "AND a.parameter_nm LIKE '%".$this->db->escape_like_str($cookie['search']['term'])."%' ";
    // }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
              a.*, b.lokasi_nm
            FROM web_jadwal_dokter a
            LEFT JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id 
            $where
            ORDER BY "
              .$cookie['order']['field']." ".$cookie['order']['type'].
            " LIMIT ".$cookie['cur_page'].",".$cookie['per_page'];
    $query = $this->db->query($sql);
    $result = $query->result_array();
    // 
    foreach($result as $key => $val) {
      $result[$key]['rinc_senin'] = $this->jadwal_dokter_rinc($result[$key]['jadwal_id'], 'senin');
      $result[$key]['rinc_selasa'] = $this->jadwal_dokter_rinc($result[$key]['jadwal_id'], 'selasa');
      $result[$key]['rinc_rabu'] = $this->jadwal_dokter_rinc($result[$key]['jadwal_id'], 'rabu');
      $result[$key]['rinc_kamis'] = $this->jadwal_dokter_rinc($result[$key]['jadwal_id'], 'kamis');
      $result[$key]['rinc_jumat'] = $this->jadwal_dokter_rinc($result[$key]['jadwal_id'], 'jumat');
      $result[$key]['rinc_sabtu'] = $this->jadwal_dokter_rinc($result[$key]['jadwal_id'], 'sabtu');
      $result[$key]['rinc_minggu'] = $this->jadwal_dokter_rinc($result[$key]['jadwal_id'], 'minggu');
    }
    return $result;
  }

  public function jadwal_dokter_rinc($jadwal_id='', $hari='')
  {
    $sql = "SELECT a.*, b.pegawai_nm 
            FROM web_jadwal_dokter_rinc a 
            LEFT JOIN mst_pegawai b ON a.dokter_id=b.pegawai_id
            WHERE a.is_deleted = 0 
            AND a.jadwal_id=?
            AND a.hari=? 
            ORDER BY created_at";
    $query = $this->db->query($sql, array($jadwal_id, $hari));
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM web_jadwal_dokter a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM web_jadwal_dokter a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id) {
    $sql = "SELECT * FROM web_jadwal_dokter WHERE jadwal_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  function check_by_lokasi_id($lokasi_id) {
    $sql = "SELECT * FROM web_jadwal_dokter WHERE lokasi_id=?";
    $query = $this->db->query($sql, array($lokasi_id));
    $row = $query->row_array();
    return $row;
  }

  function count_jadwal_dokter_rinc($id, $hari) {
    $sql = "SELECT COUNT(*) AS jml_data FROM web_jadwal_dokter_rinc WHERE jadwal_id=? AND hari=?";
    $query = $this->db->query($sql, array($id, $hari));
    $row = $query->row_array();
    if ($row['jml_data'] > 0) {
      return $row['jml_data']-1;
    }else{
      return $row['jml_data'];
    }
  }

  public function delete_item($id)
  {
    $this->db->where('jadwalrinc_id', $id)->delete('web_jadwal_dokter_rinc');
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    $web_jadwal_dokter['lokasi_id'] = $data['lokasi_id'];
    if ($id == null) {
      $web_jadwal_dokter['created_at'] = date('Y-m-d H:i:s');
      $web_jadwal_dokter['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('web_jadwal_dokter', $web_jadwal_dokter);
      $jadwal_id = $this->db->insert_id();
    }else{
      $web_jadwal_dokter['updated_at'] = date('Y-m-d H:i:s');
      $web_jadwal_dokter['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('jadwal_id', $id)->update('web_jadwal_dokter', $web_jadwal_dokter);
      $jadwal_id = $id;
    }

    // senin
    for($n=1; $n<=$data['item_no_senin']; $n++) {
      $jadwalrinc_id = @$data['jadwalrinc_id_senin_'.$n];
      $senin['jadwal_id'] = @$jadwal_id;
      $senin['dokter_id'] = @$data['dokter_id_senin_'.$n];
      $senin['hari'] = 'senin';
      $senin['jam'] = @$data['jam_senin_'.$n];
      $senin['jeda_waktu'] = @$data['jeda_waktu_senin_'.$n];
      $senin['max_pasien'] = @$data['max_pasien_senin_'.$n];
      //
      if ($jadwalrinc_id == null) {
        $senin['created_at'] = date('Y-m-d H:i:s');
        $senin['created_by'] = $this->session->userdata('sess_user_realname');
        $this->db->insert('web_jadwal_dokter_rinc', $senin);
      }else{
        $senin['updated_at'] = date('Y-m-d H:i:s');
        $senin['updated_by'] = $this->session->userdata('sess_user_realname');
        $this->db->where('jadwalrinc_id', $jadwalrinc_id)->update('web_jadwal_dokter_rinc', $senin);
      }
    }

    // selasa
    for($n=1; $n<=$data['item_no_selasa']; $n++) {
      $jadwalrinc_id = @$data['jadwalrinc_id_selasa_'.$n];
      $selasa['jadwal_id'] = @$jadwal_id;
      $selasa['dokter_id'] = @$data['dokter_id_selasa_'.$n];
      $selasa['hari'] = 'selasa';
      $selasa['jam'] = @$data['jam_selasa_'.$n];
      $selasa['jeda_waktu'] = @$data['jeda_waktu_selasa_'.$n];
      $selasa['max_pasien'] = @$data['max_pasien_selasa_'.$n];
      //
      if ($jadwalrinc_id == null) {
        $selasa['created_at'] = date('Y-m-d H:i:s');
        $selasa['created_by'] = $this->session->userdata('sess_user_realname');
        $this->db->insert('web_jadwal_dokter_rinc', $selasa);
      }else{
        $selasa['updated_at'] = date('Y-m-d H:i:s');
        $selasa['updated_by'] = $this->session->userdata('sess_user_realname');
        $this->db->where('jadwalrinc_id', $jadwalrinc_id)->update('web_jadwal_dokter_rinc', $selasa);
      }
    }

    // rabu
    for($n=1; $n<=$data['item_no_rabu']; $n++) {
      $jadwalrinc_id = @$data['jadwalrinc_id_rabu_'.$n];
      $rabu['jadwal_id'] = @$jadwal_id;
      $rabu['dokter_id'] = @$data['dokter_id_rabu_'.$n];
      $rabu['hari'] = 'rabu';
      $rabu['jam'] = @$data['jam_rabu_'.$n];
      $rabu['jeda_waktu'] = @$data['jeda_waktu_rabu_'.$n];
      $rabu['max_pasien'] = @$data['max_pasien_rabu_'.$n];
      //
      if ($jadwalrinc_id == null) {
        $rabu['created_at'] = date('Y-m-d H:i:s');
        $rabu['created_by'] = $this->session->userdata('sess_user_realname');
        $this->db->insert('web_jadwal_dokter_rinc', $rabu);
      }else{
        $rabu['updated_at'] = date('Y-m-d H:i:s');
        $rabu['updated_by'] = $this->session->userdata('sess_user_realname');
        $this->db->where('jadwalrinc_id', $jadwalrinc_id)->update('web_jadwal_dokter_rinc', $rabu);
      }
    }

    // kamis
    for($n=1; $n<=$data['item_no_kamis']; $n++) {
      $jadwalrinc_id = @$data['jadwalrinc_id_kamis_'.$n];
      $kamis['jadwal_id'] = @$jadwal_id;
      $kamis['dokter_id'] = @$data['dokter_id_kamis_'.$n];
      $kamis['hari'] = 'kamis';
      $kamis['jam'] = @$data['jam_kamis_'.$n];
      $kamis['jeda_waktu'] = @$data['jeda_waktu_kamis_'.$n];
      $kamis['max_pasien'] = @$data['max_pasien_kamis_'.$n];
      //
      if ($jadwalrinc_id == null) {
        $kamis['created_at'] = date('Y-m-d H:i:s');
        $kamis['created_by'] = $this->session->userdata('sess_user_realname');
        $this->db->insert('web_jadwal_dokter_rinc', $kamis);
      }else{
        $kamis['updated_at'] = date('Y-m-d H:i:s');
        $kamis['updated_by'] = $this->session->userdata('sess_user_realname');
        $this->db->where('jadwalrinc_id', $jadwalrinc_id)->update('web_jadwal_dokter_rinc', $kamis);
      }
    }

    // jumat
    for($n=1; $n<=$data['item_no_jumat']; $n++) {
      $jadwalrinc_id = @$data['jadwalrinc_id_jumat_'.$n];
      $jumat['jadwal_id'] = @$jadwal_id;
      $jumat['dokter_id'] = @$data['dokter_id_jumat_'.$n];
      $jumat['hari'] = 'jumat';
      $jumat['jam'] = @$data['jam_jumat_'.$n];
      $jumat['jeda_waktu'] = @$data['jeda_waktu_jumat_'.$n];
      $jumat['max_pasien'] = @$data['max_pasien_jumat_'.$n];
      //
      if ($jadwalrinc_id == null) {
        $jumat['created_at'] = date('Y-m-d H:i:s');
        $jumat['created_by'] = $this->session->userdata('sess_user_realname');
        $this->db->insert('web_jadwal_dokter_rinc', $jumat);
      }else{
        $jumat['updated_at'] = date('Y-m-d H:i:s');
        $jumat['updated_by'] = $this->session->userdata('sess_user_realname');
        $this->db->where('jadwalrinc_id', $jadwalrinc_id)->update('web_jadwal_dokter_rinc', $jumat);
      }
    }

    // sabtu
    for($n=1; $n<=$data['item_no_sabtu']; $n++) {
      $jadwalrinc_id = @$data['jadwalrinc_id_sabtu_'.$n];
      $sabtu['jadwal_id'] = @$jadwal_id;
      $sabtu['dokter_id'] = @$data['dokter_id_sabtu_'.$n];
      $sabtu['hari'] = 'sabtu';
      $sabtu['jam'] = @$data['jam_sabtu_'.$n];
      $sabtu['jeda_waktu'] = @$data['jeda_waktu_sabtu_'.$n];
      $sabtu['max_pasien'] = @$data['max_pasien_sabtu_'.$n];
      //
      if ($jadwalrinc_id == null) {
        $sabtu['created_at'] = date('Y-m-d H:i:s');
        $sabtu['created_by'] = $this->session->userdata('sess_user_realname');
        $this->db->insert('web_jadwal_dokter_rinc', $sabtu);
      }else{
        $sabtu['updated_at'] = date('Y-m-d H:i:s');
        $sabtu['updated_by'] = $this->session->userdata('sess_user_realname');
        $this->db->where('jadwalrinc_id', $jadwalrinc_id)->update('web_jadwal_dokter_rinc', $sabtu);
      }
    }

    // minggu
    for($n=1; $n<=$data['item_no_minggu']; $n++) {
      $jadwalrinc_id = @$data['jadwalrinc_id_minggu_'.$n];
      $minggu['jadwal_id'] = @$jadwal_id;
      $minggu['dokter_id'] = @$data['dokter_id_minggu_'.$n];
      $minggu['hari'] = 'minggu';
      $minggu['jam'] = @$data['jam_minggu_'.$n];
      $minggu['jeda_waktu'] = @$data['jeda_waktu_minggu_'.$n];
      $minggu['max_pasien'] = @$data['max_pasien_minggu_'.$n];
      //
      if ($jadwalrinc_id == null) {
        $minggu['created_at'] = date('Y-m-d H:i:s');
        $minggu['created_by'] = $this->session->userdata('sess_user_realname');
        $this->db->insert('web_jadwal_dokter_rinc', $minggu);
      }else{
        $minggu['updated_at'] = date('Y-m-d H:i:s');
        $minggu['updated_by'] = $this->session->userdata('sess_user_realname');
        $this->db->where('jadwalrinc_id', $jadwalrinc_id)->update('web_jadwal_dokter_rinc', $minggu);
      }
    }

  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('jadwal_id',$id)->update('web_jadwal_dokter', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('jadwal_id', $id)->delete('web_jadwal_dokter');
      $this->db->where('jadwal_id', $id)->delete('web_jadwal_dokter_rinc');
    }else{
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('jadwal_id', $id)->update('web_jadwal_dokter', $data);
    }
  }
  
}