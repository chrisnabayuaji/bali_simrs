<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_reg_ranap_kunjungan extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 AND a.is_ranap = 1 ";
    if (@$cookie['search']['tgl_registrasi_from'] != '' && @$cookie['search']['tgl_registrasi_to']) {
      $where .= "AND (DATE(a.tgl_registrasi) BETWEEN '" . to_date(@$cookie['search']['tgl_registrasi_from']) . "' AND '" . to_date(@$cookie['search']['tgl_registrasi_to']) . "')";
    }
    if (@$cookie['search']['no_rm_nm'] != '') {
      $where .= "AND (a.pasien_id LIKE '%" . $this->db->escape_like_str($cookie['search']['no_rm_nm']) . "%' OR a.pasien_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['no_rm_nm']) . "%' ) ";
    }
    if (@$cookie['search']['periksa_st_lab'] != '') {
      $where .= "AND a.periksa_st = '" . $this->db->escape_like_str($cookie['search']['periksa_st_lab']) . "' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
              a.*,
              b.jenispasien_nm,
              c.lokasi_nm AS lokasi_asal,
              d.lokasi_nm AS lokasi_bangsal,
              e.kamar_nm,
              f.parameter_val AS status_ranap_nm, 
              g.kelas_nm 
            FROM reg_pasien a
            LEFT JOIN mst_jenis_pasien b ON a.jenispasien_id = b.jenispasien_id
            JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            JOIN mst_lokasi d ON a.lokasi_id = d.lokasi_id
            LEFT JOIN mst_kamar e ON a.kamar_id = e.kamar_id
            LEFT JOIN mst_parameter f ON a.status_ranap_cd = f.parameter_cd AND parameter_group = 'RANAP'
            LEFT JOIN mst_kelas g ON a.kelas_id = g.kelas_id
            $where
            ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM reg_pasien a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT a.*
            FROM reg_pasien a
            $where";
    $query = $this->db->query($sql);
    return $query->num_rows();
  }

  public function total_verifikasi()
  {
    $sql = "SELECT 
              a.reg_id, a.pasien_id, a.pasien_nm, a.alamat, a.sex_cd, a.provinsi, a.kabupaten, a.kecamatan, a.kelurahan,
              b.tgl_tindaklanjut,
              c.jenispasien_nm,
              d.lokasi_nm as lokasi_asal, d.lokasi_id as lokasi_asal_id,
              e.lokasi_nm as lokasi_bangsal, e.lokasi_id as lokasi_bangsal_id
            FROM reg_pasien a
            INNER JOIN reg_pasien_tindaklanjut b ON a.reg_id = b.reg_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
            LEFT JOIN mst_lokasi d ON a.lokasi_id = d.lokasi_id
            LEFT JOIN mst_lokasi e ON b.lokasi_id = e.lokasi_id 
            WHERE b.is_deleted = 0 AND a.tindaklanjut_cd = '02' AND b.st_verif_ranap IS NULL";
    $query = $this->db->query($sql);
    return $query->num_rows();
  }

  public function get_data($id)
  {
    $sql = "SELECT 
              a.*,
              a.src_lokasi_id  AS lokasi_asal_id,
              b.lokasi_id AS lokasi_bangsal_id, b.kamar_id, 
              c.pegawai_nm, 
              d.jenispasien_nm, 
              e.lokasi_nm 
            FROM reg_pasien a
            JOIN reg_pasien_kamar b ON b.reg_id = a.reg_id
            LEFT JOIN mst_pegawai c ON a.dokter_id=c.pegawai_id
            LEFT JOIN mst_jenis_pasien d ON a.jenispasien_id=d.jenispasien_id
            LEFT JOIN mst_lokasi e ON a.src_lokasi_id = e.lokasi_id
            WHERE a.reg_id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    $data['provinsi'] = get_wilayah($data['wilayah_prop'], 'name');
    $data['kabupaten'] = get_wilayah($data['wilayah_kab'], 'name');
    $data['kecamatan'] = get_wilayah($data['wilayah_kec'], 'name');
    $data['kelurahan'] = get_wilayah($data['wilayah_kel'], 'name');
    $data['wilayah_id'] = get_wilayah($data['wilayah_kel'], 'id');
    $data['tgl_lahir'] = to_date($data['tgl_lahir']);
    $data['tgl_registrasi'] = to_date($data['tgl_registrasi'], '-', 'full_date');
    $data['kelompokumur_id'] = get_kelompokumur($data['tgl_lahir']);
    $data['reg_id'] = get_id('reg_pasien');
    $data['is_billing'] = 0;
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['created_by'] = $this->session->userdata('sess_user_realname');
    $data['is_ranap'] = 1;

    $reg_id_ibu = $data['reg_id_ibu'];
    $ibu_nm = $data['ibu_nm'];
    $ibu_umur = $data['ibu_umur'];
    $ayah_nm = $data['ayah_nm'];
    $jam_lahir = $data['jam_lahir'];
    $bb = $data['bb'];
    $pb = $data['pb'];
    $ibu_groupreg_in = $data['ibu_groupreg_in'];
    $lokasi_id = $data['lokasi_asal_id'];
    $kelas_id = $data['src_kelas_id'];
    $lokasi_bangsal_id = $data['lokasi_bangsal_id'];
    $dr_persalinan = $data['dr_persalinan'];

    if ($data['jns_rawat_cd'] == '01') {
      $data['groupreg_id'] = null;
      $data['groupreg_in'] = null;
      $data['lokasi_id'] = $data['lokasi_asal_id'];
      $data['kelas_id'] = $data['src_kelas_id'];
      $data['src_lokasi_id'] = $data['lokasi_asal_id'];
      $data['src_kelas_id'] = $data['src_kelas_id'];
      $data['status_ranap_cd'] = 'S';
    } elseif ($data['jns_rawat_cd'] == '02') {
      $data['lokasi_id'] = $data['lokasi_bangsal_id'];
      $data['kelas_id'] = $data['kelas_id'];
      $data['src_lokasi_id'] = $data['lokasi_asal_id'];
      $data['src_kelas_id'] = $data['src_kelas_id'];
    }
    //
    unset($data['lokasi_asal_id'], $data['lokasi_bangsal_id'], $data['reg_id_ibu'], $data['ibu_umur'], $data['ibu_groupreg_in'], $data['jam_lahir'], $data['ayah_nm'], $data['bb'], $data['pb'], $data['dr_persalinan']);
    unset($data['wilayah_prop'], $data['wilayah_kab'], $data['wilayah_kec'], $data['wilayah_kel']);
    unset($data['ibureg_id'], $data['ibu_nm'], $data['update_master'], $data['rm_otomatis']);

    // insert mst_pasien
    if ($data['statuspasien_cd'] == 'B') {
      $status_mst_pasien = $this->insert_mst_pasien($data);
      if ($status_mst_pasien != null || $status_mst_pasien != '') {
        // insert reg_pasien
        $data['pasien_id'] = $status_mst_pasien;
        $this->db->insert('reg_pasien', $data);
        update_id('reg_pasien', $data['reg_id']);

        //insert ke tabel jika itu bayi
        //insert ke table reg_pasien_bayi
        $rb = array(
          'reg_id' => $data['reg_id'],
          'pasien_id' => $status_mst_pasien,
          'anak_ke' => $data['anak_ke'],
          'ibureg_id' => $reg_id_ibu,
          'ibu_nm' => $ibu_nm,
          'ibu_umur' => $ibu_umur,
          'ayah_nm' => $ayah_nm,
          'tgl_lahir' => $data['tgl_lahir'],
          'jam_lahir' => $jam_lahir,
          'bb' => $bb,
          'pb' => $pb,
          'sex_cd' => $data['sex_cd'],
          'dr_persalinan' => $dr_persalinan
        );
        $this->db->insert('reg_pasien_bayi', $rb);

        // Kemudian update reg_pasien ibu (update reg_pasien where reg_id=$ibureg_id) dgn kondisi sbb :
        $di = array(
          'groupreg_in' => $ibu_groupreg_in . $data['reg_id'] . ';'
        );
        $this->db->where('reg_id', $reg_id_ibu)->update('reg_pasien', $di);

        // if ($data['jns_rawat_cd'] == '01') {
        //   //insert tindakan bayi
        //   $this->insert_dat_tindakan_bayi($data, '05.01.002', $status_mst_pasien, $lokasi_id, $kelas_id, $reg_id_ibu);
        // }
      }
    }

    if ($data['jns_rawat_cd'] == '01') {
      // get tindakan
      $kamar_bayi = $this->get_tindakan('05.01.002', $data['src_kelas_id']);
      //3. insert ke table reg_pasien_kamar
      $kamar = $this->m_kamar->by_field('kamar_id', $data['kamar_id'], 'row');
      $d2 = array(
        'regkamar_id' => get_id('reg_pasien_kamar'),
        'reg_id' => $data['reg_id'],
        'pasien_id' => $status_mst_pasien,
        'kelas_id' => $kelas_id,
        'tarif_id' => $kamar_bayi['tarif_id'],
        'lokasi_id' => $lokasi_id,
        'kamar_id' => @$data['kamar_id'],
        'tgl_masuk' => $data['tgl_registrasi'],
        'nom_tarif' => $kamar_bayi['nominal'],
        'jml_awal' => $kamar_bayi['nominal'],
        'jml_tagihan' => $kamar_bayi['nominal']
      );
      $this->db->insert('reg_pasien_kamar', $d2);
      update_id('reg_pasien_kamar', $d2['regkamar_id']);
    } elseif ($data['jns_rawat_cd'] == '02') {
      //3. insert ke table reg_pasien_kamar
      $kamar = $this->m_kamar->by_field('kamar_id', $data['kamar_id'], 'row');
      $d2 = array(
        'regkamar_id' => get_id('reg_pasien_kamar'),
        'reg_id' => $data['reg_id'],
        'pasien_id' => $status_mst_pasien,
        'kelas_id' => @$data['kelas_id'],
        'tarif_id' => $kamar['tarif_id'],
        'lokasi_id' => $lokasi_bangsal_id,
        'kamar_id' => @$data['kamar_id'],
        'tgl_masuk' => $data['tgl_registrasi'],
        'nom_tarif' => $kamar['nom_tarif'],
        'jml_awal' => $kamar['nom_tarif'],
        'jml_tagihan' => $kamar['nom_tarif']
      );
      $this->db->insert('reg_pasien_kamar', $d2);
      update_id('reg_pasien_kamar', $d2['regkamar_id']);

      // 4. update mst_kamar
      // update mst_kamar set jml_bed_kosong=jml_bed_kosong-1, jml_bed_terpakai=jml_bed_terpakai+1 where kamar_id=$kamar_baru
      $d4 = array(
        'jml_bed_kosong' => $kamar['jml_bed_kosong'] - 1,
        'jml_bed_terpakai' => $kamar['jml_bed_terpakai'] + 1
      );
      $this->db->where('kamar_id', $kamar['kamar_id'])->update('mst_kamar', $d4);
    }
  }

  public function save_edit($id = null)
  {
    $data = html_escape($this->input->post());
    $data['provinsi'] = get_wilayah($data['wilayah_prop'], 'name');
    $data['kabupaten'] = get_wilayah($data['wilayah_kab'], 'name');
    $data['kecamatan'] = get_wilayah($data['wilayah_kec'], 'name');
    $data['kelurahan'] = get_wilayah($data['wilayah_kel'], 'name');
    $data['wilayah_id'] = get_wilayah($data['wilayah_kel'], 'id');
    $data['tgl_lahir'] = to_date($data['tgl_lahir']);
    $data['kelompokumur_id'] = get_kelompokumur($data['tgl_lahir']);
    $data['jeniskunjungan_cd'] = 'L';
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('sess_user_realname');

    if ($data['jns_rawat_cd'] == '01') {
      $data['groupreg_id'] = null;
      $data['groupreg_in'] = null;
      $data['lokasi_id'] = $data['lokasi_asal_id'];
      $data['kelas_id'] = $data['src_kelas_id'];
      $data['src_lokasi_id'] = $data['lokasi_asal_id'];
      $data['src_kelas_id'] = $data['src_kelas_id'];
      $data['status_ranap_cd'] = 'S';
    } elseif ($data['jns_rawat_cd'] == '02') {
      $data['lokasi_id'] = $data['lokasi_bangsal_id'];
      $data['kelas_id'] = $data['kelas_id'];
      $data['src_lokasi_id'] = $data['lokasi_asal_id'];
      $data['src_kelas_id'] = $data['src_kelas_id'];
    }
    //
    unset($data['lokasi_asal_id'], $data['lokasi_bangsal_id'], $data['reg_id_ibu'], $data['ibu_umur'], $data['ibu_groupreg_in'], $data['jam_lahir'], $data['ayah_nm'], $data['bb'], $data['pb'], $data['dr_persalinan']);
    unset($data['wilayah_prop'], $data['wilayah_kab'], $data['wilayah_kec'], $data['wilayah_kel']);
    unset($data['ibureg_id'], $data['ibu_nm'], $data['update_master'], $data['rm_otomatis']);

    $this->db->where('reg_id', $id)->update('reg_pasien', $data);
  }

  public function insert_mst_pasien($data)
  {
    $data_mst_pasien['pasien_id'] = get_last_pasien_id();

    // $data_mst_pasien['pasien_id'] = $data['pasien_id'];
    $data_mst_pasien['pasien_nm'] = $data['pasien_nm'];
    $data_mst_pasien['sebutan_cd'] = $data['sebutan_cd'];
    $data_mst_pasien['anak_ke'] = $data['anak_ke'];
    $data_mst_pasien['nik'] = $data['nik'];
    $data_mst_pasien['nama_kk'] = $data['nama_kk'];
    $data_mst_pasien['no_kk'] = $data['no_kk'];
    $data_mst_pasien['alamat'] = $data['alamat'];
    $data_mst_pasien['kode_pos'] = $data['kode_pos'];
    $data_mst_pasien['wilayah_st'] = $data['wilayah_st'];
    $data_mst_pasien['provinsi'] = $data['provinsi'];
    $data_mst_pasien['kabupaten'] = $data['kabupaten'];
    $data_mst_pasien['kecamatan'] = $data['kecamatan'];
    $data_mst_pasien['kelurahan'] = $data['kelurahan'];
    $data_mst_pasien['wilayah_id'] = $data['wilayah_id'];
    $data_mst_pasien['tmp_lahir'] = $data['tmp_lahir'];
    $data_mst_pasien['tgl_lahir'] = $data['tgl_lahir'];
    $data_mst_pasien['sex_cd'] = $data['sex_cd'];
    $data_mst_pasien['goldarah_cd'] = $data['goldarah_cd'];
    $data_mst_pasien['pendidikan_cd'] = $data['pendidikan_cd'];
    $data_mst_pasien['pekerjaan_cd'] = $data['pekerjaan_cd'];
    $data_mst_pasien['agama_cd'] = $data['agama_cd'];
    $data_mst_pasien['telp'] = $data['no_telp'];
    $data_mst_pasien['jenispasien_id'] = $data['jenispasien_id'];
    $data_mst_pasien['no_kartu'] = $data['no_kartu'];
    $data_mst_pasien['kepesertaan_cd'] = $data['kepesertaan_cd'];
    $data_mst_pasien['nm_pj_kepesertaan'] = $data['nm_pj_kepesertaan'];
    $data_mst_pasien['pasien_st'] = 1;
    $data_mst_pasien['tgl_catat'] = date('Y-m-d');
    $data_mst_pasien['created_at'] = date('Y-m-d H:i:s');
    $data_mst_pasien['created_by'] = $this->session->userdata('sess_user_realname');


    $this->db->trans_start();

    $this->db->insert('mst_pasien', $data_mst_pasien);

    $this->db->trans_complete();

    if ($this->db->trans_status() === FALSE) {
      return null;
    } else {
      return $data_mst_pasien['pasien_id'];
    }
  }

  public function update_mst_pasien($data)
  {
    $data_mst_pasien['pasien_id'] = $data['pasien_id'];
    $data_mst_pasien['pasien_nm'] = $data['pasien_nm'];
    $data_mst_pasien['sebutan_cd'] = $data['sebutan_cd'];
    $data_mst_pasien['anak_ke'] = $data['anak_ke'];
    $data_mst_pasien['nik'] = $data['nik'];
    $data_mst_pasien['nama_kk'] = $data['nama_kk'];
    $data_mst_pasien['no_kk'] = $data['no_kk'];
    $data_mst_pasien['alamat'] = $data['alamat'];
    $data_mst_pasien['kode_pos'] = $data['kode_pos'];
    $data_mst_pasien['wilayah_st'] = $data['wilayah_st'];
    $data_mst_pasien['provinsi'] = $data['provinsi'];
    $data_mst_pasien['kabupaten'] = $data['kabupaten'];
    $data_mst_pasien['kecamatan'] = $data['kecamatan'];
    $data_mst_pasien['kelurahan'] = $data['kelurahan'];
    $data_mst_pasien['wilayah_id'] = $data['wilayah_id'];
    $data_mst_pasien['tmp_lahir'] = $data['tmp_lahir'];
    $data_mst_pasien['tgl_lahir'] = $data['tgl_lahir'];
    $data_mst_pasien['sex_cd'] = $data['sex_cd'];
    $data_mst_pasien['goldarah_cd'] = $data['goldarah_cd'];
    $data_mst_pasien['pendidikan_cd'] = $data['pendidikan_cd'];
    $data_mst_pasien['pekerjaan_cd'] = $data['pekerjaan_cd'];
    $data_mst_pasien['agama_cd'] = $data['agama_cd'];
    $data_mst_pasien['telp'] = $data['no_telp'];
    $data_mst_pasien['jenispasien_id'] = $data['jenispasien_id'];
    $data_mst_pasien['no_kartu'] = $data['no_kartu'];
    $data_mst_pasien['kepesertaan_cd'] = $data['kepesertaan_cd'];
    $data_mst_pasien['nm_pj_kepesertaan'] = $data['nm_pj_kepesertaan'];
    $data_mst_pasien['pasien_st'] = 1;
    $data_mst_pasien['tgl_catat'] = date('Y-m-d');
    $data_mst_pasien['created_at'] = date('Y-m-d H:i:s');
    $data_mst_pasien['created_by'] = $this->session->userdata('sess_user_realname');
    $this->db->where('pasien_id', $data['pasien_id'])->update('mst_pasien', $data_mst_pasien);
  }

  public function insert_dat_tindakan($data)
  {
    $count_admisi_dokter = $this->count_admisi_dokter($data['kelas_id'], $data['lokasi_id'], $data['dokter_id']);
    if ($count_admisi_dokter > 0) {
      $result_data = $this->get_result_data($data['kelas_id'], $data['lokasi_id'], $data['dokter_id']);
    } else {
      $result_data = $this->get_result_data($data['kelas_id'], $data['lokasi_id']);
    }

    foreach ($result_data as $value) {
      $data_dat_tindakan['tindakan_id'] = get_id('dat_tindakan');
      $data_dat_tindakan['reg_id'] = $data['reg_id'];
      $data_dat_tindakan['pasien_id'] = $data['pasien_id'];
      $data_dat_tindakan['lokasi_id'] = $data['lokasi_id'];
      $data_dat_tindakan['kelas_id'] = $data['kelas_id'];
      $data_dat_tindakan['tarif_id'] = $value['tarif_id'];
      $data_dat_tindakan['tarif_nm'] = $value['tarif_nm'];
      $data_dat_tindakan['js'] = $value['js'];
      $data_dat_tindakan['jp'] = $value['jp'];
      $data_dat_tindakan['jb'] = $value['jb'];
      $data_dat_tindakan['nom_tarif'] = $value['nominal'];
      $data_dat_tindakan['qty'] = 1;
      $data_dat_tindakan['jml_awal'] = $value['nominal'] * 1;
      $data_dat_tindakan['jml_tagihan'] = $value['nominal'] * 1;
      $data_dat_tindakan['jenistindakan_cd'] = 1;
      $data_dat_tindakan['petugas_id'] = $data['dokter_id'];
      $data_dat_tindakan['tgl_catat'] = date('Y-m-d H:i:s');
      $data_dat_tindakan['created_at'] = date('Y-m-d H:i:s');
      $data_dat_tindakan['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_tindakan', $data_dat_tindakan);
      update_id('dat_tindakan', $data_dat_tindakan['tindakan_id']);
    }
  }

  public function insert_dat_tindakan_bayi($data, $tarif_id, $pasien_id, $lokasi_id, $kelas_id, $reg_id_ibu)
  {
    $tindakan = $this->get_tindakan($tarif_id, $data['src_kelas_id']);
    //
    $data_dat_tindakan['tindakan_id'] = get_id('dat_tindakan');
    $data_dat_tindakan['reg_id'] = $reg_id_ibu;
    $data_dat_tindakan['pasien_id'] = $pasien_id;
    $data_dat_tindakan['lokasi_id'] = $lokasi_id;
    $data_dat_tindakan['kelas_id'] = $kelas_id;
    $data_dat_tindakan['tarif_id'] = $tindakan['tarif_id'];
    $data_dat_tindakan['tarif_nm'] = $tindakan['tarif_nm'];
    $data_dat_tindakan['js'] = $tindakan['js'];
    $data_dat_tindakan['jp'] = $tindakan['jp'];
    $data_dat_tindakan['jb'] = $tindakan['jb'];
    $data_dat_tindakan['nom_tarif'] = $tindakan['nominal'];
    $data_dat_tindakan['qty'] = 1;
    $data_dat_tindakan['jml_awal'] = $tindakan['nominal'] * 1;
    $data_dat_tindakan['jml_tagihan'] = $tindakan['nominal'] * 1;
    $data_dat_tindakan['jenistindakan_cd'] = 1;
    $data_dat_tindakan['petugas_id'] = $data['dokter_id'];
    $data_dat_tindakan['tgl_catat'] = date('Y-m-d H:i:s');
    $data_dat_tindakan['created_at'] = date('Y-m-d H:i:s');
    $data_dat_tindakan['created_by'] = $this->session->userdata('sess_user_realname');
    $this->db->insert('dat_tindakan', $data_dat_tindakan);
    update_id('dat_tindakan', $data_dat_tindakan['tindakan_id']);
  }

  public function get_tindakan($tarif_id = null, $kelas_id = null)
  {
    $sql = "SELECT
              a.tarifkelas_id,
              a.tarif_id,
              b.tarif_nm,
              a.kelas_id,
              a.js,
              a.jp,
              a.jb,
              a.nominal 
            FROM
              mst_tarif_kelas a
              JOIN mst_tarif b ON a.tarif_id = b.tarif_id 
            WHERE
              a.tarif_id=? AND a.kelas_id=? AND a.is_deleted = 0 AND a.is_active = 1";
    $query = $this->db->query($sql, array($tarif_id, $kelas_id));
    $result = $query->row_array();
    return $result;
  }

  public function insert_activity_vclaim($reg_id = null, $data = null)
  {
    $this->db->where('reg_id', $reg_id)->update('reg_pasien', $data);
  }

  public function insert_no_sep($reg_id = null, $sep_no = null)
  {
    $data['sep_no'] = $sep_no;
    $this->db->where('reg_id', $reg_id)->update('reg_pasien', $data);
  }

  public function delete($id)
  {
    trash('reg_pasien', array('reg_id' => $id));
    trash('reg_pasien_kamar', array('reg_id' => $id));
    $this->db->where('reg_id', $id)->delete('reg_pasien');
    $this->db->where('reg_id', $id)->delete('reg_pasien_kamar');
  }

  // Pindah Bangsal
  function get_data_pindah_bangsal($reg_id = null, $pasien_id = null, $kelas_id = null, $lokasi_id = null, $kamar_id = null)
  {
    $sql = "SELECT 
              a.*, b.lokasi_nm, c.kamar_nm
            FROM reg_pasien_kamar a 
            LEFT JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id
            LEFT JOIN mst_kamar c ON a.kamar_id=c.kamar_id
            WHERE a.reg_id=? AND a.pasien_id=? AND a.kelas_id=? AND a.lokasi_id=? AND a.kamar_id=?";
    $query = $this->db->query($sql, array($reg_id, $pasien_id, $kelas_id, $lokasi_id, $kamar_id));
    $row = $query->row_array();
    return $row;
  }

  public function save_pindah_bangsal($reg_id = null)
  {
    $data = html_escape($this->input->post());
    $reg = $this->db->where('reg_id', $reg_id)->get('reg_pasien')->row_array();
    $get_data = $this->db->where('reg_id', $reg_id)->where('regkamar_id', $data['regkamar_id'])->get('reg_pasien_kamar')->row_array();

    $masuk = date_create($get_data['tgl_masuk']);
    $keluar = date_create(to_date($data['tgl_pindah'], '-', 'full_date'));
    $interval = date_diff($masuk, $keluar);
    $jumlah_hari = $interval->format('%d');
    $jumlah_jam = $interval->format('%h');

    //cek pasien bpjs / umum
    $selisih = $jumlah_hari;
    if ($reg['jenispasien_id'] == '02') {
      $selisih += 1;
    }

    if ($jumlah_jam >= 12) {
      $selisih += 1;
    }

    $update_reg_pasien_kamar['lokasi_tujuan_id'] = $data['lokasi_id'];
    $update_reg_pasien_kamar['kamar_tujuan_id'] = $data['kamar_id'];
    $update_reg_pasien_kamar['tgl_keluar'] = to_date($data['tgl_pindah'], '-', 'full_date');
    $update_reg_pasien_kamar['jml_hari'] = $selisih;
    $update_reg_pasien_kamar['jml_awal'] = $get_data['nom_tarif'] * $update_reg_pasien_kamar['jml_hari'];
    $update_reg_pasien_kamar['jml_tagihan'] = $get_data['nom_tarif'] * $update_reg_pasien_kamar['jml_hari'];
    $update_reg_pasien_kamar['user_cd'] = $this->session->userdata('sess_user_cd');
    $update_reg_pasien_kamar['updated_at'] = date('Y-m-d H:i:s');
    $update_reg_pasien_kamar['updated_by'] = $this->session->userdata('sess_user_realname');
    $this->db->where('regkamar_id', $data['regkamar_id'])->update('reg_pasien_kamar', $update_reg_pasien_kamar);

    // Insert reg_pasien_kamar
    $kamar = $this->db->where('kamar_id', $data['kamar_id'])->get('mst_kamar')->row_array();
    $insert_reg_pasien_kamar['regkamar_id'] = get_id('reg_pasien_kamar');
    $insert_reg_pasien_kamar['reg_id'] = $data['reg_id'];
    $insert_reg_pasien_kamar['pasien_id'] = $data['pasien_id'];
    $insert_reg_pasien_kamar['kelas_id'] = $data['kelas_id'];
    $insert_reg_pasien_kamar['lokasi_id'] = $data['lokasi_id'];
    $insert_reg_pasien_kamar['kamar_id'] = $data['kamar_id'];
    $insert_reg_pasien_kamar['lokasi_asal_id'] = $data['lokasi_asal_id'];
    $insert_reg_pasien_kamar['kamar_asal_id'] = $data['kamar_asal_id'];
    $insert_reg_pasien_kamar['tgl_masuk'] = to_date($data['tgl_pindah'], '-', 'full_date');
    $insert_reg_pasien_kamar['tgl_keluar'] = NULL;
    $insert_reg_pasien_kamar['jml_hari'] = NULL;
    $insert_reg_pasien_kamar['tarif_id'] = $kamar['tarif_id'];
    $insert_reg_pasien_kamar['nom_tarif'] = $kamar['nom_tarif'];
    $insert_reg_pasien_kamar['jml_awal'] = $kamar['nom_tarif'];
    $insert_reg_pasien_kamar['jml_tagihan'] = $kamar['nom_tarif'];
    $insert_reg_pasien_kamar['keterangan_kamar'] = $data['keterangan_kamar'];
    $insert_reg_pasien_kamar['user_cd'] = $this->session->userdata('sess_user_cd');
    $insert_reg_pasien_kamar['created_at'] = date('Y-m-d H:i:s');
    $insert_reg_pasien_kamar['created_by'] = $this->session->userdata('sess_user_realname');
    $this->db->insert('reg_pasien_kamar', $insert_reg_pasien_kamar);
    update_id('reg_pasien_kamar', $insert_reg_pasien_kamar['regkamar_id']);

    // Update reg_pasien group
    $registrasi = $this->db->where('reg_id', $data['reg_id'])->get('reg_pasien')->row_array();
    $reg_in = explode(';', $registrasi['groupreg_in']);
    foreach ($reg_in as $v) {
      $dk = array(
        'lokasi_id' => $data['lokasi_id'],
      );
      $this->db->where('reg_id', $v)->update('reg_pasien_tindaklanjut', $dk);
    }

    // Update reg_pasien current
    $update_reg_pasien['lokasi_id'] = $data['lokasi_id'];
    $update_reg_pasien['kelas_id'] = $data['kelas_id'];
    $update_reg_pasien['kamar_id'] = $data['kamar_id'];
    $update_reg_pasien['updated_at'] = date('Y-m-d H:i:s');
    $update_reg_pasien['updated_by'] = $this->session->userdata('sess_user_realname');
    $this->db->where('reg_id', $data['reg_id'])->update('reg_pasien', $update_reg_pasien);

    //konversi tarif ke kamar yang existing
    $tindakan = $this->db->query(
      "SELECT * FROM dat_tindakan WHERE '" . $reg['groupreg_in'] . "' LIKE CONCAT('%', reg_id,';%') "
    )->result_array();
    foreach ($tindakan as $r) {
      $tarif = $this->db->where('tarif_id', $r['tarif_id'])->where('kelas_id', $data['kelas_id'])->get('mst_tarif_kelas')->row_array();
      $dtarif = array(
        'kelas_id' => $tarif['kelas_id'],
        'js' => $tarif['js'],
        'jp' => $tarif['jp'],
        'jb' => $tarif['jb'],
        'nom_tarif' => $tarif['nominal'],
        'jml_awal' => $r['qty'] * $tarif['nominal'],
      );
      $dtarif['jml_tagihan'] = $dtarif['nom_tarif'] - $r['jml_potongan'];
      $this->db->where('tindakan_id', $r['tindakan_id'])->where('is_bayar', 0)->update('dat_tindakan', $dtarif);
    }

    //update jumlah tt
    //jika rawat gabung tidak mengurangi kamar
    if ($reg['jns_rawat_cd'] != '01') {
      $sql1 = "UPDATE mst_kamar SET jml_bed_kosong=ifnull(jml_bed_kosong,0) + 1, jml_bed_terpakai=ifnull(jml_bed_terpakai,0) - 1 
             WHERE kamar_id='" . $get_data['kamar_id'] . "'";
      $this->db->query($sql1);

      $sql2 = "UPDATE mst_kamar SET jml_bed_kosong=ifnull(jml_bed_kosong,0)-1, jml_bed_terpakai=ifnull(jml_bed_terpakai,0)+1 
             WHERE kamar_id='" . $data['kamar_id'] . "'";
      $this->db->query($sql2);
    }
  }

  // Riwayat Bangsal
  public function list_riwayat_bangsal($reg_id = null)
  {
    $sql = "SELECT a.*, b.lokasi_nm, c.kelas_nm, d.kamar_nm  
            FROM reg_pasien_kamar a 
            LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
            LEFT JOIN mst_kelas c ON a.kelas_id = c.kelas_id
            LEFT JOIN mst_kamar d ON a.kamar_id = d.kamar_id
            WHERE a.is_deleted = 0 AND a.reg_id=?
            ORDER BY regkamar_id ASC";
    $query = $this->db->query($sql, $reg_id);
    return $query->result_array();
  }

  public function ibu_row($id)
  {
    $sql = "SELECT * FROM reg_pasien WHERE reg_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function pasien_row($id)
  {
    $sql = "SELECT * FROM mst_pasien WHERE pasien_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    // sebutan_cd
    if (@$row['sebutan_cd'] != '') {
      $row['sebutan_cd'] = $row['sebutan_cd'];
    } else {
      if ($row['sex_cd'] == 'L') {
        $row['sebutan_cd'] = 'Tn';
      } elseif ($row['sex_cd'] == 'P') {
        $row['sebutan_cd'] = 'Ny';
      }
    }
    return $row;
  }

  public function count_admisi_dokter($kelas_id = null, $lokasi_id = null, $dokter_id = null)
  {
    $sql = "SELECT 
              COUNT(*) AS count_data
            FROM mst_tarif_admisi 
            WHERE kelas_id='$kelas_id' AND lokasi_id='$lokasi_id' AND dokter_id='$dokter_id'";
    $query = $this->db->query($sql, array($kelas_id, $lokasi_id, $dokter_id));
    $row = $query->row_array();
    return $row['count_data'];
  }

  public function get_result_data($kelas_id = null, $lokasi_id = null, $dokter_id = null)
  {
    $where = "";
    if ($dokter_id != null) {
      $where .= "AND a.dokter_id='$dokter_id'";
    }
    $sql = "SELECT 
              a.*, b.js, b.jp, b.jb, b.nominal, c.tarif_nm
            FROM mst_tarif_admisi a
            JOIN mst_tarif_kelas b ON a.tarif_id = b.tarif_id AND a.kelas_id = b.kelas_id
            JOIN mst_tarif c ON a.tarif_id = c.tarif_id
            WHERE a.kelas_id=? AND a.lokasi_id=? $where";
    $query = $this->db->query($sql, array($kelas_id, $lokasi_id));
    $result = $query->result_array();
    return $result;
  }

  public function no_rm_row($id)
  {
    $sql = "SELECT * FROM mst_pasien WHERE pasien_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    $row['status_cari'] = (@$row != '') ? '1' : '0';
    $row['statuspasien_cd'] = (@$row['statuspasien_cd'] != '') ? @$row['statuspasien_cd'] : 'B';
    $row['wilayah_st'] = (@$row['wilayah_st'] != '') ? @$row['wilayah_st'] : 'D';
    $row['wilayah_id'] = (@$row['wilayah_id'] != '') ? @$row['wilayah_id'] : '';
    $row['tgl_lahir'] = (@$row['tgl_lahir'] != '') ? @$row['tgl_lahir'] : date('Y-m-d');
    $row['sex_cd'] = (@$row['sex_cd'] != '') ? @$row['sex_cd'] : 'L';
    $row['pendidikan_cd'] = (@$row['pendidikan_cd'] != '') ? @$row['pendidikan_cd'] : '00';
    $row['pekerjaan_cd'] = (@$row['pekerjaan_cd'] != '') ? @$row['pekerjaan_cd'] : '00';
    $row['agama_cd'] = (@$row['agama_cd'] != '') ? @$row['agama_cd'] : '00';
    $row['jenispasien_id'] = (@$row['jenispasien_id'] != '') ? @$row['jenispasien_id'] : '01';
    // sebutan_cd
    if (@$row['sebutan_cd'] != '') {
      $row['sebutan_cd'] = $row['sebutan_cd'];
    } else {
      if ($row['sex_cd'] == 'L') {
        $row['sebutan_cd'] = 'Tn';
      } elseif ($row['sex_cd'] == 'P') {
        $row['sebutan_cd'] = 'Ny';
      }
    }
    return $row;
  }

  public function get_reg_pasien($reg_id)
  {
    $sql = "SELECT 
              *
            FROM reg_pasien 
            WHERE reg_id=?";
    $query = $this->db->query($sql, array($reg_id));
    $row = $query->row_array();
    return $row;
  }
}
