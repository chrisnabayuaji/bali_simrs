<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_reg_ranap_verifikasi extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE b.is_deleted = 0 AND a.tindaklanjut_cd = '02' AND b.st_verif_ranap IS NULL ";
    if (@$cookie['search']['tgl_tindaklanjut_from'] != '' && @$cookie['search']['tgl_tindaklanjut_to']) {
      $where .= "AND (DATE(b.tgl_tindaklanjut) BETWEEN '" . to_date(@$cookie['search']['tgl_tindaklanjut_from']) . "' AND '" . to_date(@$cookie['search']['tgl_tindaklanjut_to']) . "')";
    }
    if (@$cookie['search']['no_rm_nm'] != '') {
      $where .= "AND (a.pasien_id LIKE '%" . $this->db->escape_like_str($cookie['search']['no_rm_nm']) . "%' OR a.pasien_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['no_rm_nm']) . "%' ) ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
              a.reg_id, a.pasien_id, a.pasien_nm, a.sebutan_cd, a.alamat, a.sex_cd, a.provinsi, a.kabupaten, a.kecamatan, a.kelurahan,
              b.tgl_tindaklanjut,
              c.jenispasien_nm,
              d.lokasi_nm as lokasi_asal,
              e.lokasi_nm as lokasi_bangsal
            FROM reg_pasien a
            INNER JOIN reg_pasien_tindaklanjut b ON a.reg_id = b.reg_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
            LEFT JOIN mst_lokasi d ON a.lokasi_id = d.lokasi_id
            LEFT JOIN mst_lokasi e ON b.lokasi_id = e.lokasi_id 
            $where
            ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT 
              a.reg_id, a.pasien_id, a.pasien_nm, a.alamat, a.sex_cd, a.provinsi, a.kabupaten, a.kecamatan, a.kelurahan,
              b.tgl_tindaklanjut,
              c.jenispasien_nm,
              d.lokasi_nm as lokasi_asal,
              e.lokasi_nm as lokasi_bangsal
            FROM reg_pasien a
            INNER JOIN reg_pasien_tindaklanjut b ON a.reg_id = b.reg_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
            LEFT JOIN mst_lokasi d ON a.lokasi_id = d.lokasi_id
            LEFT JOIN mst_lokasi e ON b.lokasi_id = e.lokasi_id 
            $where";
    $query = $this->db->query($sql);
    return $query->num_rows();
  }

  public function total_verifikasi()
  {
    $sql = "SELECT 
              a.reg_id, a.pasien_id, a.pasien_nm, a.alamat, a.sex_cd, a.provinsi, a.kabupaten, a.kecamatan, a.kelurahan,
              b.tgl_tindaklanjut,
              c.jenispasien_nm,
              d.lokasi_nm as lokasi_asal, d.lokasi_id as lokasi_asal_id,
              e.lokasi_nm as lokasi_bangsal, e.lokasi_id as lokasi_bangsal_id
            FROM reg_pasien a
            INNER JOIN reg_pasien_tindaklanjut b ON a.reg_id = b.reg_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
            LEFT JOIN mst_lokasi d ON a.lokasi_id = d.lokasi_id
            LEFT JOIN mst_lokasi e ON b.lokasi_id = e.lokasi_id 
            WHERE b.is_deleted = 0 AND a.tindaklanjut_cd = '02' AND b.st_verif_ranap IS NULL";
    $query = $this->db->query($sql);
    return $query->num_rows();
  }

  public function get_data($id)
  {
    $sql = "SELECT 
              a.*,
              b.tgl_tindaklanjut,
              c.jenispasien_nm,
              d.lokasi_nm as lokasi_asal, d.lokasi_id as lokasi_asal_id,
              e.lokasi_nm as lokasi_bangsal, e.lokasi_id as lokasi_bangsal_id
            FROM reg_pasien a
            INNER JOIN reg_pasien_tindaklanjut b ON a.reg_id = b.reg_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
            LEFT JOIN mst_lokasi d ON a.lokasi_id = d.lokasi_id
            LEFT JOIN mst_lokasi e ON b.lokasi_id = e.lokasi_id 
            WHERE b.reg_id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    $d = $data;
    // $d['provinsi'] = get_wilayah($d['wilayah_prop'], 'name');
    // $d['kabupaten'] = get_wilayah($d['wilayah_kab'], 'name');
    // $d['kecamatan'] = get_wilayah($d['wilayah_kec'], 'name');
    // $d['kelurahan'] = get_wilayah($d['wilayah_kel'], 'name');
    // $d['wilayah_id'] = get_wilayah($d['wilayah_kel'], 'id');

    $d['provinsi'] = get_wilayah(@$d['wilayah_prop'], 'name');
    $d['kabupaten'] = get_wilayah(@$d['wilayah_kab'], 'name');
    $d['kecamatan'] = get_wilayah(@$d['wilayah_kec'], 'name');
    $d['kelurahan'] = get_wilayah(@$d['wilayah_kel'], 'name');

    if (@$d['wilayah_prop'] == '') {
      $d['wilayah_id'] = '00.00.00.0000';
    } elseif (@$d['wilayah_kab'] == '') {
      $pecah_prov = explode("#", @$d['wilayah_prop'])[0];
      $d['wilayah_id'] = $pecah_prov . '.00.00.0000';
    } elseif (@$d['wilayah_kec'] == '') {
      $pecah_prov = explode("#", @$d['wilayah_prop'])[0];
      $pecah_kab = explode(".", explode("#", @$d['wilayah_kab'])[0])[1];
      $d['wilayah_id'] = $pecah_prov . '.' . $pecah_kab . '.00.0000';
    } elseif (@$d['wilayah_kel'] == '') {
      $pecah_prov = explode("#", @$d['wilayah_prop'])[0];
      $pecah_kab = explode(".", explode("#", @$d['wilayah_kab'])[0])[1];
      $pecah_kec = explode(".", explode("#", @$d['wilayah_kec'])[0])[2];
      $d['wilayah_id'] = $pecah_prov . '.' . $pecah_kab . '.' . $pecah_kec . '.0000';
    } else {
      $d['wilayah_id'] = get_wilayah($d['wilayah_kel'], 'id');
    }

    $d['tgl_lahir'] = to_date($d['tgl_lahir']);
    $d['tgl_registrasi'] = to_date($d['tgl_registrasi'], '-', 'full_date');
    $d['kelompokumur_id'] = get_kelompokumur($d['tgl_lahir']);
    unset($d['wilayah_prop'], $d['wilayah_kab'], $d['wilayah_kec'], $d['wilayah_kel'], $d['update_master']);
    //1. update table reg_pasien_tindaklanjut
    $this->db->where('reg_id', $id)->update('reg_pasien_tindaklanjut', array('st_verif_ranap' => 1));
    //update master_pasien
    if (@$data['update_master']) {
      $this->update_mst_pasien($data);
    }
    //2. insert table reg pasien
    $d['src_reg_id'] = $id;
    $d['src_lokasi_id'] = $d['lokasi_asal_id'];
    $d['lokasi_id'] = $d['lokasi_bangsal_id'];
    $d['reg_id'] = get_id('reg_pasien');
    $d['is_ranap'] = 1;
    $d['is_billing'] = 1;
    $d['groupreg_id'] = $d['reg_id'];
    $d['groupreg_in'] = $d['src_reg_id'] . ';' . $d['reg_id'] . ';';
    $d['created_at'] = date('Y-m-d H:i:s');
    $d['created_by'] = $this->session->userdata('sess_user_realname');
    unset($d['lokasi_asal_id'], $d['lokasi_bangsal_id'], $d['keterangan'], $d['keterangan_kamar']);
    $this->db->insert('reg_pasien', $d);
    update_id('reg_pasien', $d['reg_id']);
    // Update reg_pasien data rajal : utk meng-null kan groupreg agar jadi 1 tagihan billingnya dgn ranap
    $this->db->query("UPDATE reg_pasien SET groupreg_id = null, groupreg_in = null, is_billing=0 WHERE reg_id=$id");
    //3. insert ke table reg_pasien_kamar
    $kamar = $this->m_kamar->by_field('kamar_id', $data['kamar_id'], 'row');
    $d2 = array(
      'regkamar_id' => get_id('reg_pasien_kamar'),
      'reg_id' => $d['reg_id'],
      'pasien_id' => $data['pasien_id'],
      'kelas_id' => $data['kelas_id'],
      'tarif_id' => $kamar['tarif_id'],
      'lokasi_id' => $d['lokasi_id'],
      'kamar_id' => $data['kamar_id'],
      'tgl_masuk' => $d['tgl_registrasi'],
      'nom_tarif' => $kamar['nom_tarif'],
      'jml_awal' => $kamar['nom_tarif'],
      'jml_tagihan' => $kamar['nom_tarif'],
      'keterangan_kamar' => $data['keterangan_kamar']
    );
    $this->db->insert('reg_pasien_kamar', $d2);
    update_id('reg_pasien_kamar', $d2['regkamar_id']);
    // 4. update mst_kamar
    // update mst_kamar set jml_bed_kosong=jml_bed_kosong-1, jml_bed_terpakai=jml_bed_terpakai+1 where kamar_id=$kamar_baru
    $d4 = array(
      'jml_bed_kosong' => $kamar['jml_bed_kosong'] - 1,
      'jml_bed_terpakai' => $kamar['jml_bed_terpakai'] + 1
    );
    $this->db->where('kamar_id', $kamar['kamar_id'])->update('mst_kamar', $d4);
    // 5. tambah tarif administrasi ranap
    // cek dat_tindakan ada tarif admin ranap
    $tarif_admin_ranap = '05.01.005'; // administrasi rawat inap
    $cek_admin = $this->db
      ->where('reg_id', $d['reg_id'])
      ->where('tarif_id', $tarif_admin_ranap)
      ->get('dat_tindakan')->row_array();
    if ($cek_admin == null) {
      $tarif_kelas = $this->db->query(
        "SELECT a.*, b.tarif_nm 
        FROM mst_tarif_kelas a 
        JOIN mst_tarif b ON a.tarif_id = b.tarif_id
        WHERE a.tarif_id = '$tarif_admin_ranap' AND kelas_id = '" . $data['kelas_id'] . "'"
      )->row_array();
      $dadmin = array(
        'tindakan_id' => get_id('dat_tindakan'),
        'reg_id' => $d['reg_id'],
        'pasien_id' => $data['pasien_id'],
        'lokasi_id' => $d['lokasi_id'],
        'kelas_id' => $data['kelas_id'],
        'tarif_id' => $tarif_admin_ranap,
        'tarif_nm' => @$tarif_kelas['tarif_nm'],
        'js' => @$tarif_kelas['js'],
        'jp' => @$tarif_kelas['jp'],
        'jb' => @$tarif_kelas['jb'],
        'jb' => @$tarif_kelas['jb'],
        'nom_tarif' => @$tarif_kelas['nominal'],
        'qty' => 1,
        'jml_awal' => @$tarif_kelas['nominal'] * 1,
        'jml_tagihan' => @$tarif_kelas['nominal'] * 1,
        'jenistindakan_cd' => 1,
      );
      $this->db->insert('dat_tindakan', $dadmin);
      update_id('dat_tindakan', $dadmin['tindakan_id']);
    }
  }

  public function update_mst_pasien($data)
  {
    $data_mst_pasien['pasien_id'] = $data['pasien_id'];
    $data_mst_pasien['pasien_nm'] = $data['pasien_nm'];
    $data_mst_pasien['sebutan_cd'] = $data['sebutan_cd'];
    $data_mst_pasien['nik'] = $data['nik'];
    $data_mst_pasien['nama_kk'] = $data['nama_kk'];
    $data_mst_pasien['no_kk'] = $data['no_kk'];
    $data_mst_pasien['alamat'] = $data['alamat'];
    $data_mst_pasien['kode_pos'] = $data['kode_pos'];
    $data_mst_pasien['wilayah_st'] = $data['wilayah_st'];
    $data_mst_pasien['provinsi'] = $data['provinsi'];
    $data_mst_pasien['kabupaten'] = $data['kabupaten'];
    $data_mst_pasien['kecamatan'] = $data['kecamatan'];
    $data_mst_pasien['kelurahan'] = $data['kelurahan'];
    $data_mst_pasien['wilayah_id'] = $data['wilayah_id'];
    $data_mst_pasien['tmp_lahir'] = $data['tmp_lahir'];
    $data_mst_pasien['tgl_lahir'] = $data['tgl_lahir'];
    $data_mst_pasien['sex_cd'] = $data['sex_cd'];
    $data_mst_pasien['goldarah_cd'] = $data['goldarah_cd'];
    $data_mst_pasien['pendidikan_cd'] = $data['pendidikan_cd'];
    $data_mst_pasien['pekerjaan_cd'] = $data['pekerjaan_cd'];
    $data_mst_pasien['agama_cd'] = $data['agama_cd'];
    $data_mst_pasien['telp'] = $data['no_telp'];
    $data_mst_pasien['jenispasien_id'] = $data['jenispasien_id'];
    $data_mst_pasien['no_kartu'] = $data['no_kartu'];
    $data_mst_pasien['pasien_st'] = 1;
    $data_mst_pasien['tgl_catat'] = date('Y-m-d');
    $data_mst_pasien['created_at'] = date('Y-m-d H:i:s');
    $data_mst_pasien['created_by'] = $this->session->userdata('sess_user_realname');
    $this->db->where('pasien_id', $data['pasien_id'])->update('mst_pasien', $data_mst_pasien);
  }
}
