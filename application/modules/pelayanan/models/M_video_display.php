<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_video_display extends CI_Model {

	public function where($cookie)
	{
		$where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND a.info_nm LIKE '%".$this->db->escape_like_str($cookie['search']['term'])."%' ";
		}
		return $where;
	}

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT * FROM lkt_antrian_info a 
            $where
            ORDER BY "
              .$cookie['order']['field']." ".$cookie['order']['type'].
            " LIMIT ".$cookie['cur_page'].",".$cookie['per_page'];
    $query = $this->db->query($sql);
    $result = $query->result_array();

    foreach ($result as $key => $val) {
      $result[$key]['list_video'] = $this->list_video($val);
    }

    return $result;
  }

  public function list_video($data)
  {
    $result = '';
    if ($data['file_media_01'] !='') {
      $result .= '<a href="'.base_url().'assets/videos/'.$data['file_media_01'].'" target="_blank" class="btn btn-table btn-danger" style="margin: 2px;" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-danger" data-original-title="Play Video 01"><i class="fas fa-play"></i> Video 01</a>';
    }
    if ($data['file_media_02'] !='') {
      $result .= '<a href="'.base_url().'assets/videos/'.$data['file_media_02'].'" target="_blank" class="btn btn-table btn-danger" style="margin: 2px;" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-danger" data-original-title="Play Video 02"><i class="fas fa-play"></i> Video 02</a>';
    }
    if ($data['file_media_03'] !='') {
      $result .= '<a href="'.base_url().'assets/videos/'.$data['file_media_03'].'" target="_blank" class="btn btn-table btn-danger" style="margin: 2px;" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-danger" data-original-title="Play Video 03"><i class="fas fa-play"></i> Video 03</a>';
    }
    if ($data['file_media_04'] !='') {
      $result .= '<a href="'.base_url().'assets/videos/'.$data['file_media_04'].'" target="_blank" class="btn btn-table btn-danger" style="margin: 2px;" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-danger" data-original-title="Play Video 04"><i class="fas fa-play"></i> Video 04</a>';
    }
    if ($data['file_media_05'] !='') {
      $result .= '<a href="'.base_url().'assets/videos/'.$data['file_media_05'].'" target="_blank" class="btn btn-table btn-danger" style="margin: 2px;" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-danger" data-original-title="Play Video 05"><i class="fas fa-play"></i> Video 05</a>';
    }
    return $result;
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM lkt_antrian_info a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM lkt_antrian_info a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id) {
    $sql = "SELECT * FROM lkt_antrian_info WHERE info_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  function get_id() {
    $sql = "SELECT MAX(info_id) AS max_info_id FROM lkt_antrian_info";
    $query = $this->db->query($sql);
    $check = $query->row_array();
    if ($check['max_info_id'] == '') {
      $data['info_id'] = '001';
    }else{
      $info_id = $check['max_info_id']+1;
      $data['info_id'] = sprintf("%03d", $info_id);
    }
    return $data['info_id'];
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    // unset data
    unset($data['file_media_01'], $data['file_media_02'], $data['file_media_03'], $data['file_media_04'], $data['file_media_05'], $_FILES['file_media_01'], $_FILES['file_media_02'], $_FILES['file_media_03'], $_FILES['file_media_04'], $_FILES['file_media_05']);
    if ($id == null) {
      $data['periode_tgl_awal'] = to_date($data['periode_tgl_awal']);
      $data['periode_tgl_akhir'] = to_date($data['periode_tgl_akhir']);
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('lkt_antrian_info', $data);
    }else{
      $data['periode_tgl_awal'] = to_date($data['periode_tgl_awal']);
      $data['periode_tgl_akhir'] = to_date($data['periode_tgl_akhir']);
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('info_id', $id)->update('lkt_antrian_info', $data);
    }
  }

  public function insert_video($name_file='', $info_id='') {
    $data = $_POST;

    $path_dir = "assets/videos/";
    $tmp_name = @$_FILES[$name_file]['tmp_name'];
    $video_name = upload_move_video($path_dir, $tmp_name, @$_FILES[$name_file]['name']);
    $result = '';
    $data[$name_file] = $video_name;
    $check = $this->get_data($info_id);
    // unlink video
    if ($check[$name_file] !='') {
      $path_dir = "assets/videos/";
      unlink($path_dir . $check[$name_file]);
    }
    //
    if ($check['info_id'] == $info_id) {
      $this->db->where('info_id', $info_id)->update('lkt_antrian_info', $data);
    }else{
      $data['info_id'] = $info_id;
      $this->db->insert('lkt_antrian_info', $data);
    }
    return $result;
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('info_id',$id)->update('lkt_antrian_info', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $get_data = $this->get_data($id);
      // unlink
      $path_dir = "assets/videos/";
      unlink($path_dir . $get_data['file_media_01']);
      unlink($path_dir . $get_data['file_media_02']);
      unlink($path_dir . $get_data['file_media_03']);
      unlink($path_dir . $get_data['file_media_04']);
      unlink($path_dir . $get_data['file_media_05']);
      //
      $this->db->where('info_id', $id)->delete('lkt_antrian_info');
    }else{
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('info_id', $id)->update('lkt_antrian_info', $data);
    }
  }

  public function delete_video($name=null, $info_id=null) {
    $get_data = $this->get_data($info_id);
    // unlink
    $path_dir = "assets/videos/";
    $result = unlink($path_dir . $get_data[$name]);
    // update
    $data[$name] = '';
    $this->db->where('info_id',$info_id)->update('lkt_antrian_info', $data);
    return $result;
  }
  
}