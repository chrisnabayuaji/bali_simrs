<!-- js -->
<?php $this->load->view('pelayanan/caller_antrian/_js')?>
<!-- / -->

<form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?=$form_action?>" class="needs-validation" novalidate>
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline col-title">
          <div class="col-back">
            <a href="<?=site_url($nav['nav_module'].'/dashboard')?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
          </div>
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            <?=$nav['nav_nm']?>
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?=site_url('app/dashboard')?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?=site_url($module_nav['nav_url'])?>"><i class="fas fa-folder-open"></i> <?=$module_nav['nav_nm']?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-user-injured"></i> Registrasi</a></li>
            <li class="breadcrumb-item"><a href="<?=site_url($nav['nav_url'])?>"><?=$nav['nav_nm']?></a></li>
            <li class="breadcrumb-item active"><span>Caller</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>

    <div class="row mt-4">
      <div class="col"></div>
      <div class="col-lg-5 grid-margin stretch-card">
        <div class="card border-none mt-3">
          <div class="card-body card-shadow">
            <div id="antrian_container">
              <h4 class="card-title border-bottom border-2 pb-2 mb-3">Pemanggilan Antrian (<?=@$main['lokasi_nm']?>)</h4>
              <h1 id="now_antrian" class="text-center text-primary" style="font-size:5rem">-</h1>
              <h6 id="total_antrian" class="text-center">Antrian Terlayani : <?=@$main['antrian_cd'].'.'.@$main['call_antrian_no']?>/<?=@$main['antrian_cd']?>.<?=@$main['antrian_no']?></h6>
              <input id="lokasi_id" type="hidden" value="<?=$id?>">
              <input id="antrian" type="hidden" value="<?=@$main['antrian_cd']?>.<?=@$main['call_antrian_no']?>">
              <div class="row mt-3">
                <div class="col"></div>
                <div class="col">
                  <div id="tanggal" class="alert alert-success text-center p-1" role="alert"><?=date('d-m-Y')?></div>
                </div>
                <div class="col"></div>
              </div>
              <div class="form-group row mb-2">
                <label class="col-lg-5 col-md-3 col-form-label">Loket</label>
                <div class="col-lg-3 col-md-3">
                  <select class="form-control chosen-select" name="loket" id="loket">
                    <?php for($i=1;$i<=9;$i++):?>
                      <option value="<?=$i?>"><?=$i?></option>
                    <?php endfor;?>
                  </select>
                </div>
              </div>
              <div class="row">
                <div class="col-5">
                  <button type="button" id="btn_recall_antrian" class="btn-call btn btn-warning btn-block" onclick="recall_antrian()"><i class="fas fa-sync-alt"></i> Panggil Ulang</button>
                </div>
                <div class="col-7">
                  <button type="button" id="btn_call_antrian" class="btn-call btn btn-primary btn-block" onclick="call_antrian()" disabled>-</button>
                </div>
              </div>
              <div class="row mt-3">
                <div class="col">
                  <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#random_modal"><i class="fas fa-random"></i> Panggil Acak</button>
                </div>
              </div>
            </div>
            <div id="belum_container">
              Belum ada antrian hari ini
            </div>
          </div>
        </div>
      </div>
      <div class="col"></div>
    </div>
  </div>
</form>
<!-- Modal -->
<div class="modal fade" id="random_modal" tabindex="-1" role="dialog" aria-labelledby="random_modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="random_modalLabel">Pemanggilan Acak</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" style="font-size:20px"><?=@$main['antrian_cd']?>.</span>
            <input type="hidden" name="random_antrian_cd" id="random_antrian_cd"  value="<?=@$main['antrian_cd']?>">
          </div>
          <input id="random_antrian_no" type="number" class="form-control" style="font-size:20px" placeholder="Nomor">
          <div class="input-group-append">
            <button id="btn-recall" type="button" class="btn btn- btn-primary" onclick="random_antrian()"><i class="fas fa-headset"></i> Panggil</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>