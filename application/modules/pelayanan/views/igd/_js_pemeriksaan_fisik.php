<script type="text/javascript">
  var pemeriksaan_fisik_form;
  $(document).ready(function() {
    var pemeriksaan_fisik_form = $("#pemeriksaan_fisik_form").validate({
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $("#pemeriksaan_fisik_action").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#pemeriksaan_fisik_action").attr("disabled", "disabled");
        $("#pemeriksaan_fisik_cancel").attr("disabled", "disabled");
        var pasien_id = $("#pasien_id").val();
        var reg_id = $("#reg_id").val();
        var lokasi_id = $("#lokasi_id").val();
        $.ajax({
          type: 'post',
          url: '<?= site_url($nav['nav_url']) ?>/ajax_pemeriksaan_fisik/save',
          data: $(form).serialize() + '&pasien_id=' + pasien_id + '&reg_id=' + reg_id + '&lokasi_id=' + lokasi_id,
          success: function(data) {
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil disimpan.',
              icon: 'success',
              position: 'top-right'
            })
            $("#pemeriksaan_fisik_action").html('<i class="fas fa-save"></i> Simpan');
            $("#pemeriksaan_fisik_action").attr("disabled", false);
            $("#pemeriksaan_fisik_cancel").attr("disabled", false);
            pemeriksaan_fisik_data();
          }
        })
        $(form).submit(function(e) {
          return false;
        });
      }
    });

    var pasien_id = $("#pasien_id").val();
    var reg_id = $("#reg_id").val();
    var lokasi_id = $("#lokasi_id").val();

    $("#pills-history-pemeriksaan-fisik-tab").on('click', function() {
      pemeriksaan_fisik_history(pasien_id, reg_id, lokasi_id);
    });

    pemeriksaan_fisik_data();

    $(".pemeriksaan_fisik_field").on('change', function() {
      $("#auto-save-status").removeClass('d-none');
      var pasien_id = $("#pasien_id").val();
      var reg_id = $("#reg_id").val();
      var lokasi_id = $("#lokasi_id").val();
      $.ajax({
        type: 'post',
        url: '<?= site_url($nav['nav_url']) ?>/ajax_pemeriksaan_fisik/save',
        data: $("#pemeriksaan_fisik_form").serialize() + '&pasien_id=' + pasien_id + '&reg_id=' + reg_id + '&lokasi_id=' + lokasi_id,
        success: function(data) {
          pemeriksaan_fisik_data();
          $("#auto-save-status").addClass('d-none');
        }
      })
    })

    $('.datetimepicker').daterangepicker({
      startDate: moment("<?= date('Y-m-d H:i:s') ?>"),
      endDate: moment(),
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    // autocomplete
    $('#petugas_id_pemeriksaan_fisik').select2({
      minimumInputLength: 2,
      ajax: {
        url: "<?= site_url($nav['nav_url']) ?>/ajax_pemeriksaan_fisik/autocomplete",
        dataType: "json",

        data: function(params) {
          return {
            pegawai_nm: params.term
          };
        },
        processResults: function(data) {
          return {
            results: data
          }
        }
      }
    });
    $('.select2-container').css('width', '100%');

    $('.modal-href').click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");

      $("#modal-title").html(modal_title);
      $("#modal-size").addClass('modal-' + modal_size);
      if (modal_custom_size) {
        $("#modal-size").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
      }
      $("#myModal").modal('show');
      $.post($(this).data('href'), function(data) {
        $("#modal-body").html(data.html);
      }, 'json');
    });

  })

  function pemeriksaan_fisik_history(pasien_id = '', reg_id = '', lokasi_id = '') {
    $('#pemeriksaan_fisik_history').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_pemeriksaan_fisik/history' ?>', {
      pasien_id: pasien_id,
      reg_id: reg_id,
      lokasi_id: lokasi_id
    }, function(data) {
      $('#pemeriksaan_fisik_history').html(data.html);
    }, 'json');
  }

  function pemeriksaan_fisik_data() {
    var pasien_id = $("#pasien_id").val();
    var reg_id = $("#reg_id").val();
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_pemeriksaan_fisik/data' ?>', {
      reg_id: reg_id,
      pasien_id: pasien_id
    }, function(data) {
      if (data != null) {
        $("#keluhan_utama").val(data.keluhan_utama);
        $("#anamnesis_id").val(data.anamnesis_id);
        $("#systole").val(data.systole);
        $("#diastole").val(data.diastole);
        $("#tinggi").val(data.tinggi);
        $("#berat").val(data.berat);
        $("#suhu").val(data.suhu);
        $("#nadi").val(data.nadi);
        $("#respiration_rate").val(data.respiration_rate);
        $("#sao2").val(data.sao2);

        // autocomplete
        var data2 = {
          id: data.petugas_id,
          text: data.petugas_nm
        };
        var newOption = new Option(data2.text, data2.id, false, false);
        $('#petugas_id_pemeriksaan_fisik').append(newOption).trigger('change');
        $('#petugas_id_pemeriksaan_fisik').val(data.petugas_id);
        $('#petugas_id_pemeriksaan_fisik').trigger('change');
        $('.select2-container').css('width', '100%');
      }
    }, 'json');
  }

  function petugas_fill(id) {
    $.ajax({
      type: 'post',
      url: '<?= site_url($nav['nav_url'] . '/ajax_pemeriksaan_fisik/petugas_fill') ?>',
      dataType: 'json',
      data: 'pegawai_id=' + id,
      success: function(data) {
        // autocomplete
        var data2 = {
          id: data.pegawai_id,
          text: data.pegawai_nm
        };
        var newOption = new Option(data2.text, data2.id, false, false);
        $('#petugas_id_pemeriksaan_fisik').append(newOption).trigger('change');
        $('#petugas_id_pemeriksaan_fisik').val(data.pegawai_id);
        $('#petugas_id_pemeriksaan_fisik').trigger('change');
        $('.select2-container').css('width', '100%');
        $("#myModal").modal('hide');
      }
    })
  }
</script>