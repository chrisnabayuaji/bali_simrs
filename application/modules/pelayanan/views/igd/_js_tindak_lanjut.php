<script type="text/javascript">
  $(document).ready(function() {
    var pasien_id = $("#pasien_id").val();
    var reg_id = $("#reg_id").val();
    $("#tindak_lanjut_pasien_id").val(pasien_id);
    $("#tindak_lanjut_reg_id").val(reg_id);
    tindak_lanjut_data(pasien_id, reg_id);
    reg_pasien_data(pasien_id, reg_id);
    status_pulang_data(pasien_id, reg_id);

    $("#form-data").validate({
      rules: {
        tindaklanjut_cd: {
          valueNotEquals: ""
        },
        lokasi_jenisreg_1: {
          valueNotEquals: ""
        },
        lokasi_jenisreg_2: {
          valueNotEquals: ""
        },
        subtindaklanjut_cd: {
          required: true
        },
        pulang_st: {
          required: true
        },
        tindaklanjut_cd: {
          valueNotEquals: ""
        },
        keadaan_cd: {
          valueNotEquals: ""
        },
        carapulang_cd: {
          valueNotEquals: ""
        },
      },
      messages: {
        tindaklanjut_cd: {
          valueNotEquals: "Pilih salah satu!"
        },
        lokasi_jenisreg_1: {
          valueNotEquals: "Pilih salah satu!"
        },
        lokasi_jenisreg_2: {
          valueNotEquals: "Pilih salah satu!"
        },
        subtindaklanjut_cd: {
          valueNotEquals: "Pilih salah satu!"
        },
        tindaklanjut_cd: {
          valueNotEquals: "Pilih salah satu!"
        },
        keadaan_cd: {
          valueNotEquals: "Pilih salah satu!"
        },
        carapulang_cd: {
          valueNotEquals: "Pilih salah satu!"
        },
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-n1');
        } else if ($(element).hasClass('form-check-input')) {
          error.insertAfter(element.next(".input-helper")).addClass('mt-2 ml-n3');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    $('.modal-href').click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");

      $("#modal-title").html(modal_title);
      $("#modal-size").addClass('modal-' + modal_size);
      if (modal_custom_size) {
        $("#modal-size").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
      }
      $("#myModal").modal('show');
      $.post($(this).data('href'), function(data) {
        $("#modal-body").html(data.html);
      }, 'json');
    });

    $("#tgl_pulang").daterangepicker({
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: "Clear",
        format: "DD-MM-YYYY H:mm:ss",
      },
      isInvalidDate: function(date) {
        return "";
      },
    });

    $('#tgl_kontrol').daterangepicker({
      // maxDate: new Date(),
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })

    $('#tindaklanjut_cd').bind('change', function(e) {
      e.preventDefault();
      var i = $(this).val();
      if (i == '01') {
        $('#label-hide').removeClass('d-none').html('Lokasi <span class="text-danger">*</span>');
        $('#lokasi-jenisreg-2').addClass('d-none');
        $('#lokasi_jenisreg_2').val('').trigger('change');
        $('#rs-rujukan').addClass('d-none');
        $('#rsrujukan_id').val('').trigger('change');
        $('#btn-rs-rujukan').addClass('d-none');
        $('#lokasi-jenisreg-1').removeClass('d-none');
        $('#col-subtindaklanjut-cd').removeClass('d-none');
      } else if (i == '02') {
        $('#label-hide').removeClass('d-none').html('Lokasi <span class="text-danger">*</span>');
        $('#lokasi-jenisreg-1').addClass('d-none');
        $('#lokasi_jenisreg_1').val('').trigger('change');
        $('#col-subtindaklanjut-cd').addClass('d-none');
        $('.subtindaklanjut_cd').prop('checked', false);
        $('#rs-rujukan').addClass('d-none');
        $('#rsrujukan_id').val('').trigger('change');
        $('#btn-rs-rujukan').addClass('d-none');
        $('#lokasi-jenisreg-2').removeClass('d-none');
      } else if (i == '03') {
        $('#label-hide').removeClass('d-none').html('RS Rujukan <span class="text-danger">*</span>');
        $('#lokasi-jenisreg-1').addClass('d-none');
        $('#lokasi_jenisreg_1').val('').trigger('change');
        $('#lokasi-jenisreg-2').addClass('d-none');
        $('#lokasi_jenisreg_2').val('').trigger('change');
        $('#col-subtindaklanjut-cd').addClass('d-none');
        $('.subtindaklanjut_cd').prop('checked', false);
        $('#rs-rujukan').removeClass('d-none');
        $('#btn-rs-rujukan').removeClass('d-none');
      } else {
        $('#label-hide').addClass('d-none');
        $('#lokasi-jenisreg-1').addClass('d-none');
        $('#lokasi_jenisreg_1').val('').trigger('change');
        $('#lokasi-jenisreg-2').addClass('d-none');
        $('#lokasi_jenisreg_2').val('').trigger('change');
        $('#col-subtindaklanjut-cd').addClass('d-none');
        $('.subtindaklanjut_cd').prop('checked', false);
        $('#rs-rujukan').addClass('d-none');
        $('#rsrujukan_id').val('').trigger('change');
        $('#btn-rs-rujukan').addClass('d-none');
      }
    })

    $('.pulang_st').bind('change', function(e) {
      e.preventDefault();
      var i = $(this).val();
      if (i == '1') {
        $('#col-pulang').removeClass('d-none');
      } else {
        $('#col-pulang').addClass('d-none');
      }
    })

  })

  function tindak_lanjut_data(pasien_id = '', reg_id = '') {
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_tindak_lanjut/tindak_lanjut_data' ?>', {
      pasien_id: pasien_id,
      reg_id: reg_id
    }, function(data) {
      if (data.main != null) {
        $("#tindaklanjut_cd").val(data.main.tindaklanjut_cd).trigger('change').removeClass("is-valid").removeClass("is-invalid");
        if (data.main.tindaklanjut_cd == '01') {
          $("#lokasi_jenisreg_1").val(data.main.lokasi_id).trigger('change').removeClass("is-valid").removeClass("is-invalid");
          $("input[name=subtindaklanjut_cd][value=" + data.main.subtindaklanjut_cd + "]").prop('checked', true);
        } else if (data.main.tindaklanjut_cd == '02') {
          $("#lokasi_jenisreg_2").val(data.main.lokasi_id).trigger('change').removeClass("is-valid").removeClass("is-invalid");
        } else if (data.main.tindaklanjut_cd == '03') {
          $("#rsrujukan_id").val(data.main.rsrujukan_id).trigger('change').removeClass("is-valid").removeClass("is-invalid");
        }
        $("#catatan_tindaklanjut").val(data.main.catatan_tindaklanjut).removeClass("is-valid").removeClass("is-invalid");
      }
    }, 'json');
  }

  function reg_pasien_data(pasien_id = '', reg_id = '') {
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_tindak_lanjut/reg_pasien_data' ?>', {
      pasien_id: pasien_id,
      reg_id: reg_id
    }, function(data) {
      $("input[name=pulang_st][value=" + data.main.pulang_st + "]").prop('checked', true);
      if (data.main.pulang_st == '1') {
        $('#col-pulang').removeClass('d-none');
      }
    }, 'json');
  }

  function status_pulang_data(pasien_id = '', reg_id = '') {
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_tindak_lanjut/status_pulang_data' ?>', {
      pasien_id: pasien_id,
      reg_id: reg_id
    }, function(data) {
      if (data.main != null) {
        if (data.main.tgl_pulang === '' || data.main.tgl_pulang === null || data.main.tgl_pulang === '0000-00-00 00:00:00') {

        } else {
          $('#tgl_pulang').daterangepicker({
            startDate: data.main.tgl_pulang,
            timePicker: true,
            timePicker24Hour: true,
            timePickerSeconds: true,
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
              cancelLabel: 'Clear',
              format: 'DD-MM-YYYY H:mm:ss'
            },
            isInvalidDate: function(date) {
              return '';
            }
          });
        }

        if (data.main.tgl_kontrol === '' || data.main.tgl_kontrol === null || data.main.tgl_kontrol === '0000-00-00') {

        } else {
          $('#tgl_kontrol').daterangepicker({
            startDate: data.main.tgl_kontrol,
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
              cancelLabel: 'Clear',
              format: 'DD-MM-YYYY'
            },
            isInvalidDate: function(date) {
              return '';
            }
          });
        }
        // $("#tgl_pulang").val(data.main.tgl_pulang).removeClass("is-valid").removeClass("is-invalid");
        // $("#tgl_kontrol").val(data.main.tgl_kontrol).removeClass("is-valid").removeClass("is-invalid");
        $("#keadaan_cd").val(data.main.keadaan_cd).trigger('change').removeClass("is-valid").removeClass("is-invalid");
        $("#carapulang_cd").val(data.main.carapulang_cd).trigger('change').removeClass("is-valid").removeClass("is-invalid");
        $("#catatan_dokter").val(data.main.catatan_dokter).removeClass("is-valid").removeClass("is-invalid");
        $("#catatan_keperawatan").val(data.main.catatan_keperawatan).removeClass("is-valid").removeClass("is-invalid");
        $("#catatan_farmasi").val(data.main.catatan_farmasi).removeClass("is-valid").removeClass("is-invalid");
      }
    }, 'json');
  }
</script>