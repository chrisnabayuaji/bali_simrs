<?php if(@$main == null):?>
	<tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else: ?>
	<?php 
    $i=1;
    $tot_biaya = 0;
    foreach($main as $row):
    $tot_biaya += $row['jml_tagihan'];
  ?>
	<tr>
		<td class="text-center align-top"><?=$i++?></td>
    <td class="text-center align-top"><?=$row['tarif_id']?></td>
    <td class="text-left align-top"><?=$row['tarif_nm']?> - <?=$row['kelas_nm']?></td>
    <td class="text-center align-top"><?=$row['qty']?></td>
    <td class="text-center align-top"><?=get_parameter_value('jenistindakan_cd', $row['jenistindakan_cd'])?></td>
    <td class="text-right align-top"><?=num_id($row['jml_tagihan'])?></td>
    <td class="text-left">
      <?=($row['pegawai_nm_1'] !='') ? '<b>Operator/Dokter</b> :<br>- '.$row['pegawai_nm_1'].'<br>' : ''?>
      <?=($row['pegawai_nm_2'] !='') ? '<b>Anastesi</b> :<br>- '.$row['pegawai_nm_2'].'<br>' : ''?>
      <?=($row['pegawai_nm_3'] !='') ? '<b>Perawat Bedah</b> :<br>- '.$row['pegawai_nm_3'].'<br>' : ''?>
      <?=($row['pegawai_nm_4'] !='') ? '<b>Dokter Konsul</b> :<br>- '.$row['pegawai_nm_4'].'<br>' : ''?>
      <?=($row['pegawai_nm_5'] !='') ? '<b>Perawat Lainnya</b> :<br>- '.$row['pegawai_nm_5'].'' : ''?>
    </td>
	</tr>
	<?php endforeach; ?>
  <tr>
    <td colspan="5" class="text-right"><b>Total Biaya</b></td>
    <td class="text-right"><b><?=num_id($tot_biaya)?></b></td>
  </tr>
<?php endif; ?>