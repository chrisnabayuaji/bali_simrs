<?php $this->load->view('_js_bhp'); ?>
<div class="row">
  <div class="col-md-7">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-list"></i> List Data BHP (Barang Habis Pakai)</h4>
        <!-- <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="pills-saat-ini-bhp-tab" data-toggle="pill" href="#pills-saat-ini-bhp" role="tab" aria-controls="pills-saat-ini-bhp" aria-selected="false"><i class="fas fa-copy"></i> Data Saat Ini</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " id="pills-history-bhp-tab" data-toggle="pill" href="#pills-history-bhp" role="tab" aria-controls="pills-history-bhp" aria-selected="true"><i class="fas fa-history"></i> Data History</a>
          </li>
        </ul> -->
        <div class="tab-content p-0" id="pills-tabContent-bhp" style="border:0px !important">
          <div class="tab-pane fade show active" id="pills-saat-ini-bhp" role="tabpanel" aria-labelledby="pills-saat-ini-bhp-tab">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-sm">
                <thead>
                  <tr>
                    <th class="text-center" width="20">No</th>
                    <th class="text-center" width="60">Aksi</th>
                    <th class="text-center">Kode</th>
                    <th class="text-center">Nama BHP</th>
                    <th class="text-center">Qty</th>
                    <th class="text-center">Keterangan</th>
                  </tr>
                </thead>
                <tbody id="bhp_data"></tbody>
              </table>
            </div>
          </div>
          <div class="tab-pane fade" id="pills-history-bhp" role="tabpanel" aria-labelledby="pills-history-bhp-tab">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-sm">
                <thead>
                  <tr>
                    <th class="text-center" width="20">No</th>
                    <th class="text-center">Tanggal</th>
                    <th class="text-center">Kode</th>
                    <th class="text-center">Nama BHP</th>
                    <th class="text-center">Qty</th>
                    <th class="text-center">Keterangan</th>
                    <th class="text-center">Lokasi Pelayanan</th>
                  </tr>
                </thead>
                <tbody id="bhp_history"></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-5">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-box-open"></i> BHP (Barang Habis Pakai)</h4>
        <form id="bhp_form" action="" method="post" autocomplete="off">
          <input type="hidden" name="bhp_id" id="bhp_id">
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Tanggal Catat <span class="text-danger">*</span></label>
            <div class="col-lg-5 col-md-6">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                </div>
                <input type="text" class="form-control datetimepicker" name="tgl_catat" id="tgl_catat" required="" aria-invalid="false">
              </div>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">BHP <span class="text-danger">*</span></label>
            <div class="col-lg-8 col-md-3">
              <select class="form-control select2" name="barang_id" id="barang_id_bhp">
                <option value="">- Pilih -</option>
              </select>
              <input type="hidden" class="form-control" name="stokdepo_id" id="stokdepo_id">
              <input type="hidden" class="form-control" name="stokdepo_id_sebelum" id="stokdepo_id_sebelum">
            </div>
            <div class="col-lg-1 col-md-1">
              <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_bhp/search_bhp' ?>" modal-title="List Data BHP" modal-size="lg" class="btn btn-xs btn-default modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari BHP" data-original-title="Cari BHP"><i class="fas fa-search"></i></a>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Qty <span class="text-danger">*</span></label>
            <div class="col-lg-3 col-md-3">
              <input type="number" class="form-control" name="qty" id="qty" value="1" min="1">
              <input type="hidden" class="form-control" name="qty_sebelum" id="qty_sebelum">
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Keterangan</label>
            <div class="col-lg-9 col-md-3">
              <textarea class="form-control" name="keterangan_bhp" id="keterangan_bhp" value="" rows="5"></textarea>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-9 offset-lg-3 p-0">
              <button type="submit" id="bhp_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
              <button type="button" id="bhp_cancel" class="btn btn-xs btn-secondary"><i class="fas fa-times"></i> Batal</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>