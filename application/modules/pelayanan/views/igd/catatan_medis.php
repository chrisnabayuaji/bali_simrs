<!-- js -->
<?php $this->load->view('_js_catatan_medis'); ?>
<div class="row">
  <div class="col-md-5">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-copy"></i> Catatan Medis</h4>
        <form id="catatan_medis_form" action="" method="post" autocomplete="off">
          <input type="hidden" name="catatanmedis_id" id="catatanmedis_id">
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Tanggal Catat <span class="text-danger">*</span></label>
            <div class="col-lg-5 col-md-5">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                </div>
                <input type="text" class="form-control datetimepicker" name="tgl_catat" id="tgl_catat" required="" aria-invalid="false">
              </div>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Riwayat Penyakit <span class="text-danger">*</span></label>
            <div class="col-lg-9 col-md-3">
              <textarea class="form-control" name="riwayat_penyakit" id="riwayat_penyakit" value="" required="" rows="5"></textarea>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Alergi Obat <span class="text-danger">*</span></label>
            <div class="col-lg-9 col-md-3">
              <textarea class="form-control" name="alergi_obat" id="alergi_obat" value="" required="" rows="5"></textarea>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Petugas</label>
            <div class="col-lg-8 col-md-3">
              <select class="form-control select2" name="petugas_id" id="petugas_id_catatan_medis">
                <option value="">---</option>
              </select>
            </div>
            <div class="col-lg-1 col-md-1">
              <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_catatan_medis/search_petugas' ?>" modal-title="List Data Petugas" modal-size="lg" class="btn btn-xs btn-default modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Petugas" data-original-title="Cari Petugas"><i class="fas fa-search"></i></a>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-9 offset-lg-3 p-0">
              <button type="submit" id="catatan_medis_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
              <button type="button" id="catatan_medis_cancel" class="btn btn-xs btn-secondary"><i class="fas fa-times"></i> Batal</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-7">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-list"></i> List Data Catatan Medis</h4>
        <!-- <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="pills-saat-ini-catatan-medis-tab" data-toggle="pill" href="#pills-saat-ini-catatan-medis" role="tab" aria-controls="pills-saat-ini-catatan-medis" aria-selected="false"><i class="fas fa-copy"></i> Data Saat Ini</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " id="pills-history-catatan-medis-tab" data-toggle="pill" href="#pills-history-catatan-medis" role="tab" aria-controls="pills-history-catatan-medis" aria-selected="true"><i class="fas fa-history"></i> Data History</a>
          </li>
        </ul> -->
        <div class="tab-content p-0" id="pills-tabContent-catatan-medis" style="border:0px !important">
          <div class="tab-pane fade show active" id="pills-saat-ini-catatan-medis" role="tabpanel" aria-labelledby="pills-saat-ini-catatan-medis-tab">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-sm">
                <thead>
                  <tr>
                    <th class="text-center" width="20">No</th>
                    <th class="text-center" width="60">Aksi</th>
                    <th class="text-center" width="135">Tanggal</th>
                    <th class="text-center">Riwayat Penyakit</th>
                    <th class="text-center">Alergi Obat</th>
                    <th class="text-center">Petugas</th>
                  </tr>
                </thead>
                <tbody id="catatan_medis_data"></tbody>
              </table>
            </div>
          </div>
          <div class="tab-pane fade" id="pills-history-catatan-medis" role="tabpanel" aria-labelledby="pills-history-catatan-medis-tab">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-sm">
                <thead>
                  <tr>
                    <th class="text-center" width="20">No</th>
                    <th class="text-center" width="135">Tanggal</th>
                    <th class="text-center">Riwayat Penyakit</th>
                    <th class="text-center">Alergi Obat</th>
                    <th class="text-center">Lokasi Pelayanan</th>
                  </tr>
                </thead>
                <tbody id="catatan_medis_history"></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>