<?php if (@$main == null) : ?>
	<tr>
		<td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
	</tr>
<?php else : ?>
	<?php $i = 1;
	foreach ($main as $row) : ?>
		<tr>
			<td class="text-center"><?= $i++ ?></td>
			<td class="text-center"><?= to_date($row['tgl_catat'], '', 'full_date') ?></td>
			<td class="text-left"><?= $row['riwayat_penyakit'] ?></td>
			<td class="text-left"><?= $row['alergi_obat'] ?></td>
			<td class="text-center"><?= $row['lokasi_nm'] ?></td>
		</tr>
	<?php endforeach; ?>
<?php endif; ?>