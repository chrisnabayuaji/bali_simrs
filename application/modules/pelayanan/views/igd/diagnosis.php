<?php $this->load->view('_js_diagnosis'); ?>
<div class="row">
  <div class="col-md-5">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-copy"></i> Diagnosis</h4>
        <form id="diagnosis_form" action="" method="post" autocomplete="off">
          <input type="hidden" name="diagnosis_id" id="diagnosis_id">
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Tanggal Catat <span class="text-danger">*</span></label>
            <div class="col-lg-5 col-md-6">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                </div>
                <input type="text" class="form-control datetimepicker" name="tgl_catat" id="tgl_catat" required="" aria-invalid="false">
              </div>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Diagnosis</label>
            <div class="col-lg-8 col-md-3">
              <select class="form-control select2" name="penyakit_id" id="penyakit_id">
                <option value="">---</option>
              </select>
            </div>
            <div class="col-lg-1 col-md-1">
              <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_diagnosis/search_diagnosis' ?>" modal-title="List Data Penyakit" modal-size="lg" class="btn btn-xs btn-default modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Diagnosis" data-original-title="Cari Diagnosis"><i class="fas fa-search"></i></a>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Kasus <span class="text-danger">*<span></label>
            <div class="col-lg-4 col-md-9">
              <select class="form-control chosen-select" name="jenisdiagnosis_cd" id="jenisdiagnosis_cd">
                <?php foreach (get_parameter('jenisdiagnosis_cd') as $r) : ?>
                  <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['jenisdiagnosis_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <label class="col-lg-2 col-md-3 col-form-label">Tipe</label>
            <div class="col-lg-3 col-md-9">
              <select class="form-control chosen-select" name="tipediagnosis_cd" id="tipediagnosis_cd">
                <option value="">---</option>
                <?php foreach (get_parameter('tipediagnosis_cd') as $r) : ?>
                  <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['tipediagnosis_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Keterangan</label>
            <div class="col-lg-9 col-md-3">
              <textarea class="form-control" name="keterangan_diagnosis" id="keterangan_diagnosis" value="" rows="5"></textarea>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-9 offset-lg-3 p-0">
              <button type="submit" id="diagnosis_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
              <button type="button" id="diagnosis_cancel" class="btn btn-xs btn-secondary"><i class="fas fa-times"></i> Batal</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-7">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-list"></i> List Data Diagnosis</h4>
        <!-- <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="pills-saat-ini-diagnosis-tab" data-toggle="pill" href="#pills-saat-ini-diagnosis" role="tab" aria-controls="pills-saat-ini-diagnosis" aria-selected="false"><i class="fas fa-copy"></i> Data Saat Ini</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " id="pills-history-diagnosis-tab" data-toggle="pill" href="#pills-history-diagnosis" role="tab" aria-controls="pills-history-diagnosis" aria-selected="true"><i class="fas fa-history"></i> Data History</a>
          </li>
        </ul> -->
        <div class="tab-content p-0" id="pills-tabContent-diagnosis" style="border:0px !important">
          <div class="tab-pane fade show active" id="pills-saat-ini-diagnosis" role="tabpanel" aria-labelledby="pills-saat-ini-diagnosis-tab">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-sm">
                <thead>
                  <tr>
                    <th class="text-center" width="20">No</th>
                    <th class="text-center" width="60">Aksi</th>
                    <th class="text-center">Kode</th>
                    <th class="text-center">Diagnosis</th>
                    <th class="text-center">Kasus</th>
                    <th class="text-center">Tipe</th>
                    <th class="text-center">Keterangan</th>
                  </tr>
                </thead>
                <tbody id="diagnosis_data"></tbody>
              </table>
            </div>
          </div>
          <div class="tab-pane fade" id="pills-history-diagnosis" role="tabpanel" aria-labelledby="pills-history-diagnosis-tab">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-sm">
                <thead>
                  <tr>
                    <th class="text-center" width="20">No</th>
                    <th class="text-center">Tanggal</th>
                    <th class="text-center">Kode</th>
                    <th class="text-center">Diagnosis</th>
                    <th class="text-center">Kasus</th>
                    <th class="text-center">Tipe</th>
                    <th class="text-center">Keterangan</th>
                    <th class="text-center">Lokasi Pelayanan</th>
                  </tr>
                </thead>
                <tbody id="diagnosis_history"></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>