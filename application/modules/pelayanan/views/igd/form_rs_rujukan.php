<!-- js -->
<?php $this->load->view('pelayanan/igd/_js_form_rs_rujukan')?>
<!-- / -->
<form id="rs_rujukan_form" action="" method="post" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Kode <span class="text-danger">*<span></label>
        <div class="col-lg-3 col-md-5">
          <input type="text" class="form-control" name="rsrujukan_kode" id="rsrujukan_kode" required="" maxlength="3">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Nama RS Rujukan <span class="text-danger">*<span></label>
        <div class="col-lg-7 col-md-9">
          <input type="text" class="form-control" name="rsrujukan_nm" id="rsrujukan_nm" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Alamat <span class="text-danger">*<span></label>
        <div class="col-lg-8 col-md-8">
          <input type="text" class="form-control" name="rsrujukan_alamat" id="rsrujukan_alamat" required="">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Status</label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="wilayah_st" id="wilayah_st">
            <option value="D">Dalam Wilayah</option>
            <option value="L">Luar Wilayah</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" id="rs_rujukan_cancel" class="btn btn-xs btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" id="rs_rujukan_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>