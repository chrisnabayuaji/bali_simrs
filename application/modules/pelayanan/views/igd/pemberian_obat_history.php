<?php if(@$main == null):?>
  <tr>
    <th class="text-center" width="20">No</th>
    <th class="text-center">Kode</th>
    <th class="text-center">Nama Obat</th>
    <th class="text-center">Jenis Resep</th>
    <th class="text-center">Dosis</th>
    <th class="text-center">Qty</th> 
    <th class="text-center">Cara Pakai</th>
    <th class="text-center">Keterangan</th>
  </tr>
  <tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else: ?>
  <?php foreach($main as $row):?>
  <tr bgcolor="#f5f5f5">
    <td class="text-left" colspan="99"><b>NO.RESEP : <?=$row['resepgroup_id']?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; TANGGAL : <?=to_date($row['tgl_catat'], '' , 'date')?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; LOKASI PELAYANAN : <?=$row['lokasi_nm']?></b></td>
  </tr>
  <tr>
    <th class="text-center" width="20">No</th>
    <th class="text-center">Kode</th>
    <th class="text-center">Nama Obat</th>
    <th class="text-center">Jenis Resep</th>
    <th class="text-center">Dosis</th>
    <th class="text-center">Qty</th> 
    <th class="text-center">Cara Pakai</th>
    <th class="text-center">Keterangan</th>
  </tr>
    <?php $i=1;foreach ($row['list_dat_resep'] as $dat_resep): ?>
      <tr>
        <td class="text-center" width="20"><?=$i++?></td>
        <td class="text-center"><?=$dat_resep['obat_id']?></td>
        <td class="text-left"><?=$dat_resep['obat_nm']?></td>
        <td class="text-center"><?php echo ($dat_resep['jenisresep_cd'] == 1) ? 'Dalam' : 'Luar' ; ?></td>
        <td class="text-left"><?=$dat_resep['dosis']?></td>
        <td class="text-center"><?=$dat_resep['qty']?></td>
        <td class="text-center"><?=get_parameter_value('carapakai_cd', $dat_resep['carapakai_cd'])?></td>
        <td class="text-left"><?=$dat_resep['keterangan_resep']?></td>
      </tr>
   <?php endforeach; ?>
  <?php endforeach; ?>
<?php endif; ?>