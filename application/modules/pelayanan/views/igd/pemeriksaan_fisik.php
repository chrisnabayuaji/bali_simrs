<!-- js -->
<?php $this->load->view('_js_pemeriksaan_fisik'); ?>
<div class="row">
  <div class="col-7">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-stethoscope"></i> Pemeriksaan Fisik</h4>
        <!-- <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="pills-saat-ini-pemeriksaan-fisik-tab" data-toggle="pill" href="#pills-saat-ini-pemeriksaan-fisik" role="tab" aria-controls="pills-saat-ini-pemeriksaan-fisik" aria-selected="false"><i class="fas fa-copy"></i> Data Saat Ini</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " id="pills-history-pemeriksaan-fisik-tab" data-toggle="pill" href="#pills-history-pemeriksaan-fisik" role="tab" aria-controls="pills-history-pemeriksaan-fisik" aria-selected="true"><i class="fas fa-history"></i> Data History</a>
          </li>
        </ul> -->
        <!-- <div id="auto-save-status" class="float-right d-none" style="margin-top: -35px;">
          Proses simpan otomatis <i class="fas fa-spin fa-spinner"></i>
        </div> -->
        <!-- <div class="text-danger mb-2">
          <i class="fas fa-info-circle"></i> Proses ini disimpan secara otomatis ketika sudah selesai mengetik
        </div> -->
        <div class="tab-content p-0" id="pills-tabContent-pemeriksaan-fisik" style="border:0px !important">
          <div class="tab-pane fade show active" id="pills-saat-ini-pemeriksaan-fisik" role="tabpanel" aria-labelledby="pills-saat-ini-pemeriksaan-fisik-tab">
            <form id="pemeriksaan_fisik_form" action="" method="post" autocomplete="off">
              <input type="hidden" name="anamnesis_id" id="anamnesis_id">
              <div class="form-group row mb-2">
                <label class="col-lg-3 col-md-3 col-form-label">Keluhan Utama <span class="text-danger">*</span></label>
                <div class="col-lg-9 col-md-3">
                  <textarea class="form-control" name="keluhan_utama" id="keluhan_utama" value="" required="" rows="5"></textarea>
                </div>
              </div>
              <div class="form-group row mb-2">
                <label class="col-lg-3 col-md-3 col-form-label">Systole <span class="text-danger">*</span></label>
                <div class="col-lg-2 col-md-3">
                  <input class="form-control" type="text" name="systole" id="systole" value="" />
                </div>
                <label class="col-lg-2 col-md-3 col-form-label">Diastole <span class="text-danger">*</span></label>
                <div class="col-lg-2 col-md-3">
                  <input class="form-control" type="text" name="diastole" id="diastole" value="" />
                </div>
              </div>
              <div class="form-group row mb-2">
                <label class="col-lg-3 col-md-3 col-form-label">Tinggi <span class="text-danger">*</span></label>
                <div class="col-lg-2 col-md-3">
                  <input class="form-control" type="text" name="tinggi" id="tinggi" value="" />
                </div>
                <label class="col-lg-2 col-md-3 col-form-label">Berat <span class="text-danger">*</span></label>
                <div class="col-lg-2 col-md-3">
                  <input class="form-control" type="text" name="berat" id="berat" value="" />
                </div>
              </div>
              <div class="form-group row mb-2">
                <label class="col-lg-3 col-md-3 col-form-label">Suhu <span class="text-danger">*</span></label>
                <div class="col-lg-2 col-md-3">
                  <input class="form-control" type="text" name="suhu" id="suhu" value="" />
                </div>
                <label class="col-lg-2 col-md-3 col-form-label">Nadi <span class="text-danger">*</span></label>
                <div class="col-lg-2 col-md-3">
                  <input class="form-control" type="text" name="nadi" id="nadi" value="" />
                </div>
              </div>
              <div class="form-group row mb-2">
                <label class="col-lg-3 col-md-3 col-form-label">Respiration Rate <span class="text-danger">*</span></label>
                <div class="col-lg-2 col-md-3">
                  <input class="form-control" type="text" name="respiration_rate" id="respiration_rate" value="" />
                </div>
              </div>
              <div class="form-group row mb-2">
                <label class="col-lg-3 col-md-3 col-form-label">SpO<sub>2</sub> <span class="text-danger">*</span></label>
                <div class="col-lg-2 col-md-3">
                  <input class="form-control" type="text" name="sao2" id="sao2" value="" />
                </div>
              </div>
              <div class="form-group row mb-2">
                <label class="col-lg-3 col-md-3 col-form-label">Petugas</label>
                <div class="col-lg-5 col-md-3">
                  <select class="form-control select2" name="petugas_id" id="petugas_id_pemeriksaan_fisik">
                    <option value="">---</option>
                  </select>
                </div>
                <div class="col-lg-1 col-md-1">
                  <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_pemeriksaan_fisik/search_petugas' ?>" modal-title="List Data Petugas" modal-size="lg" class="btn btn-xs btn-default modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Petugas" data-original-title="Cari Petugas"><i class="fas fa-search"></i></a>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-9 offset-lg-3 p-0">
                  <button type="submit" id="pemeriksaan_fisik_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
                </div>
              </div>
            </form>
          </div>
          <div class="tab-pane fade" id="pills-history-pemeriksaan-fisik" role="tabpanel" aria-labelledby="pills-history-pemeriksaan-fisik-tab">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-sm">
                <thead>
                  <tr>
                    <th class="text-center" width="20">No</th>
                    <th class="text-left" colspan="2">Uraian Pemeriksaan Fisik</th>
                  </tr>
                </thead>
                <tbody id="pemeriksaan_fisik_history"></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>