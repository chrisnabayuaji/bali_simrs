<?php $this->load->view('_js_penunjang'); ?>
<div class="row">
  <div class="col-3">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-edit"></i> Order Penunjang</h4>
        <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_penunjang_laboratorium/modal/' . $reg['reg_id'] ?>" modal-tab="laboratorium" modal-title="Order Laboratorium" modal-size="lg" class="btn btn-sm btn-default btn-block modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Order Laboratorium" data-original-title="Order Laboratorium"><i class="fas fa-flask"></i> Order Laboratorium</a>
        <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_penunjang_radiologi/modal/' . $reg['reg_id'] ?>" modal-tab="radiologi" modal-title="Order Permintaan Radiologi" modal-size="lg" class="btn btn-sm btn-default btn-block modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Order Radiologi" data-original-title="Order Radiologi"><i class="fas fa-bone"></i> Order Radiologi</a>
        <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_penunjang_bedah_sentral/modal/' . $reg['reg_id'] ?>" modal-tab="bedah_sentral" modal-title="Order Bedah Sentral" modal-size="lg" class="btn btn-sm btn-default btn-block modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Order Bedah Sentral" data-original-title="Order Bedah Sentral"><i class="fas fa-user-md"></i> Order Bedah Sentral</a>
        <!-- <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_penunjang_vk/modal/' . $reg['reg_id'] ?>" modal-tab="vk" modal-title="Order VK" modal-size="md" class="btn btn-sm btn-default btn-block modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Order Bedah Sentral" data-original-title="Order VK"><i class="fas fa-baby"></i> Order VK</a>
        <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_penunjang_icu/modal/' . $reg['reg_id'] ?>" modal-tab="icu" modal-title="Order ICU/PICU/NICU" modal-size="md" class="btn btn-sm btn-default btn-block modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Order Bedah Sentral" data-original-title="Order ICU/PICU/NICU"><i class="fas fa-procedures"></i> Order ICU/PICU/NICU</a>
        <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_penunjang_hemodialisa/modal/' . $reg['reg_id'] ?>" modal-tab="hemodialisa" modal-title="Order Hemodialisa" modal-size="md" class="btn btn-sm btn-default btn-block modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Order Bedah Sentral" data-original-title="Order Hemodialisa"><i class="fas fa-diagnoses"></i> Order Hemodialisa</a> -->
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-list"></i> Riwayat Penunjang</h4>
        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" onclick="laboratorium_data('<?= $reg['reg_id'] ?>','<?= $reg['pasien_id'] ?>','<?= $reg['lokasi_id'] ?>')" id="home-tab" data-toggle="tab" href="#laboratorium" role="tab" aria-controls="laboratorium" aria-selected="true"><i class="fas fa-flask"></i> Laboratorium</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" onclick="radiologi_data('<?= $reg['reg_id'] ?>','<?= $reg['pasien_id'] ?>','<?= $reg['lokasi_id'] ?>')" id="profile-tab" data-toggle="tab" href="#radiologi" role="tab" aria-controls="radiologi" aria-selected="false"><i class="fas fa-bone"></i> Radiologi</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" onclick="bedah_sentral_data('<?= $reg['reg_id'] ?>','<?= $reg['pasien_id'] ?>','<?= $reg['lokasi_id'] ?>')" id="contact-tab" data-toggle="tab" href="#bedah_sentral" role="tab" aria-controls="bedah_sentral" aria-selected="false"><i class="fas fa-user-md"></i> Bedah Sentral</a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" onclick="vk_data('<?= $reg['reg_id'] ?>','<?= $reg['pasien_id'] ?>','<?= $reg['lokasi_id'] ?>')" id="vk-tab" data-toggle="tab" href="#vk" role="tab" aria-controls="vk" aria-selected="false"><i class="fas fa-baby"></i> VK</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" onclick="icu_data('<?= $reg['reg_id'] ?>','<?= $reg['pasien_id'] ?>','<?= $reg['lokasi_id'] ?>')" id="icu-tab" data-toggle="tab" href="#icu" role="tab" aria-controls="icu" aria-selected="false"><i class="fas fa-procedures"></i> ICU/PICU/NICU</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" onclick="hemodialisa_data('<?= $reg['reg_id'] ?>','<?= $reg['pasien_id'] ?>','<?= $reg['lokasi_id'] ?>')" id="hemodialisa-tab" data-toggle="tab" href="#hemodialisa" role="tab" aria-controls="hemodialisa" aria-selected="false"><i class="fas fa-diagnoses"></i> Hemodialisa</a>
          </li> -->
        </ul>
        <div class="tab-content">
          <div class="tab-pane fade show active" id="laboratorium" role="tabpanel" aria-labelledby="home-tab">
            <div class="media">
              <div class="media-body">
                <div class="row mt-n4 pb-2">
                  <!-- <div class="col">
                    <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="pills-saat-ini-penunjang-laboratorium-tab" data-toggle="pill" href="#pills-saat-ini-penunjang-laboratorium" role="tab" aria-controls="pills-saat-ini-penunjang-laboratorium" aria-selected="false"><i class="fas fa-copy"></i> Data Saat Ini</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " id="pills-history-penunjang-laboratorium-tab" data-toggle="pill" href="#pills-history-penunjang-laboratorium" role="tab" aria-controls="pills-history-penunjang-laboratorium" aria-selected="true"><i class="fas fa-history"></i> Data History</a>
                      </li>
                    </ul>
                  </div> -->
                  <div class="col">
                    <button type="button" class="btn btn-sm btn-primary float-right" id="laboratorium_refresh" data-type="saat-ini"><i class="fas fa-sync-alt"></i> Refresh Data</button>
                  </div>
                </div>
                <div class="tab-content p-0" id="pills-tabContent-penunjang-laboratorium" style="border:0px !important">
                  <div class="tab-pane fade show active" id="pills-saat-ini-penunjang-laboratorium" role="tabpanel" aria-labelledby="pills-saat-ini-penunjang-laboratorium-tab">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped table-sm">
                        <thead>
                          <tr>
                            <th class="text-center" width="20">No</th>
                            <th class="text-center">Tgl. Order</th>
                            <th class="text-center">Keterangan Order</th>
                            <th class="text-center">Tgl. Diperiksa</th>
                            <th class="text-center">Dokter Pengirim</th>
                            <th class="text-center">Lokasi Asal</th>
                            <th class="text-center">Status</th>
                            <th class="text-center" width="90">Aksi</th>
                          </tr>
                        </thead>
                        <tbody id="laboratorium_data"></tbody>
                      </table>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="pills-history-penunjang-laboratorium" role="tabpanel" aria-labelledby="pills-history-penunjang-laboratorium-tab">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped table-sm">
                        <thead>
                          <tr>
                            <th class="text-center" width="20">No</th>
                            <th class="text-center">Tgl. Order</th>
                            <th class="text-center">Keterangan Order</th>
                            <th class="text-center">Tgl. Diperiksa</th>
                            <th class="text-center">Dokter Pengirim</th>
                            <th class="text-center">Lokasi Asal</th>
                            <th class="text-center">Status</th>
                            <th class="text-center" width="90">Detail</th>
                          </tr>
                        </thead>
                        <tbody id="laboratorium_history"></tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="radiologi" role="tabpanel" aria-labelledby="profile-tab">
            <div class="media">
              <div class="media-body">
                <div class="row mt-n4 pb-2">
                  <!-- <div class="col">
                    <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="pills-saat-ini-penunjang-radiologi-tab" data-toggle="pill" href="#pills-saat-ini-penunjang-radiologi" role="tab" aria-controls="pills-saat-ini-penunjang-radiologi" aria-selected="false"><i class="fas fa-copy"></i> Data Saat Ini</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " id="pills-history-penunjang-radiologi-tab" data-toggle="pill" href="#pills-history-penunjang-radiologi" role="tab" aria-controls="pills-history-penunjang-radiologi" aria-selected="true"><i class="fas fa-history"></i> Data History</a>
                      </li>
                    </ul>
                  </div> -->
                  <div class="col">
                    <button type="button" class="btn btn-sm btn-primary float-right" id="radiologi_refresh" data-type="saat-ini"><i class="fas fa-sync-alt"></i> Refresh Data</button>
                  </div>
                </div>
                <div class="tab-content p-0" id="pills-tabContent-penunjang-radiologi" style="border:0px !important">
                  <div class="tab-pane fade show active" id="pills-saat-ini-penunjang-radiologi" role="tabpanel" aria-labelledby="pills-saat-ini-penunjang-radiologi-tab">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped table-sm">
                        <thead>
                          <tr>
                            <th class="text-center" width="20">No</th>
                            <th class="text-center">Tgl. Order</th>
                            <th class="text-center">Keterangan Order</th>
                            <th class="text-center">Tgl. Diperiksa</th>
                            <th class="text-center">Dokter Pengirim</th>
                            <th class="text-center">Lokasi Asal</th>
                            <th class="text-center">Status</th>
                            <th class="text-center" width="90">Aksi</th>
                          </tr>
                        </thead>
                        <tbody id="radiologi_data"></tbody>
                      </table>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="pills-history-penunjang-radiologi" role="tabpanel" aria-labelledby="pills-history-penunjang-radiologi-tab">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped table-sm">
                        <thead>
                          <tr>
                            <th class="text-center" width="20">No</th>
                            <th class="text-center">Tgl. Order</th>
                            <th class="text-center">Keterangan Order</th>
                            <th class="text-center">Tgl. Diperiksa</th>
                            <th class="text-center">Dokter Pengirim</th>
                            <th class="text-center">Lokasi Asal</th>
                            <th class="text-center">Status</th>
                            <th class="text-center" width="90">Detail</th>
                          </tr>
                        </thead>
                        <tbody id="radiologi_history"></tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="bedah_sentral" role="tabpanel" aria-labelledby="contact-tab">
            <div class="media">
              <div class="media-body">
                <div class="row mt-n4 pb-2">
                  <!-- <div class="col">
                    <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="pills-saat-ini-penunjang-bedah-sentral-tab" data-toggle="pill" href="#pills-saat-ini-penunjang-bedah-sentral" role="tab" aria-controls="pills-saat-ini-penunjang-bedah-sentral" aria-selected="false"><i class="fas fa-copy"></i> Data Saat Ini</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " id="pills-history-penunjang-bedah-sentral-tab" data-toggle="pill" href="#pills-history-penunjang-bedah-sentral" role="tab" aria-controls="pills-history-penunjang-bedah-sentral" aria-selected="true"><i class="fas fa-history"></i> Data History</a>
                      </li>
                    </ul>
                  </div> -->
                  <div class="col">
                    <button type="button" class="btn btn-sm btn-primary float-right" id="bedah_sentral_refresh" data-type="saat-ini"><i class="fas fa-sync-alt"></i> Refresh Data</button>
                  </div>
                </div>
                <div class="tab-content p-0" id="pills-tabContent-penunjang-diagnosis" style="border:0px !important">
                  <div class="tab-pane fade show active" id="pills-saat-ini-penunjang-bedah-sentral" role="tabpanel" aria-labelledby="pills-saat-ini-penunjang-bedah-sentral-tab">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped table-sm">
                        <thead>
                          <tr>
                            <th class="text-center" width="20">No</th>
                            <th class="text-center">Tgl. Order</th>
                            <th class="text-center">Keterangan Order</th>
                            <th class="text-center">Tgl. Diperiksa</th>
                            <th class="text-center">Dokter Pengirim</th>
                            <th class="text-center">Lokasi Asal</th>
                            <th class="text-center">Status</th>
                            <th class="text-center" width="90">Aksi</th>
                          </tr>
                        </thead>
                        <tbody id="bedah_sentral_data"></tbody>
                      </table>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="pills-history-penunjang-bedah-sentral" role="tabpanel" aria-labelledby="pills-history-penunjang-bedah-sentral-tab">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped table-sm">
                        <thead>
                          <tr>
                            <th class="text-center" width="20">No</th>
                            <th class="text-center">Tgl. Order</th>
                            <th class="text-center">Keterangan Order</th>
                            <th class="text-center">Tgl. Diperiksa</th>
                            <th class="text-center">Dokter Pengirim</th>
                            <th class="text-center">Lokasi Asal</th>
                            <th class="text-center">Status</th>
                            <th class="text-center" width="90">Detail</th>
                          </tr>
                        </thead>
                        <tbody id="bedah_sentral_history"></tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="vk" role="tabpanel" aria-labelledby="vk-tab">
            <div class="media">
              <div class="media-body">
                <div class="row mt-n4 pb-2">
                  <!-- <div class="col">
                    <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="pills-saat-ini-penunjang-vk-tab" data-toggle="pill" href="#pills-saat-ini-penunjang-vk" role="tab" aria-controls="pills-saat-ini-penunjang-vk" aria-selected="false"><i class="fas fa-copy"></i> Data Saat Ini</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " id="pills-history-penunjang-vk-tab" data-toggle="pill" href="#pills-history-penunjang-vk" role="tab" aria-controls="pills-history-penunjang-vk" aria-selected="true"><i class="fas fa-history"></i> Data History</a>
                      </li>
                    </ul>
                  </div> -->
                  <div class="col">
                    <button type="button" class="btn btn-sm btn-primary float-right" id="vk_refresh" data-type="saat-ini"><i class="fas fa-sync-alt"></i> Refresh Data</button>
                  </div>
                </div>
                <div class="tab-content p-0" id="pills-tabContent-penunjang-vk" style="border:0px !important">
                  <div class="tab-pane fade show active" id="pills-saat-ini-penunjang-vk" role="tabpanel" aria-labelledby="pills-saat-ini-penunjang-vk-tab">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped table-sm">
                        <thead>
                          <tr>
                            <th class="text-center" width="20">No</th>
                            <th class="text-center">Tgl. Order</th>
                            <th class="text-center">Keterangan Order</th>
                            <th class="text-center">Tgl. Diperiksa</th>
                            <th class="text-center">Dokter Pengirim</th>
                            <th class="text-center">Lokasi Asal</th>
                            <th class="text-center">Status</th>
                            <th class="text-center" width="90">Aksi</th>
                          </tr>
                        </thead>
                        <tbody id="vk_data"></tbody>
                      </table>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="pills-history-penunjang-vk" role="tabpanel" aria-labelledby="pills-history-penunjang-vk-tab">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped table-sm">
                        <thead>
                          <tr>
                            <th class="text-center" width="20">No</th>
                            <th class="text-center">Tgl. Order</th>
                            <th class="text-center">Keterangan Order</th>
                            <th class="text-center">Tgl. Diperiksa</th>
                            <th class="text-center">Dokter Pengirim</th>
                            <th class="text-center">Lokasi Asal</th>
                            <th class="text-center">Status</th>
                            <th class="text-center" width="90">Detail</th>
                          </tr>
                        </thead>
                        <tbody id="vk_history"></tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="icu" role="tabpanel" aria-labelledby="icu-tab">
            <div class="media">
              <div class="media-body">
                <div class="row mt-n4 pb-2">
                  <!-- <div class="col">
                    <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="pills-saat-ini-penunjang-icu-tab" data-toggle="pill" href="#pills-saat-ini-penunjang-icu" role="tab" aria-controls="pills-saat-ini-penunjang-icu" aria-selected="false"><i class="fas fa-copy"></i> Data Saat Ini</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " id="pills-history-penunjang-icu-tab" data-toggle="pill" href="#pills-history-penunjang-icu" role="tab" aria-controls="pills-history-penunjang-icu" aria-selected="true"><i class="fas fa-history"></i> Data History</a>
                      </li>
                    </ul>
                  </div> -->
                  <div class="col">
                    <button type="button" class="btn btn-sm btn-primary float-right" id="icu_refresh" data-type="saat-ini"><i class="fas fa-sync-alt"></i> Refresh Data</button>
                  </div>
                </div>
                <div class="tab-content p-0" id="pills-tabContent-penunjang-icu" style="border:0px !important">
                  <div class="tab-pane fade show active" id="pills-saat-ini-penunjang-icu" role="tabpanel" aria-labelledby="pills-saat-ini-penunjang-icu-tab">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped table-sm">
                        <thead>
                          <tr>
                            <th class="text-center" width="20">No</th>
                            <th class="text-center">Tgl. Order</th>
                            <th class="text-center">Keterangan Order</th>
                            <th class="text-center">Tgl. Diperiksa</th>
                            <th class="text-center">Dokter Pengirim</th>
                            <th class="text-center">Lokasi Asal</th>
                            <th class="text-center">Status</th>
                            <th class="text-center" width="90">Aksi</th>
                          </tr>
                        </thead>
                        <tbody id="icu_data"></tbody>
                      </table>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="pills-history-penunjang-icu" role="tabpanel" aria-labelledby="pills-history-penunjang-icu-tab">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped table-sm">
                        <thead>
                          <tr>
                            <th class="text-center" width="20">No</th>
                            <th class="text-center">Tgl. Order</th>
                            <th class="text-center">Keterangan Order</th>
                            <th class="text-center">Tgl. Diperiksa</th>
                            <th class="text-center">Dokter Pengirim</th>
                            <th class="text-center">Lokasi Asal</th>
                            <th class="text-center">Status</th>
                            <th class="text-center" width="90">Detail</th>
                          </tr>
                        </thead>
                        <tbody id="icu_history"></tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="tab-pane fade" id="hemodialisa" role="tabpanel" aria-labelledby="hemodialisa-tab">
            <div class="media">
              <div class="media-body">
                <div class="row mt-n4 pb-2">
                  <!-- <div class="col">
                    <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" id="pills-saat-ini-penunjang-hemodialisa-tab" data-toggle="pill" href="#pills-saat-ini-penunjang-hemodialisa" role="tab" aria-controls="pills-saat-ini-penunjang-hemodialisa" aria-selected="false"><i class="fas fa-copy"></i> Data Saat Ini</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link " id="pills-history-penunjang-hemodialisa-tab" data-toggle="pill" href="#pills-history-penunjang-hemodialisa" role="tab" aria-controls="pills-history-penunjang-hemodialisa" aria-selected="true"><i class="fas fa-history"></i> Data History</a>
                      </li>
                    </ul>
                  </div> -->
                  <div class="col">
                    <button type="button" class="btn btn-sm btn-primary float-right" id="hemodialisa_refresh" data-type="saat-ini"><i class="fas fa-sync-alt"></i> Refresh Data</button>
                  </div>
                </div>
                <div class="tab-content p-0" id="pills-tabContent-penunjang-hemodialisa" style="border:0px !important">
                  <div class="tab-pane fade show active" id="pills-saat-ini-penunjang-hemodialisa" role="tabpanel" aria-labelledby="pills-saat-ini-penunjang-hemodialisa-tab">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped table-sm">
                        <thead>
                          <tr>
                            <th class="text-center" width="20">No</th>
                            <th class="text-center">Tgl. Order</th>
                            <th class="text-center">Keterangan Order</th>
                            <th class="text-center">Tgl. Diperiksa</th>
                            <th class="text-center">Dokter Pengirim</th>
                            <th class="text-center">Lokasi Asal</th>
                            <th class="text-center">Status</th>
                            <th class="text-center" width="90">Aksi</th>
                          </tr>
                        </thead>
                        <tbody id="hemodialisa_data"></tbody>
                      </table>
                    </div>
                  </div>
                  <div class="tab-pane fade" id="pills-history-penunjang-hemodialisa" role="tabpanel" aria-labelledby="pills-history-penunjang-hemodialisa-tab">
                    <div class="table-responsive">
                      <table class="table table-bordered table-striped table-sm">
                        <thead>
                          <tr>
                            <th class="text-center" width="20">No</th>
                            <th class="text-center">Tgl. Order</th>
                            <th class="text-center">Keterangan Order</th>
                            <th class="text-center">Tgl. Diperiksa</th>
                            <th class="text-center">Dokter Pengirim</th>
                            <th class="text-center">Lokasi Asal</th>
                            <th class="text-center">Status</th>
                            <th class="text-center" width="90">Detail</th>
                          </tr>
                        </thead>
                        <tbody id="hemodialisa_history"></tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>