<?php if (@$main == null) : ?>
  <tr>
    <th class="text-center" width="20">No</th>
    <th class="text-center" width="40">Aksi</th>
    <th class="text-center">Nama Obat</th>
    <th class="text-center">Jenis</th>
    <th class="text-center">Dosis</th>
    <th class="text-center">Qty</th>
    <th class="text-center">Aturan Pakai</th>
    <th class="text-center">Jam Minum</th>
    <th class="text-center">Peringatan Khusus</th>
  </tr>
  <tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else : ?>
  <?php foreach ($main as $k => $v) : ?>
    <tr bgcolor="#f5f5f5">
      <td class="text-left" colspan="99">
        <b>NO.RESEP : <?= $v['resep_id'] ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; TANGGAL : <?= to_date($v['tgl_catat'], '', 'date') ?></b>
        <span class="float-right">
          <a href="javascript:void(0)" class="btn btn-danger btn-table btn-delete-resep" data-reg-id="<?= $v['reg_id'] ?>" data-pasien-id="<?= $v['pasien_id'] ?>" data-resep-id="<?= $v['resep_id'] ?>" title="Hapus Resep">
            <i class="fas fa-trash-alt"></i> Hapus Resep
          </a>
        </span>
      </td>
    </tr>
    <tr>
      <th class="text-center" width="20">No</th>
      <th class="text-center" width="40">Aksi</th>
      <th class="text-center">Nama Obat/Alias</th>
      <th class="text-center">Sediaan</th>
      <th class="text-center">Rute</th>
      <th class="text-center">Qty</th>
      <th class="text-center">Aturan Pakai</th>
      <th class="text-center">Jadwal Pemberian</th>
      <th class="text-center">Aturan Tambahan</th>
    </tr>
    <?php $i = 1; ?>
    <?php foreach ($v['rinc'] as $k2 => $v2) : ?>
      <tr>
        <td rowspan="2" class="text-top text-center"><?= $i++ ?></td>
        <td rowspan="2" class="text-top text-center" width="60">
          <?php if (to_date($v['tgl_catat'], '', 'date') == date('d-m-Y')) : ?>
            <a href="javascript:void(0)" class="btn btn-primary btn-table btn-edit" data-resep-id="<?= $v2['resep_id'] ?>" data-reg-id="<?= $v2['reg_id'] ?>" data-lokasi-id="<?= $v2['lokasi_id'] ?>" data-rincian-id="<?= $v2['rincian_id'] ?>" title="Ubah Data"><i class="fas fa-pencil-alt"></i></a>
            <a href="javascript:void(0)" class="btn btn-danger btn-table btn-delete" data-resep-id="<?= $v2['resep_id'] ?>" data-reg-id="<?= $v2['reg_id'] ?>" data-lokasi-id="<?= $v2['lokasi_id'] ?>" data-rincian-id="<?= $v2['rincian_id'] ?>" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
          <?php endif; ?>
        </td>
        <td class="text-top text-left">
          <?= $v2['obat_nm'] ?>
          <?= ($v2['obat_alias'] != '') ? ' (' . $v2['obat_alias'] . ')' : '' ?>
        </td>
        <td class="text-top"><?= $v2['sediaan_nm'] ?></td>
        <td class="text-top"><?= $v2['rute_nm'] ?></td>
        <td class="text-top"><?= $v2['qty'] ?></td>
        <td class="text-top"><?= $v2['aturan_jml'] . ' ' . $v2['satuan_nm'] ?> tiap <?= $v2['aturan_waktu'] ?> <?= $v2['waktu_nm'] ?></td>
        <td class="text-top"><?= $v2['jadwal'] ?></td>
        <td class="text-top"><?= $v2['aturan_tambahan'] ?></td>
      </tr>
      <tr>
        <td colspan="99">
          <table style="width:100%">
            <thead>
              <tr>
                <th class="text-center" style="border-top: solid rgb(221, 221, 221) 1px" width="">Komposisi / Alias</th>
                <th class="text-center" style="border-top: solid rgb(221, 221, 221) 1px" width="50">Qty</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($v2['komposisi'] as $v3) : ?>
                <tr>
                  <td><?= $v3['komposisi'] ?></td>
                  <td><?= $v3['komposisi_qty'] ?></td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </td>
      </tr>
    <?php endforeach; ?>
  <?php endforeach; ?>
<?php endif; ?>

<script type="text/javascript">
  $(document).ready(function() {
    // edit
    $('.btn-edit').bind('click', function(e) {
      e.preventDefault();
      var resep_id = $(this).attr("data-resep-id");
      var rincian_id = $(this).attr("data-rincian-id");
      var reg_id = $(this).attr("data-reg-id");
      var lokasi_id = $(this).attr("data-lokasi-id");
      var pasien_id = $("#pasien_id").val();
      //
      $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_racik/get_data' ?>', {
        resep_id: resep_id,
        rincian_id: rincian_id,
        reg_id: reg_id,
        lokasi_id: lokasi_id,
        pasien_id: pasien_id
      }, function(data) {
        $("#resep_id").val(data.main.resep_id).removeClass("is-valid").removeClass("is-invalid");
        $("#rincian_id").val(data.main.rincian_id).removeClass("is-valid").removeClass("is-invalid");
        $("#obat_nm").val(data.main.obat_nm).removeClass("is-valid").removeClass("is-invalid");
        $("#racik_rute_cd").val(data.main.rute_cd).trigger('change').removeClass("is-valid").removeClass("is-invalid");
        $("#qty").val(data.main.qty).removeClass("is-valid").removeClass("is-invalid");
        $("#aturan_jml").val(data.main.aturan_jml).removeClass("is-valid").removeClass("is-invalid");
        $("#racik_satuan_cd").val(data.main.satuan_cd).trigger('change').removeClass("is-valid").removeClass("is-invalid");
        $("#aturan_waktu").val(data.main.aturan_waktu).removeClass("is-valid").removeClass("is-invalid");
        $("#racik_waktu_cd").val(data.main.waktu_cd).trigger('change').removeClass("is-valid").removeClass("is-invalid");
        $("#jadwal").val(data.main.jadwal).removeClass("is-valid").removeClass("is-invalid");
        $("#aturan_tambahan").val(data.main.aturan_tambahan).removeClass("is-valid").removeClass("is-invalid");
        $.each(data.main.komposisi, function(index, value) {
          console.log(value);
          var alias = '';
          if (value.komposisi_alias != '') {
            alias = ' (' + value.komposisi_alias + ')';
          }
          var html = '<tr>' +
            '<td class="text-center">' +
            '<button type="button" class="btn btn-danger btn-table btn-delete-komposisi"><i class="fas fa-trash-alt"></i></button>' +
            '<input type="hidden" name="komposisi_id[]" value="' + value.komposisi_id + '">' +
            '</td>' +
            '<td>' +
            value.komposisi + alias +
            '<input type="hidden" name="komposisi[]" value="' + value.komposisi + '">' +
            '<input type="hidden" name="komposisi_alias[]" value="' + value.komposisi_alias + '">' +
            '</td>' +
            '<td class="text-right">' +
            value.komposisi_qty +
            '<input type="hidden" name="komposisi_qty[]" value="' + value.komposisi_qty + '">' +
            '</td>' +
            '</tr>';
          $("#komposisi_data").append(html);
        });
        $('#resep_action').html('<i class="fas fa-save"></i> Ubah');
      }, 'json');
    });
    // delete
    $('.btn-delete').on('click', function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Apakah Anda yakin?',
        text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#eb3b5a',
        cancelButtonColor: '#b2bec3',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        customClass: 'swal-wide'
      }).then((result) => {
        if (result.value) {
          var resep_id = $(this).attr("data-resep-id");
          var rincian_id = $(this).attr("data-rincian-id");
          var reg_id = $(this).attr("data-reg-id");
          var lokasi_id = $(this).attr("data-lokasi-id");
          var pasien_id = $("#pasien_id").val();
          //
          $('#racik_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
          $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_racik/delete_data' ?>', {
            resep_id: resep_id,
            rincian_id: rincian_id,
            reg_id: reg_id,
            lokasi_id: lokasi_id,
            pasien_id: pasien_id
          }, function(data) {
            console.log(data);
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil dihapus.',
              icon: 'success',
              position: 'top-right'
            })
            $('#racik_data').html(data.html);
          }, 'json');
        }
      })
    })

    $('.btn-delete-resep').on('click', function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Apakah Anda yakin?',
        text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#eb3b5a',
        cancelButtonColor: '#b2bec3',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        customClass: 'swal-wide'
      }).then((result) => {
        if (result.value) {
          var resep_id = $(this).attr("data-resep-id");
          var reg_id = $(this).attr("data-reg-id");
          var pasien_id = $("#pasien_id").val();
          $('#racik_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
          $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_racik/delete_resep' ?>', {
            resep_id: resep_id,
            reg_id: reg_id,
            pasien_id: pasien_id
          }, function(data) {
            console.log(data);
            $.toast({
              heading: 'Sukses',
              text: 'Resep berhasil dihapus.',
              icon: 'success',
              position: 'top-right'
            })
            $('#racik_data').html(data.html);
          }, 'json');
        }
      })
    })
  });
</script>