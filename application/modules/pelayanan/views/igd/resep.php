<?php $this->load->view('_js_resep'); ?>
<div class="row">
  <div class="col-md-6">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-list"></i> Data Resep Non Racikan</h4>
        <!-- <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="pills-saat-ini-resep-tab" data-toggle="pill" href="#pills-saat-ini-resep" role="tab" aria-controls="pills-saat-ini-resep" aria-selected="false"><i class="fas fa-copy"></i> Data Saat Ini</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " id="pills-history-resep-tab" data-toggle="pill" href="#pills-history-resep" role="tab" aria-controls="pills-history-resep" aria-selected="true"><i class="fas fa-history"></i> Data History</a>
          </li>
        </ul> -->
        <div class="tab-content p-0" id="pills-tabContent-resep" style="border:0px !important">
          <div class="tab-pane fade show active" id="pills-saat-ini-resep" role="tabpanel" aria-labelledby="pills-saat-ini-resep-tab">
            <div class="table-responsive">
              <table id="resep_data" class="table table-bordered table-striped table-sm">

              </table>
            </div>
          </div>
          <div class="tab-pane fade" id="pills-history-resep" role="tabpanel" aria-labelledby="pills-history-resep-tab">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-sm">
                <thead>
                  <tr>
                    <th class="text-center" width="20">No</th>
                    <th class="text-center">Tanggal</th>
                    <th class="text-center">Kode</th>
                    <th class="text-center">resep</th>
                    <th class="text-center">Kasus</th>
                    <th class="text-center">Tipe</th>
                    <th class="text-center">Keterangan</th>
                    <th class="text-center">Lokasi Pelayanan</th>
                  </tr>
                </thead>
                <tbody id="resep_history"></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-prescription"></i> Resep Non Racik</h4>
        <form id="resep_form" action="" method="post" autocomplete="off">
          <input type="hidden" name="resep_id" id="resep_id">
          <input type="hidden" name="rincian_id" id="rincian_id">
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Nama Obat <span class="text-danger">*</span></label>
            <div class="col-lg-9 col-md-8">
              <input type="text" class="form-control" name="obat_nm" id="obat_nm" required="" aria-invalid="false">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Alias Nama Obat</label>
            <div class="col-lg-9 col-md-8">
              <input type="text" class="form-control" name="obat_alias" id="obat_alias" aria-invalid="false">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Rute <span class="text-danger">*<span></label>
            <div class="col-lg-4 col-md-9">
              <select class="form-control chosen-select" name="rute_cd" id="rute_cd" required="">
                <?php foreach (get_parameter('rute_cd') as $r) : ?>
                  <option value="<?= $r['parameter_cd'] ?>"><?= $r['parameter_val'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Qty <span class="text-danger">*</span></label>
            <div class="col-lg-3 col-md-3">
              <input type="text" class="form-control" name="qty" id="qty" required="" aria-invalid="false">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Aturan Pakai <span class="text-danger">*</span></label>
            <div class="col-lg-2 col-md-3">
              <input type="text" class="form-control" name="aturan_jml" id="aturan_jml" required="" aria-invalid="false" placeholder="Jml">
            </div>
            <div class="col-lg-2 col-md-9">
              <select class="form-control chosen-select" name="satuan_cd" id="satuan_cd" required="">
                <?php foreach (get_parameter('satuan_cd') as $r) : ?>
                  <option value="<?= $r['parameter_cd'] ?>"><?= $r['parameter_val'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="col-lg-1 col-md-1 pt-3">
              <label><strong>Tiap</strong></label>
            </div>
            <div class="col-lg-2 col-md-3">
              <input type="text" class="form-control" name="aturan_waktu" id="aturan_waktu" required="" aria-invalid="false" placeholder="Jml">
            </div>
            <div class="col-lg-2 col-md-9">
              <select class="form-control chosen-select" name="waktu_cd" id="waktu_cd" required="">
                <?php foreach (get_parameter('waktu_cd') as $r) : ?>
                  <option value="<?= $r['parameter_cd'] ?>"><?= $r['parameter_val'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Jadwal Pemberian</label>
            <div class="col-lg-9 col-md-9">
              <input type="text" class="form-control" name="jadwal" id="jadwal" aria-invalid="false">
              <small class="text-info">Tuliskan jam dan pisahkan dengan spasi. Cth: 07:00 15:00 22:00</small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Aturan Tambahan</label>
            <div class="col-lg-9 col-md-9">
              <input type="text" class="form-control" name="aturan_tambahan" id="aturan_tambahan" aria-invalid="false">
            </div>
          </div>
          <div class="row">
            <div class="col-lg-9 offset-lg-3 p-0">
              <button type="submit" id="resep_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
              <button type="button" id="resep_cancel" class="btn btn-xs btn-secondary"><i class="fas fa-times"></i> Batal</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>