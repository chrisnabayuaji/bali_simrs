<?php if(@$main == null):?>
	<tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else: ?>
	<?php $i=1;foreach($main as $row):?>
	<tr>
		<td class="text-center"><?=$i++?></td>
    <td class="text-center"><?=to_date($row['tgl_catat'], '', 'full_date', '<br>')?></td>
    <td class="text-center"><?=$row['tarif_id']?></td>
    <td class="text-left"><?=$row['tarif_nm']?> - <?=$row['kelas_nm']?></td>
    <td class="text-center"><?=$row['qty']?></td>
    <td class="text-center"><?=num_id($row['nom_tarif'])?></td>
    <td class="text-left">
      <?=($row['pegawai_nm_1'] !='') ? $row['pegawai_nm_1'].',' : ''?>
      <?=($row['pegawai_nm_2'] !='') ? $row['pegawai_nm_2'].',' : ''?>
      <?=($row['pegawai_nm_3'] !='') ? $row['pegawai_nm_3'].',' : ''?>
      <?=($row['pegawai_nm_4'] !='') ? $row['pegawai_nm_4'].',' : ''?>
      <?=($row['pegawai_nm_5'] !='') ? $row['pegawai_nm_5'].'' : ''?>
    </td>
    <td class="text-center"><?=$row['lokasi_nm']?></td>
	</tr>
	<?php endforeach; ?>
<?php endif; ?>