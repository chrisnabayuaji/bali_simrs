<!-- js -->
<?php $this->load->view('pelayanan/info_antrian/_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Nama Info</label>
        <div class="col-lg-9 col-md-9">
          <input type="text" class="form-control" name="info_nm" value="<?=@$main['info_nm']?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Informasi <span class="text-danger">*<span></label>
        <div class="col-lg-9 col-md-9">
          <textarea class="form-control" rows="8" name="info_txt" required="" style="line-height: 1.5!important"><?=@$main['info_txt']?></textarea>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>