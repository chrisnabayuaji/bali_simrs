<script type="text/javascript">
  $(document).ready(function () {
    var form = $("#form-data").validate( {
      rules: {
        lokasi_id:{
          valueNotEquals: "",
          remote: {
            url: '<?=site_url().'/'.$nav['nav_url']?>/ajax/cek_id/<?=@$main['lokasi_id']?>',
            type: 'post',
            data: {
              lokasi_id: function() {
                return $("#lokasi_id").val();
              }
            }
          }
        },
        periode_tgl_awal: {
          dateID: true
        },
        periode_tgl_akhir: {
          dateID: true
        }
      },
      messages: {
        lokasi_id : {
          valueNotEquals: "Pilih salah satu!",
          remote : 'Lokasi sudah terdaftar!'
        }
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container"));
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    // item senin
    add_item('<?=$count_senin?>', 'senin', '<?=(@$id !='') ? $id : ""?>');
    $('#add_item_senin').bind('click',function(e) {
      e.preventDefault();
      var no = $('#item_no_senin').val();
      add_item(no, 'senin');
    });

    // item selasa
    add_item('<?=$count_selasa?>', 'selasa', '<?=(@$id !='') ? $id : ""?>');
    $('#add_item_selasa').bind('click',function(e) {
      e.preventDefault();
      var no = $('#item_no_selasa').val();
      add_item(no, 'selasa');
    });

    // item rabu
    add_item('<?=$count_rabu?>', 'rabu', '<?=(@$id !='') ? $id : ""?>');
    $('#add_item_rabu').bind('click',function(e) {
      e.preventDefault();
      var no = $('#item_no_rabu').val();
      add_item(no, 'rabu');
    });

    // item kamis
    add_item('<?=$count_kamis?>', 'kamis', '<?=(@$id !='') ? $id : ""?>');
    $('#add_item_kamis').bind('click',function(e) {
      e.preventDefault();
      var no = $('#item_no_kamis').val();
      add_item(no, 'kamis');
    });

    // item jumat
    add_item('<?=$count_jumat?>', 'jumat', '<?=(@$id !='') ? $id : ""?>');
    $('#add_item_jumat').bind('click',function(e) {
      e.preventDefault();
      var no = $('#item_no_jumat').val();
      add_item(no, 'jumat');
    });

    // item sabtu
    add_item('<?=$count_sabtu?>', 'sabtu', '<?=(@$id !='') ? $id : ""?>');
    $('#add_item_sabtu').bind('click',function(e) {
      e.preventDefault();
      var no = $('#item_no_sabtu').val();
      add_item(no, 'sabtu');
    });

    // item minggu
    add_item('<?=$count_minggu?>', 'minggu', '<?=(@$id !='') ? $id : ""?>');
    $('#add_item_minggu').bind('click',function(e) {
      e.preventDefault();
      var no = $('#item_no_minggu').val();
      add_item(no, 'minggu');
    });

  })

  function add_item(no, hari, jadwal_id) {
    $.get('<?=site_url($nav['nav_url'])?>/ajax/add_item?item_no='+no+'&hari='+hari+'&jadwal_id='+jadwal_id,null,function(data) {
      $('#box_item_'+hari).append(data.html);
      $('#item_no_'+hari).val(data.item_no);
    },'json');
  } 
</script>