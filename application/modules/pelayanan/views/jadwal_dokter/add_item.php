<script type="text/javascript">
  $(document).ready(function () {
    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    $('.timepicker').daterangepicker({
      timePicker: true,
      timePicker24Hour: true,
      timePickerIncrement: 2,
      locale: {
          format: 'HH:mm'
      }
    }).on('show.daterangepicker', function (ev, picker) {
      picker.container.find(".calendar-table").hide();
    });

    // delete
    $('.delete-item-add').on('click', function (e) {
      e.preventDefault();
      Swal.fire({
        title: 'Apakah Anda yakin?',
        text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#eb3b5a',
        cancelButtonColor: '#b2bec3',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        customClass: 'swal-wide'
      }).then((result) => {
        if (result.value) {
          var item_no = $(this).attr("data-item-no");
          var hari = $(this).attr("data-hari");
          //
          $("#dokter-"+hari+"-"+item_no).remove();
          $("#jam-"+hari+"-"+item_no).remove();
          $("#jeda-waktu-"+hari+"-"+item_no).remove();
          $("#max-pasien-"+hari+"-"+item_no).remove();
        }
      })
    })

    $('.delete-item-edit').on('click', function (e) {
      e.preventDefault();
      Swal.fire({
        title: 'Apakah Anda yakin?',
        text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#eb3b5a',
        cancelButtonColor: '#b2bec3',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        customClass: 'swal-wide'
      }).then((result) => {
        if (result.value) {
          var jadwalrinc_id = $(this).attr("data-jadwalrinc-id");
          var hari = $(this).attr("data-hari");
          //
          $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax/delete_item'?>', {jadwalrinc_id: jadwalrinc_id}, function (data) {
            if (data.callback == 'true') {
              $("#dokter-"+hari+"-"+jadwalrinc_id).remove();
              $("#jam-"+hari+"-"+jadwalrinc_id).remove();
              $("#jeda-waktu-"+hari+"-"+jadwalrinc_id).remove();
              $("#max-pasien-"+hari+"-"+jadwalrinc_id).remove();
            }
          }, 'json');
        }
      })
    })
  })
</script>
<?php if(@$list_rinc == null):?>
  <div class="form-group row" id="dokter-<?=$hari?>-<?=$item_no?>">
    <label class="col-lg-2 col-md-2 col-form-label">Dokter <span class="text-danger">*<span></label>
    <div class="col-lg-6 col-md-6">
      <select class="chosen-select custom-select w-100" name="dokter_id_<?=$hari?>_<?=$item_no?>">
        <option value="">- Pilih -</option>
        <?php foreach($dokter as $r):?>
          <option value="<?=$r['pegawai_id']?>" <?php if($r['pegawai_id'] == @$main['dokter_id']) echo 'selected'?>><?=$r['pegawai_nm']?></option>
        <?php endforeach;?>
      </select>
    </div>
    <div class="col-lg-2">
      <a href="javascript:void(0)" class="btn btn-xs btn-secondary delete-item-add" data-item-no="<?=@$item_no?>" data-hari="<?=$hari?>" title="Hapus Petugas"><i class="fas fa-times"></i></a>
    </div>
  </div>
  <div class="form-group row" id="jam-<?=$hari?>-<?=$item_no?>">
    <label class="col-lg-2 col-md-2 col-form-label">Jam <span class="text-danger">*<span></label>
    <div class="col-lg-3 col-md-3">
      <input class="form-control form-control-sm timepicker" type="text" name="jam_<?=$hari?>_<?=$item_no?>" value="<?=@$main['jam_<?=$item_no?>']?>">
    </div>
  </div>
  <div class="form-group row" id="jeda-waktu-<?=$hari?>-<?=$item_no?>">
    <label class="col-lg-2 col-md-2 col-form-label">Jeda Waktu<span class="text-danger">*<span></label>
    <div class="col-lg-2 col-md-2">
      <input class="form-control form-control-sm text-center" type="text" name="jeda_waktu_<?=$hari?>_<?=$item_no?>" value="<?=@$main['jeda_waktu_<?=$item_no?>']?>">
    </div>
    <div class="col-lg-2 col-md-2 mt-1 ml-n4">Menit</div>
  </div>
  <div class="form-group row" id="max-pasien-<?=$hari?>-<?=$item_no?>">
    <label class="col-lg-2 col-md-2 col-form-label">Max Pasien<span class="text-danger">*<span></label>
    <div class="col-lg-2 col-md-2">
      <input class="form-control form-control-sm text-center" type="text" name="max_pasien_<?=$hari?>_<?=$item_no?>" value="<?=@$main['max_pasien_<?=$item_no?>']?>">
    </div>
    <div class="col-lg-2 col-md-2 mt-1 ml-n4">Pasien</div>
  </div>
<?php else: ?>
  <?php $i=1;foreach ($list_rinc as $row): ?>
  <input type="hidden" name="jadwalrinc_id_<?=$hari?>_<?=$i?>" value="<?=$row['jadwalrinc_id']?>">
    <div class="form-group row" id="dokter-<?=$hari?>-<?=$row['jadwalrinc_id']?>">
      <label class="col-lg-2 col-md-2 col-form-label">Dokter <span class="text-danger">*<span></label>
      <div class="col-lg-6 col-md-6">
        <select class="chosen-select custom-select w-100" name="dokter_id_<?=$hari?>_<?=$i?>">
          <option value="">- Pilih -</option>
          <?php foreach($dokter as $r):?>
            <option value="<?=$r['pegawai_id']?>" <?php if($r['pegawai_id'] == @$row['dokter_id']) echo 'selected'?>><?=$r['pegawai_nm']?></option>
          <?php endforeach;?>
        </select>
      </div>
      <div class="col-lg-2">
        <a href="javascript:void(0)" class="btn btn-xs btn-secondary delete-item-edit" data-jadwalrinc-id="<?=@$row['jadwalrinc_id']?>" data-hari="<?=$hari?>" title="Hapus Petugas"><i class="fas fa-times"></i></a>
      </div>
    </div>
    <div class="form-group row" id="jam-<?=$hari?>-<?=$row['jadwalrinc_id']?>">
      <label class="col-lg-2 col-md-2 col-form-label">Jam <span class="text-danger">*<span></label>
      <div class="col-lg-3 col-md-3">
        <input class="form-control form-control-sm timepicker" type="text" name="jam_<?=$hari?>_<?=$i?>" value="<?=@$row['jam']?>">
      </div>
    </div>
    <div class="form-group row" id="jeda-waktu-<?=$hari?>-<?=$row['jadwalrinc_id']?>">
      <label class="col-lg-2 col-md-2 col-form-label">Jeda Waktu<span class="text-danger">*<span></label>
      <div class="col-lg-2 col-md-2">
        <input class="form-control form-control-sm text-center" type="text" name="jeda_waktu_<?=$hari?>_<?=$i?>" value="<?=@$row['jeda_waktu']?>">
      </div>
      <div class="col-lg-2 col-md-2 mt-1 ml-n4">Menit</div>
    </div>
    <div class="form-group row" id="max-pasien-<?=$hari?>-<?=$row['jadwalrinc_id']?>">
      <label class="col-lg-2 col-md-2 col-form-label">Max Pasien<span class="text-danger">*<span></label>
      <div class="col-lg-2 col-md-2">
        <input class="form-control form-control-sm text-center" type="text" name="max_pasien_<?=$hari?>_<?=$i?>" value="<?=@$row['max_pasien']?>">
      </div>
      <div class="col-lg-2 col-md-2 mt-1 ml-n4">Pasien</div>
    </div>
  <?php $i++; endforeach; ?>
<?php endif; ?>