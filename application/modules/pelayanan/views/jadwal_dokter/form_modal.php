<!-- js -->
<?php $this->load->view('_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-12">
          <div class="form-group row mb-n3">
            <label class="col-lg-2 col-md-2 col-form-label">Lokasi <span class="text-danger">*<span></label>
            <div class="col-lg-6 col-md-6">
              <select class="chosen-select custom-select w-100" name="lokasi_id" id="lokasi_id">
                <option value="">- Pilih -</option>
                <?php foreach($lokasi as $r):?>
                <option value="<?=$r['lokasi_id']?>" <?php if($r['lokasi_id'] == @$main['lokasi_id']) echo 'selected'?>><?=$r['lokasi_id']?> - <?=$r['lokasi_nm']?></option>
                <?php endforeach;?>
              </select>
            </div>
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
        <div class="col-12">
          <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="pills-senin-tab" data-toggle="pill" href="#pills-senin" role="tab" aria-controls="pills-senin" aria-selected="false">Senin</a>
            </li>
            <li class="nav-item">
              <a class="nav-link " id="pills-selasa-tab" data-toggle="pill" href="#pills-selasa" role="tab" aria-controls="pills-selasa" aria-selected="true">Selasa</a>
            </li>
            <li class="nav-item">
              <a class="nav-link " id="pills-rabu-tab" data-toggle="pill" href="#pills-rabu" role="tab" aria-controls="pills-rabu" aria-selected="true">Rabu</a>
            </li>
            <li class="nav-item">
              <a class="nav-link " id="pills-kamis-tab" data-toggle="pill" href="#pills-kamis" role="tab" aria-controls="pills-kamis" aria-selected="true">Kamis</a>
            </li>
            <li class="nav-item">
              <a class="nav-link " id="pills-jumat-tab" data-toggle="pill" href="#pills-jumat" role="tab" aria-controls="pills-jumat" aria-selected="true">Jum'at</a>
            </li>
            <li class="nav-item">
              <a class="nav-link " id="pills-sabtu-tab" data-toggle="pill" href="#pills-sabtu" role="tab" aria-controls="pills-sabtu" aria-selected="true">Sabtu</a>
            </li>
            <li class="nav-item">
              <a class="nav-link " id="pills-minggu-tab" data-toggle="pill" href="#pills-minggu" role="tab" aria-controls="pills-minggu" aria-selected="true">Minggu</a>
            </li>
          </ul>
          <div class="tab-content p-0" id="pills-tabContent" style="border:0px !important">
            <div class="tab-pane fade show active" id="pills-senin" role="tabpanel" aria-labelledby="pills-senin-tab">
              <div id="box_item_senin"></div>
              <div class="form-group row mb-2">
                <label class="col-lg-2 col-md-2 col-form-label"></label>
                <div class="col-lg-6 col-md-6">
                  <input type="hidden" name="item_no_senin" id="item_no_senin" value="0">
                  <a href="javascript:void(0)" class="btn btn-xs btn-default float-right" id="add_item_senin" title="Tambah Item"><i class="fas fa-plus"></i> Tambah Item</a>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="pills-selasa" role="tabpanel" aria-labelledby="pills-selasa-tab">
              <div id="box_item_selasa"></div>
              <div class="form-group row mb-2">
                <label class="col-lg-2 col-md-2 col-form-label"></label>
                <div class="col-lg-6 col-md-6">
                  <input type="hidden" name="item_no_selasa" id="item_no_selasa" value="0">
                  <a href="javascript:void(0)" class="btn btn-xs btn-default float-right" id="add_item_selasa" title="Tambah Item"><i class="fas fa-plus"></i> Tambah Item</a>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="pills-rabu" role="tabpanel" aria-labelledby="pills-rabu-tab">
              <div id="box_item_rabu"></div>
              <div class="form-group row mb-2">
                <label class="col-lg-2 col-md-2 col-form-label"></label>
                <div class="col-lg-6 col-md-6">
                  <input type="hidden" name="item_no_rabu" id="item_no_rabu" value="0">
                  <a href="javascript:void(0)" class="btn btn-xs btn-default float-right" id="add_item_rabu" title="Tambah Item"><i class="fas fa-plus"></i> Tambah Item</a>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="pills-kamis" role="tabpanel" aria-labelledby="pills-kamis-tab">
              <div id="box_item_kamis"></div>
              <div class="form-group row mb-2">
                <label class="col-lg-2 col-md-2 col-form-label"></label>
                <div class="col-lg-6 col-md-6">
                  <input type="hidden" name="item_no_kamis" id="item_no_kamis" value="0">
                  <a href="javascript:void(0)" class="btn btn-xs btn-default float-right" id="add_item_kamis" title="Tambah Item"><i class="fas fa-plus"></i> Tambah Item</a>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="pills-jumat" role="tabpanel" aria-labelledby="pills-jumat-tab">
              <div id="box_item_jumat"></div>
              <div class="form-group row mb-2">
                <label class="col-lg-2 col-md-2 col-form-label"></label>
                <div class="col-lg-6 col-md-6">
                  <input type="hidden" name="item_no_jumat" id="item_no_jumat" value="0">
                  <a href="javascript:void(0)" class="btn btn-xs btn-default float-right" id="add_item_jumat" title="Tambah Item"><i class="fas fa-plus"></i> Tambah Item</a>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="pills-sabtu" role="tabpanel" aria-labelledby="pills-sabtu-tab">
              <div id="box_item_sabtu"></div>
              <div class="form-group row mb-2">
                <label class="col-lg-2 col-md-2 col-form-label"></label>
                <div class="col-lg-6 col-md-6">
                  <input type="hidden" name="item_no_sabtu" id="item_no_sabtu" value="0">
                  <a href="javascript:void(0)" class="btn btn-xs btn-default float-right" id="add_item_sabtu" title="Tambah Item"><i class="fas fa-plus"></i> Tambah Item</a>
                </div>
              </div>
            </div>
            <div class="tab-pane fade" id="pills-minggu" role="tabpanel" aria-labelledby="pills-minggu-tab">
              <div id="box_item_minggu"></div>
              <div class="form-group row mb-2">
                <label class="col-lg-2 col-md-2 col-form-label"></label>
                <div class="col-lg-6 col-md-6">
                  <input type="hidden" name="item_no_minggu" id="item_no_minggu" value="0">
                  <a href="javascript:void(0)" class="btn btn-xs btn-default float-right" id="add_item_minggu" title="Tambah Item"><i class="fas fa-plus"></i> Tambah Item</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>