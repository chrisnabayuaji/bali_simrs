<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline">
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          <?=$nav['nav_nm']?>
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?=site_url('app/dashboard')?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url($module_nav['nav_url'])?>"><i class="fas fa-folder-open"></i> <?=$module_nav['nav_nm']?></a></li>
          <li class="breadcrumb-item"><a href="#"><i class="fas fa-folder-open"></i> Data/Informasi</a></li>
          <li class="breadcrumb-item"><a href="<?=site_url($nav['nav_url'])?>"><?=$nav['nav_nm']?></a></li>
          <li class="breadcrumb-item active"><span>Index</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>
  <!-- flash message -->
  <div class="flash-success" data-flashsuccess="<?=$this->session->flashdata('flash_success')?>"></div>
  <!-- /flash message -->
  <div class="row full-page mt-4 mb-n2">
    <div class="col-lg-12 grid-margin-xl-0 stretch-card">
      <div class="card border-none">
        <div class="card-body">
          <form id="form-search" action="<?=site_url().'/app/search/'.$nav['nav_id']?>" method="post" autocomplete="off">
            <div class="row">
              <div class="col-md-2">
                <?php if ($nav['_add']):?>
                  <a href="#" data-href="<?=site_url().'/'.$nav['nav_url'].'/form_modal'?>" modal-title="Tambah Data" modal-size="md" modal-custom-size="600" class="btn btn-xs btn-primary modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Tambah Data"><i class="fas fa-plus-circle"></i> Tambah</a>
                <?php endif;?>
              </div>
              <!-- <div class="col-md-3 offset-md-7">
                <div class="input-group">
                  <input type="text" name="term" value="<?=@$cookie['search']['term']?>" class="form-control form-control-sm" placeholder="Pencarian...">
                  <div class="input-group-append">
                    <button type="submit" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cari Data"><i class="fas fa-search"></i></button>
                    <a class="btn btn-xs btn-default" href="<?=site_url().'/app/reset/'.$nav['nav_id']?>" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-default" title="" data-original-title="Reset"><i class="fas fa-sync-alt"></i></a>
                  </div>
                </div>
              </div> -->
            </div><!-- /.row -->
          </form>
          <div class="row mt-2 pagination-info">
            <div class="col-md-6">
              <form id="form-paging" action="<?=site_url().'/app/per_page/'.$nav['nav_id']?>" method="post">
                <label><i class="mdi mdi-bookmark"></i> Tampilkan</label>
                <select name="per_page" class="select-pagination" onchange="$('#form-paging').submit()">
                  <option value="10" <?php if($cookie['per_page'] == 10){echo 'selected';}?>>10</option>
                  <option value="50" <?php if($cookie['per_page'] == 50){echo 'selected';}?>>50</option>
                  <option value="100" <?php if($cookie['per_page'] == 100){echo 'selected';}?>>100</option>
                </select>
                <label>data per halaman.</label>
              </form>
            </div>
            <div class="col-md-6 text-right">
              <div class="pt-1"><?=@$pagination_info?></div>
            </div>
          </div><!-- /.row -->
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                <form id="form-multiple" action="<?=site_url().'/'.$nav['nav_url'].'/multiple/enable'?>" method="post">
                  <table class="table table-hover table-striped table-bordered table-fixed">
                    <thead>
                      <tr>
                        <th class="text-center" width="36">No</th>
                        <th class="text-center" width="36">
                          <div class="form-check form-check-primary form-check-th">
                            <label class="form-check-label">
                              <input type="checkbox" class="form-check-input cb-all">
                            </label>
                          </div>
                        </th>
                        <th class="text-center" width="60">Aksi</th>
                        <th class="text-center"><?=table_sort($nav['nav_id'], 'Lokasi', 'lokasi_nm', $cookie['order'])?></th>
                        <th class="text-center" width="150">Senin</th>
                        <th class="text-center" width="150">Selasa</th>
                        <th class="text-center" width="150">Rabu</th>
                        <th class="text-center" width="150">Kamis</th>
                        <th class="text-center" width="150">Jum'at</th>
                        <th class="text-center" width="150">Sabtu</th>
                        <th class="text-center" width="150">Minggu</th>
                      </tr>
                    </thead>
                    <?php if(@$main == null):?>
                      <tbody>
                        <tr>
                          <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                        </tr>
                      </tbody>
                    <?php else:?>
                      <tbody>
                        <?php $i=1;foreach($main as $row):?>
                          <tr>
                            <td class="text-center text-middle" width="36"><?=$cookie['cur_page']+($i++)?></td>
                            <td class="text-center text-middle" width="36">
                              <div class="form-check form-check-primary form-check-td">
                                <label class="form-check-label">
                                  <input type="checkbox" class="form-check-input cb-item" name="checkitem[]" value="<?=$row['jadwal_id']?>">
                                </label>
                              </div>
                            </td>
                            <td class="text-center text-middle" width="60">
                              <?php if($nav['_update'] || $nav['_delete']):?>
                                <?php if($nav['_update']):?>
                                  <a href="#" data-href="<?=site_url().'/'.$nav['nav_url'].'/form_modal/'.$row['jadwal_id']?>" modal-title="Ubah Data" modal-size="md" modal-custom-size="600" class="btn btn-primary btn-table modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Ubah Data"><i class="fas fa-pencil-alt"></i></a>
                                <?php endif;?>
                                <?php if($nav['_delete']):?>
                                  <a href="#" data-href="<?=site_url().'/'.$nav['nav_url'].'/delete/'.$row['jadwal_id']?>" class="btn btn-danger btn-table btn-delete" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-danger" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
                                <?php endif;?>
                              <?php endif;?>
                            </td>
                            <td><?=$row['lokasi_nm']?></td>
                            <td class="text-center text-top" width="150">
                              <?php if (count($row['rinc_senin']) > 1): ?>
                                <?php foreach ($row['rinc_senin'] as $senin): ?>
                                  <b><?=$senin['pegawai_nm']?></b><br><?=$senin['jam']?><hr>
                                <?php endforeach; ?>
                              <?php else: ?>
                                <?php foreach ($row['rinc_senin'] as $senin): ?>
                                  <?php if ($senin['dokter_id'] !=''): ?>
                                    <b><?=$senin['pegawai_nm']?></b><br><?=$senin['jam']?>
                                  <?php endif; ?>
                                <?php endforeach; ?>
                              <?php endif; ?>
                            </td>
                            <td class="text-center text-top" width="150">
                              <?php if (count($row['rinc_selasa']) > 1): ?>
                                <?php foreach ($row['rinc_selasa'] as $selasa): ?>
                                  <b><?=$selasa['pegawai_nm']?></b><br><?=$selasa['jam']?><hr>
                                <?php endforeach; ?>
                              <?php else: ?>
                                <?php foreach ($row['rinc_selasa'] as $selasa): ?>
                                  <?php if ($selasa['dokter_id'] !=''): ?>
                                    <b><?=$selasa['pegawai_nm']?></b><br><?=$selasa['jam']?>
                                  <?php endif; ?>
                                <?php endforeach; ?>
                              <?php endif; ?>
                            </td>
                            <td class="text-center text-top" width="150">
                              <?php if (count($row['rinc_rabu']) > 1): ?>
                                <?php foreach ($row['rinc_rabu'] as $rabu): ?>
                                  <b><?=$rabu['pegawai_nm']?></b><br><?=$rabu['jam']?><hr>
                                <?php endforeach; ?>
                              <?php else: ?>
                                <?php foreach ($row['rinc_rabu'] as $rabu): ?>
                                  <?php if ($rabu['dokter_id'] !=''): ?>
                                    <b><?=$rabu['pegawai_nm']?></b><br><?=$rabu['jam']?>
                                  <?php endif; ?>
                                <?php endforeach; ?>
                              <?php endif; ?>
                            </td>
                            <td class="text-center text-top" width="150">
                              <?php if (count($row['rinc_kamis']) > 1): ?>
                                <?php foreach ($row['rinc_kamis'] as $kamis): ?>
                                  <b><?=$kamis['pegawai_nm']?></b><br><?=$kamis['jam']?><hr>
                                <?php endforeach; ?>
                              <?php else: ?>
                                <?php foreach ($row['rinc_kamis'] as $kamis): ?>
                                  <?php if ($kamis['dokter_id'] !=''): ?>
                                    <b><?=$kamis['pegawai_nm']?></b><br><?=$kamis['jam']?>
                                  <?php endif; ?>
                                <?php endforeach; ?>
                              <?php endif; ?>
                            </td>
                            <td class="text-center text-top" width="150">
                              <?php if (count($row['rinc_jumat']) > 1): ?>
                                <?php foreach ($row['rinc_jumat'] as $jumat): ?>
                                  <b><?=$jumat['pegawai_nm']?></b><br><?=$jumat['jam']?><hr>
                                <?php endforeach; ?>
                              <?php else: ?>
                                <?php foreach ($row['rinc_jumat'] as $jumat): ?>
                                  <?php if ($jumat['dokter_id'] !=''): ?>
                                    <b><?=$jumat['pegawai_nm']?></b><br><?=$jumat['jam']?>
                                  <?php endif; ?>
                                <?php endforeach; ?>
                              <?php endif; ?>
                            </td>
                            <td class="text-center text-top" width="150">
                              <?php if (count($row['rinc_sabtu']) > 1): ?>
                                <?php foreach ($row['rinc_sabtu'] as $sabtu): ?>
                                  <b><?=$sabtu['pegawai_nm']?></b><br><?=$sabtu['jam']?><hr>
                                <?php endforeach; ?>
                              <?php else: ?>
                                <?php foreach ($row['rinc_sabtu'] as $sabtu): ?>
                                  <?php if ($sabtu['dokter_id'] !=''): ?>
                                    <b><?=$sabtu['pegawai_nm']?></b><br><?=$sabtu['jam']?>
                                  <?php endif; ?>
                                <?php endforeach; ?>
                              <?php endif; ?>
                            </td>
                            <td class="text-center text-top" width="150">
                              <?php if (count($row['rinc_minggu']) > 1): ?>
                                <?php foreach ($row['rinc_minggu'] as $minggu): ?>
                                  <b><?=$minggu['pegawai_nm']?></b><br><?=$minggu['jam']?><hr>
                                <?php endforeach; ?>
                              <?php else: ?>
                                <?php foreach ($row['rinc_minggu'] as $minggu): ?>
                                  <?php if ($minggu['dokter_id'] !=''): ?>
                                    <b><?=$minggu['pegawai_nm']?></b><br><?=$minggu['jam']?>
                                  <?php endif; ?>
                                <?php endforeach; ?>
                              <?php endif; ?>
                            </td>
                          </tr>
                        <?php endforeach;?>
                      </tbody>
                    <?php endif;?>
                  </table>
                </form>
              </div>
            </div>
          </div><!-- /.row -->
          <div class="row mt-2">
            <div class="col-6 text-middle pt-1">
              <?php if($nav['_update'] == 1 || $nav['_delete'] == 1):?>
                <div class="input-group-prepend">
                  <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    Aksi Multi
                  </button>
                  <div class="dropdown-menu">
                    <?php if($nav['_update'] == 1):?>
                      <a class="dropdown-item" href="javascript:multipleAction('enable')">Aktif</a>
                      <a class="dropdown-item" href="javascript:multipleAction('disable')">Non Aktif</a>
                    <?php endif; ?>
                    <?php if($nav['_delete'] == 1):?>
                      <a class="dropdown-item" href="javascript:multipleAction('delete')">Hapus</a>
                    <?php endif; ?>
                  </div>
                </div>
              <?php endif; ?>
            </div>
            <div class="col-6 text-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>