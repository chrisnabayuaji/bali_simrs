<script type="text/javascript">

  $(document).ready(function () {
    var form = $("#form-data").validate( {
      rules: {
        lokasi_id:{
          valueNotEquals: "",
          remote: {
            url: '<?=site_url().'/'.$nav['nav_url']?>/ajax/cek_id/<?=@$main['lokasi_id']?>',
            type: 'post',
            data: {
              lokasi_id: function() {
                return $("#lokasi_id").val();
              }
            }
          }
        },
        periode_tgl_awal: {
          dateID: true
        },
        periode_tgl_akhir: {
          dateID: true
        }
      },
      messages: {
        lokasi_id : {
          valueNotEquals: "Pilih salah satu!",
          remote : 'Lokasi sudah terdaftar!'
        }
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container"));
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    get_antrian_cd();

     // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');
    $('.chosen-select').on('select2:select', function (e) {
      form.element("#lokasi_id");
      get_antrian_cd();
    });
    //DATEPICKER ============================================================================================================
    $('.datepicker').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })
    //END DATEPICKER ============================================================================================================
  })

  function get_antrian_cd() {
    var lokasi_id = $("#lokasi_id").val();
    $.ajax({
      type : 'post',
      url : '<?=site_url().'/'.$nav['nav_url'].'/ajax/get_antrian_cd'?>',
      dataType : 'json',
      data : 'lokasi_id='+lokasi_id,
      success : function (data) {
        if (data != null) {
          $("#antrian_cd").val(data.antrian_cd);
        }
      }
    })
  }
</script>