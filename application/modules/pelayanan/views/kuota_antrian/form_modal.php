<!-- js -->
<?php $this->load->view('pelayanan/kuota_antrian/_js')?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=$form_action?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-5 col-md-3 col-form-label">Lokasi <span class="text-danger">*<span></label>
        <div class="col-lg-7 col-md-5">
          <select class="chosen-select custom-select w-100" name="lokasi_id" id="lokasi_id">
            <option value="">- Pilih -</option>
            <?php foreach($lokasi as $r):?>
            <option value="<?=$r['lokasi_id']?>" <?php if($r['lokasi_id'] == @$main['lokasi_id']) echo 'selected'?>><?=$r['lokasi_id']?> - <?=$r['lokasi_nm']?></option>
            <?php endforeach;?>
          </select>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-5 col-md-3 col-form-label">Periode Tanggal Awal <span class="text-danger">*<span></label>
        <div class="col-lg-4 col-md-9">
          <input type="text" class="form-control datepicker" name="periode_tgl_awal" id="periode_tgl_awal" value="<?=@to_date($main['periode_tgl_awal'])?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-5 col-md-3 col-form-label">Periode Tanggal Akhir <span class="text-danger">*<span></label>
        <div class="col-lg-4 col-md-9">
          <input type="text" class="form-control datepicker" name="periode_tgl_akhir" id="periode_tgl_akhir" value="<?=@to_date($main['periode_tgl_akhir'])?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-5 col-md-3 col-form-label">Kode Antrian <span class="text-danger">*<span></label>
        <div class="col-lg-4 col-md-9">
          <input type="text" class="form-control" name="antrian_cd" id="antrian_cd" value="<?=@$main['antrian_cd']?>" readonly>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-5 col-md-3 col-form-label">Antrian No <span class="text-danger">*<span></label>
        <div class="col-lg-4 col-md-9">
          <input type="text" class="form-control" name="antrian_no" id="antrian_no" value="<?=@$main['antrian_no']?>" required>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-5 col-md-3 col-form-label">Warna <span class="text-danger">*<span></label>
        <div class="col-lg-4 col-md-5">
          <select class="chosen-select custom-select w-100" name="warna" id="warna">
            <option value="success" <?php if('success' == @$main['warna']) echo 'selected'?>>Hijau</option>
            <option value="danger" <?php if('danger' == @$main['warna']) echo 'selected'?>>Merah</option>
            <option value="warning" <?php if('warning' == @$main['warna']) echo 'selected'?>>Kuning</option>
            <option value="info" <?php if('info' == @$main['warna']) echo 'selected'?>>Biru</option>
          </select>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>