<script type="text/javascript">
  var alkes_form;
  $(document).ready(function () {
    var alkes_form = $("#alkes_form").validate( {
      rules: {
        barang_id: { valueNotEquals: "" },
        qty: { valueNotEquals: "" }
      },
      messages: {
        barang_id: { valueNotEquals: "Pilih salah satu!" },
        qty: { valueNotEquals: "Kosong!" }
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('select2')){
          error.insertAfter(element.next(".select2-container"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container"));
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $("#alkes_action").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#alkes_action").attr("disabled", "disabled");
        $("#alkes_cancel").attr("disabled", "disabled");
        var pasien_id = $("#pasien_id").val();
        var reg_id = $("#reg_id").val();
        var lokasi_id = $("#lokasi_id").val();
        $.ajax({
          type : 'post',
          url : '<?=site_url($nav['nav_url'])?>/ajax_alkes/save',
          data : $(form).serialize()+'&pasien_id='+pasien_id+'&reg_id='+reg_id+'&lokasi_id='+lokasi_id,
          success : function (data) {
            alkes_reset();
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil disimpan.',
              icon: 'success',
              position: 'top-right'
            })
            $("#alkes_action").html('<i class="fas fa-save"></i> Simpan');
            $("#alkes_action").attr("disabled", false);
            $("#alkes_cancel").attr("disabled", false);
            alkes_data(pasien_id, reg_id, lokasi_id);
          }
        })
        return false;
      }
    });

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    // autocomplete
    $('#barang_id_alkes').select2({
      minimumInputLength: 2,
      ajax: {
        url: "<?=site_url($nav['nav_url'])?>/ajax_alkes/autocomplete",
        dataType: "json",
        
        data: function(params) {
          return{
            obat_nm : params.term,
            map_lokasi_depo : $('#map_lokasi_depo').val()
          };
        },
        processResults: function(data){
          return{
            results: data
          }
        }
      }
    });
    $('.select2-container').css('width', '100%');

    var pasien_id = $("#pasien_id").val();
    var reg_id = $("#reg_id").val();
    var lokasi_id = $("#lokasi_id").val();
    alkes_data(pasien_id, reg_id, lokasi_id);

    $("#pills-history-alkes-tab").on('click', function () {
      alkes_history(pasien_id, reg_id, lokasi_id);
    });

    $("#alkes_cancel").on('click', function () {
      alkes_reset();
    });

    $('.modal-href').click(function (e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");
      
      $("#modal-title").html(modal_title);
      $("#modal-size").addClass('modal-' + modal_size);
      if(modal_custom_size){
        $("#modal-size").attr('style', 'max-width: '+modal_custom_size+'px !important');
      }
      $("#myModal").modal('show');
      $.post($(this).data('href'), function (data) {
        $("#modal-body").html(data.html);
      }, 'json');
    });

    $('.datetimepicker').daterangepicker({
      startDate: moment("<?=date('Y-m-d H:i:s')?>"),
      endDate: moment(),
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })

  })

  function alkes_data(pasien_id='', reg_id='', lokasi_id='') {
    $('#alkes_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_alkes/data'?>', {pasien_id: pasien_id, reg_id: reg_id, lokasi_id: lokasi_id}, function (data) {
      $('#alkes_data').html(data.html);
    }, 'json');
  }

  function alkes_history(pasien_id='', reg_id='', lokasi_id='') {
    $('#alkes_history').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_alkes/history'?>', {pasien_id: pasien_id, reg_id: reg_id, lokasi_id: lokasi_id}, function (data) {
      $('#alkes_history').html(data.html);
    }, 'json');
  }

  function alkes_reset() {
    $("#alkes_id").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#barang_id_alkes").val('').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#stokdepo_id").val('').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#stokdepo_id_sebelum").val('').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#qty").val('1').removeClass("is-valid").removeClass("is-invalid");
    $("#qty_sebelum").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#keterangan_alkes").val('').removeClass("is-valid").removeClass("is-invalid");
    $('#alkes_action').html('<i class="fas fa-save"></i> Simpan');

    $('.datetimepicker').daterangepicker({
      startDate: moment("<?=date('Y-m-d H:i:s')?>"),
      endDate: moment(),
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })
  }

  function alkes_fill(id, stokdepo_id = null) {
    $.ajax({
      type : 'post',
      url : '<?=site_url($nav['nav_url'].'/ajax_alkes/alkes_fill')?>',
      dataType : 'json',
      data : 'obat_id='+id+'&stokdepo_id='+stokdepo_id,
      success : function (data) {
        // autocomplete
        var data2 = {
          id: data.obat_id+'#'+data.obat_nm+'#'+data.stokdepo_id,
          text: data.obat_id+' - '+data.obat_nm
        };
        var newOption = new Option(data2.text, data2.id, false, false);
        $('#barang_id_alkes').append(newOption).trigger('change');
        $('#barang_id_alkes').val(data.obat_id+'#'+data.obat_nm+'#'+data.stokdepo_id);
        $('#barang_id_alkes').trigger('change');
        $('.select2-container').css('width', '100%');
        //update stokdepo_id
        $("#stokdepo_id").val(data.stokdepo_id);
        $("#myModal").modal('hide');
      }
    })
  }
</script>