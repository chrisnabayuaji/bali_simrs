<script type="text/javascript">
  var catatan_medis_form;
  $(document).ready(function() {
    var catatan_medis_form = $("#catatan_medis_form").validate({
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $("#catatan_medis_action").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#catatan_medis_action").attr("disabled", "disabled");
        $("#catatan_medis_cancel").attr("disabled", "disabled");
        var pasien_id = $("#pasien_id").val();
        var reg_id = $("#reg_id").val();
        var lokasi_id = $("#lokasi_id").val();
        $.ajax({
          type: 'post',
          url: '<?= site_url($nav['nav_url']) ?>/ajax_catatan_medis/save',
          data: $(form).serialize() + '&pasien_id=' + pasien_id + '&reg_id=' + reg_id + '&lokasi_id=' + lokasi_id,
          success: function(data) {
            catatan_medis_reset();
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil disimpan.',
              icon: 'success',
              position: 'top-right'
            })
            $("#catatan_medis_action").html('<i class="fas fa-save"></i> Simpan');
            $("#catatan_medis_action").attr("disabled", false);
            $("#catatan_medis_cancel").attr("disabled", false);
            catatan_medis_data(pasien_id, reg_id, lokasi_id);
          }
        })
        $(form).submit(function(e) {
          return false;
        });
        window.location.replace("<?= site_url($nav['nav_url']) ?>/form/"+reg_id+"#catatan-medis");
      }
    });

    var pasien_id = $("#pasien_id").val();
    var reg_id = $("#reg_id").val();
    var lokasi_id = $("#lokasi_id").val();
    catatan_medis_data(pasien_id, reg_id, lokasi_id);

    $("#pills-history-catatan-medis-tab").on('click', function() {
      catatan_medis_history(pasien_id, reg_id, lokasi_id);
    });

    $("#catatan_medis_cancel").on('click', function() {
      catatan_medis_reset();
    });

    $('.datetimepicker').daterangepicker({
      startDate: moment("<?= date('Y-m-d H:i:s') ?>"),
      endDate: moment(),
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    // autocomplete
    $('#petugas_id_catatan_medis').select2({
      minimumInputLength: 2,
      ajax: {
        url: "<?= site_url($nav['nav_url']) ?>/ajax_catatan_medis/autocomplete",
        dataType: "json",

        data: function(params) {
          return {
            pegawai_nm: params.term
          };
        },
        processResults: function(data) {
          return {
            results: data
          }
        }
      }
    });
    $('.select2-container').css('width', '100%');

    $('.modal-href').click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");

      $("#modal-title").html(modal_title);
      $("#modal-size").addClass('modal-' + modal_size);
      if (modal_custom_size) {
        $("#modal-size").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
      }
      $("#myModal").modal('show');
      $.post($(this).data('href'), function(data) {
        $("#modal-body").html(data.html);
      }, 'json');
    });

  })

  function catatan_medis_data(pasien_id = '', reg_id = '', lokasi_id = '') {
    $('#catatan_medis_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_catatan_medis/data' ?>', {
      pasien_id: pasien_id,
      reg_id: reg_id,
      lokasi_id: lokasi_id
    }, function(data) {
      $('#catatan_medis_data').html(data.html);
    }, 'json');
  }

  function catatan_medis_history(pasien_id = '', reg_id = '', lokasi_id = '') {
    $('#catatan_medis_history').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_catatan_medis/history' ?>', {
      pasien_id: pasien_id,
      reg_id: reg_id,
      lokasi_id: lokasi_id
    }, function(data) {
      $('#catatan_medis_history').html(data.html);
    }, 'json');
  }

  function catatan_medis_reset() {
    $("#catatanmedis_id").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#riwayat_penyakit").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#alergi_obat").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#petugas_id_catatan_medis").val('').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $('#catatan_medis_action').html('<i class="fas fa-save"></i> Simpan');

    $('.datetimepicker').daterangepicker({
      startDate: moment("<?= date('Y-m-d H:i:s') ?>"),
      endDate: moment(),
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })
  }

  function petugas_fill(id) {
    $.ajax({
      type: 'post',
      url: '<?= site_url($nav['nav_url'] . '/ajax_catatan_medis/petugas_fill') ?>',
      dataType: 'json',
      data: 'pegawai_id=' + id,
      success: function(data) {
        // autocomplete
        var data2 = {
          id: data.pegawai_id,
          text: data.pegawai_nm
        };
        var newOption = new Option(data2.text, data2.id, false, false);
        $('#petugas_id_catatan_medis').append(newOption).trigger('change');
        $('#petugas_id_catatan_medis').val(data.pegawai_id);
        $('#petugas_id_catatan_medis').trigger('change');
        $('.select2-container').css('width', '100%');
        $("#myModal").modal('hide');
      }
    })
  }
</script>