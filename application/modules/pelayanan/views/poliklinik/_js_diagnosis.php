<script type="text/javascript">
  var diagnosis_form;
  $(document).ready(function() {
    var diagnosis_form = $("#diagnosis_form").validate({
      rules: {
        jenisdiagnosis_cd: {
          valueNotEquals: ""
        },
      },
      messages: {
        jenisdiagnosis_cd: {
          valueNotEquals: "Pilih salah satu!"
        },
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $("#diagnosis_action").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#diagnosis_action").attr("disabled", "disabled");
        $("#diagnosis_cancel").attr("disabled", "disabled");
        var pasien_id = $("#pasien_id").val();
        var reg_id = $("#reg_id").val();
        var lokasi_id = $("#lokasi_id").val();
        $.ajax({
          type: 'post',
          url: '<?= site_url($nav['nav_url']) ?>/ajax_diagnosis/save',
          data: $(form).serialize() + '&pasien_id=' + pasien_id + '&reg_id=' + reg_id + '&lokasi_id=' + lokasi_id,
          success: function(data) {
            diagnosis_reset();
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil disimpan.',
              icon: 'success',
              position: 'top-right'
            })
            $("#diagnosis_action").html('<i class="fas fa-save"></i> Simpan');
            $("#diagnosis_action").attr("disabled", false);
            $("#diagnosis_cancel").attr("disabled", false);
            diagnosis_data(pasien_id, reg_id, lokasi_id);
          }
        })
        return false;
      }
    });

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    // autocomplete
    $('#penyakit_id').select2({
      minimumInputLength: 2,
      ajax: {
        url: "<?= site_url($nav['nav_url']) ?>/ajax_diagnosis/autocomplete",
        dataType: "json",

        data: function(params) {
          return {
            penyakit_nm: params.term
          };
        },
        processResults: function(data) {
          return {
            results: data
          }
        }
      }
    });
    $('.select2-container').css('width', '100%');

    var pasien_id = $("#pasien_id").val();
    var reg_id = $("#reg_id").val();
    var lokasi_id = $("#lokasi_id").val();
    diagnosis_data(pasien_id, reg_id, lokasi_id);

    $("#pills-history-diagnosis-tab").on('click', function() {
      diagnosis_history(pasien_id, reg_id, lokasi_id);
    });

    $("#diagnosis_cancel").on('click', function() {
      diagnosis_reset();
    });

    $('.modal-href').click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");

      $("#modal-title").html(modal_title);
      $("#modal-size").addClass('modal-' + modal_size);
      if (modal_custom_size) {
        $("#modal-size").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
      }
      $("#myModal").modal('show');
      $.post($(this).data('href'), function(data) {
        $("#modal-body").html(data.html);
      }, 'json');
    });

    $('.datetimepicker').daterangepicker({
      startDate: moment("<?= date('Y-m-d H:i:s') ?>"),
      endDate: moment(),
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })

  })

  function diagnosis_data(pasien_id = '', reg_id = '', lokasi_id = '') {
    $('#diagnosis_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_diagnosis/data' ?>', {
      pasien_id: pasien_id,
      reg_id: reg_id,
      lokasi_id: lokasi_id
    }, function(data) {
      $('#diagnosis_data').html(data.html);
    }, 'json');
  }

  function diagnosis_history(pasien_id = '', reg_id = '', lokasi_id = '') {
    $('#diagnosis_history').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_diagnosis/history' ?>', {
      pasien_id: pasien_id,
      reg_id: reg_id,
      lokasi_id: lokasi_id
    }, function(data) {
      $('#diagnosis_history').html(data.html);
    }, 'json');
  }

  function diagnosis_reset() {
    $("#diagnosis_id").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#penyakit_id").val('').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#jenisdiagnosis_cd").val('B').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#tipediagnosis_cd").val('').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#keterangan_diagnosis").val('').removeClass("is-valid").removeClass("is-invalid");
    $('#diagnosis_action').html('<i class="fas fa-save"></i> Simpan');

    $('.datetimepicker').daterangepicker({
      startDate: moment("<?= date('Y-m-d H:i:s') ?>"),
      endDate: moment(),
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })
  }

  function diagnosis_fill(id) {
    $.ajax({
      type: 'post',
      url: '<?= site_url($nav['nav_url'] . '/ajax_diagnosis/diagnosis_fill') ?>',
      dataType: 'json',
      data: 'penyakit_id=' + id,
      success: function(data) {
        // autocomplete
        var data2 = {
          id: data.penyakit_id + '#' + data.icdx,
          text: data.icdx + ' - ' + data.penyakit_nm
        };
        var newOption = new Option(data2.text, data2.id, false, false);
        $('#penyakit_id').append(newOption).trigger('change');
        $('#penyakit_id').val(data.penyakit_id + '#' + data.icdx);
        $('#penyakit_id').trigger('change');
        $('.select2-container').css('width', '100%');
        $("#myModal").modal('hide');
      }
    })
  }
</script>