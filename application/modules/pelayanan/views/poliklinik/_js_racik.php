<script type="text/javascript">
  var racik_form, komposisi_form;
  var komposisi = [];
  $(document).ready(function() {
    var racik_form = $("#racik_form").validate({
      rules: {
        resep_qty: {
          number: true
        }
      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $("#resep_action").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#resep_action").attr("disabled", "disabled");
        $("#resep_cancel").attr("disabled", "disabled");
        var pasien_id = $("#pasien_id").val();
        var reg_id = $("#reg_id").val();
        var lokasi_id = $("#lokasi_id").val();
        var dokter_id = $("#dokter_id").val();
        $.ajax({
          type: 'post',
          url: '<?= site_url($nav['nav_url']) ?>/ajax_racik/save',
          data: $(form).serialize() + '&pasien_id=' + pasien_id + '&reg_id=' + reg_id + '&lokasi_id=' + lokasi_id + '&dokter_id=' + dokter_id,
          success: function(data) {
            racik_reset();
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil disimpan.',
              icon: 'success',
              position: 'top-right'
            })
            $("#resep_action").html('<i class="fas fa-save"></i> Simpan');
            $("#resep_action").attr("disabled", false);
            $("#resep_cancel").attr("disabled", false);
            racik_data(pasien_id, reg_id, lokasi_id);
          }
        })
        return false;
      }
    });

    var komposisi_form = $("#komposisi_form").validate({
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        var data = objectifyForm($(form).serializeArray());
        var alias = '';
        if(data.komposisi_alias != ''){
          alias = ' ('+ data.komposisi_alias + ')';
        }
        var html = '<tr>' +
          '<td class="text-center">' +
          '<button type="button" class="btn btn-danger btn-table btn-delete-komposisi"><i class="fas fa-trash-alt"></i></button>' +
          '<input type="hidden" name="komposisi_id[]" value="">' +
          '</td>' +
          '<td>' +
          data.komposisi + alias +
          '<input type="hidden" name="komposisi[]" value="' + data.komposisi + '">' +
          '<input type="hidden" name="komposisi_alias[]" value="' + data.komposisi_alias + '">' +
          '</td>' +
          '<td class="text-right">' +
          data.komposisi_qty +
          '<input type="hidden" name="komposisi_qty[]" value="' + data.komposisi_qty + '">' +
          '</td>' +
          '</tr>';
        $("#komposisi_data").append(html);
        $("#komposisi").val('').removeClass("is-valid").removeClass("is-invalid");
        $("#komposisi_alias").val('').removeClass("is-valid").removeClass("is-invalid");
        $("#komposisi_qty").val(0).removeClass("is-valid").removeClass("is-invalid");
        $("#modal_komposisi").modal('hide');
        return false;
      }
    });

    $("#table_komposisi").on("click", ".btn-delete-komposisi", function() {
      $(this).closest("tr").remove();
    });

    $("#btn_add_komposisi").on('click', () => {
      $("#komposisi").val('').removeClass("is-valid").removeClass("is-invalid");
      $("#komposisi_alias").val('').removeClass("is-valid").removeClass("is-invalid");
      $("#komposisi_qty").val(0).removeClass("is-valid").removeClass("is-invalid");
      $("#modal_komposisi").modal('show');
    })

    $("#rincian_action").on('click', () => {
      $("#racik_form").submit();
    })

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    // autocomplete
    $('#penyakit_id').select2({
      minimumInputLength: 2,
      ajax: {
        url: "<?= site_url($nav['nav_url']) ?>/ajax_resep/autocomplete",
        dataType: "json",
        data: function(params) {
          return {
            penyakit_nm: params.term
          };
        },
        processResults: function(data) {
          return {
            results: data
          }
        }
      }
    });
    $('.select2-container').css('width', '100%');

    var pasien_id = $("#pasien_id").val();
    var reg_id = $("#reg_id").val();
    var lokasi_id = $("#lokasi_id").val();
    racik_data(pasien_id, reg_id, lokasi_id);

    $("#pills-history-resep-tab").on('click', function() {
      resep_history(pasien_id, reg_id, lokasi_id);
    });

    $("#resep_cancel").on('click', function() {
      racik_reset();
    });

    $('.modal-href').click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");

      $("#modal-title").html(modal_title);
      $("#modal-size").addClass('modal-' + modal_size);
      if (modal_custom_size) {
        $("#modal-size").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
      }
      $("#myModal").modal('show');
      $.post($(this).data('href'), function(data) {
        $("#modal-body").html(data.html);
      }, 'json');
    });
  })

  function objectifyForm(formArray) {
    //serialize data function
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
      returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
  }

  function racik_data(pasien_id='', reg_id='', lokasi_id='') {
    $('#racik_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_racik/data' ?>', {pasien_id: pasien_id, reg_id: reg_id, lokasi_id: lokasi_id}, function (data) {
      $('#racik_data').html(data.html);
    }, 'json');
  }

  // function resep_history(pasien_id='', reg_id='', lokasi_id='') {
  //   $('#resep_history').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
  //   $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_resep/history' ?>', {pasien_id: pasien_id, reg_id: reg_id, lokasi_id: lokasi_id}, function (data) {
  //     $('#resep_history').html(data.html);
  //   }, 'json');
  // }

  function racik_reset() {
    $("#resep_id").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#rincian_id").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#obat_nm").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#racik_rute_cd").val('01').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#qty").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#aturan_jml").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#racik_satuan_cd").val('01').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#aturan_waktu").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#racik_waktu_cd").val('01').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#jadwal").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#aturan_tambahan").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#komposisi_data").html('');
    $('#resep_action').html('<i class="fas fa-save"></i> Simpan');
  }
</script>