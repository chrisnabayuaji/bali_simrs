<?php if(@$main == null):?>
	<tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else: ?>
	<?php $i=1;foreach($main as $row):?>
	<tr>
		<td class="text-center"><?=$i++?></td>
		<td class="text-center">
		  <a href="javascript:void(0)" class="btn btn-primary btn-table btn-edit" data-diagnosis-id="<?=$row['diagnosis_id']?>" data-reg-id="<?=$row['reg_id']?>" data-pasien-id="<?=$row['pasien_id']?>" title="Ubah Data"><i class="fas fa-pencil-alt"></i></a>
		  <a href="javascript:void(0)" class="btn btn-danger btn-table btn-delete" data-diagnosis-id="<?=$row['diagnosis_id']?>" data-reg-id="<?=$row['reg_id']?>" data-pasien-id="<?=$row['pasien_id']?>" data-lokasi-id="<?=$row['lokasi_id']?>" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
		</td>
    <td class="text-center"><?=$row['penyakit_id']?></td>
    <td class="text-left"><?=$row['icdx']?> - <?=$row['penyakit_nm']?></td>
    <td class="text-center"><?=get_parameter_value('jenisdiagnosis_cd', $row['jenisdiagnosis_cd'])?></td>
    <td class="text-center"><?=get_parameter_value('tipediagnosis_cd', $row['tipediagnosis_cd'])?></td>
    <td class="text-left"><?=$row['keterangan_diagnosis']?></td>
	</tr>
	<?php endforeach; ?>
<?php endif; ?>

<script type="text/javascript">
$(document).ready(function () {
  // edit
  $('.btn-edit').bind('click',function(e) {
    e.preventDefault();
    var diagnosis_id = $(this).attr("data-diagnosis-id");
    var reg_id = $(this).attr("data-reg-id");
    var pasien_id = $(this).attr("data-pasien-id");
    //
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_diagnosis/get_data'?>', {diagnosis_id: diagnosis_id, reg_id: reg_id, pasien_id: pasien_id}, function (data) {

      $('.datetimepicker').daterangepicker({
        startDate: moment(data.main.tgl_catat),
        endDate: moment(),
        timePicker: true,
        timePicker24Hour: true,
        timePickerSeconds: true,
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
          cancelLabel: 'Clear',
          format: 'DD-MM-YYYY H:mm:ss'
        },
        isInvalidDate: function(date) {
          return '';
        }
      })
      
      $('#diagnosis_id').val(data.main.diagnosis_id);

      // autocomplete
      var data2 = {
        id: data.main.penyakit_id+'#'+data.main.icdx,
        text: data.main.icdx+' - '+data.main.penyakit_nm
      };
      var newOption = new Option(data2.text, data2.id, false, false);
      $('#penyakit_id').append(newOption).trigger('change');
      $('#penyakit_id').val(data.main.penyakit_id+'#'+data.main.icdx);
      $('#penyakit_id').trigger('change');
      $('.select2-container').css('width', '100%');

      $("#jenisdiagnosis_cd").val(data.main.jenisdiagnosis_cd).trigger('change').removeClass("is-valid").removeClass("is-invalid");
      $("#tipediagnosis_cd").val(data.main.tipediagnosis_cd).trigger('change').removeClass("is-valid").removeClass("is-invalid");
      $("#keterangan_diagnosis").val(data.main.keterangan_diagnosis).removeClass("is-valid").removeClass("is-invalid");
      $('#diagnosis_action').html('<i class="fas fa-save"></i> Ubah');
    }, 'json');
  });
  // delete
  $('.btn-delete').on('click', function (e) {
    e.preventDefault();
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#eb3b5a',
      cancelButtonColor: '#b2bec3',
      confirmButtonText: 'Hapus',
      cancelButtonText: 'Batal',
      customClass: 'swal-wide'
    }).then((result) => {
      if (result.value) {
        var diagnosis_id = $(this).attr("data-diagnosis-id");
		    var reg_id = $(this).attr("data-reg-id");
		    var pasien_id = $(this).attr("data-pasien-id");
		    var lokasi_id = $(this).attr("data-lokasi-id");
		    //
		    $('#diagnosis_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
		    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_diagnosis/delete_data'?>', {diagnosis_id: diagnosis_id, reg_id: reg_id, pasien_id: pasien_id, lokasi_id: lokasi_id}, function (data) {
		    	$.toast({
            heading: 'Sukses',
            text: 'Data berhasil dihapus.',
            icon: 'success',
            position: 'top-right'
          })
		      $('#diagnosis_data').html(data.html);
		    }, 'json');
      }
    })
  })
});
</script>