<script type="text/javascript">
  $(document).ready(function() {
    var reg_id = <?= @$reg_id ?>;

    history_detail_catatan_medis(reg_id);
    history_detail_pemeriksaan_fisik(reg_id);
    history_detail_diagnosis(reg_id);
    history_detail_tindakan(reg_id);
    history_detail_resep(reg_id);
    history_detail_resep_racik(reg_id);
    history_detail_bhp(reg_id);
    history_detail_alkes(reg_id);
    history_detail_laboratorium(reg_id);
    history_detail_radiologi(reg_id);
    history_detail_bedah_sentral(reg_id);
    history_detail_vk(reg_id);
    history_detail_icu(reg_id);
    history_detail_hemodialisa(reg_id);
  })

  function history_detail_pemeriksaan_fisik(reg_id = '') {
    $('#history_detail_pemeriksaan_fisik').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_history/history_detail_pemeriksaan_fisik' ?>', {
      reg_id: reg_id
    }, function(data) {
      $('#history_detail_pemeriksaan_fisik').html(data.html);
    }, 'json');
  }

  function history_detail_catatan_medis(reg_id = '') {
    $('#history_detail_catatan_medis').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_history/history_detail_catatan_medis' ?>', {
      reg_id: reg_id
    }, function(data) {
      $('#history_detail_catatan_medis').html(data.html);
    }, 'json');
  }

  function history_detail_diagnosis(reg_id = '') {
    $('#history_detail_diagnosis').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_history/history_detail_diagnosis' ?>', {
      reg_id: reg_id
    }, function(data) {
      $('#history_detail_diagnosis').html(data.html);
    }, 'json');
  }

  function history_detail_tindakan(reg_id = '') {
    $('#history_detail_tindakan').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_history/history_detail_tindakan' ?>', {
      reg_id: reg_id
    }, function(data) {
      $('#history_detail_tindakan').html(data.html);
    }, 'json');
  }

  function history_detail_resep(reg_id = '') {
    $('#history_detail_resep').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_history/history_detail_resep' ?>', {
      reg_id: reg_id
    }, function(data) {
      $('#history_detail_resep').html(data.html);
    }, 'json');
  }

  function history_detail_resep_racik(reg_id = '') {
    $('#history_detail_resep').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_history/history_detail_resep_racik' ?>', {
      reg_id: reg_id
    }, function(data) {
      $('#history_detail_resep_racik').html(data.html);
    }, 'json');
  }

  function history_detail_bhp(reg_id = '') {
    $('#history_detail_bhp').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_history/history_detail_bhp' ?>', {
      reg_id: reg_id
    }, function(data) {
      $('#history_detail_bhp').html(data.html);
    }, 'json');
  }

  function history_detail_alkes(reg_id = '') {
    $('#history_detail_alkes').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_history/history_detail_alkes' ?>', {
      reg_id: reg_id
    }, function(data) {
      $('#history_detail_alkes').html(data.html);
    }, 'json');
  }

  function history_detail_laboratorium(reg_id = '') {
    $('#history_detail_laboratorium').html('<div class="mb-2"><div class="pt-1" style="border-bottom: 2px solid #f2f2f2;"><div class="text-center mb-3"><i class="fas fa-spin fa-spinner"></i><br>Loading</div></div></div>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_history/history_detail_laboratorium' ?>', {
      reg_id: reg_id
    }, function(data) {
      $('#history_detail_laboratorium').html(data.html);
    }, 'json');
  }

  function history_detail_radiologi(reg_id = '') {
    $('#history_detail_radiologi').html('<div class="mb-2"><div class="pt-1" style="border-bottom: 2px solid #f2f2f2;"><div class="text-center mb-3"><i class="fas fa-spin fa-spinner"></i><br>Loading</div></div></div>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_history/history_detail_radiologi' ?>', {
      reg_id: reg_id
    }, function(data) {
      $('#history_detail_radiologi').html(data.html);
    }, 'json');
  }

  function history_detail_bedah_sentral(reg_id = '') {
    $('#history_detail_bedah_sentral').html('<div class="mb-2"><div class="pt-1" style="border-bottom: 2px solid #f2f2f2;"><div class="text-center mb-3"><i class="fas fa-spin fa-spinner"></i><br>Loading</div></div></div>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_history/history_detail_bedah_sentral' ?>', {
      reg_id: reg_id
    }, function(data) {
      $('#history_detail_bedah_sentral').html(data.html);
    }, 'json');
  }

  function history_detail_vk(reg_id = '') {
    $('#history_detail_vk').html('<div class="mb-2"><div class="pt-1" style="border-bottom: 2px solid #f2f2f2;"><div class="text-center mb-3"><i class="fas fa-spin fa-spinner"></i><br>Loading</div></div></div>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_history/history_detail_vk' ?>', {
      reg_id: reg_id
    }, function(data) {
      $('#history_detail_vk').html(data.html);
    }, 'json');
  }

  function history_detail_icu(reg_id = '') {
    $('#history_detail_icu').html('<div class="mb-2"><div class="pt-1" style="border-bottom: 2px solid #f2f2f2;"><div class="text-center mb-3"><i class="fas fa-spin fa-spinner"></i><br>Loading</div></div></div>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_history/history_detail_icu' ?>', {
      reg_id: reg_id
    }, function(data) {
      $('#history_detail_icu').html(data.html);
    }, 'json');
  }

  function history_detail_hemodialisa(reg_id = '') {
    $('#history_detail_hemodialisa').html('<div class="mb-2"><div class="pt-1" style="border-bottom: 2px solid #f2f2f2;"><div class="text-center mb-3"><i class="fas fa-spin fa-spinner"></i><br>Loading</div></div></div>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_history/history_detail_hemodialisa' ?>', {
      reg_id: reg_id
    }, function(data) {
      $('#history_detail_hemodialisa').html(data.html);
    }, 'json');
  }
</script>

<div class="row">
  <div class="col-6 mb-3">
    <table class="table" style="width: 100% !important; border-bottom: 1px solid #f2f2f2;">
      <tbody>
        <tr>
          <td width="150" style="font-weight: 500 !important;">No.Reg</td>
          <td width="20">:</td>
          <td><?= @$main['reg_id'] ?></td>
        </tr>
        <tr>
          <td width="150" style="font-weight: 500 !important;">No.RM / Nama Pasien</td>
          <td width="20">:</td>
          <td><?= @$main['pasien_id'] ?> / <?= @$main['pasien_nm'] ?></td>
        </tr>
        <tr>
          <td width="150">Alamat Pasien</td>
          <td width="20">:</td>
          <td><?= @$main['alamat'] ?>, <?= @$main['kelurahan'] ?>, <?= @$main['kecamatan'] ?>, <?= @$main['kabupaten'] ?>, <?= @$main['provinsi'] ?></td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="col-6 mb-3">
    <table class="table" style="width: 100% !important; border-bottom: 1px solid #f2f2f2;">
      <tbody>
        <tr>
          <td width="150">Dokter PJ</td>
          <td width="20">:</td>
          <td><?= @$main['pegawai_nm'] ?></td>
        </tr>
        <tr>
          <td width="150">Umur / JK</td>
          <td width="20">:</td>
          <td><?= @$main['umur_thn'] ?> Th <?= @$main['umur_bln'] ?> Bl <?= @$main['umur_hr'] ?> Hr / <?= get_parameter_value('sex_cd', @$main['sex_cd']) ?></td>
        </tr>
        <tr>
          <td width="150">Jenis / Asal Pasien</td>
          <td width="20">:</td>
          <td><?= @$main['jenispasien_nm'] ?> / <?= get_parameter_value('asalpasien_cd', @$main['asalpasien_cd']) ?></td>
        </tr>
      </tbody>
    </table>
  </div>
  <div class="col-md-12 mb-3">
    <div class="mb-2">
      <h7><b><i class="fas fa-copy"></i> A. HISTORY CATATAN MEDIS</b></h7>
    </div>
    <div class="table-responsive">
      <table class="table table-bordered table-striped table-sm">
        <thead>
          <tr>
            <th class="text-center text-middle" width="20">No</th>
            <th class="text-center text-middle" width="135">Tanggal</th>
            <th class="text-center text-middle">Riwayat Penyakit</th>
            <th class="text-center text-middle">Alergi Obat</th>
            <th class="text-center text-middle">Lokasi Pelayanan</th>
            <th class="text-center text-middle">Petugas</th>
          </tr>
        </thead>
        <tbody id="history_detail_catatan_medis"></tbody>
      </table>
    </div>
  </div>
  <div class="col-md-12 mb-3">
    <div class="mb-2">
      <h7><b><i class="fas fa-stethoscope"></i> B. HISTORY PEMERIKSAAN FISIK</b></h7>
    </div>
    <div class="table-responsive">
      <table class="table table-bordered table-striped table-sm">
        <thead>
          <tr>
            <th class="text-center text-middle" width="20">No</th>
            <th class="text-center text-middle" width="135">Tanggal</th>
            <th class="text-center text-middle" width="200">Keluhan Utama</th>
            <th class="text-center text-middle">Systole</th>
            <th class="text-center text-middle">Diastole</th>
            <th class="text-center text-middle">Tinggi</th>
            <th class="text-center text-middle">Berat</th>
            <th class="text-center text-middle">Suhu</th>
            <th class="text-center text-middle">Nadi</th>
            <th class="text-center text-middle">Respiration Rate</th>
            <th class="text-center text-middle">SaO<sub>2</sub></th>
            <th class="text-center text-middle">Petugas</th>
          </tr>
        </thead>
        <tbody id="history_detail_pemeriksaan_fisik"></tbody>
      </table>
    </div>
  </div>
  <div class="col-md-12 mb-3">
    <div class="mb-2">
      <h7><b><i class="fas fa-diagnoses"></i> C. HISTORY DIAGNOSIS</b></h7>
    </div>
    <div class="table-responsive">
      <table class="table table-bordered table-striped table-sm">
        <thead>
          <tr>
            <th class="text-center" width="20">No</th>
            <th class="text-center">Tanggal</th>
            <th class="text-center">Kode</th>
            <th class="text-center">Diagnosis</th>
            <th class="text-center">Kasus</th>
            <th class="text-center">Tipe</th>
            <th class="text-center">Keterangan</th>
            <th class="text-center">Lokasi Pelayanan</th>
          </tr>
        </thead>
        <tbody id="history_detail_diagnosis"></tbody>
      </table>
    </div>
  </div>
  <div class="col-md-12 mb-3">
    <div class="mb-2">
      <h7><b><i class="fas fa-procedures"></i> D. HISTORY TINDAKAN</b></h7>
    </div>
    <div class="table-responsive">
      <table class="table table-bordered table-striped table-sm">
        <thead>
          <tr>
            <th class="text-center text-middle" width="20">No</th>
            <th class="text-center text-middle">Tanggal</th>
            <th class="text-center text-middle" width="100">Kode</th>
            <th class="text-center text-middle">Tindakan</th>
            <th class="text-center text-middle">Qty</th>
            <th class="text-center text-middle">Jenis</th>
            <th class="text-center text-middle">Biaya</th>
            <th class="text-center text-middle">Petugas</th>
            <th class="text-center text-middle">Lokasi Pelayanan</th>
          </tr>
        </thead>
        <tbody id="history_detail_tindakan"></tbody>
      </table>
    </div>
  </div>
  <div class="col-md-12 mb-3">
    <div class="mb-2">
      <h7><b><i class="fas fa-pills"></i> E. HISTORY RESEP</b></h7>
    </div>
    <!-- <div class="table-responsive">
      <table class="table table-bordered table-sm">
        <tbody id="history_detail_pemberian_obat"></tbody>
      </table>
    </div> -->
    <h6>E.1. Resep Non Racik</h6>
    <div class="table-responsive">
      <table class="table table-bordered table-sm">
        <tbody id="history_detail_resep"></tbody>
      </table>
    </div>
    <h6 class="mt-2">E.2. Resep Racik</h6>
    <div class="table-responsive">
      <table class="table table-bordered table-sm">
        <tbody id="history_detail_resep_racik"></tbody>
      </table>
    </div>
  </div>
  <div class="col-md-12 mb-3">
    <div class="mb-2">
      <h7><b><i class="fas fa-box-open"></i> F. HISTORY BHP (BARANG HABIS PAKAI)</b></h7>
    </div>
    <div class="table-responsive">
      <table class="table table-bordered table-striped table-sm">
        <thead>
          <tr>
            <th class="text-center" width="20">No</th>
            <th class="text-center" width="180">Tanggal</th>
            <th class="text-center">Kode</th>
            <th class="text-center">Nama BHP</th>
            <th class="text-center">Qty</th>
            <th class="text-center">Keterangan</th>
            <th class="text-center">Lokasi Pelayanan</th>
          </tr>
        </thead>
        <tbody id="history_detail_bhp"></tbody>
      </table>
    </div>
  </div>
  <div class="col-md-12 mb-3">
    <div class="mb-2">
      <h7><b><i class="fas fa-box-open"></i> G. HISTORY ALKES (ALAT KESEHATAN)</b></h7>
    </div>
    <div class="table-responsive">
      <table class="table table-bordered table-striped table-sm">
        <thead>
          <tr>
            <th class="text-center" width="20">No</th>
            <th class="text-center" width="180">Tanggal</th>
            <th class="text-center">Kode</th>
            <th class="text-center">Nama Alkes</th>
            <th class="text-center">Qty</th>
            <th class="text-center">Keterangan</th>
            <th class="text-center">Lokasi Pelayanan</th>
          </tr>
        </thead>
        <tbody id="history_detail_alkes"></tbody>
      </table>
    </div>
  </div>
  <div class="col-md-12 mb-2">
    <div class="pb-2" style="border-bottom: 2px solid #f2f2f2;">
      <h7><b><i class="fas fa-flask"></i> I. HISTORY PENUNJANG LABORATORIUM</b></h7>
    </div>
  </div>
  <div class="col-md-12" id="history_detail_laboratorium"></div>
  <div class="col-md-12 mb-2">
    <div class="pb-2" style="border-bottom: 2px solid #f2f2f2;">
      <h7><b><i class="fas fa-bone"></i> J. HISTORY PENUNJANG RADIOLOGI</b></h7>
    </div>
  </div>
  <div class="col-md-12" id="history_detail_radiologi"></div>
  <div class="col-md-12 mb-2">
    <div class="pb-2" style="border-bottom: 2px solid #f2f2f2;">
      <h7><b><i class="fas fa-user-md"></i> K. HISTORY PENUNJANG BEDAH SENTRAL</b></h7>
    </div>
  </div>
  <div class="col-md-12" id="history_detail_bedah_sentral"></div>
  <div class="col-md-12 mb-2">
    <div class="pb-2" style="border-bottom: 2px solid #f2f2f2;">
      <h7><b><i class="fas fa-baby"></i> L. HISTORY PENUNJANG VK</b></h7>
    </div>
  </div>
  <div class="col-md-12" id="history_detail_vk"></div>
  <div class="col-md-12 mb-2">
    <div class="pb-2" style="border-bottom: 2px solid #f2f2f2;">
      <h7><b><i class="fas fa-procedures"></i> M. HISTORY PENUNJANG ICU</b></h7>
    </div>
  </div>
  <div class="col-md-12" id="history_detail_icu"></div>
  <div class="col-md-12 mb-2">
    <div class="pb-2" style="border-bottom: 2px solid #f2f2f2;">
      <h7><b><i class="fas fa-diagnoses"></i> N. HISTORY PENUNJANG HEMODIALISA</b></h7>
    </div>
  </div>
  <div class="col-md-12" id="history_detail_hemodialisa"></div>
</div>
<hr>
<div class="col-2 offset-md-10 mt-n2">
  <div class="float-right">
    <button type="button" class="btn btn-xs btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Tutup</button>
  </div>
</div>