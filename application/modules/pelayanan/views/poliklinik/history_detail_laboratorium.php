<?php if (@$main == null) : ?>
  <div class="mb-2">
    <div class="pt-1" style="border-bottom: 2px solid #f2f2f2;">
      <div class="text-center mb-3">Tidak ada data!</div>
    </div>
  </div>
<?php else : ?>
  <?php foreach ($main as $row) : ?>
    <script type="text/javascript">
      $(document).ready(function() {
        var reg_id = <?= @$reg_id ?>;
        var pemeriksaan_id = <?= @$row['pemeriksaan_id'] ?>;

        laboratorium_pemeriksaan_data_<?= $row['pemeriksaan_id'] ?>(reg_id, pemeriksaan_id);
        laboratorium_tarif_tindakan_data_<?= $row['pemeriksaan_id'] ?>(reg_id, pemeriksaan_id);
        laboratorium_bhp_data_<?= $row['pemeriksaan_id'] ?>(reg_id, pemeriksaan_id);
        laboratorium_alkes_data_<?= $row['pemeriksaan_id'] ?>(reg_id, pemeriksaan_id);
      })

      function laboratorium_pemeriksaan_data_<?= $row['pemeriksaan_id'] ?>(reg_id = '', pemeriksaan_id = '') {
        $('#laboratorium_pemeriksaan_data_<?= $row['pemeriksaan_id'] ?>').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
        $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_penunjang_laboratorium/laboratorium_pemeriksaan_data' ?>', {
          reg_id: reg_id,
          pemeriksaan_id: pemeriksaan_id
        }, function(data) {
          $('#laboratorium_pemeriksaan_data_<?= $row['pemeriksaan_id'] ?>').html(data.html);
        }, 'json');
      }

      function laboratorium_tarif_tindakan_data_<?= $row['pemeriksaan_id'] ?>(reg_id = '', pemeriksaan_id = '') {
        $('#laboratorium_tarif_tindakan_data_<?= $row['pemeriksaan_id'] ?>').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
        $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_penunjang_laboratorium/laboratorium_tarif_tindakan_data' ?>', {
          reg_id: reg_id,
          pemeriksaan_id: pemeriksaan_id
        }, function(data) {
          $('#laboratorium_tarif_tindakan_data_<?= $row['pemeriksaan_id'] ?>').html(data.html);
        }, 'json');
      }

      function laboratorium_bhp_data_<?= $row['pemeriksaan_id'] ?>(reg_id = '', pemeriksaan_id = '') {
        $('#laboratorium_bhp_data_<?= $row['pemeriksaan_id'] ?>').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
        $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_penunjang_laboratorium/laboratorium_bhp_data' ?>', {
          reg_id: reg_id,
          pemeriksaan_id: pemeriksaan_id
        }, function(data) {
          $('#laboratorium_bhp_data_<?= $row['pemeriksaan_id'] ?>').html(data.html);
        }, 'json');
      }

      function laboratorium_alkes_data_<?= $row['pemeriksaan_id'] ?>(reg_id = '', pemeriksaan_id = '') {
        $('#laboratorium_alkes_data_<?= $row['pemeriksaan_id'] ?>').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
        $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_penunjang_laboratorium/laboratorium_alkes_data' ?>', {
          reg_id: reg_id,
          pemeriksaan_id: pemeriksaan_id
        }, function(data) {
          $('#laboratorium_alkes_data_<?= $row['pemeriksaan_id'] ?>').html(data.html);
        }, 'json');
      }
    </script>
    <div class="mb-2">
      <div class="pt-1" style="border-bottom: 2px solid #f2f2f2;">
        <div class="row">
          <div class="col-md-4 mb-2">
            <div class="mb-2">
              <h7><b><i class="fas fa-clipboard-list"></i> Data Order Laboratorium</b></h7>
            </div>
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-sm">
                <tbody>
                  <tr>
                    <td width="130" class="text-top" style="font-weight: 500 !important;">Tgl. Order</td>
                    <td width="20" class="text-center text-top">:</td>
                    <td><?= to_date($row['tgl_order'], '', 'full_date') ?></td>
                  </tr>
                  <tr>
                    <td width="130" class="text-top" style="font-weight: 500 !important;">Keterangan Order</td>
                    <td width="20" class="text-center text-top">:</td>
                    <td><?= $row['keterangan_order'] ?></td>
                  </tr>
                  <tr>
                    <td width="130" class="text-top" style="font-weight: 500 !important;">Tgl. Diperiksa</td>
                    <td width="20" class="text-center text-top">:</td>
                    <td><?= to_date($row['tgl_diperiksa'], '', 'full_date') ?></td>
                  </tr>
                  <tr>
                    <td width="130" class="text-top" style="font-weight: 500 !important;">Dokter Pengirim</td>
                    <td width="20" class="text-center text-top">:</td>
                    <td><?= $row['dokter_nm'] ?></td>
                  </tr>
                  <tr>
                    <td width="130" class="text-top" style="font-weight: 500 !important;">Lokasi Asal</td>
                    <td width="20" class="text-center text-top">:</td>
                    <td><?= $row['lokasi_nm'] ?></td>
                  </tr>
                  <tr>
                    <td width="130" class="text-top" style="font-weight: 500 !important;">Status</td>
                    <td width="20" class="text-center text-top">:</td>
                    <td><?= ($row['periksa_st'] == 1) ? "Sudah Diperiksa" : "Belum Diperiksa" ?></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="col-md-8 mb-3">
            <div class="mb-3">
              <div class="mb-2">
                <h7><b><i class="fas fa-clipboard-check"></i> Hasil Pemeriksaan Laboratorium</b></h7>
              </div>
              <div class="table-responsive">
                <table class="table table-hover table-bordered table-striped table-sm w-100">
                  <thead>
                    <tr>
                      <th class="text-center" width="20">No.</th>
                      <th class="text-center" width="50">Kode</th>
                      <th class="text-center" width="300">Nama </th>
                      <th class="text-center" width="150">Tgl Hasil </th>
                      <th class="text-center">Nilai Normal </th>
                      <th class="text-center">Hasil Lab </th>
                      <th class="text-center">Catatan </th>
                    </tr>
                  </thead>
                  <tbody id="laboratorium_pemeriksaan_data_<?= $row['pemeriksaan_id'] ?>"></tbody>
                </table>
              </div>
            </div>
            <div class="mb-3">
              <div class="mb-2">
                <h7><b><i class="fas fa-money-bill-alt"></i> Tarif Tindakan Laboratorium</b></h7>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-sm">
                  <thead>
                    <tr>
                      <th class="text-center" width="20">No</th>
                      <th class="text-center" width="135">Kode</th>
                      <th class="text-center" width="300">Tindakan</th>
                      <th class="text-center">Qty</th>
                      <th class="text-center">Biaya</th>
                      <th class="text-center">Petugas</th>
                    </tr>
                  </thead>
                  <tbody id="laboratorium_tarif_tindakan_data_<?= $row['pemeriksaan_id'] ?>"></tbody>
                </table>
              </div>
            </div>
            <div class="mb-3">
              <div class="mb-2">
                <h7><b><i class="fas fa-syringe"></i> BHP Laboratorium</b></h7>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-sm">
                  <thead>
                    <tr>
                      <th class="text-center" width="20">No</th>
                      <th class="text-center" width="135">Kode</th>
                      <th class="text-center" width="300">Nama</th>
                      <th class="text-center">Qty</th>
                      <th class="text-center">Keterangan</th>
                    </tr>
                  </thead>
                  <tbody id="laboratorium_bhp_data_<?= $row['pemeriksaan_id'] ?>"></tbody>
                </table>
              </div>
            </div>
            <div class="mb-3">
              <div class="mb-2">
                <h7><b><i class="fas fa-prescription-bottle-alt"></i> ALKES Laboratorium</b></h7>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-striped table-sm">
                  <thead>
                    <tr>
                      <th class="text-center" width="20">No</th>
                      <th class="text-center" width="135">Kode</th>
                      <th class="text-center" width="300">Nama</th>
                      <th class="text-center">Qty</th>
                      <th class="text-center">Keterangan</th>
                    </tr>
                  </thead>
                  <tbody id="laboratorium_alkes_data_<?= $row['pemeriksaan_id'] ?>"></tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  <?php endforeach; ?>
<?php endif; ?>