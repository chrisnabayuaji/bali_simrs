<?php if (@$main == null) : ?>
  <tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else : ?>
  <tr>
    <td width="150" style="font-weight: 500 !important;">Tanggal Catat</td>
    <td width="20">:</td>
    <td><?= to_date(@$main['tgl_catat'], '', 'full_date') ?></td>
  </tr>
  <tr>
    <td width="150" style="font-weight: 500 !important;">Keluhan Utama</td>
    <td width="20">:</td>
    <td><?= @$main['keluhan_utama'] ?></td>
  </tr>
  <tr>
    <td width="150">Systole</td>
    <td width="20">:</td>
    <td><?= @$main['systole'] ?></td>
  </tr>
  <tr>
    <td width="150">Diastole</td>
    <td width="20">:</td>
    <td><?= @$main['diastole'] ?></td>
  </tr>
  <tr>
    <td width="150">Tinggi</td>
    <td width="20">:</td>
    <td><?= @$main['tinggi'] ?></td>
  </tr>
  <tr>
    <td width="150">Berat</td>
    <td width="20">:</td>
    <td><?= @$main['berat'] ?></td>
  </tr>
  <tr>
    <td width="150">Suhu</td>
    <td width="20">:</td>
    <td><?= @$main['suhu'] ?></td>
  </tr>
  <tr>
    <td width="150">Nadi</td>
    <td width="20">:</td>
    <td><?= @$main['nadi'] ?></td>
  </tr>
  <tr>
    <td width="150">Respiration Rate</td>
    <td width="20">:</td>
    <td><?= @$main['respiration_rate'] ?></td>
  </tr>
  <tr>
    <td width="150">SaO<sub>2</sub></td>
    <td width="20">:</td>
    <td><?= @$main['sao2'] ?></td>
  </tr>
  <tr>
    <td width="150">Petugas</td>
    <td width="20">:</td>
    <td><?= @$main['petugas_nm'] ?></td>
  </tr>
<?php endif; ?>