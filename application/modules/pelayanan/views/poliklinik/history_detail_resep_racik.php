<?php if (@$main == null) : ?>
  <tr>
    <th class="text-center" width="20">No</th>
    <th class="text-center">Nama Obat</th>
    <th class="text-center">Jenis</th>
    <th class="text-center">Dosis</th>
    <th class="text-center">Qty</th>
    <th class="text-center">Aturan Pakai</th>
    <th class="text-center">Jam Minum</th>
    <th class="text-center">Peringatan Khusus</th>
  </tr>
  <tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else : ?>
  <?php foreach ($main as $row) : ?>
    <tr bgcolor="#f5f5f5">
      <td class="text-left" colspan="99"><b>NO.RESEP : <?= $row['resep_id'] ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; TANGGAL : <?= to_date($row['tgl_catat'], '', 'date') ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; LOKASI PELAYANAN : <?= $row['lokasi_nm'] ?></b></td>
    </tr>
    <tr>
      <th class="text-center" width="20">No</th>
      <th class="text-center">Nama Obat/Alias</th>
      <th class="text-center">Rute</th>
      <th class="text-center">Qty</th>
      <th class="text-center">Aturan Pakai</th>
      <th class="text-center">Jadwal Pemberian</th>
      <th class="text-center">Aturan Tambahan</th>
    </tr>
    <?php $i = 1;
    foreach ($row['rinc'] as $rinc) : ?>
      <tr>
        <td class="text-center" width="20"><?= $i++ ?></td>
        <td class="text-left">
          <?= $rinc['obat_nm'] ?>
          <?= ($rinc['obat_alias'] != '') ? ' (' . $rinc['obat_alias'] . ')' : '' ?>
        </td>
        <td><?= $rinc['rute_nm'] ?></td>
        <td><?= $rinc['qty'] ?></td>
        <td><?= $rinc['aturan_jml'] . ' ' . $rinc['satuan_nm'] ?> tiap <?= $rinc['aturan_waktu'] ?> <?= $rinc['waktu_nm'] ?></td>
        <td><?= $rinc['jadwal'] ?></td>
        <td><?= $rinc['aturan_tambahan'] ?></td>
      </tr>
      <?php if (@$rinc['komposisi']): ?>
        <tr>
          <td></td>
          <td colspan="99">
            <table style="width:50%">
              <thead>
                <tr>
                  <th class="text-center" style="border-top: solid rgb(221, 221, 221) 1px" width="30">No</th>
                  <th class="text-center" style="border-top: solid rgb(221, 221, 221) 1px">Komposisi</th>
                  <th class="text-center" style="border-top: solid rgb(221, 221, 221) 1px">Qty</th>
                </tr>
              </thead>
              <tbody>
                <?php $j = 1;foreach($rinc['komposisi'] as $komposisi): ?>
                  <tr>
                    <td class="text-center"><?=$j++?></td>
                    <td><?=$komposisi['komposisi']?></td>
                    <td class="text-center" width="80"><?=$komposisi['komposisi_qty']?></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </td>
        </tr>
      <?php endif;?>
    <?php endforeach; ?>
  <?php endforeach; ?>
<?php endif; ?>