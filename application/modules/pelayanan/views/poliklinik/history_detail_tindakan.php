<?php if (@$main == null) : ?>
  <tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else : ?>
  <?php
  $i = 1;
  $tot_biaya = 0;
  foreach ($main as $row) :
    $tot_biaya += $row['jml_tagihan'];
  ?>
    <tr>
      <td class="text-center"><?= $i++ ?></td>
      <td class="text-center"><?= to_date($row['tgl_catat'], '', 'full_date') ?></td>
      <td class="text-center"><?= $row['tarif_id'] ?></td>
      <td class="text-left"><?= $row['tarif_nm'] ?> - <?= $row['kelas_nm'] ?></td>
      <td class="text-center"><?= $row['qty'] ?></td>
      <td class="text-center"><?= get_parameter_value('jenistindakan_cd', $row['jenistindakan_cd']) ?></td>
      <td class="text-right"><?= num_id($row['jml_tagihan']) ?></td>
      <td class="text-left">
        <?= ($row['pegawai_nm_1'] != '') ? $row['pegawai_nm_1'] . ', <br>' : '' ?>
        <?= ($row['pegawai_nm_2'] != '') ? $row['pegawai_nm_2'] . ', <br>' : '' ?>
        <?= ($row['pegawai_nm_3'] != '') ? $row['pegawai_nm_3'] . ', <br>' : '' ?>
        <?= ($row['pegawai_nm_4'] != '') ? $row['pegawai_nm_4'] . ', <br>' : '' ?>
        <?= ($row['pegawai_nm_5'] != '') ? $row['pegawai_nm_5'] . '' : '' ?>
      </td>
      <td class="text-center"><?= $row['lokasi_nm'] ?></td>
    </tr>
  <?php endforeach; ?>
  <tr>
    <td colspan="6" class="text-right"><b>Total Biaya</b></td>
    <td class="text-right"><b><?= num_id($tot_biaya) ?></b></td>
  </tr>
<?php endif; ?>