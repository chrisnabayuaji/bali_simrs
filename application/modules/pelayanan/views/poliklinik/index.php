<script type="text/javascript">
  $(document).ready(function() {
    <?php if (@$cookie['search']['tgl_registrasi_from'] == '') : ?>
      $('.datepicker-from').val('');
    <?php endif; ?>
    <?php if (@$cookie['search']['tgl_registrasi_to'] == '') : ?>
      $('.datepicker-to').val('');
    <?php endif; ?>
  })
</script>
<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline col-title">
        <div class="col-back">
          <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
        </div>
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          <?= $nav['nav_nm'] ?>
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item"><a href="javascript:void(0)"><i class="fas fa-ambulance"></i> Rawat Jalan</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item active"><span>Index</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>
  <!-- flash message -->
  <div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
  <!-- /flash message -->
  <div class="row full-page mt-4 mb-n2">
    <div class="col-lg-12 grid-margin-xl-0 stretch-card">
      <div class="card border-none">
        <div class="card-body">
          <form id="form-search" action="<?= site_url() . '/app/search/' . $nav['nav_id'] ?>" method="post" autocomplete="off">
            <h4 class="card-title border-bottom border-2 pb-2 mb-2">Filter Data
              <span class="float-right text-gray" data-toggle="collapse" data-target="#container-search" aria-expanded="false" aria-controls="container-search"><i class="fas fa-search"></i></span>
            </h4>
            <div class="row border-dotted collapse show multi-collapse mb-2" id="container-search">
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Tgl.Registrasi</label>
                  <div class="col-lg-8 col-md-8">
                    <div class="row">
                      <div class="col-md-5" style="padding-right: 5px; flex: 0 0 46.4%; max-width: 46.4%;">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datepicker datepicker-from text-center" name="tgl_registrasi_from" id="tgl_registrasi_from" value="<?= @$cookie['search']['tgl_registrasi_from'] ?>">
                        </div>
                      </div>
                      <div class="col-md-2 text-center-group">s.d</div>
                      <div class="col-md-5" style="padding-left: 5px; flex: 0 0 46.4%; max-width: 46.4%;">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datepicker datepicker-to text-center" name="tgl_registrasi_to" id="tgl_registrasi_to" value="<?= @$cookie['search']['tgl_registrasi_to'] ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">No.RM/Nama Pasien</label>
                  <div class="col-lg-8 col-md-8">
                    <input type="text" class="form-control" name="no_rm_nm" id="no_rm_nm" value="<?= @$cookie['search']['no_rm_nm'] ?>">
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Lokasi</label>
                  <div class="col-lg-8 col-md-8">
                    <select class="form-control chosen-select" name="lokasi_id" id="lokasi_id" onchange="$('#form-search').submit()">
                      <option value="">- Semua -</option>
                      <?php foreach ($lokasi as $r) : ?>
                        <?php if (in_array($r['lokasi_id'], $this->session->userdata('sess_lokasi'))) : ?>
                          <option value="<?= $r['lokasi_id'] ?>" <?= (@$cookie['search']['lokasi_id'] == $r['lokasi_id']) ? 'selected' : ''; ?>><?= $r['lokasi_nm'] ?></option>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Jenis Pasien</label>
                  <div class="col-lg-5 col-md-8">
                    <select class="form-control chosen-select" name="jenispasien_id" id="jenispasien_id" onchange="$('#form-search').submit()">
                      <option value="">- Semua -</option>
                      <?php foreach ($jenis_pasien as $r) : ?>
                        <option value="<?= $r['jenispasien_id'] ?>" <?= (@$cookie['search']['jenispasien_id'] == $r['jenispasien_id']) ? 'selected' : ''; ?>><?= $r['jenispasien_nm'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group row">
                  <label class="col-lg-6 col-md-4 col-form-label">Status Periksa</label>
                  <div class="col-lg-6 col-md-8">
                    <select class="form-control chosen-select" name="periksa_st" id="periksa_st" onchange="$('#form-search').submit()">
                      <option value="">- Semua -</option>
                      <option value="1" <?= (@$cookie['search']['periksa_st'] == '1') ? 'selected' : ''; ?>>Sudah</option>
                      <option value="0" <?= (@$cookie['search']['periksa_st'] == '0') ? 'selected' : ''; ?>>Belum</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-6 col-md-4 col-form-label">Status Pulang</label>
                  <div class="col-lg-6 col-md-8">
                    <select class="form-control chosen-select" name="pulang_st" id="pulang_st" onchange="$('#form-search').submit()">
                      <option value="">- Semua -</option>
                      <option value="1" <?= (@$cookie['search']['pulang_st'] == '1') ? 'selected' : ''; ?>>Sudah</option>
                      <option value="0" <?= (@$cookie['search']['pulang_st'] == '0') ? 'selected' : ''; ?>>Belum</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-lg-4 mb-2">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label"></label>
                  <div class="col-lg-8 col-md-8">
                    <button type="submit" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cari Data"><i class="fas fa-search"></i> Cari</button>
                    <a class="btn btn-xs btn-default" href="<?= site_url() . '/app/reset/' . $nav['nav_id'] ?>" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-default" title="" data-original-title="Reset"><i class="fas fa-sync-alt"></i> Reset</a>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                <form id="form-multiple" action="<?= site_url() . '/' . $nav['nav_url'] . '/multiple/enable' ?>" method="post">
                  <table class="table table-striped table-bordered table-fixed">
                    <thead>
                      <tr>
                        <th class="text-center" width="36">No</th>
                        <th class="text-center" width="80">Aksi</th>
                        <th class="text-center" width="80"><?= table_sort($nav['nav_id'], 'Tgl. Reg', 'tgl_registrasi', $cookie['order']) ?></th>
                        <th class="text-center" width="80"><?= table_sort($nav['nav_id'], 'No. RM', 'pasien_id', $cookie['order']) ?></th>
                        <th class="text-center"><?= table_sort($nav['nav_id'], 'Nama Pasien', 'pasien_nm', $cookie['order']) ?></th>
                        <th class="text-center" width="30">JK</th>
                        <th class="text-center" width="70">Umur</th>
                        <th class="text-center">Alamat</th>
                        <th class="text-center" width="110"><?= table_sort($nav['nav_id'], 'Jenis Pasien', 'jenispasien_id', $cookie['order']) ?></th>
                        <th class="text-center" width="170"><?= table_sort($nav['nav_id'], 'Lokasi & Kelas', 'lokasi_nm', $cookie['order']) ?></th>
                        <th class="text-center" width="150">Tindakan</th>
                        <th class="text-center" width="70">Periksa</th>
                        <th class="text-center" width="70">Pulang</th>
                        <th class="text-center" width="150">Tindak Lanjut</th>
                      </tr>
                    </thead>
                    <?php if (@$main == null) : ?>
                      <tbody>
                        <tr>
                          <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                        </tr>
                      </tbody>
                    <?php else : ?>
                      <tbody>
                        <?php $i = 1;
                        foreach ($main as $row) : ?>
                          <tr>
                            <td class="text-center" width="36"><?= $cookie['cur_page'] + ($i++) ?></td>
                            <td class="text-center" width="80">
                              <?php if ($nav['_update'] || $nav['_delete']) : ?>
                                <?php if ($nav['_update']) : ?>
                                  <?php if ($row['tindaklanjut_cd'] != '02') : ?>
                                    <div class="btn-group">
                                      <button type="button" class="btn btn-primary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Aksi
                                      </button>
                                      <div class="dropdown-menu">
                                        <a class="dropdown-item" href="<?= site_url() . '/' . $nav['nav_url'] . '/form/' . $row['reg_id'] ?>#catatan-medis" modal-title="Periksa" modal-size="md"><i class="fas fa-stethoscope"></i> Periksa</a>
                                        <a class="dropdown-item modal-href" href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/piutang_modal/' . $row['reg_id'] ?>?groupreg_in=<?= $row['groupreg_in'] ?>" modal-title="Detail Tagihan" modal-size="lg"><i class="fas fa-list"></i> Detail Tagihan</a>
                                      </div>
                                    </div>
                                  <?php else : ?>
                                    <span class="badge badge-xs badge-warning blink">di <br> Ranapkan</span>
                                  <?php endif; ?>
                                <?php endif; ?>
                              <?php endif; ?>
                            </td>
                            <td class="text-center" width="80"><?= to_date($row['tgl_registrasi'], '', 'full_date', '<br>') ?></td>
                            <td class="text-center" width="80"><?= $row['pasien_id'] ?></td>
                            <td class="text-left"><?= $row['pasien_nm'] . ', ' . $row['sebutan_cd'] ?></td>
                            <td class="text-center" width="30"><?= $row['sex_cd'] ?></td>
                            <td class="text-center" width="70">
                              <?= $row['umur_thn'] ?> tahun <br>
                              <?= $row['umur_bln'] ?> bulan <br>
                              <?= $row['umur_hr'] ?> hari
                            </td>
                            <td class="text-left">
                              <?= ($row['alamat'] != '') ? $row['alamat'] . ', ' : '' ?>
                              <?= ($row['kelurahan'] != '') ? ucwords(strtolower($row['kelurahan'])) . ', ' : '' ?>
                              <?= ($row['kecamatan'] != '') ? ucwords(strtolower($row['kecamatan'])) . ', ' : '' ?>
                              <?= ($row['kabupaten'] != '') ? ucwords(strtolower($row['kabupaten'])) . ', ' : '' ?>
                              <?= ($row['provinsi'] != '') ? ucwords(strtolower($row['provinsi'])) : '' ?>
                            </td>
                            <td class="text-center" width="110"><?= $row['jenispasien_nm'] ?></td>
                            <td class="text-center" width="170"><?= $row['lokasi_nm'] ?> <br> <?= get_kelas_nm($row['kelas_nm']) ?></td>
                            <td class="text-left" width="150">
                              <?= (@$row['check_pendaftaran'] != null) ? '<i class="fas fa-check-circle text-success"></i>' : '<i class="fas fa-ban text-warning"></i>' ?> Pendaftaran</br>
                              <?= (@$row['check_poli'] != null) ? '<i class="fas fa-check-circle text-success"></i>' : '<i class="fas fa-ban text-warning"></i>' ?> Poliklinik</br>
                              <?php if (@$row['check_resep_exist'] == null) : ?>
                                <i class="fas fa-times-circle text-disabled"></i> <del>Farmasi</del>
                              <?php else : ?>
                                <?php if (@$row['check_resep']['resep_st'] == 2) : ?>
                                  <i class="fas fa-check-circle text-success"></i>
                                <?php elseif (@$row['check_resep']['resep_st'] == 1) : ?>
                                  <i class="fas fa-sync-alt text-warning"></i>
                                <?php else : ?>
                                  <i class="fas fa-ban text-warning"></i>
                                <?php endif; ?>
                                Farmasi
                              <?php endif; ?>
                            </td>
                            <td class="text-center" width="70">
                              <?php if ($row['periksa_st'] == '2') : ?>
                                <div class="badge badge-xs badge-success">&nbsp; Selesai &nbsp;</div>
                              <?php else : ?>
                                <i class="fa <?= ($row['periksa_st'] == '1' ? 'fa-check-circle text-success' : 'fa-minus-circle text-danger blink') ?>"></i>
                              <?php endif; ?>
                            </td>
                            <td class="text-center" width="70"><i class="fa <?= ($row['pulang_st'] == '1' ? 'fa-check-circle text-success' : 'fa-minus-circle text-danger blink') ?>"></i></td>
                            <td class="text-left" width="150">
                              <span class="blink text-bold"><?= get_parameter_value('tindaklanjut_cd', $row['tindaklanjut_cd']) ?>
                                <?php if ($row['tindaklanjut_cd'] == '01' || $row['tindaklanjut_cd'] == '02') : ?>
                                  : <br> <?= $row['lokasi_nm_tindaklanjut'] ?>
                                <?php elseif ($row['tindaklanjut_cd'] == '03') : ?>
                                  : <br> <?= $row['rsrujukan_nm'] ?>
                                <?php endif; ?></span>
                            </td>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                    <?php endif; ?>
                  </table>
                </form>
              </div>
            </div>
          </div><!-- /.row -->
          <div class="row mt-2">
            <div class="col-6 text-middle">
              <div class="row">
                <div class="col">
                  <div class="row pagination-info">
                    <div class="col-md-8" style="padding-top:3px !important">
                      <form id="form-paging" action="<?= site_url() . '/app/per_page/' . $nav['nav_id'] ?>" method="post">
                        <label><i class="fas fa-bookmark"></i> Perhalaman</label>
                        <select name="per_page" class="select-pagination" onchange="$('#form-paging').submit()">
                          <option value="10" <?php if ($cookie['per_page'] == 10) {
                                                echo 'selected';
                                              } ?>>10</option>
                          <option value="50" <?php if ($cookie['per_page'] == 50) {
                                                echo 'selected';
                                              } ?>>50</option>
                          <option value="100" <?php if ($cookie['per_page'] == 100) {
                                                echo 'selected';
                                              } ?>>100</option>
                        </select>
                        <label class="ml-2"><?= @$pagination_info ?></label>
                      </form>
                    </div>
                  </div><!-- /.row -->
                </div>
              </div>
            </div>
            <div class="col-6 text-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>