<?php $this->load->view('_js_pemberian_obat'); ?>
<div class="row">
  <div class="col-md-7">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-list"></i> List Data Pemberian Obat</h4>
        <ul class="nav nav-pills nav-pills-primary mt-n2" id="pills-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="pills-saat-ini-pemberian-obat-tab" data-toggle="pill" href="#pills-saat-ini-pemberian-obat" role="tab" aria-controls="pills-saat-ini-pemberian-obat" aria-selected="false"><i class="fas fa-copy"></i> Data Saat Ini</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " id="pills-history-pemberian-obat-tab" data-toggle="pill" href="#pills-history-pemberian-obat" role="tab" aria-controls="pills-history-pemberian-obat" aria-selected="true"><i class="fas fa-history"></i> Data History</a>
          </li>
        </ul>
        <div class="tab-content p-0" id="pills-tabContent-pemberian-obat" style="border:0px !important">
          <div class="tab-pane fade show active" id="pills-saat-ini-pemberian-obat" role="tabpanel" aria-labelledby="pills-saat-ini-pemberian-obat-tab">
            <div class="table-responsive">
              <table class="table table-bordered table-sm">
                <tbody id="pemberian_obat_data"></tbody>
              </table>
            </div>
          </div>
          <div class="tab-pane fade" id="pills-history-pemberian-obat" role="tabpanel" aria-labelledby="pills-history-pemberian-obat-tab">
            <div class="table-responsive">
              <table class="table table-bordered table-sm">
                <tbody id="pemberian_obat_history"></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-5">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-pills"></i> Pemberian Obat</h4>
        <form id="pemberian_obat_form" action="" method="post" autocomplete="off">
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Tanggal Catat <span class="text-danger">*</span></label>
            <div class="col-lg-5 col-md-6">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                </div>
                <input type="text" class="form-control" name="tgl_catat" id="tgl_catat" value="<?= date('d-m-Y H:i:s') ?>" required="" aria-invalid="false" readonly>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">No. Register <span class="text-danger">*</span></label>
            <div class="col-lg-5 col-md-5">
              <input type="text" class="form-control" name="resep_id" id="resep_id" value="otomatis" readonly="">
              <input type="hidden" name="farmasi_id" id="farmasi_id" value="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Obat <span class="text-danger">*</span></label>
            <div class="col-lg-7 col-md-3">
              <select class="form-control select2" name="obat_id" id="obat_id">
                <option value="">- Pilih -</option>
              </select>
              <input type="hidden" class="form-control" name="stokdepo_id" id="stokdepo_id">
            </div>
            <div class="col-lg-1 col-md-1">
              <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_pemberian_obat/search_obat' ?>" modal-title="List Data Obat" modal-size="lg" class="btn btn-xs btn-default modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Obat" data-original-title="Cari Obat"><i class="fas fa-search"></i></a>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Qty <span class="text-danger">*</span></label>
            <div class="col-lg-8 col-md-2">
              <input type="text" class="form-control autonumeric col-lg-4" name="qty" id="qty" min="1" value="1" dir="RTL" onkeyup="hitung_total()">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Aturan Pakai <span class="text-danger">*</span></label>
            <div class="col-lg-8 col-md-2">
              <input type="text" class="form-control" name="aturan_pakai" id="aturan_pakai" value="<?= @$detail['aturan_pakai'] ?>" required>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Jadwal Pemakaian</label>
            <div class="col-lg-8 col-md-2">
              <input type="text" class="form-control" name="jadwal" id="jadwal" value="<?= @$detail['jadwal'] ?>">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Aturan Tambahan</label>
            <div class="col-lg-8 col-md-2">
              <input type="text" class="form-control" name="aturan_tambahan" id="aturan_tambahan" value="<?= @$detail['aturan_tambahan'] ?>">
            </div>
          </div>
          <div class="row">
            <div class="col-lg-9 offset-lg-3 p-0">
              <button type="submit" id="pemberian_obat_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
              <button type="button" id="pemberian_obat_cancel" class="btn btn-xs btn-secondary"><i class="fas fa-times"></i> Batal</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>