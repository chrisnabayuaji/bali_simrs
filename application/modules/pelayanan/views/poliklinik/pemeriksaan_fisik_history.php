<?php if (@$main == null) : ?>
  <tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else : ?>
  <?php $i = 1;
  foreach ($main as $row) : ?>
    <tr>
      <td class="text-center text-top" rowspan="7"><b><?= $i++ ?></b></td>
      <td width="200"><b>Tanggal Catat</b></td>
      <td><b>: <?= to_date($row['tgl_catat'], '', 'full_date') ?></b></td>
    </tr>
    <tr>
      <td>Lokasi Pelayanan</td>
      <td>: <?= $row['lokasi_nm'] ?></td>
    </tr>
    <tr>
      <td>Keluhan Utama</td>
      <td>: <?= $row['keluhan_utama'] ?></td>
    </tr>
    <tr>
      <td>Systole : <?= $row['systole'] ?></td>
      <td>Diastole : <?= $row['diastole'] ?></td>
    </tr>
    <tr>
      <td>Tinggi : <?= $row['tinggi'] ?></td>
      <td>Berat : <?= $row['berat'] ?></td>
    </tr>
    <tr>
      <td>Suhu : <?= $row['suhu'] ?></td>
      <td>Nadi : <?= $row['nadi'] ?></td>
    </tr>
    <tr>
      <td>Respiration Rate : <?= $row['respiration_rate'] ?></td>
      <td>SaO<sub>2</sub> : <?= $row['sao2'] ?></td>
    </tr>
  <?php endforeach; ?>
<?php endif; ?>