<table class="table table-striped table-sm">
  <tbody>
    <tr>
      <td class="text-right" width="80">Kode</td>
      <td class="text-center" width="20">:</td>
      <td class="text-left"><?= $prosedur['prosedur_id'] ?></td>
    </tr>
    <tr>
      <td class="text-right">Prosedur</td>
      <td class="text-center">:</td>
      <td><?= $prosedur['prosedur_nm'] ?></td>
    </tr>
    <tr>
      <td class="text-right">Singkatan</td>
      <td class="text-center">:</td>
      <td class="text-left"><?= $prosedur['prosedur_short'] ?></td>
    </tr>
  </tbody>
</table>