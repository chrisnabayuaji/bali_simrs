<?php $this->load->view('_js_racik'); ?>
<div class="row">
  <div class="col-md-6">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-list"></i> Data Resep Racik</h4>
        <div class="tab-content p-0" id="pills-tabContent-resep" style="border:0px !important">
          <div class="tab-pane fade show active" id="pills-saat-ini-resep" role="tabpanel" aria-labelledby="pills-saat-ini-resep-tab">
            <div class="table-responsive">
              <table id="racik_data" class="table table-bordered table-striped table-sm">

              </table>
            </div>
          </div>
          <div class="tab-pane fade" id="pills-history-resep" role="tabpanel" aria-labelledby="pills-history-resep-tab">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-sm">
                <thead>
                  <tr>
                    <th class="text-center" width="20">No</th>
                    <th class="text-center">Tanggal</th>
                    <th class="text-center">Kode</th>
                    <th class="text-center">resep</th>
                    <th class="text-center">Kasus</th>
                    <th class="text-center">Tipe</th>
                    <th class="text-center">Keterangan</th>
                    <th class="text-center">Lokasi Pelayanan</th>
                  </tr>
                </thead>
                <tbody id="resep_history"></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-prescription"></i> Resep Racik</h4>
        <form id="racik_form" action="" method="post" autocomplete="off">
          <input type="hidden" name="resep_id" id="resep_id">
          <input type="hidden" name="rincian_id" id="rincian_id">
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Nama Racikan <span class="text-danger">*</span></label>
            <div class="col-lg-9 col-md-8">
              <input type="text" class="form-control" name="obat_nm" id="obat_nm" required="" aria-invalid="false">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Sediaan <span class="text-danger">*<span></label>
            <div class="col-lg-4 col-md-9">
              <select class="form-control chosen-select" name="sediaan_cd" id="racik_sediaan_cd" required="">
                <?php foreach (get_parameter('sediaan_cd') as $r) : ?>
                  <option value="<?= $r['parameter_cd'] ?>"><?= $r['parameter_val'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Rute <span class="text-danger">*<span></label>
            <div class="col-lg-4 col-md-9">
              <select class="form-control chosen-select" name="rute_cd" id="racik_rute_cd" required="">
                <?php foreach (get_parameter('rute_cd') as $r) : ?>
                  <option value="<?= $r['parameter_cd'] ?>"><?= $r['parameter_val'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Qty <span class="text-danger">*</span></label>
            <div class="col-lg-3 col-md-3">
              <input type="text" class="form-control" name="qty" id="qty" required="" aria-invalid="false">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Aturan Pakai <span class="text-danger">*</span></label>
            <div class="col-lg-2 col-md-3">
              <input type="text" class="form-control" name="aturan_jml" id="aturan_jml" required="" aria-invalid="false" placeholder="Jml">
            </div>
            <div class="col-lg-2 col-md-9">
              <select class="form-control chosen-select" name="satuan_cd" id="racik_satuan_cd" required="">
                <?php foreach (get_parameter('satuan_cd') as $r) : ?>
                  <option value="<?= $r['parameter_cd'] ?>"><?= $r['parameter_val'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
            <div class="col-lg-1 col-md-1 pt-3">
              <label><strong>Tiap</strong></label>
            </div>
            <div class="col-lg-2 col-md-3">
              <input type="text" class="form-control" name="aturan_waktu" id="aturan_waktu" required="" aria-invalid="false" placeholder="Jml">
            </div>
            <div class="col-lg-2 col-md-9">
              <select class="form-control chosen-select" name="waktu_cd" id="racik_waktu_cd" required="">
                <?php foreach (get_parameter('waktu_cd') as $r) : ?>
                  <option value="<?= $r['parameter_cd'] ?>"><?= $r['parameter_val'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Jadwal Pemberian</label>
            <div class="col-lg-9 col-md-9">
              <input type="text" class="form-control" name="jadwal" id="jadwal" aria-invalid="false">
              <small class="text-info">Tuliskan jam dan pisahkan dengan spasi. Cth: 07:00 15:00 22:00</small>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Aturan Tambahan</label>
            <div class="col-lg-9 col-md-9">
              <input type="text" class="form-control" name="aturan_tambahan" id="aturan_tambahan" aria-invalid="false">
            </div>
          </div>
          <div class="border-bottom mb-2"></div>
          <h5>Komposisi</h5>
          <div class="row">
            <div class="col-3 text-right">Tabel Komposisi</div>
            <div class="col-9 pl-0">
              <button type="button" id="btn_add_komposisi" class="btn btn-xs btn-primary"><i class="fas fa-plus"></i></button>
              <table id="table_komposisi" class="table table-bordered table-striped table-sm mt-1">
                <thead>
                  <tr>
                    <th class="text-center" width="40">Aksi</th>
                    <th class="text-center">Nama Komposisi</th>
                    <th class="text-center" width="50">Qty</th>
                  </tr>
                </thead>
                <tbody id="komposisi_data">

                </tbody>
              </table>
            </div>
          </div>
          <div class="border-bottom mt-4"></div>
          <div class="row mt-2">
            <div class="col-lg-9 offset-lg-3 p-0">
              <button type="button" id="rincian_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
              <button type="button" id="rincian_cancel" class="btn btn-xs btn-secondary"><i class="fas fa-times"></i> Batal</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<div id="modal_komposisi" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Komposisi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="komposisi_form" action="" method="post">
        <div class="modal-body">
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Nama Komposisi</label>
            <div class="col-lg-9 col-md-9">
              <input type="text" class="form-control" id="komposisi" name="komposisi" required aria-invalid="false">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Alias Komposisi</label>
            <div class="col-lg-5">
              <input type="text" class="form-control" id="komposisi_alias" name="komposisi_alias" aria-invalid="false">
            </div>
            <label class="col-lg-2 col-md-3 col-form-label">Qty</label>
            <div class="col-lg-2 pr-0">
              <input type="text" class="form-control" id="komposisi_qty" name="komposisi_qty" required aria-invalid="false">
            </div>
          </div>
          <div class="form-group row">
            <div class="col-lg-9 offset-lg-3 p-0">
              <button type="submit" id="rincian_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
              <button type="button" class="btn btn-xs btn-secondary" data-dismiss="modal">Batal</button>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>