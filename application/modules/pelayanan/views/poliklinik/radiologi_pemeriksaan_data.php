<?php if ($main == null) : ?>
  <tbody>
    <tr>
      <td class="text-center" colspan="99">Tidak ada data.</td>
    </tr>
  </tbody>
<?php else : ?>
  <?php $no = 1;
  foreach ($main as $row) : ?>
    <?php if ($row['parent_id'] == '') : ?>
      <tr>
        <td colspan="99"><b><?= @$row['itemrad_nm'] ?></b></td>
      </tr>
    <?php else : ?>
      <tr>
        <td class="text-center"><?= $no++ ?></td>
        <td class="text-left"><?= $row['itemrad_id'] ?></td>
        <td class="text-left pl-<?= count(explode('.', $row['itemrad_id'])) ?>" colspan="99">|- <?= $row['itemrad_nm'] ?></td>
      </tr>
    <?php endif; ?>
    <?php if (count($row['rinc']) > 0) : ?>
      <?php foreach ($row['rinc'] as $row2) : ?>
        <tr>
          <td class="text-center"><?= $no++ ?></td>
          <td class="text-left"><?= $row2['itemrad_id'] ?></td>
          <td class="text-left pl-<?= count(explode('.', $row2['itemrad_id'])) ?>">|- <?= $row2['itemrad_nm'] ?></td>
          <td class="text-center"><?= to_date(@$row2['tgl_hasil'], '-', 'full_date') ?></td>
          <td class="text-center"><?= $row2['hasil_rad'] ?></td>
          <td class="text-left"><?= $row2['catatan_rad'] ?></td>
        </tr>
      <?php endforeach; ?>
    <?php endif; ?>
  <?php endforeach; ?>
<?php endif; ?>