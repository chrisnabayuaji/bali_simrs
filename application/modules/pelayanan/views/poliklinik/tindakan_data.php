<?php if (@$main == null) : ?>
  <tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else : ?>
  <?php
  $i = 1;
  $tot_biaya = 0;
  foreach ($main as $row) :
    $tot_biaya += $row['jml_tagihan'];
  ?>
    <tr>
      <td class="text-center"><?= $i++ ?></td>
      <td class="text-center">
        <a href="javascript:void(0)" class="btn btn-primary btn-table btn-edit" data-tindakan-id="<?= $row['tindakan_id'] ?>" data-reg-id="<?= $row['reg_id'] ?>" data-pasien-id="<?= $row['pasien_id'] ?>" title="Ubah Data"><i class="fas fa-pencil-alt"></i></a>
        <a href="javascript:void(0)" class="btn btn-danger btn-table btn-delete" data-tindakan-id="<?= $row['tindakan_id'] ?>" data-reg-id="<?= $row['reg_id'] ?>" data-pasien-id="<?= $row['pasien_id'] ?>" data-lokasi-id="<?= $row['lokasi_id'] ?>" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
      </td>
      <td class="text-center"><?= $row['tarif_id'] ?></td>
      <td class="text-left"><?= $row['tarif_nm'] ?> - <?= $row['kelas_nm'] ?></td>
      <td class="text-center"><?= $row['qty'] ?></td>
      <td class="text-center"><?= get_parameter_value('jenistindakan_cd', $row['jenistindakan_cd']) ?></td>
      <td class="text-right"><?= num_id($row['jml_tagihan']) ?></td>
      <td class="text-left">
        <?= (@$row['pegawai_nm_1'] != '') ? @$row['pegawai_nm_1'] . ',' : '' ?>
        <?= (@$row['pegawai_nm_2'] != '') ? @$row['pegawai_nm_2'] . ',' : '' ?>
        <?= (@$row['pegawai_nm_3'] != '') ? @$row['pegawai_nm_3'] . ',' : '' ?>
        <?= (@$row['pegawai_nm_4'] != '') ? @$row['pegawai_nm_4'] . ',' : '' ?>
        <?= (@$row['pegawai_nm_5'] != '') ? @$row['pegawai_nm_5'] . '' : '' ?>
      </td>
      <td class="text-center">
        <?php if ($row['prosedur_id'] != '') : ?>
          <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/prosedur_detail/' . $row['prosedur_id'] ?>" modal-title="Prosedur" modal-size="md" class="text-primary modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Prosedur"><?= ($row['prosedur_id']) ?></a>
        <?php endif; ?>
      </td>
    </tr>
  <?php endforeach; ?>
  <tr>
    <td colspan="6" class="text-right"><b>Total Biaya</b></td>
    <td class="text-right"><b><?= num_id($tot_biaya) ?></b></td>
  </tr>
<?php endif; ?>

<script type="text/javascript">
  $(document).ready(function() {
    // edit
    $('.btn-edit').bind('click', function(e) {
      e.preventDefault();
      var tindakan_id = $(this).attr("data-tindakan-id");
      var reg_id = $(this).attr("data-reg-id");
      var pasien_id = $(this).attr("data-pasien-id");

      $('#add_petugas').removeClass('disabled');
      //
      $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_tindakan/get_data' ?>', {
        tindakan_id: tindakan_id,
        reg_id: reg_id,
        pasien_id: pasien_id
      }, function(data) {

        $('.datetimepicker').daterangepicker({
          startDate: moment(data.main.tgl_catat),
          endDate: moment(),
          timePicker: true,
          timePicker24Hour: true,
          timePickerSeconds: true,
          singleDatePicker: true,
          showDropdowns: true,
          locale: {
            cancelLabel: 'Clear',
            format: 'DD-MM-YYYY H:mm:ss'
          },
          isInvalidDate: function(date) {
            return '';
          }
        })

        $('#tindakan_id').val(data.main.tindakan_id);

        $('#box_petugas').html('');

        // autocomplete
        var tarifKelasData = {
          id: data.main.tarif_id + '.' + data.main.kelas_id,
          text: data.main.tarif_id + '.' + data.main.kelas_id + ' - ' + data.main.tarif_nm + ' - ' + data.main.kelas_nm
        };
        var tarifKelasOpt = new Option(tarifKelasData.text, tarifKelasData.id, false, false);
        $('#tarifkelas_id').append(tarifKelasOpt).trigger('change');
        $('#tarifkelas_id').val(data.main.tarif_id + '.' + data.main.kelas_id);
        $('#tarifkelas_id').trigger('change');

        // autocomplete
        var prosedurData = {
          id: data.main.prosedur_id,
          text: data.main.prosedur_id + ' - ' + data.main.prosedur_nm
        };
        var prosedurOpt = new Option(prosedurData.text, prosedurData.id, false, false);
        $('#prosedur_id').append(prosedurOpt).trigger('change');
        $('#prosedur_id').val(data.main.prosedur_id);
        $('#prosedur_id').trigger('change');

        add_petugas(data.main.jml_petugas, tindakan_id);

        $("#qty").val(data.main.qty).removeClass("is-valid").removeClass("is-invalid");
        $("#jenistindakan_cd").val(data.main.jenistindakan_cd).trigger('change').removeClass("is-valid").removeClass("is-invalid");
        $('.select2-container').css('width', '100%');
        $('#tindakan_action').html('<i class="fas fa-save"></i> Ubah');
      }, 'json');
    });

    function add_petugas(no, tindakan_id) {
      $.get('<?= site_url($nav['nav_url']) ?>/ajax_tindakan/add_petugas?petugas_no=' + no + '&tindakan_id=' + tindakan_id, null, function(data) {
        $('#box_petugas').append(data.html);
        $('#petugas_no').val(data.petugas_no);
        if (data.petugas_no == '5') {
          $('#add_petugas').addClass('disabled');
        }
      }, 'json');
    }

    // delete
    $('.btn-delete').on('click', function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Apakah Anda yakin?',
        text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#eb3b5a',
        cancelButtonColor: '#b2bec3',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        customClass: 'swal-wide'
      }).then((result) => {
        if (result.value) {
          var tindakan_id = $(this).attr("data-tindakan-id");
          var reg_id = $(this).attr("data-reg-id");
          var pasien_id = $(this).attr("data-pasien-id");
          var lokasi_id = $(this).attr("data-lokasi-id");
          //
          $('#tindakan_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
          $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_tindakan/delete_data' ?>', {
            tindakan_id: tindakan_id,
            reg_id: reg_id,
            pasien_id: pasien_id,
            lokasi_id: lokasi_id
          }, function(data) {
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil dihapus.',
              icon: 'success',
              position: 'top-right'
            })
            $('#tindakan_data').html(data.html);
          }, 'json');
        }
      })
    })
  });
</script>