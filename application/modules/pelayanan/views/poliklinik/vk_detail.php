<form id="vk_form" action="" method="post" autocomplete="off">
  <div class="row">
    <input type="hidden" name="pemeriksaan_id" id="pemeriksaan_id" value="<?=@$mainvk['pemeriksaan_id']?>">
    <input type="hidden" name="src_reg_id" id="src_reg_id" value="<?=@$reg['reg_id']?>">
    <input type="hidden" name="pasien_id" id="pasien_id" value="<?=@$reg['pasien_id']?>">
    <div class="col-6">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Dokter Pengirim</label>
        <div class="col-lg-9 col-md-3">
          <input type="text" class="form-control" id="dokterpengirim_nm" value="<?=@$reg['pegawai_nm']?>" required="" readonly>
          <input type="hidden" name="dokterpengirim_id" value="<?=$reg['dokter_id']?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Tanggal Order</label>
        <div class="col-lg-5 col-md-9">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
            </div>
            <input type="text" class="form-control datetimepicker" name="tgl_order" id="tgl_order" value="<?php if(@$mainvk){echo to_date(@$mainvk['tgl_order'],'-','full_date');}else{echo date('d-m-Y H:i:s');}?>" required readonly aria-invalid="false">
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Lokasi Asal</label>
        <div class="col-lg-9 col-md-3">
          <input type="text" class="form-control" id="lokasi_nm" value="<?=@$reg['lokasi_nm']?>" required="" readonly disabled>
          <input type="hidden" name="src_lokasi_id" value="<?=$reg['lokasi_id']?>">
        </div>
      </div>
    </div>
    <div class="col-6">
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Keterangan</label>
        <div class="col-lg-8 col-md-3">
          <textarea class="form-control" name="keterangan_order" id="keterangan_order" rows="6" readonly><?=@$mainvk['keterangan_order']?></textarea>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <ul class="nav nav-pills nav-pills-primary mt-n2" id="pills-tab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" onclick="vk_tindakan_data('<?=@$mainvk['pemeriksaan_id']?>', '<?=@$mainvk['reg_id']?>')" id="vk_tindakan-tab" data-toggle="pill" href="#vk_tindakan" role="tab" aria-controls="vk_tindakan" aria-selected="false"><i class="fas fa-procedures"></i> TAB TINDAKAN</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" onclick="vk_bhp_data('<?=@$mainvk['pemeriksaan_id']?>', '<?=@$mainvk['reg_id']?>')" id="vk_bhp-tab" data-toggle="pill" href="#vk_bhp" role="tab" aria-controls="vk_bhp" aria-selected="false"><i class="fas fa-syringe"></i> TAB BHP</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" onclick="vk_alkes_data('<?=@$mainvk['pemeriksaan_id']?>', '<?=@$mainvk['reg_id']?>')"id="vk_alkes-tab" data-toggle="pill" href="#vk_alkes" role="tab" aria-controls="vk_alkes" aria-selected="false"><i class="fas fa-prescription-bottle-alt"></i> TAB ALKES</a>
    </li>
  </ul>
  <div class="tab-content p-0 no-border">
    <div class="tab-pane fade show active" id="vk_tindakan" role="tabpanel" aria-labelledby="vk_tindakan-tab">
      <div class="media">
        <div class="media-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-sm">
              <thead>
                <tr>
                  <th class="text-center" width="20">No</th>
                  <th class="text-center" width="100">Kode</th>
                  <th class="text-center">Tindakan</th>
                  <th class="text-center">Qty</th>
                  <th class="text-center">Jenis</th>
                  <th class="text-center">Biaya</th>
                  <th class="text-center">Petugas</th>
                </tr>
              </thead>
              <tbody id="vk_tindakan_data"></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="tab-pane fade" id="vk_bhp" role="tabpanel" aria-labelledby="vk_bhp-tab">
      <div class="media">
        <div class="media-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-sm">
              <thead>
                <tr>
                  <th class="text-center" width="20">No</th>
                  <th class="text-center">Kode</th>
                  <th class="text-center">Nama</th>
                  <th class="text-center">Qty</th>
                  <th class="text-center">Keterangan</th>
                </tr>
              </thead>
              <tbody id="vk_bhp_data"></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="tab-pane fade" id="vk_alkes" role="tabpanel" aria-labelledby="vk_alkes-tab">
      <div class="media">
        <div class="media-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-sm">
              <thead>
                <tr>
                  <th class="text-center" width="20">No</th>
                  <th class="text-center">Kode</th>
                  <th class="text-center">Nama</th>
                  <th class="text-center">Qty</th>
                  <th class="text-center">Keterangan</th>
                </tr>
              </thead>
              <tbody id="vk_alkes_data"></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="col-2 offset-md-10 mt-n2">
    <div class="float-right">
      <button type="button" class="btn btn-xs btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Tutup</button>
    </div>
  </div>
</form>

<script type="text/javascript">

  vk_tindakan_data('<?=@$mainvk['pemeriksaan_id']?>', '<?=@$mainvk['reg_id']?>');

  function vk_tindakan_data(pemeriksaan_id='', reg_id='') {
    $('#vk_tindakan_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_penunjang_vk/vk_tindakan_data'?>', {pemeriksaan_id: pemeriksaan_id, reg_id: reg_id}, function (data) {
      $('#vk_tindakan_data').html(data.html);
    }, 'json');
  }

  function vk_bhp_data(pemeriksaan_id='', reg_id='') {
    $('#vk_bhp_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_penunjang_vk/vk_bhp_data'?>', {pemeriksaan_id: pemeriksaan_id, reg_id: reg_id}, function (data) {
      $('#vk_bhp_data').html(data.html);
    }, 'json');
  }

  function vk_alkes_data(pemeriksaan_id='', reg_id='') {
    $('#vk_alkes_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_penunjang_vk/vk_alkes_data'?>', {pemeriksaan_id: pemeriksaan_id, reg_id: reg_id}, function (data) {
      $('#vk_alkes_data').html(data.html);
    }, 'json');
  }
</script>