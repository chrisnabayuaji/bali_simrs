<script type="text/javascript" src="<?= base_url() ?>assets/plugins/webcam/webcam.js"></script>
<script>
  $(document).ready(function() {
    // CAMERA SETTINGS.
    Webcam.set({
      width: 480,
      height: 390,
      image_format: 'jpeg',
      jpeg_quality: 100
    });
    Webcam.attach('#camera');

    $("#btnCloseModal").click(function() {
      Webcam.reset();
      $("#myModal").modal("hide");
    });

    setTimeout(function() {
      $(".btnTakePhoto").removeClass('disabled');
      $('.btnTakePhoto').prop("disabled", false);
    }, 3000);

    // SHOW THE SNAPSHOT.
    takeSnapShot = function() {
      Webcam.snap(function(data_uri) {
        Webcam.reset();
        $("#myModal").modal("hide");
        $("#preview-take-photo").removeClass('d-none');
        $("#imgWebCam").val(data_uri);
        $("#foto_selfie_ktp").prop('required', false);
        document.getElementById('snapShot').innerHTML =
          '<img src="' + data_uri + '" width="420px" height="320px"/>';
      });
    }
  });
</script>
<div class="row">
  <div class="col-lg-12">
    <div id="camera" style="height:auto;width:auto; text-align:left;"></div>
  </div>
</div>
<div style="border-top: 1px solid #e5e5e5; margin-top: 20px;">
  <div>
    <div class="text-right mt-2">
      <button type="button" class="btn btn-sm btn-primary btnTakePhoto disabled" id="btnTakePhoto" onclick="takeSnapShot()" disabled><i class="fa fa-camera"></i> Ambil Foto Selfie</button>
      <button type="button" class="btn btn-sm btn-default" id="btnCloseModal"><i class="fa fa-times"></i> Tutup</button>
    </div>
  </div>
</div>