<script type="text/javascript">
  var rs_rujukan_form;
  $(document).ready(function () {
    var rs_rujukan_form = $("#rs_rujukan_form").validate( {
      rules: {
        rsrujukan_kode:{
          remote: {
            url: '<?=site_url().'/'.$nav['nav_url']?>/ajax_tindak_lanjut/cek_id',
            type: 'post',
            data: {
              rsrujukan_kode: function() {
                return $("#rsrujukan_kode").val();
              }
            }
          }
        },
      },
      messages: {
        rsrujukan_kode : {
          remote : 'Kode sudah digunakan!'
        }
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container"));
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $("#rs_rujukan_action").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#rs_rujukan_action").attr("disabled", "disabled");
        $("#rs_rujukan_cancel").attr("disabled", "disabled");
        $.ajax({
          type : 'post',
          url : '<?=site_url($nav['nav_url'])?>/ajax_tindak_lanjut/save_rs_rujukan',
          dataType : 'json',
          data : $(form).serialize(),
          success : function (data) {
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil disimpan.',
              icon: 'success',
              position: 'top-right'
            })
            $("#rs_rujukan_action").html('<i class="fas fa-save"></i> Simpan');
            $("#rs_rujukan_action").attr("disabled", false);
            $("#rs_rujukan_cancel").attr("disabled", false);

            // select
            var data2 = {
              id: data.main.rsrujukan_kode,
              text: data.main.rsrujukan_nm
            };
            var newOption = new Option(data2.text, data2.id, false, false);
            $('#rsrujukan_id').append(newOption).trigger('change');
            $('#rsrujukan_id').val(data.main.rsrujukan_kode);
            $('#rsrujukan_id').trigger('change');
            $('.select2-container').css('width', '100%');
            $("#myModal").modal('hide');
          }
        })
        return false;
      }
    });
  })
  // select2
  $(".chosen-select").select2();
  $('.select2-container').css('width', '100%');
</script>