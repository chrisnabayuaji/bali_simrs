<script type="text/javascript">
  $(document).ready(function() {

    var pasien_id = $("#pasien_id").val();
    var reg_id = $("#reg_id").val();
    var lokasi_id = $("#lokasi_id").val();

    pasien_history(pasien_id, reg_id);
  })

  function pasien_history(pasien_id = '', reg_id = '') {
    $('#pasien_history').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_history/pasien_history' ?>', {
      pasien_id: pasien_id,
      reg_id: reg_id
    }, function(data) {
      $('#pasien_history').html(data.html);
    }, 'json');
  }
</script>