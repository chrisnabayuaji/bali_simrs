<script type="text/javascript">
  var pemeriksaan_fisik_form;
  $(document).ready(function() {
    var pemeriksaan_fisik_form = $("#pemeriksaan_fisik_form").validate({
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $("#pemeriksaan_fisik_action").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#pemeriksaan_fisik_action").attr("disabled", "disabled");
        $("#pemeriksaan_fisik_cancel").attr("disabled", "disabled");
        var pasien_id = $("#pasien_id").val();
        var reg_id = $("#reg_id").val();
        var lokasi_id = $("#lokasi_id").val();
        $.ajax({
          type: 'post',
          url: '<?= site_url($nav['nav_url']) ?>/ajax_pemeriksaan_fisik/save',
          data: $(form).serialize() + '&pasien_id=' + pasien_id + '&reg_id=' + reg_id + '&lokasi_id=' + lokasi_id,
          success: function(data) {
            pemeriksaan_fisik_reset();
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil disimpan.',
              icon: 'success',
              position: 'top-right'
            })
            $("#pemeriksaan_fisik_action").html('<i class="fas fa-save"></i> Simpan');
            $("#pemeriksaan_fisik_action").attr("disabled", false);
            $("#pemeriksaan_fisik_cancel").attr("disabled", false);
            pemeriksaan_fisik_data(pasien_id, reg_id, lokasi_id);
            $('#body-chart-tekanan-darah').html('');
            GrafikTekananDarah(pasien_id, reg_id, lokasi_id);
            $('#body-chart-suhu-tubuh').html('');
            GrafikSuhuTubuh(pasien_id, reg_id, lokasi_id);
          }
        })
        $(form).submit(function(e) {
          return false;
        });
      }
    });

    var pasien_id = $("#pasien_id").val();
    var reg_id = $("#reg_id").val();
    var lokasi_id = $("#lokasi_id").val();
    pemeriksaan_fisik_data(pasien_id, reg_id, lokasi_id);
    GrafikTekananDarah(pasien_id, reg_id, lokasi_id);
    GrafikSuhuTubuh(pasien_id, reg_id, lokasi_id);

    $("#pemeriksaan_fisik_cancel").on('click', function() {
      pemeriksaan_fisik_reset();
    });

    $('.datetimepicker').daterangepicker({
      startDate: moment("<?= date('Y-m-d H:i:s') ?>"),
      endDate: moment(),
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    // autocomplete
    $('#petugas_id_pemeriksaan_fisik').select2({
      minimumInputLength: 2,
      ajax: {
        url: "<?= site_url($nav['nav_url']) ?>/ajax_pemeriksaan_fisik/autocomplete",
        dataType: "json",

        data: function(params) {
          return {
            pegawai_nm: params.term
          };
        },
        processResults: function(data) {
          return {
            results: data
          }
        }
      }
    });
    $('.select2-container').css('width', '100%');

    $('.modal-href').click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");

      $("#modal-title").html(modal_title);
      $("#modal-size").addClass('modal-' + modal_size);
      if (modal_custom_size) {
        $("#modal-size").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
      }
      $("#myModal").modal('show');
      $.post($(this).data('href'), function(data) {
        $("#modal-body").html(data.html);
      }, 'json');
    });
  })

  function pemeriksaan_fisik_data(pasien_id = '', reg_id = '', lokasi_id = '') {
    $('#pemeriksaan_fisik_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_pemeriksaan_fisik/data' ?>', {
      pasien_id: pasien_id,
      reg_id: reg_id,
      lokasi_id: lokasi_id
    }, function(data) {
      $('#pemeriksaan_fisik_data').html(data.html);
    }, 'json');
  }

  function pemeriksaan_fisik_reset() {
    $("#anamnesis_id").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#keluhan_utama").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#systole").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#diastole").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#tinggi").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#berat").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#suhu").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#nadi").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#respiration_rate").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#sao2").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#petugas_id_pemeriksaan_fisik").val('').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $('#pemeriksaan_fisik_action').html('<i class="fas fa-save"></i> Simpan');

    $('.datetimepicker').daterangepicker({
      startDate: moment("<?= date('Y-m-d H:i:s') ?>"),
      endDate: moment(),
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })
  }

  function petugas_fill(id) {
    $.ajax({
      type: 'post',
      url: '<?= site_url($nav['nav_url'] . '/ajax_pemeriksaan_fisik/petugas_fill') ?>',
      dataType: 'json',
      data: 'pegawai_id=' + id,
      success: function(data) {
        // autocomplete
        var data2 = {
          id: data.pegawai_id,
          text: data.pegawai_nm
        };
        var newOption = new Option(data2.text, data2.id, false, false);
        $('#petugas_id_pemeriksaan_fisik').append(newOption).trigger('change');
        $('#petugas_id_pemeriksaan_fisik').val(data.pegawai_id);
        $('#petugas_id_pemeriksaan_fisik').trigger('change');
        $('.select2-container').css('width', '100%');
        $("#myModal").modal('hide');
      }
    })
  }

  function GrafikTekananDarah(pasien_id = '', reg_id = '', lokasi_id = '') {
    $('#grafik-tekanan-darah').html('<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i></div><div class="text-center mt-2">Memuat...</div>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_pemeriksaan_fisik/grafik_tekanan_darah' ?>', {
      pasien_id: pasien_id,
      reg_id: reg_id,
      lokasi_id: lokasi_id
    }, function(data) {
      if (data.html == 'Tidak ada data') {
        $('#body-chart-tekanan-darah').html('<div class="mb-2"></div>');
      } else {
        $('#body-chart-tekanan-darah').html('<canvas id="canvas-tekanan-darah"></canvas>');
      }
      $('#grafik-tekanan-darah').html(data.html);
    }, 'json');
  }

  function GrafikSuhuTubuh(pasien_id = '', reg_id = '', lokasi_id = '') {
    $('#grafik-suhu-tubuh').html('<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i></div><div class="text-center mt-2">Memuat...</div>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_pemeriksaan_fisik/grafik_suhu_tubuh' ?>', {
      pasien_id: pasien_id,
      reg_id: reg_id,
      lokasi_id: lokasi_id
    }, function(data) {
      if (data.html == 'Tidak ada data') {
        $('#body-chart-suhu-tubuh').html('<div class="mb-2"></div>');
      } else {
        $('#body-chart-suhu-tubuh').html('<canvas id="canvas-suhu-tubuh"></canvas>');
      }
      $('#grafik-suhu-tubuh').html(data.html);
    }, 'json');
  }
</script>