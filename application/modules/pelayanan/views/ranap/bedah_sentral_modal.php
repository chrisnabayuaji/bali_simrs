<form id="bedah_sentral_form" action="" method="post" autocomplete="off">
  <div class="row">
    <input type="hidden" name="pemeriksaan_id" id="pemeriksaan_id" value="<?= @$mainbs['pemeriksaan_id'] ?>">
    <input type="hidden" name="reg_id" id="reg_id" value="<?= @$reg['reg_id'] ?>">
    <input type="hidden" name="src_reg_id" id="src_reg_id" value="<?= @$reg['reg_id'] ?>">
    <input type="hidden" name="pasien_id" id="pasien_id" value="<?= @$reg['pasien_id'] ?>">
    <div class="col-5">
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Dokter Pengirim <span class="text-danger">*</span></label>
        <div class="col-lg-8 col-md-3">
          <input type="text" class="form-control" id="dokterpengirim_nm" value="<?= @$reg['pegawai_nm'] ?>" required="" readonly>
          <input type="hidden" name="dokterpengirim_id" value="<?= $reg['dokter_id'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Tanggal Order<span class="text-danger">*<span></label>
        <div class="col-lg-5 col-md-9">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
            </div>
            <input type="text" class="form-control datetimepicker" name="tgl_order" id="tgl_order" value="<?php if (@$mainbs) {
                                                                                                            echo to_date(@$mainbs['tgl_order'], '-', 'full_date');
                                                                                                          } else {
                                                                                                            echo date('d-m-Y H:i:s');
                                                                                                          } ?>" required aria-invalid="false">
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Lokasi Asal <span class="text-danger">*</span></label>
        <div class="col-lg-8 col-md-3">
          <input type="text" class="form-control" id="lokasi_nm" value="<?= @$reg['lokasi_nm'] ?>" required="" readonly>
          <input type="hidden" name="src_lokasi_id" value="<?= $reg['lokasi_id'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Keterangan</label>
        <div class="col-lg-8 col-md-3">
          <textarea class="form-control" name="keterangan_order" id="keterangan_order" rows="6"><?= @$mainbs['keterangan_order'] ?></textarea>
        </div>
      </div>
    </div>
    <div class="col">
      <div class="table-responsive">
        <table id="bedah_sentral_table" class="table table-hover table-bordered table-striped table-sm w-100">
          <thead>
            <tr>
              <th class="text-center" width="20">No.</th>
              <th class="text-center" width="50">Kode</th>
              <th class="text-center">Nama </th>
              <th class="text-center" width="20">Aksi</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
  <hr>
  <div class="col-6 offset-md-6 ">
    <div class="float-right">
      <button id="bedah_sentral_cancel" type="button" class="btn btn-xs btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
      <button id="bedah_sentral_action" type="submit" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
    </div>
  </div>
</form>
<script>
  var bedah_sentralTable;
  $(document).ready(function() {
    $('.datetimepicker').daterangepicker({
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })

    //datatables
    bedahsentralTable = $('#bedah_sentral_table').DataTable({
      'retrieve': true,
      "autoWidth": false,
      "processing": true,
      "serverSide": false,
      "order": [],
      "ajax": {
        "url": "<?php echo site_url() . '/' . $nav['nav_url'] . '/ajax_penunjang_bedah_sentral/search_data/' . @$reg['reg_id'] . '/' . @$mainbs['pemeriksaan_id'] ?>",
        "type": "POST"
      },
      "columnDefs": [{
          "targets": 0,
          "className": 'text-center',
          "orderable": false
        },
        {
          "targets": 1,
          "className": 'text-left',
          "orderable": false
        },
        {
          "targets": 2,
          "className": 'text-left',
          "orderable": false
        },
        {
          "targets": 3,
          "className": 'text-center',
          "orderable": false
        }
      ],
    });
    bedahsentralTable.columns.adjust().draw();

    //FORM
    var bedah_sentral_form = $("#bedah_sentral_form").validate({
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $("#bedah_sentral_action").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#bedah_sentral_action").attr("disabled", "disabled");
        $("#bedah_sentral_cancel").attr("disabled", "disabled");

        $.ajax({
          type: 'post',
          url: '<?= site_url($nav['nav_url']) ?>/ajax_penunjang_bedah_sentral/save/<?= @$reg['reg_id'] . '/' . @$mainbs['pemeriksaan_id'] ?>',
          data: $(form).serialize(),
          dataType: 'json',
          success: function(data) {

          }
        })
        $("#myModal").modal('hide');
        $.toast({
          heading: 'Sukses',
          text: 'Data berhasil disimpan.',
          icon: 'success',
          position: 'top-right'
        })
        bedah_sentral_data('<?= $reg['reg_id'] ?>', '<?= $reg['pasien_id'] ?>', '<?= $reg['lokasi_id'] ?>');
        return false;
      }
    });


  })
</script>