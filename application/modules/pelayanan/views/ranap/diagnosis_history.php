<?php if(@$main == null):?>
	<tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else: ?>
	<?php $i=1;foreach($main as $row):?>
	<tr>
		<td class="text-center"><?=$i++?></td>
    <td class="text-center"><?=to_date($row['tgl_catat'], '', 'full_date', '<br>')?></td>
    <td class="text-center"><?=$row['penyakit_id']?></td>
    <td class="text-left"><?=$row['icdx']?> - <?=$row['penyakit_nm']?></td>
    <td class="text-center"><?=get_parameter_value('jenisdiagnosis_cd', $row['jenisdiagnosis_cd'])?></td>
    <td class="text-center"><?=get_parameter_value('tipediagnosis_cd', $row['tipediagnosis_cd'])?></td>
    <td class="text-left"><?=$row['keterangan_diagnosis']?></td>
    <td class="text-center"><?=$row['lokasi_nm']?></td>
	</tr>
	<?php endforeach; ?>
<?php endif; ?>