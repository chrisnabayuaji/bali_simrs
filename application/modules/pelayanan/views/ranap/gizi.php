<!-- js -->
<?php $this->load->view('_js_gizi'); ?>
<div class="row">
  <div class="col-md-7">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-list"></i> List Data Order Gizi</h4>
        <!-- <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="pills-saat-ini-gizi-tab" data-toggle="pill" href="#pills-saat-ini-gizi" role="tab" aria-controls="pills-saat-ini-gizi" aria-selected="false"><i class="fas fa-copy"></i> Data Saat Ini</a>
          </li>
          <li class="nav-item">
            <a class="nav-link " id="pills-history-gizi-tab" data-toggle="pill" href="#pills-history-gizi" role="tab" aria-controls="pills-history-gizi" aria-selected="true"><i class="fas fa-history"></i> Data History</a>
          </li>
        </ul> -->
        <div class="tab-content p-0" id="pills-tabContent-gizi" style="border:0px !important">
          <div class="tab-pane fade show active" id="pills-saat-ini-gizi" role="tabpanel" aria-labelledby="pills-saat-ini-gizi-tab">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-sm">
                <thead>
                  <tr>
                    <th class="text-center" width="20">No</th>
                    <th class="text-center" width="60">Aksi</th>
                    <th class="text-center" width="135">Tanggal</th>
                    <th class="text-center">Jenis Gizi</th>
                    <th class="text-center">Jam Pemberian</th>
                    <th class="text-center">Keterangan</th>
                    <th class="text-center">Status</th>
                  </tr>
                </thead>
                <tbody id="gizi_data"></tbody>
              </table>
            </div>
          </div>
          <div class="tab-pane fade" id="pills-history-gizi" role="tabpanel" aria-labelledby="pills-history-gizi-tab">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-sm">
                <thead>
                  <tr>
                    <th class="text-center" width="20">No</th>
                    <th class="text-center" width="60">Aksi</th>
                    <th class="text-center" width="135">Tanggal</th>
                    <th class="text-center">Jam Pemberian</th>
                    <th class="text-center">Jenis Gizi</th>
                    <th class="text-center">Keterangan</th>
                    <th class="text-center">Petugas</th>
                  </tr>
                </thead>
                <tbody id="gizi_history"></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-5">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-utensils"></i> Order Gizi</h4>
        <form id="gizi_form" action="" method="post" autocomplete="off">
          <input type="hidden" name="gizi_id" id="gizi_id">
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Tanggal Catat <span class="text-danger">*</span></label>
            <div class="col-lg-5 col-md-5">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                </div>
                <input type="text" class="form-control datetimepicker" name="tgl_catat" id="tgl_catat" required="" aria-invalid="false">
              </div>
            </div>
          </div>
          <div class="form-group row ">
            <label class="col-lg-3 col-md-3 col-form-label">Jam Pemberian <span class="text-danger">*</span></label>
            <div class="col-lg-6 col-md-3">
              <select class="form-control chosen-select" name="jamgizi_id" id="jamgizi_id" required>
                <option value="">- Pilih Jam Pemberian -</option>
                <?php foreach ($jamgizi as $row) : ?>
                  <option value="<?= $row['jamgizi_id'] ?>"><?= $row['jamgizi_id'] ?> - <?= $row['jamgizi_nm'] ?> - <?= $row['jam_pemberian'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row ">
            <label class="col-lg-3 col-md-3 col-form-label">Jenis Gizi <span class="text-danger">*</span></label>
            <div class="col-lg-6 col-md-3">
              <select class="form-control chosen-select" name="jenisgizi_id" id="jenisgizi_id" required>
                <option value="">- Pilih Jenis Gizi -</option>
                <?php foreach ($jenisgizi as $row) : ?>
                  <option value="<?= $row['jenisgizi_id'] ?>"><?= $row['jenisgizi_id'] ?> - <?= $row['jenisgizi_nm'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Keterangan <span class="text-danger">*</span></label>
            <div class="col-lg-9 col-md-3">
              <textarea class="form-control" name="keterangan_gizi" id="keterangan_gizi" value="" required="" rows="5"></textarea>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Petugas Order</label>
            <div class="col-lg-7 col-md-3">
              <select class="form-control select2" name="petugas_id" id="petugas_id_gizi">
                <option value="">---</option>
              </select>
            </div>
            <div class="col-lg-1 col-md-1">
              <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_gizi/search_petugas' ?>" modal-title="List Data Petugas" modal-size="lg" class="btn btn-xs btn-default modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Petugas" data-original-title="Cari Petugas"><i class="fas fa-search"></i></a>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-9 offset-lg-3 p-0">
              <button type="submit" id="gizi_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
              <button type="button" id="gizi_cancel" class="btn btn-xs btn-secondary"><i class="fas fa-times"></i> Batal</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>