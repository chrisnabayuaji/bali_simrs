<?php if (@$main == null) : ?>
  <tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else : ?>
  <?php $i = 1;
  foreach ($main as $row) : ?>
    <tr>
      <td class="text-center"><?= $i++ ?></td>
      <td class="text-center">
        <?php if ($row['status_pemberian'] == 0) : ?>
          <a href="javascript:void(0)" class="btn btn-primary btn-table btn-edit" data-gizi-id="<?= $row['gizi_id'] ?>" data-reg-id="<?= $row['reg_id'] ?>" data-pasien-id="<?= $row['pasien_id'] ?>" title="Ubah Data"><i class="fas fa-pencil-alt"></i></a>
          <a href="javascript:void(0)" class="btn btn-danger btn-table btn-delete" data-gizi-id="<?= $row['gizi_id'] ?>" data-reg-id="<?= $row['reg_id'] ?>" data-pasien-id="<?= $row['pasien_id'] ?>" data-lokasi-id="<?= $row['lokasi_id'] ?>" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
        <?php endif; ?>
      </td>
      <td class="text-center"><?= to_date($row['tgl_catat'], '', 'full_date') ?></td>
      <td class="text-left"><?= $row['jenisgizi_id'] . ' - ' . $row['jenisgizi_nm'] ?></td>
      <td class="text-left"><?= $row['jamgizi_id'] . ' - ' . $row['jamgizi_nm'] . ' - ' . $row['jam_pemberian'] ?></td>
      <td class="text-left"><?= $row['keterangan_gizi'] ?></td>
      <td class="text-center">
        <li class="fa <?= ($row['status_pemberian'] == 1 ? 'fa-check-circle text-success' : 'fa-minus-circle text-disabled') ?>"></li>
      </td>
    </tr>
  <?php endforeach; ?>
<?php endif; ?>

<script type="text/javascript">
  $(document).ready(function() {
    // edit
    $('.btn-edit').bind('click', function(e) {
      e.preventDefault();
      var gizi_id = $(this).attr("data-gizi-id");
      var reg_id = $(this).attr("data-reg-id");
      var pasien_id = $(this).attr("data-pasien-id");
      //
      $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_gizi/get_data' ?>', {
        gizi_id: gizi_id,
        reg_id: reg_id,
        pasien_id: pasien_id
      }, function(data) {

        $('.datetimepicker').daterangepicker({
          startDate: moment(data.main.tgl_catat),
          endDate: moment(),
          timePicker: true,
          timePicker24Hour: true,
          timePickerSeconds: true,
          singleDatePicker: true,
          showDropdowns: true,
          locale: {
            cancelLabel: 'Clear',
            format: 'DD-MM-YYYY H:mm:ss'
          },
          isInvalidDate: function(date) {
            return '';
          }
        })

        var data2 = {
          id: data.main.petugas_id,
          text: data.main.petugas_nm
        };
        var newOption = new Option(data2.text, data2.id, false, false);
        $('#petugas_id_gizi').append(newOption).trigger('change');
        $('#petugas_id_gizi').val(data.main.petugas_order);
        $('#petugas_id_gizi').trigger('change');
        $('.select2-container').css('width', '100%');

        $('#gizi_id').val(data.main.gizi_id);
        $('#jamgizi_id').val(data.main.jamgizi_id).trigger('change');
        $('#jenisgizi_id').val(data.main.jenisgizi_id).trigger('change');
        $('#keterangan_gizi').val(data.main.keterangan_gizi);
        $('#gizi_action').html('<i class="fas fa-save"></i> Ubah');
      }, 'json');
    });
    // delete
    $('.btn-delete').on('click', function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Apakah Anda yakin?',
        text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#eb3b5a',
        cancelButtonColor: '#b2bec3',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        customClass: 'swal-wide'
      }).then((result) => {
        if (result.value) {
          var gizi_id = $(this).attr("data-gizi-id");
          var reg_id = $(this).attr("data-reg-id");
          var pasien_id = $(this).attr("data-pasien-id");
          var lokasi_id = $(this).attr("data-lokasi-id");
          //
          $('#gizi_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
          $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_gizi/delete_data' ?>', {
            gizi_id: gizi_id,
            reg_id: reg_id,
            pasien_id: pasien_id,
            lokasi_id: lokasi_id
          }, function(data) {
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil dihapus.',
              icon: 'success',
              position: 'top-right'
            })
            $('#gizi_data').html(data.html);
          }, 'json');
        }
      })
    })
  });
</script>