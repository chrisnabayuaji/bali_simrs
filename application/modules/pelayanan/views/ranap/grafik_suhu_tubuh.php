<script>
  var color = Chart.helpers.color;
  var ctx = document.getElementById('canvas-suhu-tubuh').getContext('2d');
  window.myLine = new Chart(ctx, {
    type: 'line',
    data: {
      labels: [
        <?php foreach ($main as $date) : ?> '<?= to_date($date['tgl_catat'], '', 'full_date') ?>',
        <?php endforeach; ?>
      ],
      datasets: [{
        label: 'Suhu Tubuh',
        backgroundColor: 'rgb(255, 99, 132)',
        borderColor: 'rgb(255, 99, 132)',
        data: [
          <?php foreach ($main as $suhu) : ?> '<?= $suhu['suhu'] ?>',
          <?php endforeach; ?>
        ],
        fill: false,
      }]
    },
    options: {
      responsive: true,
      legend: {
        position: 'bottom',
      },
      title: {
        display: false,
        text: ''
      },
      tooltips: {
        mode: 'index',
        intersect: false,
      },
      hover: {
        mode: 'nearest',
        intersect: true
      },
      scales: {
        xAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Tanggal Catat',
            fontStyle: 'oblique'
          }
        }],
        yAxes: [{
          display: true,
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });
</script>