<script>
  var color = Chart.helpers.color;
  var ctx = document.getElementById('canvas-tekanan-darah').getContext('2d');
  window.myBar = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: [
        <?php foreach ($main as $date) : ?> '<?= to_date($date['tgl_catat'], '', 'full_date') ?>',
        <?php endforeach; ?>
      ],
      datasets: [{
        label: 'Systole',
        backgroundColor: color('rgb(255, 99, 132)').alpha(0.5).rgbString(),
        borderColor: 'rgb(255, 99, 132)',
        borderWidth: 1,
        data: [
          <?php foreach ($main as $systole) : ?> '<?= $systole['systole'] ?>',
          <?php endforeach; ?>
        ]
      }, {
        label: 'Diastole',
        backgroundColor: color('rgb(54, 162, 235)').alpha(0.5).rgbString(),
        borderColor: 'rgb(54, 162, 235)',
        borderWidth: 1,
        data: [
          <?php foreach ($main as $diastole) : ?> '<?= $diastole['diastole'] ?>',
          <?php endforeach; ?>
        ]
      }]
    },
    options: {
      responsive: true,
      legend: {
        position: 'bottom',
      },
      title: {
        display: false,
        text: ''
      },
      tooltips: {
        mode: 'label',
        callbacks: {
          label: function(t, d) {
            var dstLabel = d.datasets[t.datasetIndex].label;
            var yLabel = t.yLabel;
            return dstLabel + ' : ' + yLabel;
          }
        }
      },
      scales: {
        xAxes: [{
          display: true,
          scaleLabel: {
            display: true,
            labelString: 'Tanggal Catat',
            fontStyle: 'oblique'
          }
        }],
        yAxes: [{
          display: true,
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }
  });
</script>