<!-- js -->
<?php $this->load->view('_js_history'); ?>
<div class="row">
  <div class="col-12">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-user-injured"></i> History Pasien</h4>
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-sm">
            <thead>
              <tr>
                <th class="text-center text-middle" width="20">No</th>
                <th class="text-center text-middle" width="150">No. Register</th>
                <th class="text-center text-middle" width="70">Aksi</th>
                <th class="text-center text-middle">Tgl. Registrasi</th>
                <th class="text-center text-middle">Jenis Pasien</th>
                <th class="text-center text-middle">Bangsa Kamar</th>
                <th class="text-center text-middle">Dokter PJ</th>
              </tr>
            </thead>
            <tbody id="pasien_history"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>