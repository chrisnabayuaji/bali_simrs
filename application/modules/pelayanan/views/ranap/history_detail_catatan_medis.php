<?php if (@$main == null) : ?>
  <tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else : ?>
  <?php $i = 1;
  foreach ($main as $row) : ?>
    <tr>
      <td class="text-center text-top"><?= $i++ ?></td>
      <td class="text-center text-top"><?= to_date($row['tgl_catat'], '', 'full_date') ?></td>
      <td class="text-left text-top"><?= $row['riwayat_penyakit'] ?></td>
      <td class="text-left text-top"><?= $row['alergi_obat'] ?></td>
      <td class="text-center text-top"><?= $row['lokasi_nm'] ?></td>
      <td class="text-left text-top"><?= $row['petugas_nm'] ?></td>
    </tr>
  <?php endforeach; ?>
<?php endif; ?>