<?php if (@$main == null) : ?>
  <tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else : ?>
  <?php $i = 1;
  foreach ($main as $row) : ?>
    <tr>
      <td class="text-center text-top"><?= $i++ ?></td>
      <td class="text-center text-top"><?= to_date($row['tgl_catat'], '', 'full_date') ?></td>
      <td class="text-left text-top"><?= $row['keluhan_utama'] ?></td>
      <td class="text-center text-top"><?= $row['systole'] ?></td>
      <td class="text-center text-top"><?= $row['diastole'] ?></td>
      <td class="text-center text-top"><?= $row['tinggi'] ?></td>
      <td class="text-center text-top"><?= $row['berat'] ?></td>
      <td class="text-center text-top"><?= $row['suhu'] ?></td>
      <td class="text-center text-top"><?= $row['nadi'] ?></td>
      <td class="text-center text-top"><?= $row['respiration_rate'] ?></td>
      <td class="text-center text-top"><?= $row['sao2'] ?></td>
      <td class="text-left text-top"><?= $row['petugas_nm'] ?></td>
    </tr>
  <?php endforeach; ?>
<?php endif; ?>