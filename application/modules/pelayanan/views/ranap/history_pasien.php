<?php if (@$main == null) : ?>
  <tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else : ?>
  <?php $i = 1;
  foreach ($main as $row) : ?>
    <tr>
      <td class="text-center"><?= $i++ ?></td>
      <td class="text-center"><?= $row['reg_id'] ?></td>
      <td class="text-center">
        <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_history/detail/' . $row['reg_id'] ?>" modal-tab="history" modal-title="Detail History Pasien" modal-size="lg" class="btn btn-info btn-table modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-info" title="Detail Data" data-original-title="Detail Data"><i class="fas fa-list-alt"></i></a>
      </td>
      <td class="text-center"><?= to_date($row['tgl_registrasi'], '', 'full_date') ?></td>
      <td class="text-center"><?= $row['jenispasien_nm'] ?></td>
      <td class="text-left">
        <b>Bangsal &nbsp;:&nbsp; </b><?= $row['lokasi_nm'] ?><br>
        <b>Kamar &nbsp;&nbsp;&nbsp;:&nbsp; </b><?= $row['kamar_nm'] ?>
      </td>
      <td class="text-center"><?= $row['dokter_pj'] ?></td>
    </tr>
  <?php endforeach; ?>
<?php endif; ?>

<script type="text/javascript">
  $(document).ready(function() {
    $('.modal-href').click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");
      var modal_tab = $(this).attr("modal-tab");

      $("#modal-title").html(modal_title);
      $("#modal-size").addClass('modal-' + modal_size);
      if (modal_custom_size) {
        $("#modal-size").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
      }
      $("#myModal").modal('show');
      $.post($(this).data('href'), function(data) {
        $("#modal-body").html(data.html);
      }, 'json');
    });
  });
</script>