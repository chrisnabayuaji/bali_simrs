<?php if ($main == null) : ?>
  <tbody>
    <tr>
      <td class="text-center" colspan="99">Tidak ada data.</td>
    </tr>
  </tbody>
<?php else : ?>
  <?php $no = 1;
  foreach ($main as $row) : ?>
    <?php if ($row['parent_id'] == '') : ?>
      <tr>
        <td colspan="99"><b><?= @$row['itemlab_nm'] ?></b></td>
      </tr>
    <?php else : ?>
      <tr>
        <td class="text-center"><?= $no++ ?></td>
        <td class="text-left"><?= $row['itemlab_id'] ?></td>
        <td class="text-left pl-<?= count(explode('.', $row['itemlab_id'])) ?>" colspan="99">|- <?= $row['itemlab_nm'] ?></td>
      </tr>
    <?php endif; ?>
    <?php if (count($row['rinc']) > 0) : ?>
      <?php foreach ($row['rinc'] as $row2) : ?>
        <tr>
          <td class="text-center"><?= $no++ ?></td>
          <td class="text-left"><?= $row2['itemlab_id'] ?></td>
          <td class="text-left pl-<?= count(explode('.', $row2['itemlab_id'])) ?>">|- <?= $row2['itemlab_nm'] ?></td>
          <td class="text-center"><?= to_date(@$row2['tgl_hasil'], '-', 'full_date') ?></td>
          <td class="text-center">
            <?php
            switch ($row2['tipe_rujukan']) {
              case 1:
                echo ($row2['konfirmasi'] == 0) ? 'Negatif' : 'Positif';
                break;

              case 2:
                echo float_id($row2['rentang_min']) . " - " . float_id($row2['rentang_max']) . " " . $row2['satuan'];
                break;

              case 3:
                echo "Laki-laki : " . float_id($row2['rentang_l_min']) . " - " . float_id($row2['rentang_l_max']) . " " . $row2['satuan'] . "<br>";
                echo "Perempuan : " . float_id($row2['rentang_p_min']) . " - " . float_id($row2['rentang_p_max']) . " " . $row2['satuan'];
                break;

              case 4:
                echo "< "  . float_id($row2['kurang']) . " " . $row2['satuan'];
                break;

              case 5:
                echo "Laki-laki : < " . float_id($row2['kurang_l']) . $row2['satuan'] . "<br>";
                echo "Perempuan : < " . float_id($row2['kurang_p'])  . $row2['satuan'];
                break;

              case 6:
                echo "> " . float_id($row2['lebih']) . " " . $row2['satuan'];
                break;

              case 7:
                echo "Laki-laki : > " . float_id($row2['lebih_l']) . $row2['satuan'] . "<br>";
                echo "Perempuan : > " . float_id($row2['lebih_p'])  . $row2['satuan'];
                break;

              case 8:
                echo $row2['informasi'];
                break;
            }
            ?>
          </td>
          <td class="text-center">
            <?php if ($row2['hasil_lab'] != '') : ?>
              <?php if ($row2['tipe_rujukan'] == 1) : ?>
                <?php if ($row2['hasil_lab'] == 1) : ?>
                  Positif
                <?php else : ?>
                  Negatif
                <?php endif; ?>
              <?php elseif ($row2['tipe_rujukan'] > 1 && $row2['tipe_rujukan'] <= 7) : ?>
                <?= num_id($row2['hasil_lab']) ?>
              <?php else : ?>
                <?= $row2['hasil_lab'] ?>
              <?php endif; ?>
            <?php endif; ?>
          </td>
          <td class="text-left"><?= $row2['catatan_lab'] ?></td>
        </tr>
      <?php endforeach; ?>
    <?php endif; ?>
  <?php endforeach; ?>
<?php endif; ?>