<?php if (@$main == null) : ?>
  <tr>
    <th class="text-center" width="20">No</th>
    <th class="text-center" width="60">Aksi</th>
    <th class="text-center">Nama Obat</th>
    <th class="text-center">Jenis</th>
    <th class="text-center">Dosis</th>
    <th class="text-center">Qty</th>
    <th class="text-center">Aturan Pakai</th>
    <th class="text-center">Jam Minum</th>
    <th class="text-center">Peringatan Khusus</th>
  </tr>
  <tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else : ?>
  <?php foreach ($main as $row) : ?>
    <tr bgcolor="#f5f5f5">
      <td class="text-left" colspan="99"><b>NO.REGISTER : <?= $row['resep_id'] ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; TANGGAL : <?= to_date($row['tgl_catat'], '', 'date') ?></b></td>
    </tr>
    <tr>
      <th class="text-center" width="20">No</th>
      <th class="text-center" width="60">Aksi</th>
      <th class="text-center">Nama Obat</th>
      <th class="text-center">Qty</th>
      <th class="text-center">Aturan Pakai</th>
      <th class="text-center">Jadwal Pemakaian</th>
      <th class="text-center">Peringatan Khusus</th>
    </tr>
    <?php $i = 1;
    foreach ($row['list_dat_resep'] as $r) : ?>
      <tr>
        <td class="text-center" width="20"><?= $i++ ?></td>
        <td class="text-center" width="60">
          <a href="javascript:void(0)" class="btn btn-primary btn-table btn-edit" data-farmasi-id="<?= $r['farmasi_id'] ?>" data-resep-id="<?= $r['resep_id'] ?>" data-reg-id="<?= $r['reg_id'] ?>" title="Ubah Data">
            <i class="fas fa-pencil-alt"></i>
          </a>
          <a href="javascript:void(0)" class="btn btn-danger btn-table btn-delete" data-farmasi-id="<?= $r['farmasi_id'] ?>" data-resep-id="<?= $r['resep_id'] ?>" data-reg-id="<?= $r['reg_id'] ?>" data-lokasi-id="<?= $r['lokasi_id'] ?>" data-pasien-id="<?= $r['pasien_id'] ?>" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
        </td>
        <td class="text-left"><?= $r['obat_nm'] ?></td>
        <td class="text-center"><?= $r['qty'] ?></td>
        <td class="text-center"><?= $r['aturan_pakai'] ?></td>
        <td class="text-left"><?= $r['jadwal'] ?></td>
        <td class="text-left"><?= $r['aturan_tambahan'] ?></td>
      </tr>
    <?php endforeach; ?>
  <?php endforeach; ?>
<?php endif; ?>

<script type="text/javascript">
  $(document).ready(function() {
    // edit
    $('.btn-edit').bind('click', function(e) {
      e.preventDefault();
      var farmasi_id = $(this).attr("data-farmasi-id");
      var resep_id = $(this).attr("data-resep-id");
      var reg_id = $(this).attr("data-reg-id");


      //
      $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_pemberian_obat/get_data' ?>', {
        farmasi_id: farmasi_id,
        resep_id: resep_id,
        reg_id: reg_id,
      }, function(data) {
        console.log(data);
        $('#resep_id').val(data.main.resep_id);

        // autocomplete
        var data2 = {
          id: data.main.obat_id + '#' + data.main.obat_nm + '#' + data.main.stokdepo_id,
          text: data.main.obat_id + ' - ' + data.main.obat_nm
        };
        var newOption = new Option(data2.text, data2.id, false, false);
        $('#obat_id').append(newOption).trigger('change');
        $('#obat_id').val(data.main.obat_id + '#' + data.main.obat_nm + '#' + data.main.stokdepo_id);
        $('#obat_id').trigger('change');
        $('.select2-container').css('width', '100%');
        $("#tgl_catat").val(to_date(data.main.tgl_catat, '-', 'full_date')).removeClass("is-valid").removeClass("is-invalid");
        $("#farmasi_id").val(data.main.farmasi_id).removeClass("is-valid").removeClass("is-invalid");
        $("#stokdepo_id").val(data.main.stokdepo_id).removeClass("is-valid").removeClass("is-invalid");
        $("#aturan_pakai").val(data.main.aturan_pakai).trigger('change').removeClass("is-valid").removeClass("is-invalid");
        $("#jadwal").val(data.main.jadwal).trigger('change').removeClass("is-valid").removeClass("is-invalid");
        $("#aturan_tambahan").val(data.main.aturan_tambahan).trigger('change').removeClass("is-valid").removeClass("is-invalid");
        $("#keterangan_resep").val(data.main.keterangan_resep).removeClass("is-valid").removeClass("is-invalid");
        $('#pemberian_obat_action').html('<i class="fas fa-save"></i> Ubah');
      }, 'json');
    });
    // delete
    $('.btn-delete').on('click', function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Apakah Anda yakin?',
        text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#eb3b5a',
        cancelButtonColor: '#b2bec3',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        customClass: 'swal-wide'
      }).then((result) => {
        if (result.value) {
          var resep_id = $(this).attr("data-resep-id");
          var reg_id = $(this).attr("data-reg-id");
          var farmasi_id = $(this).attr("data-farmasi-id");
          var lokasi_id = $(this).attr("data-lokasi-id");
          var pasien_id = $(this).attr("data-pasien-id");
          //
          $('#pemberian_obat_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
          $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_pemberian_obat/delete_data' ?>', {
            resep_id: resep_id,
            reg_id: reg_id,
            farmasi_id: farmasi_id,
            lokasi_id: lokasi_id,
            pasien_id: pasien_id,
          }, function(data) {
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil dihapus.',
              icon: 'success',
              position: 'top-right'
            })
            $('#pemberian_obat_data').html(data.html);
          }, 'json');
        }
      })
    })
  });
</script>