<!-- js -->
<?php $this->load->view('_js_pemeriksaan_fisik'); ?>
<div class="row">
  <div class="col-md-5">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-stethoscope"></i> Pemeriksaan Fisik</h4>
        <div class="tab-content p-0" id="pills-tabContent-pemeriksaan-fisik" style="border:0px !important">
          <div class="tab-pane fade show active" id="pills-saat-ini-pemeriksaan-fisik" role="tabpanel" aria-labelledby="pills-saat-ini-pemeriksaan-fisik-tab">
            <form id="pemeriksaan_fisik_form" action="" method="post" autocomplete="off">
              <input type="hidden" name="anamnesis_id" id="anamnesis_id">
              <div class="form-group row">
                <label class="col-lg-3 col-md-3 col-form-label">Tanggal Catat</label>
                <div class="col-lg-5 col-md-5">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                    </div>
                    <input type="text" class="form-control datetimepicker" name="tgl_catat" id="tgl_catat" required="" aria-invalid="false">
                  </div>
                </div>
              </div>
              <div class="form-group row mb-3">
                <label class="col-lg-3 col-md-3 col-form-label">Keluhan Utama</label>
                <div class="col-lg-9 col-md-3">
                  <textarea class="form-control" name="keluhan_utama" id="keluhan_utama" value="" required="" rows="5"></textarea>
                </div>
              </div>
              <div class="form-group row mb-2">
                <label class="col-lg-3 col-md-3 col-form-label">Systole</label>
                <div class="col-lg-3 col-md-3">
                  <input class="form-control" type="text" name="systole" id="systole" value="" />
                </div>
                <label class="col-lg-2 col-md-3 col-form-label">Diastole</label>
                <div class="col-lg-3 col-md-3">
                  <input class="form-control" type="text" name="diastole" id="diastole" value="" />
                </div>
              </div>
              <div class="form-group row mb-2">
                <label class="col-lg-3 col-md-3 col-form-label">Tinggi</label>
                <div class="col-lg-3 col-md-3">
                  <input class="form-control" type="text" name="tinggi" id="tinggi" value="" />
                </div>
                <label class="col-lg-2 col-md-3 col-form-label">Berat</label>
                <div class="col-lg-3 col-md-3">
                  <input class="form-control" type="text" name="berat" id="berat" value="" />
                </div>
              </div>
              <div class="form-group row mb-2">
                <label class="col-lg-3 col-md-3 col-form-label">Suhu</label>
                <div class="col-lg-3 col-md-3">
                  <input class="form-control" type="text" name="suhu" id="suhu" value="" />
                </div>
                <label class="col-lg-2 col-md-3 col-form-label">Nadi</label>
                <div class="col-lg-3 col-md-3">
                  <input class="form-control" type="text" name="nadi" id="nadi" value="" />
                </div>
              </div>
              <div class="form-group row mb-2">
                <label class="col-lg-3 col-md-3 col-form-label">Respiration Rate</label>
                <div class="col-lg-3 col-md-3">
                  <input class="form-control" type="text" name="respiration_rate" id="respiration_rate" value="" />
                </div>
              </div>
              <div class="form-group row mb-2">
                <label class="col-lg-3 col-md-3 col-form-label">SpO<sub>2</sub></label>
                <div class="col-lg-3 col-md-3">
                  <input class="form-control" type="text" name="sao2" id="sao2" value="" />
                </div>
              </div>
              <div class="form-group row mb-2">
                <label class="col-lg-3 col-md-3 col-form-label">Petugas</label>
                <div class="col-lg-6 col-md-6">
                  <select class="form-control select2" name="petugas_id" id="petugas_id_pemeriksaan_fisik">
                    <option value="">---</option>
                  </select>
                </div>
                <div class="col-lg-1 col-md-1">
                  <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_pemeriksaan_fisik/search_petugas' ?>" modal-title="List Data Petugas" modal-size="lg" class="btn btn-xs btn-default modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Petugas" data-original-title="Cari Petugas"><i class="fas fa-search"></i></a>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-9 offset-lg-3 p-0">
                  <button type="submit" id="pemeriksaan_fisik_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
                  <button type="button" id="pemeriksaan_fisik_cancel" class="btn btn-xs btn-secondary"><i class="fas fa-times"></i> Batal</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-7">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-list"></i> List Data Pemeriksaan Fisik</h4>
        <div class="tab-content p-0" id="pills-tabContent-catatan-medis" style="border:0px !important">
          <div class="tab-pane fade show active" id="pills-saat-ini-catatan-medis" role="tabpanel" aria-labelledby="pills-saat-ini-catatan-medis-tab">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-sm">
                <thead>
                  <tr>
                    <th class="text-center text-middle" width="20">No</th>
                    <th class="text-center text-middle" width="60">Aksi</th>
                    <th class="text-center text-middle" width="90">Tanggal</th>
                    <th class="text-center text-middle" width="130">Keluhan<br>Utama</th>
                    <th class="text-center text-middle">Systole</th>
                    <th class="text-center text-middle">Diastole</th>
                    <th class="text-center text-middle">Tinggi</th>
                    <th class="text-center text-middle">Berat</th>
                    <th class="text-center text-middle">Suhu</th>
                    <th class="text-center text-middle">Nadi</th>
                    <th class="text-center text-middle">Respiration<br>Rate</th>
                    <th class="text-center text-middle">SpO<sub>2</sub></th>
                  </tr>
                </thead>
                <tbody id="pemeriksaan_fisik_data"></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="card border-none no-shadow mt-3">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-chart-bar"></i> Grafik Tekanan Darah</h4>
        <div id="grafik-tekanan-darah"></div>
        <div class="table-responsive">
          <div id="body-chart-tekanan-darah" style="width: 100%;"></div>
        </div>
      </div>
    </div>
    <div class="card border-none no-shadow mt-3">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-chart-line"></i> Grafik Suhu Tubuh</h4>
        <div id="grafik-suhu-tubuh"></div>
        <div class="table-responsive">
          <div id="body-chart-suhu-tubuh" style="width: 100%;"></div>
        </div>
      </div>
    </div>
  </div>
</div>