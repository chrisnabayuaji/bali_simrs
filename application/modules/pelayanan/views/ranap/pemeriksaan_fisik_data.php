<?php if (@$main == null) : ?>
  <tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else : ?>
  <?php $i = 1;
  foreach ($main as $row) : ?>
    <tr>
      <td class="text-center text-top" rowspan="2"><?= $i++ ?></td>
      <td class="text-center text-top">
        <a href="javascript:void(0)" class="btn btn-primary btn-table btn-edit" data-anamnesis-id="<?= $row['anamnesis_id'] ?>" data-reg-id="<?= $row['reg_id'] ?>" data-pasien-id="<?= $row['pasien_id'] ?>" title="Ubah Data"><i class="fas fa-pencil-alt"></i></a>
        <a href="javascript:void(0)" class="btn btn-danger btn-table btn-delete" data-anamnesis-id="<?= $row['anamnesis_id'] ?>" data-reg-id="<?= $row['reg_id'] ?>" data-pasien-id="<?= $row['pasien_id'] ?>" data-lokasi-id="<?= $row['lokasi_id'] ?>" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
      </td>
      <td class="text-center text-top"><?= to_date($row['tgl_catat'], '', 'full_date', '<br>') ?></td>
      <td class="text-left text-top"><?= $row['keluhan_utama'] ?></td>
      <td class="text-center text-top"><?= $row['systole'] ?></td>
      <td class="text-center text-top"><?= $row['diastole'] ?></td>
      <td class="text-center text-top"><?= $row['tinggi'] ?></td>
      <td class="text-center text-top"><?= $row['berat'] ?></td>
      <td class="text-center text-top"><?= $row['suhu'] ?></td>
      <td class="text-center text-top"><?= $row['nadi'] ?></td>
      <td class="text-center text-top"><?= $row['respiration_rate'] ?></td>
      <td class="text-center text-top"><?= $row['sao2'] ?></td>
    </tr>
    <tr>
      <td colspan="11">
        <div style="margin-left: 60px;">
          <b>Petugas : </b> <?= ($row['petugas_nm'] != '') ? $row['petugas_nm'] : '-' ?>
        </div>
      </td>
    </tr>
  <?php endforeach; ?>
<?php endif; ?>

<script type="text/javascript">
  $(document).ready(function() {
    // edit
    $('.btn-edit').bind('click', function(e) {
      e.preventDefault();
      var anamnesis_id = $(this).attr("data-anamnesis-id");
      var reg_id = $(this).attr("data-reg-id");
      var pasien_id = $(this).attr("data-pasien-id");
      //
      $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_pemeriksaan_fisik/get_data' ?>', {
        anamnesis_id: anamnesis_id,
        reg_id: reg_id,
        pasien_id: pasien_id
      }, function(data) {

        $('.datetimepicker').daterangepicker({
          startDate: moment(data.main.tgl_catat),
          endDate: moment(),
          timePicker: true,
          timePicker24Hour: true,
          timePickerSeconds: true,
          singleDatePicker: true,
          showDropdowns: true,
          locale: {
            cancelLabel: 'Clear',
            format: 'DD-MM-YYYY H:mm:ss'
          },
          isInvalidDate: function(date) {
            return '';
          }
        })

        // autocomplete
        var data2 = {
          id: data.main.petugas_id,
          text: data.main.petugas_nm
        };
        var newOption = new Option(data2.text, data2.id, false, false);
        $('#petugas_id').append(newOption).trigger('change');
        $('#petugas_id').val(data.main.petugas_id);
        $('#petugas_id').trigger('change');
        $('.select2-container').css('width', '100%');

        $('#anamnesis_id').val(data.main.anamnesis_id);
        $('#keluhan_utama').val(data.main.keluhan_utama);
        $('#systole').val(data.main.systole);
        $('#diastole').val(data.main.diastole);
        $('#tinggi').val(data.main.tinggi);
        $('#berat').val(data.main.berat);
        $('#suhu').val(data.main.suhu);
        $('#nadi').val(data.main.nadi);
        $('#respiration_rate').val(data.main.respiration_rate);
        $('#sao2').val(data.main.sao2);
        $('#pemeriksaan_fisik_action').html('<i class="fas fa-save"></i> Ubah');
      }, 'json');
    });
    // delete
    $('.btn-delete').on('click', function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Apakah Anda yakin?',
        text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#eb3b5a',
        cancelButtonColor: '#b2bec3',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        customClass: 'swal-wide'
      }).then((result) => {
        if (result.value) {
          var anamnesis_id = $(this).attr("data-anamnesis-id");
          var reg_id = $(this).attr("data-reg-id");
          var pasien_id = $(this).attr("data-pasien-id");
          var lokasi_id = $(this).attr("data-lokasi-id");
          //
          $('#pemeriksaan_fisik_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
          $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_pemeriksaan_fisik/delete_data' ?>', {
            anamnesis_id: anamnesis_id,
            reg_id: reg_id,
            pasien_id: pasien_id,
            lokasi_id: lokasi_id
          }, function(data) {
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil dihapus.',
              icon: 'success',
              position: 'top-right'
            })
            $('#pemeriksaan_fisik_data').html(data.html);
          }, 'json');

          setTimeout(function() {
            $('#body-chart-tekanan-darah').html('');
            GrafikTekananDarah(pasien_id, reg_id, lokasi_id);
          }, 1000);
          setTimeout(function() {
            $('#body-chart-suhu-tubuh').html('');
            GrafikSuhuTubuh(pasien_id, reg_id, lokasi_id);
          }, 2000);
        }
      })
    })
  });

  function GrafikTekananDarah(pasien_id = '', reg_id = '', lokasi_id = '') {
    $('#grafik-tekanan-darah').html('<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i></div><div class="text-center mt-2">Memuat...</div>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_pemeriksaan_fisik/grafik_tekanan_darah' ?>', {
      pasien_id: pasien_id,
      reg_id: reg_id,
      lokasi_id: lokasi_id
    }, function(data) {
      if (data.html == 'Tidak ada data') {
        $('#body-chart-tekanan-darah').html('<div class="mb-2"></div>');
      } else {
        $('#body-chart-tekanan-darah').html('<canvas id="canvas-tekanan-darah"></canvas>');
      }
      $('#grafik-tekanan-darah').html(data.html);
    }, 'json');
  }

  function GrafikSuhuTubuh(pasien_id = '', reg_id = '', lokasi_id = '') {
    $('#grafik-suhu-tubuh').html('<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i></div><div class="text-center mt-2">Memuat...</div>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_pemeriksaan_fisik/grafik_suhu_tubuh' ?>', {
      pasien_id: pasien_id,
      reg_id: reg_id,
      lokasi_id: lokasi_id
    }, function(data) {
      if (data.html == 'Tidak ada data') {
        $('#body-chart-suhu-tubuh').html('<div class="mb-2"></div>');
      } else {
        $('#body-chart-suhu-tubuh').html('<canvas id="canvas-suhu-tubuh"></canvas>');
      }
      $('#grafik-suhu-tubuh').html(data.html);
    }, 'json');
  }
</script>