<form id="radiologi_form" action="" method="post" autocomplete="off">
  <div class="row">
    <input type="hidden" name="pemeriksaan_id" id="pemeriksaan_id" value="<?= @$mainrad['pemeriksaan_id'] ?>">
    <input type="hidden" name="reg_id" id="reg_id" value="<?= @$reg['reg_id'] ?>">
    <input type="hidden" name="src_reg_id" id="src_reg_id" value="<?= @$reg['reg_id'] ?>">
    <input type="hidden" name="pasien_id" id="pasien_id" value="<?= @$reg['pasien_id'] ?>">
    <input type="hidden" name="kelas_id" id="kelas_id" value="<?= @$reg['kelas_id'] ?>">
    <div class="col-5">
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Dokter Pengirim <span class="text-danger">*</span></label>
        <div class="col-lg-8 col-md-3">
          <input type="text" class="form-control" id="dokterpengirim_nm" value="<?= @$reg['pegawai_nm'] ?>" required="" readonly>
          <input type="hidden" name="dokterpengirim_id" value="<?= $reg['dokter_id'] ?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Tanggal Order<span class="text-danger">*<span></label>
        <div class="col-lg-5 col-md-9">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
            </div>
            <input type="text" class="form-control datetimepicker" name="tgl_order" id="tgl_order" value="<?php if (@$mainrad) {
                                                                                                            echo to_date(@$mainrad['tgl_order'], '-', 'full_date');
                                                                                                          } else {
                                                                                                            echo date('d-m-Y H:i:s');
                                                                                                          } ?>" required aria-invalid="false">
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Lokasi Asal <span class="text-danger">*</span></label>
        <div class="col-lg-8 col-md-3">
          <input type="text" class="form-control" id="lokasi_nm" value="<?= @$reg['lokasi_nm'] ?>" required="" readonly>
          <input type="hidden" name="src_lokasi_id" value="<?= $reg['lokasi_id'] ?>">
        </div>
      </div>
      <div class="form-group row mb-2">
        <label class="col-lg-4 col-md-3 col-form-label">Diagnosis</label>
        <div class="col-lg-8 col-md-3">
          <div class="card-disabled">
            <?php foreach ($diagnosis as $row) : ?>
              <?= $row['icdx'] ?> - <?= $row['penyakit_nm'] ?> <br>
            <?php endforeach; ?>
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Keterangan</label>
        <div class="col-lg-8 col-md-3">
          <textarea class="form-control" name="keterangan_order" id="keterangan_order" rows="6"><?= @$mainrad['keterangan_order'] ?></textarea>
        </div>
      </div>
    </div>
    <div class="col">
      <div class="table-responsive">
        <table id="radiologi_table" class="table table-hover table-bordered table-striped table-sm w-100">
          <thead>
            <tr>
              <th class="text-center" width="20">No.</th>
              <th class="text-center" width="50">Kode</th>
              <th class="text-center">Nama</th>
              <th class="text-center" width="20">Aksi</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
  <hr>
  <div class="col-2 offset-md-10 ">
    <div class="float-right">
      <button id="radiologi_cancel" type="button" class="btn btn-xs btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
      <button id="radiologi_action" type="submit" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
    </div>
  </div>
</form>
<script>
  var radiologiTable;
  $(document).ready(function() {
    $('.datetimepicker').daterangepicker({
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })

    //datatables
    radiologiTable = $('#radiologi_table').DataTable({
      'retrieve': true,
      "autoWidth": false,
      "processing": true,
      "serverSide": false,
      "order": [],
      "ajax": {
        "url": "<?php echo site_url() . '/' . $nav['nav_url'] . '/ajax_penunjang_radiologi/search_data/' . @$reg['reg_id'] . '/' . @$mainrad['pemeriksaan_id'] ?>",
        "type": "POST"
      },
      "columnDefs": [{
          "targets": 0,
          "className": 'text-center',
          "orderable": false
        },
        {
          "targets": 1,
          "className": 'text-left',
          "orderable": false
        },
        {
          "targets": 2,
          "className": 'text-left',
          "orderable": false
        },
        {
          "targets": 3,
          "className": 'text-center',
          "orderable": false
        }
      ],
    });
    radiologiTable.columns.adjust().draw();


    //FORM
    var radiologi_form = $("#radiologi_form").validate({
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $("#radiologi_action").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#radiologi_action").attr("disabled", "disabled");
        $("#radiologi_cancel").attr("disabled", "disabled");

        radiologiTable.$('input[type="checkbox"]').each(function() {
          // If checkbox doesn't exist in DOM
          if (!$.contains(document, this)) {
            // If checkbox is checked
            if (this.checked) {
              // Create a hidden element
              $(form).append(
                $('<input>')
                .attr('type', 'hidden')
                .attr('name', this.name)
                .val(this.value)
              );
            }
          }
        });

        $.ajax({
          type: 'post',
          url: '<?= site_url($nav['nav_url']) ?>/ajax_penunjang_radiologi/save/<?= @$reg['reg_id'] . '/' . @$mainrad['pemeriksaan_id'] ?>',
          data: $(form).serialize(),
          dataType: 'json',
          success: function(data) {

          }
        })
        $("#myModal").modal('hide');
        $.toast({
          heading: 'Sukses',
          text: 'Data berhasil disimpan.',
          icon: 'success',
          position: 'top-right'
        })
        radiologi_data('<?= $reg['reg_id'] ?>', '<?= $reg['pasien_id'] ?>', '<?= $reg['lokasi_id'] ?>');
        return false;
      }
    });


  })
</script>