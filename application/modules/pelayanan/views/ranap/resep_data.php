<?php if (count(@$main) == 0) : ?>
  <tr>
    <th class="text-center" width="20">No</th>
    <th class="text-center" width="40">Aksi</th>
    <th class="text-center">Nama Obat</th>
    <th class="text-center">Jenis</th>
    <th class="text-center">Dosis</th>
    <th class="text-center">Qty</th>
    <th class="text-center">Aturan Pakai</th>
    <th class="text-center">Jam Minum</th>
    <th class="text-center">Peringatan Khusus</th>
  </tr>
  <tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else : ?>
  <?php foreach ($main as $resep) : ?>
    <tr bgcolor="#f5f5f5">
      <td class="text-left" colspan="99">
        <b>NO.RESEP : <?= $resep['resep_id'] ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; TANGGAL : <?= to_date($resep['tgl_catat'], '', 'date') ?></b>
        <span class="float-right">
          <a href="javascript:void(0)" class="btn btn-danger btn-table btn-delete-resep" data-reg-id="<?= $resep['reg_id'] ?>" data-pasien-id="<?= $resep['pasien_id'] ?>" data-resep-id="<?= $resep['resep_id'] ?>" title="Hapus Resep">
            <i class="fas fa-trash-alt"></i> Hapus Resep
          </a>
        </span>
      </td>
    </tr>
    <tr>
      <th class="text-center" width="20">No</th>
      <th class="text-center" width="40">Aksi</th>
      <th class="text-center">Nama Obat/Alias</th>
      <th class="text-center">Rute</th>
      <th class="text-center">Qty</th>
      <th class="text-center">Aturan Pakai</th>
      <th class="text-center">Jadwal Pemberian</th>
      <th class="text-center">Aturan Tambahan</th>
    </tr>
    <?php $i = 1;
    foreach ($resep['rinc'] as $rinc) : ?>
      <tr>
        <td class="text-center"><?= $i++ ?></td>
        <td class="text-center" width="60">
          <?php if (to_date($resep['tgl_catat'], '', 'date') == date('d-m-Y')) : ?>
            <a href="javascript:void(0)" class="btn btn-primary btn-table btn-edit" data-resep-id="<?= $rinc['resep_id'] ?>" data-reg-id="<?= $rinc['reg_id'] ?>" data-lokasi-id="<?= $rinc['lokasi_id'] ?>" data-rincian-id="<?= $rinc['rincian_id'] ?>" title="Ubah Data"><i class="fas fa-pencil-alt"></i></a>
            <a href="javascript:void(0)" class="btn btn-danger btn-table btn-delete" data-resep-id="<?= $rinc['resep_id'] ?>" data-reg-id="<?= $rinc['reg_id'] ?>" data-lokasi-id="<?= $rinc['lokasi_id'] ?>" data-rincian-id="<?= $rinc['rincian_id'] ?>" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
          <?php endif; ?>
        </td>
        <td class="text-left">
          <?= $rinc['obat_nm'] ?>
          <?= ($rinc['obat_alias'] != '') ? ' (' . $rinc['obat_alias'] . ')' : '' ?>
        </td>
        <td><?= $rinc['rute_nm'] ?></td>
        <td><?= $rinc['qty'] ?></td>
        <td><?= $rinc['aturan_jml'] . ' ' . $rinc['satuan_nm'] ?> tiap <?= $rinc['aturan_waktu'] ?> <?= $rinc['waktu_nm'] ?></td>
        <td><?= $rinc['jadwal'] ?></td>
        <td><?= $rinc['aturan_tambahan'] ?></td>
      </tr>
    <?php endforeach; ?>
  <?php endforeach; ?>
<?php endif; ?>

<script type="text/javascript">
  $(document).ready(function() {
    // edit
    $('.btn-edit').bind('click', function(e) {
      e.preventDefault();
      var resep_id = $(this).attr("data-resep-id");
      var rincian_id = $(this).attr("data-rincian-id");
      var reg_id = $(this).attr("data-reg-id");
      var lokasi_id = $(this).attr("data-lokasi-id");
      var pasien_id = $("#pasien_id").val();
      //
      $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_resep/get_data' ?>', {
        resep_id: resep_id,
        rincian_id: rincian_id,
        reg_id: reg_id,
        lokasi_id: lokasi_id,
        pasien_id: pasien_id
      }, function(data) {
        $("#resep_id").val(data.main.resep_id).removeClass("is-valid").removeClass("is-invalid");
        $("#rincian_id").val(data.main.rincian_id).removeClass("is-valid").removeClass("is-invalid");
        $("#obat_nm").val(data.main.obat_nm).removeClass("is-valid").removeClass("is-invalid");
        $("#obat_alias").val(data.main.obat_alias).removeClass("is-valid").removeClass("is-invalid");
        $("#rute_cd").val(data.main.rute_cd).trigger('change').removeClass("is-valid").removeClass("is-invalid");
        $("#qty").val(data.main.qty).removeClass("is-valid").removeClass("is-invalid");
        $("#aturan_jml").val(data.main.aturan_jml).removeClass("is-valid").removeClass("is-invalid");
        $("#satuan_cd").val(data.main.satuan_cd).trigger('change').removeClass("is-valid").removeClass("is-invalid");
        $("#aturan_waktu").val(data.main.aturan_waktu).removeClass("is-valid").removeClass("is-invalid");
        $("#waktu_cd").val(data.main.waktu_cd).trigger('change').removeClass("is-valid").removeClass("is-invalid");
        $("#jadwal").val(data.main.jadwal).removeClass("is-valid").removeClass("is-invalid");
        $("#aturan_tambahan").val(data.main.aturan_tambahan).removeClass("is-valid").removeClass("is-invalid");
        $('#resep_action').html('<i class="fas fa-save"></i> Ubah');
      }, 'json');
    });
    // delete
    $('.btn-delete').on('click', function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Apakah Anda yakin?',
        text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#eb3b5a',
        cancelButtonColor: '#b2bec3',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        customClass: 'swal-wide'
      }).then((result) => {
        if (result.value) {
          var resep_id = $(this).attr("data-resep-id");
          var rincian_id = $(this).attr("data-rincian-id");
          var reg_id = $(this).attr("data-reg-id");
          var lokasi_id = $(this).attr("data-lokasi-id");
          var pasien_id = $("#pasien_id").val();
          //
          $('#resep_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
          $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_resep/delete_data' ?>', {
            resep_id: resep_id,
            rincian_id: rincian_id,
            reg_id: reg_id,
            lokasi_id: lokasi_id,
            pasien_id: pasien_id
          }, function(data) {
            console.log(data);
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil dihapus.',
              icon: 'success',
              position: 'top-right'
            })
            $('#resep_data').html(data.html);
          }, 'json');
        }
      })
    })

    $('.btn-delete-resep').on('click', function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Apakah Anda yakin?',
        text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#eb3b5a',
        cancelButtonColor: '#b2bec3',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        customClass: 'swal-wide'
      }).then((result) => {
        if (result.value) {
          var resep_id = $(this).attr("data-resep-id");
          var reg_id = $(this).attr("data-reg-id");
          var pasien_id = $("#pasien_id").val();
          $('#resep_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
          $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_resep/delete_resep' ?>', {
            resep_id: resep_id,
            reg_id: reg_id,
            pasien_id: pasien_id
          }, function(data) {
            console.log(data);
            $.toast({
              heading: 'Sukses',
              text: 'Resep berhasil dihapus.',
              icon: 'success',
              position: 'top-right'
            })
            $('#resep_data').html(data.html);
          }, 'json');
        }
      })
    })
  });
</script>