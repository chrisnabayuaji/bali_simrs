<div class="row">
  <div class="col">
    <div class="table-responsive">
      <table id="kamar_table" class="table table-hover table-bordered table-striped table-sm w-100">
        <thead>
          <tr>
            <th class="text-center" width="20">No.</th>
            <th class="text-center">Bangsal</th>
            <th class="text-center">Kamar</th>
            <th class="text-center">Tgl.Masuk</th>
            <th class="text-center">Tgl.Keluar</th>
            <th class="text-center">Jml.Hari</th>
            <th class="text-center">Tagihan</th>
            <th class="text-center">Keterangan Kamar</th>
          </tr>
        </thead>
        <?php if(@$main == null):?>
          <tbody>
            <tr>
              <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
            </tr>
          </tbody>
        <?php else:?>
          <tbody>
            <?php $i=1;foreach($main as $row):?>
              <tr>
                <td class="text-center" width="20"><?=($i++)?></td>
                <td><?=$row['lokasi_nm']?></td>
                <td><?=$row['kamar_nm']?></td>
                <td><?=to_date($row['tgl_masuk'], '', 'full_date')?></td>
                <?php if ($row['jml_hari'] !=''): ?>
                  <td><?=to_date($row['tgl_keluar'], '', 'full_date')?></td>
                  <td class="text-center"><?=$row['jml_hari']?></td>
                  <td class="text-center"><?=num_id($row['jml_tagihan'])?></td>
                <?php else: ?>
                  <td><?=date('d-m-Y H:i:s')?></td>
                  <td class="text-center"><?=selisih_hari(date('d-m-Y H:i:s'), to_date($row['tgl_masuk'],'','date'))?></td>
                  <td class="text-center"><?=num_id($row['nom_tarif'] * selisih_hari(date('d-m-Y H:i:s'), to_date($row['tgl_masuk'],'','date')))?></td>
                <?php endif; ?>
                <td><?=$row['keterangan_kamar']?></td>
              </tr>
            <?php endforeach;?>
          </tbody>
        <?php endif;?>
      </table>
    </div>
  </div>
</div>
<div class="border-top border-2 mt-2 pt-2 pb-0">
  <div class="row">
    <div class="offset-lg-11 offset-md-11">
      <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Tutup</button>
    </div>
  </div>
</div>