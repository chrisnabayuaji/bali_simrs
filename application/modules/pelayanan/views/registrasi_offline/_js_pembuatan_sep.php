<script type="text/javascript">
  var sep_form;
  $(document).ready(function() {
    var sep_form = $("#sep_form").validate({
      rules: {},
      messages: {},
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-n2 mb-2');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-clear").attr("disabled", "disabled");
        $('#loadingModal').modal({
          backdrop: 'static',
          keyboard: false
        });
        $.ajax({
          type: 'post',
          url: '<?= @$form_action ?>',
          data: $(form).serialize(),
          dataType: 'json',
          success: function(data) {
            console.log(data);
            setTimeout(function() {
              $('#loadingModal').modal('hide');
            }, 500);
            $(".btn-submit").html('<i class="fas fa-paper-plane"></i> Buat SEP');
            $(".btn-submit").attr("disabled", false);
            $(".btn-clear").attr("disabled", false);
            if (data.metaData['code'] == '200') {
              var buttons = $('<div class="mb-3">')
                .append('<div style="font-size: 12px !important; color: #686868; font-weight: initial; margin-bottom: 20px;">Nomor SEP : ' + data.response['sep']['noSep'] + ' <br> Nomor SEP sudah disimpan di SIMRS</div>')
                .append('<a href="javascript:void(0)" class="btn btn-sm btn-primary mr-1 ml-1 mb-1 mt-1 text-white modal-cetak-sep" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/cetak_modal/cetak_sep_pdf/' . @$main['reg_id'] . '/' ?>' + data.response['sep']['noSep'] + '" modal-title="Cetak SEP ' + data.response['sep']['noSep'] + '" modal-size="lg" modal-content-top="-75px"><i class="fas fa-print"></i> Cetak SEP</a>')
                .append(createButton('<i class="fas fa-chevron-circle-left"></i> Kembali Ke List Data Rajal', '<?= site_url($nav['nav_url']) ?>'));

              Swal.fire({
                title: "Pembuatan SEP Berhasil",
                html: buttons,
                type: "success",
                showConfirmButton: false,
                showCancelButton: false,
                customClass: "swal-wide",
              });

              $(".modal-cetak-sep").click(function(e) {
                e.preventDefault();
                swal.close();
                var modal_title = $(this).attr("modal-title");
                var modal_size = $(this).attr("modal-size");
                var modal_custom_size = $(this).attr("modal-custom-size");
                var modal_header = $(this).attr("modal-header");
                var modal_content_top = $(this).attr("modal-content-top");

                $("#modal-size")
                  .removeClass("modal-lg")
                  .removeClass("modal-md")
                  .removeClass("modal-sm");

                $("#modal-title").html(modal_title);
                $("#modal-size").addClass("modal-" + modal_size);
                if (modal_custom_size) {
                  $("#modal-size").attr(
                    "style",
                    "max-width: " + modal_custom_size + "px !important"
                  );
                }
                if (modal_content_top) {
                  $(".modal-content-top").attr(
                    "style",
                    "margin-top: " + modal_content_top + " !important"
                  );
                }
                if (modal_header == "hidden") {
                  $("#modal-header").addClass("d-none");
                } else {
                  $("#modal-header").removeClass("d-none");
                }
                $("#myModal").modal("show");
                $("#modal-body").html(
                  '<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i><br>Loading</div>'
                );
                $.post(
                  $(this).data("href"),
                  function(data) {
                    $("#modal-body").html(data.html);
                  },
                  "json"
                );
              });
            } else {
              Swal.fire({
                title: "Pembuatan SEP Gagal !",
                text: data.metaData['message'],
                type: "warning",
                cancelButtonColor: "#3498db",
                cancelButtonText: "OK",
                customClass: "swal-wide",
              });
            }
          },
          error: function(data) {
            $(".btn-submit").html('<i class="fas fa-paper-plane"></i> Buat SEP');
            $(".btn-submit").removeClass('disabled');
            $(".btn-clear").removeClass('disabled');
          }
        })
        return false;
      }
    });

    $("#list-data").click(function() {
      window.location.replace("<?= site_url($nav['nav_url']) ?>");
    });

    // $('#tglSep').val('');
    $('#tglKejadian').val('');
    // $('#tglRujukan').val('');


    // autocomplete
    $('#penyakit_id').select2({
      minimumInputLength: 2,
      ajax: {
        url: "<?= site_url($nav['nav_url']) ?>/ajax_diagnosis/autocomplete",
        dataType: "json",

        data: function(params) {
          return {
            penyakit_nm: params.term
          };
        },
        processResults: function(data) {
          return {
            results: data
          }
        }
      }
    });

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    $('#lakaLantas').bind('change', function(e) {
      e.preventDefault();
      var i = $(this).val();
      if (i == 1) {
        $('#jaminan-lakalantas-ya').removeClass('d-none');
      } else {
        $('#jaminan-lakalantas-ya').addClass('d-none');
      }
    });

    $('#suplesi').bind('change', function(e) {
      e.preventDefault();
      var i = $(this).val();
      if (i == 1) {
        $('#label-no-sep-suplesi').removeClass('d-none');
        $('#form-no-sep-suplesi').removeClass('d-none');
        $('#suplesi-ya').removeClass('d-none');
      } else {
        $('#label-no-sep-suplesi').addClass('d-none');
        $('#form-no-sep-suplesi').addClass('d-none');
        $('#suplesi-ya').addClass('d-none');
      }
    });

    $('#wilayah_prop').bind('change', function(e) {
      e.preventDefault();
      var i = $(this).val();
      _get_wilayah(i);
      $(this).on('select2:select', function(e) {
        form.element(this);
      });
    })
  });

  function _get_wilayah(i, prop = '', kab = '', kec = '', kel = '') {
    var ext_var = 'wilayah_prop=' + prop;
    ext_var += '&wilayah_kab=' + kab;
    ext_var += '&wilayah_kec=' + kec;
    ext_var += '&wilayah_kel=' + kel;
    $.get('<?= site_url($nav['nav_url'] . '/ajax_wilayah/get_wilayah_id_name') ?>?wilayah_parent=' + i + '&' + ext_var, null, function(data) {
      $('#box_wilayah_kab').html(data.html);
    }, 'json');
  }

  function diagnosis_fill(id) {
    $.ajax({
      type: 'post',
      url: '<?= site_url($nav['nav_url'] . '/ajax_diagnosis/diagnosis_fill') ?>',
      dataType: 'json',
      data: 'penyakit_id=' + id,
      success: function(data) {
        // autocomplete
        var data2 = {
          id: data.penyakit_id + '#' + data.icdx,
          text: data.icdx + ' - ' + data.penyakit_nm
        };
        var newOption = new Option(data2.text, data2.id, false, false);
        $('#penyakit_id').append(newOption).trigger('change');
        $('#penyakit_id').val(data.penyakit_id + '#' + data.icdx);
        $('#penyakit_id').trigger('change');
        $('.select2-container').css('width', '100%');
        $("#myModal").modal('hide');
      }
    })
  }

  function createButton(text, href) {
    return $('<a href="' + href + '" class="btn btn-sm btn-primary mr-1 ml-1 mb-1 mt-1 text-white">' + text + '</a>');
  }
</script>