<style>
  #frame_pdf {
    width: 100%;
    min-height: 68vh;
  }
</style>
<script type="text/javascript">
  $(document).ready(function() {
    GetRujukan('<?= @$sep_no ?>');
  });

  function GetRujukan(sep_no) {
    $('#box-data').html('<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i><br>Memuat...</div>');
    $.ajax({
      url: '<?= site_url($nav['nav_url'] . '/ajax/cari_sep') ?>',
      type: 'POST',
      dataType: 'json',
      data: {
        'sep_no': sep_no
      },
      error: function() {
        $('#box-data').html('<div class="text-center mb-4 mt-4"><i class="fas fa-exclamation-circle"></i> Gagal memuat data, silahkan klik tombol <b>MUAT ULANG</b> untuk memuat ulang data <button type="button" class="btn btn-sm btn-secondary" id="muat-ulang"><i class="fas fa-sync-alt"></i> Muat Ulang</button><div>');
        $("#muat-ulang").click(function() {
          GetRujukan('<?= @$sep_no ?>');
        });
      },
      success: function(data) {
        if (data.code == 200) {
          $('#box-data').html('<iframe id="frame_pdf" src="<?= $url ?>" frameborder="0"></iframe><a href="<?= $url ?>" target="_blank" class="float-right btn btn-xs btn-primary mt-1">Buka Di Tab Baru <i class="fas fa-search"></i></a>');
        } else {
          $('#box-data').html('<div class="text-center mb-3"><h5>' + data.message + '</h5></div>');
        }
      },
      timeout: 180000 // 3 menit
    });
  }
</script>
<div class="content-body">
  <div class="row">
    <div class="col-md-12">
      <div id="box-data"></div>
      <button type="button" class="float-right btn btn-xs btn-secondary btn-cancel mt-1 mr-2" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
    </div>
  </div>
</div>