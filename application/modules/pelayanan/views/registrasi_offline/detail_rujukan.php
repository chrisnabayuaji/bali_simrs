<style>
  .jsgrid .jsgrid-table td,
  .jsgrid .jsgrid-table th,
  .table td,
  .table th {
    border-top: 1px solid #ddd;
  }
</style>
<div class="row">
  <div class="col-md-6">
    <div class="table-responsive">
      <table class="table table-striped" style="border-bottom: 1px solid #ddd;">
        <tbody>
          <?php if (@$code == 200) : ?>
            <tr>
              <td class="text-left" width="150"><b>No.Kartu BPJS</b></td>
              <td class="text-center" width="3">:</td>
              <td><?= @$main['peserta']['noKartu'] ?> ( MR. <?= @$main['peserta']['mr']['noMR'] ?> )</td>
            </tr>
            <tr>
              <td class="text-left" width="150"><b>NIK</b></td>
              <td class="text-center" width="3">:</td>
              <td><?= @$main['peserta']['nik'] ?></td>
            </tr>
            <tr>
              <td class="text-left" width="150"><b>Nama Peserta</b></td>
              <td class="text-center" width="3">:</td>
              <td><?= @$main['peserta']['nama'] ?></td>
            </tr>
            <tr>
              <td class="text-left" width="150"><b>Jenis Kelamin</b></td>
              <td class="text-center" width="3">:</td>
              <td>
                <?php if (@$main['peserta']['sex'] == 'L') : ?>
                  Laki-Laki
                <?php elseif (@$main['peserta']['sex'] == 'P') : ?>
                  Perempuan
                <?php else : ?>
                  -
                <?php endif; ?>
              </td>
            </tr>
            <tr>
              <td class="text-left" width="150"><b>Tgl. Lahir</b></td>
              <td class="text-center" width="3">:</td>
              <td><?= to_date(@$main['peserta']['tglLahir']) ?></td>
            </tr>
            <tr>
              <td class="text-left" width="150"><b>No. Telepon</b></td>
              <td class="text-center" width="3">:</td>
              <td><?= @$main['peserta']['mr']['noTelepon'] ?></td>
            </tr>
            <tr>
              <td class="text-left" width="150"><b>Peserta</b></td>
              <td class="text-center" width="3">:</td>
              <td>-</td>
            </tr>
          <?php else : ?>
            <tr>
              <td colspan="3" class="text-center"><?= @$message ?></td>
            </tr>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>
  <div class="col-md-6">
    <div class="table-responsive">
      <table class="table table-striped" style="border-bottom: 1px solid #ddd;">
        <tbody>
          <?php if (@$code == 200) : ?>
            <tr>
              <td class="text-left" width="150"><b>No. Rujukan</b></td>
              <td class="text-center" width="3">:</td>
              <td><?= @$main['noKunjungan'] ?></td>
            </tr>
            <tr>
              <td class="text-left" width="150"><b>Tgl. Rujukan</b></td>
              <td class="text-center" width="3">:</td>
              <td><?= to_date(@$main['tglKunjungan']) ?></td>
            </tr>
            <tr>
              <td class="text-left" width="150"><b>Diagnosa Awal</b></td>
              <td class="text-center" width="3">:</td>
              <td><?= @$main['diagnosa']['kode'] ?> - <?= @$main['diagnosa']['nama'] ?></td>
            </tr>
            <tr>
              <td class="text-left" width="150"><b>Keluhan</b></td>
              <td class="text-center" width="3">:</td>
              <td><?= @$main['keluhan'] ?></td>
            </tr>
            <tr>
              <td class="text-left" width="150"><b>Poli Tujuan</b></td>
              <td class="text-center" width="3">:</td>
              <td><?= @$main['poliRujukan']['nama'] ?></td>
            </tr>
          <?php else : ?>
            <tr>
              <td colspan="3" class="text-center"><?= @$message ?></td>
            </tr>
          <?php endif; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
<div class="border-top border-2 mt-2 pt-2 pb-0">
  <div class="row">
    <div class="offset-lg-11 offset-md-11">
      <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Tutup</button>
    </div>
  </div>
</div>