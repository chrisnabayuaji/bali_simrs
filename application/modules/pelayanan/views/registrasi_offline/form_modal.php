<form role="form" id="form-history" method="post" enctype="multipart/form-data" action="" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-2 col-md-2 col-form-label">No.RM / Nama Pasien <span class="text-danger">*</span></label>
        <div class="col-lg-3 col-md-3">
          <input type="text" class="form-control" name="history_term" id="history_term" value="" required="">
          <small class="text-danger">Silahkan isikan No.RM / Nama Pasien terlebih dahulu</small>
        </div>
        <div class="col-lg-3 col-md-3">
          <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-sync-alt"></i> Proses</button>
          <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="table-responsive">
    <table class="table table-hover table-striped table-bordered table-fixed">
      <thead>
        <tr>
          <th class="text-center" width="40">No</th>
          <th class="text-center" width="90">No Reg</th>
          <th class="text-center" width="125">Tanggal</th>
          <th class="text-center" width="90">No RM</th>
          <th class="text-center" width="250">Nama Pasien</th>
          <th class="text-center" width="250">Alamat</th>
          <th class="text-center" width="200">Lokasi</th>
          <th class="text-center" width="200">Dokter PJ</th>
        </tr>
      </thead>
      <tbody id="row_history">
        <tr>
          <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
        </tr>
      </tbody>
    </table>
  </div>
</form>
<script>
  $(document).ready(function() {
    var form_history = $("#form-history").validate({
      rules: {},
      messages: {},
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-n2 mb-2');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");

        $("#row_history").html('');
        var loading = '<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i> Mencari data...</td></tr>';
        $("#row_history").html(loading);

        $.ajax({
          type: 'post',
          url: '<?= site_url() . '/' . $nav['nav_url'] . '/ajax/search-history' ?>',
          data: 'term=' + $("#history_term").val(),
          dataType: 'json',
          success: function(data) {
            console.log(data);
            if (data == null || data.length == 0) {
              var html = '<tr><td class="text-center" colspan="99"><i class="fas fa-danger"></i> Data tidak ditemukan!</td></tr>';
              $("#row_history").html(html);
            } else {
              var html = '';
              $.each(data, function(index, value) {
                html += '<tr>' +
                  '<td class="text-center" width="40">' + (index + 1) + '</td>' +
                  '<td class="text-center" width="90">' + value.reg_id + '</td>' +
                  '<td class="text-center" width="125">' + value.tgl_registrasi + '</td>' +
                  '<td class="text-center" width="90">' + value.pasien_id + '</td>' +
                  '<td class="text-left" width="250">' + value.pasien_nm + ', ' + value.sebutan_cd + '</td>' +
                  '<td class="text-left" width="250">' + value.alamat + '</td>' +
                  '<td class="text-center" width="200">' + value.lokasi_nm + '</td>' +
                  '<td class="text-left" width="200">' + value.dokter_nm + '</td>' +
                  '</tr>';
              });
              $("#row_history").html(html);
            }

            $(".btn-submit").html('<i class="fas fa-sync-alt"></i> Proses');
            $(".btn-submit").attr("disabled", false);
            $(".btn-cancel").attr("disabled", false);
          }
        });
        return false;
      }
    });
  })
</script>