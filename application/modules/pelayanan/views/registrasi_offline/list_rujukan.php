<?php if (@$main != null) : ?>
  <?php if (@$code == 200) : ?>
    <?php $no = 1;
    foreach ($main as $row) : ?>
      <tr>
        <td class="text-center" width="36"><?= $no++ ?></td>
        <td class="text-center" width="80">
          <div class="btn-group">
            <button type="button" class="btn btn-primary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Aksi
            </button>
            <div class="dropdown-menu">
              <a class="dropdown-item modal-href" href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax/detail_rujukan/' . $row['noKunjungan'] ?>" modal-title="Detail Rujukan : <?= $row['noKunjungan'] ?>" modal-size="lg"><i class="fas fa-list"></i> Detail Data</a>
              <a class="dropdown-item modal-href" href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/cetak_modal/cetak_pdf/' . $row['peserta']['noKartu'] . '/' . $row['noKunjungan'] ?>" modal-title="Cetak SEP" modal-size="lg" modal-content-top="-75px" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cetak SEP"><i class="fas fa-print"></i> Cetak SEP</a>
            </div>
          </div>
        </td>
        <td class="text-center" width="160"><?= $row['noKunjungan'] ?></td>
        <td class="text-center" width="100"><?= to_date($row['tglKunjungan'], '', 'date') ?></td>
        <td class="text-center" width="120"><?= $row['peserta']['noKartu'] ?></td>
        <td class="text-left"><?= $row['peserta']['nama'] ?></td>
        <td class="text-left" width="250"><?= $row['diagnosa']['kode'] ?> - <?= @$row['diagnosa']['nama'] ?></td>
        <td class="text-left" width="220"><?= $row['poliRujukan']['nama'] ?></td>
      </tr>
    <?php endforeach; ?>
  <?php else : ?>
    <tr>
      <td colspan="99" class="text-center"><?= @$message ?></td>
    </tr>
  <?php endif; ?>
<?php else : ?>
  <tr>
    <td colspan="99" class="text-center">Data tidak ditemukan</td>
  </tr>
<?php endif; ?>

<script>
  $(document).ready(function() {
    $('.modal-href').click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");
      var modal_header = $(this).attr("modal-header");
      var modal_content_top = $(this).attr("modal-content-top");

      $("#modal-size").removeClass('modal-lg').removeClass('modal-md').removeClass('modal-sm');

      $("#modal-title").html(modal_title);
      $("#modal-size").addClass('modal-' + modal_size);
      if (modal_custom_size) {
        $("#modal-size").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
      }
      if (modal_content_top) {
        $(".modal-content-top").attr('style', 'margin-top: ' + modal_content_top + ' !important');
      }
      if (modal_header == 'hidden') {
        $("#modal-header").addClass('d-none');
      } else {
        $("#modal-header").removeClass('d-none');
      }
      $("#myModal").modal('show');
      $("#modal-body").html('<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i><br>Loading</div>');
      $.post($(this).data('href'), function(data) {
        $("#modal-body").html(data.html);
      }, 'json');
    });
  })
</script>