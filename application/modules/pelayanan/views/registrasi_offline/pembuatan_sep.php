<?php $this->load->view('pelayanan/registrasi_offline/_js_pembuatan_sep'); ?>
<div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
<form id="sep_form" action="" method="post" autocomplete="off">
  <input type="hidden" name="noKartu" value="<?= @$main['no_kartu'] ?>">
  <input type="hidden" name="ppkPelayanan" value="<?= @$identitas_rs['kode_faskes_bpjs'] ?>">
  <input type="hidden" name="noMR" value="<?= @$main['pasien_id'] ?>">
  <input type="hidden" name="noTelp" value="<?= anti_injection(@$main['no_telp']) ?>">
  <input type="hidden" name="user" value="<?= @$this->session->userdata('sess_user_realname') ?>">
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline col-title">
          <div class="col-back">
            <a href="<?= site_url($nav['nav_url']) ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
          </div>
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            Pembuatan SEP
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-user-injured"></i> Registrasi</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item active"><span>Pembuatan SEP</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>

    <div class="row full-page mt-4">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <div class="row mt-1">
              <div class="col-md-12">
                <h4 class="card-title border-bottom border-2 pb-2 mb-3">Identitas Pasien</h4>
                <div class="row mb-3 mt-n1">
                  <div class="col-md-6">
                    <table class="table" style="width: 100% !important; border-bottom: 1px solid #f2f2f2;">
                      <tbody>
                        <tr>
                          <td width="150" style="font-weight: 500 !important;">No.Reg</td>
                          <td width="20">:</td>
                          <td><?= @$main['reg_id'] ?></td>
                        </tr>
                        <tr>
                          <td width="150" style="font-weight: 500 !important;">NIK</td>
                          <td width="20">:</td>
                          <td><?= @$main['nik'] ?></td>
                        </tr>
                        <tr>
                          <td width="150" style="font-weight: 500 !important;">No.RM / Nama Pasien</td>
                          <td width="20">:</td>
                          <td><?= @$main['pasien_id'] ?> / <?= @$main['pasien_nm'] ?>, <?= @$main['sebutan_cd'] ?></td>
                        </tr>
                        <tr>
                          <td width="150">Alamat Pasien</td>
                          <td width="20">:</td>
                          <td>
                            <?= (@$main['alamat'] != '') ? @$main['alamat'] . ', ' : '' ?>
                            <?= (@$main['kelurahan'] != '') ? ucwords(strtolower(@$main['kelurahan'])) . ', ' : '' ?>
                            <?= (@$main['kecamatan'] != '') ? ucwords(strtolower(@$main['kecamatan'])) . ', ' : '' ?>
                            <?= (@$main['kabupaten'] != '') ? ucwords(strtolower(@$main['kabupaten'])) . ', ' : '' ?>
                            <?= (@$main['provinsi'] != '') ? ucwords(strtolower(@$main['provinsi'])) : '' ?>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-6">
                    <table class="table" style="width: 100% !important; border-bottom: 1px solid #f2f2f2;">
                      <tbody>
                        <tr>
                          <td width="150">Dokter PJ</td>
                          <td width="20">:</td>
                          <td><?= @$main['pegawai_nm'] ?></td>
                        </tr>
                        <tr>
                          <td width="150">Umur / JK</td>
                          <td width="20">:</td>
                          <td><?= @$main['umur_thn'] ?> Th <?= @$main['umur_bln'] ?> Bl <?= @$main['umur_hr'] ?> Hr / <?= get_parameter_value('sex_cd', @$main['sex_cd']) ?></td>
                        </tr>
                        <tr>
                          <td width="150">Jenis / Asal Pasien</td>
                          <td width="20">:</td>
                          <td><?= @$main['jenispasien_nm'] ?> <?= (@$main['jenispasien_id'] != '01') ? '(' . @$main['no_kartu'] . ')' : '' ?> / <?= get_parameter_value('asalpasien_cd', @$main['asalpasien_cd']) ?></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <h6 class="text-primary">SEP</h6>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label mt-n3">Tgl.Penerbitan SEP <span class="text-danger">*<span></label>
                  <div class="col-lg-3 col-md-9">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datepicker" name="tglSep" id="tglSep" placeholder="dd-mm-yyyy" required="">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label mt-n3">Jenis Pelayanan <span class="text-danger">*</span></label>
                  <div class="col-lg-4 col-md-9">
                    <select class="form-control chosen-select" name="jnsPelayanan" required="">
                      <option value="">- Pilih -</option>
                      <option value="1">Rawat Inap</option>
                      <option value="2" selected>Rawat Jalan</option>
                    </select>
                  </div>
                  <label class="col-lg-2 col-md-3 col-form-label">Kelas Rawat <span class="text-danger">*</span></label>
                  <div class="col-lg-4 col-md-9">
                    <select class="form-control chosen-select" name="klsRawat" required="">
                      <option value="">- Pilih -</option>
                      <option value="1" <?= (@$main['kelas_id'] == 04) ? 'selected' : ''; ?>>Kelas 1</option>
                      <option value="2" <?= (@$main['kelas_id'] == 05) ? 'selected' : ''; ?>>Kelas 2</option>
                      <option value="3" <?= (@$main['kelas_id'] == 06) ? 'selected' : ''; ?>>Kelas 3</option>
                    </select>
                  </div>
                </div>
                <h6 class="text-primary">Rujukan</h6>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Asal Rujukan <span class="text-danger">*</span></label>
                  <div class="col-lg-4 col-md-9">
                    <select class="form-control chosen-select" name="asalRujukan" required="">
                      <option value="">- Pilih -</option>
                      <option value="1">Faskes 1</option>
                      <option value="2">Faskes 2(RS)</option>
                    </select>
                  </div>
                  <label class="col-lg-2 col-md-3 col-form-label">Tgl. Rujukan <span class="text-danger">*</span></label>
                  <div class="col-lg-3 col-md-9">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datepicker" name="tglRujukan" id="tglRujukan" placeholder="dd-mm-yyyy" required="">
                    </div>
                  </div>
                </div>
                <div class="form-group row mt-2">
                  <label class="col-lg-2 col-md-3 col-form-label mt-n3">Nomor Rujukan <span class="text-danger">*</span></label>
                  <div class="col-lg-4 col-md-9">
                    <input type="text" class="form-control" name="noRujukan" required="">
                  </div>
                  <label class="col-lg-2 col-md-3 col-form-label mt-n3">Kode Faskes Rujukan <span class="text-danger">*</span></label>
                  <div class="col-lg-3 col-md-9">
                    <input type="text" class="form-control" name="ppkRujukan" value="1120R001" required="">
                  </div>
                </div>
                <h6 class="text-primary">Poli</h6>
                <div class="form-group row pb-2">
                  <label class="col-lg-2 col-md-3 col-form-label">Poli Tujuan</label>
                  <div class="col-lg-4 col-md-9">
                    <select class="form-control chosen-select" name="tujuan" required="">
                      <option value="">- Pilih -</option>
                      <?php foreach ($lokasi as $r) : ?>
                        <option value="<?= $r['kd_poli_bpjs'] ?>" <?= (@$main['lokasi_id'] == $r['lokasi_id']) ? 'selected' : ''; ?>><?= $r['lokasi_nm'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <label class="col-lg-2 col-md-3 col-form-label">Poli Eksekutif</label>
                  <div class="col-lg-3 col-md-9">
                    <select class="form-control chosen-select" name="eksekutif">
                      <option value="0">Tidak</option>
                      <option value="1">Ya</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <h6 class="text-primary">Lain-lain</h6>
                <div class="form-group row pt-2">
                  <label class="col-lg-2 col-md-3 col-form-label mt-n3">Diagnosa Awal <span class="text-danger">*</span></label>
                  <div class="col-lg-5 col-md-9">
                    <select class="form-control select2" name="diagAwal" id="penyakit_id">
                      <option value="">- Pilih -</option>
                    </select>
                  </div>
                  <div class="col-lg-3 col-md-3">
                    <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_diagnosis/search_diagnosis' ?>" modal-title="List Data Penyakit" modal-size="lg" class="btn btn-xs btn-default modal-href"><i class="fas fa-search"></i> Cari Diagnosis</a>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label mt-n3">Catatan Peserta</label>
                  <div class="col-lg-5 col-md-9">
                    <textarea class="form-control" name="catatan" rows="3"></textarea>
                  </div>
                </div>
                <div class="form-group row mt-2">
                  <label class="col-lg-2 col-md-3 col-form-label">COB</label>
                  <div class="col-lg-4 col-md-9">
                    <select class="form-control chosen-select" name="cob">
                      <option value="0">Tidak</option>
                      <option value="1">Ya</option>
                    </select>
                  </div>
                  <label class="col-lg-2 col-md-3 col-form-label">Katarak</label>
                  <div class="col-lg-3 col-md-9">
                    <select class="form-control chosen-select" name="katarak">
                      <option value="0">Tidak</option>
                      <option value="1">Ya</option>
                    </select>
                  </div>
                </div>
                <h6 class="text-primary">SKDP</h6>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label mt-n3">Nomor Surat Kontrol</label>
                  <div class="col-lg-4 col-md-9">
                    <input type="text" class="form-control" name="noSurat">
                  </div>
                  <label class="col-lg-2 col-md-3 col-form-label mt-n3">Kode Dokter DPJP</label>
                  <div class="col-lg-3 col-md-9">
                    <input type="text" class="form-control" name="kodeDPJP">
                  </div>
                </div>
                <h6 class="text-primary">Jaminan</h6>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label mt-n3">Jaminan Lakalantas</label>
                  <div class="col-lg-4 col-md-9">
                    <select class="form-control chosen-select" name="lakaLantas" id="lakaLantas">
                      <option value="0">Tidak</option>
                      <option value="1">Ya</option>
                    </select>
                  </div>
                </div>
                <div id="jaminan-lakalantas-ya" class="d-none">
                  <div class="form-group row">
                    <label class="col-lg-2 col-md-3 col-form-label mt-n3">Penjamin Lakalantas <span class="text-danger">*</span></label>
                    <div class="col-lg-4 col-md-9">
                      <select class="form-control chosen-select" name="penjamin" required="">
                        <option value="">- Pilih -</option>
                        <option value="1">Jasa Raharja PT</option>
                        <option value="2">BPJS Ketenagakerjaan</option>
                        <option value="3">TASPEN PT</option>
                        <option value="4">ASABRI PT</option>
                      </select>
                    </div>
                    <label class="col-lg-2 col-md-3 col-form-label">Tgl. Kejadian <span class="text-danger">*</span></label>
                    <div class="col-lg-3 col-md-9">
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                        </div>
                        <input type="text" class="form-control datepicker" name="tglKejadian" id="tglKejadian" placeholder="dd-mm-yyyy" required="">
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-lg-2 col-md-3 col-form-label mt-n3">Keterangan Kejadian Kecelakaan <span class="text-danger">*</span></label>
                    <div class="col-lg-6 col-md-9">
                      <textarea class="form-control pt-2" name="keterangan" rows="3"></textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-lg-2 col-md-3 col-form-label">Suplesi</label>
                    <div class="col-lg-4 col-md-9">
                      <select class="form-control chosen-select" name="suplesi" id="suplesi">
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                      </select>
                    </div>
                    <label class="col-lg-2 col-md-3 col-form-label mt-n3 d-none" id="label-no-sep-suplesi">No. SEP Suplesi <span class="text-danger">*</span></label>
                    <div class="col-lg-4 col-md-9 d-none" id="form-no-sep-suplesi">
                      <input type="text" class="form-control" name="noSepSuplesi" required="">
                    </div>
                  </div>
                  <div id="suplesi-ya" class="d-none">
                    <h6 class="text-default mt-n2">Lokasi Kecelakaan Lalu Lintas</h6>
                    <div class="form-group row">
                      <label class="col-lg-2 col-md-3 col-form-label">Propinsi <span class="text-danger">*</span></label>
                      <div class="col-lg-4 col-md-5">
                        <div id="box_wilayah_prop">
                          <select class="chosen-select custom-select w-100" name="wilayah_prop" id="wilayah_prop">
                            <option value="">- Propinsi -</option>
                            <?php foreach ($list_wilayah_prop as $wp) : ?>
                              <option value="<?= $wp['wilayah_id'] ?>#<?= $wp['wilayah_nm'] ?>#<?= $wp['wilayah_id_bpjs'] ?>"><?= $wp['wilayah_id'] ?> - <?= $wp['wilayah_nm'] ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>
                      <label class="col-lg-2 col-md-3 col-form-label">Kab/Kota <span class="text-danger">*</span></label>
                      <div class="col-lg-4 col-md-5">
                        <div id="box_wilayah_kab">
                          <select class="chosen-select custom-select w-100" name="wilayah_kab" id="wilayah_kab">
                            <option value="">- Kab/Kota -</option>
                          </select>
                        </div>
                      </div>
                      <label class="col-lg-2 col-md-3 col-form-label">Kecamatan</label>
                      <div class="col-lg-4 col-md-5">
                        <div id="box_wilayah_kec">
                          <select class="chosen-select custom-select w-100" name="wilayah_kec" id="wilayah_kec">
                            <option value="">- Kecamatan -</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="btn-form">
    <div class="btn-form-action">
      <div class="btn-form-action-bottom w-100 small-text clearfix">
        <div class="col-lg-10 col-md-8 col-4">
        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-clear">
          <button type="reset" onClick="window.location.reload();" class="btn btn-xs btn-secondary btn-clear"><i class="fas fa-sync-alt"></i> Reset</button>
        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-save">
          <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-paper-plane"></i> Buat SEP</button>
        </div>
      </div>
    </div>
  </div>
</form>