<div class="row">
  <div class="col-md-6">
    <table class="table" style="width: 100% !important; border-bottom: 1px solid #f2f2f2;">
      <tbody>
        <tr>
          <td width="150">No. Antrian</td>
          <td width="20">:</td>
          <td><?=@$main['antrian_cd']?>.<?=@$main['antrian_no']?></td>
        </tr>
        <tr>
          <td width="150">No. Rekam Medis</td>
          <td width="20">:</td>
          <td><?=@$main['pasien_id']?></td>
        </tr>
        <tr>
          <td width="150">Nama Pasien</td>
          <td width="20">:</td>
          <td><?=@$main['pasien_nm']?></td>
        </tr>
        <tr>
          <td width="150">Jenis Kelamin</td>
          <td width="20">:</td>
          <td><?=get_parameter_value('sex_cd', @$main['sex_cd'])?></td>
        </tr>
        <tr>
          <td width="150">Tempat / Tgl. Lahir</td>
          <td width="20">:</td>
          <td><?=@$main['tmp_lahir']?> / <?=to_date(@$main['tgl_lahir'])?></td>
        </tr>
        <tr>
          <td width="150">No. Telpon</td>
          <td width="20">:</td>
          <td><?=@$main['no_telp']?></td>
        </tr>
        <tr>
          <td width="150">Tgl. Periksa</td>
          <td width="20">:</td>
          <td><?=to_date(@$main['tgl_periksa'], '', 'full_date')?></td>
        </tr>
        <tr>
          <td width="150">Klinik Tujuan</td>
          <td width="20">:</td>
          <td><?=@$main['lokasi_nm']?></td>
        </tr>
        <tr>
          <td width="150">Jenis Pasien</td>
          <td width="20">:</td>
          <td><?=@$main['jenispasien_nm']?></td>
        </tr>
        <?php if (@$main['jenispasien_id'] !='01'): ?>
        <tr>
          <td width="150">No. Kartu</td>
          <td width="20">:</td>
          <td><?=@$main['no_kartu']?></td>
        </tr>
        <?php endif; ?>
      </tbody>
    </table>
  </div>
  <div class="col-md-6">
    <table class="table" style="width: 100% !important; border-bottom: 1px solid #f2f2f2;">
      <tbody>
        <tr>
          <td width="150">Provinsi</td>
          <td width="20">:</td>
          <td><?=@$main['provinsi']?></td>
        </tr>
        <tr>
          <td width="150">Kabupaten</td>
          <td width="20">:</td>
          <td><?=@$main['kabupaten']?></td>
        </tr>
        <tr>
          <td width="150">Kecamatan</td>
          <td width="20">:</td>
          <td><?=@$main['kecamatan']?></td>
        </tr>
        <tr>
          <td width="150">Kelurahan</td>
          <td width="20">:</td>
          <td><?=@$main['kelurahan']?></td>
        </tr>
        <tr>
          <td width="150">Alamat</td>
          <td width="20">:</td>
          <td><?=@$main['alamat']?></td>
        </tr>
        <tr>
          <td width="150">Memiliki Surat Rujukan</td>
          <td width="20">:</td>
          <td><?=(@$main['is_have_rujukan'] == '1') ? 'Iya' : 'Tidak'?></td>
        </tr>
      </tbody>
    </table>
    <?php if (@$main['is_have_rujukan'] == '1'): ?>
    <div class="row mt-2">
      <div class="col-md-6">
        <?php if (@$main['file_ktp'] !=''): ?>
        <a href="<?=base_url('assets/images/pendaftaran_online/file_ktp/'.@$main['file_ktp'])?>" target="_blank">
          <img class="w-100 rounded mx-auto d-block" src="<?=base_url('assets/images/pendaftaran_online/file_ktp/'.@$main['file_ktp'])?>">
        </a>
        <?php endif; ?>
      </div>
      <div class="col-md-6">
        <?php if (@$main['file_rujukan'] !=''): ?>
        <a href="<?=base_url('assets/images/pendaftaran_online/file_rujukan/'.@$main['file_rujukan'])?>" target="_blank">
          <img class="w-100 rounded mx-auto d-block" src="<?=base_url('assets/images/pendaftaran_online/file_rujukan/'.@$main['file_rujukan'])?>">
        </a>
        <?php endif; ?>
      </div>
      <div class="col-md-6">
        <?php if (@$main['file_ktp'] !=''): ?>
        <div class="text-center font-weight-bold mt-1" style="font-size: 12px !important;">
          Foto EKTP/Sep Terakhir
        </div>
        <?php endif; ?>
      </div>
      <div class="col-md-6">
        <?php if (@$main['file_rujukan'] !=''): ?>
        <div class="text-center font-weight-bold mt-1" style="font-size: 12px !important;">
          Foto Surat Rujukan
        </div>
        <?php endif; ?>
      </div>
    </div>
    <?php endif; ?>
  </div>
</div>
<div class="border-top border-2 mt-2 pt-2 pb-0">
  <div class="row">
    <div class="offset-lg-11 offset-md-11">
      <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Close</button>
    </div>
  </div>
</div>