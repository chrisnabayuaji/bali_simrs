<script type="text/javascript">
  $(document).ready(function() {
    var form = $("#form-data").validate({
      rules: {
        nik: {
          'number': true
        }
      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-n2 mb-2');
        } else if ($(element).hasClass('chosen-select-2')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-n2 mb-2');
        } else if ($(element).hasClass('form-input-group')) {
          error.insertAfter(element.next(".input-group-prepend"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    $("#update_box").hide();
    // statuspasien();
    jenispasien();
    sebutancd();
    hitung_umur();
    jnsrawat();

    // select2
    $(".chosen-select-2").select2();
    $('.select2-container').css('width', '100%');

    $('#lokasi_bangsal_id').bind('change', function(e) {
      e.preventDefault();
      var i = $(this).val();
      _get_kamar(i);
    })

    $('.chosen-select').on('select2:select', function(e) {
      form.element(this);
    });

    $('#wilayah_st').bind('change', function(e) {
      e.preventDefault();
      var i = $(this).val();
      _get_provinsi(i);
    })

    <?php if (@$id != '') : ?>
      _get_wilayah('<?= get_wilayah_id(@$main["wilayah_id"], 'provinsi') ?>', '<?= get_wilayah_id(@$main["wilayah_id"], 'provinsi') ?>', '<?= get_wilayah_id(@$main["wilayah_id"], 'kabupaten') ?>', '<?= get_wilayah_id(@$main["wilayah_id"], 'kecamatan') ?>', '<?= get_wilayah_id(@$main["wilayah_id"], 'kelurahan') ?>');
    <?php endif; ?>
    //
    $('#wilayah_prop').bind('change', function(e) {
      e.preventDefault();
      var i = $(this).val();
      _get_wilayah(i);
      $(this).on('select2:select', function(e) {
        form.element(this);
      });
    })
    //

    var typing_timer;
    $('#pasien_id').on('keyup', function() {
      clearTimeout(typing_timer);
      if ($("#statuspasien_cd").val() == 'L') {
        typing_timer = setTimeout(typing_done, 700);
      }
    });

    $('#pasien_id').on('keydown', function() {
      clearTimeout(typing_timer);
    });

    function typing_done() {
      var id = $('#pasien_id').val();
      $('#no-rm-used').addClass('d-none');
      no_rm_fill(id);
    }

    $('#cari_rm').on('click', function() {
      var id = $('#pasien_id').val();
      if (id == '') {
        $.toast({
          heading: 'Error',
          text: 'No Rekam Medis belum diisi!',
          icon: 'error',
          position: 'top-right'
        });
      } else {
        no_rm_fill(id);
      }
    });

    $('#lokasi_asal_id').bind('change', function(e) {
      e.preventDefault();
      var i = $(this).val();
      _get_kelas(i);
    })

    $("#ibu_box").hide();

    <?php if (@$main['reg_id']) : ?>
      _get_kelas('<?= @$main['lokasi_id'] ?>', '<?= @$main['kelas_id'] ?>');
      <?php if (@$main['is_bayi']) : ?>
        $("#ibu_box").show();
      <?php endif; ?>
      $("#statuspasien_cd").attr('disabled');
    <?php endif; ?>

    $('#is_bayi').change(function() {
      if ($(this).is(":checked")) {
        $("#ibu_box").show();
      } else {
        $("#ibu_box").hide();
      }
    });

    // $('#statuspasien_cd').bind('change', function(e) {
    //   e.preventDefault();
    //   var i = $(this).val();
    //   if (i == 'B') {
    //     $("#update_box").hide();
    //     $("#cari_rm").hide();
    //     $("#cari_daftar_pasien").hide();
    //     $("#box_cari_daftar_pasien").hide();
    //     $("#no_rm_otomatis").show();
    //     reset_form();
    //     $("#rm_otomatis").prop('checked', true);
    //     $('#pasien_id').prop('readonly', true);
    //     $("#pasien_id").val('otomatis');
    //   } else {
    //     $("#update_box").show();
    //     $("#cari_rm").show();
    //     $("#cari_daftar_pasien").show();
    //     $("#box_cari_daftar_pasien").show();
    //     $("#no_rm_otomatis").hide();
    //     $('#pasien_id').prop('readonly', false);
    //     $("#pasien_id").val('');
    //   }
    // });

    $('#rm_otomatis').change(function() {
      if ($(this).is(":checked")) {
        $("#pasien_id").val('otomatis');
        $('#pasien_id').prop('readonly', true);
      } else {
        $("#pasien_id").val('');
        $('#pasien_id').prop('readonly', false);
      }
    });

    $('#tgl_lahir').val('');
  })

  function _empty_kabupaten() {
    $.get('<?= site_url('master/wilayah/ajax/empty_kabupaten') ?>', null, function(data) {
      $('#box_wilayah_kab').html(data.html);
    }, 'json');
  }

  function _empty_kecamatan() {
    $.get('<?= site_url('master/wilayah/ajax/empty_kecamatan') ?>', null, function(data) {
      $('#box_wilayah_kec').html(data.html);
    }, 'json');
  }

  function _empty_kelurahan() {
    $.get('<?= site_url('master/wilayah/ajax/empty_kelurahan') ?>', null, function(data) {
      $('#box_wilayah_kel').html(data.html);
    }, 'json');
  }

  function _get_provinsi(i) {
    $.get('<?= site_url('master/wilayah/ajax/get_provinsi_by_st') ?>?wilayah_st=' + i, null, function(data) {
      $('#box_wilayah_prop').html(data.html);
    }, 'json');
  }

  function _get_wilayah(i, prop = '', kab = '', kec = '', kel = '') {
    var ext_var = 'wilayah_prop=' + prop;
    ext_var += '&wilayah_kab=' + kab;
    ext_var += '&wilayah_kec=' + kec;
    ext_var += '&wilayah_kel=' + kel;
    $.get('<?= site_url('master/wilayah/ajax/get_wilayah_id_name') ?>?wilayah_parent=' + i + '&' + ext_var, null, function(data) {
      $('#box_wilayah_kab').html(data.html);
    }, 'json');
  }

  function statuspasien() {
    var val = $("#statuspasien_cd").val();
    if (val == 'B') {
      $("#update_box").hide();
      $("#cari_rm").hide();
      $("#cari_daftar_pasien").hide();
      $("#box_cari_daftar_pasien").hide();
      $("#no_rm_otomatis").show();
      // reset_form();
      $("#rm_otomatis").prop('checked', true);
      $('#pasien_id').prop('readonly', true);
      $("#pasien_id").val('otomatis');
    } else {
      $("#update_box").show();
      $("#cari_rm").show();
      $("#cari_daftar_pasien").show();
      $("#box_cari_daftar_pasien").show();
      $("#no_rm_otomatis").hide();
      $('#pasien_id').prop('readonly', false);
      $("#pasien_id").val('');
    }
  }

  function jenispasien() {
    var val = $("#jenispasien_id").val();
    if (val != '01') {
      $('#label-no-kartu').removeClass('d-none');
      $('#col-no-kartu').removeClass('d-none');
      $("#penjamin_cd").val('AS').trigger('change');
      if (val == '02') {
        $("#penjamin_nm").val('BPJS');
        $('#sep_no_box').removeClass('d-none');
      } else {
        $('#sep_no_box').addClass('d-none');
        $("#penjamin_nm").val('ASURANSI LAIN');
      }
      $('#label-kelas').html('Kelas');
    } else {
      $('#label-no-kartu').addClass('d-none');
      $('#col-no-kartu').addClass('d-none');
      $('#sep_no_box').addClass('d-none');
      $("#penjamin_cd").val('PR').trigger('change');
      $("#penjamin_nm").val($("#pasien_nm").val());
      $('#label-kelas').html('Kelas <span class="text-danger">*<span>');
    }
  }

  function sebutancd() {
    var val = $("#sebutan_cd").val();
    if (val == 'BY') {
      $("#box-anak-ke").removeClass('d-none');
    } else if (val == 'BY NY') {
      $("#box-anak-ke").removeClass('d-none');
    } else {
      $("#box-anak-ke").addClass('d-none');
    }

    if (val == 'NY' || val == 'NN') {
      $("#sex_cd").val('P').trigger('change');
    } else {
      $("#sex_cd").val('L').trigger('change');
    }
  }

  function jnsrawat() {
    var val = $("#jns_rawat_cd").val();
    if (val == '02') {
      $('#rawat-pisah').removeClass('d-none');
    } else if (val == '01') {
      $('#rawat-pisah').removeClass('d-none');
    }
  }

  function hitung_umur() {
    var tanggal = to_date($("#tgl_lahir").val());
    var umur = get_age(tanggal);
    $("#umur_thn").val(umur.years);
    $("#umur_bln").val(umur.months);
    $("#umur_hr").val(umur.days);
  }

  function zero_fill(str, max) {
    str = str.toString();
    return str.length < max ? zero_fill("0" + str, max) : str;
  }

  function ucfirst(str) {
    str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
      return letter.toUpperCase();
    });
    return str;
  }

  function str_left(str, pad) {
    var pad = pad.toString() + str.toString();
    var ans = pad.substr(0, pad.length - str.length) + str;
    return ans;
  }

  function _get_kelas(i, j) {
    $.post('<?= site_url($nav['nav_url'] . '/ajax/get_kelas') ?>', {
      lokasi_id: i,
      kelas_id: j
    }, function(data) {
      $('#box_kelas').html(data.html);
    }, 'json');
  }

  function _get_kamar(i, j) {
    $.post('<?= site_url('pelayanan/registrasi_ranap/kunjungan' . '/ajax/get_kamar') ?>', {
      lokasi_id: i
    }, function(data) {
      $('#box_kamar').html(data.html);
    }, 'json');
  }

  function _cek_kamar(id, jns_rawat_cd) {
    $.post('<?= site_url('pelayanan/registrasi_ranap/kunjungan' . '/ajax/cek_kamar') ?>', {
      kamar_id: id,
      jns_rawat_cd: jns_rawat_cd,
    }, function(data) {
      if (data.status === false) {
        _get_kamar($("#lokasi_bangsal_id").val());
        $.toast({
          heading: 'Error',
          text: 'Kamar tidak tersedia',
          icon: 'error',
          position: 'top-right'
        })
      }
    }, 'json');
  }

  function reset_form() {
    $("#pasien_id").val('');
    $("#sebutan_cd").val('TN').trigger('change');
    $("#pasien_nm").val('');
    $("#nik").val('');
    $("#nama_kk").val('');
    $("#no_kk").val('');
    $("#alamat").val('');
    $("#kode_pos").val('');
    $("#wilayah_st").val('D').trigger('change');
    _empty_kabupaten();
    _empty_kecamatan();
    _empty_kelurahan();
    $("#is_bayi").prop('checked', false);
    $("#tmp_lahir").val('');
    $("#tgl_lahir").val('');
    hitung_umur();
    $("#pendidikan_cd").val('00').trigger('change');
    $("#pekerjaan_cd").val('00').trigger('change');
    $("#agama_cd").val('00').trigger('change');
    $("#no_telp").val('');
    $("#jenispasien_id").val('01').trigger('change');
    $("#asalpasien_cd").val('01').trigger('change');
    $("#nm_pj_kepesertaan").val('');
  }
</script>