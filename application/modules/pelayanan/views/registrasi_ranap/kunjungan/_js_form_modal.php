<script>
  $(document).ready(function() {
    $("#form-data").validate({
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });



    $('#table-register').DataTable({
      "ordering": false
    });
    $('.datetimepicker').daterangepicker({
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })
  });

  function pilih_reg(id) {
    $.ajax({
      type: 'post',
      url: '<?= site_url() . '/' . $nav['nav_url'] . '/ajax/get_registrasi' ?>',
      dataType: 'json',
      data: 'reg_id=' + id,
      success: (data) => {
        $("#reg_id").val(data.reg_id)
        $("#pasien_nm").val(data.pasien_nm + ', ' + data.sebutan_cd)
        $("#pasien_id").val(data.pasien_id)
        $("#lokasi_nm").val(data.lokasi_nm)
        $("#dokter_nm").val(data.dokter_nm)
      }
    })
  }
</script>