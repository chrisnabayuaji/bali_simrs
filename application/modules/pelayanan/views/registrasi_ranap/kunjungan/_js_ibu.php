<script type="text/javascript">
  var ibuTable;
  $(document).ready(function() {
    //datatables
    ibuTable = $('#ibu_table').DataTable({
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "order": [],
      "ajax": {
        "url": "<?php echo site_url() . '/' . $nav['nav_url'] . '/get_data_ibu' ?>",
        "type": "POST"
      },
      "columnDefs": [{
          "targets": 0,
          "className": 'text-center',
          "orderable": false
        },
        {
          "targets": 1,
          "className": 'text-center',
          "orderable": true
        },
        {
          "targets": 2,
          "className": 'text-left',
          "orderable": true
        },
        {
          "targets": 3,
          "className": 'text-left',
          "orderable": false
        },
        {
          "targets": 4,
          "className": 'text-left',
          "orderable": false
        },
        {
          "targets": 5,
          "className": 'text-center',
          "orderable": false
        },
        {
          "targets": 6,
          "className": 'text-center',
          "orderable": false
        },
        {
          "targets": 7,
          "className": 'text-center',
          "orderable": false
        },
        {
          "targets": 8,
          "className": 'text-center',
          "orderable": false
        }
      ],
    });
    ibuTable.columns.adjust().draw();
  })
</script>