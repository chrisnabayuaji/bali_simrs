<script type="text/javascript">
  $(document).ready(function() {
    $("#form-data").validate({
      rules: {},
      messages: {},
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-n2 mb-2');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    $('.datetimepicker').daterangepicker({
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY HH:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })

    $('#lokasi_bangsal_id').bind('change', function(e) {
      e.preventDefault();
      var i = $(this).val();
      _get_kamar(i);
    })

    function _get_kamar(i, j) {
      $.post('<?php echo site_url() . '/' . $nav['nav_url'] . '/ajax/get_kamar_pindah_bangsal' ?>', {
        lokasi_id: i
      }, function(data) {
        $('#box_kamar').html(data.html);
      }, 'json');
    }

    //datatables
    kamarTable = $('#kamar_table').DataTable({
      'retrieve': true,
      "autoWidth": false,
      "processing": true,
      "serverSide": false,
      "order": [],
      "ajax": {
        "url": "<?php echo site_url() . '/' . $nav['nav_url'] . '/ajax/data_kamar' ?>",
        "type": "POST"
      },
      "columnDefs": [{
          "targets": 0,
          "className": 'text-center',
          "orderable": false
        },
        {
          "targets": 1,
          "className": 'text-left',
          "orderable": false
        },
        {
          "targets": 2,
          "className": 'text-left',
          "orderable": false
        },
        {
          "targets": 3,
          "className": 'text-center',
          "orderable": false
        }
      ],
    });
    kamarTable.columns.adjust().draw();

  })
</script>