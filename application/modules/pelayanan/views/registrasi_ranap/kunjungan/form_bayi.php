<?php $this->load->view('pelayanan/registrasi_ranap/kunjungan/_js_form_bayi'); ?>
<div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
<form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?= $form_action ?>" class="needs-validation" novalidate autocomplete="off">
  <input type="hidden" name="reg_id_ibu" value="<?= @$main['reg_id'] ?>">
  <input type="hidden" name="ibu_umur" value="<?= @$main['umur_thn'] ?>">
  <input type="hidden" name="ibu_groupreg_in" value="<?= @$main['groupreg_in'] ?>">
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline col-title">
          <div class="col-back">
            <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
          </div>
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            <?= $nav['nav_nm'] ?>
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-user-injured"></i> Registrasi</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item active"><span>Form</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>

    <div class="row full-page mt-4">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <div class="row mt-1">
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Tgl.Registrasi<span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-9">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datetimepicker" name="tgl_registrasi" id="tgl_registrasi" value="<?php if (@$main) {
                                                                                                                                echo to_date(@$main['tgl_registrasi'], '-', 'full_date');
                                                                                                                              } else {
                                                                                                                                echo date('d-m-Y H:i:s');
                                                                                                                              } ?>" required aria-invalid="false">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Status Pasien</label>
                  <div class="col-lg-4 col-md-9">
                    <!-- <select class="form-control chosen-select" name="statuspasien_cd" id="statuspasien_cd" onchange="statuspasien()">
                      <?php foreach (get_parameter('statuspasien_cd') as $r) : ?>
                        <option value="<?= $r['parameter_cd'] ?>" <?= ($r['parameter_cd'] == 'B') ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                      <?php endforeach; ?>
                    </select> -->
                    <input type="text" class="form-control" value="Baru" disabled>
                    <input type="hidden" class="form-control" name="statuspasien_cd" value="B">
                  </div>
                  <div id="update_box" class="col-lg-2 col-md-9 d-none">
                    <div class="form-check form-check-primary mt-3">
                      <label class="form-check-label">
                        <input type="checkbox" class="form-check-input" name="update_master" id="update_master" value="1" checked> Perbarui data
                      </label>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">No. RM</label>
                  <div class="col-lg-4 col-md-9">
                    <input type="text" class="form-control form-control-lg font-weight-bold" name="pasien_id" id="pasien_id" value="otomatis" readonly>
                    <!-- <i class="fas fa-spinner fa-pulse font-loading-input d-none" id="pasien_id_loading"></i> -->
                    <!-- <div class="small-text text-success mb-2 d-none" id="no-rm-used">No. Rekam Medis sudah digunakan</div> -->
                    <div class="d-none" id="status-sumber"></div>
                  </div>
                  <div class="col-lg-5">
                    <!-- <button class="btn btn-xs btn-default" type="button" id="cari_rm"><i class="fas fa-search"></i> Cari RM</button> -->
                    <div class="form-check form-check-primary mt-3" id="no_rm_otomatis">
                      <label class="form-check-label text-left" style="margin-left: 18px;">
                        <!-- <input type="checkbox" class="form-check-input" name="rm_otomatis" id="rm_otomatis" value="1" checked> No RM Otomatis -->
                        <input type="hidden" name="rm_otomatis" value="1">
                      </label>
                    </div>
                  </div>
                </div>
                <h6 class="text-primary">Identitas Pasien</h6>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Nama Pasien</label>
                  <div class="col-lg-2 col-md-2">
                    <select class="form-control chosen-select" name="sebutan_cd" id="sebutan_cd" onchange="sebutancd()">
                      <?php foreach (get_parameter('sebutan_cd') as $r) : ?>
                        <?php if ($r['parameter_cd'] == 'BY NY') : ?>
                          <option value="<?= $r['parameter_cd'] ?>" selected><?= $r['parameter_cd'] ?></option>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="col-lg-7 col-md-6">
                    <input type="text" class="form-control" name="pasien_nm" id="pasien_nm" value="<?= @$main['pasien_nm'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Anak Ke <span class="text-danger">*<span></label>
                  <div class="col-lg-3 col-md-9">
                    <input type="text" class="form-control" name="anak_ke" id="anak_ke" value="" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Jns Kelamin <span class="text-danger">*<span></label>
                  <div class="col-lg-3 col-md-9">
                    <select class="form-control chosen-select" name="sex_cd" id="sex_cds" required>
                      <option value="">- Pilih -</option>
                      <?php foreach (get_parameter('sex_cd') as $r) : ?>
                        <option value="<?= $r['parameter_cd'] ?>"><?= $r['parameter_val'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Tempat Lahir</label>
                  <div class="col-lg-3 col-md-9">
                    <input type="text" class="form-control" name="tmp_lahir" id="tmp_lahir" value="PURWOREJO" placeholder="Tempat Lahir">
                  </div>
                  <!-- <label class="col-lg-1 col-md-3 col-form-label pl-0">Umur</label>
                  <div class="col-lg-1 col-md-9">
                    <input type="text" class="form-control" name="umur_thn" id="umur_thn" value="<?= @$main['umur_thn'] ?>" readonly>
                  </div>
                  <small>Th</small>
                  <div class="col-lg-1 col-md-9">
                    <input type="text" class="form-control" name="umur_bln" id="umur_bln" value="<?= @$main['umur_bln'] ?>" readonly>
                  </div>
                  <small>Bl</small>
                  <div class="col-lg-1 col-md-9">
                    <input type="text" class="form-control" name="umur_hr" id="umur_hr" value="<?= @$main['umur_hr'] ?>" readonly>
                  </div>
                  <small>Hr</small> -->
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Tgl Jam Lahir <span class="text-danger">*</span></label>
                  <div class="col-lg-3 col-md-9">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datepicker" name="tgl_lahir" id="tgl_lahir" value="<?= @to_date(date('Y-m-d')) ?>" placeholder="dd-mm-yyyy" required>
                    </div>
                  </div>
                  <div class="col-lg-3 col-md-9">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-clock"></i></span>
                      </div>
                      <input type="text" class="form-control" name="jam_lahir" id="jam_lahir" placeholder="Lahir Jam" required>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Nama Ibu <span class="text-danger">*<span></label>
                  <div class="col-lg-5 col-md-9">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text" style="font-size: 12px;">NY</span>
                      </div>
                      <input type="text" class="form-control" name="ibu_nm" id="ibu_nm" value="<?= @$main['pasien_nm'] ?>" required>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Nama Ayah</label>
                  <div class="col-lg-5 col-md-9">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text" style="font-size: 12px;">TN</span>
                      </div>
                      <input type="text" class="form-control" name="ayah_nm" id="ayah_nm" value="<?= (@$main['kepesertaan_cd'] == 'S') ? @$main['nm_pj_kepesertaan'] : '' ?>">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Persalinan <span class="text-danger">*<span></label>
                  <div class="col-lg-5 col-md-9">
                    <input type="text" class="form-control" name="persalinan" id="persalinan" value="<?= @$main['persalinan'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Berat <span class="text-danger">*<span></label>
                  <div class="col-lg-3 col-md-9">
                    <div class="input-group">
                      <input type="text" class="form-control form-input-group" name="bb" id="bb" required>
                      <div class="input-group-prepend">
                        <span class="input-group-text" style="font-size: 12px;">Gram</span>
                      </div>
                    </div>
                  </div>
                  <label class="col-lg-2 col-md-3 col-form-label">Panjang <span class="text-danger">*<span></label>
                  <div class="col-lg-3 col-md-9">
                    <div class="input-group">
                      <input type="text" class="form-control form-input-group" name="pb" id="pb" required>
                      <div class="input-group-prepend">
                        <span class="input-group-text" style="font-size: 12px;">Cm</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">NIK</label>
                  <div class="col-lg-4 col-md-9">
                    <input type="text" class="form-control" name="nik" id="nik" value="0" required>
                  </div>
                </div>
                <input type="hidden" name="umur_thn" value="0">
                <input type="hidden" name="umur_bln" value="0">
                <input type="hidden" name="umur_hr" value="0">
                <input type="hidden" name="goldarah_cd" value="">
                <input type="hidden" name="no_kk" value="">
                <input type="hidden" name="nama_kk" value="">
                <h6 class="text-primary">Domisili Pasien</h6>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Alamat</label>
                  <div class="col-lg-6 col-md-9">
                    <input type="text" class="form-control" name="alamat" id="alamat" value="<?= @$main['alamat'] ?>">
                  </div>
                  <!-- <label class="col-lg-2 col-md-3 col-form-label">Kode Pos</label>
                  <div class="col-lg-2 col-md-9">
                    <input type="text" class="form-control" name="kode_pos" id="kode_pos" value="<?= @$main['kode_pos'] ?>">
                  </div> -->
                  <input type="hidden" name="kode_pos" value="">
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Status Wilayah</label>
                  <div class="col-lg-3 col-md-9">
                    <select class="form-control chosen-select" name="wilayah_st" id="wilayah_st">
                      <option value="D" <?= (@$main['wilayah_st'] == 'D') ? 'selected' : ''; ?>>Dalam Wilayah</option>
                      <option value="L" <?= (@$main['wilayah_st'] == 'L') ? 'selected' : ''; ?>>Luar Wilayah</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Propinsi</label>
                  <div class="col-lg-4 col-md-5">
                    <div id="box_wilayah_prop">
                      <select class="chosen-select custom-select w-100" name="wilayah_prop" id="wilayah_prop">
                        <option value="">- Propinsi -</option>
                        <?php foreach ($list_wilayah_prop as $wp) : ?>
                          <option value="<?= $wp['wilayah_id'] ?>#<?= $wp['wilayah_nm'] ?>" <?php if ($wp['wilayah_id'] == get_wilayah_id(@$main['wilayah_id'], 'provinsi')) echo 'selected' ?>><?= $wp['wilayah_id'] ?> - <?= $wp['wilayah_nm'] ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <label class="col-lg-2 col-md-3 col-form-label">Kab/Kota</label>
                  <div class="col-lg-4 col-md-5">
                    <div id="box_wilayah_kab">
                      <select class="chosen-select custom-select w-100" name="wilayah_kab" id="wilayah_kab">
                        <option value="">- Kab/Kota -</option>
                      </select>
                    </div>
                  </div>
                  <label class="col-lg-2 col-md-3 col-form-label">Kecamatan</label>
                  <div class="col-lg-4 col-md-5">
                    <div id="box_wilayah_kec">
                      <select class="chosen-select custom-select w-100" name="wilayah_kec" id="wilayah_kec">
                        <option value="">- Kecamatan -</option>
                      </select>
                    </div>
                  </div>
                  <label class="col-lg-2 col-md-3 col-form-label pl-0">Desa/Kelurahan</label>
                  <div class="col-lg-4 col-md-5">
                    <div id="box_wilayah_kel">
                      <select class="chosen-select custom-select w-100" name="wilayah_kel" id="wilayah_kel">
                        <option value="">- Desa/Kelurahan -</option>
                      </select>
                    </div>
                  </div>
                </div>
                <input type="hidden" name="pendidikan_cd" value="00">
                <input type="hidden" name="pekerjaan_cd" value="00">
                <input type="hidden" name="agama_cd" value="00">
                <input type="hidden" name="no_telp" value="">
                <input type="hidden" name="jeniskunjungan_cd" value="L">
              </div>
              <div class="col-md-6">
                <h6 class="text-primary">Kunjungan Pasien</h6>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Asal Pasien <span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-9">
                    <select class="form-control chosen-select" name="asalpasien_cd" id="asalpasien_cd">
                      <?php foreach (get_parameter('asalpasien_cd') as $r) : ?>
                        <option value="<?= $r['parameter_cd'] ?>" <?= ($r['parameter_cd'] == '05') ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Jenis Pasien <span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-9">
                    <select class="form-control chosen-select" name="jenispasien_id" id="jenispasien_id" onchange="jenispasien()" required>
                      <?php foreach ($jenis_pasien as $r) : ?>
                        <option value="<?= $r['jenispasien_id'] ?>" <?= (@$main['jenispasien_id'] == $r['jenispasien_id']) ? 'selected' : ''; ?>><?= $r['jenispasien_nm'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label d-none" id="label-no-kartu">No. Kartu <span class="text-danger">*<span></label>
                  <div class="col-lg-6 col-md-9 d-none" id="col-no-kartu">
                    <input type="text" class="form-control" name="no_kartu" id="no_kartu" value="<?= @$main['no_kartu'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Jenis Penjamin</label>
                  <div class="col-lg-4 col-md-9">
                    <select class="form-control chosen-select" name="penjamin_cd" id="penjamin_cd">
                      <?php foreach (get_parameter('penjamin_cd') as $r) : ?>
                        <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['penjamin_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Nama Penjamin</label>
                  <div class="col-lg-6 col-md-9">
                    <input type="text" class="form-control" name="penjamin_nm" id="penjamin_nm" value="<?= @$main['penjamin_nm'] ?>">
                  </div>
                </div>
                <div class="form-group row" id="sep_no_box">
                  <label class="col-lg-3 col-md-3 col-form-label">No. SEP</label>
                  <div class="col-lg-6 col-md-9">
                    <input type="text" class="form-control" name="sep_no" id="sep_no" value="<?= @$main['sep_no'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Penanggungjawab</label>
                  <div class="col-lg-3 col-md-9">
                    <select class="form-control chosen-select" name="kepesertaan_cd" id="kepesertaan_cd">
                      <option value="">---</option>
                      <?php foreach (get_parameter('kepesertaan_cd') as $r) : ?>
                        <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['kepesertaan_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Nama PJ</label>
                  <div class="col-lg-6 col-md-9">
                    <input type="text" class="form-control" name="nm_pj_kepesertaan" id="nm_pj_kepesertaan" value="<?= @$main['nm_pj_kepesertaan'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Lokasi Asal <span class="text-danger">*<span></label>
                  <div class="col-lg-6 col-md-9">
                    <select class="form-control chosen-select" name="lokasi_asal_id" id="lokasi_asal_id" required="">
                      <option value="">---</option>
                      <?php foreach ($lokasi_asal as $r) : ?>
                        <option value="<?= $r['lokasi_id'] ?>" <?= (@$main['lokasi_id'] == $r['lokasi_id']) ? 'selected' : ''; ?>><?= $r['lokasi_nm'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Kelas Asal <span class="text-danger">*<span></label>
                  <div class="col-lg-3 col-md-9">
                    <div id="box_kelas">
                      <select class="form-control chosen-select" name="src_kelas_id" id="src_kelas_id" required="">
                        <option value="">-- Pilih --</option>
                      </select>
                    </div>
                  </div>
                </div>
                <hr class="mt-2 mb-3">
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Dokter PJ <span class="text-danger">*<span></label>
                  <div class="col-lg-5 col-md-9">
                    <select class="form-control chosen-select" name="dokter_id" id="dokter_id" required>
                      <option value="">- Pilih -</option>
                      <?php foreach ($dokter as $r) : ?>
                        <option value="<?= $r['pegawai_id'] ?>"><?= $r['pegawai_nm'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label mt-n3">Dokter Penolong Persalinan <span class="text-danger">*<span></label>
                  <div class="col-lg-5 col-md-9">
                    <select class="form-control chosen-select" name="dr_persalinan" id="dr_persalinan" required>
                      <?php foreach ($dokter as $r) : ?>
                        <option value="<?= $r['pegawai_id'] ?>" <?= (@$main['dokter_id'] == $r['pegawai_id']) ? 'selected' : ''; ?>><?= $r['pegawai_nm'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Jenis Rawat Inap <span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-9">
                    <select class="form-control chosen-select" name="jns_rawat_cd" id="jns_rawat_cd" onchange="jnsrawat()" required>
                      <option value="">- Pilih -</option>
                      <option value="01">Rawat Gabung</option>
                      <option value="02">Rawat Pisah</option>
                    </select>
                  </div>
                </div>
                <div class="d-none" id="rawat-pisah">
                  <div class="form-group row">
                    <label class="col-lg-3 col-md-3 col-form-label">Status Ranap</label>
                    <div class="col-lg-4 col-md-9">
                      <select class="form-control chosen-select-2" name="status_ranap_cd" id="status_ranap_cd">
                        <?php foreach (get_parameter('status_ranap_cd') as $r) : ?>
                          <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['status_ranap_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-lg-3 col-md-3 col-form-label">Lokasi Bangsal <span class="text-danger">*<span></label>
                    <div class="col-lg-4 col-md-9">
                      <select class="form-control chosen-select-2" name="lokasi_bangsal_id" id="lokasi_bangsal_id" required="">
                        <option value="">- Pilih -</option>
                        <?php foreach ($lokasi_bangsal as $r) : ?>
                          <option value="<?= $r['lokasi_id'] ?>"><?= $r['lokasi_nm'] ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                    <label class="col-lg-2 col-md-3 col-form-label">Kamar <span class="text-danger">*<span></label>
                    <div class="col-lg-3 col-md-9">
                      <div id="box_kamar">
                        <select class="form-control chosen-select-2" name="kamar_id" id="kamar_id" required="">
                          <option value="">- Pilih -</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="col-lg-3 col-md-3 col-form-label">Kelas Ranap <span class="text-danger">*<span></label>
                    <div class="col-lg-4 col-md-9">
                      <select class="form-control chosen-select-2" name="kelas_id" id="kelas_id" required="">
                        <option value="">- Pilih -</option>
                        <?php foreach ($kelas as $r) : ?>
                          <option value="<?= $r['kelas_id'] ?>"><?= $r['kelas_nm'] ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="btn-form">
    <div class="btn-form-action">
      <div class="btn-form-action-bottom w-100 small-text clearfix">
        <div class="col-lg-10 col-md-8 col-4">
          <a href="#" data-href="<?= site_url() . '/' . 'pelayanan/registrasi_ranap/kunjungan' . '/info_kamar' ?>" modal-title="Info Ketersediaan Kamar" modal-size="md" class="btn btn-xs btn-info modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-info" title="" data-original-title="Info Ketersediaan Kamar"><i class="fas fa-info"></i> Info Ketersediaan Kamar</a>
        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-clear">
          <button type="reset" onClick="window.location.reload();" class="btn btn-xs btn-secondary btn-clear"><i class="fas fa-sync-alt"></i> Reset</button>
        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-save">
          <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>