<?php $this->load->view('pelayanan/registrasi_ranap/kunjungan/_js_edit'); ?>
<div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
<form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?= $form_action ?>" class="needs-validation" novalidate autocomplete="off">
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline col-title">
          <div class="col-back">
            <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
          </div>
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            <?= $nav['nav_nm'] ?>
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-user-injured"></i> Registrasi</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item active"><span>Form</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>

    <div class="row full-page mt-4">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <div class="row mt-1">
              <div class="col-md-7">
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Tgl. Registrasi</label>
                  <div class="col-lg-3 col-md-9">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control" value="<?= to_date(@$main['tgl_registrasi'], '-', 'full_date') ?>" required aria-invalid="false" disabled>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label mt-n1">No. RM</label>
                  <div class="col-lg-4 col-md-9">
                    <input type="hidden" name="statuspasien_cd" value="<?= @$main['statuspasien_cd'] ?>">
                    <input type="text" class="form-control form-control-lg font-weight-bold" name="pasien_id" id="pasien_id" value="<?= @$main['pasien_id'] ?>" readonly>
                    <i class="fas fa-spinner fa-pulse font-loading-input d-none" id="pasien_id_loading"></i>
                    <div class="small-text text-success mb-2 d-none" id="no-rm-used">No. Rekam Medis sudah digunakan</div>
                    <div class="d-none" id="status-sumber"></div>
                  </div>
                  <!-- <div class="col-lg-5">
                      <button class="btn btn-xs btn-default" type="button" id="cari_rm"><i class="fas fa-search"></i> Cari RM</button>
                      <div class="form-check form-check-primary mt-3" id="no_rm_otomatis" style="display: none;">
                        <label class="form-check-label text-left" style="margin-left: 18px;">
                          <input type="checkbox" class="form-check-input" name="rm_otomatis" id="rm_otomatis" value="1"> No RM Otomatis
                        </label>
                      </div>
                    </div> -->
                </div>
                <h6 class="text-primary">Identitas Pasien</h6>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Nama Pasien <span class="text-danger">*<span></label>
                  <div class="col-lg-2 col-md-2">
                    <select class="form-control chosen-select" name="sebutan_cd" id="sebutan_cd" onchange="sebutancd()">
                      <?php foreach (get_parameter('sebutan_cd') as $r) : ?>
                        <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['sebutan_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_cd'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <div class="col-lg-7 col-md-6">
                    <input type="text" class="form-control" name="pasien_nm" id="pasien_nm" value="<?= @$main['pasien_nm'] ?>" required>
                  </div>
                </div>
                <div class="form-group row d-none" id="box-anak-ke">
                  <label class="col-lg-2 col-md-3 col-form-label">Anak Ke <span class="text-danger">*<span></label>
                  <div class="col-lg-2 col-md-9">
                    <input type="text" class="form-control" name="anak_ke" id="anak_ke" value="<?= @$main['anak_ke'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Tmp Tgl Lahir</label>
                  <div class="col-lg-3 col-md-9">
                    <input type="text" class="form-control" name="tmp_lahir" id="tmp_lahir" value="<?= @$main['tmp_lahir'] ?>" placeholder="Tempat Lahir">
                  </div>
                  <div class="col-lg-2 col-md-9">
                    <input type="text" class="form-control datepicker" name="tgl_lahir" id="tgl_lahir" value="<?= @to_date(($main['tgl_lahir'] == '0000-00-00' || $main['tgl_lahir'] == '') ? date('Y-m-d') : $main['tgl_lahir']) ?>" onchange="hitung_umur()" placeholder="dd-mm-yyyy" required>
                  </div>
                  <label class="col-lg-1 col-md-3 col-form-label pl-0">Umur</label>
                  <div class="col-lg-1 col-md-9">
                    <input type="text" class="form-control" name="umur_thn" id="umur_thn" value="<?= @$main['umur_thn'] ?>" readonly>
                  </div>
                  <small>Th</small>
                  <div class="col-lg-1 col-md-9">
                    <input type="text" class="form-control" name="umur_bln" id="umur_bln" value="<?= @$main['umur_bln'] ?>" readonly>
                  </div>
                  <small>Bl</small>
                  <div class="col-lg-1 col-md-9">
                    <input type="text" class="form-control" name="umur_hr" id="umur_hr" value="<?= @$main['umur_hr'] ?>" readonly>
                  </div>
                  <small>Hr</small>
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Jenis Kelamin <span class="text-danger">*<span></label>
                  <div class="col-lg-3 col-md-9">
                    <select class="form-control chosen-select" name="sex_cd" id="sex_cd">
                      <?php foreach (get_parameter('sex_cd') as $r) : ?>
                        <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['sex_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <label class="col-lg-3 col-md-3 col-form-label">Gol. Darah</label>
                  <div class="col-lg-2 col-md-9">
                    <select class="form-control chosen-select" name="goldarah_cd" id="goldarah_cd">
                      <option value="">---</option>
                      <?php foreach (get_parameter('goldarah_cd') as $r) : ?>
                        <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['goldarah_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">NIK <span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-9">
                    <input type="text" class="form-control" name="nik" id="nik" value="<?= @$main['nik'] ?>" required>
                    <i class="fas fa-spinner fa-pulse font-loading-input d-none" id="nik_loading"></i>
                  </div>
                  <div class="col-lg-3">
                    <!-- <button type="button" class="btn btn-xs btn-primary" onclick="cari_nik()" id="btn-search-nik"><i class="fas fa-search"></i> Cari NIK</button> -->
                  </div>
                </div>
                <!-- <div class="form-group row">
                    <label class="col-lg-2 col-md-3 col-form-label">Nomor KK</label>
                    <div class="col-lg-4 col-md-9">
                      <input type="text" class="form-control" name="no_kk" id="no_kk" value="<?= @$main['no_kk'] ?>">
                    </div>
                    <label class="col-lg-2 col-md-3 col-form-label">Nama KK</label>
                    <div class="col-lg-4 col-md-9">
                      <input type="text" class="form-control" name="nama_kk" id="nama_kk" value="<?= @$main['nama_kk'] ?>">
                    </div>
                  </div> -->
                <input type="hidden" name="no_kk" value="">
                <input type="hidden" name="nama_kk" value="">
                <h6 class="text-primary">Domisili Pasien</h6>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Alamat</label>
                  <div class="col-lg-6 col-md-9">
                    <input type="text" class="form-control" name="alamat" id="alamat" value="<?= @$main['alamat'] ?>">
                  </div>
                  <!-- <label class="col-lg-2 col-md-3 col-form-label">Kode Pos</label>
                    <div class="col-lg-2 col-md-9">
                      <input type="text" class="form-control" name="kode_pos" id="kode_pos" value="<?= @$main['kode_pos'] ?>">
                    </div> -->
                  <input type="hidden" name="kode_pos" value="">
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Status Wilayah</label>
                  <div class="col-lg-3 col-md-9">
                    <select class="form-control chosen-select" name="wilayah_st" id="wilayah_st">
                      <option value="D" <?= (@$main['wilayah_st'] == 'D') ? 'selected' : ''; ?>>Dalam Wilayah</option>
                      <option value="L" <?= (@$main['wilayah_st'] == 'L') ? 'selected' : ''; ?>>Luar Wilayah</option>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Propinsi</label>
                  <div class="col-lg-4 col-md-5">
                    <div id="box_wilayah_prop">
                      <select class="chosen-select custom-select w-100" name="wilayah_prop" id="wilayah_prop">
                        <option value="">- Propinsi -</option>
                        <?php foreach ($list_wilayah_prop as $wp) : ?>
                          <option value="<?= $wp['wilayah_id'] ?>#<?= $wp['wilayah_nm'] ?>" <?php if ($wp['wilayah_id'] == get_wilayah_id(@$main['wilayah_id'], 'provinsi')) echo 'selected' ?>><?= $wp['wilayah_id'] ?> - <?= $wp['wilayah_nm'] ?></option>
                        <?php endforeach; ?>
                      </select>
                    </div>
                  </div>
                  <label class="col-lg-2 col-md-3 col-form-label">Kab/Kota</label>
                  <div class="col-lg-4 col-md-5">
                    <div id="box_wilayah_kab">
                      <select class="chosen-select custom-select w-100" name="wilayah_kab" id="wilayah_kab">
                        <option value="">- Kab/Kota -</option>
                      </select>
                    </div>
                  </div>
                  <label class="col-lg-2 col-md-3 col-form-label">Kecamatan</label>
                  <div class="col-lg-4 col-md-5">
                    <div id="box_wilayah_kec">
                      <select class="chosen-select custom-select w-100" name="wilayah_kec" id="wilayah_kec">
                        <option value="">- Kecamatan -</option>
                      </select>
                    </div>
                  </div>
                  <label class="col-lg-2 col-md-3 col-form-label pl-0">Desa/Kelurahan</label>
                  <div class="col-lg-4 col-md-5">
                    <div id="box_wilayah_kel">
                      <select class="chosen-select custom-select w-100" name="wilayah_kel" id="wilayah_kel">
                        <option value="">- Desa/Kelurahan -</option>
                      </select>
                    </div>
                  </div>
                </div>
                <h6 class="text-primary">Data Lain</h6>
                <div class="form-group row mt-2">
                  <label class="col-lg-2 col-md-3 col-form-label">Pendidikan</label>
                  <div class="col-lg-4 col-md-9">
                    <select class="form-control chosen-select" name="pendidikan_cd" id="pendidikan_cd">
                      <?php foreach (get_parameter('pendidikan_cd') as $r) : ?>
                        <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['pendidikan_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <label class="col-lg-2 col-md-3 col-form-label">Pekerjaan</label>
                  <div class="col-lg-4 col-md-9">
                    <select class="form-control chosen-select" name="pekerjaan_cd" id="pekerjaan_cd">
                      <?php foreach (get_parameter('pekerjaan_cd') as $r) : ?>
                        <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['pekerjaan_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-2 col-md-3 col-form-label">Agama</label>
                  <div class="col-lg-4 col-md-9">
                    <select class="form-control chosen-select" name="agama_cd" id="agama_cd">
                      <?php foreach (get_parameter('agama_cd') as $r) : ?>
                        <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['agama_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                  <label class="col-lg-2 col-md-3 col-form-label">No. Telp</label>
                  <div class="col-lg-4 col-md-9">
                    <input type="text" class="form-control" name="no_telp" id="no_telp" value="<?= @$main['no_telp'] ?>">
                  </div>
                </div>

              </div>
              <div class="col-md-5">
                <h6 class="text-primary">Kunjungan Pasien</h6>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Asal Pasien <span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-9">
                    <select class="form-control chosen-select" name="asalpasien_cd" id="asalpasien_cd">
                      <?php foreach (get_parameter('asalpasien_cd') as $r) : ?>
                        <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['asalpasien_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Jenis Pasien <span class="text-danger">*<span></label>
                  <div class="col-lg-4 col-md-9">
                    <select class="form-control chosen-select" name="jenispasien_id" id="jenispasien_id" onchange="jenispasien()" required>
                      <?php foreach ($jenis_pasien as $r) : ?>
                        <option value="<?= $r['jenispasien_id'] ?>" <?= (@$main['jenispasien_id'] == $r['jenispasien_id']) ? 'selected' : ''; ?>><?= $r['jenispasien_nm'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label d-none" id="label-no-kartu">No. Kartu <span class="text-danger">*<span></label>
                  <div class="col-lg-6 col-md-9 d-none" id="col-no-kartu">
                    <input type="text" class="form-control" name="no_kartu" id="no_kartu" value="<?= @$main['no_kartu'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Jenis Penjamin</label>
                  <div class="col-lg-4 col-md-9">
                    <select class="form-control chosen-select" name="penjamin_cd" id="penjamin_cd">
                      <?php foreach (get_parameter('penjamin_cd') as $r) : ?>
                        <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['penjamin_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Nama Penjamin</label>
                  <div class="col-lg-6 col-md-9">
                    <input type="text" class="form-control" name="penjamin_nm" id="penjamin_nm" value="<?= @$main['penjamin_nm'] ?>">
                  </div>
                </div>
                <div class="form-group row" id="sep_no_box">
                  <label class="col-lg-3 col-md-3 col-form-label">No. SEP</label>
                  <div class="col-lg-6 col-md-9">
                    <input type="text" class="form-control" name="sep_no" id="sep_no" value="<?= @$main['sep_no'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Penangungjawab</label>
                  <div class="col-lg-3 col-md-9">
                    <select class="form-control chosen-select" name="kepesertaan_cd" id="kepesertaan_cd">
                      <option value="">---</option>
                      <?php foreach (get_parameter('kepesertaan_cd') as $r) : ?>
                        <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['kepesertaan_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Nama PJ</label>
                  <div class="col-lg-6 col-md-9">
                    <input type="text" class="form-control" name="nm_pj_kepesertaan" id="nm_pj_kepesertaan" value="<?= @$main['nm_pj_kepesertaan'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-3 col-md-3 col-form-label">Dokter PJ <span class="text-danger">*<span></label>
                  <div class="col-lg-6 col-md-9">
                    <select class="form-control chosen-select" name="dokter_id" id="dokter_id">
                      <?php foreach ($dokter as $r) : ?>
                        <option value="<?= $r['pegawai_id'] ?>" <?= (@$main['dokter_id'] == $r['pegawai_id']) ? 'selected' : ''; ?>><?= $r['pegawai_nm'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="btn-form">
    <div class="btn-form-action">
      <div class="btn-form-action-bottom w-100 small-text clearfix">
        <div class="col-lg-10 col-md-8 col-4">
          <a href="#" data-href="<?= site_url() . '/' . 'pelayanan/registrasi_ranap/verifikasi' . '/info_kamar' ?>" modal-title="Info Ketersediaan Kamar" modal-size="md" class="btn btn-xs btn-info modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-info" title="" data-original-title="Info Ketersediaan Kamar"><i class="fas fa-info"></i> Info Ketersediaan Kamar</a>
        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-clear">
          <button type="reset" onClick="window.location.reload();" class="btn btn-xs btn-secondary btn-clear"><i class="fas fa-sync-alt"></i> Reset</button>
        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-save">
          <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>