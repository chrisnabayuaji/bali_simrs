<?php $this->load->view('_js_form_modal'); ?>
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?= $form_action ?>" autocomplete="off">
  <div class="row">
    <div class="col-md-12">
      <h4 class="card-title border-bottom border-2 pb-2 mb-2"><i class="fas fa-user-injured"></i> List Data Pasien Rawat Jalan Hari Ini</h4>
      <div class="table-responsive">
        <table id="rajal_table" class="table table-hover table-bordered table-striped table-sm w-100">
          <thead>
            <tr>
              <th class="text-center" width="20">No</th>
              <th class="text-center" width="50">No. RM</th>
              <th class="text-center" width="300">Nama Pasien</th>
              <th class="text-center" width="20">JK</th>
              <th class="text-center">Alamat</th>
              <th class="text-center" width="130">Lokasi / Jns. Pasien</th>
              <th class="text-center" width="95">Tgl. Registrasi</th>
              <th class="text-center" width="100">Aksi</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-10 offset-md-7">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
      </div>
    </div>
  </div>
</form>

<script type="text/javascript">
  var rajalTable;
  $(document).ready(function() {
    //datatables
    rajalTable = $('#rajal_table').DataTable({
      'retrieve': true,
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "order": [],
      "ajax": {
        "url": "<?php echo site_url() . '/' . $nav['nav_url'] . '/ajax_list_ibu/search_ibu' ?>",
        "type": "POST"
      },
      "columnDefs": [{
          "targets": 0,
          "className": 'text-center',
          "orderable": false
        },
        {
          "targets": 1,
          "className": 'text-center',
          "orderable": true
        },
        {
          "targets": 2,
          "className": 'text-left',
          "orderable": true
        },
        {
          "targets": 3,
          "className": 'text-center',
          "orderable": false
        },
        {
          "targets": 4,
          "className": 'text-left',
          "orderable": false
        },
        {
          "targets": 5,
          "className": 'text-center',
          "orderable": false
        },
        {
          "targets": 6,
          "className": 'text-center',
          "orderable": false
        },
        {
          "targets": 7,
          "className": 'text-center',
          "orderable": false
        },
      ],
    });
    rajalTable.columns.adjust().draw();
  })
</script>