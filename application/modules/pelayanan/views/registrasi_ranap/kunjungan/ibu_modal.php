<!-- js -->
<?php $this->load->view('pelayanan/registrasi_ranap/kunjungan/_js_ibu') ?>
<!-- / -->
<div class="table-responsive">
  <table id="ibu_table" class="table table-hover table-bordered table-striped table-sm w-100">
    <thead>
      <tr>
        <th class="text-center" width="20">No.</th>
        <th class="text-center" width="50">No. RM</th>
        <th class="text-center">Nama Pasien</th>
        <th class="text-center" width="180">Alamat Pasien</th>
        <th class="text-center" width="130">Tmp Tgl Lahir</th>
        <th class="text-center" width="70">Jenis Pasien</th>
        <th class="text-center" width="150">Lokasi</th>
        <th class="text-center" width="100">Tgl. Registrasi</th>
        <th class="text-center" width="60">Aksi</th>
      </tr>
    </thead>
  </table>
</div>