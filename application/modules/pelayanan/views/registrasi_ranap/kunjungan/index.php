<script type="text/javascript">
  $(document).ready(function() {
    <?php if (@$cookie['search']['tgl_registrasi_from'] == '') : ?>
      $('.datepicker-from').val('');
    <?php endif; ?>
    <?php if (@$cookie['search']['tgl_registrasi_to'] == '') : ?>
      $('.datepicker-to').val('');
    <?php endif; ?>
  })
</script>
<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline col-title">
        <div class="col-back">
          <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
        </div>
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          <?= $nav['nav_nm'] ?> > List Kunjungan
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item"><a href="#"><i class="fas fa-bed"></i> Registrasi Ranap</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url('pelayanan/registrasi_ranap/kunjungan') ?>"><i class="<?= $nav['nav_icon'] ?>"></i> List Kunjungan</a></li>
          <li class="breadcrumb-item active"><span>Index</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>
  <!-- flash message -->
  <div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
  <!-- /flash message -->
  <div class="row full-page mt-4 mb-n2">
    <div class="col-lg-12 grid-margin-xl-0 stretch-card">
      <div class="card border-none">
        <div class="card-body">
          <div class="row mt-n3 mb-1">
            <div class="col-md-10 mt-3">
              <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link tab-menu active" href="<?= site_url('pelayanan/registrasi_ranap/kunjungan') ?>"><i class="fas fa-users"></i> LIST KUNJUNGAN</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link tab-menu" href="<?= site_url('pelayanan/registrasi_ranap/verifikasi') ?>"><i class="fas fa-check"></i> VERIFIKASI <span class="badge badge-xs badge-warning <?php if ($total_verifikasi > 0) {
                                                                                                                                                                                                    echo 'blink';
                                                                                                                                                                                                  } ?>"><?= $total_verifikasi ?></span></a>
                </li>
              </ul>
            </div>
            <div class="col-md-2 mt-3">
              <span class="float-right text-gray" data-toggle="collapse" data-target="#container-search" aria-expanded="false" aria-controls="container-search"><i class="fas fa-search"></i></span>
            </div>
          </div>
          <form id="form-search" action="<?= site_url() . '/app/search_redirect/' . $nav['nav_id'] . '.kunjungan' ?>" method="post" autocomplete="off">
            <input type="hidden" name="redirect" value="pelayanan/registrasi_ranap/kunjungan">
            <div id="container-search" class="row border-dotted collapse show multi-collapse">
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Tgl.Masuk</label>
                  <div class="col-lg-8 col-md-8">
                    <div class="row">
                      <div class="col-md-5" style="padding-right: 5px; flex: 0 0 46.4%; max-width: 46.4%;">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datepicker datepicker-from text-center" name="tgl_registrasi_from" id="tgl_registrasi_from" value="<?= @$cookie['search']['tgl_registrasi_from'] ?>">
                        </div>
                      </div>
                      <div class="col-md-2 text-center-group">s.d</div>
                      <div class="col-md-5" style="padding-left: 5px; flex: 0 0 46.4%; max-width: 46.4%;">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datepicker datepicker-to text-center" name="tgl_registrasi_to" id="tgl_registrasi_to" value="<?= @$cookie['search']['tgl_registrasi_to'] ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">No.RM/Nama Pasien</label>
                  <div class="col-lg-8 col-md-8">
                    <input type="text" class="form-control" name="no_rm_nm" id="no_rm_nm" value="<?= @$cookie['search']['no_rm_nm'] ?>">
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Lokasi Asal</label>
                  <div class="col-lg-8 col-md-8">
                    <select class="form-control chosen-select" name="lokasi_asal" id="lokasi_asal">
                      <option value="">- Semua -</option>
                      <?php foreach ($lokasi as $r) : ?>
                        <?php if (in_array($r['lokasi_id'], $this->session->userdata('sess_lokasi'))) : ?>
                          <option value="<?= $r['lokasi_id'] ?>" <?= (@$cookie['search']['lokasi_id'] == $r['lokasi_id']) ? 'selected' : ''; ?>><?= $r['lokasi_nm'] ?></option>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Lokasi Bangsal</label>
                  <div class="col-lg-8 col-md-8">
                    <select class="form-control chosen-select" name="lokasi_bangsal" id="lokasi_bangsal">
                      <option value="">- Semua -</option>
                      <?php foreach ($lokasi_bangsal as $r) : ?>
                        <?php if (in_array($r['lokasi_id'], $this->session->userdata('sess_lokasi'))) : ?>
                          <option value="<?= $r['lokasi_id'] ?>" <?= (@$cookie['search']['lokasi_id'] == $r['lokasi_id']) ? 'selected' : ''; ?>><?= $r['lokasi_nm'] ?></option>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-2">
                <button type="submit" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cari Data"><i class="fas fa-search"></i> Cari</button>
                <a class="btn btn-xs btn-default" href="<?= site_url() . '/app/reset_redirect/' . $nav['nav_id'] . '.kunjungan/' . str_replace('/', '-', 'pelayanan/registrasi_ranap/kunjungan') ?>" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-default" title="" data-original-title="Reset"><i class="fas fa-sync-alt"></i> Reset</a>
              </div>
            </div>
          </form>
          <div class="row mt-2">
            <div class="col-md-12 mb-2">
              <?php if ($nav['_add']) : ?>
                <!-- <a href="<?= site_url() . '/pelayanan/registrasi_ranap/kunjungan/form' ?>" modal-title="Tambah Data" modal-size="md" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Tambah Data"><i class="fas fa-plus-circle"></i> Tambah</a> -->
                <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/form_modal' ?>" modal-title="" modal-size="lg" class="btn btn-xs btn-primary modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" modal-content-top="-70px" title="" data-original-title="Tambah Data Bayi"><i class="fas fa-plus-circle"></i> Tambah Data Bayi</a>
              <?php endif; ?>
            </div>
            <div class="col-md-12">
              <div class="table-responsive">
                <form id="form-multiple" action="<?= site_url() . '/' . $nav['nav_url'] . '/multiple/enable' ?>" method="post">
                  <table class="table table-hover table-striped table-bordered table-fixed">
                    <thead>
                      <tr>
                        <th class="text-center" width="36">No</th>
                        <th class="text-center" width="60">Aksi</th>
                        <th class="text-center" width="90">No. Reg</th>
                        <th class="text-center" width="80">No.RM</th>
                        <th class="text-center" width="200"><?= table_sort($nav['nav_id'], 'Nama Pasien', 'pasien_nm', $cookie['order']) ?></th>
                        <th class="text-center">Alamat</th>
                        <th class="text-center" width="30">JK</th>
                        <th class="text-center" width="120">Jenis Pasien</th>
                        <th class="text-center" width="100">Kelas Ranap</th>
                        <th class="text-center" width="100">Lokasi Asal</th>
                        <th class="text-center" width="100">Bangsal/Kamar</th>
                        <th class="text-center" width="80">Tgl. Masuk</th>
                        <th class="text-center" width="80">Tgl. Pulang</th>
                        <th class="text-center" width="50">Status</th>
                      </tr>
                    </thead>
                    <?php if (@$main == null) : ?>
                      <tbody>
                        <tr>
                          <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                        </tr>
                      </tbody>
                    <?php else : ?>
                      <tbody>
                        <?php $i = 1;
                        foreach ($main as $row) : ?>
                          <tr>
                            <td class="text-center" width="36"><?= $cookie['cur_page'] + ($i++) ?></td>
                            <td class="text-center" width="60">
                              <?php if ($nav['_update'] || $nav['_delete']) : ?>
                                <?php if ($nav['_update']) : ?>
                                  <div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                      Aksi
                                    </button>
                                    <div class="dropdown-menu">
                                      <!-- <?php if ($row['jenispasien_id'] == '02') : ?>
                                        <?php if ($row['sep_no'] != '') : ?>
                                          <a class="dropdown-item" href="<?= site_url() . '/' . $nav['nav_url'] . '/ubah_sep/' . $row['reg_id'] ?>"><i class="fas fa-edit"></i> Ubah SEP</a>
                                          <a class="dropdown-item modal-href" href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/cetak_modal/cetak_sep_pdf/' . $row['reg_id'] . '/' . $row['sep_no'] ?>" modal-title="Cetak SEP <?= $row['sep_no'] ?>" modal-size="lg" modal-content-top="-75px"><i class="fas fa-print"></i> Cetak SEP</a>
                                        <?php else : ?>
                                          <a class="dropdown-item" href="<?= site_url() . '/' . $nav['nav_url'] . '/pembuatan_sep/' . $row['reg_id'] ?>"><i class="fas fa-copy"></i> Pembuatan SEP</a>
                                        <?php endif; ?>
                                      <?php endif; ?> -->
                                      <a class="dropdown-item" href="<?= site_url('pelayanan/registrasi_ranap/kunjungan/form_edit/' . $row['reg_id']) ?>"><i class="fas fa-pencil-alt"></i> Ubah</a>
                                      <a class="dropdown-item modal-href" href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/pindah_bangsal/' . $row['reg_id'] . '/' . $row['pasien_id'] . '/' . $row['kelas_id'] . '/' . $row['lokasi_id'] . '/' . $row['kamar_id'] ?>" modal-title="Pindah Bangsal : <?= $row['pasien_nm'] ?>" modal-size="lg" modal-header="hidden"><i class="fas fa-procedures"></i> Pindah Bangsal</a>
                                      <a class="dropdown-item modal-href" href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/riwayat_bangsal/' . $row['reg_id'] ?>" modal-title="Riwayat Bangsal : <?= $row['pasien_nm'] ?>" modal-size="lg"><i class="fas fa-file-medical"></i> Riwayat Bangsal</a>
                                      <!-- <a href="#" data-href="<?= site_url('pelayanan/registrasi_ranap/kunjungan/delete/' . $row['reg_id']) ?>" class="dropdown-item btn-delete"><i class="far fa-trash-alt"></i> Hapus</a> -->
                                    </div>
                                  </div>
                                <?php endif; ?>
                              <?php endif; ?>
                            </td>
                            <td class="text-center" width="90"><?= $row['reg_id'] ?></td>
                            <td class="text-center" width="80"><?= $row['pasien_id'] ?></td>
                            <td class="text-left" width="200"><?= $row['pasien_nm'] . ', ' . $row['sebutan_cd'] ?></td>
                            <td class="text-left">
                              <?= ($row['alamat'] != '') ? $row['alamat'] . ', ' : '' ?>
                              <?= ($row['kelurahan'] != '') ? ucwords(strtolower($row['kelurahan'])) . ', ' : '' ?>
                              <?= ($row['kecamatan'] != '') ? ucwords(strtolower($row['kecamatan'])) . ', ' : '' ?>
                              <?= ($row['kabupaten'] != '') ? ucwords(strtolower($row['kabupaten'])) . ', ' : '' ?>
                              <?= ($row['provinsi'] != '') ? ucwords(strtolower($row['provinsi'])) : '' ?>
                            </td>
                            <?php
                            switch ($row['status_ranap_cd']) {
                              case 'S':
                                $color_status_ranap = 'success';
                                break;

                              case 'T':
                                $color_status_ranap = 'warning';
                                break;

                              case 'N':
                                $color_status_ranap = 'info';
                                break;
                            }
                            ?>
                            <td class="text-center" width="30"><?= $row['sex_cd'] ?></td>
                            <td class="text-center" width="120"><?= $row['jenispasien_nm'] ?><?= ($row['no_kartu'] != '') ? '<br>' . $row['no_kartu'] : '' ?><?= ($row['sep_no'] != '') ? '<br>' . $row['sep_no'] : '' ?><br><span class="badge badge-xs badge-<?= $color_status_ranap ?> blink"><?= $row['status_ranap_nm'] ?></span></td>
                            <td class="text-center" width="100"><?= $row['kelas_nm'] ?></td>
                            <td class="text-center" width="100"><?= $row['lokasi_asal'] ?></td>
                            <td class="text-center" width="100"><?= $row['lokasi_bangsal'] ?> <br> <?= $row['kamar_nm'] ?></td>
                            <td class="text-center" width="80"><?= to_date($row['tgl_registrasi'], '-', 'full_date') ?></td>
                            <td class="text-center" width="80"><?= to_date($row['tgl_pulang'], '-', 'full_date') ?></td>
                            <td class="text-center" width="50"><i class="fa <?= ($row['pulang_st'] == '1' ? 'fa-check-circle text-success' : 'fa-minus-circle text-danger blink') ?>"></i></td>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                    <?php endif; ?>
                  </table>
                </form>
              </div>
            </div>
          </div><!-- /.row -->
          <div class="row mt-2">
            <div class="col-6 text-middle">
              <div class="row">
                <div class="col">
                  <div class="row pagination-info">
                    <div class="col-md-8" style="padding-top:3px !important">
                      <form id="form-paging" action="<?= site_url() . '/app/per_page/' . $nav['nav_id'] ?>" method="post">
                        <label><i class="fas fa-bookmark"></i> Perhalaman</label>
                        <select name="per_page" class="select-pagination" onchange="$('#form-paging').submit()">
                          <option value="10" <?php if ($cookie['per_page'] == 10) {
                                                echo 'selected';
                                              } ?>>10</option>
                          <option value="50" <?php if ($cookie['per_page'] == 50) {
                                                echo 'selected';
                                              } ?>>50</option>
                          <option value="100" <?php if ($cookie['per_page'] == 100) {
                                                echo 'selected';
                                              } ?>>100</option>
                        </select>
                        <label class="ml-2"><?= @$pagination_info ?></label>
                      </form>
                    </div>
                  </div><!-- /.row -->
                </div>
              </div>
            </div>
            <div class="col-6 text-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>