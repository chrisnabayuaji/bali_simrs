<!-- js -->
<?php $this->load->view('pelayanan/registrasi_offline/_js_pasien')?>
<!-- / -->
<div class="table-responsive">
  <table id="pasien_table" class="table table-hover table-bordered table-striped table-sm w-100">
    <thead>
      <tr>
        <th class="text-center" width="20">No.</th>
        <th class="text-center" width="120">No. Rekam Medis</th>
        <th class="text-center">Nama Pasien</th>
        <th class="text-center">Alamat Pasien</th>
        <th class="text-center" width="10">JK</th>
        <th class="text-center" width="150">Tgl Lahir</th>
        <th class="text-center" width="100">Jenis Pasien</th>
        <th class="text-center" width="60">Aksi</th>
      </tr>
    </thead>
  </table>
</div>