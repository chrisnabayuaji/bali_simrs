<!-- js -->
<?php $this->load->view('pelayanan/registrasi_ranap/kunjungan/_js_pindah_bangsal') ?>
<!-- / -->
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?= $form_action ?>" autocomplete="off">
  <input type="hidden" name="regkamar_id" value="<?= @$main['regkamar_id'] ?>">
  <input type="hidden" name="reg_id" id="reg_id" value="<?= @$main['reg_id'] ?>">
  <input type="hidden" id="jns_rawat_cd" value="<?= @$reg_pasien['jns_rawat_cd'] ?>">
  <input type="hidden" name="pasien_id" value="<?= @$main['pasien_id'] ?>">
  <input type="hidden" name="kelas_id" id="kelas_id">
  <input type="hidden" name="nom_tarif" id="nom_tarif">
  <div class="row">
    <div class="col">
      <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-edit"></i> Form Pindah Bangsal</h4>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Dari Bangsal / Kamar</label>
        <div class="col-lg-4 col-md-4">
          <input type="text" class="form-control" value="<?= @$main['lokasi_nm'] ?>" disabled>
        </div>
        <div class="col-lg-4 col-md-4">
          <input type="text" class="form-control" value="<?= @$main['kamar_nm'] ?>" disabled>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Ke Bangsal / Kamar</label>
        <div class="col-lg-4 col-md-4">
          <select class="form-control chosen-select" name="lokasi_id" id="lokasi_bangsal_id" required="">
            <option value="">---</option>
            <?php foreach ($lokasi_bangsal as $r) : ?>
              <option value="<?= $r['lokasi_id'] ?>"><?= $r['lokasi_nm'] ?></option>
            <?php endforeach; ?>
          </select>
        </div>
        <div class="col-lg-4 col-md-4">
          <div id="box_kamar">
            <select class="form-control chosen-select" name="kamar_id" id="kamar_id" required="">
              <option value="">---</option>
            </select>
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Tgl. Jam Pindah</label>
        <div class="col-lg-5 col-md-5">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
            </div>
            <input type="text" class="form-control datetimepicker" name="tgl_pindah" id="tgl_pindah" value="<?= date('d-m-Y H:i:s') ?>" required>
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-4 col-md-4 col-form-label">Keterangan</label>
        <div class="col-lg-8 col-md-8">
          <textarea class="form-control" name="keterangan_kamar" rows="4"></textarea>
        </div>
      </div>
    </div>
    <div class="col">
      <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-procedures menu-icon"></i> Data Kamar Tersedia</h4>
      <div class="table-responsive">
        <table id="kamar_table" class="table table-hover table-bordered table-striped table-sm w-100">
          <thead>
            <tr>
              <th class="text-center" width="20">No.</th>
              <th class="text-center">Bangsal</th>
              <th class="text-center">Kamar</th>
              <th class="text-center" width="120">Jml.Bed Tersedia</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-10 offset-md-10">
        <button type="button" class="btn btn-xs btn-secondary btn-cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>