<div class="table-responsive">
  <table class="table table-bordered table-striped table-sm">
    <thead>
      <tr>
        <th class="text-center" width="30">No</th>
        <th class="text-center">Kamar</th>
        <th class="text-center" width="120">Jml Bed Tersedia</th>
      </tr>
    </thead>
    <?php if($main == null): ?>
      <tbody>
        <tr>
          <td class="text-center" colspan="99">Tidak ada data</td>
        </tr>
      </tbody>
    <?php else: ?>
      <tbody>
        <?php $i=1;foreach($main as $row): ?>          
          <tr>
            <td class="text-center"><?=$i++?></td>
            <td><?=$row['kamar_nm']?></td>
            <td class="text-center"><?=$row['jml_bed_kosong']?></td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    <?php endif; ?>
  </table>
</div>