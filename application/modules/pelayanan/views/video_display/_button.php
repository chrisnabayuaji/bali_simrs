<?php if (@$get_data['info_id'] !='' && @$get_data[$file_name] !=''): ?>
	<a href="<?=base_url()?>assets/videos/<?=@$get_data[$file_name]?>" target="_blank" class="btn btn-xs btn-primary"><i class="fas fa-play"></i> Putar Video</a>
	<a href="#" class="btn btn-xs btn-danger delete-video" data-name="<?=$file_name?>" data-info_id="<?=@$get_data['info_id']?>"><i class="fas fa-trash"></i> Hapus Video</a>
<?php else: ?>
	<div class="mt-n2"><small class="text-danger font-weight-semibold">Tidak ada video</small></div>
<?php endif; ?>

<?php if (@$get_data['info_id'] !='' && @$get_data[$file_name] !=''): ?>
<script type="text/javascript">
$(document).ready(function () {
  $('.delete-video').on('click', function (e) {
    e.preventDefault();

    const name = $(this).data('name');
    const info_id = $(this).data('info_id');

    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#eb3b5a',
      cancelButtonColor: '#b2bec3',
      confirmButtonText: 'Hapus',
      cancelButtonText: 'Batal',
      customClass: 'swal-wide'
    }).then((result) => {
      if (result.value) {
        DeleteVideo(name, info_id);
      }
    })
  })
  // delete video
  function DeleteVideo(name, info_id) {
    var res = name.substring(13, 11);
    $('#result'+res).html('');
    $.get('<?=site_url($nav['nav_url']."/ajax/delete_video")?>?name='+name+'&info_id='+info_id,null,function(data) {
      $('#result'+res).html('<div class="mt-n2"><small class="text-danger font-weight-semibold">Tidak ada video</small></div>');
      $.toast({
        heading: 'Sukses',
        text: 'Data berhasil dihapus.',
        icon: 'success',
        position: 'top-right'
      })
    },'json');
  }
});
</script>
<?php endif; ?>