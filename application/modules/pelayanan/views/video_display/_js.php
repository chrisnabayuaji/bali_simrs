<script type="text/javascript">
  $(document).ready(function () {
    $("#form-data").validate( {
      rules: {
        
      },
      messages: {
        
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container"));
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });
    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');
    //datepicker
    $('.datepicker').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })
  })
  //
  function _(el){
    return document.getElementById(el);
  }
  function bytesToSize(bytes) {
     var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
     if (bytes == 0) return '0 Byte';
     var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
     return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
  };
  function uploadFile(){
    var file_media_01 = document.getElementById("file_media_01").value;
    var file_media_02 = document.getElementById("file_media_02").value;
    var file_media_03 = document.getElementById("file_media_03").value;
    var file_media_04 = document.getElementById("file_media_04").value;
    var file_media_05 = document.getElementById("file_media_05").value;
    var info_id = document.getElementById("info_id").value;
    if (file_media_01 !='') {
      var file = _("file_media_01").files[0];
      var formdata = new FormData();
      formdata.append("file_media_01", file);
      var ajax = new XMLHttpRequest();
      ajax.upload.addEventListener("progress", progressHandler_01, false);
      ajax.addEventListener("load", completeHandler_01, false);
      ajax.addEventListener("error", errorHandler_01, false);
      ajax.addEventListener("abort", abortHandler_01, false);
      ajax.open("POST", "<?=site_url().'/'.$nav['nav_url']?>/upload_video/file_media_01/"+info_id);
      ajax.send(formdata);
    }else if (file_media_02 !='') {
      var file = _("file_media_02").files[0];
      var formdata = new FormData();
      formdata.append("file_media_02", file);
      var ajax = new XMLHttpRequest();
      ajax.upload.addEventListener("progress", progressHandler_02, false);
      ajax.addEventListener("load", completeHandler_02, false);
      ajax.addEventListener("error", errorHandler_02, false);
      ajax.addEventListener("abort", abortHandler_02, false);
      ajax.open("POST", "<?=site_url().'/'.$nav['nav_url']?>/upload_video/file_media_02/"+info_id);
      ajax.send(formdata);
    }else if (file_media_03 !='') {
      var file = _("file_media_03").files[0];
      var formdata = new FormData();
      formdata.append("file_media_03", file);
      var ajax = new XMLHttpRequest();
      ajax.upload.addEventListener("progress", progressHandler_03, false);
      ajax.addEventListener("load", completeHandler_03, false);
      ajax.addEventListener("error", errorHandler_03, false);
      ajax.addEventListener("abort", abortHandler_03, false);
      ajax.open("POST", "<?=site_url().'/'.$nav['nav_url']?>/upload_video/file_media_03/"+info_id);
      ajax.send(formdata);
    }else if (file_media_04 !='') {
      var file = _("file_media_04").files[0];
      var formdata = new FormData();
      formdata.append("file_media_04", file);
      var ajax = new XMLHttpRequest();
      ajax.upload.addEventListener("progress", progressHandler_04, false);
      ajax.addEventListener("load", completeHandler_04, false);
      ajax.addEventListener("error", errorHandler_04, false);
      ajax.addEventListener("abort", abortHandler_04, false);
      ajax.open("POST", "<?=site_url().'/'.$nav['nav_url']?>/upload_video/file_media_04/"+info_id);
      ajax.send(formdata);
    }else if (file_media_05 !='') {
      var file = _("file_media_05").files[0];
      var formdata = new FormData();
      formdata.append("file_media_05", file);
      var ajax = new XMLHttpRequest();
      ajax.upload.addEventListener("progress", progressHandler_05, false);
      ajax.addEventListener("load", completeHandler_05, false);
      ajax.addEventListener("error", errorHandler_05, false);
      ajax.addEventListener("abort", abortHandler_05, false);
      ajax.open("POST", "<?=site_url().'/'.$nav['nav_url']?>/upload_video/file_media_05/"+info_id);
      ajax.send(formdata);
    }else{
      Swal.fire({
        title: 'Anda belum memilih file',
        text: "Silahkan memilih file terlebih dahulu.",
        type: 'warning',
        confirmButtonColor: '#eb3b5a',
        confirmButtonText: 'Oke',
        customClass: 'swal-wide'
      })
    }
  }
  function progressHandler_01(event){
    _("result01").classList.add("d-none");
    _("progresShow01").classList.remove("d-none");
    disabledContent();
    _("loaded_n_total_01").innerHTML = "Mengupload "+bytesToSize(event.loaded)+" dari "+bytesToSize(event.total);
    var percent = (event.loaded / event.total) * 100;
    _("progressBar01").setAttribute("style", "width: "+Math.round(percent)+"%");
    _("status_01").innerHTML = Math.round(percent)+"% uploading...";
  }
  function progressHandler_02(event){
    _("result02").classList.add("d-none");
    _("progresShow02").classList.remove("d-none");
    disabledContent();
    _("loaded_n_total_02").innerHTML = "Mengupload "+bytesToSize(event.loaded)+" dari "+bytesToSize(event.total);
    var percent = (event.loaded / event.total) * 100;
    _("progressBar02").setAttribute("style", "width: "+Math.round(percent)+"%");
    _("status_02").innerHTML = Math.round(percent)+"% uploading...";
  }
  function progressHandler_03(event){
    _("result03").classList.add("d-none");
    _("progresShow03").classList.remove("d-none");
    disabledContent();
    _("loaded_n_total_03").innerHTML = "Mengupload "+bytesToSize(event.loaded)+" dari "+bytesToSize(event.total);
    var percent = (event.loaded / event.total) * 100;
    _("progressBar03").setAttribute("style", "width: "+Math.round(percent)+"%");
    _("status_03").innerHTML = Math.round(percent)+"% uploading...";
  }
  function progressHandler_04(event){
    _("result04").classList.add("d-none");
    _("progresShow04").classList.remove("d-none");
    disabledContent();
    _("loaded_n_total_04").innerHTML = "Mengupload "+bytesToSize(event.loaded)+" dari "+bytesToSize(event.total);
    var percent = (event.loaded / event.total) * 100;
    _("progressBar04").setAttribute("style", "width: "+Math.round(percent)+"%");
    _("status_04").innerHTML = Math.round(percent)+"% uploading...";
  }
  function progressHandler_05(event){
    _("result05").classList.add("d-none");
    _("progresShow05").classList.remove("d-none");
    disabledContent();
    _("loaded_n_total_05").innerHTML = "Mengupload "+bytesToSize(event.loaded)+" dari "+bytesToSize(event.total);
    var percent = (event.loaded / event.total) * 100;
    _("progressBar05").setAttribute("style", "width: "+Math.round(percent)+"%");
    _("status_05").innerHTML = Math.round(percent)+"% uploading...";
  }
  function completeHandler_01(event){
    var info_id = document.getElementById("info_id").value;
    get_video_01(info_id);
    _("status_01").innerHTML = event.target.responseText;
    setTimeout( function(){ 
      _("progresShow01").classList.add("d-none");
      _("progressBar01").setAttribute("style", "width: 0%");
      enabledContent();
    }, 1500);
  }
  function completeHandler_02(event){
    var info_id = document.getElementById("info_id").value;
    get_video_02(info_id);
    _("status_02").innerHTML = event.target.responseText;
    setTimeout( function(){ 
      _("progresShow02").classList.add("d-none");
      _("progressBar02").setAttribute("style", "width: 0%");
      enabledContent();
    }, 1500);
  }
  function completeHandler_03(event){
    var info_id = document.getElementById("info_id").value;
    get_video_03(info_id);
    _("status_03").innerHTML = event.target.responseText;
    setTimeout( function(){ 
      _("progresShow03").classList.add("d-none");
      _("progressBar03").setAttribute("style", "width: 0%");
      enabledContent();
    }, 1500);
  }
  function completeHandler_04(event){
    var info_id = document.getElementById("info_id").value;
    get_video_04(info_id);
    _("status_04").innerHTML = event.target.responseText;
    setTimeout( function(){ 
      _("progresShow04").classList.add("d-none");
      _("progressBar04").setAttribute("style", "width: 0%");
      enabledContent();
    }, 1500);
  }
  function completeHandler_05(event){
    var info_id = document.getElementById("info_id").value;
    get_video_05(info_id);
    _("status_05").innerHTML = event.target.responseText;
    setTimeout( function(){ 
      _("progresShow05").classList.add("d-none");
      _("progressBar05").setAttribute("style", "width: 0%");
      enabledContent();
    }, 1500);
  }

  function errorHandler_01(event){
    _("status_01").innerHTML = "Upload Failed";
  }
  function errorHandler_02(event){
    _("status_02").innerHTML = "Upload Failed";
  }
  function errorHandler_03(event){
    _("status_03").innerHTML = "Upload Failed";
  }
  function errorHandler_04(event){
    _("status_04").innerHTML = "Upload Failed";
  }
  function errorHandler_05(event){
    _("status_05").innerHTML = "Upload Failed";
  }

  function abortHandler_01(event){
    _("status_01").innerHTML = "Upload Aborted";
  }
  function abortHandler_02(event){
    _("status_02").innerHTML = "Upload Aborted";
  }
  function abortHandler_03(event){
    _("status_03").innerHTML = "Upload Aborted";
  }
  function abortHandler_04(event){
    _("status_04").innerHTML = "Upload Aborted";
  }
  function abortHandler_05(event){
    _("status_05").innerHTML = "Upload Aborted";
  }

  function disabledContent(){
    _("uploadVideo01").disabled = true;
    _("file_media_01").disabled = true;
    _("uploadVideo02").disabled = true;
    _("file_media_02").disabled = true;
    _("uploadVideo03").disabled = true;
    _("file_media_03").disabled = true;
    _("uploadVideo04").disabled = true;
    _("file_media_04").disabled = true;
    _("uploadVideo05").disabled = true;
    _("file_media_05").disabled = true;
  }

  function enabledContent(){
    _("file_media_01").disabled = false;
    _("file_media_01").value = "";
    _("uploadVideo01").disabled = false;
    _("file_media_02").disabled = false;
    _("file_media_02").value = "";
    _("uploadVideo02").disabled = false;
    _("file_media_03").disabled = false;
    _("file_media_03").value = "";
    _("uploadVideo03").disabled = false;
    _("file_media_04").disabled = false;
    _("file_media_04").value = "";
    _("uploadVideo04").disabled = false;
    _("file_media_05").disabled = false;
    _("file_media_05").value = "";
    _("uploadVideo05").disabled = false;
  }
  //
  get_video_01('<?=@$id?>');
  function get_video_01(id) {
    $('#result01').removeClass('d-none');
    $.get('<?=site_url().'/'.$nav['nav_url']?>/ajax/get_video_01?id='+id,null,function(data) {
      $('#result01').html(data.html);
    },'json');
  }

  get_video_02('<?=@$id?>');
  function get_video_02(id) {
    $('#result02').removeClass('d-none');
    $.get('<?=site_url().'/'.$nav['nav_url']?>/ajax/get_video_02?id='+id,null,function(data) {
      $('#result02').html(data.html);
    },'json');
  }

  get_video_03('<?=@$id?>');
  function get_video_03(id) {
    $('#result03').removeClass('d-none');
    $.get('<?=site_url().'/'.$nav['nav_url']?>/ajax/get_video_03?id='+id,null,function(data) {
      $('#result03').html(data.html);
    },'json');
  }

  get_video_04('<?=@$id?>');
  function get_video_04(id) {
    $('#result04').removeClass('d-none');
    $.get('<?=site_url().'/'.$nav['nav_url']?>/ajax/get_video_04?id='+id,null,function(data) {
      $('#result04').html(data.html);
    },'json');
  }

  get_video_05('<?=@$id?>');
  function get_video_05(id) {
    $('#result05').removeClass('d-none');
    $.get('<?=site_url().'/'.$nav['nav_url']?>/ajax/get_video_05?id='+id,null,function(data) {
      $('#result05').html(data.html);
    },'json');
  }
</script>