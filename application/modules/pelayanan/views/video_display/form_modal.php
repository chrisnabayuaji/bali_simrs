<!-- js -->
<?php $this->load->view('pelayanan/video_display/_js') ?>
<!-- / -->
<form role="form" id="form-data" method="post" action="<?= $form_action ?>" autocomplete="off" enctype="multipart/form-data">
  <div class="row">
    <div class="col-md-6">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Nama Info</label>
        <div class="col-lg-9 col-md-9">
          <input type="hidden" name="info_id" id="info_id" value="<?= @$id ?>">
          <input type="text" class="form-control" name="info_nm" value="<?= @$main['info_nm'] ?>">
        </div>
      </div>
      <div class="form-group row mb-2">
        <label class="col-lg-3 col-md-3 col-form-label">Informasi Loket</label>
        <div class="col-lg-9 col-md-9">
          <textarea class="form-control" rows="3" name="info_txt_loket" style="line-height: 1.5!important"><?= @$main['info_txt_loket'] ?></textarea>
        </div>
      </div>
      <div class="form-group row mb-2">
        <label class="col-lg-3 col-md-3 col-form-label">Informasi TV</label>
        <div class="col-lg-9 col-md-9">
          <textarea class="form-control" rows="3" name="info_txt_tv" style="line-height: 1.5!important"><?= @$main['info_txt_tv'] ?></textarea>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Periode Tanggal</label>
        <div class="col-lg-8 col-md-8">
          <div class="form-inline">
            <div class="input-group" style="width: 40%">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
              </div>
              <input type="text" class="form-control datepicker text-center" name="periode_tgl_awal" value="<?= (@$main['periode_tgl_awal'] == '0000-00-00') ? date('d-m-Y') : to_date(@$main['periode_tgl_awal']) ?>">
            </div>
            <div class="text-center" style="width: 10%">s.d</div>
            <div class="input-group" style="width: 40%">
              <div class="input-group-prepend">
                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
              </div>
              <input type="text" class="form-control datepicker text-center" name="periode_tgl_akhir" value="<?= (@$main['periode_tgl_akhir'] == '0000-00-00') ? date('d-m-Y') : to_date(@$main['periode_tgl_akhir']) ?>">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Upload Video 1</label>
        <div class="col-lg-6 col-md-6">
          <input type="file" class="form-control" name="file_media_01" id="file_media_01">
          <div class="mt-1 mb-3">
            <div class="pt-2 d-none" id="progresShow01">
              <div class="progress">
                <div class="progress-bar pl-1" id="progressBar01" role="progressbar01" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
                  <span id="status_01"></span>
                </div>
              </div>
              <div class="text-small" id="loaded_n_total_01"></div>
            </div>
            <div id="result01"></div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3">
          <button type="button" class="btn btn-primary btn-xs" id="uploadVideo01" onclick="uploadFile()"><i class="fas fa-upload"></i> Upload Video</button>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Upload Video 2</label>
        <div class="col-lg-6 col-md-6">
          <input type="file" class="form-control" name="file_media_02" id="file_media_02">
          <div class="mt-1 mb-3">
            <div class="pt-2 d-none" id="progresShow02">
              <div class="progress">
                <div class="progress-bar pl-1" id="progressBar02" role="progressbar02" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
                  <span id="status_02"></span>
                </div>
              </div>
              <div class="text-small" id="loaded_n_total_02"></div>
            </div>
            <div id="result02"></div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3">
          <button type="button" class="btn btn-primary btn-xs" id="uploadVideo02" onclick="uploadFile()"><i class="fas fa-upload"></i> Upload Video</button>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Upload Video 3</label>
        <div class="col-lg-6 col-md-6">
          <input type="file" class="form-control" name="file_media_03" id="file_media_03">
          <div class="mt-1 mb-3">
            <div class="pt-2 d-none" id="progresShow03">
              <div class="progress">
                <div class="progress-bar pl-1" id="progressBar03" role="progressbar03" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
                  <span id="status_03"></span>
                </div>
              </div>
              <div class="text-small" id="loaded_n_total_03"></div>
            </div>
            <div id="result03"></div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3">
          <button type="button" class="btn btn-primary btn-xs" id="uploadVideo03" onclick="uploadFile()"><i class="fas fa-upload"></i> Upload Video</button>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Upload Video 4</label>
        <div class="col-lg-6 col-md-6">
          <input type="file" class="form-control" name="file_media_04" id="file_media_04">
          <div class="mt-1 mb-3">
            <div class="pt-2 d-none" id="progresShow04">
              <div class="progress">
                <div class="progress-bar pl-1" id="progressBar04" role="progressbar04" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
                  <span id="status_04"></span>
                </div>
              </div>
              <div class="text-small" id="loaded_n_total_04"></div>
            </div>
            <div id="result04"></div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3">
          <button type="button" class="btn btn-primary btn-xs" id="uploadVideo04" onclick="uploadFile()"><i class="fas fa-upload"></i> Upload Video</button>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Upload Video 5</label>
        <div class="col-lg-6 col-md-6">
          <input type="file" class="form-control" name="file_media_05" id="file_media_05">
          <div class="mt-1 mb-3">
            <div class="pt-2 d-none" id="progresShow05">
              <div class="progress">
                <div class="progress-bar pl-1" id="progressBar05" role="progressbar05" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">
                  <span id="status_05"></span>
                </div>
              </div>
              <div class="text-small" id="loaded_n_total_05"></div>
            </div>
            <div id="result05"></div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3">
          <button type="button" class="btn btn-primary btn-xs" id="uploadVideo05" onclick="uploadFile()"><i class="fas fa-upload"></i> Upload Video</button>
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-10 offset-md-10">
        <button type="button" class="btn btn-xs btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>