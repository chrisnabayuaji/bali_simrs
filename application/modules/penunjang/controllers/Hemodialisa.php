<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Hemodialisa extends MY_Controller
{

	var $nav_id = '04.04.01', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'hemodialisa/m_hemodialisa',
			'hemodialisa/m_dt_bhp',
			'hemodialisa/m_dt_alkes',
			'hemodialisa/m_dt_tarifkelas',
			'hemodialisa/m_dt_petugas',
			'master/m_lokasi',
			'master/m_wilayah',
			'master/m_kelas',
			'master/m_jenis_pasien',
			'master/m_pegawai',
			'm_hemodialisa',
			'pelayanan/m_dt_mst_pasien',
			'pelayanan/m_registrasi_offline',
			'pelayanan/m_caller_antrian',
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id);
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('tgl_order_from' => '', 'tgl_order_to' => '', 'no_rm_nm' => '', 'lokasi_id' => '', 'jenispasien_id' => '', 'periksa_st' => '', 'pulang_st' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'tgl_order', 'type' => 'desc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(5, 0);
		$this->cookie['total_rows'] = $this->m_hemodialisa->all_rows($this->cookie);
		set_cookie_nav($this->nav_id, $this->cookie);
		//main data
		$data['nav'] = $this->nav;
		$data['cookie'] = $this->cookie;
		$data['main'] = $this->m_hemodialisa->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
		$data['lokasi'] = $this->m_lokasi->by_field('jenisreg_st', 1, 'result');
		$data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
		//set pagination
		$nav['nav_url'] = 'penunjang/hemodialisa';
		set_pagination($nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('penunjang/hemodialisa/index', $data);
	}

	public function periksa($reg_id = null, $pemeriksaan_id = null)
	{
		$this->authorize($this->nav, ($reg_id != '' && $pemeriksaan_id != '') ? '_update' : '_add');

		if ($reg_id == null && $pemeriksaan_id == null) {
			$data['main'] = array();
			$data['alergi'] = array();
		} else {
			$this->m_hemodialisa->update_periksa_st($pemeriksaan_id);
			$data['main'] = $this->m_hemodialisa->get_data_periksa($pemeriksaan_id);
			$data['alergi'] = $this->m_hemodialisa->get_alergi($data['main']['pasien_id']);
		}
		$data['reg_id'] = $reg_id;
		$data['pemeriksaan_id'] = $pemeriksaan_id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save/' . $reg_id . '/' . $pemeriksaan_id;

		$this->render('penunjang/hemodialisa/periksa', $data);
	}

	function ajax($type = null, $id = null)
	{
		if ($type == 'view_tab_menu') {
			$view_name = $this->input->post('view_name');
			$id = $this->input->post('reg_id');
			$data['nav'] = $this->nav;
			$data['reg'] = $this->m_hemodialisa->get_data($id);

			// Tindak Lanjut
			if ($view_name == 'tindak_lanjut') {
				$data['lokasi_jenisreg_1'] = $this->m_lokasi->by_field('jenisreg_st', 1, 'result');
				$data['lokasi_jenisreg_2'] = $this->m_lokasi->by_field('jenisreg_st', 2, 'result');
				$data['rujukan'] = $this->m_rsrujukan->all_data();
			}

			echo json_encode(array(
				'html' => $this->load->view('penunjang/hemodialisa/' . $view_name, $data, true)
			));
		}
	}

	// Tindakan
	public function ajax_tindakan($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_hemodialisa->tindakan_save();
		} elseif ($type == 'tarifkelas_autocomplete') {
			$tarif_nm = $this->input->get('tarif_nm');
			$res = $this->m_hemodialisa->tarifkelas_autocomplete($tarif_nm);
			echo json_encode($res);
		} elseif ($type == 'petugas_autocomplete') {
			$petugas_nm = $this->input->get('petugas_nm');
			$res = $this->m_hemodialisa->petugas_autocomplete($petugas_nm);
			echo json_encode($res);
		} elseif ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_hemodialisa->tindakan_data($pasien_id, $reg_id, $lokasi_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/hemodialisa/tindakan_data', $data, true)
			));
		} elseif ($type == 'get_data') {
			$tindakan_id = $this->input->post('tindakan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');

			$main = $this->m_hemodialisa->tindakan_get($tindakan_id, $reg_id, $pasien_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$tindakan_id = $this->input->post('tindakan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_hemodialisa->tindakan_delete($tindakan_id, $reg_id, $pasien_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_hemodialisa->tindakan_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/hemodialisa/tindakan_data', $data, true)
			));
		} elseif ($type == 'search_tarifkelas') {
			$data['nav'] = $this->nav;
			$data['kelas_id'] = $id;

			echo json_encode(array(
				'html' => $this->load->view('penunjang/hemodialisa/search_tarifkelas', $data, true)
			));
		} elseif ($type == 'search_tarifkelas_data') {
			$list = $this->m_dt_tarifkelas->get_datatables($id);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['tarifkelas_id'];
				$row[] = $field['tarif_nm'];
				$row[] = $field['kelas_nm'];
				$row[] = num_id($field['nominal']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="tarifkelas_fill(' . "'" . $field['tarifkelas_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_tarifkelas->count_all($id),
				"recordsFiltered" => $this->m_dt_tarifkelas->count_filtered($id),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'tarifkelas_fill') {
			$data = $this->input->post();
			$res = $this->m_hemodialisa->tarifkelas_row($data['tarifkelas_id']);
			echo json_encode($res);
		} elseif ($type == 'search_petugas') {
			$data['nav'] = $this->nav;
			$data['form_name'] = @$id;

			echo json_encode(array(
				'html' => $this->load->view('penunjang/hemodialisa/search_petugas', $data, true)
			));
		} elseif ($type == 'search_petugas_data') {
			$list = $this->m_dt_petugas->get_datatables();
			$form_name = $this->input->post('form_name');
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['pegawai_id'];
				$row[] = $field['pegawai_nm'];
				$row[] = $field['pegawai_nip'];
				$row[] = get_parameter_value('jenispegawai_cd', $field['jenispegawai_cd']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="petugas_fill(' . "'" . $field['pegawai_id'] . "'" . ',' . "'" . $form_name . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_petugas->count_all(),
				"recordsFiltered" => $this->m_dt_petugas->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'petugas_fill') {
			$data = $this->input->post();
			$res = $this->m_hemodialisa->petugas_row($data['pegawai_id']);
			echo json_encode($res);
		} else if ($type == 'add_petugas') {
			$tindakan_id = $this->input->get('tindakan_id');
			$data['nav'] = $this->nav;

			$html = '';
			if ($tindakan_id != '') {
				$petugas_no = $this->input->get('petugas_no');
				$get_tindakan = $this->m_hemodialisa->get_tindakan($tindakan_id);
				$data['tindakan_id'] = $tindakan_id;
				$data['petugas_no_post'] = $petugas_no;
				$data['type'] = 'edit';

				if ($get_tindakan['petugas_id'] != '') {
					$data['petugas_no'] = '';
					$data['form_name'] = 'petugas_id';
					$data['pegawai_id'] = $get_tindakan['petugas_id'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_1'];

					$html .= $this->load->view('penunjang/hemodialisa/add_petugas', $data, true);
				}
				if ($get_tindakan['petugas_id_2'] != '') {
					$data['petugas_no'] = '2';
					$data['form_name'] = 'petugas_id_2';
					$data['pegawai_id'] = $get_tindakan['petugas_id_2'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_2'];

					$html .= $this->load->view('penunjang/hemodialisa/add_petugas', $data, true);
				}
				if ($get_tindakan['petugas_id_3'] != '') {
					$data['petugas_no'] = '3';
					$data['form_name'] = 'petugas_id_3';
					$data['pegawai_id'] = $get_tindakan['petugas_id_3'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_3'];

					$html .= $this->load->view('penunjang/hemodialisa/add_petugas', $data, true);
				}
				if ($get_tindakan['petugas_id_4'] != '') {
					$data['petugas_no'] = '4';
					$data['form_name'] = 'petugas_id_4';
					$data['pegawai_id'] = $get_tindakan['petugas_id_4'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_4'];

					$html .= $this->load->view('penunjang/hemodialisa/add_petugas', $data, true);
				}
				if ($get_tindakan['petugas_id_5'] != '') {
					$data['petugas_no'] = '5';
					$data['form_name'] = 'petugas_id_5';
					$data['pegawai_id'] = $get_tindakan['petugas_id_5'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_5'];

					$html .= $this->load->view('penunjang/hemodialisa/add_petugas', $data, true);
				}
			} else {
				$petugas_no = $this->input->get('petugas_no') + 1;
				$data['petugas_no'] = ($petugas_no == '1') ? '' : $petugas_no;
				$data['form_name'] = ($petugas_no == '1') ? 'petugas_id' : 'petugas_id_' . $petugas_no;
				$data['petugas_no_post'] = $petugas_no;
				$data['type'] = 'add';

				$html .= $this->load->view('penunjang/hemodialisa/add_petugas', $data, true);
			}

			echo json_encode(array(
				'html' => $html,
				'petugas_no' => $petugas_no,
			));
		} elseif ($type == 'delete_petugas') {
			$tindakan_id = $this->input->post('tindakan_id');
			$form_name = $this->input->post('form_name');
			//
			$callback = 'true';
			//
			echo json_encode(array(
				'callback' => $callback
			));
		}
	}

	// Pemberian BHP
	public function ajax_bhp($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_hemodialisa->bhp_save();
		} elseif ($type == 'autocomplete') {
			$obat_nm = $this->input->get('obat_nm');
			$map_lokasi_depo = $this->input->get('map_lokasi_depo');
			$res = $this->m_poliklinik->bhp_autocomplete($obat_nm, $map_lokasi_depo);
			echo json_encode($res);
		} elseif ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_hemodialisa->bhp_data($reg_id, $lokasi_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/hemodialisa/bhp_data', $data, true)
			));
		} elseif ($type == 'get_data') {
			$bhp_id = $this->input->post('bhp_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_hemodialisa->bhp_get($bhp_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$bhp_id = $this->input->post('bhp_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_hemodialisa->bhp_delete($bhp_id, $reg_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_hemodialisa->bhp_data($reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/hemodialisa/bhp_data', $data, true)
			));
		} elseif ($type == 'search_bhp') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('penunjang/hemodialisa/search_bhp', $data, true)
			));
		} elseif ($type == 'search_data') {
			$map_lokasi_depo = $this->input->post('map_lokasi_depo');
			$list = $this->m_dt_bhp->get_datatables($map_lokasi_depo);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['no_batch'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = to_date($field['tgl_expired'], '-', 'date', ' ');
				$row[] = num_id($field['stok_akhir']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="bhp_fill(' . "'" . $field['obat_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_bhp->count_all($map_lokasi_depo),
				"recordsFiltered" => $this->m_dt_bhp->count_filtered($map_lokasi_depo),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'bhp_fill') {
			$data = $this->input->post();
			$res = $this->m_hemodialisa->bhp_row($data['barang_id']);
			echo json_encode($res);
		}
	}

	// Pemberian Alkes
	public function ajax_alkes($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_hemodialisa->alkes_save();
		} elseif ($type == 'autocomplete') {
			$obat_nm = $this->input->get('obat_nm');
			$map_lokasi_depo = $this->input->get('map_lokasi_depo');
			$res = $this->m_hemodialisa->alkes_autocomplete($obat_nm, $map_lokasi_depo);
			echo json_encode($res);
		} elseif ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_hemodialisa->alkes_data($reg_id, $lokasi_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/hemodialisa/alkes_data', $data, true)
			));
		} elseif ($type == 'get_data') {
			$alkes_id = $this->input->post('alkes_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_hemodialisa->alkes_get($alkes_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$alkes_id = $this->input->post('alkes_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_hemodialisa->alkes_delete($alkes_id, $reg_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_hemodialisa->alkes_data($reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/hemodialisa/alkes_data', $data, true)
			));
		} elseif ($type == 'search_alkes') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('penunjang/hemodialisa/search_alkes', $data, true)
			));
		} elseif ($type == 'search_data') {
			$map_lokasi_depo = $this->input->post('map_lokasi_depo');
			$list = $this->m_dt_alkes->get_datatables($map_lokasi_depo);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['no_batch'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = to_date($field['tgl_expired'], '-', 'date', ' ');
				$row[] = num_id($field['stok_akhir']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="alkes_fill(' . "'" . $field['obat_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_alkes->count_all($map_lokasi_depo),
				"recordsFiltered" => $this->m_dt_alkes->count_filtered($map_lokasi_depo),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'alkes_fill') {
			$data = $this->input->post();
			$res = $this->m_hemodialisa->alkes_row($data['barang_id']);
			echo json_encode($res);
		}
	}
}
