<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Datang extends MY_Controller
{

	var $nav_id = '04.01.01', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'laboratorium/m_lab_datang',
			'laboratorium/m_dt_bhp',
			'laboratorium/m_dt_alkes',
			'master/m_lokasi',
			'master/m_wilayah',
			'master/m_kelas',
			'master/m_jenis_pasien',
			'master/m_pegawai',
			'm_lab_datang',
			'pelayanan/m_dt_mst_pasien',
			'pelayanan/m_registrasi_offline',
			'pelayanan/m_caller_antrian',
			'pelayanan/m_dt_laboratorium',
			'app/m_profile'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id . '.datang');
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('tgl_registrasi_from' => '', 'tgl_registrasi_to' => '', 'no_rm_nm' => '', 'lokasi_id' => '', 'jenispasien_id' => '', 'periksa_st' => '', 'pulang_st' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'tgl_registrasi', 'type' => 'desc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(5, 0);
		$this->cookie['total_rows'] = $this->m_lab_datang->all_rows($this->cookie);
		set_cookie_nav($this->nav_id . '.datang', $this->cookie);
		//main data
		$data['nav'] = $this->nav;
		$data['cookie'] = $this->cookie;
		$data['main'] = $this->m_lab_datang->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
		$data['lokasi'] = $this->m_lokasi->by_field('jenisreg_st', 1, 'result');
		$data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
		//set pagination
		$nav['nav_url'] = 'penunjang/laboratorium/datang';
		set_pagination($nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('penunjang/laboratorium/datang/index', $data);
	}

	public function form($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
			$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_profile();
		} else {
			$data['main'] = $this->m_lab_datang->get_data($id);
			if ($data['main']['wilayah_st'] == 'L') {
				$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_parent('');
			} else {
				$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_profile();
			}
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['kelas'] = $this->m_kelas->all_data();
		$data['lokasi'] = $this->m_lokasi->by_field('lokasi_id', '03.02', 'result');
		$data['dokter'] = $this->m_pegawai->by_field('jenispegawai_cd', '02', 'result');
		$data['petugas'] = $this->m_pegawai->all_data();
		$data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
		$data['form_action'] = site_url() . '/penunjang/laboratorium/datang/save/' . $id;

		$this->render('penunjang/laboratorium/datang/form', $data);
	}

	public function save($id = null)
	{
		$reg_id = $this->m_lab_datang->save($id);
		$mainlab = $this->m_lab_datang->laboratorium_get('reg_id', $reg_id);

		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($id != '') ? '_update' : '_add', $this->nav_id);
		redirect(site_url() . '/penunjang/laboratorium/datang/order/' . $reg_id . '/' . $mainlab['pemeriksaan_id']);
	}

	public function periksa($reg_id = null, $pemeriksaan_id = null)
	{
		$this->authorize($this->nav, ($reg_id != '' && $pemeriksaan_id != '') ? '_update' : '_add');

		if ($reg_id == null && $pemeriksaan_id == null) {
			$data['main'] = array();
			$data['alergi'] = array();
		} else {
			$this->m_lab_datang->update_periksa_st($pemeriksaan_id);
			$data['main'] = $this->m_lab_datang->get_data_periksa($pemeriksaan_id);
			$data['alergi'] = $this->m_lab_datang->get_alergi($data['main']['pasien_id']);
		}
		$data['reg_id'] = $reg_id;
		$data['pemeriksaan_id'] = $pemeriksaan_id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save/' . $reg_id . '/' . $pemeriksaan_id;

		$this->render('penunjang/laboratorium/datang/periksa', $data);
	}

	public function delete($id = null, $pemeriksaan_id = null)
	{
		$this->m_lab_datang->delete($id, $pemeriksaan_id, true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log('_delete', $this->nav_id);
		redirect(site_url() . '/penunjang/laboratorium/datang/index/' . $this->cookie['cur_page']);
	}

	public function multiple($type = null)
	{
		$data = $this->input->post();
		if (isset($data['checkitem'])) {
			foreach ($data['checkitem'] as $key) {
				switch ($type) {
					case 'delete':
						$this->authorize($this->nav, '_delete');
						$this->m_lab_datang->delete($key, true);
						$flash = 'Data berhasil dihapus.';
						create_log('_delete', $this->nav_id);
						break;

					case 'enable':
						$this->authorize($this->nav, '_update');
						$this->m_lab_datang->update($key, array('is_active' => 1));
						$flash = 'Data berhasil diaktifkan.';
						create_log('_update', $this->nav_id);
						break;

					case 'disable':
						$this->authorize($this->nav, '_update');
						$this->m_lab_datang->update($key, array('is_active' => 0));
						$flash = 'Data berhasil dinonaktifkan.';
						create_log('_delete', $this->nav_id);
						break;
				}
			}
		}
		$this->session->set_flashdata('flash_success', $flash);
		redirect(site_url() . '/penunjang/laboratorium/datang/index/' . $this->cookie['cur_page']);
	}

	function ajax($type = null, $id = null)
	{
		if ($type == 'cek_id') {
			$data = $this->input->post();
			$cek = $this->m_lab_datang->get_data($data['lokasi_id']);
			if ($id == null) {
				if ($cek == null) {
					echo 'true';
				} else {
					echo 'false';
				}
			} else {
				if ($id != $data['lokasi_id'] && $cek != null) {
					echo 'false';
				} else {
					echo 'true';
				}
			}
		}

		if ($type == 'get_antrian_cd') {
			$data = $this->input->post();
			$res = $this->m_lokasi->get_data($data['lokasi_id']);
			echo json_encode($res);
		}

		if ($type == 'online_fill') {
			$data = $this->input->post();
			$res = $this->m_lab_datang->online_row($data['regonline_id']);
			echo json_encode($res);
		}

		if ($type == 'pasien_fill') {
			$data = $this->input->post();
			$res = $this->m_lab_datang->pasien_row($data['pasien_id']);
			echo json_encode($res);
		}

		if ($type == 'no_rm_fill') {
			$data = $this->input->post();
			$res = $this->m_lab_datang->no_rm_row($data['pasien_id']);
			echo json_encode($res);
		}

		if ($type == 'status_antrian') {
			$data = $this->input->post();
			$res = $this->m_caller_antrian->get_data($data['lokasi_id']);
			echo json_encode($res);
		}

		if ($type == 'call_antrian') {
			$data = $this->input->post();
			$res = $this->m_caller_antrian->call_antrian($data);
			echo json_encode($res);
		}

		if ($type == 'view_tab_menu') {
			$view_name = $this->input->post('view_name');
			$data['reg_id'] = $this->input->post('reg_id');
			$data['pemeriksaan_id'] = $this->input->post('pemeriksaan_id');
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_lab_datang->get_data_periksa($data['pemeriksaan_id']);

			if ($view_name == 'hasil_pemeriksaan') {
				$data['dokter'] = $this->m_pegawai->by_field('spesialisasi_cd', '48', 'result'); // Ambil Dokter Laboratorium
				$data['petugas'] = $this->m_pegawai->all_data(); // Ambil Petugas
				$data['get_tgl_hasil'] = $this->m_lab_datang->get_tgl_hasil($data['pemeriksaan_id']);
			}

			echo json_encode(array(
				'html' => $this->load->view('penunjang/laboratorium/datang/' . $view_name, $data, true)
			));
		}
	}

	public function online_modal()
	{
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('penunjang/laboratorium/datang/online_modal', $data, true)
		));
	}

	public function get_data_online()
	{
		$list = $this->m_lab_datang->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field['tgl_periksa'];
			$row[] = $field['pasien_id'];
			$row[] = $field['pasien_nm'];
			$row[] = $field['alamat'];
			$row[] = $field['sex_cd'];
			$row[] = $field['tmp_lahir'] . ', ' . @to_date($field['tgl_lahir']);
			$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="online_fill(' . "'" . $field['regonline_id'] . "'" . ')">Pilih >></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_lab_datang->count_all(),
			"recordsFiltered" => $this->m_lab_datang->count_filtered(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function pasien_modal()
	{
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('penunjang/laboratorium/datang/pasien_modal', $data, true)
		));
	}

	public function get_data_pasien()
	{
		$list = $this->m_dt_mst_pasien->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field['pasien_id'];
			$row[] = $field['pasien_nm'];
			$row[] = $field['alamat'];
			$row[] = $field['sex_cd'];
			$row[] = $field['tmp_lahir'] . ', ' . @to_date($field['tgl_lahir']);
			$row[] = $field['jenispasien_nm'];
			$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="pasien_fill(' . "'" . $field['pasien_id'] . "'" . ')">Pilih >></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_dt_mst_pasien->count_all(),
			"recordsFiltered" => $this->m_dt_mst_pasien->count_filtered(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function caller_modal()
	{
		$data['nav'] = $this->nav;
		$data['caller'] = $this->m_lab_datang->get_caller();

		echo json_encode(array(
			'html' => $this->load->view('penunjang/laboratorium/datang/caller_modal', $data, true)
		));
	}

	public function get_nik($nik)
	{
		$this->load->library('xmlrpc');
		$this->load->library('xmlrpcs');
		$url = "https://ayokitakerja.kemnaker.go.id/tools/check_nik/" . $nik;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		$data = curl_exec($ch);

		if (curl_errno($ch)) {
			$res = array('STATUS' => -1);
			echo json_encode($res);
		} else {
			$data = json_decode($data, TRUE);
			curl_close($ch);
			echo json_encode($data);
		}
	}

	public function order($reg_id, $pemeriksaan_id = null)
	{
		$data['nav'] = $this->nav;
		$data['reg_id'] = $reg_id;
		$data['pemeriksaan_id'] = $pemeriksaan_id;
		$data['reg'] = $this->m_registrasi_offline->get_data($reg_id);
		$data['mainlab'] = $this->m_lab_datang->laboratorium_get($pemeriksaan_id);
		$data['form_action'] = site_url('penunjang/laboratorium/datang/order_save/' . $reg_id . '/' . $pemeriksaan_id);

		$this->render('penunjang/laboratorium/datang/order', $data);
	}

	public function order_save($reg_id = null, $id = null)
	{
		$this->m_lab_datang->laboratorium_save($reg_id, $id);
		redirect(site_url('penunjang/laboratorium/datang'));
	}

	public function ajax_penunjang_laboratorium($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'modal') {
			$data['reg'] = $this->m_lab_datang->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainlab'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_laboratorium/save/';
			} else {
				$data['mainlab'] = $this->m_lab_datang->laboratorium_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_laboratorium/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('penunjang/laboratorium/datang/laboratorium_modal', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['reg'] = $this->m_lab_datang->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainlab'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_laboratorium/save/';
			} else {
				$data['mainlab'] = $this->m_lab_datang->laboratorium_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_laboratorium/save/' . $id;
			}

			$data['rinc'] = $this->m_lab_datang->laboratorium_detail($reg_id, $id);

			echo json_encode(array(
				'html' => $this->load->view('penunjang/laboratorium/datang/laboratorium_detail', $data, true)
			));
		}

		if ($type == 'search_data') {

			$list = $this->m_dt_laboratorium->get_datatables($id);
			$no = 0;
			$data = array();
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				if ($field['parent_id'] != '') {
					$dash = '--';
				} else {
					$dash = '';
				};
				$row[] = $field['itemlab_id'];
				$row[] = $dash . $field['itemlab_nm'];
				if ($field['parent_id'] != '') {
					if (@$field['pemeriksaanrinc_id'] != null) {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemlab_id'].'" checked="true">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="' . $field['itemlab_id'] . '" checked="true">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>';
					} else {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemlab_id'].'">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="' . $field['itemlab_id'] . '">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>';
					}
				} else {
					$row[] = '';
				}

				$data[] = $row;
			}

			$output = array(
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'save') {
			$this->m_lab_datang->laboratorium_save(@$reg_id, @$id);
		}

		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_lab_datang->laboratorium_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/laboratorium/datang/laboratorium_data', $data, true)
			));
		}

		if ($type == 'delete_data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_lab_datang->laboratorium_delete($pemeriksaan_id, $reg_id, $pasien_id, $lokasi_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_lab_datang->laboratorium_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/laboratorium/datang/laboratorium_data', $data, true)
			));
		}
	}

	public function save_hasil_pemeriksaan($reg_id = null, $pemeriksaan_id = null)
	{
		$this->m_lab_datang->save_hasil_pemeriksaan($reg_id, $pemeriksaan_id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		redirect(site_url() . '/penunjang/laboratorium/datang/periksa/' . $reg_id . '/' . $pemeriksaan_id . '#hasil-pemeriksaan');
	}

	// Hasil Pemeriksaan
	public function ajax_hasil_pemeriksaan($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'hasil_lab') {
			$data = $this->input->post();
			$data['hasil_lab'] = clear_numeric($data['hasil_lab']);
			$result = $this->m_lab_datang->hasil_lab($data);
			echo json_encode(array('hasil' => $result));
		}

		if ($type == 'data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_lab_datang->hasil_pemeriksaan_data($pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/laboratorium/datang/hasil_pemeriksaan_data', $data, true)
			));
		}

		if ($type == 'modal') {
			$data['reg'] = $this->m_lab_datang->get_data_by_reg_id($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;

			$data['mainlab'] = $this->m_lab_datang->laboratorium_get($id);
			$data['itemlab'] = $this->m_dt_laboratorium->get_datatables($id);

			echo json_encode(array(
				'html' => $this->load->view('penunjang/laboratorium/datang/laboratorium_modal', $data, true)
			));
		}

		if ($type == 'search_data') {
			$list = $this->m_dt_laboratorium->get_datatables($id);
			$no = 0;
			$data = array();
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				if ($field['parent_id'] != '') {
					$dash = '--';
				} else {
					$dash = '';
				};
				$row[] = $field['itemlab_id'];
				$row[] = $dash . $field['itemlab_nm'];
				if ($field['parent_id'] != '') {
					if (@$field['pemeriksaanrinc_id'] != null) {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemlab_id'].'" checked="true">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="' . $field['itemlab_id'] . '" data-itemlab-id="' . point_to_under($field['itemlab_id']) . '" data-tgl-hasil="' . $field['tgl_hasil'] . '" data-hasil-lab="' . space_to_dbl_hashtage(@$field['hasil_lab']) . '" data-catatan-lab="' . space_to_dbl_hashtage(@$field['catatan_lab']) . '" checked="true">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>
                      <input type="hidden" name="itemlab_id[]" value="' . point_to_under($field['itemlab_id']) . '">
                      <input type="hidden" name="tgl_hasil_' . point_to_under($field['itemlab_id']) . '" value="' . @$field['tgl_hasil'] . '">
                      <input type="hidden" name="hasil_lab_' . point_to_under($field['itemlab_id']) . '" value="' . space_to_dbl_hashtage(@$field['hasil_lab']) . '">
                      <input type="hidden" name="catatan_lab_' . point_to_under($field['itemlab_id']) . '" value="' . space_to_dbl_hashtage(@$field['catatan_lab']) . '">
                      ';
					} else {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemlab_id'].'">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="' . $field['itemlab_id'] . '" data-itemlab-id="' . point_to_under($field['itemlab_id']) . '" data-tgl-hasil="" data-hasil-lab="" data-catatan-lab="">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>
                      <input type="hidden" name="itemlab_id[]" value="' . point_to_under($field['itemlab_id']) . '">
                      <input type="hidden" name="tgl_hasil_' . point_to_under($field['itemlab_id']) . '" value="">
                      <input type="hidden" name="hasil_lab_' . point_to_under($field['itemlab_id']) . '" value="">
                      <input type="hidden" name="catatan_lab_' . point_to_under($field['itemlab_id']) . '" value="">
                      ';
					}
				} else {
					$row[] = '';
				}

				$data[] = $row;
			}

			$output = array(
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'save_laboratorium') {
			$this->m_lab_datang->hp_laboratorium_save(@$reg_id, @$id);
		}

		if ($type == 'delete_data') {
			$pemeriksaanrinc_id = $this->input->post('pemeriksaanrinc_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$this->m_lab_datang->hasil_pemeriksaan_delete($pemeriksaanrinc_id, $pemeriksaan_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_lab_datang->hasil_pemeriksaan_data($pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/laboratorium/datang/hasil_pemeriksaan_data', $data, true)
			));
		}

		if ($type == 'edit') {
			$pemeriksaanrinc_id = $this->input->post('pemeriksaanrinc_id');
			if ($pemeriksaanrinc_id == null) {
				$data['main'] = array();
			} else {
				$data['main'] = $this->m_lab_datang->get_lab_pemeriksaan_rinc($pemeriksaanrinc_id);
			}
			$data['pemeriksaanrinc_id'] = $pemeriksaanrinc_id;
			$data['nav'] = $this->nav;
			$data['form_action'] = site_url() . '/penunjang/laboratorium/datang/save/' . $pemeriksaanrinc_id;

			echo json_encode(array(
				'html' => $this->load->view('penunjang/laboratorium/datang/hasil_pemeriksaan_edit', $data, true)
			));
		}

		if ($type == 'autocomplete') {
			$itemlab_nm = $this->input->get('itemlab_nm');
			$res = $this->m_lab_datang->itemlab_autocomplete($itemlab_nm);
			echo json_encode($res);
		}

		if ($type == 'save_edit') {
			$this->m_lab_datang->datang_save_edit();
		}
	}

	// Tarif Tindakan
	public function ajax_tarif_tindakan($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_lab_datang->tarif_tindakan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/laboratorium/datang/tarif_tindakan_data', $data, true)
			));
		}
	}

	// Pemberian BHP
	public function ajax_bhp($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_lab_datang->bhp_save();
		} elseif ($type == 'autocomplete') {
			$obat_nm = $this->input->get('obat_nm');
			$map_lokasi_depo = $this->input->get('map_lokasi_depo');
			$res = $this->m_lab_datang->bhp_autocomplete($obat_nm, $map_lokasi_depo);
			echo json_encode($res);
		} elseif ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_lab_datang->bhp_data($reg_id, $lokasi_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/laboratorium/datang/bhp_data', $data, true)
			));
		} elseif ($type == 'get_data') {
			$bhp_id = $this->input->post('bhp_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_lab_datang->bhp_get($bhp_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$bhp_id = $this->input->post('bhp_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_lab_datang->bhp_delete($bhp_id, $reg_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_lab_datang->bhp_data($reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/laboratorium/datang/bhp_data', $data, true)
			));
		} elseif ($type == 'search_bhp') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('penunjang/laboratorium/datang/search_bhp', $data, true)
			));
		} elseif ($type == 'search_data') {
			$map_lokasi_depo = $this->input->post('map_lokasi_depo');
			$list = $this->m_dt_bhp->get_datatables($map_lokasi_depo);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['no_batch'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = to_date($field['tgl_expired'], '-', 'date', ' ');
				$row[] = num_id($field['stok_akhir']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="bhp_fill(' . "'" . $field['obat_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_bhp->count_all($map_lokasi_depo),
				"recordsFiltered" => $this->m_dt_bhp->count_filtered($map_lokasi_depo),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'bhp_fill') {
			$data = $this->input->post();
			$res = $this->m_lab_datang->bhp_row($data['barang_id']);
			echo json_encode($res);
		}
	}

	// Pemberian Alkes
	public function ajax_alkes($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_lab_datang->alkes_save();
		} elseif ($type == 'autocomplete') {
			$obat_nm = $this->input->get('obat_nm');
			$map_lokasi_depo = $this->input->get('map_lokasi_depo');
			$res = $this->m_lab_datang->alkes_autocomplete($obat_nm, $map_lokasi_depo);
			echo json_encode($res);
		} elseif ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_lab_datang->alkes_data($reg_id, $lokasi_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/laboratorium/datang/alkes_data', $data, true)
			));
		} elseif ($type == 'get_data') {
			$alkes_id = $this->input->post('alkes_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_lab_datang->alkes_get($alkes_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$alkes_id = $this->input->post('alkes_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_lab_datang->alkes_delete($alkes_id, $reg_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_lab_datang->alkes_data($reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/laboratorium/datang/alkes_data', $data, true)
			));
		} elseif ($type == 'search_alkes') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('penunjang/laboratorium/datang/search_alkes', $data, true)
			));
		} elseif ($type == 'search_data') {
			$map_lokasi_depo = $this->input->post('map_lokasi_depo');
			$list = $this->m_dt_alkes->get_datatables($map_lokasi_depo);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['no_batch'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = to_date($field['tgl_expired'], '-', 'date', ' ');
				$row[] = num_id($field['stok_akhir']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="alkes_fill(' . "'" . $field['obat_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_alkes->count_all($map_lokasi_depo),
				"recordsFiltered" => $this->m_dt_alkes->count_filtered($map_lokasi_depo),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'alkes_fill') {
			$data = $this->input->post();
			$res = $this->m_lab_datang->alkes_row($data['barang_id']);
			echo json_encode($res);
		}
	}

	public function cetak_modal($type = null, $reg_id = null, $pemeriksaan_id)
	{
		$nav = $this->nav;
		$data['url'] = site_url() . '/penunjang/laboratorium/datang/' . $type . '/' . $reg_id . '/' . $pemeriksaan_id;

		echo json_encode(array(
			'html' => $this->load->view('penunjang/laboratorium/datang/cetak_modal', $data, true)
		));
	}

	public function print_hasil_pemeriksaan($reg_id = '', $pemeriksaan_id = '')
	{
		$main = $this->m_lab_datang->laboratorium_get($pemeriksaan_id);
		$identitas_pasien = $this->m_lab_datang->laboratorium_identitas_pasien_get($reg_id);
		$config = $this->m_app->_get_config();
		$identitas = $this->m_app->_get_identitas();

		$nav = $this->nav;
		//generate pdf
		$profile = $this->m_profile->get_first();
		$this->load->library('pdfmc');
		$pdf = new Pdfmc('p', 'mm', array(241.3, 279.4));
		$pdf->AliasNbPages();
		$pdf->SetTitle('Cetak Hasil Pemeriksaan Laboratorium ' . $pemeriksaan_id);
		$pdf->AddPage();

		// $pdf->Image(FCPATH . 'assets/images/icon/pt-cahaya-permata-medika.png', 10, 10, 15, 15);
		// $pdf->SetFont('Arial', 'B', 9);
		// $pdf->Cell(0, 4, 'INSTALASI LABORATORIUM', 0, 1, 'C');
		// $pdf->SetFont('Arial', '', 9);
		// $pdf->Cell(0, 4, 'PT. CAHAYA PERMATA MEDIKA', 0, 1, 'C');
		// $pdf->SetFont('Arial', 'B', 11);
		// $pdf->Cell(0, 6, @$profile['title_logo_login'] . ' ' . @$profile['sub_title_logo_login'], 0, 1, 'C');
		// $pdf->SetFont('Arial', '', 7);
		// $pdf->Cell(0, 3, 'Jl. Mayjen Sutoyo No. 75 Purworejo Telp. (0275) 321031', 0, 1, 'C');
		// $pdf->Cell(0, 3, 'Email. rsiapermatapwr@gmail.com', 0, 1, 'C');
		// $pdf->Image(FCPATH . 'assets/images/icon/simrs-logo-rs.jpg', 215, 10, 15, 15);
		$pdf->Image(FCPATH . 'assets/images/icon/new-kop.jpg', 10, 6, 200, 24);
		$pdf->Cell(0, 19, '', 0, 1, 'C');
		$pdf->Line(10, 32, 230, 32);
		$pdf->Line(10, 32, 230, 32);
		$pdf->Line(10, 32, 230, 32);
		$pdf->Line(10, 32, 230, 32);
		$pdf->Line(10, 32, 230, 32);

		$pdf->Cell(0, 6, '', 0, 1, 'L');
		$pdf->SetFont('Arial', '', 11);
		$pdf->Cell(0, 4, 'HASIL PEMERIKSAAN LABORATORIUM', 0, 1, 'C');
		$pdf->Cell(0, 3, '', 0, 1, 'C');

		//Identitas Pasien
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(40, 4, 'No. Registrasi / No. Order', 0, 0, 'L');
		$pdf->Cell(120, 4, ':  ' . $identitas_pasien['reg_id'] . ' / ' . @$pemeriksaan_id, 0, 1, 'L');

		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(40, 4, 'No. RM', 0, 0, 'L');
		$pdf->Cell(120, 4, ':  ' . $identitas_pasien['pasien_id'], 0, 1, 'L');

		$pdf->Cell(40, 4, 'NIK', 0, 0, 'L');
		$pdf->Cell(120, 4, ':  ' . $identitas_pasien['nik'], 0, 1, 'L');

		$pdf->Cell(40, 4, 'Nama Pasien', 0, 0, 'L');
		$pdf->Cell(120, 4, ':  ' . strtoupper($identitas_pasien['pasien_nm'] . ', ' . $identitas_pasien['sebutan_cd']), 0, 1, 'L');

		$pdf->Cell(40, 4, 'Umur / Tempat, Tgl.Lahir', 0, 0, 'L');
		$pdf->Cell(120, 4, ':  ' . $identitas_pasien['umur_thn'] . ' thn / ' . $identitas_pasien['tmp_lahir'] . ', ' . to_date($identitas_pasien['tgl_lahir']), 0, 1, 'L');

		$pdf->Cell(40, 4, 'Jenis Kelamin', 0, 0, 'L');
		$pdf->Cell(120, 4, ($identitas_pasien['sex_cd'] == 'L') ? ':  Laki-laki' : ':  Perempuan', 0, 1, 'L');

		$pdf->Cell(40, 4, 'Alamat', 0, 0, 'L');
		$pdf->Cell(120, 4, ($identitas_pasien['alamat'] != '') ? ':  ' . strtoupper($identitas_pasien['alamat']) . ' ' . strtoupper($identitas_pasien['kelurahan']) . ' ' . strtoupper($identitas_pasien['kecamatan']) . ' ' . strtoupper($identitas_pasien['kabupaten']) : ':  ' . strtoupper($identitas_pasien['kelurahan']) . ' ' . strtoupper($identitas_pasien['kecamatan']) . ' ' . strtoupper($identitas_pasien['kabupaten']), 0, 1, 'L');

		$pdf->Cell(40, 4, 'Dokter Pengirim', 0, 0, 'L');
		$pdf->Cell(120, 4, ':  ' . @$main['dokter_nm'], 0, 1, 'L');

		$pdf->Cell(40, 4, 'Tanggal Terima', 0, 0, 'L');
		$pdf->Cell(120, 4, ':  ' . to_date(@$main['tgl_order'], '-', 'full_date'), 0, 1, 'L');

		$pdf->Cell(40, 4, 'Tanggal Pelaporan', 0, 0, 'L');
		$pdf->Cell(120, 4, ':  ' . to_date(@$main['tgl_diperiksa'], '-', 'full_date'), 0, 1, 'L');

		// generate tabel
		$laboratorium_pemeriksaan_data = $this->m_lab_datang->hasil_pemeriksaan_data($pemeriksaan_id);
		$pdf->Cell(0, 2, '', 0, 1, 'C');
		$no = 1;

		foreach ($laboratorium_pemeriksaan_data as $row) {
			$pdf->SetFont('Arial', 'B', 10);

			if ($row['parent_id'] == '') {
				$no = 1;
				$pdf->Cell(0, 5, '', 0, 1);
				$pdf->Cell(0, 6, $row['itemlab_nm'], 0, 1, 'C');
				$pdf->Cell(8, 5, 'No.', 1, 0, 'C');
				$pdf->Cell(80, 5, 'Pemeriksaan', 1, 0, 'C');
				$pdf->Cell(60, 5, 'Nilai Normal', 1, 0, 'C');
				$pdf->Cell(35, 5, 'Hasil', 1, 0, 'C');
				$pdf->Cell(35, 5, 'Catatan', 1, 1, 'C');
			} else {
				$pdf->SetFont('Arial', '', 9);
				$pdf->SetWidths(array("188"));
				$pdf->SetAligns(array("L"));
				$pdf->Row(array(
					$row['itemlab_nm']
				));
			}

			//
			if (count($row['rinc']) > 0) {
				$pdf->SetWidths(array("8", "80", "60", "35", "35"));
				// 	//$pdf->SetHeights('5');
				$pdf->SetAligns(array("C", "L", "C", "C", "L"));
				foreach ($row['rinc'] as $row2) {
					$pdf->SetFont('Arial', '', 9);
					$nilai_normal = "";
					switch ($row2['tipe_rujukan']) {
						case 1:
							if ($row2['konfirmasi'] == -1) {
								$nilai_normal = '-';
							} else if ($row2['konfirmasi'] == 0) {
								$nilai_normal =  'Negatif';
							} else if ($row2['konfirmasi'] == 1) {
								$nilai_normal =  'Positif';
							}
							break;

						case 2:
							$nilai_normal = float_id($row2['rentang_min']) . " - " . float_id($row2['rentang_max']) . " " . $row2['satuan'];
							break;

						case 3:
							$nilai_normal = "Laki-laki : " . float_id($row2['rentang_l_min']) . " - " . float_id($row2['rentang_l_max']) . " " . $row2['satuan'] . "\n";
							$nilai_normal .= "Perempuan : " . float_id($row2['rentang_p_min']) . " - " . float_id($row2['rentang_p_max']) . " " . $row2['satuan'];
							break;

						case 4:
							$nilai_normal = "< " .  float_id($row2['kurang']) . " " . $row2['satuan'];
							break;

						case 5:
							$nilai_normal = "Laki-laki : < " . float_id($row2['kurang_l']) . $row2['satuan'] . "\n";
							$nilai_normal .= "Perempuan : < " . float_id($row2['kurang_p'])  . $row2['satuan'];
							break;

						case 6:
							$nilai_normal = "> " . float_id($row2['lebih']) .  " " . $row2['satuan'];
							break;

						case 7:
							$nilai_normal = "Laki-laki : > " . float_id($row2['lebih_l']) . $row2['satuan'] . "\n";
							$nilai_normal .= "Perempuan : > " . float_id($row2['lebih_p'])  . $row2['satuan'];
							break;

						case 8:
							$nilai_normal = $row2['informasi'];
							break;
					}
					$hasil_lab = float_id($row2['hasil_lab']);
					if ($row2['tipe_rujukan'] == 1) {
						if ($hasil_lab == 1) {
							$hasil_lab = "Positif";
						} else if ($hasil_lab == 0) {
							$hasil_lab = "Negatif";
						}
					}

					if ($row2['tipe_rujukan'] == 8) {
						$hasil_lab = $row2['hasil_lab'];
					}

					$pdf->Row(array(
						$no++,
						$row2['itemlab_nm'],
						$nilai_normal,
						$hasil_lab,
						$row2['catatan_lab'],
					));
				}
			}
		}

		// petugas
		$pdf->SetFont('Arial', 'i', 8);
		$pdf->Cell(0, 10, '', 0, 1, 'C');
		$pdf->Cell(85, 4, 'Jika ada keragu-raguan hasil pemeriksaan laboratorium,', 0, 0, 'L');
		$pdf->Cell(85, 4, '', 0, 0, 'C');
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(0, 4, 'Pemeriksa,', 0, 1, 'C');
		$pdf->SetFont('Arial', 'i', 8);
		$pdf->Cell(85, 4, 'diharapkan segera menghubungi laboratorium', 0, 0, 'L');
		$pdf->Cell(0, 15, '', 0, 1, 'C');
		$pdf->SetFont('Arial', '', 8);
		$pdf->Cell(85, 4, 'Dokter Penanggungjawab Laboratorium : ' . @$main['dokterpj_nm'], 0, 0, 'L');
		$pdf->Cell(85, 4, '', 0, 0, 'L');
		$pdf->Cell(0, 4, '(' . $main['pemeriksa_nm'] . ')', 0, 1, 'C');

		$pdf->Output('I', 'Hasil_Pemeriksaan_Laboratorium_' . $reg_id . '_' . $pemeriksaan_id . '_' . date('Ymdhis') . '.pdf');
	}

	public function tambah_item()
	{
		$data = $this->input->post();
		//cek item 
		$cek = $this->m_lab_datang->cek_item($data);
		if ($cek != null) {
			$this->session->set_flashdata('flash_error', 'Item laboratorium sudah ada!');
		} else {
			$this->m_lab_datang->tambah_item($data);
		}
		redirect(site_url() . '/penunjang/laboratorium/datang/periksa/' . $data['reg_id'] . '/' . $data['pemeriksaan_id'] . '#hasil-pemeriksaan');
	}
}
