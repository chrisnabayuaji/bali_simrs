<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Datang extends MY_Controller
{

	var $nav_id = '04.02.01', $nav, $cookie;

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array(
			'radiologi/m_rad_datang',
			'radiologi/m_dt_bhp',
			'radiologi/m_dt_alkes',
			'master/m_lokasi',
			'master/m_wilayah',
			'master/m_kelas',
			'master/m_jenis_pasien',
			'master/m_pegawai',
			'm_rad_datang',
			'm_dt_petugas',
			'pelayanan/m_dt_mst_pasien',
			'pelayanan/m_registrasi_offline',
			'pelayanan/m_caller_antrian',
			'pelayanan/m_dt_radiologi',
			'app/m_profile'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
		$this->cookie = get_cookie_nav($this->nav_id . '.datang');
		if ($this->cookie['search'] == null) $this->cookie['search'] = array('tgl_registrasi_from' => '', 'tgl_registrasi_to' => '', 'no_rm_nm' => '', 'lokasi_id' => '', 'jenispasien_id' => '', 'periksa_st' => '', 'pulang_st' => '');
		if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'tgl_registrasi', 'type' => 'desc');
		if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}

	public function index()
	{
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(5, 0);
		$this->cookie['total_rows'] = $this->m_rad_datang->all_rows($this->cookie);
		set_cookie_nav($this->nav_id . '.datang', $this->cookie);
		//main data
		$data['nav'] = $this->nav;
		$data['cookie'] = $this->cookie;
		$data['main'] = $this->m_rad_datang->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
		$data['lokasi'] = $this->m_lokasi->by_field('jenisreg_st', 1, 'result');
		$data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
		//set pagination
		$nav['nav_url'] = 'penunjang/radiologi/datang';
		set_pagination($nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('penunjang/radiologi/datang/index', $data);
	}

	public function form($id = null)
	{
		$this->authorize($this->nav, ($id != '') ? '_update' : '_add');

		if ($id == null) {
			$data['main'] = array();
			$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_profile();
		} else {
			$data['main'] = $this->m_rad_datang->get_data($id);
			if ($data['main']['wilayah_st'] == 'L') {
				$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_parent('');
			} else {
				$data['list_wilayah_prop'] = $this->m_wilayah->list_wilayah_by_profile();
			}
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['kelas'] = $this->m_kelas->all_data();
		$data['lokasi'] = $this->m_lokasi->by_field('lokasi_id', '03.03', 'result');
		$data['dokter'] = $this->m_pegawai->by_field('jenispegawai_cd', '02', 'result');
		$data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
		$data['form_action'] = site_url() . '/penunjang/radiologi/datang/save/' . $id;

		$this->render('penunjang/radiologi/datang/form', $data);
	}

	public function save($id = null)
	{
		$reg_id = $this->m_rad_datang->save($id);
		$mainrad = $this->m_rad_datang->radiologi_get('reg_id', $reg_id);

		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		create_log(($id != '') ? '_update' : '_add', $this->nav_id);
		redirect(site_url() . '/penunjang/radiologi/datang/order/' . $reg_id . '/' . $mainrad['pemeriksaan_id']);
	}

	public function periksa($reg_id = null, $pemeriksaan_id = null)
	{
		$this->authorize($this->nav, ($reg_id != '' && $pemeriksaan_id != '') ? '_update' : '_add');

		if ($reg_id == null && $pemeriksaan_id == null) {
			$data['main'] = array();
			$data['alergi'] = array();
			$data['diagnosis'] = array();
		} else {
			$this->m_rad_datang->update_periksa_st($pemeriksaan_id);
			$data['main'] = $this->m_rad_datang->get_data_periksa($pemeriksaan_id);
			$data['alergi'] = $this->m_rad_datang->get_alergi($data['main']['pasien_id']);
			$data['diagnosis'] = $this->m_rad_datang->get_diagnosis($reg_id);
		}
		$data['reg_id'] = $reg_id;
		$data['pemeriksaan_id'] = $pemeriksaan_id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/save/' . $reg_id . '/' . $pemeriksaan_id;

		$this->render('penunjang/radiologi/datang/periksa', $data);
	}

	public function delete($id = null)
	{
		$this->m_rad_datang->delete($id, true);
		$this->session->set_flashdata('flash_success', 'Data berhasil dihapus');
		create_log('_delete', $this->nav_id);
		redirect(site_url() . '/penunjang/radiologi/datang/index/' . $this->cookie['cur_page']);
	}

	public function multiple($type = null)
	{
		$data = $this->input->post();
		if (isset($data['checkitem'])) {
			foreach ($data['checkitem'] as $key) {
				switch ($type) {
					case 'delete':
						$this->authorize($this->nav, '_delete');
						$this->m_rad_datang->delete($key, true);
						$flash = 'Data berhasil dihapus.';
						create_log('_delete', $this->nav_id);
						break;

					case 'enable':
						$this->authorize($this->nav, '_update');
						$this->m_rad_datang->update($key, array('is_active' => 1));
						$flash = 'Data berhasil diaktifkan.';
						create_log('_update', $this->nav_id);
						break;

					case 'disable':
						$this->authorize($this->nav, '_update');
						$this->m_rad_datang->update($key, array('is_active' => 0));
						$flash = 'Data berhasil dinonaktifkan.';
						create_log('_delete', $this->nav_id);
						break;
				}
			}
		}
		$this->session->set_flashdata('flash_success', $flash);
		redirect(site_url() . '/penunjang/radiologi/datang/index/' . $this->cookie['cur_page']);
	}

	function ajax($type = null, $id = null)
	{
		if ($type == 'cek_id') {
			$data = $this->input->post();
			$cek = $this->m_rad_datang->get_data($data['lokasi_id']);
			if ($id == null) {
				if ($cek == null) {
					echo 'true';
				} else {
					echo 'false';
				}
			} else {
				if ($id != $data['lokasi_id'] && $cek != null) {
					echo 'false';
				} else {
					echo 'true';
				}
			}
		}

		if ($type == 'get_antrian_cd') {
			$data = $this->input->post();
			$res = $this->m_lokasi->get_data($data['lokasi_id']);
			echo json_encode($res);
		}

		if ($type == 'online_fill') {
			$data = $this->input->post();
			$res = $this->m_rad_datang->online_row($data['regonline_id']);
			echo json_encode($res);
		}

		if ($type == 'pasien_fill') {
			$data = $this->input->post();
			$res = $this->m_rad_datang->pasien_row($data['pasien_id']);
			echo json_encode($res);
		}

		if ($type == 'no_rm_fill') {
			$data = $this->input->post();
			$res = $this->m_rad_datang->no_rm_row($data['pasien_id']);
			echo json_encode($res);
		}

		if ($type == 'status_antrian') {
			$data = $this->input->post();
			$res = $this->m_caller_antrian->get_data($data['lokasi_id']);
			echo json_encode($res);
		}

		if ($type == 'call_antrian') {
			$data = $this->input->post();
			$res = $this->m_caller_antrian->call_antrian($data);
			echo json_encode($res);
		}

		if ($type == 'view_tab_menu') {
			$view_name = $this->input->post('view_name');
			$data['reg_id'] = $this->input->post('reg_id');
			$data['pemeriksaan_id'] = $this->input->post('pemeriksaan_id');
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rad_datang->get_data_periksa($data['pemeriksaan_id']);

			if ($view_name == 'hasil_pemeriksaan') {
				$data['get_tgl_hasil'] = $this->m_rad_datang->get_tgl_hasil($data['pemeriksaan_id']);
			}

			echo json_encode(array(
				'html' => $this->load->view('penunjang/radiologi/datang/' . $view_name, $data, true)
			));
		}
	}

	public function online_modal()
	{
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('penunjang/radiologi/datang/online_modal', $data, true)
		));
	}

	public function get_data_online()
	{
		$list = $this->m_rad_datang->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field['tgl_periksa'];
			$row[] = $field['pasien_id'];
			$row[] = $field['pasien_nm'];
			$row[] = $field['alamat'];
			$row[] = $field['sex_cd'];
			$row[] = $field['tmp_lahir'] . ', ' . @to_date($field['tgl_lahir']);
			$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="online_fill(' . "'" . $field['regonline_id'] . "'" . ')">Pilih >></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_rad_datang->count_all(),
			"recordsFiltered" => $this->m_rad_datang->count_filtered(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function pasien_modal()
	{
		$data['nav'] = $this->nav;

		echo json_encode(array(
			'html' => $this->load->view('penunjang/radiologi/datang/pasien_modal', $data, true)
		));
	}

	public function get_data_pasien()
	{
		$list = $this->m_dt_mst_pasien->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field['pasien_id'];
			$row[] = $field['pasien_nm'];
			$row[] = $field['alamat'];
			$row[] = $field['sex_cd'];
			$row[] = $field['tmp_lahir'] . ', ' . @to_date($field['tgl_lahir']);
			$row[] = $field['jenispasien_nm'];
			$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="pasien_fill(' . "'" . $field['pasien_id'] . "'" . ')">Pilih >></button>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->m_dt_mst_pasien->count_all(),
			"recordsFiltered" => $this->m_dt_mst_pasien->count_filtered(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function caller_modal()
	{
		$data['nav'] = $this->nav;
		$data['caller'] = $this->m_rad_datang->get_caller();

		echo json_encode(array(
			'html' => $this->load->view('penunjang/radiologi/datang/caller_modal', $data, true)
		));
	}

	public function get_nik($nik)
	{
		$this->load->library('xmlrpc');
		$this->load->library('xmlrpcs');
		$url = "https://ayokitakerja.kemnaker.go.id/tools/check_nik/" . $nik;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 60);
		// curl_setopt($ch, CURLOPT_HTTPHEADER, $request_headers);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_FAILONERROR, true);
		$data = curl_exec($ch);

		if (curl_errno($ch)) {
			$res = array('STATUS' => -1);
			echo json_encode($res);
		} else {
			$data = json_decode($data, TRUE);
			curl_close($ch);
			echo json_encode($data);
		}
	}

	public function order($reg_id, $id = null)
	{
		$data['nav'] = $this->nav;
		$data['reg_id'] = $reg_id;
		$data['id'] = $id;
		$data['form_action'] = site_url('penunjang/radiologi/datang/order_save/' . $reg_id . '/' . $id);
		$data['reg'] = $this->m_registrasi_offline->get_data($reg_id);
		$data['mainrad'] = $this->m_rad_datang->radiologi_get('pemeriksaan_id', $id);

		$this->render('penunjang/radiologi/datang/order', $data);
	}

	public function order_save($reg_id = null, $id = null)
	{
		$this->m_rad_datang->radiologi_save($reg_id, $id);
		redirect(site_url('penunjang/radiologi/datang'));
	}

	public function ajax_penunjang_radiologi($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'modal') {
			$data['reg'] = $this->m_poliklinik->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainrad'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_radiologi/save/';
			} else {
				$data['mainrad'] = $this->m_poliklinik->radiologi_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_radiologi/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/radiologi_modal', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['reg'] = $this->m_poliklinik->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainrad'] = null;
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_radiologi/save/';
			} else {
				$data['mainrad'] = $this->m_poliklinik->radiologi_get($id);
				$data['form_action'] = site_url() . '/' . $this->nav['nav_url'] . '/ajax_penunjang_radiologi/save/' . $id;
			}

			$data['rinc'] = $this->m_poliklinik->radiologi_detail($reg_id, $id);

			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/radiologi_detail', $data, true)
			));
		}

		if ($type == 'search_data') {

			$list = $this->m_dt_radiologi->get_datatables($id);
			$no = 0;
			$data = array();
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				if ($field['parent_id'] != '') {
					$dash = '--';
				} else {
					$dash = '';
				};
				$row[] = $field['itemrad_id'];
				$row[] = $dash . $field['itemrad_nm'];
				if ($field['parent_id'] != '') {
					if (@$field['pemeriksaanrinc_id'] != null) {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemrad_id'].'" checked="true">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="' . $field['itemrad_id'] . '" checked="true">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>';
					} else {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemrad_id'].'">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="' . $field['itemrad_id'] . '">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>';
					}
				} else {
					$row[] = '';
				}

				$data[] = $row;
			}

			$output = array(
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'save') {
			$this->m_poliklinik->radiologi_save(@$reg_id, @$id);
		}

		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->radiologi_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/radiologi_data', $data, true)
			));
		}

		if ($type == 'delete_data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_poliklinik->radiologi_delete($pemeriksaan_id, $reg_id, $pasien_id, $lokasi_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_poliklinik->radiologi_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('pelayanan/poliklinik/radiologi_data', $data, true)
			));
		}
	}

	public function save_hasil_pemeriksaan($reg_id = null, $pemeriksaan_id = null)
	{
		$this->m_rad_datang->save_hasil_pemeriksaan($reg_id, $pemeriksaan_id);
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		redirect(site_url() . '/penunjang/radiologi/datang/periksa/' . $reg_id . '/' . $pemeriksaan_id . '#hasil-pemeriksaan');
	}

	// Hasil Pemeriksaan
	public function ajax_hasil_pemeriksaan($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['pemeriksaan_id'] = @$pemeriksaan_id;
			$data['main'] = $this->m_rad_datang->hasil_pemeriksaan_data($pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/radiologi/datang/hasil_pemeriksaan_data', $data, true)
			));
		}

		if ($type == 'modal') {
			$data['reg'] = $this->m_rad_datang->get_data_by_reg_id($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainrad'] = null;
				$data['form_action'] = site_url() . '/penunjang/radiologi/datang/ajax_penunjang_radiologi/save/';
			} else {
				$data['mainrad'] = $this->m_rad_datang->hp_radiologi_get($id);
				$data['form_action'] = site_url() . '/penunjang/radiologi/datang/ajax_penunjang_radiologi/save/' . $id;
			}

			echo json_encode(array(
				'html' => $this->load->view('penunjang/radiologi/datang/radiologi_modal', $data, true)
			));
		}

		if ($type == 'search_data') {
			$list = $this->m_dt_radiologi->get_datatables($id);
			$no = 0;
			$data = array();
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				if ($field['parent_id'] != '') {
					$dash = '--';
				} else {
					$dash = '';
				};
				$row[] = $field['itemrad_id'];
				$row[] = $dash . $field['itemrad_nm'];
				if ($field['parent_id'] != '') {
					if (@$field['pemeriksaanrinc_id'] != null) {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemrad_id'].'" checked="true">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="' . $field['itemrad_id'] . '" data-itemrad-id="' . point_to_under($field['itemrad_id']) . '" data-tgl-hasil="' . $field['tgl_hasil'] . '" data-hasil-rad="' . space_to_dbl_hashtage(@$field['hasil_rad']) . '" data-catatan-rad="' . space_to_dbl_hashtage(@$field['catatan_rad']) . '" checked="true">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>
                      <input type="hidden" name="itemrad_id[]" value="' . point_to_under($field['itemrad_id']) . '">
                      <input type="hidden" name="tgl_hasil_' . point_to_under($field['itemrad_id']) . '" value="' . @$field['tgl_hasil'] . '">
                      <input type="hidden" name="hasil_rad_' . point_to_under($field['itemrad_id']) . '" value="' . space_to_dbl_hashtage(@$field['hasil_rad']) . '">
                      <input type="hidden" name="catatan_rad_' . point_to_under($field['itemrad_id']) . '" value="' . space_to_dbl_hashtage(@$field['catatan_rad']) . '">
                      ';
					} else {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemrad_id'].'">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="' . $field['itemrad_id'] . '" data-itemrad-id="' . point_to_under($field['itemrad_id']) . '" data-tgl-hasil="" data-hasil-rad="" data-catatan-rad="">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>
                      <input type="hidden" name="itemrad_id[]" value="' . point_to_under($field['itemrad_id']) . '">
                      <input type="hidden" name="tgl_hasil_' . point_to_under($field['itemrad_id']) . '" value="">
                      <input type="hidden" name="hasil_rad_' . point_to_under($field['itemrad_id']) . '" value="">
                      <input type="hidden" name="catatan_rad_' . point_to_under($field['itemrad_id']) . '" value="">
                      ';
					}
				} else {
					$row[] = '';
				}

				$data[] = $row;
			}

			$output = array(
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'save_radiologi') {
			$this->m_rad_datang->hp_radiologi_save(@$reg_id, @$id);
		}

		if ($type == 'delete_data') {
			$pemeriksaanrinc_id = $this->input->post('pemeriksaanrinc_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$this->m_rad_datang->hasil_pemeriksaan_delete($pemeriksaanrinc_id, $pemeriksaan_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rad_datang->hasil_pemeriksaan_data($pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/radiologi/datang/hasil_pemeriksaan_data', $data, true)
			));
		}

		if ($type == 'edit') {
			$pemeriksaanrinc_id = $this->input->post('pemeriksaanrinc_id');
			if ($pemeriksaanrinc_id == null) {
				$data['main'] = array();
			} else {
				$data['main'] = $this->m_rad_datang->get_rad_pemeriksaan_rinc($pemeriksaanrinc_id);
			}
			$data['pemeriksaanrinc_id'] = $pemeriksaanrinc_id;
			$data['nav'] = $this->nav;
			$data['form_action'] = site_url() . '/penunjang/radiologi/datang/save/' . $pemeriksaanrinc_id;

			echo json_encode(array(
				'html' => $this->load->view('penunjang/radiologi/datang/hasil_pemeriksaan_edit', $data, true)
			));
		}

		if ($type == 'autocomplete') {
			$itemrad_nm = $this->input->get('itemrad_nm');
			$res = $this->m_rad_datang->itemrad_autocomplete($itemrad_nm);
			echo json_encode($res);
		}

		if ($type == 'save_edit') {
			$this->m_rad_datang->datang_save_edit();
		}

		if ($type == 'autocomplete_petugas') {
			$pegawai_nm = $this->input->get('pegawai_nm');
			$res = $this->m_rad_datang->petugas_autocomplete($pegawai_nm);
			echo json_encode($res);
		}

		if ($type == 'search_petugas') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('penunjang/radiologi/datang/search_petugas_single', $data, true)
			));
		}

		if ($type == 'search_data_petugas') {
			$list = $this->m_dt_petugas->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['pegawai_id'];
				$row[] = $field['pegawai_nm'];
				$row[] = $field['pegawai_nip'];
				$row[] = get_parameter_value('jenispegawai_cd', $field['jenispegawai_cd']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="petugas_fill(' . "'" . $field['pegawai_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_petugas->count_all(),
				"recordsFiltered" => $this->m_dt_petugas->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'petugas_fill') {
			$data = $this->input->post();
			$res = $this->m_rad_datang->petugas_row($data['pegawai_id']);
			echo json_encode($res);
		}
	}

	// Tarif Tindakan
	public function ajax_tarif_tindakan($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rad_datang->tarif_tindakan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/radiologi/datang/tarif_tindakan_data', $data, true)
			));
		}
	}

	// Pemberian BHP
	public function ajax_bhp($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_rad_datang->bhp_save();
		} elseif ($type == 'autocomplete') {
			$obat_nm = $this->input->get('obat_nm');
			$map_lokasi_depo = $this->input->get('map_lokasi_depo');
			$res = $this->m_rad_datang->bhp_autocomplete($obat_nm, $map_lokasi_depo);
			echo json_encode($res);
		} elseif ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rad_datang->bhp_data($reg_id, $lokasi_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/radiologi/datang/bhp_data', $data, true)
			));
		} elseif ($type == 'get_data') {
			$bhp_id = $this->input->post('bhp_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_rad_datang->bhp_get($bhp_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$bhp_id = $this->input->post('bhp_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_rad_datang->bhp_delete($bhp_id, $reg_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rad_datang->bhp_data($reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/radiologi/datang/bhp_data', $data, true)
			));
		} elseif ($type == 'search_bhp') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('penunjang/radiologi/datang/search_bhp', $data, true)
			));
		} elseif ($type == 'search_data') {
			$map_lokasi_depo = $this->input->post('map_lokasi_depo');
			$list = $this->m_dt_bhp->get_datatables($map_lokasi_depo);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['no_batch'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = to_date($field['tgl_expired'], '-', 'date', ' ');
				$row[] = num_id($field['stok_akhir']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="bhp_fill(' . "'" . $field['obat_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_bhp->count_all($map_lokasi_depo),
				"recordsFiltered" => $this->m_dt_bhp->count_filtered($map_lokasi_depo),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'bhp_fill') {
			$data = $this->input->post();
			$res = $this->m_rad_datang->bhp_row($data['barang_id']);
			echo json_encode($res);
		}
	}

	// Pemberian Alkes
	public function ajax_alkes($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_rad_datang->alkes_save();
		} elseif ($type == 'autocomplete') {
			$obat_nm = $this->input->get('obat_nm');
			$map_lokasi_depo = $this->input->get('map_lokasi_depo');
			$res = $this->m_rad_datang->alkes_autocomplete($obat_nm, $map_lokasi_depo);
			echo json_encode($res);
		} elseif ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rad_datang->alkes_data($reg_id, $lokasi_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/radiologi/datang/alkes_data', $data, true)
			));
		} elseif ($type == 'get_data') {
			$alkes_id = $this->input->post('alkes_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_rad_datang->alkes_get($alkes_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		} elseif ($type == 'delete_data') {
			$alkes_id = $this->input->post('alkes_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_rad_datang->alkes_delete($alkes_id, $reg_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rad_datang->alkes_data($reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('penunjang/radiologi/datang/alkes_data', $data, true)
			));
		} elseif ($type == 'search_alkes') {
			$data['nav'] = $this->nav;

			echo json_encode(array(
				'html' => $this->load->view('penunjang/radiologi/datang/search_alkes', $data, true)
			));
		} elseif ($type == 'search_data') {
			$map_lokasi_depo = $this->input->post('map_lokasi_depo');
			$list = $this->m_dt_alkes->get_datatables($map_lokasi_depo);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['no_batch'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = to_date($field['tgl_expired'], '-', 'date', ' ');
				$row[] = num_id($field['stok_akhir']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="alkes_fill(' . "'" . $field['obat_id'] . "'" . ')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_alkes->count_all($map_lokasi_depo),
				"recordsFiltered" => $this->m_dt_alkes->count_filtered($map_lokasi_depo),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		} else if ($type == 'alkes_fill') {
			$data = $this->input->post();
			$res = $this->m_rad_datang->alkes_row($data['barang_id']);
			echo json_encode($res);
		}
	}

	public function cetak_modal($type = null, $reg_id = null, $pemeriksaan_id)
	{
		$nav = $this->nav;
		$data['url'] = site_url() . '/penunjang/radiologi/datang/' . $type . '/' . $reg_id . '/' . $pemeriksaan_id;

		echo json_encode(array(
			'html' => $this->load->view('penunjang/radiologi/datang/cetak_modal', $data, true)
		));
	}

	public function print_ekspertisi($pemeriksaan_id = '', $pemeriksaanrinc_id = '')
	{
		$main = $this->m_rad_datang->radiologi_get('pemeriksaan_id', $pemeriksaan_id);
		$rinc = $this->m_rad_datang->rinc_pemeriksaan($pemeriksaanrinc_id);
		$identitas_pasien = $this->m_rad_datang->radiologi_identitas_pasien_get(@$main['reg_id'], @$main['src_lokasi_id'], @$main['pasien_id']);

		$nav = $this->nav;
		//generate pdf
		$profile = $this->m_profile->get_first();
		$this->load->library('pdf');
		$pdf = new pdf('p', 'mm', array(210, 297));
		$pdf->AliasNbPages();
		$pdf->SetTitle('Cetak Hasil Pemeriksaan Laboratorium ' . $pemeriksaan_id);
		$pdf->AddPage();

		// $pdf->Image(FCPATH . 'assets/images/icon/pt-cahaya-permata-medika.png', 10, 10, 15, 15);
		// $pdf->SetFont('Arial', 'B', 9);
		// $pdf->Cell(0, 4, 'INSTALASI RADIOLOGI', 0, 1, 'C');
		// $pdf->SetFont('Arial', '', 9);
		// $pdf->Cell(0, 4, 'PT. CAHAYA PERMATA MEDIKA', 0, 1, 'C');
		// $pdf->SetFont('Arial', 'B', 11);
		// $pdf->Cell(0, 6, @$profile['title_logo_login'] . ' ' . @$profile['sub_title_logo_login'], 0, 1, 'C');
		// $pdf->SetFont('Arial', '', 7);
		// $pdf->Cell(0, 3, 'Jl. Mayjen Sutoyo No. 75 Purworejo Telp. (0275) 321031', 0, 1, 'C');
		// $pdf->Cell(0, 3, 'Email. rsiapermatapwr@gmail.com', 0, 1, 'C');
		// $pdf->Image(FCPATH . 'assets/images/icon/simrs-logo-rs.jpg', 184, 10, 15, 15);
		$pdf->Image(FCPATH . 'assets/images/icon/new-kop.jpg', 10, 6, 170, 23);
		$pdf->Cell(0, 19, '', 0, 1, 'C');
		$pdf->Line(10, 32, 200, 32);
		$pdf->Line(10, 32, 200, 32);
		$pdf->Line(10, 32, 200, 32);
		$pdf->Line(10, 32, 200, 32);
		$pdf->Line(10, 32, 200, 32);

		$pdf->Cell(0, 6, '', 0, 1, 'L');
		$pdf->SetFont('Arial', 'B', 11);
		$pdf->Cell(0, 4, 'HASIL EKSPERTISI RADIOLOGI', 0, 1, 'C');
		$pdf->Cell(0, 5, '', 0, 1, 'C');

		//Identitas Pasien
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(30, 5, 'Nama Pasien', 0, 0, 'L');
		$pdf->Cell(77, 5, ':  ' . strtoupper(@$identitas_pasien['pasien_nm'] . ', ' . @$identitas_pasien['sebutan_cd']), 0, 0, 'L');
		$pdf->Cell(30, 5, 'Ruangan', 0, 0, 'L');
		$pdf->Cell(77, 5, ':  ' . @$identitas_pasien['lokasi_nm'], 0, 1, 'L');

		$pdf->Cell(30, 5, 'No. RM', 0, 0, 'L');
		$pdf->Cell(77, 5, ':  ' . @$identitas_pasien['pasien_id'], 0, 0, 'L');
		$pdf->Cell(30, 5, 'Tgl. Pemeriksaan', 0, 0, 'L');
		$pdf->Cell(77, 5, ':  ' . to_date(@$rinc['tgl_hasil'], '', 'full_date') . ' WIB', 0, 1, 'L');

		$pdf->Cell(30, 5, 'TTL/Umur', 0, 0, 'L');
		$pdf->Cell(77, 5, ':  ' . (@$identitas_pasien['tmp_lahir'] != '' ? ', ' : '') . to_date(@$identitas_pasien['tgl_lahir']) . ' / ' . @$identitas_pasien['umur_thn'] . ' thn', 0, 0, 'L');
		$pdf->Cell(30, 5, 'Dokter Pengirim', 0, 0, 'L');
		$pdf->Cell(77, 5, ':  ' . @$main['dokter_nm'], 0, 1, 'L');

		$pdf->Cell(30, 5, 'NIK', 0, 0, 'L');
		$pdf->Cell(77, 5, ':  ' . $identitas_pasien['nik'], 0, 0, 'L');
		$pdf->Cell(30, 5, 'Item Pemeriksaan', 0, 0, 'L');
		$pdf->Cell(77, 5, ':  ' . @$rinc['itemrad_nm'], 0, 1, 'L');

		$pdf->Cell(30, 5, 'Alamat', 0, 0, 'L');
		$pdf->Cell(77, 4, (@$identitas_pasien['alamat'] != '') ? ':  ' . strtoupper(@$identitas_pasien['alamat']) . ', ' . strtoupper(@$identitas_pasien['kelurahan']) : ':  ' . strtoupper(@$identitas_pasien['kelurahan']) . ', ', 0, 1, 'L');
		$pdf->Cell(30, 5, '', 0, 0, 'L');
		$pdf->Cell(77, 5, '   ' . strtoupper(@$identitas_pasien['kecamatan']) . ', ' . strtoupper(@$identitas_pasien['kabupaten']), 0, 1, 'L');

		// generate tabel
		$pdf->Cell(0, 8, '', 0, 1, 'C');
		$pdf->SetFont('Arial', 'B', 10);
		$pdf->Cell(0, 5, 'Hasil Ekspertisi', 0, 1, 'L');
		$pdf->SetFont('Arial', '', 10);
		$getY_1 = $pdf->GetY();
		$pdf->drawTextBox("\n" . @$rinc['hasil_rad'], 190, 120, 'L', 'T', true, 0, $getY_1);

		// petugas
		// $pdf->Cell(0, 120, '', 0, 1, 'C');
		$pdf->SetY(215);
		$pdf->SetFont('Arial', '', 10);
		$pdf->Cell(0, 10, '', 0, 1, 'C');
		$pdf->Cell(60, 4, '', 0, 0, 'C');
		$pdf->Cell(60, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, 'Dokter Radiologi', 0, 1, 'C');
		if (@$main['dokterpj_id'] == '02.0004') {
			$pdf->Image(FCPATH . 'assets/images/ttd/dr-pratiwi.png', 147, 226, 29, 23);
		}
		$pdf->Cell(60, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 15, '', 0, 1, 'C');
		$pdf->Cell(60, 4, '', 0, 0, 'C');
		$pdf->Cell(60, 4, '', 0, 0, 'C');
		$pdf->Cell(0, 4, '(' . @$main['dokterpj_nm'] . ')', 0, 1, 'C');

		$pdf->Output('I', 'Hasil_Pemeriksaan_Laboratorium_' . $pemeriksaan_id . '_' . date('Ymdhis') . '.pdf');
	}
}
