<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_bedah extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['tgl_order_from'] != '' && @$cookie['search']['tgl_order_to']) {
      $where .= "AND (DATE(a.tgl_order) BETWEEN '" . to_date(@$cookie['search']['tgl_order_from']) . "' AND '" . to_date(@$cookie['search']['tgl_order_to']) . "')";
    }
    if (@$cookie['search']['no_rm_nm'] != '') {
      $where .= "AND (b.pasien_id LIKE '%" . $this->db->escape_like_str($cookie['search']['no_rm_nm']) . "%' OR b.pasien_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['no_rm_nm']) . "%' ) ";
    }
    if (@$cookie['search']['jenispasien_id'] != '') {
      $where .= "AND b.jenispasien_id = '" . $this->db->escape_like_str($cookie['search']['jenispasien_id']) . "' ";
    }
    if (@$cookie['search']['periksa_st'] != '') {
      $where .= "AND a.periksa_st = '" . $this->db->escape_like_str($cookie['search']['periksa_st']) . "' ";
    }
    if (@$cookie['search']['proses_st'] != '') {
      $where .= "AND a.proses_st = '" . $this->db->escape_like_str($cookie['search']['proses_st']) . "' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
        a.reg_id, a.pemeriksaan_id, a.tgl_order, a.pasien_id, a.periksa_st, a.proses_st, 
        b.pasien_nm, b.sebutan_cd, b.sex_cd, b.umur_thn, b.umur_bln, b.umur_hr,
        b.alamat, b.provinsi, b.kabupaten, b.kecamatan, b.kelurahan,
        c.jenispasien_nm
      FROM ibs_pemeriksaan a
      LEFT JOIN reg_pasien b ON a.reg_id = b.reg_id
      LEFT JOIN mst_jenis_pasien c ON b.jenispasien_id = c.jenispasien_id
      $where
      ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    $result =  $query->result_array();
    //
    foreach ($result as $key => $val) {
      $result[$key]['rinc'] = $this->list_ibs_pemeriksaan_rinc($val['pemeriksaan_id']);
    }
    return $result;
  }

  public function list_ibs_pemeriksaan_rinc($pemeriksaan_id = '')
  {
    $sql = "SELECT
              b.itemoperasi_nm
            FROM
              ibs_pemeriksaan_rinc a 
            LEFT JOIN mst_item_operasi b ON a.itemoperasi_id = b.itemoperasi_id
            WHERE
              a.pemeriksaan_id =? 
              AND a.is_deleted = 0 
            ORDER BY
              a.created_at";
    $query = $this->db->query($sql, $pemeriksaan_id);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM ibs_pemeriksaan a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT 
          *
        FROM ibs_pemeriksaan a
        JOIN reg_pasien b ON a.reg_id = b.reg_id
        JOIN mst_jenis_pasien c ON b.jenispasien_id = c.jenispasien_id
      $where";
    $query = $this->db->query($sql);
    return $query->num_rows();
  }

  function get_data($id)
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm, c.jenispasien_nm, d.lokasi_nm, d.map_lokasi_depo, e.kelas_nm
            FROM reg_pasien a 
            LEFT JOIN mst_pegawai b ON a.dokter_id=b.pegawai_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id=c.jenispasien_id
            LEFT JOIN mst_lokasi d ON a.lokasi_id = d.lokasi_id
            LEFT JOIN mst_kelas e ON a.kelas_id = e.kelas_id
            WHERE a.reg_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function get_alergi($pasien_id)
  {
    return $this->db->where('pasien_id', $pasien_id)->get('dat_catatanmedis')->result_array();
  }

  public function get_reg($reg_id)
  {
    return $this->db->where('reg_id', $reg_id)->get('reg_pasien')->row_array();
  }

  public function get_ibs_pemeriksaan($reg_id, $pemeriksaan_id)
  {
    return $this->db->where('reg_id', $reg_id)->where('pemeriksaan_id', $pemeriksaan_id)->get('ibs_pemeriksaan')->row_array();
  }

  // PERIKSA
  public function update_periksa_st($id)
  {
    $data['tgl_diperiksa'] = date('Y-m-d H:i:s');
    $data['periksa_st'] = 1;
    $this->db->where('pemeriksaan_id', $id)->update('ibs_pemeriksaan', $data);
  }

  public function get_data_periksa($id)
  {
    $sql = "SELECT 
              a.*, 
              b.pasien_nm, b.umur_thn, b.umur_bln, b.umur_hr, b.sex_cd, b.nik, 
              b.alamat, b.provinsi, b.kabupaten, b.kecamatan, b.kelurahan,
              b.asalpasien_cd,
              c.jenispasien_nm,
              d.pegawai_nm,
              e.map_lokasi_depo
            FROM ibs_pemeriksaan a 
            LEFT JOIN reg_pasien b ON a.reg_id = b.reg_id
            LEFT JOIN mst_jenis_pasien c ON b.jenispasien_id = c.jenispasien_id
            LEFT JOIN mst_pegawai d ON b.dokter_id = d.pegawai_id
            LEFT JOIN mst_lokasi e ON e.lokasi_id = '03.04'
            WHERE a.pemeriksaan_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function save_tindakan($reg_id, $pemeriksaan_id)
  {
    $data = $this->input->post();
    $this->db->where('reg_id', $reg_id)->where('pemeriksaan_id', $pemeriksaan_id)->update('ibs_pemeriksaan', $data);
  }

  //Tindakan 
  public function tindakan_data($pasien_id = '', $reg_id = '', $lokasi_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, 
              b.pegawai_nm AS pegawai_nm_1, 
              c.pegawai_nm AS pegawai_nm_2, 
              d.pegawai_nm AS pegawai_nm_3, 
              e.pegawai_nm AS pegawai_nm_4, 
              f.pegawai_nm AS pegawai_nm_5, 
              g.kelas_nm
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            LEFT JOIN mst_pegawai c ON a.petugas_id_2 = c.pegawai_id
            LEFT JOIN mst_pegawai d ON a.petugas_id_3 = d.pegawai_id
            LEFT JOIN mst_pegawai e ON a.petugas_id_4 = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_5 = f.pegawai_id
            LEFT JOIN mst_kelas g ON a.kelas_id = g.kelas_id 
            WHERE a.is_deleted = 0 AND a.pasien_id='$pasien_id' AND a.reg_id='$reg_id' AND a.lokasi_id='$lokasi_id' AND a.pemeriksaan_id='$pemeriksaan_id'
            ORDER BY a.tindakan_id";
    $query = $this->db->query($sql, array($pasien_id, $reg_id, $lokasi_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function tarifkelas_autocomplete($tarif_nm = null)
  {
    $sql = "SELECT 
              a.*,b.tarif_nm,c.kelas_nm
            FROM mst_tarif_kelas a
            JOIN mst_tarif b ON a.tarif_id = b.tarif_id
            JOIN mst_kelas c ON a.kelas_id = c.kelas_id
            WHERE b.tarif_nm LIKE '%$tarif_nm%' ";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['tarifkelas_id'],
        'text' => $row['tarifkelas_id'] . ' - ' . $row['tarif_nm'] . ' - ' . $row['kelas_nm']
      );
    }
    return $res;
  }

  public function petugas_autocomplete($petugas_nm = null)
  {
    $sql = "SELECT 
              a.*
            FROM mst_pegawai a
            WHERE a.pegawai_nm LIKE '%$petugas_nm%' ";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['pegawai_id'],
        'text' => $row['pegawai_nm']
      );
    }
    return $res;
  }

  public function tindakan_save()
  {
    $data = $this->input->post();
    $data['tgl_mulai'] = to_date($data['tgl_mulai'], '-', 'full_date');
    $data['tgl_selesai'] = to_date($data['tgl_selesai'], '-', 'full_date');
    unset($data['petugas_no']);
    $data['tgl_catat'] = date('Y-m-d H:i:s');
    $data['user_cd'] = $this->session->userdata('sess_user_cd');
    $tk = $this->db
      ->select('a.*, b.tarif_nm')
      ->join('mst_tarif b', 'a.tarif_id = b.tarif_id', 'left')
      ->where('a.tarifkelas_id', $data['tarifkelas_id'])
      ->get('mst_tarif_kelas a')->row_array();
    unset($data['tarifkelas_id']);
    $data['tarif_id'] = $tk['tarif_id'];
    $data['kelas_id'] = $tk['kelas_id'];
    $data['tarif_nm'] = $tk['tarif_nm'];
    $data['js'] = $tk['js'];
    $data['jp'] = $tk['jp'];
    $data['jb'] = $tk['jb'];
    $data['nom_tarif'] = $tk['nominal'];
    $data['jml_awal'] = $tk['nominal'] * $data['qty'];
    $data['jml_tagihan'] = $tk['nominal'] * $data['qty'];
    if ($data['tindakan_id'] == null) {
      $data['tindakan_id'] = get_id('dat_tindakan');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_tindakan', $data);
      update_id('dat_tindakan', $data['tindakan_id']);
    } else {
      // petugas
      $d = $data;
      unset($d['petugas_id'], $d['petugas_id_2'], $d['petugas_id_3'], $d['petugas_id_4'], $d['petugas_id_5']);
      $petugas = array();
      for ($i = 1; $i <= 5; $i++) {
        if ($i == '1') {
          $petugas_id = $data['petugas_id'];
        } else {
          $petugas_id = $data['petugas_id_' . $i];
        }

        if ($petugas_id != '0') {
          array_push($petugas, $petugas_id);
        }
      }

      for ($j = 0; $j < count($petugas); $j++) {
        $d['petugas_id_' . ($j + 1)] = $petugas[$j];
      }

      unset($data['petugas_id'], $data['petugas_id_2'], $data['petugas_id_3'], $data['petugas_id_4'], $data['petugas_id_5']);

      $data['petugas_id'] = @$d['petugas_id_1'];
      $data['petugas_id_2'] = @$d['petugas_id_2'];
      $data['petugas_id_3'] = @$d['petugas_id_3'];
      $data['petugas_id_4'] = @$d['petugas_id_4'];
      $data['petugas_id_5'] = @$d['petugas_id_5'];

      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('tindakan_id', $data['tindakan_id'])->update('dat_tindakan', $data);
    }
  }

  public function tindakan_get($tindakan_id = '', $reg_id = '', $pasien_id = '')
  {
    $sql = "SELECT 
              a.*, b.kelas_nm, 
              c.pegawai_nm AS pegawai_nm_1,
              d.pegawai_nm AS pegawai_nm_2,
              e.pegawai_nm AS pegawai_nm_3,
              f.pegawai_nm AS pegawai_nm_4,
              g.pegawai_nm AS pegawai_nm_5
            FROM dat_tindakan a
            LEFT JOIN mst_kelas b ON a.kelas_id = b.kelas_id
            LEFT JOIN mst_pegawai c ON a.petugas_id = c.pegawai_id 
            LEFT JOIN mst_pegawai d ON a.petugas_id_2 = d.pegawai_id 
            LEFT JOIN mst_pegawai e ON a.petugas_id_3 = e.pegawai_id 
            LEFT JOIN mst_pegawai f ON a.petugas_id_4 = f.pegawai_id 
            LEFT JOIN mst_pegawai g ON a.petugas_id_5 = g.pegawai_id 
            WHERE a.tindakan_id=? AND a.reg_id=? AND a.pasien_id=?";
    $query = $this->db->query($sql, array($tindakan_id, $reg_id, $pasien_id));
    $row = $query->row_array();
    return $row;
  }

  public function tindakan_delete($tindakan_id = '', $reg_id = '', $pasien_id = '')
  {
    trash('dat_tindakan', array(
      'tindakan_id' => $tindakan_id,
      'reg_id' => $reg_id,
      'pasien_id' => $pasien_id
    ));
    $this->db->where('tindakan_id', $tindakan_id)->where('reg_id', $reg_id)->where('pasien_id', $pasien_id)->delete('dat_tindakan');
  }

  public function tarifkelas_row($id)
  {
    $sql = "SELECT a.*, b.tarif_nm, c.kelas_nm FROM mst_tarif_kelas a
      LEFT JOIN mst_tarif b ON a.tarif_id = b.tarif_id
      LEFT JOIN mst_kelas c ON a.kelas_id = c.kelas_id
      WHERE tarifkelas_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  public function petugas_row($id)
  {
    $sql = "SELECT * FROM mst_pegawai WHERE pegawai_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  public function get_tindakan($tindakan_id = null)
  {
    $sql = "SELECT 
              a.*, 
              b.pegawai_nm AS pegawai_nm_1, 
              c.pegawai_nm AS pegawai_nm_2, 
              d.pegawai_nm AS pegawai_nm_3, 
              e.pegawai_nm AS pegawai_nm_4, 
              f.pegawai_nm AS pegawai_nm_5
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            LEFT JOIN mst_pegawai c ON a.petugas_id_2 = c.pegawai_id
            LEFT JOIN mst_pegawai d ON a.petugas_id_3 = d.pegawai_id
            LEFT JOIN mst_pegawai e ON a.petugas_id_4 = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_5 = f.pegawai_id
            WHERE a.tindakan_id=?";
    $query = $this->db->query($sql, $tindakan_id);
    return $query->row_array();
  }

  public function delete_petugas($tindakan_id = null, $form_name = null)
  {
    $data[$form_name] = '';
    $query = $this->db->where('tindakan_id', $tindakan_id)->update('dat_tindakan', $data);
    return $query;
  }

  // Pemberian BHP
  public function bhp_data($reg_id = '', $lokasi_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_bhp a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.lokasi_id=? AND a.pemeriksaan_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $lokasi_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function bhp_autocomplete($obat_nm = null)
  {
    $sql = "SELECT 
              *
            FROM mst_obat a   
            WHERE jenisbarang_cd = '03' AND (a.obat_nm LIKE '%$obat_nm%' OR a.obat_id LIKE '%$obat_nm%')
            ORDER BY a.obat_id";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['obat_id'] . "#" . $row['obat_nm'],
        'text' => $row['obat_id'] . ' - ' . $row['obat_nm']
      );
    }
    return $res;
  }

  public function bhp_get($bhp_id = '', $reg_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm, DATE_FORMAT(a.tgl_catat, '%Y-%m-%d %H:%i:%s') AS tgl_catat 
            FROM dat_bhp a 
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id
            WHERE a.bhp_id=? AND a.reg_id=?";
    $query = $this->db->query($sql, array($bhp_id, $reg_id));
    return $query->row_array();
  }

  public function bhp_save()
  {
    $data = $this->input->post();
    $barang_id = explode("#", $data['barang_id']);
    $data['barang_id'] = $barang_id[0];
    $data['barang_nm'] = $barang_id[1];
    $data['tgl_catat'] = to_date($data['tgl_catat'], '', 'full_date');
    $data['user_cd'] = $this->session->userdata('sess_user_cd');

    if ($data['bhp_id'] == null) {
      $data['bhp_id'] = get_id('dat_bhp');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_bhp', $data);
      update_id('dat_bhp', $data['bhp_id']);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('bhp_id', $data['bhp_id'])->update('dat_bhp', $data);
    }
  }

  public function bhp_delete($bhp_id = '', $reg_id = '')
  {
    trash('dat_bhp', array(
      'bhp_id' => $bhp_id,
      'reg_id' => $reg_id
    ));
    $this->db->where('bhp_id', $bhp_id)->where('reg_id', $reg_id)->delete('dat_bhp');
  }

  public function bhp_row($id)
  {
    $sql = "SELECT * FROM mst_obat WHERE obat_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  // Pemberian ALkes
  public function alkes_data($reg_id = '', $lokasi_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_alkes a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.lokasi_id=? AND a.pemeriksaan_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $lokasi_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function alkes_autocomplete($obat_nm = null)
  {
    $sql = "SELECT 
              *
            FROM mst_obat a   
            WHERE jenisbarang_cd = '02' AND (a.obat_nm LIKE '%$obat_nm%' OR a.obat_id LIKE '%$obat_nm%')
            ORDER BY a.obat_id";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['obat_id'] . "#" . $row['obat_nm'],
        'text' => $row['obat_id'] . ' - ' . $row['obat_nm']
      );
    }
    return $res;
  }

  public function alkes_get($alkes_id = '', $reg_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm, DATE_FORMAT(a.tgl_catat, '%Y-%m-%d %H:%i:%s') AS tgl_catat 
            FROM dat_alkes a 
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id
            WHERE a.alkes_id=? AND a.reg_id=?";
    $query = $this->db->query($sql, array($alkes_id, $reg_id));
    return $query->row_array();
  }

  public function alkes_save()
  {
    $data = $this->input->post();
    $barang_id = explode("#", $data['barang_id']);
    $data['barang_id'] = $barang_id[0];
    $data['barang_nm'] = $barang_id[1];
    $data['tgl_catat'] = to_date($data['tgl_catat'], '', 'full_date');
    $data['user_cd'] = $this->session->userdata('sess_user_cd');

    if ($data['alkes_id'] == null) {
      $data['alkes_id'] = get_id('dat_alkes');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_alkes', $data);
      update_id('dat_alkes', $data['alkes_id']);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('alkes_id', $data['alkes_id'])->update('dat_alkes', $data);
    }
  }

  public function alkes_delete($alkes_id = '', $reg_id = '')
  {
    trash('dat_alkes', array(
      'alkes_id' => $alkes_id,
      'reg_id' => $reg_id
    ));
    $this->db->where('alkes_id', $alkes_id)->where('reg_id', $reg_id)->delete('dat_alkes');
  }

  public function alkes_row($id)
  {
    $sql = "SELECT * FROM mst_obat WHERE obat_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  // Form
  public function pasien_row($id)
  {
    $sql = "SELECT * FROM mst_pasien WHERE pasien_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    // sebutan_cd
    if (@$row['sebutan_cd'] != '') {
      $row['pasien_nm'] =  $row['pasien_nm'] . ', ' . $row['sebutan_cd'];
    } else {
      if ($row['sex_cd'] == 'L') {
        $row['pasien_nm'] = $row['pasien_nm'] . ', Tn';
      } elseif ($row['sex_cd'] == 'P') {
        $row['pasien_nm'] = $row['pasien_nm'] . ', Ny';
      }
    }
    // jenis kelamin
    if ($row['sex_cd'] == 'L') {
      $row['jns_kelamin'] = 'Laki-laki';
    } elseif ($row['sex_cd'] == 'P') {
      $row['jns_kelamin'] = 'Perempuan';
    }
    return $row;
  }

  public function no_rm_row($id)
  {
    $sql = "SELECT * FROM mst_pasien WHERE pasien_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    $row['status_cari'] = (@$row != '') ? '1' : '0';
    if ($row['status_cari'] == 1) {
      // sebutan_cd
      if (@$row['sebutan_cd'] != '') {
        $row['pasien_nm'] =  $row['pasien_nm'] . ', ' . $row['sebutan_cd'];
      } else {
        if ($row['sex_cd'] == 'L') {
          $row['pasien_nm'] = $row['pasien_nm'] . ', Tn';
        } elseif ($row['sex_cd'] == 'P') {
          $row['pasien_nm'] = $row['pasien_nm'] . ', Ny';
        }
      }
      // jenis kelamin
      if ($row['sex_cd'] == 'L') {
        $row['jns_kelamin'] = 'Laki-laki';
      } elseif ($row['sex_cd'] == 'P') {
        $row['jns_kelamin'] = 'Perempuan';
      }
    }
    return $row;
  }

  public function list_tarif()
  {
    $sql = "SELECT
              tarif_id,
              parent_id,
              tarif_nm,
              tarif_tp,
              is_active 
            FROM
              `mst_tarif` 
            WHERE
              tarif_id LIKE '02.__%' -- TINDAKAN BEDAH -- 
              AND tarif_tp = 'D' 
              AND is_deleted = 0 
              AND is_active = 1";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function save_form($id = null)
  {
    $data = html_escape($this->input->post());
    die;
    // $data['tgl_order'] = to_date($data['tgl_order'], '-', 'full_date');
    // if ($id == null) {
    //   $data['pemeriksaan_id'] = get_id('ibs_pemeriksaan');
    //   $data['user_cd'] = $this->session->userdata('sess_user_cd');
    //   $data['created_at'] = date('Y-m-d H:i:s');
    //   $data['created_by'] = $this->session->userdata('sess_user_realname');
    //   $this->db->insert('ibs_pemeriksaan', $data);
    //   update_id('ibs_pemeriksaan', $data['pemeriksaan_id']);
    // } else {
    //   $data['user_cd'] = $this->session->userdata('sess_user_cd');
    //   $data['updated_at'] = date('Y-m-d H:i:s');
    //   $data['updated_by'] = $this->session->userdata('sess_user_realname');
    //   $this->db->where('pemeriksaan_id', $id)->update('ibs_pemeriksaan', $data);
    // }
  }
}
