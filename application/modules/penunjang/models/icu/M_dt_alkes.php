<?php 

class M_dt_alkes extends CI_Model {
 
  var $table = 'far_stok_depo a'; //nama tabel dari database
  var $column_order = array(null, 'obat_id', 'obat_nm', 'no_batch'); //field yang ada di table user
  var $column_search = array('obat_id', 'obat_nm', 'no_batch'); //field yang diizin untuk pencarian 
  var $order = array('obat_id' => 'asc'); // default order 
 
  public function __construct(){
    parent::__construct();
    $this->load->database();
  }
 
  public function _get_datatables_query($map_lokasi_depo){
    $this->db->where('a.lokasi_id', $map_lokasi_depo);
    $this->db->where('a.stok_st', 1);
    $this->db->where('a.stok_akhir >', 0);
    $this->db->where('a.jenisbarang_cd', '02');
    $this->db->order_by('a.tgl_expired', 'asc');
    $this->db->order_by('a.stok_akhir', 'asc');
    $this->db->from($this->table);
 
    $i = 0;
     
    foreach ($this->column_search as $item) // looping awal
    {
      if($_POST['search']['value']) // jika datatable mengirimkan pencarian dengan metode POST
      {
        if($i===0) // looping awal
        {
          $this->db->group_start(); 
          $this->db->like($item, $_POST['search']['value']);
        }
        else
        {
          $this->db->or_like($item, $_POST['search']['value']);
        }

        if(count($this->column_search) - 1 == $i) 
          $this->db->group_end(); 
      }
      $i++;
    }
         
    if(isset($_POST['order'])) 
    {
      $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    } 
    else if(isset($this->order))
    {
      $order = $this->order;
      $this->db->order_by(key($order), $order[key($order)]);
    }
  }
 
  function get_datatables($map_lokasi_depo)
  {
    $this->_get_datatables_query($map_lokasi_depo);
    if($_POST['length'] != -1)
    $this->db->limit($_POST['length'], $_POST['start']);
    $query = $this->db->get();
    return $query->result_array();
  }
 
  public function count_filtered($map_lokasi_depo)
  {
    $this->_get_datatables_query($map_lokasi_depo);
    $query = $this->db->get();
    return $query->num_rows();
  }
 
  public function count_all($map_lokasi_depo)
  {
    $this->db->from($this->table);
    return $this->db->count_all_results();
  }
 
}