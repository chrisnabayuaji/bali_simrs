<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_lab_datang extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 AND a.lokasi_id = '03.02' ";
    if (@$cookie['search']['tgl_registrasi_from'] != '' && @$cookie['search']['tgl_registrasi_to']) {
      $where .= "AND (DATE(d.tgl_order) BETWEEN '" . to_date(@$cookie['search']['tgl_registrasi_from']) . "' AND '" . to_date(@$cookie['search']['tgl_registrasi_to']) . "')";
    }
    if (@$cookie['search']['no_rm_nm'] != '') {
      $where .= "AND (a.pasien_id LIKE '%" . $this->db->escape_like_str($cookie['search']['no_rm_nm']) . "%' OR a.pasien_nm LIKE '%" . $this->db->escape_like_str($cookie['search']['no_rm_nm']) . "%' ) ";
    }
    if (@$cookie['search']['lokasi_id'] != '') {
      $where .= "AND a.lokasi_id = '" . $this->db->escape_like_str($cookie['search']['lokasi_id']) . "' ";
    }
    if (@$cookie['search']['jenispasien_id'] != '') {
      $where .= "AND a.jenispasien_id = '" . $this->db->escape_like_str($cookie['search']['jenispasien_id']) . "' ";
    }
    if (@$cookie['search']['periksa_st'] != '') {
      $where .= "AND a.periksa_st = '" . $this->db->escape_like_str($cookie['search']['periksa_st']) . "' ";
    }
    if (@$cookie['search']['pulang_st'] != '') {
      $where .= "AND a.pulang_st = '" . $this->db->escape_like_str($cookie['search']['pulang_st']) . "' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT a.*, b.lokasi_nm, c.jenispasien_nm, d.*, e.tagihan 
      FROM reg_pasien a
      JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
      JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
      JOIN lab_pemeriksaan d ON a.reg_id = d.reg_id
      LEFT JOIN (
        SELECT SUM(jml_tagihan) as tagihan, x.pemeriksaan_id AS pemeriksaan_id_dat_tindakan, x.reg_id FROM dat_tindakan x GROUP BY pemeriksaan_id, reg_id
      ) as e ON e.pemeriksaan_id_dat_tindakan = d.pemeriksaan_id AND e.reg_id = a.reg_id
      $where
      ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM reg_pasien a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM reg_pasien a
      JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
      JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
      JOIN lab_pemeriksaan d ON a.reg_id = d.reg_id
      $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function list_parameter_group()
  {
    $sql = "SELECT parameter_group FROM reg_pasien GROUP BY parameter_group ORDER BY parameter_group ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function get_data($id)
  {
    $sql = "SELECT * FROM reg_pasien WHERE reg_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function get_alergi($pasien_id)
  {
    return $this->db->where('pasien_id', $pasien_id)->get('dat_catatanmedis')->result_array();
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    $data['provinsi'] = get_wilayah($data['wilayah_prop'], 'name');
    $data['kabupaten'] = get_wilayah($data['wilayah_kab'], 'name');
    $data['kecamatan'] = get_wilayah($data['wilayah_kec'], 'name');
    $data['kelurahan'] = get_wilayah($data['wilayah_kel'], 'name');
    $data['wilayah_id'] = get_wilayah($data['wilayah_kel'], 'id');
    $data['tgl_lahir'] = to_date($data['tgl_lahir']);
    $data['tgl_registrasi'] = to_date($data['tgl_registrasi'], '-', 'full_date');
    $data['kelompokumur_id'] = get_kelompokumur($data['tgl_lahir']);
    unset($data['wilayah_prop'], $data['wilayah_kab'], $data['wilayah_kec'], $data['wilayah_kel']);
    if ($id == null) {
      $data['reg_id'] = get_id('reg_pasien');
      $data['is_billing'] = 1;
      $data['groupreg_id'] = $data['reg_id'];
      $data['groupreg_in'] = $data['reg_id'] . ';';
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('reg_pasien', $data);
      update_id('reg_pasien', $data['reg_id']);
      // insert mst_pasien
      if (@$data['statuspasien_cd'] == 'B') {
        $this->insert_mst_pasien($data);
      }
      // update reg_pasien_online
      if (@$data['regonline_id'] != '') {
        $this->update_reg_pasien_online($data);
      }
      // insert dat_tindakan
      $this->insert_dat_tindakan($data);
      return $data['reg_id'];
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('reg_id', $id)->update('reg_pasien', $data);
      return $id;
    }
  }

  public function insert_mst_pasien($data)
  {
    $data_mst_pasien['pasien_id'] = $data['pasien_id'];
    $data_mst_pasien['pasien_nm'] = $data['pasien_nm'];
    $data_mst_pasien['sebutan_cd'] = $data['sebutan_cd'];
    $data_mst_pasien['nik'] = $data['nik'];
    $data_mst_pasien['nama_kk'] = $data['nama_kk'];
    $data_mst_pasien['no_kk'] = $data['no_kk'];
    $data_mst_pasien['alamat'] = $data['alamat'];
    $data_mst_pasien['kode_pos'] = $data['kode_pos'];
    $data_mst_pasien['wilayah_st'] = $data['wilayah_st'];
    $data_mst_pasien['provinsi'] = $data['provinsi'];
    $data_mst_pasien['kabupaten'] = $data['kabupaten'];
    $data_mst_pasien['kecamatan'] = $data['kecamatan'];
    $data_mst_pasien['kelurahan'] = $data['kelurahan'];
    $data_mst_pasien['wilayah_id'] = $data['wilayah_id'];
    $data_mst_pasien['tmp_lahir'] = $data['tmp_lahir'];
    $data_mst_pasien['tgl_lahir'] = $data['tgl_lahir'];
    $data_mst_pasien['sex_cd'] = $data['sex_cd'];
    $data_mst_pasien['goldarah_cd'] = $data['goldarah_cd'];
    $data_mst_pasien['pendidikan_cd'] = $data['pendidikan_cd'];
    $data_mst_pasien['pekerjaan_cd'] = $data['pekerjaan_cd'];
    $data_mst_pasien['agama_cd'] = $data['agama_cd'];
    $data_mst_pasien['telp'] = $data['no_telp'];
    $data_mst_pasien['jenispasien_id'] = $data['jenispasien_id'];
    $data_mst_pasien['no_kartu'] = $data['no_kartu'];
    $data_mst_pasien['pasien_st'] = 1;
    $data_mst_pasien['tgl_catat'] = date('Y-m-d');
    $data_mst_pasien['created_at'] = date('Y-m-d H:i:s');
    $data_mst_pasien['created_by'] = $this->session->userdata('fullname');
    $this->db->insert('mst_pasien', $data_mst_pasien);
  }

  public function update_reg_pasien_online($data)
  {
    $data_reg_online['is_dilayani'] = 1;
    $data_reg_online['updated_at'] = date('Y-m-d H:i:s');
    $data_reg_online['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('regonline_id', $data['regonline_id'])->update('reg_pasien_online', $data_reg_online);
  }

  public function insert_dat_tindakan($data)
  {
    $count_admisi_dokter = $this->count_admisi_dokter($data['kelas_id'], $data['lokasi_id'], $data['dokter_id']);
    if ($count_admisi_dokter > 0) {
      $result_data = $this->get_result_data($data['kelas_id'], $data['lokasi_id'], $data['dokter_id']);
    } else {
      $result_data = $this->get_result_data($data['kelas_id'], $data['lokasi_id']);
    }

    foreach ($result_data as $value) {
      $data_dat_tindakan['tindakan_id'] = get_id('dat_tindakan');
      $data_dat_tindakan['reg_id'] = $data['reg_id'];
      $data_dat_tindakan['pasien_id'] = $data['pasien_id'];
      $data_dat_tindakan['lokasi_id'] = $data['lokasi_id'];
      $data_dat_tindakan['kelas_id'] = $data['kelas_id'];
      $data_dat_tindakan['tarif_id'] = $value['tarif_id'];
      $data_dat_tindakan['tarif_nm'] = $value['tarif_nm'];
      $data_dat_tindakan['js'] = $value['js'];
      $data_dat_tindakan['jp'] = $value['jp'];
      $data_dat_tindakan['jb'] = $value['jb'];
      $data_dat_tindakan['nom_tarif'] = $value['nominal'];
      $data_dat_tindakan['qty'] = 1;
      $data_dat_tindakan['jml_awal'] = $value['nominal'] * 1;
      $data_dat_tindakan['jml_tagihan'] = $value['nominal'] * 1;
      $data_dat_tindakan['jenistindakan_cd'] = 1;
      $data_dat_tindakan['petugas_id'] = $data['dokter_id'];
      $data_dat_tindakan['tgl_catat'] = date('Y-m-d H:i:s');
      $data_dat_tindakan['created_at'] = date('Y-m-d H:i:s');
      $data_dat_tindakan['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_tindakan', $data_dat_tindakan);
      update_id('dat_tindakan', $data_dat_tindakan['tindakan_id']);
    }
  }

  public function count_admisi_dokter($kelas_id = null, $lokasi_id = null, $dokter_id = null)
  {
    $sql = "SELECT 
              COUNT(*) AS count_data
            FROM mst_tarif_admisi 
            WHERE kelas_id='$kelas_id' AND lokasi_id='$lokasi_id' AND dokter_id='$dokter_id'";
    $query = $this->db->query($sql, array($kelas_id, $lokasi_id, $dokter_id));
    $row = $query->row_array();
    return $row['count_data'];
  }

  public function get_result_data($kelas_id = null, $lokasi_id = null, $dokter_id = null)
  {
    $where = "";
    if ($dokter_id != null) {
      $where .= "AND a.dokter_id='$dokter_id'";
    }
    $sql = "SELECT 
              a.*, b.js, b.jp, b.jb, b.nominal, c.tarif_nm
            FROM mst_tarif_admisi a
            JOIN mst_tarif_kelas b ON a.tarif_id = b.tarif_id AND a.kelas_id = b.kelas_id
            JOIN mst_tarif c ON a.tarif_id = c.tarif_id
            WHERE a.kelas_id=? AND a.lokasi_id=? $where";
    $query = $this->db->query($sql, array($kelas_id, $lokasi_id));
    $result = $query->result_array();
    return $result;
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('lokasi_id', $id)->update('reg_pasien', $data);
  }

  public function delete($id, $pemeriksaan_id, $permanent = false)
  {
    trash('reg_pasien', array('reg_id' => $id));
    if ($permanent) {
      // delete table reg_pasien
      $this->db->where('reg_id', $id)->delete('reg_pasien');
      // delete table lab_pemeriksaan
      $this->db->where('pemeriksaan_id', $pemeriksaan_id)->delete('lab_pemeriksaan');
      // delete table lab_pemeriksaan_rinc
      $this->db->where('pemeriksaan_id', $pemeriksaan_id)->delete('lab_pemeriksaan_rinc');
    } else {
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('reg_id', $id)->update('reg_pasien', $data);
    }
  }

  public function online_data()
  {
    $sql = "SELECT * FROM reg_pasien_online";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function online_row($id)
  {
    $sql = "SELECT * FROM reg_pasien_online WHERE regonline_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->result_array();
    // sebutan_cd
    if ($row['sex_cd'] == 'L') {
      $row['sebutan_cd'] = 'Tn';
    } elseif ($row['sex_cd'] == 'P') {
      $row['sebutan_cd'] = 'Ny';
    }
    return $row;
  }

  public function pasien_data()
  {
    $sql = "SELECT * FROM mst_pasien";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function pasien_row($id)
  {
    $sql = "SELECT * FROM mst_pasien WHERE pasien_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    // sebutan_cd
    if (@$row['sebutan_cd'] != '') {
      $row['sebutan_cd'] = $row['sebutan_cd'];
    } else {
      if ($row['sex_cd'] == 'L') {
        $row['sebutan_cd'] = 'Tn';
      } elseif ($row['sex_cd'] == 'P') {
        $row['sebutan_cd'] = 'Ny';
      }
    }
    return $row;
  }

  public function no_rm_row($id)
  {
    $sql = "SELECT * FROM mst_pasien WHERE pasien_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    $row['status_cari'] = (@$row != '') ? '1' : '0';
    $row['statuspasien_cd'] = (@$row['statuspasien_cd'] != '') ? @$row['statuspasien_cd'] : 'B';
    $row['wilayah_st'] = (@$row['wilayah_st'] != '') ? @$row['wilayah_st'] : 'D';
    $row['wilayah_id'] = (@$row['wilayah_id'] != '') ? @$row['wilayah_id'] : '';
    $row['tgl_lahir'] = (@$row['tgl_lahir'] != '') ? @$row['tgl_lahir'] : date('Y-m-d');
    $row['sex_cd'] = (@$row['sex_cd'] != '') ? @$row['sex_cd'] : 'L';
    $row['pendidikan_cd'] = (@$row['pendidikan_cd'] != '') ? @$row['pendidikan_cd'] : '00';
    $row['pekerjaan_cd'] = (@$row['pekerjaan_cd'] != '') ? @$row['pekerjaan_cd'] : '00';
    $row['agama_cd'] = (@$row['agama_cd'] != '') ? @$row['agama_cd'] : '00';
    $row['jenispasien_id'] = (@$row['jenispasien_id'] != '') ? @$row['jenispasien_id'] : '01';
    // sebutan_cd
    if (@$row['sebutan_cd'] != '') {
      $row['sebutan_cd'] = $row['sebutan_cd'];
    } else {
      if ($row['sex_cd'] == 'L') {
        $row['sebutan_cd'] = 'Tn';
      } elseif ($row['sex_cd'] == 'P') {
        $row['sebutan_cd'] = 'Ny';
      }
    }
    return $row;
  }

  public function get_caller()
  {
    $sql = "SELECT a.* FROM lkt_antrian a WHERE a.tgl_antrian='" . date('Y-m-d') . "' AND a.lokasi_id=?";
    $query = $this->db->query($sql, array('01.01'));
    $row = $query->row_array();
    return $row;
  }

  public function laboratorium_save($reg_id = null, $id = null)
  {
    $d = $this->input->post();
    $data = $d;

    unset($data['checkitem']);
    unset($data['laboratorium_table_length']);

    $data['tgl_order'] = to_date($data['tgl_order'], '-', 'full_date');
    if ($id == null) {
      $data['pemeriksaan_id'] = get_id('lab_pemeriksaan');
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      unset($data['kelas_id']);
      $this->db->insert('lab_pemeriksaan', $data);
      update_id('lab_pemeriksaan', $data['pemeriksaan_id']);
    } else {
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      unset($data['kelas_id']);
      $this->db->where('pemeriksaan_id', $id)->update('lab_pemeriksaan', $data);
    }

    $old = $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->get('lab_pemeriksaan_rinc')->result_array();
    $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->delete('lab_pemeriksaan_rinc');
    if (isset($d['checkitem'])) {
      foreach ($d['checkitem'] as $v) {
        $item = $this->db->query("SELECT * FROM mst_item_lab WHERE itemlab_id LIKE '$v%' ")->result_array();
        foreach ($item as $row) {
          $dr = array(
            'pemeriksaanrinc_id' => get_id('lab_pemeriksaan_rinc'),
            'pemeriksaan_id' => $data['pemeriksaan_id'],
            'itemlab_id' => $row['itemlab_id'],
            'kelas_id' => $d['kelas_id'],
            'is_tagihan' => $row['is_tagihan'],
            'is_periksa' => $row['is_periksa'],
            'tarif_id' => $row['tarif_id'],
            'user_cd' => $this->session->userdata('sess_user_cd'),
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => $this->session->userdata('sess_user_realname'),
          );
          $this->db->insert('lab_pemeriksaan_rinc', $dr);
          update_id('lab_pemeriksaan_rinc', $dr['pemeriksaanrinc_id']);
        }
      }
    }

    //update rincian
    foreach ($old as $row) {
      $check = $this->db->where('itemlab_id', $row['itemlab_id'])->where('pemeriksaan_id', $data['pemeriksaan_id'])->get('lab_pemeriksaan_rinc')->row_array();
      if ($check != null) {
        $do = array(
          'tgl_hasil' => $row['tgl_hasil'],
          'hasil_lab' => $row['hasil_lab'],
          'catatan_lab' => $row['catatan_lab'],
          'user_cd' => $row['user_cd'],
          'created_at' => $row['created_at'],
          'created_by' => $row['created_by'],
          'updated_at' => date('Y-m-d H:i:s'),
          'updated_by' => $this->session->userdata('sess_user_realname'),
        );
        $this->db->where('itemlab_id', $row['itemlab_id'])->where('pemeriksaan_id', $data['pemeriksaan_id'])->update('lab_pemeriksaan_rinc', $do);
      }
    }

    //insert into dat_tindakan
    $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->where('lokasi_id', '03.02')->delete('dat_tindakan');
    $reg = $this->db->where('reg_id', $reg_id)->get('reg_pasien')->row_array();

    if (isset($d['checkitem'])) {
      foreach ($d['checkitem'] as $v) {
        $item = $this->db->where('itemlab_id', $v)->get('mst_item_lab')->row_array();
        // get mst_tarif_kelas
        $get_tarif_kelas = $this->get_tarif_kelas($item['tarif_id'], $reg['kelas_id']);
        // insert dat_tindakan
        $data_tindakan['tindakan_id'] = get_id('dat_tindakan');
        $data_tindakan['reg_id'] = $reg['reg_id'];
        $data_tindakan['pemeriksaan_id'] = $data['pemeriksaan_id'];
        $data_tindakan['pasien_id'] = $reg['pasien_id'];
        $data_tindakan['lokasi_id'] = $reg['lokasi_id'];
        $data_tindakan['kelas_id'] = $reg['kelas_id'];
        $data_tindakan['tarif_id'] = $item['tarif_id'];
        $data_tindakan['tarif_nm'] = $get_tarif_kelas['tarif_nm'];
        $data_tindakan['js'] = $get_tarif_kelas['js'];
        $data_tindakan['jp'] = $get_tarif_kelas['jp'];
        $data_tindakan['jb'] = $get_tarif_kelas['jb'];
        $data_tindakan['nom_tarif'] = $get_tarif_kelas['nominal'];
        $data_tindakan['qty'] = 1;
        $data_tindakan['jml_awal'] = $get_tarif_kelas['nominal'];
        $data_tindakan['jml_tagihan'] = $get_tarif_kelas['nominal'];
        $data_tindakan['user_cd'] = $this->session->userdata('sess_user_cd');
        $data_tindakan['tgl_catat'] = date('Y-m-d H:i:s');
        $data_tindakan['created_at'] = date('Y-m-d H:i:s');
        $data_tindakan['created_by'] = $this->session->userdata('sess_user_realname');

        $this->db->insert('dat_tindakan', $data_tindakan);
        update_id('dat_tindakan', $data_tindakan['tindakan_id']);
      }
    }
  }

  public function laboratorium_get($pemeriksaan_id = '')
  {
    $sql = "SELECT 
            a.*, b.pegawai_nm as dokter_nm,
            c.lokasi_nm,
            d.pegawai_nm as dokterpj_nm, 
            e.pegawai_nm as pemeriksa_nm 
          FROM lab_pemeriksaan a
          LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
          LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
          LEFT JOIN mst_pegawai d ON a.dokterpj_id = d.pegawai_id 
          LEFT JOIN mst_pegawai e ON a.pemeriksa_id = e.pegawai_id 
          WHERE pemeriksaan_id='$pemeriksaan_id'";
    $query = $this->db->query($sql, array($pemeriksaan_id));
    return $query->row_array();
  }

  // PERIKSA

  public function update_periksa_st($id)
  {
    $data['periksa_st'] = 1;
    $this->db->where('reg_id', $id)->update('reg_pasien', $data);
  }

  public function get_tgl_hasil($id)
  {
    $sql = "SELECT 
              a.tgl_hasil
            FROM lab_pemeriksaan_rinc a 
            WHERE a.pemeriksaan_id=?
            GROUP BY a.pemeriksaan_id";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function get_data_periksa($id)
  {
    $sql = "SELECT 
              a.*, 
              b.*, 
              b.periksa_st AS periksa_st_lab, 
              c.pegawai_nm, 
              d.jenispasien_nm, 
              e.lokasi_nm, 
              e.map_lokasi_depo, 
              f.kelas_nm, 
              g.pegawai_id AS dokterpj_id, 
              g.pegawai_nm AS dokterpj_nm 
            FROM reg_pasien a 
            LEFT JOIN lab_pemeriksaan b ON a.reg_id = b.reg_id
            LEFT JOIN mst_pegawai c ON a.dokter_id = c.pegawai_id
            LEFT JOIN mst_jenis_pasien d ON a.jenispasien_id = d.jenispasien_id
            LEFT JOIN mst_lokasi e ON a.lokasi_id = e.lokasi_id
            LEFT JOIN mst_kelas f ON a.kelas_id = f.kelas_id
            LEFT JOIN mst_pegawai g ON b.dokterpj_id = g.pegawai_id
            WHERE b.pemeriksaan_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function save_hasil_pemeriksaan($reg_id = null, $pemeriksaan_id = null)
  {
    $data = html_escape($this->input->post());
    //
    // if ($data['periksa_st'] == '1') {
    // $data_lab_pemeriksaan['tgl_diperiksa'] = date('Y-m-d H:i:s');
    // $data_lab_pemeriksaan['periksa_st'] = 1;
    // $data_lab_pemeriksaan['dokterpj_id'] = $data['dokterpj_id'];
    // $data_lab_pemeriksaan['pemeriksa_id'] = $data['pemeriksa_id'];
    // $this->db->where('pemeriksaan_id', $pemeriksaan_id);
    // $this->db->update('lab_pemeriksaan', $data_lab_pemeriksaan);
    // }

    $data_lab_pemeriksaan['tgl_diperiksa'] = date('Y-m-d H:i:s');
    $data_lab_pemeriksaan['periksa_st'] = $data['periksa_st'];
    $data_lab_pemeriksaan['dokterpj_id'] = $data['dokterpj_id'];
    $data_lab_pemeriksaan['pemeriksa_id'] = $data['pemeriksa_id'];
    $this->db->where('pemeriksaan_id', $pemeriksaan_id);
    $this->db->update('lab_pemeriksaan', $data_lab_pemeriksaan);

    // delete dat_tindakan
    // $this->db->where('reg_id', $reg_id)->where('pemeriksaan_id', $data['pemeriksaan_id'])->where('pasien_id', $data['pasien_id'])->where('lokasi_id', '03.02')->where('kelas_id', $data['kelas_id'])->delete('dat_tindakan');

    foreach ($data['pemeriksaanrinc_id'] as $key => $val) {
      // update data_lab_pemeriksaan_rinc
      $data_lab_pemeriksaan_rinc['tgl_hasil'] = to_date($data['tgl_hasil'], '', 'full_date');
      $data_lab_pemeriksaan_rinc['hasil_lab'] = clear_numeric($data['hasil_lab'][$key]);
      $data_lab_pemeriksaan_rinc['catatan_lab'] = $data['catatan_lab'][$key];
      $data_lab_pemeriksaan_rinc['updated_at'] = date('Y-m-d H:i:s');
      $data_lab_pemeriksaan_rinc['updated_by'] = $this->session->userdata('sess_user_realname');

      $this->db->where('pemeriksaanrinc_id', $val);
      $this->db->update('lab_pemeriksaan_rinc', $data_lab_pemeriksaan_rinc);
      // if ($data['tarif_id'][$key] != '') {
      //   // get mst_tarif_kelas
      //   $get_tarif_kelas = $this->get_tarif_kelas($data['tarif_id'][$key], $data['kelas_id']);
      //   // insert dat_tindakan
      //   $data_tindakan['tindakan_id'] = get_id('dat_tindakan');
      //   $data_tindakan['reg_id'] = $reg_id;
      //   $data_tindakan['pemeriksaan_id'] = $data['pemeriksaan_id'];
      //   $data_tindakan['pasien_id'] = $data['pasien_id'];
      //   $data_tindakan['lokasi_id'] = '03.02';
      //   $data_tindakan['kelas_id'] = $data['kelas_id'];
      //   $data_tindakan['tarif_id'] = $data['tarif_id'][$key];
      //   $data_tindakan['tarif_nm'] = $get_tarif_kelas['tarif_nm'];
      //   $data_tindakan['js'] = $get_tarif_kelas['js'];
      //   $data_tindakan['jp'] = $get_tarif_kelas['jp'];
      //   $data_tindakan['jb'] = $get_tarif_kelas['jb'];
      //   $data_tindakan['nom_tarif'] = $get_tarif_kelas['nominal'];
      //   $data_tindakan['qty'] = 1;
      //   $data_tindakan['jml_awal'] = $get_tarif_kelas['nominal'];
      //   $data_tindakan['jml_tagihan'] = $get_tarif_kelas['nominal'];
      //   $data_tindakan['petugas_id'] = $data['petugas_id'];
      //   $data_tindakan['user_cd'] = $this->session->userdata('sess_user_cd');
      //   $data_tindakan['created_at'] = date('Y-m-d H:i:s');
      //   $data_tindakan['created_by'] = $this->session->userdata('sess_user_realname');

      //   $this->db->insert('dat_tindakan', $data_tindakan);
      //   update_id('dat_tindakan', $data_tindakan['tindakan_id']);
      // }
    }
  }

  public function get_tarif_kelas($tarif_id = '', $kelas_id = '')
  {
    // $sql = "SELECT 
    //         a.*, b.tarif_nm
    //       FROM mst_tarif_kelas a
    //       LEFT JOIN mst_tarif b ON a.tarif_id = b.tarif_id
    //       WHERE a.tarif_id=? AND a.kelas_id=?";
    $sql = "SELECT
              a.*,
              b.tarifkelas_id,
              b.js,
              b.jp,
              b.jb,
              b.nominal 
            FROM
              mst_tarif a
              LEFT JOIN mst_tarif_kelas b ON a.tarif_id = b.tarif_id AND b.kelas_id = '$kelas_id'
            WHERE
              a.tarif_id=?";
    $query = $this->db->query($sql, $tarif_id);
    return $query->row_array();
  }

  // Hasil Pemeriksaan --------------------------------------
  public function hasil_pemeriksaan_data($pemeriksaan_id = '')
  {
    $query = $this->db->query(
      "SELECT b.parent_id FROM lab_pemeriksaan_rinc a
      JOIN mst_item_lab b ON a.itemlab_id = b.itemlab_id
      WHERE a.pemeriksaan_id = '$pemeriksaan_id'
      GROUP BY b.parent_id
      ORDER BY b.parent_id"
    )->result_array();
    $res = null;
    if ($query != null) {
      $res = array();
      foreach ($query as $k => $v) {
        $res[$k] = $this->db->query(
          "SELECT * FROM mst_item_lab a
          LEFT JOIN lab_pemeriksaan_rinc b ON a.itemlab_id = b.itemlab_id AND b.pemeriksaan_id = '$pemeriksaan_id'
          WHERE a.itemlab_id='" . $v['parent_id'] . "'"
        )->row_array();
        $res[$k]['rinc'] = $this->db->query(
          "SELECT a.*, 
            b.itemlab_nm, 
            b.tipe_rujukan,
            b.konfirmasi, 
            b.rentang_min, b.rentang_max,
            b.rentang_l_min, b.rentang_l_max, 
            b.rentang_p_min, b.rentang_p_max,
            b.kurang, b.kurang_l, b.kurang_p,
            b.lebih, b.lebih_l, b.lebih_p,
            b.satuan,
            b.informasi 
          FROM lab_pemeriksaan_rinc a
          JOIN mst_item_lab b ON a.itemlab_id = b.itemlab_id
          WHERE a.is_periksa = 1 AND b.parent_id = '" . $v['parent_id'] . "' AND a.pemeriksaan_id = '" . $pemeriksaan_id . "'
          ORDER BY a.itemlab_id ASC"
        )->result_array();
      }
    }
    return $res;
  }

  public function get_data_by_reg_id($id)
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm, c.jenispasien_nm, d.lokasi_nm 
            FROM reg_pasien a 
            LEFT JOIN mst_pegawai b ON a.dokter_id=b.pegawai_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id=c.jenispasien_id
            LEFT JOIN mst_lokasi d ON a.lokasi_id = d.lokasi_id
            WHERE a.reg_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function hp_laboratorium_get($pemeriksaan_id = '')
  {
    $sql = "SELECT 
            a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
          FROM lab_pemeriksaan a
          LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
          LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
          WHERE pemeriksaan_id=?";
    $query = $this->db->query($sql, array($pemeriksaan_id));
    return $query->row_array();
  }

  public function hp_laboratorium_save($reg_id = null, $id = null)
  {
    $d = $this->input->post();
    $data = $d;

    unset($data['checkitem']);
    unset($data['laboratorium_table_length']);

    // delete all rincian
    $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->delete('lab_pemeriksaan_rinc');
    if (isset($d['checkitem'])) {
      foreach ($d['checkitem'] as $key => $val) {
        $item = $this->db->where('itemlab_id', $val)->get('mst_item_lab')->row_array();
        $dr = array(
          'pemeriksaanrinc_id' => get_id('lab_pemeriksaan_rinc'),
          'pemeriksaan_id' => $data['pemeriksaan_id'],
          'itemlab_id' => $item['itemlab_id'],
          'kelas_id' => $d['kelas_id'],
          'tarif_id' => $item['tarif_id'],
          'nilai_normal' => $item['nilai_normal'],
          'user_cd' => $this->session->userdata('sess_user_cd'),
          'created_at' => date('Y-m-d H:i:s'),
          'created_by' => $this->session->userdata('sess_user_realname'),
        );
        $this->db->insert('lab_pemeriksaan_rinc', $dr);
        update_id('lab_pemeriksaan_rinc', $dr['pemeriksaanrinc_id']);
      }
    }

    if (isset($d['itemlab_id'])) {
      foreach ($d['itemlab_id'] as $keys => $vals) {
        $data_lab_pemeriksaan_rinc['tgl_hasil'] = ($data['tgl_hasil_' . $vals] != '') ? $data['tgl_hasil_' . $vals] : null;
        $data_lab_pemeriksaan_rinc['hasil_lab'] = dbl_hashtage_to_space($data['hasil_lab_' . $vals]);
        $data_lab_pemeriksaan_rinc['catatan_lab'] = dbl_hashtage_to_space($data['catatan_lab_' . $vals]);
        //
        $where = ['pemeriksaan_id' => $data['pemeriksaan_id'], 'itemlab_id' => under_to_point($vals)];
        $this->db->where($where);
        $this->db->update('lab_pemeriksaan_rinc', $data_lab_pemeriksaan_rinc);
      }
    }
  }

  public function hasil_pemeriksaan_delete($pemeriksaanrinc_id = '', $pemeriksaan_id = '')
  {
    $rincian = $this->db->where('pemeriksaanrinc_id', $pemeriksaanrinc_id)->get('lab_pemeriksaan_rinc')->row_array();
    //delete tindakan
    $this->db->where('pemeriksaan_id', $rincian['pemeriksaan_id'])->where('tarif_id', $rincian['tarif_id'])->delete('dat_tindakan');
    // delete pemeriksaan rincian
    $this->db->query("DELETE FROM lab_pemeriksaan_rinc WHERE itemlab_id LIKE '" . $rincian['itemlab_id'] . "%' AND pemeriksaan_id = '" . $rincian['pemeriksaan_id'] . "'");
  }

  public function get_lab_pemeriksaan_rinc($pemeriksaanrinc_id = '')
  {
    $sql = "SELECT 
            a.*, b.itemlab_nm
          FROM lab_pemeriksaan_rinc a
          LEFT JOIN mst_item_lab b ON a.itemlab_id=b.itemlab_id
          WHERE a.pemeriksaanrinc_id=?";
    $query = $this->db->query($sql, array($pemeriksaanrinc_id));
    return $query->row_array();
  }

  public function itemlab_autocomplete($itemlab_nm = null)
  {
    $sql = "SELECT 
              *
            FROM mst_item_lab a   
            WHERE (a.itemlab_nm LIKE '%$itemlab_nm%')
            ORDER BY a.itemlab_id ASC";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      if ($row['parent_id'] != '') {
        $res[] = array(
          'id' => $row['itemlab_id'],
          'text' => $row['itemlab_nm']
        );
      }
    }
    return $res;
  }

  public function datang_save_edit()
  {
    $data = $this->input->post();
    $item = $this->db->where('itemlab_id', $data['itemlab_id'])->get('mst_item_lab')->row_array();

    $data['tarif_id'] = $item['tarif_id'];
    $data['nilai_normal'] = $item['nilai_normal'];
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('sess_user_realname');
    $this->db->where('pemeriksaanrinc_id', $data['pemeriksaanrinc_id'])->update('lab_pemeriksaan_rinc', $data);
  }

  // Tarif Tindakan --------------------------------------
  public function tarif_tindakan_data($reg_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT a.*, b.pegawai_nm 
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            WHERE a.is_deleted=0 AND a.lokasi_id = '03.02'
            AND a.reg_id=? AND a.pemeriksaan_id=?";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }


  // Pemberian BHP
  public function bhp_data($reg_id = '', $lokasi_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_bhp a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.lokasi_id=? AND a.pemeriksaan_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $lokasi_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function bhp_autocomplete($obat_nm = null, $map_lokasi_depo = null)
  {
    $sql = "SELECT 
            *
          FROM far_stok_depo a
          WHERE 
            (a.obat_nm LIKE '%$obat_nm%' OR a.obat_id LIKE '%$obat_nm%') AND
            a.stok_st = 1 AND a.stok_akhir > 0 AND a.lokasi_id = '$map_lokasi_depo' 
            AND a.jenisbarang_cd = '03'
          ORDER BY a.obat_id";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['obat_id'] . "#" . $row['obat_nm'],
        'text' => $row['obat_id'] . ' - ' . $row['obat_nm']
      );
    }
    return $res;
  }

  public function bhp_get($bhp_id = '', $reg_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm, DATE_FORMAT(a.tgl_catat, '%Y-%m-%d %H:%i:%s') AS tgl_catat 
            FROM dat_bhp a 
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id
            WHERE a.bhp_id=? AND a.reg_id=?";
    $query = $this->db->query($sql, array($bhp_id, $reg_id));
    return $query->row_array();
  }

  public function bhp_save()
  {
    $data = $this->input->post();
    $barang_id = explode("#", $data['barang_id']);
    $data['barang_id'] = $barang_id[0];
    $data['barang_nm'] = $barang_id[1];
    $data['tgl_catat'] = to_date($data['tgl_catat'], '', 'full_date');
    $data['user_cd'] = $this->session->userdata('sess_user_cd');

    if ($data['bhp_id'] == null) {
      $data['bhp_id'] = get_id('dat_bhp');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_bhp', $data);
      update_id('dat_bhp', $data['bhp_id']);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('bhp_id', $data['bhp_id'])->update('dat_bhp', $data);
    }
  }

  public function bhp_delete($bhp_id = '', $reg_id = '')
  {
    trash('dat_bhp', array(
      'bhp_id' => $bhp_id,
      'reg_id' => $reg_id
    ));
    $this->db->where('bhp_id', $bhp_id)->where('reg_id', $reg_id)->delete('dat_bhp');
  }

  public function bhp_row($id)
  {
    $sql = "SELECT * FROM mst_obat WHERE obat_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  // Pemberian ALkes
  public function alkes_data($reg_id = '', $lokasi_id = '', $pemeriksaan_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_alkes a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.lokasi_id=? AND a.pemeriksaan_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $lokasi_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function alkes_autocomplete($obat_nm = null, $map_lokasi_depo = null)
  {
    $sql = "SELECT 
            *
          FROM far_stok_depo a
          WHERE 
            (a.obat_nm LIKE '%$obat_nm%' OR a.obat_id LIKE '%$obat_nm%') AND
            a.stok_st = 1 AND a.stok_akhir > 0 AND a.lokasi_id = '$map_lokasi_depo' 
            AND a.jenisbarang_cd = '02'
          ORDER BY a.obat_id";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['obat_id'] . "#" . $row['obat_nm'],
        'text' => $row['obat_id'] . ' - ' . $row['obat_nm']
      );
    }
    return $res;
  }

  public function alkes_get($alkes_id = '', $reg_id = '')
  {
    $sql = "SELECT 
              a.*, b.obat_nm, DATE_FORMAT(a.tgl_catat, '%Y-%m-%d %H:%i:%s') AS tgl_catat 
            FROM dat_alkes a 
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id
            WHERE a.alkes_id=? AND a.reg_id=?";
    $query = $this->db->query($sql, array($alkes_id, $reg_id));
    return $query->row_array();
  }

  public function alkes_save()
  {
    $data = $this->input->post();
    $barang_id = explode("#", $data['barang_id']);
    $data['barang_id'] = $barang_id[0];
    $data['barang_nm'] = $barang_id[1];
    $data['tgl_catat'] = to_date($data['tgl_catat'], '', 'full_date');
    $data['user_cd'] = $this->session->userdata('sess_user_cd');

    if ($data['alkes_id'] == null) {
      $data['alkes_id'] = get_id('dat_alkes');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_alkes', $data);
      update_id('dat_alkes', $data['alkes_id']);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('alkes_id', $data['alkes_id'])->update('dat_alkes', $data);
    }
  }

  public function alkes_delete($alkes_id = '', $reg_id = '')
  {
    trash('dat_alkes', array(
      'alkes_id' => $alkes_id,
      'reg_id' => $reg_id
    ));
    $this->db->where('alkes_id', $alkes_id)->where('reg_id', $reg_id)->delete('dat_alkes');
  }

  public function alkes_row($id)
  {
    $sql = "SELECT * FROM mst_obat WHERE obat_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  public function laboratorium_identitas_pasien_get($reg_id)
  {
    $sql = "SELECT * FROM reg_pasien WHERE reg_id = '$reg_id'";
    $query = $this->db->query($sql);
    return $query->row_array();
  }

  public function hasil_lab($data)
  {
    // itemlab
    $rinc = $this->db->where('pemeriksaanrinc_id', $data['pemeriksaanrinc_id'])->get('lab_pemeriksaan_rinc')->row_array();
    $itemlab = $this->db->where('itemlab_id', $rinc['itemlab_id'])->get('mst_item_lab')->row_array();
    //pasien
    $pemeriksaan = $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->get('lab_pemeriksaan')->row_array();
    $reg = $this->db->where('reg_id', $pemeriksaan['reg_id'])->get('reg_pasien')->row_array();
    switch ($itemlab['tipe_rujukan']) {
      case 1:
        if ($data['hasil_lab'] == 1) {
          return "+";
        } else if ($data['hasil_lab'] == 0) {
          return "-";
        } else {
          return "";
        };
        break;

      case 2:
        if ($data['hasil_lab'] < $itemlab['rentang_min']) {
          return 'Low';
        } elseif ($data['hasil_lab'] >= $itemlab['rentang_min'] && $data['hasil_lab'] <= $itemlab['rentang_max']) {
          return 'Normal';
        } elseif ($data['hasil_lab'] > $itemlab['rentang_max']) {
          return 'High';
        }
        break;

      case 3:
        if ($reg['sex_cd'] == 'L') {
          if ($data['hasil_lab'] < $itemlab['rentang_l_min']) {
            return 'Low';
          } elseif ($data['hasil_lab'] >= $itemlab['rentang_l_min'] && $data['hasil_lab'] <= $itemlab['rentang_l_max']) {
            return 'Normal';
          } elseif ($data['hasil_lab'] > $itemlab['rentang_l_max']) {
            return 'High';
          }
        } else {
          if ($data['hasil_lab'] < $itemlab['rentang_p_min']) {
            return 'Low';
          } elseif ($data['hasil_lab'] >= $itemlab['rentang_p_min'] && $data['hasil_lab'] <= $itemlab['rentang_p_max']) {
            return 'Normal';
          } elseif ($data['hasil_lab'] > $itemlab['rentang_p_max']) {
            return 'High';
          }
        }
        break;

      case 4:
        if ($data['hasil_lab'] <= $itemlab['kurang']) {
          return 'Normal';
        } else {
          return 'High';
        }
        break;

      case 5:
        if ($reg['sex_cd'] == 'L') {
          if ($data['hasil_lab'] <= $itemlab['kurang_l']) {
            return 'Normal';
          } else {
            return 'High';
          }
        } else {
          if ($data['hasil_lab'] <= $itemlab['kurang_p']) {
            return 'Normal';
          } else {
            return 'High';
          }
        }
        break;

      case 6:
        if ($data['hasil_lab'] >= $itemlab['lebih']) {
          return 'Normal';
        } else {
          return 'Low';
        }
        break;

      case 7:
        if ($reg['sex_cd'] == 'L') {
          if ($data['hasil_lab'] >= $itemlab['lebih_l']) {
            return 'Normal';
          } else {
            return 'Low';
          }
        } else {
          if ($data['hasil_lab'] >= $itemlab['lebih_p']) {
            return 'Normal';
          } else {
            return 'Low';
          }
        }
        break;

      case 8:
        return '';
        break;
    }
  }

  public function tambah_item($data)
  {
    //insert dat_pemeriksaan_detail
    $item = $this->db->query("SELECT * FROM mst_item_lab WHERE itemlab_id LIKE '" . $data['itemlab_id'] . "%' ")->result_array();
    foreach ($item as $row) {
      $dr = array(
        'pemeriksaanrinc_id' => get_id('lab_pemeriksaan_rinc'),
        'pemeriksaan_id' => $data['pemeriksaan_id'],
        'itemlab_id' => $row['itemlab_id'],
        'kelas_id' => $data['kelas_id'],
        'is_tagihan' => $row['is_tagihan'],
        'is_periksa' => $row['is_periksa'],
        'tarif_id' => $row['tarif_id'],
        'user_cd' => $this->session->userdata('sess_user_cd'),
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $this->session->userdata('sess_user_realname'),
      );
      $this->db->insert('lab_pemeriksaan_rinc', $dr);
      update_id('lab_pemeriksaan_rinc', $dr['pemeriksaanrinc_id']);
    }

    //insert dat_tindakan
    $lab = $this->db->where('itemlab_id', $data['itemlab_id'])->get('mst_item_lab')->row_array();
    // get mst_tarif_kelas
    $get_tarif_kelas = $this->get_tarif_kelas($lab['tarif_id'], $data['kelas_id']);
    // insert dat_tindakan
    $data_tindakan['tindakan_id'] = get_id('dat_tindakan');
    $data_tindakan['reg_id'] = $data['reg_id'];
    $data_tindakan['pemeriksaan_id'] = $data['pemeriksaan_id'];
    $data_tindakan['pasien_id'] = $data['pasien_id'];
    $data_tindakan['lokasi_id'] = '03.02';
    $data_tindakan['kelas_id'] = $data['kelas_id'];
    $data_tindakan['tarif_id'] = $lab['tarif_id'];
    $data_tindakan['tarif_nm'] = $get_tarif_kelas['tarif_nm'];
    $data_tindakan['js'] = $get_tarif_kelas['js'];
    $data_tindakan['jp'] = $get_tarif_kelas['jp'];
    $data_tindakan['jb'] = $get_tarif_kelas['jb'];
    $data_tindakan['nom_tarif'] = $get_tarif_kelas['nominal'];
    $data_tindakan['qty'] = 1;
    $data_tindakan['jml_awal'] = $get_tarif_kelas['nominal'];
    $data_tindakan['jml_tagihan'] = $get_tarif_kelas['nominal'];
    $data_tindakan['user_cd'] = $this->session->userdata('sess_user_cd');
    $data_tindakan['tgl_catat'] = date('Y-m-d H:i:s');
    $data_tindakan['created_at'] = date('Y-m-d H:i:s');
    $data_tindakan['created_by'] = $this->session->userdata('sess_user_realname');

    $this->db->insert('dat_tindakan', $data_tindakan);
    update_id('dat_tindakan', $data_tindakan['tindakan_id']);
  }


  public function cek_item($data)
  {
    return $this->db
      ->where('pemeriksaan_id', $data['pemeriksaan_id'])
      ->where('itemlab_id', $data['itemlab_id'])
      ->get('lab_pemeriksaan_rinc')->row_array();
  }
}
