<script type="text/javascript">
  var tindakan_form;
  $(document).ready(function() {
    var tindakan_form = $("#tindakan_form").validate({
      rules: {
        tarifkelas_id: {
          valueNotEquals: ""
        },
        petugas_id: {
          valueNotEquals: ""
        },
        qty: {
          valueNotEquals: ""
        }
      },
      messages: {
        tarifkelas_id: "Pilih salah satu!",
        petugas_id: "Pilih salah satu!",
        qty: "Kosong!"
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('autocomplete')) {
          error.insertAfter(element.next(".select2-container"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $("#tindakan_action").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#tindakan_action").attr("disabled", "disabled");
        $("#tindakan_cancel").attr("disabled", "disabled");
        var pasien_id = $("#pasien_id").val();
        var reg_id = $("#reg_id").val();
        var lokasi_id = $("#lokasi_id").val();
        var src_lokasi_id = $("#src_lokasi_id").val();
        var pemeriksaan_id = $("#pemeriksaan_id").val();
        $.ajax({
          type: 'post',
          url: '<?= site_url($nav['nav_url']) ?>/ajax_tindakan/save',
          data: $(form).serialize() + '&pasien_id=' + pasien_id + '&reg_id=' + reg_id + '&lokasi_id=' + lokasi_id + '&src_lokasi_id=' + src_lokasi_id + '&pemeriksaan_id=' + pemeriksaan_id,
          success: function(data) {
            tindakan_reset();
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil disimpan.',
              icon: 'success',
              position: 'top-right'
            })
            $("#tindakan_action").html('<i class="fas fa-save"></i> Simpan');
            $("#tindakan_action").attr("disabled", false);
            $("#tindakan_cancel").attr("disabled", false);

            //reset petugas
            // var data = {
            //   id: '',
            //   text: '- Pilih -'
            // };
            // var petugasOpt = new Option(data.text, data.id, false, false);
            // for (let index = 0; index < 5; index++) {
            //   if (index == 0) {  
            //     $('#petugas_id').append(petugasOpt).trigger('change');
            //     $('#petugas_id').val(data.id);
            //     $('#petugas_id').trigger('change');
            //   }else{
            //     $('#petugas_id_'+index).append(petugasOpt).trigger('change');
            //     $('#petugas_id_'+index).val(data.id);
            //     $('#petugas_id_'+index).trigger('change');
            //   }
            // }

            tindakan_data(pasien_id, reg_id, lokasi_id, pemeriksaan_id);
          }
        })
        return false;
      }
    });

    var form = $("#form-data").validate({
      rules: {},
      messages: {},
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    // datepicker
    $('.datepicker').daterangepicker({
      // maxDate: new Date(),
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })
    $('.datetimepicker').daterangepicker({
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY HH:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })

    // autocomplete
    $('#tarifkelas_id').select2({
      minimumInputLength: 2,
      ajax: {
        url: "<?= site_url($nav['nav_url']) ?>/ajax_tindakan/tarifkelas_autocomplete",
        dataType: "json",

        data: function(params) {
          return {
            tarif_nm: params.term
          };
        },
        processResults: function(data) {
          return {
            results: data
          }
        }
      }
    });


    var pasien_id = $("#pasien_id").val();
    var reg_id = $("#reg_id").val();
    var lokasi_id = $("#lokasi_id").val();
    var pemeriksaan_id = $("#pemeriksaan_id").val();
    tindakan_data(pasien_id, reg_id, lokasi_id, pemeriksaan_id);

    $("#tindakan_cancel").on('click', function() {
      tindakan_reset('cancel');
    });

    $('.modal-href').click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");

      $("#modal-title").html(modal_title);
      $("#modal-size").addClass('modal-' + modal_size);
      if (modal_custom_size) {
        $("#modal-size").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
      }
      $("#myModal").modal('show');
      $.post($(this).data('href'), function(data) {
        $("#modal-body").html(data.html);
      }, 'json');
    });
  })

  function tindakan_data(pasien_id = '', reg_id = '', lokasi_id = '', pemeriksaan_id = '') {
    $('#tindakan_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_tindakan/data' ?>', {
      pasien_id: pasien_id,
      reg_id: reg_id,
      lokasi_id: lokasi_id,
      pemeriksaan_id: pemeriksaan_id
    }, function(data) {
      $('#tindakan_data').html(data.html);
    }, 'json');
  }

  function tindakan_reset(type = '') {
    $("#tindakan_id").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#tarifkelas_id").val('').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#keterangan_tindakan").val('').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#box_petugas").html('');
    $("#qty").val('').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#jenistindakan_cd").val('1').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $('#tindakan_action').html('<i class="fas fa-save"></i> Simpan');
    if (type == 'cancel') {
      //reset petugas
      var data = {
        id: '',
        text: '- Pilih -'
      };
      var petugasOpt = new Option(data.text, data.id, false, false);
      for (let index = 0; index <= 5; index++) {
        if (index == 0) {
          $('#petugas_id').append(petugasOpt).trigger('change');
          $('#petugas_id').val(data.id);
          $('#petugas_id').trigger('change');
        } else {
          $('#petugas_id_' + index).append(petugasOpt).trigger('change');
          $('#petugas_id_' + index).val(data.id);
          $('#petugas_id_' + index).trigger('change');
        }
      }
    }
  }

  function tarifkelas_fill(id) {
    $.ajax({
      type: 'post',
      url: '<?= site_url($nav['nav_url'] . '/ajax_tindakan/tarifkelas_fill') ?>',
      dataType: 'json',
      data: 'tarifkelas_id=' + id,
      success: function(data) {
        // autocomplete
        var tarifKelasData = {
          id: data.tarif_id + '.' + data.kelas_id,
          text: data.tarif_id + '.' + data.kelas_id + ' - ' + data.tarif_nm + ' - ' + data.kelas_nm
        };
        var tarifKelasOpt = new Option(tarifKelasData.text, tarifKelasData.id, false, false);
        $('#tarifkelas_id').append(tarifKelasOpt).trigger('change');
        $('#tarifkelas_id').val(data.tarif_id + '.' + data.kelas_id);
        $('#tarifkelas_id').trigger('change');

        $('.select2-container').css('width', '100%');
        $("#myModal").modal('hide');
      }
    })
  }
</script>