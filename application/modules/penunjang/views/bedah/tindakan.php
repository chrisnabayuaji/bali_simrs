<?php $this->load->view('_js_tindakan'); ?>
<div class="row">
  <div class="col-md-5">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-syringe"></i> Tindakan</h4>
        <form id="tindakan_form" action="" method="post" autocomplete="off">
          <input type="hidden" name="tindakan_id" id="tindakan_id">
          <script>
            function petugas_fill(id, form_name) {
              $.ajax({
                type: 'post',
                url: '<?= site_url($nav['nav_url'] . '/ajax_tindakan/petugas_fill') ?>',
                dataType: 'json',
                data: 'pegawai_id=' + id,
                success: function(data) {
                  // autocomplete
                  var petugasData = {
                    id: data.pegawai_id,
                    text: data.pegawai_nm
                  };
                  var petugasOpt = new Option(petugasData.text, petugasData.id, false, false);
                  $('#' + form_name).append(petugasOpt).trigger('change');
                  $('#' + form_name).val(data.pegawai_id);
                  $('#' + form_name).trigger('change');

                  $('.select2-container').css('width', '100%');
                  $("#myModal").modal('hide');
                }
              })
            }
          </script>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Operator/Dokter <span class="text-danger">*</span></label>
            <div class="col-lg-6 col-md-6">
              <select class="form-control autocomplete" name="petugas_id" id="petugas_id">
                <option value="">- Pilih -</option>
              </select>
            </div>
            <div class="col-lg-1 col-md-1">
              <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_tindakan/search_petugas/petugas_id' ?>" modal-title="List Data Petugas" modal-size="lg" class="btn btn-xs btn-default modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Petugas" data-original-title="Cari Petugas"><i class="fas fa-search"></i></a>
            </div>
            <script>
              $(document).ready(function() {
                $('#petugas_id').select2({
                  minimumInputLength: 2,
                  ajax: {
                    url: "<?= site_url($nav['nav_url']) ?>/ajax_tindakan/petugas_autocomplete",
                    dataType: "json",

                    data: function(params) {
                      return {
                        petugas_nm: params.term
                      };
                    },
                    processResults: function(data) {
                      return {
                        results: data
                      }
                    }
                  }
                });
                $('.select2-container').css('width', '100%');
              })
            </script>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Anastesi <span class="text-danger">*</span></label>
            <div class="col-lg-6 col-md-6">
              <select class="form-control autocomplete" name="petugas_id_2" id="petugas_id_2">
                <option value="">- Pilih -</option>
              </select>
            </div>
            <div class="col-lg-1 col-md-1">
              <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_tindakan/search_petugas/petugas_id_2' ?>" modal-title="List Data Petugas" modal-size="lg" class="btn btn-xs btn-default modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Petugas" data-original-title="Cari Petugas"><i class="fas fa-search"></i></a>
            </div>
            <script>
              $(document).ready(function() {
                $('#petugas_id_2').select2({
                  minimumInputLength: 2,
                  ajax: {
                    url: "<?= site_url($nav['nav_url']) ?>/ajax_tindakan/petugas_autocomplete",
                    dataType: "json",

                    data: function(params) {
                      return {
                        petugas_nm: params.term
                      };
                    },
                    processResults: function(data) {
                      return {
                        results: data
                      }
                    }
                  }
                });
                $('.select2-container').css('width', '100%');
              })
            </script>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Perawat Bedah <span class="text-danger">*</span></label>
            <div class="col-lg-6 col-md-6">
              <select class="form-control autocomplete" name="petugas_id_3" id="petugas_id_3">
                <option value="">- Pilih -</option>
              </select>
            </div>
            <div class="col-lg-1 col-md-1">
              <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_tindakan/search_petugas/petugas_id_3' ?>" modal-title="List Data Petugas" modal-size="lg" class="btn btn-xs btn-default modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Petugas" data-original-title="Cari Petugas"><i class="fas fa-search"></i></a>
            </div>
            <script>
              $(document).ready(function() {
                $('#petugas_id_3').select2({
                  minimumInputLength: 2,
                  ajax: {
                    url: "<?= site_url($nav['nav_url']) ?>/ajax_tindakan/petugas_autocomplete",
                    dataType: "json",

                    data: function(params) {
                      return {
                        petugas_nm: params.term
                      };
                    },
                    processResults: function(data) {
                      return {
                        results: data
                      }
                    }
                  }
                });
                $('.select2-container').css('width', '100%');
              })
            </script>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Dokter Konsul <span class="text-danger">*</span></label>
            <div class="col-lg-6 col-md-6">
              <select class="form-control autocomplete" name="petugas_id_4" id="petugas_id_4">
                <option value="">- Pilih -</option>
              </select>
            </div>
            <div class="col-lg-1 col-md-1">
              <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_tindakan/search_petugas/petugas_id_4' ?>" modal-title="List Data Petugas" modal-size="lg" class="btn btn-xs btn-default modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Petugas" data-original-title="Cari Petugas"><i class="fas fa-search"></i></a>
            </div>
            <script>
              $(document).ready(function() {
                $('#petugas_id_4').select2({
                  minimumInputLength: 2,
                  ajax: {
                    url: "<?= site_url($nav['nav_url']) ?>/ajax_tindakan/petugas_autocomplete",
                    dataType: "json",

                    data: function(params) {
                      return {
                        petugas_nm: params.term
                      };
                    },
                    processResults: function(data) {
                      return {
                        results: data
                      }
                    }
                  }
                });
                $('.select2-container').css('width', '100%');
              })
            </script>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Perawat Lainnya <span class="text-danger">*</span></label>
            <div class="col-lg-6 col-md-6">
              <select class="form-control autocomplete" name="petugas_id_5" id="petugas_id_5">
                <option value="">- Pilih -</option>
              </select>
            </div>
            <div class="col-lg-1 col-md-1">
              <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_tindakan/search_petugas/petugas_id_5' ?>" modal-title="List Data Petugas" modal-size="lg" class="btn btn-xs btn-default modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Petugas" data-original-title="Cari Petugas"><i class="fas fa-search"></i></a>
            </div>
            <script>
              $(document).ready(function() {
                $('#petugas_id_5').select2({
                  minimumInputLength: 2,
                  ajax: {
                    url: "<?= site_url($nav['nav_url']) ?>/ajax_tindakan/petugas_autocomplete",
                    dataType: "json",

                    data: function(params) {
                      return {
                        petugas_nm: params.term
                      };
                    },
                    processResults: function(data) {
                      return {
                        results: data
                      }
                    }
                  }
                });
                $('.select2-container').css('width', '100%');
              })
            </script>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Waktu Mulai <span class="text-danger">*</span></label>
            <div class="col-lg-5 col-md-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                </div>
                <input type="text" class="form-control datetimepicker" name="tgl_mulai" id="tgl_mulai" value="<?= date('d-m-Y H:i:s') ?>">
              </div>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Waktu Selesai <span class="text-danger">*</span></label>
            <div class="col-lg-5 col-md-3">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                </div>
                <input type="text" class="form-control datetimepicker" name="tgl_selesai" id="tgl_selesai" value="<?= date('d-m-Y H:i:s') ?>">
              </div>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Tindakan <span class="text-danger">*</span></label>
            <div class="col-lg-8 col-md-3">
              <select class="form-control autocomplete" name="tarifkelas_id" id="tarifkelas_id">
                <option value="">- Pilih -</option>
              </select>
            </div>
            <div class="col-lg-1 col-md-1">
              <a href="javascript:void(0)" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/ajax_tindakan/search_tarifkelas/' . $reg['kelas_id'] ?>" modal-title="List Data Tindakan" modal-size="lg" class="btn btn-xs btn-default modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Tindakan" data-original-title="Cari Tindakan"><i class="fas fa-search"></i></a>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Qty <span class="text-danger">*</span></label>
            <div class="col-lg-3 col-md-3">
              <input type="number" class="form-control" name="qty" id="qty" required>
            </div>
            <label class="col-lg-2 col-md-2 col-form-label">Jenis <span class="text-danger">*</span></label>
            <div class="col-lg-3 col-md-3">
              <select class="form-control chosen-select" name="jenistindakan_cd" id="jenistindakan_cd">
                <?php foreach (get_parameter('jenistindakan_cd') as $r) : ?>
                  <option value="<?= $r['parameter_cd'] ?>" <?= (@$main['jenistindakan_cd'] == $r['parameter_cd']) ? 'selected' : ''; ?>><?= $r['parameter_val'] ?></option>
                <?php endforeach; ?>
              </select>
            </div>
          </div>
          <div class="row pt-2">
            <div class="col-lg-9 offset-lg-3 p-0">
              <button type="submit" id="tindakan_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
              <button type="button" id="tindakan_cancel" class="btn btn-xs btn-secondary"><i class="fas fa-times"></i> Batal</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-7">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-list"></i> List Data Tindakan</h4>
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-sm">
            <thead>
              <tr>
                <th class="text-center" width="20">No</th>
                <th class="text-center" width="60">Aksi</th>
                <th class="text-center" width="100">Kode</th>
                <th class="text-center">Tindakan</th>
                <th class="text-center">Qty</th>
                <th class="text-center">Jenis</th>
                <th class="text-center">Biaya</th>
                <th class="text-center">Petugas</th>
              </tr>
            </thead>
            <tbody id="tindakan_data"></tbody>
          </table>
        </div>
        <form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?= site_url($nav['nav_url'] . '/save_tindakan/' . $reg_id . '/' . $pemeriksaan_id) ?>" class="needs-validation" novalidate autocomplete="off">
          <div class="row mt-4">
            <div class="col-md-5 col-sm-5">
              <div class="form-group row mb-1">
                <label class="col-lg-5 col-md-5 col-form-label mt-n3">Status Terlaksana</label>
                <div class="col-lg-7 col-md-7">
                  <select class="form-control chosen-select" name="proses_st" id="proses_st" required>
                    <option value="1" <?= (@$main['proses_st'] == 1) ? 'selected' : ''; ?>>Sudah</option>
                    <option value="0" <?= (@$main['proses_st'] == 0) ? 'selected' : ''; ?>>Belum</option>
                    <option value="2" <?= (@$main['proses_st'] == 2) ? 'selected' : ''; ?>>Batal</option>
                  </select>
                  <div class="text-danger mt-n1 mb-2" style="font-size: 11px;">*Wajib diisi</div>
                </div>
              </div>
              <div class="form-group row mb-1">
                <label class="col-lg-5 col-md-5 col-form-label mt-n3"></label>
                <div class="col-lg-7 col-md-7">
                  <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>