<?php if(@$main == null):?>
	<tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else: ?>
	<?php 
    $i=1;
    $tot_biaya = 0;
    foreach($main as $row):
    $tot_biaya += $row['jml_tagihan'];
  ?>
	<tr>
		<td class="text-center align-top"><?=$i++?></td>
		<td class="text-center align-top">
		  <a href="javascript:void(0)" class="btn btn-primary btn-table btn-edit" data-tindakan-id="<?=$row['tindakan_id']?>" data-reg-id="<?=$row['reg_id']?>" data-pasien-id="<?=$row['pasien_id']?>" title="Ubah Data"><i class="fas fa-pencil-alt"></i></a>
		  <a href="javascript:void(0)" class="btn btn-danger btn-table btn-delete" data-tindakan-id="<?=$row['tindakan_id']?>" data-reg-id="<?=$row['reg_id']?>" data-pasien-id="<?=$row['pasien_id']?>" data-lokasi-id="<?=$row['lokasi_id']?>" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
		</td>
    <td class="text-center align-top"><?=$row['tarif_id']?></td>
    <td class="text-left align-top"><?=$row['tarif_nm']?> - <?=$row['kelas_nm']?></td>
    <td class="text-center align-top"><?=$row['qty']?></td>
    <td class="text-center align-top"><?=get_parameter_value('jenistindakan_cd', $row['jenistindakan_cd'])?></td>
    <td class="text-right align-top"><?=num_id($row['jml_tagihan'])?></td>
    <td class="text-left">
      <?=($row['pegawai_nm_1'] !='') ? '<b>Operator/Dokter</b> :<br>- '.$row['pegawai_nm_1'].'<br>' : ''?>
      <?=($row['pegawai_nm_2'] !='') ? '<b>Anastesi</b> :<br>- '.$row['pegawai_nm_2'].'<br>' : ''?>
      <?=($row['pegawai_nm_3'] !='') ? '<b>Perawat Bedah</b> :<br>- '.$row['pegawai_nm_3'].'<br>' : ''?>
      <?=($row['pegawai_nm_4'] !='') ? '<b>Dokter Konsul</b> :<br>- '.$row['pegawai_nm_4'].'<br>' : ''?>
      <?=($row['pegawai_nm_5'] !='') ? '<b>Perawat Lainnya</b> :<br>- '.$row['pegawai_nm_5'].'' : ''?>
    </td>
	</tr>
	<?php endforeach; ?>
  <tr>
    <td colspan="6" class="text-right"><b>Total Biaya</b></td>
    <td class="text-right"><b><?=num_id($tot_biaya)?></b></td>
  </tr>
<?php endif; ?>

<script type="text/javascript">
$(document).ready(function () {
  // edit
  $('.btn-edit').bind('click',function(e) {
    e.preventDefault();
    var tindakan_id = $(this).attr("data-tindakan-id");
    var reg_id = $(this).attr("data-reg-id");
    var pasien_id = $(this).attr("data-pasien-id");

    //
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_tindakan/get_data'?>', {tindakan_id: tindakan_id, reg_id: reg_id, pasien_id: pasien_id}, function (data) {
      $('#tindakan_id').val(data.main.tindakan_id);

      $('#box_petugas').html('');

      // autocomplete
      var tarifKelasData = {
        id: data.main.tarif_id+'.'+data.main.kelas_id,
        text: data.main.tarif_id+'.'+data.main.kelas_id+' - '+data.main.tarif_nm+' - '+data.main.kelas_nm
      };
      var tarifKelasOpt = new Option(tarifKelasData.text, tarifKelasData.id, false, false);
      $('#tarifkelas_id').append(tarifKelasOpt).trigger('change');
      $('#tarifkelas_id').val(data.main.tarif_id+'.'+data.main.kelas_id);
      $('#tarifkelas_id').trigger('change');
      //petugas 1 edit
      var petugasData;
      if (data.main.petugas_id == null) {  
        petugasData = {
          id: '',
          text: '- Pilih -'
        };
      }else{
        petugasData = {
          id: data.main.petugas_id,
          text: data.main.pegawai_nm_1
        }
      }
      var petugasOpt = new Option(petugasData.text, petugasData.id, false, false);
      $('#petugas_id').append(petugasOpt).trigger('change');
      $('#petugas_id').val(petugasData.id);
      $('#petugas_id').trigger('change');
      // petugas 2
      var petugasData2;
      if (data.main.petugas_id_2 == null) {  
        petugasData2 = {
          id: '',
          text: '- Pilih -'
        };
      }else{
        petugasData2 = {
          id: data.main.petugas_id_2,
          text: data.main.pegawai_nm_2
        }
      }
      var petugasOpt2 = new Option(petugasData2.text, petugasData2.id, false, false);
      $('#petugas_id_2').append(petugasOpt2).trigger('change');
      $('#petugas_id_2').val(petugasData2.id);
      $('#petugas_id_2').trigger('change');
      // petugas 3
      var petugasData3;
      if (data.main.petugas_id_3 == null) {  
        petugasData3 = {
          id: '',
          text: '- Pilih -'
        };
      }else{
        petugasData3 = {
          id: data.main.petugas_id_3,
          text: data.main.pegawai_nm_3
        }
      }
      var petugasOpt3 = new Option(petugasData3.text, petugasData3.id, false, false);
      $('#petugas_id_3').append(petugasOpt3).trigger('change');
      $('#petugas_id_3').val(petugasData3.id);
      $('#petugas_id_3').trigger('change');
      // petugas 4
      var petugasData4;
      if (data.main.petugas_id_4 == null) {  
        petugasData4 = {
          id: '',
          text: '- Pilih -'
        };
      }else{
        petugasData4 = {
          id: data.main.petugas_id_4,
          text: data.main.pegawai_nm_4
        }
      }
      var petugasOpt4 = new Option(petugasData4.text, petugasData4.id, false, false);
      $('#petugas_id_4').append(petugasOpt4).trigger('change');
      $('#petugas_id_4').val(petugasData4.id);
      $('#petugas_id_4').trigger('change');
      // petugas 5
      var petugasData5;
      if (data.main.petugas_id_5 == null) {  
        petugasData5 = {
          id: '',
          text: '- Pilih -'
        };
      }else{
        petugasData5 = {
          id: data.main.petugas_id_5,
          text: data.main.pegawai_nm_5
        }
      }
      var petugasOpt5 = new Option(petugasData5.text, petugasData5.id, false, false);
      $('#petugas_id_5').append(petugasOpt5).trigger('change');
      $('#petugas_id_5').val(petugasData5.id);
      $('#petugas_id_5').trigger('change');

      $("#qty").val(data.main.qty).removeClass("is-valid").removeClass("is-invalid");
      var tgl_mulai = to_date(data.main.tgl_mulai, '-', 'full_date', ' ');
      $("#tgl_mulai").val(tgl_mulai).removeClass("is-valid").removeClass("is-invalid");
      var tgl_selesai = to_date(data.main.tgl_selesai, '-', 'full_date', ' ');
      $("#tgl_selesai").val(tgl_selesai).removeClass("is-valid").removeClass("is-invalid");
      $("#keterangan_tindakan").val(data.main.keterangan_tindakan).removeClass("is-valid").removeClass("is-invalid");
      $("#jenistindakan_cd").val(data.main.jenistindakan_cd).trigger('change').removeClass("is-valid").removeClass("is-invalid");
      $('.select2-container').css('width', '100%');
      $('#tindakan_action').html('<i class="fas fa-save"></i> Ubah');
    }, 'json');
  });

  // delete
  $('.btn-delete').on('click', function (e) {
    e.preventDefault();
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#eb3b5a',
      cancelButtonColor: '#b2bec3',
      confirmButtonText: 'Hapus',
      cancelButtonText: 'Batal',
      customClass: 'swal-wide'
    }).then((result) => {
      if (result.value) {
        var tindakan_id = $(this).attr("data-tindakan-id");
		    var reg_id = $(this).attr("data-reg-id");
		    var pasien_id = $(this).attr("data-pasien-id");
		    var lokasi_id = $(this).attr("data-lokasi-id");
		    //
		    $('#tindakan_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
		    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_tindakan/delete_data'?>', {tindakan_id: tindakan_id, reg_id: reg_id, pasien_id: pasien_id, lokasi_id: lokasi_id}, function (data) {
		    	$.toast({
            heading: 'Sukses',
            text: 'Data berhasil dihapus.',
            icon: 'success',
            position: 'top-right'
          })
		      $('#tindakan_data').html(data.html);
		    }, 'json');
      }
    })
  })
});
</script>