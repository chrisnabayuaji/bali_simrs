<script type="text/javascript">
  $(document).ready(function() {
    var form = $("#form-data").validate({
      rules: {},
      messages: {},
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-n2 mb-2');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    $('.chosen-select').on('select2:select', function(e) {
      form.element(this);
    });

    var typing_timer;
    $('#pasien_id').on('keyup', function() {
      clearTimeout(typing_timer);
      typing_timer = setTimeout(typing_done, 700);
    });

    $('#pasien_id').on('keydown', function() {
      clearTimeout(typing_timer);
    });

    function typing_done() {
      var id = $('#pasien_id').val();
      $('#no-rm-used').addClass('d-none');
      no_rm_fill(id);
    }

    $('#tarif_id').bind('change', function(e) {
      e.preventDefault();
      var i = $(this).val();
      _get_kelas(i);
    })

    $('#cari_rm').on('click', function() {
      var id = $('#pasien_id').val();
      if (id == '') {
        $.toast({
          heading: 'Error',
          text: 'No Rekam Medis belum diisi!',
          icon: 'error',
          position: 'top-right'
        });
      } else {
        no_rm_fill(id);
      }
    });
  })

  function pasien_fill(id) {
    $.ajax({
      type: 'post',
      url: '<?= site_url($nav['nav_url'] . '/ajax_form/pasien_fill') ?>',
      dataType: 'json',
      data: 'pasien_id=' + id,
      success: function(data) {
        $("#pasien_id").val(data.pasien_id);
        $("#pasien_nm").val(data.pasien_nm);
        $("#nik").val(data.nik);
        $("#jns_kelamin").val(data.jns_kelamin);
        $("#myModal").modal('hide');
      }
    })
  }

  function zero_fill(str, max) {
    str = str.toString();
    return str.length < max ? zero_fill("0" + str, max) : str;
  }

  function ucfirst(str) {
    str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
      return letter.toUpperCase();
    });
    return str;
  }

  function str_left(str, pad) {
    var pad = pad.toString() + str.toString();
    var ans = pad.substr(0, pad.length - str.length) + str;
    return ans;
  }

  function no_rm_fill(id, pasien_id = '') {
    $("#pasien_id_loading").removeClass('d-none');
    var status_sumber = $("#status-sumber").html();
    $.ajax({
      type: 'post',
      url: '<?= site_url($nav['nav_url'] . '/ajax_form/no_rm_fill') ?>',
      dataType: 'json',
      data: 'pasien_id=' + id,
      success: function(data) {
        if (status_sumber == 'cari-nik') {
          if (data.status_cari == '1') {
            fill_form(data, pasien_id);
          }
        } else if (status_sumber == 'cari-online') {
          if (data.status_cari == '1') {
            fill_form(data, pasien_id);
          }
        } else {
          fill_form(data, pasien_id);
        }
        $("#pasien_id_loading").addClass('d-none');
      }
    })
  }

  function fill_form(data, pasien_id = '') {
    if (data.status_cari == '1') {
      $('#no-rm-used').removeClass('d-none');
    } else {
      $.toast({
        heading: 'Error',
        text: 'No RM tidak ditemukan!',
        icon: 'error',
        position: 'top-right'
      });
    }
    $("#pasien_id").val(data.pasien_id);
    $("#pasien_nm").val(data.pasien_nm);
    $("#nik").val(data.nik);
    $("#jns_kelamin").val(data.jns_kelamin);
    if (pasien_id == 'true') {
      $("#nik_loading").addClass('d-none');
      $("#btn-search-nik").html('<i class="fas fa-search"></i> Cari NIK');
      $("#btn-search-nik").removeAttr("disabled");
    }
  }

  function _get_kelas(i, j) {
    $.post('<?= site_url($nav['nav_url'] . '/ajax_form/get_kelas') ?>', {
      tarif_id: i,
      kelas_id: j
    }, function(data) {
      $('#box_kelas').html(data.html);
    }, 'json');
  }

  function reset_form() {
    $("#pasien_id").val('');
    $("#sebutan_cd").val('TN').trigger('change');
    $("#pasien_nm").val('');
    $("#nik").val('');
  }
</script>