<style>
  #frame_pdf {
    width: 100%;
    min-height: 80vh;
  }
</style>
<div class="content-body">
  <div class="row">
    <div class="col-md-12">
      <iframe id="frame_pdf" src="<?= $url ?>" frameborder="0"></iframe>
      <a href="<?= $url ?>" target="_blank" class="float-right btn btn-xs btn-primary mt-1">Buka Di Tab Baru <i class="fas fa-search"></i></a>
      <button type="button" class="float-right btn btn-xs btn-secondary btn-cancel mt-1 mr-2" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
    </div>
  </div>
</div>