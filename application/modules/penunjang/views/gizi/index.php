<script type="text/javascript">
  $(document).ready(function() {
    <?php if (@$cookie['search']['tgl_order_from'] == '') : ?>
      $('.datepicker-from').val('');
    <?php endif; ?>
    <?php if (@$cookie['search']['tgl_order_to'] == '') : ?>
      $('.datepicker-to').val('');
    <?php endif; ?>
  })
</script>
<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline col-title">
        <div class="col-back">
          <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
        </div>
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          <?= $nav['nav_nm'] ?> > Pasien Datang Sendiri
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item active"><span>Index</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>
  <!-- flash message -->
  <div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
  <!-- /flash message -->
  <div class="row full-page mt-4 mb-n2">
    <div class="col-lg-12 grid-margin-xl-0 stretch-card">
      <div class="card border-none">
        <div class="card-body">
          <form id="form-search" action="<?= site_url() . '/app/search/' . $nav['nav_id'] ?>" method="post" autocomplete="off">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Tgl.Registrasi</label>
                  <div class="col-lg-8 col-md-8">
                    <div class="row">
                      <div class="col-md-5" style="padding-right: 5px; flex: 0 0 46.4%; max-width: 46.4%;">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datepicker datepicker-from text-center" name="tgl_order_from" id="tgl_order_from" value="<?= @$cookie['search']['tgl_order_from'] ?>">
                        </div>
                      </div>
                      <div class="col-md-2 text-center-group">s.d</div>
                      <div class="col-md-5" style="padding-left: 5px; flex: 0 0 46.4%; max-width: 46.4%;">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datepicker datepicker-to text-center" name="tgl_order_to" id="tgl_order_to" value="<?= @$cookie['search']['tgl_order_to'] ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">No.RM/Nama Pasien</label>
                  <div class="col-lg-8 col-md-8">
                    <input type="text" class="form-control" name="no_rm_nm" id="no_rm_nm" value="<?= @$cookie['search']['no_rm_nm'] ?>">
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Jenis Pasien</label>
                  <div class="col-lg-5 col-md-8">
                    <select class="form-control chosen-select" name="jenispasien_id" id="jenispasien_id" onchange="$('#form-search').submit()">
                      <option value="">- Semua -</option>
                      <?php foreach ($jenis_pasien as $r) : ?>
                        <option value="<?= $r['jenispasien_id'] ?>" <?= (@$cookie['search']['jenispasien_id'] == $r['jenispasien_id']) ? 'selected' : ''; ?>><?= $r['jenispasien_nm'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <!-- <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Status Periksa</label>
                  <div class="col-lg-5 col-md-8">
                    <select class="form-control chosen-select" name="periksa_st" id="periksa_st">
                      <option value="">- Semua -</option>
                      <option value="1" <?= (@$cookie['search']['periksa_st'] == '1') ? 'selected' : ''; ?>>Sudah</option>
                      <option value="0" <?= (@$cookie['search']['periksa_st'] == '0') ? 'selected' : ''; ?>>Belum</option>
                    </select>
                  </div>
                </div> -->
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Status Terkirim</label>
                  <div class="col-lg-5 col-md-8">
                    <select class="form-control chosen-select" name="status_pemberian" id="status_pemberian" onchange="$('#form-search').submit()">
                      <option value="">- Semua -</option>
                      <option value="1" <?= (@$cookie['search']['status_pemberian'] == '1') ? 'selected' : ''; ?>>Sudah</option>
                      <option value="0" <?= (@$cookie['search']['status_pemberian'] == '0') ? 'selected' : ''; ?>>Belum</option>
                      <option value="2" <?= (@$cookie['search']['status_pemberian'] == '-1') ? 'selected' : ''; ?>>Batal</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-4">

              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group row">
                  <div class="col-lg-4 col-md-4">
                    <!-- <a href="<?= site_url() . '/' . $nav['nav_url'] . '/form' ?>" modal-title="Tambah Data" modal-size="md" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Tambah Data"><i class="fas fa-plus-circle"></i> Tambah</a> -->
                  </div>
                  <div class="col-lg-5 col-md-8 ml-n3">
                    <button type="submit" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cari Data"><i class="fas fa-search"></i> Cari</button>
                    <a class="btn btn-xs btn-default" href="<?= site_url() . '/app/reset/' . $nav['nav_id'] ?>" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-default" title="" data-original-title="Reset"><i class="fas fa-sync-alt"></i> Reset</a>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <div class="row mt-4 pagination-info">
            <div class="col-md-6">
              <form id="form-paging" action="<?= site_url() . '/app/per_page/' . $nav['nav_url'] ?>" method="post">
                <label><i class="mdi mdi-bookmark"></i> Tampilkan</label>
                <select name="per_page" class="select-pagination" onchange="$('#form-paging').submit()">
                  <option value="10" <?php if ($cookie['per_page'] == 10) {
                                        echo 'selected';
                                      } ?>>10</option>
                  <option value="50" <?php if ($cookie['per_page'] == 50) {
                                        echo 'selected';
                                      } ?>>50</option>
                  <option value="100" <?php if ($cookie['per_page'] == 100) {
                                        echo 'selected';
                                      } ?>>100</option>
                </select>
                <label>data per halaman.</label>
              </form>
            </div>
            <div class="col-md-6 text-right">
              <div class="pt-1"><?= @$pagination_info ?></div>
            </div>
          </div><!-- /.row -->
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                <form id="form-multiple" action="<?= site_url() . '/' . $nav['nav_url'] . '/multiple/enable' ?>" method="post">
                  <table class="table table-hover table-striped table-bordered table-fixed">
                    <thead>
                      <tr>
                        <th class="text-center" width="36">No</th>
                        <th class="text-center" width="36">
                          <div class="form-check form-check-primary form-check-th">
                            <label class="form-check-label">
                              <input type="checkbox" class="form-check-input cb-all">
                            </label>
                          </div>
                        </th>
                        <th class="text-center" width="80">Aksi</th>
                        <th class="text-center" width="100">No. Reg.</th>
                        <th class="text-center" width="80">Tgl. Order</th>
                        <th class="text-center" width="90"><?= table_sort($nav['nav_url'], 'Nomor RM', 'pasien_id', $cookie['order']) ?></th>
                        <th class="text-center" width="200"><?= table_sort($nav['nav_url'], 'Nama Pasien / Umur', 'pasien_nm', $cookie['order']) ?></th>
                        <th class="text-center" width="30">JK</th>
                        <th class="text-center">Alamat</th>
                        <th class="text-center" width="80">Jenis Pasien</th>
                        <th class="text-center" width="200">Jenis Gizi</th>
                        <th class="text-center" width="200">Jam Pemberian</th>
                        <th class="text-center" width="100">Status Terkirim</th>
                      </tr>
                    </thead>
                    <?php if (@$main == null) : ?>
                      <tbody>
                        <tr>
                          <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                        </tr>
                      </tbody>
                    <?php else : ?>
                      <tbody>
                        <?php $i = 1;
                        foreach ($main as $row) : ?>
                          <tr>
                            <td class="text-center" width="36"><?= $cookie['cur_page'] + ($i++) ?></td>
                            <td class="text-center" width="36">
                              <div class="form-check form-check-primary form-check-td">
                                <label class="form-check-label">
                                  <input type="checkbox" class="form-check-input cb-item" name="checkitem[]" value="<?= $row['reg_id'] . '#' . $row['gizi_id'] ?>">
                                </label>
                              </div>
                            </td>
                            <td class="text-center" width="80">
                              <!-- <a href="<?= site_url() . '/' . $nav['nav_url'] . '/periksa/' . $row['reg_id'] . '/' . $row['gizi_id'] ?>#tindakan" modal-title="Periksa" modal-size="md" class="btn btn-primary btn-table" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Periksa"><i class="fas fa-stethoscope"></i> Periksa</a> -->
                              <div class="btn-group">
                                <button type="button" class="btn btn-primary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  Aksi
                                </button>
                                <div class="dropdown-menu">
                                  <a class="dropdown-item modal-href" href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/cetak_modal/cetak_label/' . $row['reg_id'] . '/' . $row['gizi_id'] ?>" modal-title="Cetak Label" modal-size="lg" modal-content-top="-75px" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cetak Label"><i class="fas fa-print"></i> Cetak Label</a>
                                  <a class="dropdown-item" href="<?= site_url() . '/' . $nav['nav_url'] . '/kirim/' . $row['reg_id'] . '/' . $row['gizi_id'] ?>"><i class="fas fa-upload"></i> Kirim</a>
                                  <a class="dropdown-item" href="<?= site_url() . '/' . $nav['nav_url'] . '/batal/' . $row['reg_id'] . '/' . $row['gizi_id'] ?>"><i class="fas fa-times-circle"></i> Batal</a>
                                </div>
                              </div>
                            </td>
                            <td class="text-center" width="100"><?= $row['reg_id'] ?></td>
                            <td class="text-center" width="80"><?= to_date($row['tgl_catat'], '', 'full_date') ?></td>
                            <td class="text-center" width="90"><?= $row['pasien_id'] ?></td>
                            <td class="text-left" width="200">
                              <b><?= $row['pasien_nm'] . ', ' . $row['sebutan_cd'] ?></b><br />
                              <?= $row['umur_thn'] ?> th
                              <?= $row['umur_bln'] ?> bl
                              <?= $row['umur_hr'] ?> hr
                            </td>
                            <td class="text-center" width="30"><?= $row['sex_cd'] ?></td>
                            <td class="text-left">
                              <?= ($row['alamat'] != '') ? $row['alamat'] . ', ' : '' ?>
                              <?= ($row['kelurahan'] != '') ? ucwords(strtolower($row['kelurahan'])) . ', ' : '' ?>
                              <?= ($row['kecamatan'] != '') ? ucwords(strtolower($row['kecamatan'])) . ', ' : '' ?>
                              <?= ($row['kabupaten'] != '') ? ucwords(strtolower($row['kabupaten'])) . ', ' : '' ?>
                              <?= ($row['provinsi'] != '') ? ucwords(strtolower($row['provinsi'])) : '' ?>
                            </td>
                            <td class="text-center" width="80"><?= $row['jenispasien_nm'] ?></td>
                            <td class="text-left" width="200"><?= $row['jenisgizi_id'] ?> - <?= $row['jenisgizi_nm'] ?></td>
                            <td class="text-left" width="200"><?= $row['jamgizi_id'] ?> - <?= $row['jamgizi_nm'] ?> - <?= $row['jam_pemberian'] ?></td>
                            <td class="text-center" width="100">
                              <?php if ($row['status_pemberian'] == 1) : ?>
                                <span class="text-success font-weight-bold">SUDAH</span>
                              <?php elseif ($row['status_pemberian'] == 0) : ?>
                                <span class="text-danger font-weight-bold blink">BELUM</span>
                              <?php elseif ($row['status_pemberian'] == -1) : ?>
                                <span class="font-weight-bold">BATAL</span>
                              <?php endif; ?>
                            </td>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                    <?php endif; ?>
                  </table>
                </form>
              </div>
            </div>
          </div><!-- /.row -->
          <div class="row mt-2">
            <div class="col-6 text-middle pt-1">
              <?php if ($nav['_update'] == 1 || $nav['_delete'] == 1) : ?>
                <div class="input-group-prepend">
                  <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    Aksi Multi
                  </button>
                  <div class="dropdown-menu">
                    <?php if ($nav['_update'] == 1) : ?>
                      <a class="dropdown-item" href="javascript:multipleGiziAction('cetak_label')"><i class="fas fa-print"></i> Cetak Label</a>
                      <a class="dropdown-item" href="javascript:multipleGiziAction('kirim')"><i class="fas fa-upload"></i> Kirim</a>
                      <a class="dropdown-item" href="javascript:multipleGiziAction('batal')"><i class="fas fa-times-circle"></i> Batal</a>
                    <?php endif; ?>
                  </div>
                </div>
              <?php endif; ?>
            </div>
            <div class="col-6 text-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>