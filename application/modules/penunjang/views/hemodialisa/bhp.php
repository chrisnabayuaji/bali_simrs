<?php $this->load->view('_js_bhp'); ?>
<div class="row">
  <div class="col-md-5">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-syringe"></i> Pemberian BHP</h4>
        <form id="bhp_form" action="" method="post" autocomplete="off">
          <input type="hidden" name="bhp_id" id="bhp_id">
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Tanggal Catat <span class="text-danger">*</span></label>
            <div class="col-lg-5 col-md-6">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                </div>
                <input type="text" class="form-control datetimepicker" name="tgl_catat" id="tgl_catat" required="" aria-invalid="false">
              </div>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">BHP <span class="text-danger">*</span></label>
            <div class="col-lg-8 col-md-3">
              <select class="form-control select2" name="barang_id" id="barang_id">
                <option value="">- Pilih -</option>
              </select>
            </div>
            <div class="col-lg-1 col-md-1">
              <a href="javascript:void(0)" data-href="<?=site_url().'/'.$nav['nav_url'].'/ajax_bhp/search_bhp'?>" modal-title="List Data BHP" modal-size="lg" class="btn btn-xs btn-default modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari BHP" data-original-title="Cari BHP"><i class="fas fa-search"></i></a>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Qty <span class="text-danger">*<span></label>
            <div class="col-lg-2 col-md-2">
              <input type="number" class="form-control" name="qty" id="qty" value="1" min="1">
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Keterangan</label>
            <div class="col-lg-9 col-md-3">
              <textarea class="form-control" name="keterangan_bhp" id="keterangan_bhp" value="" rows="5"></textarea>            
            </div>
          </div>
          <div class="row">
            <div class="col-lg-9 offset-lg-3 p-0">
              <button type="submit" id="bhp_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
              <button type="button" id="bhp_cancel" class="btn btn-xs btn-secondary"><i class="fas fa-times"></i> Batal</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-7">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-list"></i> List Data Pemberian BHP</h4>
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-sm">
            <thead>
              <tr>
                <th class="text-center" width="20">No</th>
                <th class="text-center" width="60">Aksi</th>
                <th class="text-center">Kode</th>
                <th class="text-center">Nama</th>
                <th class="text-center">Qty</th>
                <th class="text-center">Keterangan</th>
              </tr>
            </thead>
            <tbody id="bhp_data"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>