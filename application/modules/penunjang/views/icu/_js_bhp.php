<script type="text/javascript">
  var bhp_form;
  $(document).ready(function () {
    var bhp_form = $("#bhp_form").validate( {
      rules: {
        barang_id: { valueNotEquals: "" },
        qty: { valueNotEquals: "" },
        tipediagnosis_cd: { valueNotEquals: "" }
      },
      messages: {
        barang_id: { valueNotEquals: "Pilih salah satu!" },
        qty: { valueNotEquals: "Kosong!" },
        tipediagnosis_cd: { valueNotEquals: "Pilih salah satu!" }
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('select2')){
          error.insertAfter(element.next(".select2-container"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container"));
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $("#bhp_action").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#bhp_action").attr("disabled", "disabled");
        $("#bhp_cancel").attr("disabled", "disabled");
        var pasien_id = $("#pasien_id").val();
        var reg_id = $("#reg_id").val();
        var lokasi_id = $("#lokasi_id").val();
        var pemeriksaan_id = $("#pemeriksaan_id").val();

        $.ajax({
          type : 'post',
          url : '<?=site_url($nav['nav_url'])?>/ajax_bhp/save',
          data : $(form).serialize()+'&reg_id='+reg_id+'&lokasi_id='+lokasi_id+'&pemeriksaan_id='+pemeriksaan_id,
          success : function (data) {
            bhp_reset();
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil disimpan.',
              icon: 'success',
              position: 'top-right'
            })
            $("#bhp_action").html('<i class="fas fa-save"></i> Simpan');
            $("#bhp_action").attr("disabled", false);
            $("#bhp_cancel").attr("disabled", false);
            bhp_data(reg_id, lokasi_id, pemeriksaan_id);
          }
        })
        return false;
      }
    });

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    // autocomplete
    $('#barang_id').select2({
      minimumInputLength: 2,
      ajax: {
        url: "<?=site_url($nav['nav_url'])?>/ajax_bhp/autocomplete",
        dataType: "json",
        
        data: function(params) {
          return{
            obat_nm : params.term,
            map_lokasi_depo : $("#map_lokasi_depo").val()
          };
        },
        processResults: function(data){
          return{
            results: data
          }
        }
      }
    });
    $('.select2-container').css('width', '100%');

    var reg_id = $("#reg_id").val();
    var lokasi_id = $("#lokasi_id").val();
    var pemeriksaan_id = $("#pemeriksaan_id").val();
    bhp_data(reg_id, lokasi_id, pemeriksaan_id);

    $("#bhp_cancel").on('click', function () {
      bhp_reset();
    });

    $('.modal-href').click(function (e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");
      
      $("#modal-title").html(modal_title);
      $("#modal-size").addClass('modal-' + modal_size);
      if(modal_custom_size){
        $("#modal-size").attr('style', 'max-width: '+modal_custom_size+'px !important');
      }
      $("#myModal").modal('show');
      $.post($(this).data('href'), function (data) {
        $("#modal-body").html(data.html);
      }, 'json');
    });

    $('.datetimepicker').daterangepicker({
      startDate: moment("<?=date('Y-m-d H:i:s')?>"),
      endDate: moment(),
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })

  })

  function bhp_data(reg_id='', lokasi_id='', pemeriksaan_id = '') {
    $('#bhp_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_bhp/data'?>', {reg_id: reg_id, lokasi_id: lokasi_id, pemeriksaan_id: pemeriksaan_id}, function (data) {
      $('#bhp_data').html(data.html);
    }, 'json');
  }

  function bhp_reset() {
    $("#bhp_id").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#barang_id").val('').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#qty").val('1').removeClass("is-valid").removeClass("is-invalid");
    $("#keterangan_bhp").val('').removeClass("is-valid").removeClass("is-invalid");
    $('#bhp_action').html('<i class="fas fa-save"></i> Simpan');

    $('.datetimepicker').daterangepicker({
      startDate: moment("<?=date('Y-m-d H:i:s')?>"),
      endDate: moment(),
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })
  }

  function bhp_fill(id) {
    $.ajax({
      type : 'post',
      url : '<?=site_url($nav['nav_url'].'/ajax_bhp/bhp_fill')?>',
      dataType : 'json',
      data : 'barang_id='+id,
      success : function (data) {
        // autocomplete
        var data2 = {
          id: data.obat_id+'#'+data.obat_nm,
          text: data.obat_id+' - '+data.obat_nm
        };
        var newOption = new Option(data2.text, data2.id, false, false);
        $('#barang_id').append(newOption).trigger('change');
        $('#barang_id').val(data.obat_id+'#'+data.obat_nm);
        $('#barang_id').trigger('change');
        $('.select2-container').css('width', '100%');
        $("#myModal").modal('hide');
      }
    })
  }
</script>