<script type="text/javascript">
  $(document).ready(function() {
    var form = $("#form-data").validate({
      rules: {
        petugas_id: {
          valueNotEquals: ""
        },
        pemeriksa_id: {
          valueNotEquals: ""
        },
      },
      messages: {
        petugas_id: "Pilih salah satu Dokter Penanggung Jawab Laborat!",
        pemeriksa_id: "Pilih salah satu Pemeriksa!",
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('autocomplete')) {
          error.insertAfter(element.next(".select2-container"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-n2 mb-2');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    var pasien_id = $("#pasien_id").val();
    var reg_id = $("#reg_id").val();
    var lokasi_id = $("#lokasi_id").val();
    var pemeriksaan_id = $("#pemeriksaan_id").val();
    hasil_pemeriksaan_data(pemeriksaan_id);

    // pemeriksaan_fisik_data();

    $(".pemeriksaan_fisik_field").on('change', function() {
      var pasien_id = $("#pasien_id").val();
      var reg_id = $("#reg_id").val();
      var lokasi_id = $("#lokasi_id").val();
      $.ajax({
        type: 'post',
        url: '<?= site_url($nav['nav_url']) ?>/ajax_pemeriksaan_fisik/save',
        data: $("#pemeriksaan_fisik_form").serialize() + '&pasien_id=' + pasien_id + '&reg_id=' + reg_id + '&lokasi_id=' + lokasi_id,
        success: function(data) {
          pemeriksaan_fisik_data();
        }
      })
    })

    $('.datetimepicker').daterangepicker({
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    $('.modal-href').click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");
      var modal_header = $(this).attr("modal-header");
      var modal_content_top = $(this).attr("modal-content-top");

      $("#modal-size").removeClass('modal-lg').removeClass('modal-md').removeClass('modal-sm');

      $("#modal-title").html(modal_title);
      $("#modal-size").addClass('modal-' + modal_size);
      if (modal_custom_size) {
        $("#modal-size").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
      }
      if (modal_content_top) {
        $(".modal-content-top").attr('style', 'margin-top: ' + modal_content_top + ' !important');
      }
      if (modal_header == 'hidden') {
        $("#modal-header").addClass('d-none');
      } else {
        $("#modal-header").removeClass('d-none');
      }
      $("#myModal").modal('show');
      $("#modal-body").html('<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i><br>Loading</div>');
      $.post($(this).data('href'), function(data) {
        $("#modal-body").html(data.html);
      }, 'json');
    });

    $('.belum-bayar').click(function(e) {
      e.preventDefault();
      Swal.fire({
        title: "Tidak bisa Cetak Hasil Pemeriksaan",
        text: "Saat ini Anda tidak bisa melakukan Cetak Hasil Pemeriksaan Laboratorium dikarenakan pasien ini belum membayar tagihan.",
        type: "error",
        customClass: "swal-wide",
      })
    });
  })

  function hasil_pemeriksaan_data(pemeriksaan_id = '') {
    $('#hasil_pemeriksaan_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_hasil_pemeriksaan/data' ?>', {
      pemeriksaan_id: pemeriksaan_id
    }, function(data) {
      $('#hasil_pemeriksaan_data').html(data.html);
    }, 'json');
  }

  function pemeriksaan_fisik_data() {
    var pasien_id = $("#pasien_id").val();
    var reg_id = $("#reg_id").val();
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_pemeriksaan_fisik/data' ?>', {
      reg_id: reg_id,
      pasien_id: pasien_id
    }, function(data) {
      if (data != null) {
        $("#keluhan_utama").val(data.keluhan_utama);
        $("#anamnesis_id").val(data.anamnesis_id);
        $("#systole").val(data.systole);
        $("#diastole").val(data.diastole);
        $("#tinggi").val(data.tinggi);
        $("#berat").val(data.berat);
        $("#suhu").val(data.suhu);
        $("#nadi").val(data.nadi);
        $("#respiration_rate").val(data.respiration_rate);
        $("#sao2").val(data.sao2);
      }
    }, 'json');
  }

  function petugas_fill(id) {
    $.ajax({
      type: 'post',
      url: '<?= site_url($nav['nav_url'] . '/ajax_hasil_pemeriksaan/petugas_fill') ?>',
      dataType: 'json',
      data: 'pegawai_id=' + id,
      success: function(data) {
        // autocomplete
        var data2 = {
          id: data.pegawai_id,
          text: data.pegawai_nm
        };
        var newOption = new Option(data2.text, data2.id, false, false);
        $('#petugas_id').append(newOption).trigger('change');
        $('#petugas_id').val(data.pegawai_id);
        $('#petugas_id').trigger('change');
        $('.select2-container').css('width', '100%');
        $("#myModal").modal('hide');
      }
    })
  }
</script>