<script type="text/javascript">
  var onlineTable;
  $(document).ready(function () {
    //datatables
    onlineTable = $('#online_table').DataTable({
      "autoWidth" : false,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
      "ajax": {
          "url": "<?php echo site_url().'/'.'penunjang/laboratorium/datang'.'/get_data_online'?>",
          "type": "POST"
      },      
      "columnDefs": [
        {"targets": 0, "className": 'text-center', "orderable" : false},
        {"targets": 1, "className": 'text-center', "orderable" : false},
        {"targets": 2, "className": 'text-center', "orderable" : false},
        {"targets": 3, "className": 'text-left', "orderable" : false},
        {"targets": 4, "className": 'text-left', "orderable" : false},
        {"targets": 5, "className": 'text-center', "orderable" : false},
        {"targets": 6, "className": 'text-left', "orderable" : false},
        {"targets": 7, "className": 'text-center', "orderable" : false},
      ],
    });
    onlineTable.columns.adjust().draw();
  })
</script>