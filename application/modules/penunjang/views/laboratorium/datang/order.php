<?php $this->load->view('penunjang/laboratorium/datang/_js'); ?>
<div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
<form id="laboratorium_form" action="<?= site_url('penunjang/laboratorium/datang/order_save/' . $reg_id . '/' . $pemeriksaan_id) ?>" method="post" autocomplete="off">
  <div class="content-wrapper mw-100">
    <div class="row mt-n4 mb-n3">
      <div class="col-lg-4 col-md-12">
        <div class="d-lg-flex align-items-baseline">
          <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
            <?= $nav['nav_nm'] ?> > Pasien Datang Sendiri
            <span class="line-title"></span>
          </div>
        </div>
      </div>
      <div class="col-lg-8 col-md-12">
        <!-- Breadcrumb -->
        <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
          <ol class="breadcrumb breadcrumb-custom">
            <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-flask"></i> Laboratorium</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
            <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> Pasien Datang Sendiri</a></li>
            <li class="breadcrumb-item active"><span>Order</span></li>
          </ol>
        </nav>
        <!-- End Breadcrumb -->
      </div>
    </div>

    <div class="row full-page mt-4">
      <div class="col-lg-12 grid-margin stretch-card">
        <div class="card border-none">
          <div class="card-body card-shadow">
            <h4 class="card-title border-bottom border-2 pb-2 mb-3">Order Laboratorium</h4>
            <div class="row">
              <input type="hidden" name="pemeriksaan_id" id="pemeriksaan_id" value="<?= @$mainlab['pemeriksaan_id'] ?>">
              <input type="hidden" name="reg_id" id="reg_id" value="<?= @$reg['reg_id'] ?>">
              <input type="hidden" name="pasien_id" id="pasien_id" value="<?= @$reg['pasien_id'] ?>">
              <input type="hidden" name="kelas_id" id="kelas_id" value="<?= @$reg['kelas_id'] ?>">
              <div class="col-5">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-3 col-form-label">Dokter Pengirim <span class="text-danger">*</span></label>
                  <div class="col-lg-8 col-md-3">
                    <input type="text" class="form-control" id="dokterpengirim_nm" value="<?= $reg['pegawai_nm'] ?>" required="" readonly>
                    <input type="hidden" name="dokterpengirim_id" value="<?= $reg['dokter_id'] ?>">
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-3 col-form-label">Tanggal Order<span class="text-danger">*<span></label>
                  <div class="col-lg-5 col-md-9">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                      </div>
                      <input type="text" class="form-control datetimepicker" name="tgl_order" id="tgl_order" value="<?php if (@$mainlab) {
                                                                                                                      echo to_date(@$mainlab['tgl_order'], '-', 'full_date');
                                                                                                                    } else {
                                                                                                                      echo date('d-m-Y H:i:s');
                                                                                                                    } ?>" required aria-invalid="false">
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-3 col-form-label">Lokasi Asal <span class="text-danger">*</span></label>
                  <div class="col-lg-8 col-md-3">
                    <input type="text" class="form-control" id="lokasi_nm" value="<?= @$reg['lokasi_nm'] ?>" required="" readonly>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-3 col-form-label">Keterangan</label>
                  <div class="col-lg-8 col-md-3">
                    <textarea class="form-control" name="keterangan_order" id="keterangan_order" rows="6"><?= @$mainlab['keterangan_order'] ?></textarea>
                  </div>
                </div>
                <hr>
                <table class="table table-bordered table-striped table-sm mt-3">
                  <thead>
                    <tr>
                      <th class="text-center" colspan="99">Paket Pemeriksaan</th>
                    </tr>
                    <tr>
                      <th class="text-center" width="50">#</th>
                      <th class="text-center">Nama Paket</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>
                        <div class="d-flex justify-content-center">
                          <div class="d-flex justify-content-center">
                            <div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
                              <label class="form-check-label">
                                <input type="checkbox" class="form-check-input paket" data-group="01.01;01.05;01.02;01.04;01.06;01.09;01.10;01.11;01.12;01.08">
                                <i class="input-helper"></i></label>
                            </div>
                          </div>
                        </div>
                      </td>
                      <td>Darah Lengkap</td>
                    </tr>
                    <tr>
                      <td>
                        <div class="d-flex justify-content-center">
                          <div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
                            <label class="form-check-label">
                              <input type="checkbox" class="form-check-input paket" data-group="01.01;01.05;01.02;01.04;01.06;01.09;01.10;01.11;01.12;01.08;01.03">
                              <i class="input-helper"></i></label>
                          </div>
                        </div>
                      </td>
                      <td>Darah Lengkap + LED</td>
                    </tr>
                    <tr>
                      <td>
                        <div class="d-flex justify-content-center">
                          <div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
                            <label class="form-check-label">
                              <input type="checkbox" class="form-check-input paket" data-group="01.01;01.05;01.02;01.04;01.06;01.08">
                              <i class="input-helper"></i></label>
                          </div>
                        </div>
                      </td>
                      <td>Darah Rutin</td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col">
                <div class="table-responsive">
                  <table id="laboratorium_table" class="table table-hover table-bordered table-striped table-sm w-100">
                    <thead>
                      <tr>
                        <th class="text-center" width="20">No.</th>
                        <th class="text-center" width="50">Kode</th>
                        <th class="text-center">Nama </th>
                        <th class="text-center" width="20">Aksi</th>
                      </tr>
                    </thead>
                  </table>
                </div>
                <button id="laboratorium_reset" type="button" class="btn btn-xs btn-danger"><i class="fas fa-times"></i> Hapus Semua Pilihan</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="btn-form">
    <div class="btn-form-action">
      <div class="btn-form-action-bottom w-100 small-text clearfix">
        <div class="col-lg-10 col-md-8 col-4">

        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-clear">
          <button type="reset" onClick="window.location.reload();" class="btn btn-xs btn-secondary" id="laboratorium_cancel"><i class="fas fa-sync-alt"></i> Reset</button>
        </div>
        <div class="col-lg-1 col-md-2 col-4 btn-form-save">
          <button type="submit" class="btn btn-xs btn-primary" id="laboratorium_action"><i class="fas fa-save"></i> Simpan</button>
        </div>
      </div>
    </div>
  </div>
</form>
<script>
  var laboratoriumTable;
  $(document).ready(function() {
    //datatables
    laboratoriumTable = $('#laboratorium_table').DataTable({
      'retrieve': true,
      "autoWidth": false,
      "processing": true,
      "serverSide": false,
      "order": [],
      "ajax": {
        "url": "<?php echo site_url() . '/' . 'penunjang/laboratorium/datang' . '/ajax_penunjang_laboratorium/search_data/' . @$reg['reg_id'] . '/' . @$mainlab['pemeriksaan_id'] ?>",
        "type": "POST"
      },
      "columnDefs": [{
          "targets": 0,
          "className": 'text-center',
          "orderable": false
        },
        {
          "targets": 1,
          "className": 'text-left',
          "orderable": false
        },
        {
          "targets": 2,
          "className": 'text-left',
          "orderable": false
        },
        {
          "targets": 3,
          "className": 'text-center',
          "orderable": false
        }
      ],
    });
    laboratoriumTable.columns.adjust().draw();


    //FORM
    var laboratorium_form = $("#laboratorium_form").validate({
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $("#laboratorium_action").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#laboratorium_action").attr("disabled", "disabled");
        $("#laboratorium_cancel").attr("disabled", "disabled");

        laboratoriumTable.$('input[type="checkbox"]').each(function() {
          // If checkbox doesn't exist in DOM
          if (!$.contains(document, this)) {
            // If checkbox is checked
            if (this.checked) {
              // Create a hidden element
              $(form).append(
                $('<input>')
                .attr('type', 'hidden')
                .attr('name', this.name)
                .val(this.value)
              );
            }
          }
        });

        // $.ajax({
        //   type: 'post',
        //   url: '<?= site_url('penunjang/laboratorium/datang/order_save/' . $reg_id . '/' . $pemeriksaan_id) ?>',
        //   data: $(form).serialize(),
        //   dataType: 'json',
        //   async: false,
        //   success: function(data) {
        //     window.location.replace("<?= site_url('penunjang/laboratorium/datang') ?>");
        //   }
        // })
        form.submit();

      }
    });

    $('.paket').click(function() {
      var group = $(this).data('group');
      var lab_arr = group.split(";");
      if (this.checked) {
        laboratoriumTable.$('input[type="checkbox"]').each(function() {
          if (lab_arr.includes(this.value)) {
            $(this).prop('checked', true);
          }
        });
      } else {
        laboratoriumTable.$('input[type="checkbox"]').each(function() {
          if (lab_arr.includes(this.value)) {
            $(this).prop('checked', false);
          }
        });
      }
    });

    $('#laboratorium_reset').click(function() {
      $(".paket").prop('checked', false);
      laboratoriumTable.$('input[type="checkbox"]').each(function() {
        $(this).prop('checked', false);
      });
    })
  })
</script>