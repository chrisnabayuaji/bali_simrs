<script type="text/javascript">
  $(document).ready(function () {
    var form = $("#form-data").validate( {
      rules: {
        nik : {
          'number' : true
        }
      },
      messages: {
        
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container"));
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    $('.chosen-select').on('select2:select', function (e) {
      form.element(this);
    });

    $('#wilayah_st').bind('change',function(e) {
      e.preventDefault();
      var i = $(this).val();
      _get_provinsi(i);
    })

    <?php if(@$id != ''):?>
    _get_wilayah('<?=get_wilayah_id(@$main["wilayah_id"], 'provinsi')?>', '<?=get_wilayah_id(@$main["wilayah_id"], 'provinsi')?>', '<?=get_wilayah_id(@$main["wilayah_id"], 'kabupaten')?>', '<?=get_wilayah_id(@$main["wilayah_id"], 'kecamatan')?>', '<?=get_wilayah_id(@$main["wilayah_id"], 'kelurahan')?>');
    <?php endif;?>
    //
    $('#wilayah_prop').bind('change',function(e) {
      e.preventDefault();
      var i = $(this).val();
      _get_wilayah(i);
      $(this).on('select2:select', function (e) {
        form.element(this);
      });
    })
    //
    
    jenispasien();
    
    hitung_umur();

    var typing_timer;
    $('#pasien_id').on('keyup', function () {
      clearTimeout(typing_timer);
      typing_timer = setTimeout(typing_done, 700);
    });

    $('#pasien_id').on('keydown', function () {
      clearTimeout(typing_timer);
    });

    function typing_done() {
      var id = $('#pasien_id').val();
      no_rm_fill(id);
    }

    function no_rm_fill(id) {
      $("#pasien_id_loading").removeClass('d-none');
      $.ajax({
        type : 'post',
        url : '<?=site_url($nav['nav_url'].'/ajax/no_rm_fill')?>',
        dataType : 'json',
        data : 'pasien_id='+id,
        success : function (data) {
          if (data.pasien_id !='') {
            $("#statuspasien_cd").val('L').trigger('change');
          }else{
            $("#statuspasien_cd").val('B').trigger('change');
          }
          $("#pasien_nm").val(data.pasien_nm);
          $("#nik").val(data.nik);
          $("#nama_kk").val(data.nama_kk);
          $("#no_kk").val(data.no_kk);
          $("#kode_pos").val(data.kode_pos);
          $("#alamat").val(data.alamat);
          $("#wilayah_st").val(data.wilayah_st);
          _get_provinsi(data.wilayah_st);
          if (data.wilayah_id !='') {
            var wil = data.wilayah_id.split(".");
          }
          setTimeout(function(){ 
            if (data.wilayah_id !='') {
              $("#wilayah_prop").val(wil[0]+"#"+data.provinsi).trigger('change');
            }else{
              $("#wilayah_prop").val('').trigger('change');
            }
          }, 200);
          setTimeout(function(){ 
            if (data.wilayah_id !='') {
              _get_wilayah(wil[0], wil[0], wil[0]+'.'+wil[1], wil[0]+'.'+wil[1]+'.'+wil[2], data.wilayah_id);
            }else{
              _empty_kabupaten();
              _empty_kecamatan();
              _empty_kelurahan();
            }
          }, 400);
          $("#tmp_lahir").val(data.tmp_lahir);
          $("#tgl_lahir").val(to_date(data.tgl_lahir));
          $("#sex_cd").val(data.sex_cd).trigger('change');
          $("#goldarah_cd").val(data.goldarah_cd).trigger('change');
          $("#pendidikan_cd").val(data.pendidikan_cd).trigger('change');
          $("#pekerjaan_cd").val(data.pekerjaan_cd).trigger('change');
          $("#agama_cd").val(data.agama_cd).trigger('change');
          $("#no_telp").val(data.telp);
          $("#jenispasien_id").val(data.jenispasien_id).trigger('change');
          $("#no_kartu").val(data.no_kartu);
          hitung_umur();
          $("#pasien_id_loading").addClass('d-none');
        }
      })
    }

  })

  function _empty_kabupaten() {
    $.get('<?=site_url('master/wilayah/ajax/empty_kabupaten')?>',null,function(data) {
        $('#box_wilayah_kab').html(data.html);
    },'json');
  }

  function _empty_kecamatan() {
    $.get('<?=site_url('master/wilayah/ajax/empty_kecamatan')?>',null,function(data) {
        $('#box_wilayah_kec').html(data.html);
    },'json');
  }

  function _empty_kelurahan() {
    $.get('<?=site_url('master/wilayah/ajax/empty_kelurahan')?>',null,function(data) {
        $('#box_wilayah_kel').html(data.html);
    },'json');
  }

  function _get_provinsi(i) {
    $.get('<?=site_url('master/wilayah/ajax/get_provinsi_by_st')?>?wilayah_st='+i,null,function(data) {
        $('#box_wilayah_prop').html(data.html);
    },'json');
  }

  function _get_wilayah(i, prop='', kab='', kec='', kel='') {
    var ext_var = 'wilayah_prop='+prop;
        ext_var+= '&wilayah_kab='+kab;
        ext_var+= '&wilayah_kec='+kec;
        ext_var+= '&wilayah_kel='+kel;
    $.get('<?=site_url('master/wilayah/ajax/get_wilayah_id_name')?>?wilayah_parent='+i+'&'+ext_var,null,function(data) {
        $('#box_wilayah_kab').html(data.html);
    },'json');
  }

  function jenispasien() {
    var val = $("#jenispasien_id").val();
    if (val != '01') {
      $('#label-no-kartu').removeClass('d-none');
      $('#col-no-kartu').removeClass('d-none');
    }else{
      $('#label-no-kartu').addClass('d-none');
      $('#col-no-kartu').addClass('d-none');
    }
  }

  function hitung_umur() {
    var tanggal = to_date($("#tgl_lahir").val());
    var umur = get_age(tanggal);
    $("#umur_thn").val(umur.years);
    $("#umur_bln").val(umur.months);
    $("#umur_hr").val(umur.days);
  }

  function online_fill(id) {
    $.ajax({
      type : 'post',
      url : '<?=site_url($nav['nav_url'].'/ajax/online_fill')?>',
      dataType : 'json',
      data : 'regonline_id='+id,
      success : function (data) {
        if (data.pasien_id !='') {
          $("#statuspasien_cd").val('L').trigger('change');
        }else{
          $("#statuspasien_cd").val('B').trigger('change');
        }
        $("#regonline_id").val(data.regonline_id);
        $("#pasien_id").val(data.pasien_id);
        $("#pasien_nm").val(data.pasien_nm);
        $("#alamat").val(data.alamat);
        $("#wilayah_st").val(data.wilayah_st);
        _get_provinsi(data.wilayah_st);
        var wil = data.wilayah_id.split(".");
        setTimeout(function(){ 
          $("#wilayah_prop").val(wil[0]+"#"+data.provinsi).trigger('change');
        }, 200);
        setTimeout(function(){ 
          _get_wilayah(wil[0], wil[0], wil[0]+'.'+wil[1], wil[0]+'.'+wil[1]+'.'+wil[2], data.wilayah_id);
        }, 400);
        $("#tmp_lahir").val(data.tmp_lahir);
        $("#tgl_lahir").val(to_date(data.tgl_lahir));
        $("#sex_cd").val(data.sex_cd).trigger('change');
        $("#no_telp").val(data.no_telp);
        $("#jenispasien_id").val(data.jenispasien_id).trigger('change');
        $("#no_kartu").val(data.no_kartu);
        hitung_umur();
        $("#myModal").modal('hide');
      }
    })
  }

  function pasien_fill(id) {
    $.ajax({
      type : 'post',
      url : '<?=site_url($nav['nav_url'].'/ajax/pasien_fill')?>',
      dataType : 'json',
      data : 'pasien_id='+id,
      success : function (data) {
        if (data.pasien_id !='') {
          $("#statuspasien_cd").val('L').trigger('change');
        }else{
          $("#statuspasien_cd").val('B').trigger('change');
        }
        $("#pasien_id").val(data.pasien_id);
        $("#pasien_nm").val(data.pasien_nm);
        $("#nik").val(data.nik);
        $("#nama_kk").val(data.nama_kk);
        $("#no_kk").val(data.no_kk);
        $("#kode_pos").val(data.kode_pos);
        $("#alamat").val(data.alamat);
        $("#wilayah_st").val(data.wilayah_st);
        _get_provinsi(data.wilayah_st);
        var wil = data.wilayah_id.split(".");
        setTimeout(function(){ 
          $("#wilayah_prop").val(wil[0]+"#"+data.provinsi).trigger('change');
        }, 200);
        setTimeout(function(){ 
          _get_wilayah(wil[0], wil[0], wil[0]+'.'+wil[1], wil[0]+'.'+wil[1]+'.'+wil[2], data.wilayah_id);
        }, 400);
        $("#tmp_lahir").val(data.tmp_lahir);
        $("#tgl_lahir").val(to_date(data.tgl_lahir));
        $("#sex_cd").val(data.sex_cd).trigger('change');
        $("#goldarah_cd").val(data.goldarah_cd).trigger('change');
        $("#pendidikan_cd").val(data.pendidikan_cd).trigger('change');
        $("#pekerjaan_cd").val(data.pekerjaan_cd).trigger('change');
        $("#agama_cd").val(data.agama_cd).trigger('change');
        $("#no_telp").val(data.telp);
        $("#jenispasien_id").val(data.jenispasien_id).trigger('change');
        $("#no_kartu").val(data.no_kartu);
        hitung_umur();
        $("#myModal").modal('hide');
      }
    })
  }

  function zero_fill(str, max) {
    str = str.toString();
    return str.length < max ? zero_fill("0" + str, max) : str;
  }

  function ucfirst(str) {
    str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
        return letter.toUpperCase();
    });
    return str;
  }

  function cari_nik() {
    $("#nik_loading").removeClass('d-none');
    $("#btn-search-nik").html('<i class="fas fa-spin fa-spinner"></i> Proses');
    $("#btn-search-nik").attr("disabled", "disabled");
    var nik = $("#nik").val();
    if (!Number.isInteger(parseInt(nik))) {
      alert('NIK harus berupa angka!');
    }else{
      $.ajax({
        type: "POST",
        url: "<?=site_url($nav['nav_url'])?>/get_nik/"+nik,
        dataType: "json",
        success: function (data) {
          if (data.STATUS == 0) {
            $.toast({
              heading: 'Error',
              text: 'Data tidak ditemukan!',
              icon: 'error',
              position: 'top-right'
            })
          }else if(data.STATUS == -1){
            $.toast({
              heading: 'Error',
              text: 'Terjadi Kesalahan Server!',
              icon: 'error',
              position: 'top-right'
            })
          }else if(data.STATUS == 1){
            $("#pasien_nm").val(data.NAMA_LGKP);
            if (data.JENIS_KLMIN === 'LAKI-LAKI') {
              $("#sex_cd").val('L').trigger('change');
            }else{
              $("#sex_cd").val('P').trigger('change');
            }
            $("#tmp_lahir").val(data.TMPT_LHR);
            var tgl = data.TGL_LHR.split(".");
            $("#tgl_lahir").val(tgl[0]+'-'+tgl[1]+'-'+tgl[2]);
            hitung_umur();
            var alamat = data.ALAMAT+' RT. '+data.NO_RT+' RW. ';
            if (data.NO_RW == null) {
              alamat += '- ';
            }else{
              alamat += data.NO_RW+' ';
            };
            if (data.DUSUN !='-' && data.DUSUN !=null) {
              alamat += 'Dsn. '+data.DUSUN;  
            }
            $("#alamat").val(alamat);
            $("#no_kk").val(data.NO_KK);
            $("#kode_pos").val(data.KODE_POS);
            var prop = zero_fill(data.NO_PROP,2);
            var kab = prop+'.'+zero_fill(data.NO_KAB,2);
            var kec = kab+'.'+zero_fill(data.NO_KEC,2);
            var kel = kec+'.'+zero_fill(data.NO_KEL,4);
            var wilayah_id = zero_fill(data.NO_PROP,2)+'.'+zero_fill(data.NO_KAB,2)+'.'+zero_fill(data.NO_KEC,2)+'.'+zero_fill(data.NO_KEL,4);
            if (prop != '33') {
              var wilayah_st = 'L';
            }else{
              var wilayah_st = 'D';
            }
            $("#wilayah_st").val(wilayah_st).trigger('change');
            _get_provinsi(wilayah_st);
            if (wilayah_id !='') {
              var wil = wilayah_id.split(".");
            }
            setTimeout(function(){ 
              if (wilayah_id !='') {
                $("#wilayah_prop").val(wil[0]+"#"+data.PROP_NAME).trigger('change');
              }else{
                $("#wilayah_prop").val('').trigger('change');
              }
            }, 150);
            setTimeout(function(){ 
              if (wilayah_id !='') {
                _get_wilayah(wil[0], wil[0], wil[0]+'.'+wil[1], wil[0]+'.'+wil[1]+'.'+wil[2], wilayah_id);
              }else{
                _empty_kabupaten();
                _empty_kecamatan();
                _empty_kelurahan();
              }
            }, 360);

            if (data.GOL_DARAH == 'TIDAK TAHU') {
              $("#goldarah_cd").val('').trigger('change');
            }else{
              let goldarah_cd = $('#goldarah_cd').find("option:contains('"+data.GOL_DARAH+"')").val()
              $('#goldarah_cd').val(goldarah_cd).trigger('change.select2');
            }

            let agama_cd = $('#agama_cd').find("option:contains('"+ucfirst(data.AGAMA)+"')").val()
            $('#agama_cd').val(agama_cd).trigger('change.select2');
            
          }
          $("#nik_loading").addClass('d-none');
          $("#btn-search-nik").html('<i class="fas fa-search"></i> Cari NIK');
          $("#btn-search-nik").removeAttr("disabled");
        },
        error: function (xhr, ajaxOptions, thrownError) {
          alert(xhr.status);
          alert(thrownError);
        }
      });
    }
  }

  function str_left(str,pad) {
    var pad = pad.toString() + str.toString();
    var ans = pad.substr(0, pad.length - str.length) + str;
    return ans;
  }
</script>