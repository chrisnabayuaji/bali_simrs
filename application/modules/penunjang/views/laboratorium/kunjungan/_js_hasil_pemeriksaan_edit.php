<script type="text/javascript">
  var hasil_pemeriksaan_edit;
  $(document).ready(function () {
    var hasil_pemeriksaan_edit = $("#hasil_pemeriksaan_edit").validate( {
      rules: {
        
      },
      messages: {
        
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('select2')){
          error.insertAfter(element.next(".select2-container"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container"));
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $("#edit_action").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#edit_action").attr("disabled", "disabled");
        $("#edit_cancel").attr("disabled", "disabled");
        var pemeriksaan_id = $("#pemeriksaan_id").val();
        $.ajax({
          type : 'post',
          url : '<?=site_url($nav['nav_url'])?>/ajax_hasil_pemeriksaan/save_edit',
          data : $(form).serialize(),
          success : function (data) {
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil disimpan.',
              icon: 'success',
              position: 'top-right'
            })
            $("#edit_action").html('<i class="fas fa-save"></i> Simpan');
            $("#edit_action").attr("disabled", false);
            $("#edit_cancel").attr("disabled", false);
            $("#myModal").modal('hide');
            hasil_pemeriksaan_data(pemeriksaan_id);
          }
        })
        return false;
      }
    });

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    // autocomplete
    var data2 = {
      id: '<?=@$main['itemlab_id']?>',
      text: '<?=@$main['itemlab_nm']?>'
    };
    var newOption = new Option(data2.text, data2.id, false, false);
    $('#itemlab_id').append(newOption).trigger('change');
    $('#itemlab_id').val('<?=@$main['itemlab_id']?>');
    $('#itemlab_id').trigger('change');
    $('.select2-container').css('width', '100%');

    // autocomplete
    $('#itemlab_id').select2({
      minimumInputLength: 2,
      ajax: {
        url: "<?=site_url($nav['nav_url'])?>/ajax_hasil_pemeriksaan/autocomplete",
        dataType: "json",
        
        data: function(params) {
          return{
            itemlab_nm : params.term
          };
        },
        processResults: function(data){
          return{
            results: data
          }
        }
      }
    });
    $('.select2-container').css('width', '100%');

  })

  function hasil_pemeriksaan_data(pemeriksaan_id='') {
    $('#hasil_pemeriksaan_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_hasil_pemeriksaan/data'?>', {pemeriksaan_id: pemeriksaan_id}, function (data) {
      $('#hasil_pemeriksaan_data').html(data.html);
    }, 'json');
  }
</script>