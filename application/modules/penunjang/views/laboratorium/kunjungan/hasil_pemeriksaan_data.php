<?php if ($main == null) : ?>
  <tr>
    <td class="text-center" colspan="99">Tidak ada data.</td>
  </tr>
<?php else : ?>
  <?php $no = 1;
  foreach ($main as $row) : ?>
    <?php if ($row['parent_id'] == '') : ?>
      <tr>
        <td colspan="99"><b><?= @$row['itemlab_nm'] ?></b></td>
      </tr>
    <?php else : ?>
      <tr>
        <td class="text-center"><?= $no++ ?></td>
        <td class="text-left"><?= $row['itemlab_id'] ?></td>
        <td class="text-right"><?= ($row['jml_tagihan'] == 0) ? '' : num_id($row['jml_tagihan']) ?></td>
        <td class="text-left pl-<?= count(explode('.', $row['itemlab_id'])) ?>" colspan="4">|- <?= $row['itemlab_nm'] ?></td>
        <td class="text-center">
          <?php if ($row['is_tagihan'] == 1) : ?>
            <a href="javascript:void(0)" class="btn btn-danger btn-table btn-delete" data-pemeriksaanrinc-id="<?= $row['pemeriksaanrinc_id'] ?>" data-pemeriksaan-id="<?= $row['pemeriksaan_id'] ?>" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
          <?php endif; ?>
        </td>
      </tr>
    <?php endif; ?>
    <?php if (count($row['rinc']) > 0) : ?>
      <?php foreach ($row['rinc'] as $row2) : ?>
        <tr>
          <td class="text-center"><?= $no++ ?></td>
          <td class="text-left"><?= $row2['itemlab_id'] ?></td>
          <td class="text-right"><?= ($row2['jml_tagihan'] == 0) ? '' : num_id($row2['jml_tagihan']) ?></td>
          <td class="text-left pl-<?= count(explode('.', $row2['itemlab_id'])) ?>">|- <?= $row2['itemlab_nm'] ?></td>
          <td class="text-center">
            <?php
            switch ($row2['tipe_rujukan']) {
              case 1:
                if ($row2['konfirmasi'] == -1) {
                  echo '-';
                } else if ($row2['konfirmasi'] == 0) {
                  echo 'Negatif';
                } else if ($row2['konfirmasi'] == 1) {
                  echo 'Positif';
                }
                break;

              case 2:
                echo float_id($row2['rentang_min']) . " - " . float_id($row2['rentang_max']) . " " . $row2['satuan'];
                break;

              case 3:
                echo "Laki-laki : " . float_id($row2['rentang_l_min']) . " - " . float_id($row2['rentang_l_max']) . " " . $row2['satuan'] . "<br>";
                echo "Perempuan : " . float_id($row2['rentang_p_min']) . " - " . float_id($row2['rentang_p_max']) . " " . $row2['satuan'];
                break;

              case 4:
                echo "< "  . float_id($row2['kurang']) . " " . $row2['satuan'];
                break;

              case 5:
                echo "Laki-laki : < " . float_id($row2['kurang_l']) . $row2['satuan'] . "<br>";
                echo "Perempuan : < " . float_id($row2['kurang_p'])  . $row2['satuan'];
                break;

              case 6:
                echo "> " . float_id($row2['lebih']) . " " . $row2['satuan'];
                break;

              case 7:
                echo "Laki-laki : > " . float_id($row2['lebih_l']) . $row2['satuan'] . "<br>";
                echo "Perempuan : > " . float_id($row2['lebih_p'])  . $row2['satuan'];
                break;

              case 8:
                echo $row2['informasi'];
                break;
            }
            ?>
          </td>
          <td class="text-center">
            <div class="form-group">
              <div class="col-md-12">
                <input type="hidden" class="hasilpemeriksaanrinc_id_<?= $row2['pemeriksaanrinc_id'] ?>" name="pemeriksaanrinc_id[]" value="<?= $row2['pemeriksaanrinc_id'] ?>">
                <input type="hidden" name="tarif_id[]" value="<?= $row2['tarif_id'] ?>">
                <?php if ($row2['tipe_rujukan'] == 1) : ?>
                  <select class="form-control chosen-select" name="hasil_lab[]" id="hasil_lab_<?= $row2['pemeriksaanrinc_id'] ?>" onchange="calc_hasil_lab(this)" data-pemeriksaanrinc-id="<?= $row2['pemeriksaanrinc_id'] ?>" name="hasil_lab[]">
                    <!-- <option value="0" <?= ($row2['konfirmasi'] == 0) ? 'selected' : '' ?>>Negatif</option> -->
                    <!-- <option value="1" <?= ($row2['konfirmasi'] == 1) ? 'selected' : '' ?>>Positif</option> -->
                    <option value="0" <?= ($row2['hasil_lab'] == 0) ? 'selected' : '' ?>>Negatif</option>
                    <option value="1" <?= ($row2['hasil_lab'] == 1) ? 'selected' : '' ?>>Positif</option>
                  </select>
                <?php elseif ($row2['tipe_rujukan'] > 1 && $row2['tipe_rujukan'] <= 7) : ?>
                  <input type="text" class="form-control autonumeric-float text-center" id="hasil_lab_<?= $row2['pemeriksaanrinc_id'] ?>" name="hasil_lab[]" onkeyup="calc_hasil_lab(this)" data-pemeriksaanrinc-id="<?= $row2['pemeriksaanrinc_id'] ?>" value="<?= float_id($row2['hasil_lab']) ?>">
                <?php else : ?>
                  <input type="text" class="form-control text-center" id="hasil_lab_<?= $row2['pemeriksaanrinc_id'] ?>" data-pemeriksaanrinc-id="<?= $row2['pemeriksaanrinc_id'] ?>" onkeyup="calc_hasil_lab(this)" name="hasil_lab[]" value="<?= $row2['hasil_lab'] ?>">
                <?php endif; ?>
              </div>
            </div>
          </td>
          <td class="text-left">
            <div class="form-group">
              <div class="col-md-12">
                <input type="text" class="form-control" name="catatan_lab[]" id="catatan_lab_<?= $row2['pemeriksaanrinc_id'] ?>" data-pemeriksaanrinc-id="<?= $row2['pemeriksaanrinc_id'] ?>" value="<?= $row2['catatan_lab'] ?>">
              </div>
            </div>
          </td>
          <td class="text-center">
            <?php if ($row2['is_tagihan'] == 1) : ?>
              <a href="javascript:void(0)" class="btn btn-danger btn-table btn-delete" data-pemeriksaanrinc-id="<?= $row2['pemeriksaanrinc_id'] ?>" data-pemeriksaan-id="<?= $row2['pemeriksaan_id'] ?>" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
            <?php endif; ?>
          </td>
        </tr>
      <?php endforeach; ?>
    <?php endif; ?>
  <?php endforeach; ?>
<?php endif; ?>

<script type="text/javascript">
  $(document).ready(function() {
    // delete
    $('.btn-delete').on('click', function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Apakah Anda yakin?',
        text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#eb3b5a',
        cancelButtonColor: '#b2bec3',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        customClass: 'swal-wide'
      }).then((result) => {
        if (result.value) {
          var pemeriksaanrinc_id = $(this).attr("data-pemeriksaanrinc-id");
          var pemeriksaan_id = $(this).attr("data-pemeriksaan-id");
          //
          $('#hasil_pemeriksaan_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
          $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_hasil_pemeriksaan/delete_data' ?>', {
            pemeriksaanrinc_id: pemeriksaanrinc_id,
            pemeriksaan_id: pemeriksaan_id
          }, function(data) {
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil dihapus.',
              icon: 'success',
              position: 'top-right'
            })
            $('#hasil_pemeriksaan_data').html(data.html);
          }, 'json');
        }
      })
    })

    $('.modal-href').click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");
      var pemeriksaanrinc_id = $(this).attr("data-pemeriksaanrinc-id");

      $("#modal-title").html(modal_title);
      $("#modal-size").addClass('modal-' + modal_size);
      if (modal_custom_size) {
        $("#modal-size").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
      }
      $("#myModal").modal('show');
      $.post($(this).data('href'), {
        pemeriksaanrinc_id: pemeriksaanrinc_id
      }, function(data) {
        $("#modal-body").html(data.html);
      }, 'json');
    });

    $(".autonumeric-float").autoNumeric({
      aSep: ".",
      aDec: ",",
      aForm: true,
      vMax: "999999999999999999.99999999",
      vMin: "-999999999999999999.99999999",
      aPad: false
    });

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    $(".chosen-select").on('change', function() {
      calc_hasil_lab(this);
    })
  });

  function calc_hasil_lab(obj) {
    var pemeriksaan_id = $("#pemeriksaan_id").val();
    var pemeriksaanrinc_id = $(obj).data("pemeriksaanrinc-id");
    var hasil_lab = $(obj).val();
    $.ajax({
      type: 'post',
      url: '<?= site_url() . '/penunjang/laboratorium/kunjungan/ajax_hasil_pemeriksaan/hasil_lab' ?>',
      data: 'pemeriksaan_id=' + pemeriksaan_id + '&pemeriksaanrinc_id=' + pemeriksaanrinc_id + '&hasil_lab=' + hasil_lab,
      dataType: 'json',
      success: function(data) {
        $("#catatan_lab_" + pemeriksaanrinc_id).val(data.hasil);
      }
    })
  }
</script>