<!-- js -->
<?php $this->load->view('penunjang/laboratorium/kunjungan/_js_hasil_pemeriksaan_edit')?>
<!-- / -->
<form id="hasil_pemeriksaan_edit" action="" method="post" autocomplete="off">
  <input type="hidden" name="pemeriksaanrinc_id" id="pemeriksaanrinc_id" value="<?=@$main['pemeriksaanrinc_id']?>">
  <input type="hidden" name="pemeriksaan_id" id="pemeriksaan_id" value="<?=@$main['pemeriksaan_id']?>">
  <div class="row">
    <div class="col-md-12">
      <div class="form-group row mb-2">
        <label class="col-lg-3 col-md-3 col-form-label">Item <span class="text-danger">*</span></label>
        <div class="col-lg-9 col-md-9">
          <select class="form-control select2" name="itemlab_id" id="itemlab_id">
            <option value="">- Pilih -</option>
          </select>
        </div>
      </div>
      <div class="form-group row mb-2">
        <label class="col-lg-3 col-md-3 col-form-label">Hasil</label>
        <div class="col-lg-9 col-md-9">
          <input type="text" class="form-control" name="hasil_lab" value="<?=@$main['hasil_lab']?>">
        </div>
      </div>
      <div class="form-group row mb-2">
        <label class="col-lg-3 col-md-3 col-form-label">Keterangan</label>
        <div class="col-lg-9 col-md-9">
          <input type="text" class="form-control" name="catatan_lab" value="<?=@$main['catatan_lab']?>">
        </div>
      </div>
    </div>
  </div>
  <div class="border-top border-2 mt-2 pt-2 pb-0">
    <div class="row">
      <div class="offset-lg-8 offset-md-8">
        <button type="button" class="btn btn-xs btn-secondary" id="edit_cancel" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
        <button type="submit" class="btn btn-xs btn-primary" id="edit_action"><i class="fas fa-save"></i> Simpan</button>
      </div>
    </div>
  </div>
</form>