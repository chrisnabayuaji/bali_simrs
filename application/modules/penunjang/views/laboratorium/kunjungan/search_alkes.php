<div class="table-responsive">
  <table id="obat_table" class="table table-hover table-bordered table-striped table-sm w-100">
    <thead>
      <tr>
        <th class="text-center" width="20">No.</th>
        <th class="text-center" width="50">Kode</th>
        <th class="text-center" width="100">No Batch</th>
        <th class="text-center">Nama Obat</th>
        <th class="text-center" width="80">Satuan</th>
        <th class="text-center" width="100">Jenis Barang</th>
        <th class="text-center" width="100">Tgl. Expired</th>
        <th class="text-center" width="100">Stok Barang</th>
        <th class="text-center" width="60">Aksi</th>
      </tr>
    </thead>
  </table>
</div>

<script type="text/javascript">
  var obatTable;
  $(document).ready(function () {
    var map_lokasi_depo = $("#map_lokasi_depo").val();
    //datatables
    obatTable = $('#obat_table').DataTable({
      "autoWidth" : false,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
      "ajax": {
        "url": "<?php echo site_url().'/'.'penunjang/laboratorium/kunjungan'.'/ajax_alkes/search_data'?>",
        "type": "POST",
        "data": {
          'map_lokasi_depo' : map_lokasi_depo
        }
      },
      "columnDefs": [
        {"targets": 0, "className": 'text-center', "orderable" : false},
        {"targets": 1, "className": 'text-center', "orderable" : true},
        {"targets": 2, "className": 'text-center', "orderable" : false},
        {"targets": 3, "className": 'text-left', "orderable" : true},
        {"targets": 4, "className": 'text-center', "orderable" : false},
        {"targets": 5, "className": 'text-center', "orderable" : false},
        {"targets": 6, "className": 'text-center', "orderable" : true},
        {"targets": 7, "className": 'text-right', "orderable" : false},
        {"targets": 8, "className": 'text-center', "orderable" : false},
      ],
    });
    obatTable.columns.adjust().draw();
  })
</script>