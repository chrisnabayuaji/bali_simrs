<script type="text/javascript">
  var pasienTable;
  $(document).ready(function () {
    //datatables
    pasienTable = $('#pasien_table').DataTable({
      "autoWidth" : false,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
      "ajax": {
          "url": "<?php echo site_url().'/'.'penunjang/radiologi/datang'.'/get_data_pasien'?>",
          "type": "POST"
      },      
      "columnDefs": [
        {"targets": 0, "className": 'text-center', "orderable" : false},
        {"targets": 1, "className": 'text-center', "orderable" : false},
        {"targets": 2, "className": 'text-left', "orderable" : false},
        {"targets": 3, "className": 'text-left', "orderable" : false},
        {"targets": 4, "className": 'text-center', "orderable" : false},
        {"targets": 5, "className": 'text-left', "orderable" : false},
        {"targets": 6, "className": 'text-center', "orderable" : false},
        {"targets": 7, "className": 'text-center', "orderable" : false},
      ],
    });
    pasienTable.columns.adjust().draw();
  })
</script>