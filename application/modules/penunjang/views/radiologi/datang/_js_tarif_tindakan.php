<script type="text/javascript">
  $(document).ready(function () {

    var pasien_id = $("#pasien_id").val();
    var reg_id = $("#reg_id").val();
    var lokasi_id = $("#lokasi_id").val();
    var pemeriksaan_id = $("#pemeriksaan_id").val();
    tarif_tindakan_data(reg_id, pemeriksaan_id);

  })

  function tarif_tindakan_data(reg_id='', pemeriksaan_id='') {
    $('#tarif_tindakan_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?=site_url().'/penunjang/radiologi/datang/ajax_tarif_tindakan/data'?>', {reg_id: reg_id, pemeriksaan_id: pemeriksaan_id}, function (data) {
      $('#tarif_tindakan_data').html(data.html);
    }, 'json');
  }
</script>