<!-- js -->
<?php $this->load->view('pelayanan/registrasi_offline/_js_caller')?>
<!-- / -->
<div class="row">
  <div class="col-lg-12">
    <input id="caller_lokasi_id" type="hidden" value="<?=@$caller['lokasi_id']?>">
    <div id="antrian_container">
      <h1 id="now_antrian" class="text-center text-primary" style="font-size:5rem">-</h1>
      <h6 id="total_antrian" class="text-center">Antrian Terlayani : <?=@$caller['antrian_cd'].'.'.@$caller['call_antrian_no']?>/<?=@$caller['antrian_cd']?>.<?=@$caller['antrian_no']?></h6>
      <input id="antrian" type="hidden" value="<?=@$caller['antrian_cd']?>.<?=@$caller['call_antrian_no']?>">
      <div class="row mt-3">
        <div class="col"></div>
        <div class="col">
          <div id="tanggal" class="alert alert-success text-center p-1" role="alert"><?=date('d-m-Y')?></div>
        </div>
        <div class="col"></div>
      </div>
      <div class="form-group row mb-2">
        <label class="col-lg-5 col-md-3 col-form-label">Loket</label>
        <div class="col-lg-3 col-md-3">
          <select class="form-control chosen-select" name="loket" id="loket">
            <?php for($i=1;$i<=9;$i++):?>
              <option value="<?=$i?>"><?=$i?></option>
            <?php endfor;?>
          </select>
        </div>
      </div>
      <div class="row">
        <div class="col-5">
          <button type="button" id="btn_recall_antrian" class="btn-call btn btn-warning btn-block" onclick="recall_antrian()"><i class="fas fa-sync-alt"></i> Panggil Ulang</button>
        </div>
        <div class="col-7">
          <button type="button" id="btn_call_antrian" class="btn-call btn btn-primary btn-block" onclick="call_antrian()" disabled>-</button>
        </div>
      </div>
      <div class="row mt-3">
        <div class="col">
          <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#random_modal"><i class="fas fa-random"></i> Panggil Acak</button>
        </div>
      </div>
    </div>
    <div id="belum_container">
      Belum ada antrian hari ini
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="random_modal" tabindex="-1" role="dialog" aria-labelledby="random_modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="random_modalLabel">Pemanggilan Acak</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="input-group mb-3">
          <div class="input-group-prepend">
            <span class="input-group-text" style="font-size:20px"><?=@$caller['antrian_cd']?>.</span>
            <input type="hidden" name="random_antrian_cd" id="random_antrian_cd"  value="<?=@$caller['antrian_cd']?>">
          </div>
          <input id="random_antrian_no" type="number" class="form-control" style="font-size:20px" placeholder="Nomor">
          <div class="input-group-append">
            <button id="btn-recall" type="button" class="btn btn- btn-primary" onclick="random_antrian()"><i class="fas fa-headset"></i> Panggil</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>