<!-- js -->
<?php $this->load->view('_js_hasil_pemeriksaan'); ?>
<div class="row">
  <div class="col">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-copy"></i> Hasil Pemeriksaan Radiologi</h4>
        <form role="form" id="form-data" method="POST" enctype="multipart/form-data" action="<?= site_url('/penunjang/radiologi/datang/save_hasil_pemeriksaan/' . $reg_id . '/' . $pemeriksaan_id) ?>" class="needs-validation" novalidate autocomplete="off">
          <input type="hidden" name="petugas_id" value="<?= @$main['dokter_id'] ?>">
          <input type="hidden" name="pasien_id" value="<?= @$main['pasien_id'] ?>">
          <input type="hidden" name="lokasi_id" value="<?= @$main['lokasi_id'] ?>">
          <input type="hidden" name="kelas_id" value="<?= @$main['kelas_id'] ?>">
          <input type="hidden" name="pemeriksaan_id" value="<?= @$main['pemeriksaan_id'] ?>">
          <div class="row mb-1 mt-n1">
            <div class="col-md-3 col-sm-6">
              <div class="form-group row">
                <label class="col-lg-4 col-md-4 col-form-label">Tgl.Hasil</label>
                <div class="col-lg-8 col-md-8">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                    </div>
                    <input type="text" class="form-control datetimepicker text-left" name="tgl_hasil" id="tgl_hasil" value="<?php if (@$get_tgl_hasil['tgl_hasil'] != '') {
                                                                                                                              echo to_date(@$get_tgl_hasil['tgl_hasil'], '-', 'full_date');
                                                                                                                            } else {
                                                                                                                              echo date('d-m-Y H:i:s');
                                                                                                                            } ?>">
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6 col-sm-6">
              <!-- <a href="javascript:void(0)" data-href="<?= site_url() . '/penunjang/radiologi/datang/ajax_hasil_pemeriksaan/modal/' . $reg_id . '/' . $pemeriksaan_id ?>" modal-title="Tambah Item Pemeriksaan" modal-size="md" modal-custom-size="700" class="btn btn-xs btn-primary modal-href" title="Tambah Item Pemeriksaan"><i class="fas fa-plus-circle"></i> TAMBAH ITEM PEMERIKSAAN</a> -->
              <!-- <a target="_blank" href="<?= site_url() . '/penunjang/radiologi/datang/print_hasil_pemeriksaan/' . $reg_id . '/' . $pemeriksaan_id ?>" class="btn btn-xs btn-primary" title="Cetak Hasil"><i class="fas fa-print"></i> Cetak Hasil</a> -->
            </div>
          </div>
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-sm">
              <thead>
                <tr>
                  <th class="text-center" width="20">No</th>
                  <th class="text-center" width="135">Kode</th>
                  <th class="text-center" width="135">Cetak Hasil Ekspertisi</th>
                  <th class="text-center">Item Pemeriksaan</th>
                  <th class="text-center">Hasil</th>
                  <th class="text-center">Keterangan</th>
                  <th class="text-center" width="60">Aksi</th>
                </tr>
              </thead>
              <tbody id="hasil_pemeriksaan_data"></tbody>
            </table>
          </div>
          <div class="row mt-3">
            <div class="col-md-6 col-sm-6">
              <div class="form-group row mb-1">
                <label class="col-lg-3 col-md-3 col-form-label mt-n3">Dokter Radiolog <span class="text-danger">*</span></label>
                <div class="col-lg-7 col-md-7">
                  <select class="form-control select2" name="dokterpj_id" id="petugas_id" required="">
                    <option value="">- Pilih -</option>
                  </select>
                </div>
                <div class="col-lg-1 col-md-1">
                  <a href="javascript:void(0)" data-href="<?= site_url() . '/penunjang/radiologi/datang/ajax_hasil_pemeriksaan/search_petugas' ?>" modal-title="List Data Petugas" modal-size="lg" class="btn btn-xs btn-default modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Petugas" data-original-title="Cari Petugas"><i class="fas fa-search"></i></a>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-lg-3 col-md-3 col-form-label">Tindak Lanjut</label>
                <div class="col-lg-3 col-md-3">
                  <div class="form-check form-radio form-check-primary">
                    <label class="form-check-label label-radio">
                      <input type="radio" class="form-check-input periksa_st" name="periksa_st" value="0" <?php if (@$main['periksa_st_rad'] == '0') echo 'checked' ?>>
                      Belum Selesai
                      <i class="input-helper"></i></label>
                  </div>
                </div>
                <div class="col-lg-3 col-md-3">
                  <div class="form-check form-radio form-check-primary">
                    <label class="form-check-label label-radio">
                      <input type="radio" class="form-check-input periksa_st" name="periksa_st" value="1" <?php if (@$main['periksa_st_rad'] == '1') echo 'checked' ?>>
                      Selesai
                      <i class="input-helper"></i></label>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-2">
              <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>