<script type="text/javascript">
  $(document).ready(function() {
    <?php if (@$cookie['search']['tgl_registrasi_from'] == '') : ?>
      $('.datepicker-from').val('');
    <?php endif; ?>
    <?php if (@$cookie['search']['tgl_registrasi_to'] == '') : ?>
      $('.datepicker-to').val('');
    <?php endif; ?>
  })
</script>
<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline col-title">
        <div class="col-back">
          <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
        </div>
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          <?= $nav['nav_nm'] ?> > Pasien Datang Sendiri
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item"><a href="#"><i class="fas fa-radiation-alt"></i> Radiologi</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> Pasien Datang Sendiri</a></li>
          <li class="breadcrumb-item active"><span>Index</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>
  <!-- flash message -->
  <div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
  <!-- /flash message -->
  <div class="row full-page mt-4 mb-n2">
    <div class="col-lg-12 grid-margin-xl-0 stretch-card">
      <div class="card border-none">
        <div class="card-body">
          <div class="row mt-n3 mb-1">
            <div class="col-md-12 mt-3">
              <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link tab-menu" href="<?= site_url('penunjang/radiologi/kunjungan') ?>"><i class="fas fa-users"></i> LIST KUNJUNGAN</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link tab-menu active" href="<?= site_url('penunjang/radiologi/datang') ?>"><i class="fas fa-user-injured"></i> PASIEN DATANG SENDIRI</a>
                </li>
              </ul>
            </div>
          </div>
          <form id="form-search" action="<?= site_url() . '/app/search_redirect/' . $nav['nav_id'] . '.datang' ?>" method="post" autocomplete="off">
            <input type="hidden" name="redirect" value="penunjang/radiologi/datang">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Tgl.Registrasi</label>
                  <div class="col-lg-8 col-md-8">
                    <div class="row">
                      <div class="col-md-5" style="padding-right: 5px; flex: 0 0 46.4%; max-width: 46.4%;">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datepicker datepicker-from text-center" name="tgl_registrasi_from" id="tgl_registrasi_from" value="<?= @$cookie['search']['tgl_registrasi_from'] ?>">
                        </div>
                      </div>
                      <div class="col-md-2 text-center-group">s.d</div>
                      <div class="col-md-5" style="padding-left: 5px; flex: 0 0 46.4%; max-width: 46.4%;">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datepicker datepicker-to text-center" name="tgl_registrasi_to" id="tgl_registrasi_to" value="<?= @$cookie['search']['tgl_registrasi_to'] ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">No.RM/Nama Pasien</label>
                  <div class="col-lg-8 col-md-8">
                    <input type="text" class="form-control" name="no_rm_nm" id="no_rm_nm" value="<?= @$cookie['search']['no_rm_nm'] ?>">
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 row">
                <div class="col-md-4">
                  <?php if ($nav['_add']) : ?>
                    <a href="<?= site_url() . '/penunjang/radiologi/datang/form' ?>" modal-title="Tambah Data" modal-size="md" class="btn btn-xs btn-danger" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Tambah Data"><i class="fas fa-plus-circle"></i> Tambah</a>
                  <?php endif; ?>
                </div>
                <div class="col-md-6 pl-1">
                  <button type="submit" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cari Data"><i class="fas fa-search"></i> Cari</button>
                  <a class="btn btn-xs btn-default" href="<?= site_url() . '/app/reset_redirect/' . $nav['nav_id'] . '.datang/' . str_replace('/', '-', 'penunjang/radiologi/datang') ?>" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-default" title="" data-original-title="Reset"><i class="fas fa-sync-alt"></i> Reset</a>
                </div>
              </div>
            </div>
          </form>
          <div class="row mt-4 pagination-info">
            <div class="col-md-6">
              <form id="form-paging" action="<?= site_url() . '/app/per_page/' . 'penunjang/radiologi/datang' ?>" method="post">
                <label><i class="mdi mdi-bookmark"></i> Tampilkan</label>
                <select name="per_page" class="select-pagination" onchange="$('#form-paging').submit()">
                  <option value="10" <?php if ($cookie['per_page'] == 10) {
                                        echo 'selected';
                                      } ?>>10</option>
                  <option value="50" <?php if ($cookie['per_page'] == 50) {
                                        echo 'selected';
                                      } ?>>50</option>
                  <option value="100" <?php if ($cookie['per_page'] == 100) {
                                        echo 'selected';
                                      } ?>>100</option>
                </select>
                <label>data per halaman.</label>
              </form>
            </div>
            <div class="col-md-6 text-right">
              <div class="pt-1"><?= @$pagination_info ?></div>
            </div>
          </div><!-- /.row -->
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                <form id="form-multiple" action="<?= site_url() . '/' . $nav['nav_url'] . '/multiple/enable' ?>" method="post">
                  <table class="table table-hover table-striped table-bordered table-fixed">
                    <thead>
                      <tr>
                        <th class="text-center" width="36">No</th>
                        <th class="text-center" width="36">
                          <div class="form-check form-check-primary form-check-th">
                            <label class="form-check-label">
                              <input type="checkbox" class="form-check-input cb-all">
                            </label>
                          </div>
                        </th>
                        <th class="text-center" width="80">Aksi</th>
                        <th class="text-center" width="120"><?= table_sort('penunjang/radiologi/datang', 'No. Register', 'reg_id', $cookie['order']) ?></th>
                        <th class="text-center" width="100"><?= table_sort('penunjang/radiologi/datang', 'Nomor RM', 'pasien_id', $cookie['order']) ?></th>
                        <th class="text-center"><?= table_sort('penunjang/radiologi/datang', 'Nama Pasien', 'pasien_nm', $cookie['order']) ?></th>
                        <th class="text-center">Alamat</th>
                        <th class="text-center" width="30">JK</th>
                        <th class="text-center" width="110"><?= table_sort('penunjang/radiologi/datang', 'Jenis Pasien', 'jenispasien_id', $cookie['order']) ?></th>
                        <th class="text-center" width="170"><?= table_sort('penunjang/radiologi/datang', 'Lokasi', 'lokasi_nm', $cookie['order']) ?></th>
                        <th class="text-center" width="170"><?= table_sort('penunjang/radiologi/datang', 'Tgl. Registrasi', 'tgl_registrasi', $cookie['order']) ?></th>
                        <th class="text-center" width="70">Periksa</th>
                        <th class="text-center" width="70">Pulang</th>
                      </tr>
                    </thead>
                    <?php if (@$main == null) : ?>
                      <tbody>
                        <tr>
                          <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                        </tr>
                      </tbody>
                    <?php else : ?>
                      <tbody>
                        <?php $i = 1;
                        foreach ($main as $row) : ?>
                          <tr>
                            <td class="text-center" width="36"><?= $cookie['cur_page'] + ($i++) ?></td>
                            <td class="text-center" width="36">
                              <div class="form-check form-check-primary form-check-td">
                                <label class="form-check-label">
                                  <input type="checkbox" class="form-check-input cb-item" name="checkitem[]" value="<?= $row['reg_id'] ?>">
                                </label>
                              </div>
                            </td>
                            <td class="text-center" width="80">
                              <?php if ($nav['_update'] || $nav['_delete']) : ?>
                                <div class="btn-group">
                                  <button type="button" class="btn btn-primary btn-table dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Aksi
                                  </button>
                                  <div class="dropdown-menu">
                                    <?php if ($nav['_update']) : ?>
                                      <a class="dropdown-item" href="<?= site_url() . '/penunjang/radiologi/datang/periksa/' . $row['reg_id'] . '/' . $row['pemeriksaan_id'] ?>#hasil-pemeriksaan"><i class="fas fa-stethoscope"></i> Periksa</a>
                                      <a class="dropdown-item" href="<?= site_url() . '/penunjang/radiologi/datang/order/' . $row['reg_id'] . '/' . $row['pemeriksaan_id'] ?>"><i class="fas fa-pencil-alt"></i> Ubah Order Pemeriksaan Radiologi</a>
                                      <a class="dropdown-item" href="<?= site_url() . '/penunjang/radiologi/datang/form/' . $row['reg_id'] ?>"><i class="fas fa-pencil-alt"></i> Ubah Data Pasien</a>
                                    <?php endif; ?>
                                    <?php if ($nav['_delete']) : ?>
                                      <a class="dropdown-item btn-delete" href="#" data-href="<?= site_url() . '/' . '/penunjang/radiologi/datang/delete/' . $row['reg_id'] . '/' . $row['pemeriksaan_id'] ?>"><i class="fas fa-trash-alt"></i> Hapus Data</a>
                                    <?php endif; ?>
                                  </div>
                                </div>
                              <?php endif; ?>
                            </td>
                            <td class="text-center" width="120"><?= $row['reg_id'] ?></td>
                            <td class="text-center" width="100"><?= $row['pasien_id'] ?></td>
                            <td class="text-left"><?= $row['pasien_nm'] . ', ' . $row['sebutan_cd'] ?></td>
                            <td class="text-left">
                              <?= ($row['alamat'] != '') ? $row['alamat'] . ', ' : '' ?>
                              <?= ($row['kelurahan'] != '') ? ucwords(strtolower($row['kelurahan'])) . ', ' : '' ?>
                              <?= ($row['kecamatan'] != '') ? ucwords(strtolower($row['kecamatan'])) . ', ' : '' ?>
                              <?= ($row['kabupaten'] != '') ? ucwords(strtolower($row['kabupaten'])) . ', ' : '' ?>
                              <?= ($row['provinsi'] != '') ? ucwords(strtolower($row['provinsi'])) : '' ?>
                            </td>
                            <td class="text-center" width="30"><?= $row['sex_cd'] ?></td>
                            <td class="text-center" width="110"><?= $row['jenispasien_nm'] ?></td>
                            <td class="text-left" width="170"><?= $row['lokasi_nm'] ?></td>
                            <td class="text-center" width="170"><?= $row['tgl_registrasi'] ?></td>
                            <td class="text-center" width="70">
                              <li class="fa <?= ($row['periksa_st'] == '1' ? 'fa-check-circle text-success' : 'fa-minus-circle text-danger blink') ?>"></li>
                            </td>
                            <td class="text-center" width="70">
                              <li class="fa <?= ($row['pulang_st'] == '1' ? 'fa-check-circle text-success' : 'fa-minus-circle text-danger') ?>"></li>
                            </td>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                    <?php endif; ?>
                  </table>
                </form>
              </div>
            </div>
          </div><!-- /.row -->
          <div class="row mt-2">
            <div class="col-6 text-middle pt-1">
              <?php if ($nav['_update'] == 1 || $nav['_delete'] == 1) : ?>
                <div class="input-group-prepend">
                  <button type="button" class="btn btn-xs btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    Aksi Multi
                  </button>
                  <div class="dropdown-menu">
                    <?php if ($nav['_update'] == 1) : ?>
                      <a class="dropdown-item" href="javascript:multipleAction('enable')">Aktif</a>
                      <a class="dropdown-item" href="javascript:multipleAction('disable')">Non Aktif</a>
                    <?php endif; ?>
                    <?php if ($nav['_delete'] == 1) : ?>
                      <a class="dropdown-item" href="javascript:multipleAction('delete')">Hapus</a>
                    <?php endif; ?>
                  </div>
                </div>
              <?php endif; ?>
            </div>
            <div class="col-6 text-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>