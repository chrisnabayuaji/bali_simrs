<form id="radiologi_form" action="" method="post" autocomplete="off">
  <div class="row">
    <input type="hidden" name="pemeriksaan_id" id="pemeriksaan_id" value="<?= @$mainrad['pemeriksaan_id'] ?>">
    <input type="hidden" name="reg_id" id="reg_id" value="<?= @$reg['reg_id'] ?>">
    <input type="hidden" name="src_reg_id" id="src_reg_id" value="<?= @$reg['reg_id'] ?>">
    <input type="hidden" name="pasien_id" id="pasien_id" value="<?= @$reg['pasien_id'] ?>">
    <input type="hidden" name="kelas_id" id="kelas_id" value="<?= @$reg['kelas_id'] ?>">
    <div class="col">
      <div class="table-responsive">
        <table id="radiologi_table" class="table table-hover table-bordered table-striped table-sm w-100">
          <thead>
            <tr>
              <th class="text-center" width="20">No.</th>
              <th class="text-center" width="50">Kode</th>
              <th class="text-center">Nama </th>
              <th class="text-center" width="20">Aksi</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
  <hr>
  <div class="col-4 offset-md-8 ">
    <div class="float-right">
      <button id="radiologi_cancel" type="button" class="btn btn-xs btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
      <button id="radiologi_action" type="submit" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
    </div>
  </div>
</form>
<script>
  var radiologiTable;
  $(document).ready(function() {
    $('.datetimepicker').daterangepicker({
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY HH:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })

    //datatables
    radiologiTable = $('#radiologi_table').DataTable({
      'retrieve': true,
      "autoWidth": false,
      "processing": true,
      "serverSide": false,
      "order": [],
      "ajax": {
        "url": "<?php echo site_url() . '/penunjang/radiologi/datang/ajax_hasil_pemeriksaan/search_data/' . @$reg['reg_id'] . '/' . @$mainrad['pemeriksaan_id'] ?>",
        "type": "POST"
      },
      "columnDefs": [{
          "targets": 0,
          "className": 'text-center',
          "orderable": false
        },
        {
          "targets": 1,
          "className": 'text-left',
          "orderable": false
        },
        {
          "targets": 2,
          "className": 'text-left',
          "orderable": false
        },
        {
          "targets": 3,
          "className": 'text-center',
          "orderable": false
        }
      ],
    });
    radiologiTable.columns.adjust().draw();


    //FORM
    var radiologi_form = $("#radiologi_form").validate({
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $("#radiologi_action").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#radiologi_action").attr("disabled", "disabled");
        $("#radiologi_cancel").attr("disabled", "disabled");

        radiologiTable.$('input[type="checkbox"]').each(function() {
          // If checkbox doesn't exist in DOM
          if (!$.contains(document, this)) {
            // If checkbox is checked
            if (this.checked) {
              // Create a hidden element
              $(form).append(
                $('<input>')
                .attr('type', 'hidden')
                .attr('name', this.name)
                .val(this.value)
              );
              itemrad_id = $(this).data("itemrad-id");
              tgl_hasil = $(this).data("tgl_hasil");
              hasil_rad = $(this).data("hasil-rad");
              catatan_rad = $(this).data("catatan-rad");
              $(form).append(
                $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'itemrad_id[]')
                .val(itemrad_id)
              );
              $(form).append(
                $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'tgl_hasil_' + itemrad_id)
                .val(tgl_hasil)
              );
              $(form).append(
                $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'hasil_rad_' + itemrad_id)
                .val(hasil_rad)
              );
              $(form).append(
                $('<input>')
                .attr('type', 'hidden')
                .attr('name', 'catatan_rad_' + itemrad_id)
                .val(catatan_rad)
              );
            }
          }
        });

        console.log($(form).serialize());

        $.ajax({
          type: 'post',
          url: '<?= site_url() ?>/penunjang/radiologi/datang/ajax_hasil_pemeriksaan/save_radiologi/<?= @$reg['reg_id'] . '/' . @$mainrad['pemeriksaan_id'] ?>',
          data: $(form).serialize(),
          dataType: 'json',
          success: function(data) {

          }
        })
        $("#myModal").modal('hide');
        $.toast({
          heading: 'Sukses',
          text: 'Data berhasil disimpan.',
          icon: 'success',
          position: 'top-right'
        })
        hasil_pemeriksaan_data('<?= $mainrad['pemeriksaan_id'] ?>');
        return false;
      }
    });


  })
</script>