<!-- js -->
<?php $this->load->view('_js_tarif_tindakan'); ?>
<div class="row">
  <div class="col-lg-8">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-copy"></i> Tarif Tindakan</h4>
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-sm">
            <thead>
              <tr>
                <th class="text-center" width="20">No</th>
                <th class="text-center" width="135">Kode</th>
                <th class="text-center">Tindakan</th>
                <th class="text-center">Qty</th>
                <th class="text-center">Biaya</th>
                <th class="text-center">Petugas</th>
              </tr>
            </thead>
            <tbody id="tarif_tindakan_data"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>