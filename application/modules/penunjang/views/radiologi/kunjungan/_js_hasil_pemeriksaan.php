<script type="text/javascript">
  $(document).ready(function() {
    var form = $("#form-data").validate({
      rules: {
        dokterpj_id: {
          valueNotEquals: ""
        },
        operator_id: {
          valueNotEquals: ""
        },
      },
      messages: {
        dokterpj_id: "Pilih salah satu Dokter Radiolog!",
        operator_id: "Pilih salah satu Operator!",
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-n1');
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-n1');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").attr("disabled", "disabled");
        $(".btn-cancel").attr("disabled", "disabled");
        form.submit();
      }
    });

    var pasien_id = $("#pasien_id").val();
    var reg_id = $("#reg_id").val();
    var lokasi_id = $("#lokasi_id").val();
    var pemeriksaan_id = $("#pemeriksaan_id").val();
    hasil_pemeriksaan_data(pemeriksaan_id);

    $("#catatan_medis_cancel").on('click', function() {
      catatan_medis_reset();
    });

    // pemeriksaan_fisik_data();

    $(".pemeriksaan_fisik_field").on('change', function() {
      var pasien_id = $("#pasien_id").val();
      var reg_id = $("#reg_id").val();
      var lokasi_id = $("#lokasi_id").val();
      $.ajax({
        type: 'post',
        url: '<?= site_url($nav['nav_url']) ?>/ajax_pemeriksaan_fisik/save',
        data: $("#pemeriksaan_fisik_form").serialize() + '&pasien_id=' + pasien_id + '&reg_id=' + reg_id + '&lokasi_id=' + lokasi_id,
        success: function(data) {
          pemeriksaan_fisik_data();
        }
      })
    })

    $('.datetimepicker').daterangepicker({
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })

    <?php if (@$main['dokterpj_id'] != '') : ?>
      // autocomplete
      var data2 = {
        id: '<?= @$main['dokterpj_id'] ?>',
        text: '<?= @$main['dokterpj_nm'] ?>'
      };
      var newOption = new Option(data2.text, data2.id, false, false);
      $('#petugas_id').append(newOption).trigger('change');
      $('#petugas_id').val('<?= @$main['dokterpj_id'] ?>');
      $('#petugas_id').trigger('change');
      $('.select2-container').css('width', '100%');
    <?php endif; ?>

    <?php if (@$main['operator_id'] != '') : ?>
      // autocomplete
      var data3 = {
        id: '<?= @$main['operator_id'] ?>',
        text: '<?= @$main['operator_nm'] ?>'
      };
      var newOption = new Option(data3.text, data3.id, false, false);
      $('#operator_id').append(newOption).trigger('change');
      $('#operator_id').val('<?= @$main['operator_id'] ?>');
      $('#operator_id').trigger('change');
      $('.select2-container').css('width', '100%');
    <?php endif; ?>

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    // autocomplete
    $('#petugas_id').select2({
      minimumInputLength: 2,
      ajax: {
        url: "<?= site_url($nav['nav_url']) ?>/ajax_hasil_pemeriksaan/autocomplete_petugas",
        dataType: "json",

        data: function(params) {
          return {
            pegawai_nm: params.term
          };
        },
        processResults: function(data) {
          return {
            results: data
          }
        }
      }
    });

    $('#operator_id').select2({
      minimumInputLength: 2,
      ajax: {
        url: "<?= site_url($nav['nav_url']) ?>/ajax_hasil_pemeriksaan/autocomplete_operator",
        dataType: "json",

        data: function(params) {
          return {
            pegawai_nm: params.term
          };
        },
        processResults: function(data) {
          return {
            results: data
          }
        }
      }
    });
    $('.select2-container').css('width', '100%');

    $('.modal-href').click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");
      var modal_header = $(this).attr("modal-header");
      var modal_content_top = $(this).attr("modal-content-top");

      $("#modal-size").removeClass('modal-lg').removeClass('modal-md').removeClass('modal-sm');

      $("#modal-title").html(modal_title);
      $("#modal-size").addClass('modal-' + modal_size);
      if (modal_custom_size) {
        $("#modal-size").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
      }
      if (modal_content_top) {
        $(".modal-content-top").attr('style', 'margin-top: ' + modal_content_top + ' !important');
      }
      if (modal_header == 'hidden') {
        $("#modal-header").addClass('d-none');
      } else {
        $("#modal-header").removeClass('d-none');
      }
      $("#myModal").modal('show');
      $("#modal-body").html('<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i><br>Loading</div>');
      $.post($(this).data('href'), function(data) {
        $("#modal-body").html(data.html);
      }, 'json');
    });

  })

  function hasil_pemeriksaan_data(pemeriksaan_id = '') {
    $('#hasil_pemeriksaan_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_hasil_pemeriksaan/data' ?>', {
      pemeriksaan_id: pemeriksaan_id
    }, function(data) {
      $('#hasil_pemeriksaan_data').html(data.html);
    }, 'json');
  }

  function petugas_fill(id) {
    $.ajax({
      type: 'post',
      url: '<?= site_url($nav['nav_url'] . '/ajax_hasil_pemeriksaan/petugas_fill') ?>',
      dataType: 'json',
      data: 'pegawai_id=' + id,
      success: function(data) {
        // autocomplete
        var data2 = {
          id: data.pegawai_id,
          text: data.pegawai_nm
        };
        var newOption = new Option(data2.text, data2.id, false, false);
        $('#petugas_id').append(newOption).trigger('change');
        $('#petugas_id').val(data.pegawai_id);
        $('#petugas_id').trigger('change');
        $('.select2-container').css('width', '100%');
        $("#myModal").modal('hide');
      }
    })
  }

  function operator_fill(id) {
    $.ajax({
      type: 'post',
      url: '<?= site_url($nav['nav_url'] . '/ajax_hasil_pemeriksaan/operator_fill') ?>',
      dataType: 'json',
      data: 'pegawai_id=' + id,
      success: function(data) {
        // autocomplete
        var data2 = {
          id: data.pegawai_id,
          text: data.pegawai_nm
        };
        var newOption = new Option(data2.text, data2.id, false, false);
        $('#operator_id').append(newOption).trigger('change');
        $('#operator_id').val(data.pegawai_id);
        $('#operator_id').trigger('change');
        $('.select2-container').css('width', '100%');
        $("#myModal").modal('hide');
      }
    })
  }
</script>