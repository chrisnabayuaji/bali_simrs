<?php if(@$main == null):?>
	<tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else: ?>
	<?php $i=1;foreach($main as $row):?>
	<tr>
		<td class="text-center"><?=$i++?></td>
		<td class="text-center">
		  <a href="javascript:void(0)" class="btn btn-primary btn-table btn-edit" data-bhp-id="<?=$row['bhp_id']?>" data-reg-id="<?=$row['reg_id']?>" title="Ubah Data"><i class="fas fa-pencil-alt"></i></a>
		  <a href="javascript:void(0)" class="btn btn-danger btn-table btn-delete" data-bhp-id="<?=$row['bhp_id']?>" data-reg-id="<?=$row['reg_id']?>" data-lokasi-id="<?=$row['lokasi_id']?>" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
		</td>
    <td class="text-center"><?=$row['barang_id']?></td>
    <td class="text-left"><?=$row['barang_nm']?></td>
    <td class="text-center"><?=$row['qty']?></td>
    <td class="text-left"><?=$row['keterangan_bhp']?></td>
	</tr>
	<?php endforeach; ?>
<?php endif; ?>

<script type="text/javascript">
$(document).ready(function () {
  // edit
  $('.btn-edit').bind('click',function(e) {
    e.preventDefault();
    var bhp_id = $(this).attr("data-bhp-id");
    var reg_id = $(this).attr("data-reg-id");
    //
    $.post('<?=site_url().'/'.'penunjang/radiologi/kunjungan'.'/ajax_bhp/get_data'?>', {bhp_id: bhp_id, reg_id: reg_id}, function (data) {

      $('.datetimepicker').daterangepicker({
        startDate: moment(data.main.tgl_catat),
        endDate: moment(),
        timePicker: true,
        timePicker24Hour: true,
        timePickerSeconds: true,
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
          cancelLabel: 'Clear',
          format: 'DD-MM-YYYY H:mm:ss'
        },
        isInvalidDate: function(date) {
          return '';
        }
      })
      
      $('#bhp_id').val(data.main.bhp_id);

      // autocomplete
      var data2 = {
        id: data.main.barang_id+'#'+data.main.obat_nm,
        text: data.main.barang_id+' - '+data.main.obat_nm
      };
      var newOption = new Option(data2.text, data2.id, false, false);
      $('#barang_id').append(newOption).trigger('change');
      $('#barang_id').val(data.main.barang_id+'#'+data.main.obat_nm);
      $('#barang_id').trigger('change');
      $('.select2-container').css('width', '100%');

      $("#dosis").val(data.main.dosis).removeClass("is-valid").removeClass("is-invalid");
      $("#qty").val(data.main.qty).removeClass("is-valid").removeClass("is-invalid");
      $("#carapakai_cd").val(data.main.carapakai_cd).trigger('change').removeClass("is-valid").removeClass("is-invalid");
      $("#keterangan_bhp").val(data.main.keterangan_bhp).removeClass("is-valid").removeClass("is-invalid");
      $('#bhp_action').html('<i class="fas fa-save"></i> Ubah');
    }, 'json');
  });
  // delete
  $('.btn-delete').on('click', function (e) {
    e.preventDefault();
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#eb3b5a',
      cancelButtonColor: '#b2bec3',
      confirmButtonText: 'Hapus',
      cancelButtonText: 'Batal',
      customClass: 'swal-wide'
    }).then((result) => {
      if (result.value) {
        var bhp_id = $(this).attr("data-bhp-id");
		    var reg_id = $(this).attr("data-reg-id");
		    var lokasi_id = $(this).attr("data-lokasi-id");
		    //
		    $('#bhp_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
		    $.post('<?=site_url().'/'.'penunjang/radiologi/kunjungan'.'/ajax_bhp/delete_data'?>', {bhp_id: bhp_id, reg_id: reg_id, lokasi_id: lokasi_id}, function (data) {
		    	$.toast({
            heading: 'Sukses',
            text: 'Data berhasil dihapus.',
            icon: 'success',
            position: 'top-right'
          })
		      $('#bhp_data').html(data.html);
		    }, 'json');
      }
    })
  })
});
</script>