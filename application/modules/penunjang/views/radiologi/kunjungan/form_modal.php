<table id="table-registrasi" class="table table-hover table-striped table-bordered table-sm">
  <thead>
    <tr>
      <th class="text-center" width="25">No</th>
      <th class="text-center" width="50">No Reg</th>
      <th class="text-center" width="60">Nomor RM</th>
      <th class="text-center" width="200">Nama Pasien</th>
      <th class="text-center">Alamat</th>
      <th class="text-center" width="10">JK</th>
      <th class="text-center" width="80">Jenis Pasien</th>
      <th class="text-center" width="100">Lokasi & Kelas</th>
      <th class="text-center" width="50">Tgl. Registrasi</th>
      <th class="text-center" width="80">Pilih</th>
    </tr>
  </thead>
  <?php if (@$main == null) : ?>
    <tbody>
      <tr>
        <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
      </tr>
    </tbody>
  <?php else : ?>
    <tbody>
      <?php $i = 1;
      foreach ($main as $row) : ?>
        <tr>
          <td class="text-center">
            <?= ($i++) ?>
          </td>
          <td class="text-center"><?= $row['reg_id'] ?></td>
          <td class="text-center"><?= $row['pasien_id'] ?></td>
          <td class="text-left"><?= $row['pasien_nm'] . ', ' . $row['sebutan_cd'] ?></td>
          <td class="text-left">
            <?= ($row['alamat'] != '') ? $row['alamat'] . ', ' : '' ?>
            <?= ($row['kelurahan'] != '') ? ucwords(strtolower($row['kelurahan'])) . ', ' : '' ?>
            <?= ($row['kecamatan'] != '') ? ucwords(strtolower($row['kecamatan'])) . ', ' : '' ?>
            <?= ($row['kabupaten'] != '') ? ucwords(strtolower($row['kabupaten'])) . ', ' : '' ?>
            <?= ($row['provinsi'] != '') ? ucwords(strtolower($row['provinsi'])) : '' ?>
          </td>
          <td class="text-center"><?= $row['sex_cd'] ?></td>
          <td class="text-center"><?= $row['jenispasien_nm'] ?> <br> <?= $row['no_kartu'] ?> <?= ($row['sep_no'] != '') ? '<br>' . $row['sep_no'] : '' ?></td>
          <td class="text-center"><?= $row['lokasi_nm'] ?> <br> <?= get_kelas_nm($row['kelas_nm']) ?></td>
          <td class="text-center"><?= $row['tgl_registrasi'] ?></td>
          <td class="text-center">
            <a class="btn btn-xs btn-table btn-primary" href="<?= site_url('penunjang/radiologi/kunjungan/order/' . $row['reg_id']) ?>">Pilih >></a>
          </td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  <?php endif; ?>
</table>
<script>
  $(document).ready(function() {
    registrasiTable = $('#table-registrasi').DataTable({
      "retrieve": true,
      "autoWidth": false,
      "processing": true,
    });
    registrasiTable.columns.adjust().draw();
  })
</script>