<?php if ($main == null) : ?>
  <tr>
    <td class="text-center" colspan="99">Tidak ada data.</td>
  </tr>
<?php else : ?>
  <?php $no = 1;
  foreach ($main as $row) : ?>
    <?php if ($row['parent_id'] == '') : ?>
      <tr>
        <td colspan="99"><b><?= @$row['itemrad_nm'] ?></b></td>
      </tr>
    <?php else : ?>
      <tr>
        <td class="text-center"><?= $no++ ?></td>
        <td class="text-left"><?= $row['itemrad_id'] ?></td>
        <td class="text-left"></td>
        <td class="text-left pl-<?= count(explode('.', $row['itemrad_id'])) ?>" colspan="3">|- <?= $row['itemrad_nm'] ?></td>
        <td class="text-center">
          <?php if ($row['is_tagihan'] == 1) : ?>
            <a href="javascript:void(0)" class="btn btn-danger btn-table btn-delete" data-pemeriksaanrinc-id="<?= $row['pemeriksaanrinc_id'] ?>" data-pemeriksaan-id="<?= $row['pemeriksaan_id'] ?>" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
          <?php endif; ?>
        </td>
      </tr>
    <?php endif; ?>
    <?php if (count($row['rinc']) > 0) : ?>
      <?php foreach ($row['rinc'] as $row2) : ?>
        <tr>
          <td class="text-center text-top"><?= $no++ ?></td>
          <td class="text-left text-top"><?= $row2['itemrad_id'] ?></td>
          <td class="text-center text-top">
            <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/cetak_modal/print_ekspertisi/' . @$pemeriksaan_id . '/' . $row2['pemeriksaanrinc_id'] ?>" modal-title="Cetak Hasil Ekspertisi Radiologi" modal-size="lg" modal-content-top="-75px" class="btn btn-xs btn-primary modal-href"><i class="fas fa-print"></i> Cetak Ekspertisi</a>
          </td>
          <td class="text-left text-top pl-<?= count(explode('.', $row2['itemrad_id'])) ?>">|- <?= $row2['itemrad_nm'] ?></td>
          <td class="text-center text-top">
            <div class="form-group">
              <div class="col-md-12">
                <input type="hidden" class="form-control" name="pemeriksaanrinc_id[]" value="<?= $row2['pemeriksaanrinc_id'] ?>">
                <input type="hidden" class="form-control" name="tarif_id[]" value="<?= $row2['tarif_id'] ?>">
                <textarea class="form-control" name="hasil_rad[]" rows="4"><?= $row2['hasil_rad'] ?></textarea>
              </div>
            </div>
          </td>
          <td class="text-left text-top">
            <div class="form-group">
              <div class="col-md-12">
                <input type="file" name="file_rad[]" class="form-control">
                <input type="hidden" name="check_file_rad[]" value="<?= $row2['file_rad'] ?>">
                <div class="text-danger text-center">Format File : .jpg .jpeg .png</div>
                <?php if ($row2['file_rad'] != '') : ?>
                  <div class="text-danger text-center">Untuk mengubah foto silahkan upload lagi</div>
                <?php endif; ?>
              </div>
            </div>
          </td>
          <td class="text-center text-top">
            <?php if ($row2['file_rad'] != '') : ?>
              <a href="#" data-href="<?= site_url() . '/' . $nav['nav_url'] . '/detail_foto/' . $row2['pemeriksaanrinc_id'] ?>" modal-title="Detail Foto" modal-size="lg" modal-content-top="-75px" class="btn btn-table btn-primary modal-href"><i class="fas fa-eye"></i> Lihat Foto</a>
            <?php else : ?>
              <div class="text-danger text-center">Tidak ada foto yang diupload</div>
            <?php endif; ?>
          </td>
          <td class="text-center text-top">
            <?php if ($row2['is_tagihan'] == 1) : ?>
              <a href="javascript:void(0)" class="btn btn-danger btn-table btn-delete" data-pemeriksaanrinc-id="<?= $row2['pemeriksaanrinc_id'] ?>" data-pemeriksaan-id="<?= $row2['pemeriksaan_id'] ?>" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
            <?php endif; ?>
          </td>
        </tr>
      <?php endforeach; ?>
    <?php endif; ?>
  <?php endforeach; ?>
<?php endif; ?>

<script type="text/javascript">
  $(document).ready(function() {
    // delete
    $('.btn-delete').on('click', function(e) {
      e.preventDefault();
      Swal.fire({
        title: 'Apakah Anda yakin?',
        text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#eb3b5a',
        cancelButtonColor: '#b2bec3',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        customClass: 'swal-wide'
      }).then((result) => {
        if (result.value) {
          var pemeriksaanrinc_id = $(this).attr("data-pemeriksaanrinc-id");
          var pemeriksaan_id = $(this).attr("data-pemeriksaan-id");
          //
          $('#hasil_pemeriksaan_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
          $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax_hasil_pemeriksaan/delete_data' ?>', {
            pemeriksaanrinc_id: pemeriksaanrinc_id,
            pemeriksaan_id: pemeriksaan_id
          }, function(data) {
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil dihapus.',
              icon: 'success',
              position: 'top-right'
            })
            $('#hasil_pemeriksaan_data').html(data.html);
          }, 'json');
        }
      })
    })

    $('.modal-href').click(function(e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");
      var modal_header = $(this).attr("modal-header");
      var modal_content_top = $(this).attr("modal-content-top");

      $("#modal-size").removeClass('modal-lg').removeClass('modal-md').removeClass('modal-sm');

      $("#modal-title").html(modal_title);
      $("#modal-size").addClass('modal-' + modal_size);
      if (modal_custom_size) {
        $("#modal-size").attr('style', 'max-width: ' + modal_custom_size + 'px !important');
      }
      if (modal_content_top) {
        $(".modal-content-top").attr('style', 'margin-top: ' + modal_content_top + ' !important');
      }
      if (modal_header == 'hidden') {
        $("#modal-header").addClass('d-none');
      } else {
        $("#modal-header").removeClass('d-none');
      }
      $("#myModal").modal('show');
      $("#modal-body").html('<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i><br>Loading</div>');
      $.post($(this).data('href'), function(data) {
        $("#modal-body").html(data.html);
      }, 'json');
    });
  });
</script>