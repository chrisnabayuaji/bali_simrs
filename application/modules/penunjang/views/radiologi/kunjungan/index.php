<script type="text/javascript">
  $(document).ready(function() {
    <?php if (@$cookie['search']['tgl_order_from'] == '') : ?>
      $('.datepicker-from').val('');
    <?php endif; ?>
    <?php if (@$cookie['search']['tgl_order_to'] == '') : ?>
      $('.datepicker-to').val('');
    <?php endif; ?>
  })
</script>
<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline col-title">
        <div class="col-back">
          <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
        </div>
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          <?= $nav['nav_nm'] ?> > List Kunjungan
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item"><a href="#"><i class="fas fa-radiation-alt"></i> Radiologi</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> List Kunjungan</a></li>
          <li class="breadcrumb-item active"><span>Index</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>
  <!-- flash message -->
  <div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
  <!-- /flash message -->
  <div class="row full-page mt-4 mb-n2">
    <div class="col-lg-12 grid-margin-xl-0 stretch-card">
      <div class="card border-none">
        <div class="card-body">
          <div class="row mt-n3 mb-1">
            <div class="col-md-12 mt-3">
              <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link tab-menu active" href="<?= site_url('penunjang/radiologi/kunjungan') ?>"><i class="fas fa-users"></i> LIST KUNJUNGAN</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link tab-menu" href="<?= site_url('penunjang/radiologi/datang') ?>"><i class="fas fa-user-injured"></i> PASIEN DATANG SENDIRI</a>
                </li>
              </ul>
            </div>
          </div>
          <form id="form-search" action="<?= site_url() . '/app/search_redirect/' . $nav['nav_id'] . '.kunjungan' ?>" method="post" autocomplete="off">
            <input type="hidden" name="redirect" value="penunjang/radiologi/kunjungan">
            <div class="row">
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Tgl.Order</label>
                  <div class="col-lg-8 col-md-8">
                    <div class="row">
                      <div class="col-md-5" style="padding-right: 5px; flex: 0 0 46.4%; max-width: 46.4%;">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datepicker datepicker-from text-center" name="tgl_order_from" id="tgl_order_from" value="<?= @$cookie['search']['tgl_order_from'] ?>">
                        </div>
                      </div>
                      <div class="col-md-2 text-center-group">s.d</div>
                      <div class="col-md-5" style="padding-left: 5px; flex: 0 0 46.4%; max-width: 46.4%;">
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                          </div>
                          <input type="text" class="form-control datepicker datepicker-to text-center" name="tgl_order_to" id="tgl_order_to" value="<?= @$cookie['search']['tgl_order_to'] ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">No.RM/Nama Pasien</label>
                  <div class="col-lg-8 col-md-8">
                    <input type="text" class="form-control" name="no_rm_nm" id="no_rm_nm" value="<?= @$cookie['search']['no_rm_nm'] ?>">
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Lokasi Asal</label>
                  <div class="col-lg-8 col-md-8">
                    <select class="form-control chosen-select" name="lokasi_id" id="lokasi_id" onchange="$('#form-search').submit()">
                      <option value="">- Semua -</option>
                      <?php foreach ($lokasi as $r) : ?>
                        <?php if (in_array($r['lokasi_id'], $this->session->userdata('sess_lokasi'))) : ?>
                          <option value="<?= $r['lokasi_id'] ?>" <?= (@$cookie['search']['lokasi_id'] == $r['lokasi_id']) ? 'selected' : ''; ?>><?= $r['lokasi_nm'] ?></option>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Jenis Pasien</label>
                  <div class="col-lg-5 col-md-8">
                    <select class="form-control chosen-select" name="jenispasien_id" id="jenispasien_id" onchange="$('#form-search').submit()">
                      <option value="">- Semua -</option>
                      <?php foreach ($jenis_pasien as $r) : ?>
                        <option value="<?= $r['jenispasien_id'] ?>" <?= (@$cookie['search']['jenispasien_id'] == $r['jenispasien_id']) ? 'selected' : ''; ?>><?= $r['jenispasien_nm'] ?></option>
                      <?php endforeach; ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group row">
                  <label class="col-lg-4 col-md-4 col-form-label">Status Periksa</label>
                  <div class="col-lg-5 col-md-8">
                    <select class="form-control chosen-select" name="periksa_st_rad" id="periksa_st_rad" onchange="$('#form-search').submit()">
                      <option value="">- Semua -</option>
                      <option value="1" <?= (@$cookie['search']['periksa_st_rad'] == '1') ? 'selected' : ''; ?>>Sudah</option>
                      <option value="0" <?= (@$cookie['search']['periksa_st_rad'] == '0') ? 'selected' : ''; ?>>Belum</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-1">
                <?php if ($nav['_add']) : ?>
                  <a href="#" data-href="<?= site_url() . '/penunjang/radiologi/kunjungan/form_modal' ?>" modal-title="Tambah Data" modal-size="lg" class="btn btn-xs btn-danger modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Tambah Data"><i class="fas fa-plus-circle"></i> Tambah</a>
                <?php endif; ?>
              </div>
              <div class="col-md-4 row">
                <div class="col-md-1"></div>
                <div class="col-md-6">
                  <button type="submit" class="btn btn-xs btn-primary" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Cari Data"><i class="fas fa-search"></i> Cari</button>
                  <a class="btn btn-xs btn-default" href="<?= site_url() . '/app/reset_redirect/' . $nav['nav_id'] . '.kunjungan/' . str_replace('/', '-', 'penunjang/radiologi/kunjungan') ?>" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-default" title="" data-original-title="Reset"><i class="fas fa-sync-alt"></i> Reset</a>
                </div>
              </div>
            </div>
          </form>
          <div class="row mt-4 pagination-info">
            <div class="col-md-6">
              <form id="form-paging" action="<?= site_url() . '/app/per_page/' . $nav['nav_id'] ?>" method="post">
                <label><i class="mdi mdi-bookmark"></i> Tampilkan</label>
                <select name="per_page" class="select-pagination" onchange="$('#form-paging').submit()">
                  <option value="10" <?php if ($cookie['per_page'] == 10) {
                                        echo 'selected';
                                      } ?>>10</option>
                  <option value="50" <?php if ($cookie['per_page'] == 50) {
                                        echo 'selected';
                                      } ?>>50</option>
                  <option value="100" <?php if ($cookie['per_page'] == 100) {
                                        echo 'selected';
                                      } ?>>100</option>
                </select>
                <label>data per halaman.</label>
              </form>
            </div>
            <div class="col-md-6 text-right">
              <div class="pt-1"><?= @$pagination_info ?></div>
            </div>
          </div><!-- /.row -->
          <div class="row">
            <div class="col-md-12">
              <div class="table-responsive">
                <form id="form-multiple" action="<?= site_url() . '/' . $nav['nav_url'] . '/multiple/enable' ?>" method="post">
                  <table class="table table-hover table-striped table-bordered table-fixed">
                    <thead>
                      <tr>
                        <th class="text-center text-middle" width="36">No</th>
                        <th class="text-center text-middle" width="80">Aksi</th>
                        <th class="text-center text-middle" width="100">Kode Pemeriksaan</th>
                        <th class="text-center text-middle" width="110">Tgl.Order</th>
                        <th class="text-center text-middle" width="100">No.Reg Pasien</th>
                        <th class="text-center text-middle" width="100">No.RM</th>
                        <th class="text-center text-middle"><?= table_sort($nav['nav_id'], 'Nama Pasien', 'pasien_nm', $cookie['order']) ?></th>
                        <th class="text-center text-middle">Alamat</th>
                        <th class="text-center text-middle" width="30">JK</th>
                        <th class="text-center text-middle" width="160">Jenis Pasien & Lokasi</th>
                        <th class="text-center text-middle" width="70">Periksa</th>
                      </tr>
                    </thead>
                    <?php if (@$main == null) : ?>
                      <tbody>
                        <tr>
                          <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                        </tr>
                      </tbody>
                    <?php else : ?>
                      <tbody>
                        <?php $i = 1;
                        foreach ($main as $row) : ?>
                          <tr>
                            <td class="text-center" width="36"><?= $cookie['cur_page'] + ($i++) ?></td>
                            <td class="text-center" width="80">
                              <?php if ($nav['_update'] || $nav['_delete']) : ?>
                                <?php if ($nav['_update']) : ?>
                                  <a href="<?= site_url() . '/' . $nav['nav_url'] . '/periksa/' . $row['reg_id'] . '/' . $row['pemeriksaan_id'] ?>#hasil-pemeriksaan" modal-title="Periksa" modal-size="md" class="btn btn-primary btn-table" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="" data-original-title="Periksa"><i class="fas fa-stethoscope"></i> Periksa</a>
                                <?php endif; ?>
                              <?php endif; ?>
                            </td>
                            <td class="text-center" width="100"><?= $row['pemeriksaan_id'] ?></td>
                            <td class="text-center" width="110"><?= to_date($row['tgl_order'], '', 'full_date') ?></td>
                            <td class="text-center" width="100"><?= $row['reg_id'] ?></td>
                            <td class="text-center" width="100"><?= $row['pasien_id'] ?></td>
                            <td class="text-left">
                              <?= $row['pasien_nm'] . ', ' . $row['sebutan_cd'] ?> <br>
                              <?= $row['umur_thn'] ?> th <?= $row['umur_bln'] ?> bl <?= $row['umur_hr'] ?> hr
                            </td>
                            <td class="text-left">
                              <?= ($row['alamat'] != '') ? $row['alamat'] . ', ' : '' ?>
                              <?= ($row['kelurahan'] != '') ? ucwords(strtolower($row['kelurahan'])) . ', ' : '' ?>
                              <?= ($row['kecamatan'] != '') ? ucwords(strtolower($row['kecamatan'])) . ', ' : '' ?>
                              <?= ($row['kabupaten'] != '') ? ucwords(strtolower($row['kabupaten'])) . ', ' : '' ?>
                              <?= ($row['provinsi'] != '') ? ucwords(strtolower($row['provinsi'])) : '' ?>
                            </td>
                            <td class="text-center" width="30"><?= $row['sex_cd'] ?></td>
                            <td class="text-center" width="160">
                              <?= $row['jenispasien_nm'] ?> <br>
                              <?= ($row['jenispasien_id'] == '02') ? $row['no_kartu'] . '<br>' : '' ?>
                              <?= $row['lokasi_nm'] ?>
                            </td>
                            <td class="text-center" width="70">
                              <li class="fa <?= ($row['periksa_st_rad'] == '1' ? 'fa-check-circle text-success' : 'fa-minus-circle text-danger blink') ?>"></li>
                            </td>
                          </tr>
                        <?php endforeach; ?>
                      </tbody>
                    <?php endif; ?>
                  </table>
                </form>
              </div>
            </div>
          </div><!-- /.row -->
          <div class="row mt-2">
            <div class="col-6 text-middle pt-1">
            </div>
            <div class="col-6 text-right">
              <?php echo $this->pagination->create_links(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>