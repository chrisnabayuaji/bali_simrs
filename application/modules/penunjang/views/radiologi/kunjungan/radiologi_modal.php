<form id="radiologi_form" action="<?= site_url() ?>/penunjang/radiologi/kunjungan/tambah_item" method="post" autocomplete="off">
  <div class="row">
    <input type="hidden" name="pemeriksaan_id" id="pemeriksaan_id" value="<?= @$mainrad['pemeriksaan_id'] ?>">
    <input type="hidden" name="reg_id" id="reg_id" value="<?= @$reg['reg_id'] ?>">
    <input type="hidden" name="src_reg_id" id="src_reg_id" value="<?= @$reg['reg_id'] ?>">
    <input type="hidden" name="pasien_id" id="pasien_id" value="<?= @$reg['pasien_id'] ?>">
    <input type="hidden" name="kelas_id" id="kelas_id" value="<?= @$reg['kelas_id'] ?>">
    <div class="col-md-12">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Item Radiologi <span class="text-danger">*</span></label>
        <div class="col-lg-7 col-md-7">
          <select class="form-control chosen-select" name="itemrad_id">
            <?php foreach ($itemrad as $r) : ?>
              <option value="<?= $r['itemrad_id'] ?>"><?= $r['itemrad_nm'] ?></option>
            <?php endforeach; ?>
          </select>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="col-4 offset-md-8 ">
    <div class="float-right">
      <button id="radiologi_cancel" type="button" class="btn btn-xs btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Batal</button>
      <button id="radiologi_action" type="submit" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
    </div>
  </div>
</form>
<script>
  var radiologiTable;
  $(document).ready(function() {
    $('.datetimepicker').daterangepicker({
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY HH:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })

    if ($(".chosen-select").length) {
      $(".chosen-select").select2();
      // addClass
      $(".filter-data").addClass("filter-data-chosen-select");
    }
    if ($(".chosen-select-filter").length) {
      $(".chosen-select-filter").select2();
      //
      $(".select2-container").css("margin-bottom", "-1px");
    }

    $('.select2-container').css('width', '100%');

    //FORM
    var radiologi_form = $("#radiologi_form").validate({
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container"));
        } else if ($(element).hasClass('chosen-select')) {
          error.insertAfter(element.next(".select2-container"));
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $("#radiologi_action").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#radiologi_action").attr("disabled", "disabled");
        $("#radiologi_cancel").attr("disabled", "disabled");
        form.submit();
      }
    });


  })
</script>