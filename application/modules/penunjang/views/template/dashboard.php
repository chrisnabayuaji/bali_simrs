<div class="content-wrapper mw-100">
  <div class="d-flex justify-content-center col-title-dashboard">
    <p class="ml-md-3 font-weight-semibold mb-0 mt-1 text-primary text-center">SELAMAT DATANG DI <?=strtoupper($module_nav['nav_nm'])?></p>
  </div>
  <div class="row d-flex justify-content-center">
    <div class="col-md-6 col-xl-6 pb-2">
      <form>
        <div class="form-group">
          <select name="ses_user_st" id="menu_module" class="select-search-menu-module w-100" data-text="<?=$module_nav['nav_nm']?>">
            <option value="">- Pilih Menu -</option>
            <?php foreach ($group_nav as $group): ?>
              <optgroup label="<?=$group['nav_nm']?>"></optgroup>
              <?php foreach($group['item_nav'] as $inav):?>
                <option value="<?=site_url($inav['nav_url'])?>">&nbsp;&nbsp;&nbsp; <?=$inav['nav_nm']?></option>
              <?php endforeach; ?>
            <?php endforeach; ?>
          </select>
        </div>
      </form>
    </div>
    <div class="col-md-12 col-xl-12 grid-margin stretch-card">
      <div class="card card-shadow">
          <ul class="nav nav-tabs" role="tablist">
            <?php foreach ($group_nav as $group): ?>
              <?php if (count($group['item_nav']) !=0): ?>
                <li class="nav-item">
                  <a class="nav-link <?php if($group['no'] == '1') echo "active" ?>" id="item-tab-<?=$group['no']?>" data-toggle="tab" href="#tab-<?=$group['no']?>" role="tab" aria-controls="tab-<?=$group['no']?>" aria-selected="true"> <i class="<?=$group['nav_font_icon']?>"></i> <?=$group['nav_nm']?></a>
                </li>
              <?php else: ?>
                <li class="nav-item">
                  <a class="nav-link" href="<?=site_url($group['nav_url'])?>"> <i class="<?=$group['nav_font_icon']?>"></i> <?=$group['nav_nm']?></a>
                </li>
              <?php endif; ?>
            <?php endforeach; ?>
          </ul>
          <div class="tab-content">
            <?php foreach ($group_nav as $group): ?>
            <div class="tab-pane fade <?php if($group['no'] == '1') echo "show active" ?>" id="tab-<?=$group['no']?>" role="tabpanel" aria-labelledby="item-tab-<?=$group['no']?>">
              <div class="tab-pane-body d-flex justify-content-center">
                <?php foreach($group['item_nav'] as $inav):?>
                <div class="col-menu-icon">
                  <div onclick="window.location.href='<?=site_url($inav['nav_url'])?>'" class="card card-menu-icon card-shadow">
                    <div class="card-body text-center p-1">
                      <?php if ($inav['nav_icon'] !=''): ?>
                        <img class="img-menu-icon lazy-load" data-src="<?=base_url()?>assets/images/module/<?=$inav['nav_icon']?>">
                      <?php else: ?>
                        <img class="img-menu-icon lazy-load" data-src="<?=base_url()?>assets/images/icon/no-image.png">
                      <?php endif; ?>
                      <div class="title-menu-icon"><?=$inav['nav_nm']?></div>
                    </div>
                  </div>
                </div>
                <?php endforeach; ?>
              </div>
            </div>
            <?php endforeach; ?>
          </div>
      </div>
    </div>
  </div>
</div>