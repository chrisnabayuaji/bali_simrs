<div class="table-responsive">
  <table id="petugas_table_<?=$form_name?>" class="table table-hover table-bordered table-striped table-sm w-100">
    <thead>
      <tr>
        <th class="text-center" width="20">No.</th>
        <th class="text-center" width="100">Kode</th>
        <th class="text-center">Nama Pegawai</th>
        <th class="text-center">NIP</th>
        <th class="text-center">Jenis Pegawai</th>
        <th class="text-center" width="60">Aksi</th>
      </tr>
    </thead>
  </table>
</div>

<script type="text/javascript">
  var petugasTable;
  $(document).ready(function () {
    //datatables
    petugasTable = $('#petugas_table_<?=$form_name?>').DataTable({
      'retrieve': true,
      "autoWidth" : false,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
      "ajax": {
          "url": "<?php echo site_url().'/'.$nav['nav_url'].'/ajax_tindakan/search_petugas_data'?>",
          "type": "POST",
          "data" : {
            'form_name' : '<?=$form_name?>'
          }
      },      
      "columnDefs": [
        {"targets": 0, "className": 'text-center', "orderable" : false},
        {"targets": 1, "className": 'text-center', "orderable" : false},
        {"targets": 2, "className": 'text-left', "orderable" : false},
        {"targets": 3, "className": 'text-left', "orderable" : false},
        {"targets": 4, "className": 'text-left', "orderable" : false},
        {"targets": 5, "className": 'text-center', "orderable" : false},
      ],
    });
    petugasTable.columns.adjust().draw();
  })
</script>