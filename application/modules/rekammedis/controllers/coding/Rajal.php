<?php 
if (!defined('BASEPATH')) exit ('No direct script access allowed');

class Rajal extends MY_Controller{

	var $nav_id = '08.01.01', $nav, $cookie;
	
	public function __construct() {
		parent::__construct();
		$this->load->model(array(
			'm_rajal',
			'master/m_lokasi',
			'master/m_jenis_pasien',
			'm_dt_diagnosis',
			'm_dt_tarifkelas',
			'm_dt_petugas',
			'm_dt_obat',
			'm_dt_obat_master',
			'm_dt_bhp',
			'm_dt_alkes',
			'm_dt_laboratorium',
			'm_dt_radiologi',
			'master/m_rsrujukan'
		));

		$this->nav = $this->m_app->_get_nav($this->nav_id);

		//cookie
    $this->cookie = get_cookie_nav($this->nav_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('tgl_registrasi_from' => '', 'tgl_registrasi_to' => '', 'no_rm_nm' => '', 'lokasi_id' => '', 'jenispasien_id' => '', 'dx_st' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'tgl_registrasi','type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
		if ($this->cookie['cur_page'] == null) $this->cookie['cur_page'] = 0;
	}
	
	public function index() {	
		$this->authorize($this->nav, '_view');
		//cookie
		$this->cookie['cur_page'] = $this->uri->segment(5, 0);
    $this->cookie['total_rows'] = $this->m_rajal->all_rows($this->cookie);
    set_cookie_nav($this->nav_id, $this->cookie);
    //main data
    $data['nav'] = $this->nav;
    $data['cookie'] = $this->cookie;
    $data['main'] = $this->m_rajal->list_data($this->cookie);
		$data['pagination_info'] = pagination_info(count($data['main']), $this->cookie);
		$data['lokasi'] = $this->m_lokasi->by_field('jenisreg_st', 1, 'result');
		$data['jenis_pasien'] = $this->m_jenis_pasien->all_data();
    //set pagination
    set_pagination($this->nav, $this->cookie);
		//render
		create_log('_view', $this->nav_id);
		$this->render('rekammedis/coding/rajal/index',$data);
	}

	public function form($id=null) {
    $this->authorize($this->nav, ($id != '' ) ? '_update' : '_add');
		
		if($id == null) {
			$data['main'] = array();
			$data['alergi'] = array();
		} else {
			$data['main'] = $this->m_rajal->get_data($id);
			$data['alergi'] = $this->m_rajal->get_alergi($data['main']['pasien_id']);
			$this->m_rajal->update_periksa_st($id, $data['main']['periksa_st']);
		}
		$data['id'] = $id;
		$data['nav'] = $this->nav;
		$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/save/'.$id;
			
		$this->render('rekammedis/coding/rajal/form',$data);
	}

	function ajax($type=null, $id=null) {
		if ($type == 'view_tab_menu') {
			$view_name = $this->input->post('view_name');
			$id = $this->input->post('id');
			$data['nav'] = $this->nav;
			$data['reg'] = $this->m_rajal->get_data($id);

			// Tindak Lanjut
			if ($view_name == 'tindak_lanjut') {
				$data['lokasi_jenisreg_1'] = $this->m_lokasi->by_field('jenisreg_st', 1, 'result');
				$data['lokasi_jenisreg_2'] = $this->m_lokasi->by_field('jenisreg_st', 2, 'result');
				$data['rujukan'] = $this->m_rsrujukan->all_data();
			}

			echo json_encode(array(
        'html' => $this->load->view('rekammedis/coding/rajal/'.$view_name, $data, true)
      ));
		}
	}

	//CATATAN MEDIS
	public function ajax_catatan_medis($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_rajal->catatan_medis_save();
		}elseif ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->catatan_medis_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/catatan_medis_data', $data, true)
			));
		}elseif ($type == 'get_data') {
			$catatanmedis_id = $this->input->post('catatanmedis_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');

			$main = $this->m_rajal->catatan_medis_get($catatanmedis_id, $reg_id, $pasien_id);
			echo json_encode(array(
				'main' => $main
			));
		}elseif ($type == 'delete_data') {
			$catatanmedis_id = $this->input->post('catatanmedis_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_rajal->catatan_medis_delete($catatanmedis_id, $reg_id, $pasien_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->catatan_medis_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/catatan_medis_data', $data, true)
			));
		}
	}

	// Pemeriksaan fisik
	public function ajax_pemeriksaan_fisik($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_rajal->pemeriksaan_fisik_save();
		}elseif($type == 'data'){
			$res = $this->m_rajal->pemeriksaan_fisik_data();
			echo json_encode($res);
		}
	}

	// Diagnosis
	public function ajax_diagnosis($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_rajal->diagnosis_save();
		}elseif($type == 'autocomplete'){
			$penyakit_nm = $this->input->get('penyakit_nm');
			$res = $this->m_rajal->penyakit_autocomplete($penyakit_nm);
			echo json_encode($res);
		}elseif ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->diagnosis_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/diagnosis_data', $data, true)
			));
		}elseif ($type == 'get_data') {
			$diagnosis_id = $this->input->post('diagnosis_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');

			$main = $this->m_rajal->diagnosis_get($diagnosis_id, $reg_id, $pasien_id);
			echo json_encode(array(
				'main' => $main
			));
		}elseif ($type == 'delete_data') {
			$diagnosis_id = $this->input->post('diagnosis_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_rajal->diagnosis_delete($diagnosis_id, $reg_id, $pasien_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->diagnosis_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/diagnosis_data', $data, true)
			));
		}elseif ($type == 'search_diagnosis') {
			$data['nav'] = $this->nav;
			
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/search_diagnosis', $data, true)
			));
		}elseif ($type == 'search_data') {
			$list = $this->m_dt_diagnosis->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['penyakit_id'];
				$row[] = $field['icdx'];
				$row[] = $field['penyakit_nm'];
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="diagnosis_fill('."'".$field['penyakit_id']."'".')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_diagnosis->count_all(),
				"recordsFiltered" => $this->m_dt_diagnosis->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}else if($type == 'diagnosis_fill'){
			$data = $this->input->post();
			$res = $this->m_rajal->diagnosis_row($data['penyakit_id']);
			echo json_encode($res);
		}
	}

	// Tindakan
	public function ajax_tindakan($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_rajal->tindakan_save();
		}elseif($type == 'tarifkelas_autocomplete'){
			$tarif_nm = $this->input->get('tarif_nm');
			$res = $this->m_rajal->tarifkelas_autocomplete($tarif_nm);
			echo json_encode($res);
		}elseif($type == 'petugas_autocomplete'){
			$petugas_nm = $this->input->get('petugas_nm');
			$res = $this->m_rajal->petugas_autocomplete($petugas_nm);
			echo json_encode($res);
		}elseif ($type == 'data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->tindakan_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/tindakan_data', $data, true)
			));
		}elseif ($type == 'get_data') {
			$tindakan_id = $this->input->post('tindakan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');

			$main = $this->m_rajal->tindakan_get($tindakan_id, $reg_id, $pasien_id);
			echo json_encode(array(
				'main' => $main
			));
		}elseif ($type == 'delete_data') {
			$tindakan_id = $this->input->post('tindakan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_rajal->tindakan_delete($tindakan_id, $reg_id, $pasien_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->tindakan_data($pasien_id, $reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/tindakan_data', $data, true)
			));
		}elseif ($type == 'search_tarifkelas') {
			$data['nav'] = $this->nav;
			$data['kelas_id'] = $id;
			
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/search_tarifkelas', $data, true)
			));
		}elseif ($type == 'search_tarifkelas_data') {
			$list = $this->m_dt_tarifkelas->get_datatables($id);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['tarifkelas_id'];
				$row[] = $field['tarif_nm'];
				$row[] = $field['kelas_nm'];
				$row[] = num_id($field['nominal']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="tarifkelas_fill('."'".$field['tarifkelas_id']."'".')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_tarifkelas->count_all($id),
				"recordsFiltered" => $this->m_dt_tarifkelas->count_filtered($id),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}else if($type == 'tarifkelas_fill'){
			$data = $this->input->post();
			$res = $this->m_rajal->tarifkelas_row($data['tarifkelas_id']);
			echo json_encode($res);
		}elseif ($type == 'search_petugas') {
			$data['nav'] = $this->nav;
			$data['form_name'] = @$id;
			
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/search_petugas', $data, true)
			));
		}elseif ($type == 'search_petugas_data') {
			$list = $this->m_dt_petugas->get_datatables();
			$form_name = $this->input->post('form_name');
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['pegawai_id'];
				$row[] = $field['pegawai_nm'];
				$row[] = $field['pegawai_nip'];
				$row[] = get_parameter_value('jenispegawai_cd', $field['jenispegawai_cd']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="petugas_fill('."'".$field['pegawai_id']."'".','."'".$form_name."'".')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_petugas->count_all(),
				"recordsFiltered" => $this->m_dt_petugas->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}else if($type == 'petugas_fill'){
			$data = $this->input->post();
			$res = $this->m_rajal->petugas_row($data['pegawai_id']);
			echo json_encode($res);
		}else if($type == 'add_petugas') {
			$tindakan_id = $this->input->get('tindakan_id');
			$data['nav'] = $this->nav;
			
			$html = '';
			if ($tindakan_id !='') {
				$petugas_no = $this->input->get('petugas_no');
				$get_tindakan = $this->m_rajal->get_tindakan($tindakan_id);
				$data['tindakan_id'] = $tindakan_id;
				$data['petugas_no_post'] = $petugas_no;
				$data['type'] = 'edit';

				if ($get_tindakan['petugas_id'] !='') {
					$data['petugas_no'] = ''; 
					$data['form_name'] = 'petugas_id';
					$data['pegawai_id'] = $get_tindakan['petugas_id'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_1'];

					$html.= $this->load->view('rekammedis/coding/rajal/add_petugas', $data, true);
				}
				if ($get_tindakan['petugas_id_2'] !='') {
					$data['petugas_no'] = '2'; 
					$data['form_name'] = 'petugas_id_2';
					$data['pegawai_id'] = $get_tindakan['petugas_id_2'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_2'];

					$html.= $this->load->view('rekammedis/coding/rajal/add_petugas', $data, true);
				}
				if ($get_tindakan['petugas_id_3'] !='') {
					$data['petugas_no'] = '3'; 
					$data['form_name'] = 'petugas_id_3';
					$data['pegawai_id'] = $get_tindakan['petugas_id_3'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_3'];

					$html.= $this->load->view('rekammedis/coding/rajal/add_petugas', $data, true);
				}
				if ($get_tindakan['petugas_id_4'] !='') {
					$data['petugas_no'] = '4'; 
					$data['form_name'] = 'petugas_id_4';
					$data['pegawai_id'] = $get_tindakan['petugas_id_4'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_4'];

					$html.= $this->load->view('rekammedis/coding/rajal/add_petugas', $data, true);
				}
				if ($get_tindakan['petugas_id_5'] !='') {
					$data['petugas_no'] = '5'; 
					$data['form_name'] = 'petugas_id_5';
					$data['pegawai_id'] = $get_tindakan['petugas_id_5'];
					$data['pegawai_nm'] = $get_tindakan['pegawai_nm_5'];

					$html.= $this->load->view('rekammedis/coding/rajal/add_petugas', $data, true);
				}
			}else{
				$petugas_no = $this->input->get('petugas_no')+1;
				$data['petugas_no'] = ($petugas_no == '1') ? '' : $petugas_no; 
				$data['form_name'] = ($petugas_no == '1') ? 'petugas_id' : 'petugas_id_'.$petugas_no;
				$data['petugas_no_post'] = $petugas_no;
				$data['type'] = 'add';

				$html.= $this->load->view('rekammedis/coding/rajal/add_petugas', $data, true);
			}
					
      echo json_encode(array(
      	'html' => $html,
      	'petugas_no' => $petugas_no,
      ));
		}elseif ($type == 'delete_petugas') {
			$tindakan_id = $this->input->post('tindakan_id');
			$form_name = $this->input->post('form_name');
			//
			$callback = 'true';
			//
			echo json_encode(array(
				'callback' => $callback
			));
		}
	}

	// Pemberian Obat
	public function ajax_pemberian_obat($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_rajal->pemberian_obat_save();
		}elseif($type == 'autocomplete'){
			$obat_nm = $this->input->get('obat_nm');
			$map_lokasi_depo = $this->input->get('map_lokasi_depo');
			$res = $this->m_rajal->obat_autocomplete($obat_nm, $map_lokasi_depo);
			echo json_encode($res);
		}elseif ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->pemberian_obat_data($reg_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/pemberian_obat_data', $data, true)
			));
		}elseif ($type == 'get_data') {
			$resep_id = $this->input->post('resep_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_rajal->pemberian_obat_get($resep_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		}elseif ($type == 'delete_data') {
			$resep_id = $this->input->post('resep_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$resepgroup_id = $this->input->post('resepgroup_id');

			$this->m_rajal->pemberian_obat_delete($resep_id, $reg_id, $resepgroup_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->pemberian_obat_data($reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/pemberian_obat_data', $data, true)
			));
		}elseif ($type == 'search_obat') {
			$data['nav'] = $this->nav;
			
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/search_obat', $data, true)
			));
		}elseif ($type == 'search_data') {
			$map_lokasi_depo = $this->input->post('map_lokasi_depo');
			$list = $this->m_dt_obat->get_datatables($map_lokasi_depo);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['no_batch'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = to_date($field['tgl_expired'],'-','date',' ');
				$row[] = num_id($field['stok_akhir']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="obat_fill('."'".$field['obat_id']."'".','."'".$field['stokdepo_id']."'".')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_obat->count_all($map_lokasi_depo),
				"recordsFiltered" => $this->m_dt_obat->count_filtered($map_lokasi_depo),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}elseif ($type == 'search_data_master') {
			$list = $this->m_dt_obat_master->get_datatables();
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="obat_fill_master('."'".$field['obat_id']."'".')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_obat_master->count_all(),
				"recordsFiltered" => $this->m_dt_obat_master->count_filtered(),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}else if($type == 'obat_fill'){
			$data = $this->input->post();
			$res = $this->m_rajal->obat_row($data);
			echo json_encode($res);
		}else if($type == 'obat_fill_master'){
			$data = $this->input->post();
			$res = $this->m_rajal->obat_row_master($data['obat_id']);
			echo json_encode($res);
		}elseif($type == 'get_no_resep'){
			$reg_id = $this->input->post('reg_id');
			$res = $this->m_rajal->get_no_resep($reg_id);
			echo json_encode($res);
		}
	}

	// BHP
	public function ajax_bhp($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_rajal->bhp_save();
		}elseif($type == 'autocomplete'){
			$obat_nm = $this->input->get('obat_nm');
			$map_lokasi_depo = $this->input->get('map_lokasi_depo');
			$res = $this->m_rajal->bhp_autocomplete($obat_nm, $map_lokasi_depo);
			echo json_encode($res);
		}elseif ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->bhp_data($reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/bhp_data', $data, true)
			));
		}elseif ($type == 'get_data') {
			$bhp_id = $this->input->post('bhp_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_rajal->bhp_get($bhp_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		}elseif ($type == 'delete_data') {
			$bhp_id = $this->input->post('bhp_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_rajal->bhp_delete($bhp_id, $reg_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->bhp_data($reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/bhp_data', $data, true)
			));
		}elseif ($type == 'search_bhp') {
			$data['nav'] = $this->nav;
			
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/search_bhp', $data, true)
			));
		}elseif ($type == 'search_data') {
			$map_lokasi_depo = $this->input->post('map_lokasi_depo');
			$list = $this->m_dt_bhp->get_datatables($map_lokasi_depo);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['no_batch'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = to_date($field['tgl_expired'],'-','date',' ');
				$row[] = num_id($field['stok_akhir']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="bhp_fill('."'".$field['obat_id']."'".','."'".$field['stokdepo_id']."'".')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_bhp->count_all($map_lokasi_depo),
				"recordsFiltered" => $this->m_dt_bhp->count_filtered($map_lokasi_depo),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}else if($type == 'bhp_fill'){
			$data = $this->input->post();
			$res = $this->m_rajal->bhp_row($data);
			echo json_encode($res);
		}
	}

	// ALKES
	public function ajax_alkes($type = null, $id = null)
	{
		if ($type == 'save') {
			$this->m_rajal->alkes_save();
		}elseif($type == 'autocomplete'){
			$obat_nm = $this->input->get('obat_nm');
			$map_lokasi_depo = $this->input->get('map_lokasi_depo');
			$res = $this->m_rajal->alkes_autocomplete($obat_nm, $map_lokasi_depo);
			echo json_encode($res);
		}elseif ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->alkes_data($reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/alkes_data', $data, true)
			));
		}elseif ($type == 'get_data') {
			$alkes_id = $this->input->post('alkes_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_rajal->alkes_get($alkes_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		}elseif ($type == 'delete_data') {
			$alkes_id = $this->input->post('alkes_id');
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_rajal->alkes_delete($alkes_id, $reg_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->alkes_data($reg_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/alkes_data', $data, true)
			));
		}elseif ($type == 'search_alkes') {
			$data['nav'] = $this->nav;
			
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/search_alkes', $data, true)
			));
		}elseif ($type == 'search_data') {
			$map_lokasi_depo = $this->input->post('map_lokasi_depo');
			$list = $this->m_dt_alkes->get_datatables($map_lokasi_depo);
			$data = array();
			$no = $_POST['start'];
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				$row[] = $field['obat_id'];
				$row[] = $field['no_batch'];
				$row[] = $field['obat_nm'];
				$row[] = get_parameter_value('satuan_cd', $field['satuan_cd']);
				$row[] = get_parameter_value('jenisbarang_cd', $field['jenisbarang_cd']);
				$row[] = to_date($field['tgl_expired'],'-','date',' ');
				$row[] = num_id($field['stok_akhir']);
				$row[] = '<button type="button" class="btn btn-xs btn-primary pt-1 pb-1" onclick="alkes_fill('."'".$field['obat_id']."'".','."'".$field['stokdepo_id']."'".')">Pilih >></button>';

				$data[] = $row;
			}

			$output = array(
				"draw" => $_POST['draw'],
				"recordsTotal" => $this->m_dt_alkes->count_all($map_lokasi_depo),
				"recordsFiltered" => $this->m_dt_alkes->count_filtered($map_lokasi_depo),
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}else if($type == 'alkes_fill'){
			$data = $this->input->post();
			$res = $this->m_rajal->alkes_row($data);
			echo json_encode($res);
		}
	}

	//  Penunjang
	public function ajax_penunjang_laboratorium($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'modal') {
			$data['reg'] = $this->m_rajal->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainlab'] = null;
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_laboratorium/save/';
			}else{
				$data['mainlab'] = $this->m_rajal->laboratorium_get($id);
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_laboratorium/save/'.$id;
			}
				
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/laboratorium_modal', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['reg'] = $this->m_rajal->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainlab'] = null;
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_laboratorium/save/';
			}else{
				$data['mainlab'] = $this->m_rajal->laboratorium_get($id);
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_laboratorium/save/'.$id;
			}
				
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/laboratorium_detail', $data, true)
			));
		}

		if ($type == 'laboratorium_pemeriksaan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->laboratorium_pemeriksaan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/laboratorium_pemeriksaan_data', $data, true)
			));
		}

		if ($type == 'laboratorium_tarif_tindakan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->laboratorium_tarif_tindakan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/laboratorium_tarif_tindakan_data', $data, true)
			));
		}

		if ($type == 'laboratorium_bhp_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->laboratorium_bhp_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/laboratorium_bhp_data', $data, true)
			));
		}

		if ($type == 'laboratorium_alkes_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->laboratorium_alkes_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/laboratorium_alkes_data', $data, true)
			));
		}

		if ($type == 'search_data') {
			$list = $this->m_dt_laboratorium->get_datatables($id);
			$no = 0;
			$data = array();
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				if ($field['parent_id'] != '') {$dash = '--';}else {$dash = '';};
				$row[] = $field['itemlab_id'];
				$row[] = $dash.$field['itemlab_nm'];
				if ($field['parent_id'] != '') {
					if (@$field['pemeriksaanrinc_id'] != null) {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemlab_id'].'" checked="true">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="'.$field['itemlab_id'].'" checked="true">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>';
					}else{
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemlab_id'].'">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="'.$field['itemlab_id'].'">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>';
					}
				}else {
					$row[] = '';
				}

				$data[] = $row;
			}

			$output = array(
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'save') {
			$this->m_rajal->laboratorium_save(@$reg_id, @$id);
		}

		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->laboratorium_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/laboratorium_data', $data, true)
			));
		}

		if ($type == 'delete_data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_rajal->laboratorium_delete($pemeriksaan_id, $reg_id, $pasien_id, $lokasi_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->laboratorium_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/laboratorium_data', $data, true)
			));
		}

	}

	public function ajax_penunjang_radiologi($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'modal') {
			$data['reg'] = $this->m_rajal->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainrad'] = null;
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_radiologi/save/';
			}else{
				$data['mainrad'] = $this->m_rajal->radiologi_get($id);
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_radiologi/save/'.$id;
			}
				
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/radiologi_modal', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['reg'] = $this->m_rajal->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainrad'] = null;
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_radiologi/save/';
			}else{
				$data['mainrad'] = $this->m_rajal->radiologi_get($id);
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_radiologi/save/'.$id;
			}
				
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/radiologi_detail', $data, true)
			));
		}

		if ($type == 'radiologi_pemeriksaan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->radiologi_pemeriksaan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/radiologi_pemeriksaan_data', $data, true)
			));
		}

		if ($type == 'radiologi_tarif_tindakan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->radiologi_tarif_tindakan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/radiologi_tarif_tindakan_data', $data, true)
			));
		}

		if ($type == 'radiologi_bhp_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->radiologi_bhp_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/radiologi_bhp_data', $data, true)
			));
		}

		if ($type == 'radiologi_alkes_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->radiologi_alkes_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/radiologi_alkes_data', $data, true)
			));
		}	

		if ($type == 'search_data') {
			$list = $this->m_dt_radiologi->get_datatables($id);
			$no = 0;
			$data = array();
			foreach ($list as $field) {
				$no++;
				$row = array();
				$row[] = $no;
				if ($field['parent_id'] != '') {$dash = '--';}else {$dash = '';};
				$row[] = $field['itemrad_id'];
				$row[] = $dash.$field['itemrad_nm'];
				if ($field['parent_id'] != '') {
					if (@$field['pemeriksaanrinc_id'] != null) {
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemrad_id'].'" checked="true">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="'.$field['itemrad_id'].'" checked="true">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>';
					}else{
						// $row[] = '<input type="checkbox" name="checkitem[]" value="'.$field['itemrad_id'].'">';
						$row[] = '<div class="d-flex justify-content-center">
												<div class="form-check form-check-primary text-center" style="margin-left: -10px !important;">
	                        <label class="form-check-label">
	                          <input type="checkbox" class="form-check-input" name="checkitem[]" value="'.$field['itemrad_id'].'">
	                        <i class="input-helper"></i></label>
                      	</div>
                      </div>';
					}
				}else {
					$row[] = '';
				}

				$data[] = $row;
			}

			$output = array(
				"data" => $data,
			);
			//output dalam format JSON
			echo json_encode($output);
		}

		if ($type == 'save') {
			$this->m_rajal->radiologi_save(@$reg_id, @$id);
		}

		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->radiologi_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/radiologi_data', $data, true)
			));
		}

		if ($type == 'delete_data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_rajal->radiologi_delete($pemeriksaan_id, $reg_id, $pasien_id, $lokasi_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->radiologi_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/radiologi_data', $data, true)
			));
		}

	}

	public function ajax_penunjang_bedah_sentral($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'modal') {
			$data['reg'] = $this->m_rajal->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainbs'] = null;
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_bedah_sentral/save/';
			}else{
				$data['mainbs'] = $this->m_rajal->bedah_sentral_get($id);
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_bedah_sentral/save/'.$id;
			}
				
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/bedah_sentral_modal', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['reg'] = $this->m_rajal->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainbs'] = null;
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_bedah_sentral/save/';
			}else{
				$data['mainbs'] = $this->m_rajal->bedah_sentral_get($id);
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_bedah_sentral/save/'.$id;
			}
				
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/bedah_sentral_detail', $data, true)
			));
		}

		if ($type == 'bedah_sentral_tindakan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->bedah_sentral_tindakan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/bedah_sentral_tindakan_data', $data, true)
			));
		}

		if ($type == 'bedah_sentral_bhp_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->bedah_sentral_bhp_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/bedah_sentral_bhp_data', $data, true)
			));
		}

		if ($type == 'bedah_sentral_alkes_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->bedah_sentral_alkes_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/bedah_sentral_alkes_data', $data, true)
			));
		}

		if ($type == 'save') {
			$this->m_rajal->bedah_sentral_save(@$reg_id, @$id);
		}

		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->bedah_sentral_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/bedah_sentral_data', $data, true)
			));
		}

		if ($type == 'delete_data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_rajal->bedah_sentral_delete($pemeriksaan_id, $reg_id, $pasien_id, $lokasi_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->bedah_sentral_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/bedah_sentral_data', $data, true)
			));
		}

	}

	public function ajax_penunjang_vk($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'modal') {
			$data['reg'] = $this->m_rajal->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainvk'] = null;
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_vk/save/';
			}else{
				$data['mainvk'] = $this->m_rajal->vk_get($id);
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_vk/save/'.$id;
			}
				
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/vk_modal', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['reg'] = $this->m_rajal->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainvk'] = null;
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_vk/save/';
			}else{
				$data['mainvk'] = $this->m_rajal->vk_get($id);
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_vk/save/'.$id;
			}
				
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/vk_detail', $data, true)
			));
		}

		if ($type == 'vk_tindakan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->vk_tindakan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/vk_tindakan_data', $data, true)
			));
		}

		if ($type == 'vk_bhp_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->vk_bhp_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/vk_bhp_data', $data, true)
			));
		}

		if ($type == 'vk_alkes_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->vk_alkes_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/vk_alkes_data', $data, true)
			));
		}

		if ($type == 'save') {
			$this->m_rajal->vk_save(@$reg_id, @$id);
		}

		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->vk_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/vk_data', $data, true)
			));
		}

		if ($type == 'delete_data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_rajal->vk_delete($pemeriksaan_id, $reg_id, $pasien_id, $lokasi_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->vk_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/vk_data', $data, true)
			));
		}

	}

	public function ajax_penunjang_icu($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'modal') {
			$data['reg'] = $this->m_rajal->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainicu'] = null;
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_icu/save/';
			}else{
				$data['mainicu'] = $this->m_rajal->icu_get($id);
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_icu/save/'.$id;
			}
				
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/icu_modal', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['reg'] = $this->m_rajal->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainicu'] = null;
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_icu/save/';
			}else{
				$data['mainicu'] = $this->m_rajal->icu_get($id);
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_icu/save/'.$id;
			}
				
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/icu_detail', $data, true)
			));
		}

		if ($type == 'icu_tindakan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->icu_tindakan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/icu_tindakan_data', $data, true)
			));
		}

		if ($type == 'icu_bhp_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->icu_bhp_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/icu_bhp_data', $data, true)
			));
		}

		if ($type == 'icu_alkes_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->icu_alkes_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/icu_alkes_data', $data, true)
			));
		}

		if ($type == 'save') {
			$this->m_rajal->icu_save(@$reg_id, @$id);
		}

		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->icu_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/icu_data', $data, true)
			));
		}

		if ($type == 'delete_data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_rajal->icu_delete($pemeriksaan_id, $reg_id, $pasien_id, $lokasi_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->icu_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/icu_data', $data, true)
			));
		}

	}

	public function ajax_penunjang_hemodialisa($type = null, $reg_id = null, $id = null)
	{
		if ($type == 'modal') {
			$data['reg'] = $this->m_rajal->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainhemo'] = null;
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_hemodialisa/save/';
			}else{
				$data['mainhemo'] = $this->m_rajal->hemodialisa_get($id);
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_hemodialisa/save/'.$id;
			}
				
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/hemodialisa_modal', $data, true)
			));
		}

		if ($type == 'detail') {
			$data['reg'] = $this->m_rajal->get_data($reg_id);
			$data['pemeriksaan_id'] = $id;
			$data['reg_id'] = $reg_id;
			$data['nav'] = $this->nav;
			if ($id == null) {
				$data['mainhemo'] = null;
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_hemodialisa/save/';
			}else{
				$data['mainhemo'] = $this->m_rajal->hemodialisa_get($id);
				$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_penunjang_hemodialisa/save/'.$id;
			}
				
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/hemodialisa_detail', $data, true)
			));
		}

		if ($type == 'hemodialisa_tindakan_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->hemodialisa_tindakan_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/hemodialisa_tindakan_data', $data, true)
			));
		}

		if ($type == 'hemodialisa_bhp_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->hemodialisa_bhp_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/hemodialisa_bhp_data', $data, true)
			));
		}

		if ($type == 'hemodialisa_alkes_data') {
			$reg_id = $this->input->post('reg_id');
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->hemodialisa_alkes_data($reg_id, $pemeriksaan_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/hemodialisa_alkes_data', $data, true)
			));
		}

		if ($type == 'save') {
			$this->m_rajal->hemodialisa_save(@$reg_id, @$id);
		}

		if ($type == 'data') {
			$reg_id = $this->input->post('reg_id');
			$lokasi_id = $this->input->post('lokasi_id');
			$pasien_id = $this->input->post('pasien_id');
			
			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->hemodialisa_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/hemodialisa_data', $data, true)
			));
		}

		if ($type == 'delete_data') {
			$pemeriksaan_id = $this->input->post('pemeriksaan_id');
			$reg_id = $this->input->post('reg_id');
			$pasien_id = $this->input->post('pasien_id');
			$lokasi_id = $this->input->post('lokasi_id');

			$this->m_rajal->hemodialisa_delete($pemeriksaan_id, $reg_id, $pasien_id, $lokasi_id);

			$data['nav'] = $this->nav;
			$data['main'] = $this->m_rajal->hemodialisa_data($reg_id, $pasien_id, $lokasi_id);
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/hemodialisa_data', $data, true)
			));
		}

	}

	//  Tindak Lanjut
	public function ajax_tindak_lanjut($type = null, $id = null)
	{
		if ($type == 'form_rs_rujukan') {
			$data['nav'] = $this->nav;
			$data['form_action'] = site_url().'/'.$this->nav['nav_url'].'/ajax_tindakan/save/'.$id;
				
			echo json_encode(array(
				'html' => $this->load->view('rekammedis/coding/rajal/form_rs_rujukan', $data, true)
			));
		}else if ($type == 'cek_id') {
			$data = $this->input->post();
			$data['rsrujukan_id'] = $data['rsrujukan_kode'];
			$cek = $this->m_rsrujukan->get_data($data['rsrujukan_id']);
			if ($id == null) {
				if ($cek == null) {
					echo 'true';
				}else{
					echo 'false';
				}
			}else{
				if ($id != $data['rsrujukan_id'] && $cek != null) {
					echo 'false';
				}else{
					echo 'true';
				}
			}
		}else if ($type == 'save_rs_rujukan') {
			$data = $this->input->post();
			$this->m_rajal->save_rs_rujukan();

			echo json_encode(array(
				'main' => $data
			));
		}else if ($type == 'save_tindak_lanjut') {
			$data = $this->input->post();
			$this->m_rajal->save_tindak_lanjut();
		}elseif ($type == 'tindak_lanjut_data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_rajal->tindak_lanjut_data($pasien_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		}elseif ($type == 'reg_pasien_data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_rajal->reg_pasien_data_pulang_st($pasien_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		}else if ($type == 'save_status_pulang') {
			$data = $this->input->post();
			$this->m_rajal->save_status_pulang();
		}elseif ($type == 'status_pulang_data') {
			$pasien_id = $this->input->post('pasien_id');
			$reg_id = $this->input->post('reg_id');

			$main = $this->m_rajal->status_pulang_data($pasien_id, $reg_id);
			echo json_encode(array(
				'main' => $main
			));
		}
	}

	public function save_tindak_lanjut()
	{
		$this->m_rajal->save_tindak_lanjut();
		$this->m_rajal->save_status_pulang();
		$this->session->set_flashdata('flash_success', 'Data berhasil disimpan');
		redirect(site_url().'/'.$this->nav['nav_url']);
	}
}