<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_caller_antrian extends CI_Model {

	public function where($cookie)
	{
		$where = "WHERE a.is_deleted = 0";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND c.lokasi_nm LIKE '%".$this->db->escape_like_str($cookie['search']['term'])."%' ";
		}
		return $where;
	}

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
              a.lokasi_id, a.antrian_cd, c.lokasi_nm 
            FROM lkt_antrian_kuota a
            JOIN mst_lokasi c ON a.lokasi_id = c.lokasi_id
            $where
            ORDER BY "
              .$cookie['order']['field']." ".$cookie['order']['type'].
            " LIMIT ".$cookie['cur_page'].",".$cookie['per_page'];
    $query = $this->db->query($sql);
    $result = $query->result_array();

    foreach ($result as $key => $val) {
      $get_lkt_antrian = $this->get_lkt_antrian($val['lokasi_id']);
      $result[$key]['antrian_no'] = ($get_lkt_antrian['antrian_no'] !='') ? $get_lkt_antrian['antrian_no'] : '0';
      $result[$key]['call_antrian_no'] = ($get_lkt_antrian['call_antrian_no'] !='') ? $get_lkt_antrian['call_antrian_no'] : '0';
    }

    return $result;
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM lkt_antrian a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT 
              COUNT(a.lokasi_id) as total 
            FROM lkt_antrian_kuota a
            JOIN mst_lokasi c ON a.lokasi_id = c.lokasi_id
            $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function get_data($id) {
    $sql = "SELECT a.*,b.lokasi_nm FROM lkt_antrian a LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id WHERE tgl_antrian='".date('Y-m-d')."' AND a.lokasi_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function get_kuota($id) {
    $sql = "SELECT a.* FROM lkt_antrian_kuota a WHERE a.lokasi_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    $row['warna'] = ($row['warna'] !='') ? $row['warna'] : 'info';
    return $row;
  }

  public  function get_lkt_antrian($lokasi_id) {
    $sql = "SELECT a.* FROM lkt_antrian a WHERE a.tgl_antrian='".date('Y-m-d')."' AND a.lokasi_id=?";
    $query = $this->db->query($sql, array($lokasi_id));
    $row = $query->row_array();
    return $row;
  }

  public function call_antrian($data)
  {
    $raw = $this->get_data($data['lokasi_id']);
    $now = null;
    if ($raw['call_antrian_no'] < $raw['antrian_no']) {
      $this->db
        ->where('lokasi_id',$data['lokasi_id'])
        ->where('tgl_antrian', date('Y-m-d'))
        ->update('lkt_antrian', array(
          'call_antrian_no' => ++$raw['call_antrian_no'],
          'call_antrian_cd' => $raw['antrian_cd'],
          'loket' => $data['loket']
        ));
      $now = $this->get_data($data['lokasi_id']);
    }
    return $now;
  }

  public function display_antrian($data)
  {
    $get_kuota = $this->get_kuota($data['lokasi_id']);
    $data['warna'] = $get_kuota['warna'];
    $data['created_at'] = date('Y-m-d H:i:s').microtime();
    $data['created_by'] = @$this->session->userdata('sess_user_realname');
    $this->db->insert('dpy_antrian', $data);
  }
  
}