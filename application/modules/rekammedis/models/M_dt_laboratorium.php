<?php 

class M_dt_laboratorium extends CI_Model {
 
  function get_datatables($pemeriksaan_id = null)
  {
    if ($pemeriksaan_id != null) {
      $where = "WHERE b.pemeriksaan_id = '$pemeriksaan_id'";
      $query = $this->db->query(
        "SELECT 
          a.*,
          c.pemeriksaanrinc_id,
          c.tgl_hasil,
          c.hasil_lab,
          c.catatan_lab 
        FROM mst_item_lab a 
        LEFT JOIN (
          SELECT b.* FROM lab_pemeriksaan_rinc b 
          $where
        ) as c ON a.itemlab_id = c.itemlab_id 
        ORDER BY a.itemlab_id;"
      );
    }else{
      $query = $this->db->query(
        "SELECT 
          a.*
        FROM mst_item_lab a 
        ORDER BY a.itemlab_id;"
      );
    };
    return $query->result_array();
  }

}