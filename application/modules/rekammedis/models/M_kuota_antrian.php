<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kuota_antrian extends CI_Model {

	public function where($cookie)
	{
		$where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND a.parameter_nm LIKE '%".$this->db->escape_like_str($cookie['search']['term'])."%' ";
		}
    // if (@$cookie['search']['parameter_group'] != '') {
    //   $where .= "AND a.parameter_group LIKE '%".$this->db->escape_like_str($cookie['search']['parameter_group'])."%' ";
		// }
    // if (@$cookie['search']['parameter_nm'] != '') {
    //   $where .= "AND a.parameter_nm LIKE '%".$this->db->escape_like_str($cookie['search']['parameter_nm'])."%' ";
		// }
		return $where;
	}

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT a.*, b.lokasi_nm FROM lkt_antrian_kuota a 
      LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
      $where
      ORDER BY "
        .$cookie['order']['field']." ".$cookie['order']['type'].
      " LIMIT ".$cookie['cur_page'].",".$cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM lkt_antrian_kuota a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM lkt_antrian_kuota a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function list_parameter_group() {
    $sql = "SELECT parameter_group FROM lkt_antrian_kuota GROUP BY parameter_group ORDER BY parameter_group ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function get_data($id) {
    $sql = "SELECT * FROM lkt_antrian_kuota WHERE lokasi_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    $data['periode_tgl_awal'] = to_date($data['periode_tgl_awal']);
    $data['periode_tgl_akhir'] = to_date($data['periode_tgl_akhir']);
    if ($id == null) {
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('lkt_antrian_kuota', $data);
    }else{
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('lokasi_id', $id)->update('lkt_antrian_kuota', $data);
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('lokasi_id',$id)->update('lkt_antrian_kuota', $data);
  }

  public function delete($id, $permanent = false)
  {
    if ($permanent) {
      $this->db->where('lokasi_id', $id)->delete('lkt_antrian_kuota');
    }else{
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('lokasi_id', $id)->update('lkt_antrian_kuota', $data);
    }
  }
  
}