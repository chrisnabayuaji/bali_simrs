<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_ranap extends CI_Model {

	public function where($cookie)
	{
		$where = "WHERE a.is_deleted = 0 AND b.jenisreg_st = 2 ";
    if (@$cookie['search']['tgl_registrasi_from'] != '' && @$cookie['search']['tgl_registrasi_to']) {
      $where .= "AND (DATE(a.tgl_registrasi) BETWEEN '".to_date(@$cookie['search']['tgl_registrasi_from'])."' AND '".to_date(@$cookie['search']['tgl_registrasi_to'])."')";
		}else{
      $where .= "AND DATE(a.tgl_registrasi) = '".date('Y-m-d')."'";
    }
    if (@$cookie['search']['no_rm_nm'] != '') {
      $where .= "AND (a.pasien_id LIKE '%".$this->db->escape_like_str($cookie['search']['no_rm_nm'])."%' OR a.pasien_nm LIKE '%".$this->db->escape_like_str($cookie['search']['no_rm_nm'])."%' ) ";
    }
    if (@$cookie['search']['lokasi_id'] != '') {
      $where .= "AND a.lokasi_id = '".$this->db->escape_like_str($cookie['search']['lokasi_id'])."' ";
    }
    if (@$cookie['search']['jenispasien_id'] != '') {
      $where .= "AND a.jenispasien_id = '".$this->db->escape_like_str($cookie['search']['jenispasien_id'])."' ";
    }
    if (@$cookie['search']['dx_st'] != '') {
      if(@$cookie['search']['dx_st'] == 1){
        $where .= "AND a.diagnosisakhir_penyakit_id IS NOT NULL ";
      }
      if(@$cookie['search']['dx_st'] == 2){
        $where .= "AND a.diagnosisakhir_penyakit_id IS NULL ";
      }
    }
		return $where;
	}

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
              a.*, 
              b.lokasi_nm, 
              c.jenispasien_nm, 
              d.parameter_val as jeniskunjungan_nm,
              e.pegawai_nm as dokter_nm,
              f.icdx, f.penyakit_nm
            FROM reg_pasien a
            LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
            JOIN mst_parameter d ON a.jeniskunjungan_cd = d.parameter_cd AND d.parameter_field = 'jeniskunjungan_cd'
            LEFT JOIN mst_pegawai e ON a.dokter_id = e.pegawai_id
            LEFT JOIN mst_penyakit f on a.diagnosisakhir_penyakit_id = f.penyakit_id 
            $where
            ORDER BY a.periksa_st ASC, "
              .$cookie['order']['field']." ".$cookie['order']['type'].
            " LIMIT ".$cookie['cur_page'].",".$cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM reg_pasien a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT a.*, b.lokasi_nm, c.jenispasien_nm, d.parameter_val as jeniskunjungan_nm FROM reg_pasien a
    LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
    LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
    JOIN mst_parameter d ON a.jeniskunjungan_cd = d.parameter_cd AND d.parameter_field = 'jeniskunjungan_cd'
    $where";
    $query = $this->db->query($sql);
    return $query->num_rows();
  }

  function get_data($id) {
    $sql = "SELECT 
              a.*, b.pegawai_nm, c.jenispasien_nm, d.lokasi_nm, d.map_lokasi_depo, e.kelas_nm
            FROM reg_pasien a 
            LEFT JOIN mst_pegawai b ON a.dokter_id=b.pegawai_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id=c.jenispasien_id
            LEFT JOIN mst_lokasi d ON a.lokasi_id = d.lokasi_id
            LEFT JOIN mst_kelas e ON a.kelas_id = e.kelas_id
            WHERE a.reg_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function update_periksa_st($id, $periksa_st)
  {
    if ($periksa_st != '2') {
      $data['periksa_st'] = 1;
      $this->db->where('reg_id',$id)->update('reg_pasien', $data);
    }
  }


  // CATATAN MEDIS --------------------------------------
  public function catatan_medis_data($pasien_id='', $reg_id='', $lokasi_id='')
  {
    $sql = "SELECT * FROM dat_catatanmedis WHERE is_deleted=0 AND pasien_id=? AND reg_id=? AND lokasi_id=? ORDER BY catatanmedis_id";
    $query = $this->db->query($sql, array($pasien_id, $reg_id, $lokasi_id));
    return $query->result_array();
  }

  public function catatan_medis_get($catatanmedis_id='', $reg_id='', $pasien_id='')
  {
    $sql = "SELECT *, DATE_FORMAT(tgl_catat, '%Y-%m-%d %H:%i:%s') AS tgl_catat FROM dat_catatanmedis WHERE catatanmedis_id=? AND reg_id=? AND pasien_id=?";
    $query = $this->db->query($sql, array($catatanmedis_id, $reg_id, $pasien_id));
    return $query->row_array();
  }

  public function catatan_medis_save()
  {
    $data = $this->input->post();
    $data['tgl_catat'] = to_date($data['tgl_catat'], '', 'full_date');
    $data['user_cd'] = $this->session->userdata('sess_user_cd');
    if ($data['catatanmedis_id'] == null) {
      $data['catatanmedis_id'] = get_id('dat_catatanmedis');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_catatanmedis', $data);
      update_id('dat_catatanmedis', $data['catatanmedis_id']);
    }else{
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('catatanmedis_id', $data['catatanmedis_id'])->update('dat_catatanmedis', $data);
    }
  }

  public function catatan_medis_delete($catatanmedis_id='', $reg_id='', $pasien_id='')
  {
    trash('dat_catatanmedis', array(
      'catatanmedis_id' => $catatanmedis_id,
      'reg_id' => $reg_id,
      'pasien_id' => $pasien_id
    ));
    $this->db->where('catatanmedis_id', $catatanmedis_id)->where('reg_id', $reg_id)->where('pasien_id', $pasien_id)->delete('dat_catatanmedis');
  }

  // Pemeriksaan Medis
  public function pemeriksaan_fisik_data()
  {
    $data = $this->input->post();
    return $this->db->where('reg_id', $data['reg_id'])->where('pasien_id', $data['pasien_id'])->get('dat_anamnesis')->row_array();
  }

  public function pemeriksaan_fisik_save()
  {
    $data = $this->input->post();
    $cek = $this->db->where('reg_id', $data['reg_id'])->where('pasien_id', $data['pasien_id'])->get('dat_anamnesis')->row_array();
    if ($cek == null) {
      $data['anamnesis_id'] = get_id('dat_anamnesis');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_anamnesis', $data);
      update_id('dat_anamnesis', $data['anamnesis_id']);
    }else{
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('reg_id', $data['reg_id'])->where('pasien_id', $data['pasien_id'])->update('dat_anamnesis', $data);
    }
  }

  // Diagnosis
  public function diagnosis_data($pasien_id='', $reg_id='', $lokasi_id='')
  {
    $sql = "SELECT 
              a.*, b.penyakit_nm 
            FROM dat_diagnosis a
            LEFT JOIN mst_penyakit b ON a.penyakit_id=b.penyakit_id 
            WHERE a.is_deleted=0 AND a.pasien_id=? AND a.reg_id=? AND a.lokasi_id=? 
            ORDER BY a.diagnosis_id";
    $query = $this->db->query($sql, array($pasien_id, $reg_id, $lokasi_id));
    return $query->result_array();
  }

  public function penyakit_autocomplete($penyakit_nm=null) {
    $sql = "SELECT 
              *
            FROM mst_penyakit a   
            WHERE (a.penyakit_nm LIKE '%$penyakit_nm%' OR a.icdx LIKE '%$penyakit_nm%')
            ORDER BY a.icdx";                
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['penyakit_id']."#".$row['icdx'],
        'text' => $row['icdx'].' - '.$row['penyakit_nm']
      );
    }
    return $res;
  }

  public function diagnosis_get($diagnosis_id='', $reg_id='', $pasien_id='')
  {
    $sql = "SELECT 
              a.*, b.penyakit_nm, DATE_FORMAT(a.tgl_catat, '%Y-%m-%d %H:%i:%s') AS tgl_catat 
            FROM dat_diagnosis a 
            LEFT JOIN mst_penyakit b ON a.penyakit_id=b.penyakit_id
            WHERE a.diagnosis_id=? AND a.reg_id=? AND a.pasien_id=?";
    $query = $this->db->query($sql, array($diagnosis_id, $reg_id, $pasien_id));
    return $query->row_array();
  }

  public function diagnosis_save()
  {
    $data = $this->input->post();
    $penyakit_id = explode("#",$data['penyakit_id']);
    $data['penyakit_id'] = $penyakit_id[0];
    $data['icdx'] = $penyakit_id[1];
    $data['tgl_catat'] = to_date($data['tgl_catat'], '', 'full_date');
    $data['user_cd'] = $this->session->userdata('sess_user_cd');

    if ($data['diagnosis_id'] == null) {
      $data['diagnosis_id'] = get_id('dat_diagnosis');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_diagnosis', $data);
      update_id('dat_diagnosis', $data['diagnosis_id']);
    }else{
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('diagnosis_id', $data['diagnosis_id'])->update('dat_diagnosis', $data);
    }

    if ($data['is_diagnosisakhir'] == 1) {
      $this->db->where('reg_id', $data['reg_id'])->update('reg_pasien', array('diagnosisakhir_penyakit_id' => $data['penyakit_id']));
    }
  }

  public function diagnosis_delete($diagnosis_id='', $reg_id='', $pasien_id='')
  {
    trash('dat_diagnosis', array(
      'diagnosis_id' => $diagnosis_id,
      'reg_id' => $reg_id,
      'pasien_id' => $pasien_id
    ));
    $this->db->where('diagnosis_id', $diagnosis_id)->where('reg_id', $reg_id)->where('pasien_id', $pasien_id)->delete('dat_diagnosis');
  }

  public function diagnosis_row($id)
  {
    $sql = "SELECT * FROM mst_penyakit WHERE penyakit_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }


  //Tindakan 
  public function tindakan_data($pasien_id='', $reg_id='', $lokasi_id='')
  {
    $sql = "SELECT 
              a.*, 
              b.pegawai_nm AS pegawai_nm_1, 
              c.pegawai_nm AS pegawai_nm_2, 
              d.pegawai_nm AS pegawai_nm_3, 
              e.pegawai_nm AS pegawai_nm_4, 
              f.pegawai_nm AS pegawai_nm_5, 
              g.kelas_nm
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            LEFT JOIN mst_pegawai c ON a.petugas_id_2 = c.pegawai_id
            LEFT JOIN mst_pegawai d ON a.petugas_id_3 = d.pegawai_id
            LEFT JOIN mst_pegawai e ON a.petugas_id_4 = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_5 = f.pegawai_id
            LEFT JOIN mst_kelas g ON a.kelas_id = g.kelas_id 
            WHERE a.is_deleted = 0 AND a.pasien_id=? AND a.reg_id=? AND a.lokasi_id=? 
            ORDER BY a.tindakan_id";
    $query = $this->db->query($sql, array($pasien_id, $reg_id, $lokasi_id));
    return $query->result_array();
  }

  public function tarifkelas_autocomplete($tarif_nm=null) {
    $sql = "SELECT 
              a.*,b.tarif_nm,c.kelas_nm
            FROM mst_tarif_kelas a
            JOIN mst_tarif b ON a.tarif_id = b.tarif_id
            JOIN mst_kelas c ON a.kelas_id = c.kelas_id
            WHERE b.tarif_nm LIKE '%$tarif_nm%' ";                
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['tarifkelas_id'],
        'text' => $row['tarifkelas_id'].' - '.$row['tarif_nm'].' - '.$row['kelas_nm']
      );
    }
    return $res;
  }

  public function petugas_autocomplete($petugas_nm=null) {
    $sql = "SELECT 
              a.*
            FROM mst_pegawai a
            WHERE a.pegawai_nm LIKE '%$petugas_nm%' ";                
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['pegawai_id'],
        'text' => $row['pegawai_nm']
      );
    }
    return $res;
  }

  public function tindakan_save()
  {
    $data = $this->input->post();
    unset($data['petugas_no']);
    $data['tgl_catat'] = to_date($data['tgl_catat'], '', 'full_date');
    $data['user_cd'] = $this->session->userdata('sess_user_cd');
    $tk = $this->db
      ->select('a.*, b.tarif_nm')
      ->join('mst_tarif b','a.tarif_id = b.tarif_id', 'left')
      ->where('a.tarifkelas_id', $data['tarifkelas_id'])
      ->get('mst_tarif_kelas a')->row_array();
    unset($data['tarifkelas_id']);
    $data['tarif_id'] = $tk['tarif_id'];
    $data['kelas_id'] = $tk['kelas_id'];
    $data['tarif_nm'] = $tk['tarif_nm'];
    $data['js'] = $tk['js'];
    $data['jp'] = $tk['jp'];
    $data['jb'] = $tk['jb'];
    $data['nom_tarif'] = $tk['nominal'];
    $data['jml_awal'] = $tk['nominal']*$data['qty'];
    $data['jml_tagihan'] = $tk['nominal']*$data['qty'];

    // petugas
    $d = $data;
    unset($d['petugas_id'], $d['petugas_id_2'], $d['petugas_id_3'], $d['petugas_id_4'], $d['petugas_id_5']);
    $petugas = array();
    for ($i=1; $i <= 5 ; $i++) { 
      if ($i == '1') {
        $petugas_id = $data['petugas_id'];
      }else{
        $petugas_id = $data['petugas_id_'.$i];
      }

      if ($petugas_id !='0') {
        array_push($petugas, $petugas_id);
      }
    }
    
    for ($j=0; $j < count($petugas) ; $j++) { 
      $d['petugas_id_'.($j+1)] = $petugas[$j];
    }

    unset($data['petugas_id'], $data['petugas_id_2'], $data['petugas_id_3'], $data['petugas_id_4'], $data['petugas_id_5']);

    $data['petugas_id'] = @$d['petugas_id_1'];
    $data['petugas_id_2'] = @$d['petugas_id_2'];
    $data['petugas_id_3'] = @$d['petugas_id_3'];
    $data['petugas_id_4'] = @$d['petugas_id_4'];
    $data['petugas_id_5'] = @$d['petugas_id_5'];

    if ($data['tindakan_id'] == null) {
      $data['tindakan_id'] = get_id('dat_tindakan');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_tindakan', $data);
      update_id('dat_tindakan', $data['tindakan_id']);
    }else{
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('tindakan_id', $data['tindakan_id'])->update('dat_tindakan', $data);
    }
  }

  public function tindakan_get($tindakan_id='', $reg_id='', $pasien_id='')
  {
    $sql = "SELECT 
              a.*, b.kelas_nm, c.pegawai_nm, DATE_FORMAT(a.tgl_catat, '%Y-%m-%d %H:%i:%s') AS tgl_catat
            FROM dat_tindakan a
            LEFT JOIN mst_kelas b ON a.kelas_id = b.kelas_id
            LEFT JOIN mst_pegawai c ON a.petugas_id = c.pegawai_id 
            WHERE a.tindakan_id=? AND a.reg_id=? AND a.pasien_id=?";
    $query = $this->db->query($sql, array($tindakan_id, $reg_id, $pasien_id));
    $row = $query->row_array();
    $jml_petugas = 0;
    if ($row['petugas_id'] !='') {
      $jml_petugas += 1;
    }
    if ($row['petugas_id_2'] !='') {
      $jml_petugas += 1;
    }
    if ($row['petugas_id_3'] !='') {
      $jml_petugas += 1;
    }
    if ($row['petugas_id_4'] !='') {
      $jml_petugas += 1;
    }
    if ($row['petugas_id_5'] !='') {
      $jml_petugas += 1;
    }
    $row['jml_petugas'] = $jml_petugas;
    return $row;
  }

  public function tindakan_delete($tindakan_id='', $reg_id='', $pasien_id='')
  {
    trash('dat_tindakan', array(
      'tindakan_id' => $tindakan_id,
      'reg_id' => $reg_id,
      'pasien_id' => $pasien_id
    ));
    $this->db->where('tindakan_id', $tindakan_id)->where('reg_id', $reg_id)->where('pasien_id', $pasien_id)->delete('dat_tindakan');
  }

  public function tarifkelas_row($id)
  {
    $sql = "SELECT a.*, b.tarif_nm, c.kelas_nm FROM mst_tarif_kelas a
      LEFT JOIN mst_tarif b ON a.tarif_id = b.tarif_id
      LEFT JOIN mst_kelas c ON a.kelas_id = c.kelas_id
      WHERE tarifkelas_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  public function petugas_row($id)
  {
    $sql = "SELECT * FROM mst_pegawai WHERE pegawai_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  public function get_tindakan($tindakan_id = null) {
    $sql = "SELECT 
              a.*, 
              b.pegawai_nm AS pegawai_nm_1, 
              c.pegawai_nm AS pegawai_nm_2, 
              d.pegawai_nm AS pegawai_nm_3, 
              e.pegawai_nm AS pegawai_nm_4, 
              f.pegawai_nm AS pegawai_nm_5
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            LEFT JOIN mst_pegawai c ON a.petugas_id_2 = c.pegawai_id
            LEFT JOIN mst_pegawai d ON a.petugas_id_3 = d.pegawai_id
            LEFT JOIN mst_pegawai e ON a.petugas_id_4 = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_5 = f.pegawai_id
            WHERE a.tindakan_id=?";
    $query = $this->db->query($sql, $tindakan_id);
    return $query->row_array();
  }

  public function delete_petugas($tindakan_id = null, $form_name = null) {
    $data[$form_name] = '';
    $query = $this->db->where('tindakan_id', $tindakan_id)->update('dat_tindakan', $data);
    return $query;
  }


  // Pemberian Obat
  public function pemberian_obat_data($reg_id='')
  {
    $sql = "SELECT 
              a.* 
            FROM dat_resep_group a
            WHERE a.is_deleted=0 AND a.reg_id=? 
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, $reg_id);
    $result = $query->result_array();
    // 
    foreach($result as $key => $val) {
      $result[$key]['list_dat_resep'] = $this->list_dat_resep($val['resepgroup_id'], $reg_id);
    }
    return $result;
  }

  public function list_dat_resep($resepgroup_id='', $reg_id='')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_resep a
            LEFT JOIN mst_obat b ON a.obat_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.resepgroup_id=? AND a.reg_id=? 
            ORDER BY a.reg_id ASC";
    $query = $this->db->query($sql, array($resepgroup_id, $reg_id));
    return $query->result_array();
  }

  public function obat_autocomplete($obat_nm=null, $map_lokasi_depo=null) {
    $sql = "SELECT 
              *
            FROM far_stok_depo a
            WHERE 
              (a.obat_nm LIKE '%$obat_nm%' OR a.obat_id LIKE '%$obat_nm%') AND
              a.stok_st = 1 AND a.stok_akhir > 0 AND a.lokasi_id = '$map_lokasi_depo'
            ORDER BY a.obat_id";                
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['obat_id']."#".$row['obat_nm']."#".$row['stokdepo_id'],
        'text' => $row['obat_id'].' - '.$row['obat_nm']
      );
    }
    return $res;
  }

  public function pemberian_obat_get($resep_id='', $reg_id='')
  {
    $sql = "SELECT 
              a.*, b.obat_nm, DATE_FORMAT(a.tgl_catat, '%Y-%m-%d %H:%i:%s') AS tgl_catat 
            FROM dat_resep a 
            LEFT JOIN mst_obat b ON a.obat_id=b.obat_id
            WHERE a.resep_id=? AND a.reg_id=?";
    $query = $this->db->query($sql, array($resep_id, $reg_id));
    return $query->row_array();
  }

  public function pemberian_obat_save()
  {
    $data = $this->input->post();

    $obat_id = explode("#",$data['obat_id']);
    $dat_resep['obat_id'] = $obat_id[0];
    $dat_resep['obat_nm'] = $obat_id[1];
    $dat_resep['stokdepo_id'] = @$obat_id[2];

    $tgl = substr($data['tgl_catat'],0,2);
    $bln = substr($data['tgl_catat'],3,2);
    $thn = substr($data['tgl_catat'],8,2);

    // get data far_stok_depo
    $far_stok_depo = $this->db->where('stokdepo_id', @$obat_id[2])->get('far_stok_depo')->row_array();
    $cek_by_reg_id = $this->cek_by_reg_id($data['reg_id'], $data['tgl_catat']);
    // $cek_resepgroup_id = substr($cek_by_reg_id['resepgroup_id'],0,6);
    $tgl_catat = to_date($data['tgl_catat'], '', 'full_date');

    $dat_resep['dosis'] = $data['dosis'];
    $dat_resep['qty'] = $data['qty'];
    $dat_resep['carapakai_cd'] = $data['carapakai_cd'];
    $dat_resep['jenisresep_cd'] = $data['jenisresep_cd'];
    $dat_resep['keterangan_resep'] = $data['keterangan_resep'];
    $dat_resep['reg_id'] = $data['reg_id'];
    $dat_resep['lokasi_id'] = $data['lokasi_id'];
    $dat_resep['harga'] = @$far_stok_depo['harga_akhir'];
    $dat_resep['jml_awal'] = $dat_resep['harga'] * $dat_resep['qty'];
    $dat_resep['jml_potongan'] = 0;
    $dat_resep['jml_tagihan'] = $dat_resep['jml_awal'] - $dat_resep['jml_potongan'];
    $dat_resep['tgl_catat'] = to_date($data['tgl_catat'], '', 'full_date');
    $dat_resep['user_cd'] = $this->session->userdata('sess_user_cd');

    if ($data['resep_id'] == null) {
      if ($cek_by_reg_id['resepgroup_id'] == '' || $cek_by_reg_id['resepgroup_id'] == NULL) {
        if ($data['resepgroup_id'] == 'otomatis') {
          $resepgroup_id = get_id('dat_resep_group');
        }
      }else{
        if (to_date($cek_by_reg_id['tgl_catat'], '', 'date') != to_date($tgl_catat, '', 'date')) {
          $resepgroup_id = get_id('dat_resep_group');
        }else{
          $resepgroup_id = $cek_by_reg_id['resepgroup_id'];
        }
      }
    }else{
      $resepgroup_id = $data['resepgroup_id'];
    }

    if ($data['resep_id'] == null) {
      $dat_resep['resep_id'] = get_id('dat_resep');
      $dat_resep['resepgroup_id'] = $resepgroup_id;
      $dat_resep['created_at'] = date('Y-m-d H:i:s');
      $dat_resep['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_resep', $dat_resep);
      update_id('dat_resep', $dat_resep['resep_id']);

      if ($cek_by_reg_id['resepgroup_id'] == '' || $cek_by_reg_id['resepgroup_id'] == NULL) {
        if ($data['resepgroup_id'] == 'otomatis') {
          update_id('dat_resep_group', $resepgroup_id);
        }
      }else{
        if (to_date($cek_by_reg_id['tgl_catat'], '', 'date') != to_date($tgl_catat, '', 'date')) {
          update_id('dat_resep_group', $resepgroup_id);
        }
      }

    }else{
      $dat_resep['updated_at'] = date('Y-m-d H:i:s');
      $dat_resep['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('resep_id', $data['resep_id'])->update('dat_resep', $dat_resep);
    }

    // insert dat_resep_group
    $get_jml = $this->get_jml($resepgroup_id);
    $dat_resep_group['reg_id'] = $data['reg_id'];
    $dat_resep_group['lokasi_id'] = $data['lokasi_id'];
    $dat_resep_group['group_jml_awal'] = $get_jml['group_jml_awal'];
    $dat_resep_group['group_jml_potongan'] = $get_jml['group_jml_potongan'];
    $dat_resep_group['group_jml_tagihan'] = $get_jml['group_jml_tagihan'];
    $dat_resep_group['grand_jml_bruto'] = 0;
    $dat_resep_group['grand_prs_ppn'] = 0;
    $dat_resep_group['grand_nom_ppn'] = 0;
    $dat_resep_group['grand_embalace'] = 0;
    $dat_resep_group['grand_jml_potongan'] = 0;
    $dat_resep_group['grand_jml_netto'] = 0;
    $dat_resep_group['grand_pembulatan'] = 0;
    $dat_resep_group['grand_jml_tagihan'] = 0;
    $dat_resep_group['tgl_catat'] = to_date($data['tgl_catat'], '', 'full_date');
    $dat_resep_group['user_cd'] = $this->session->userdata('sess_user_cd');

    if ($data['resep_id'] == null) {
      if ($cek_by_reg_id['resepgroup_id'] == '' || $cek_by_reg_id['resepgroup_id'] == NULL) {
        if ($data['resepgroup_id'] == 'otomatis') {
          $dat_resep_group['resepgroup_id'] = $resepgroup_id;
          $dat_resep_group['created_at'] = date('Y-m-d H:i:s');
          $dat_resep_group['created_by'] = $this->session->userdata('sess_user_realname');
          $this->db->insert('dat_resep_group', $dat_resep_group);
        }else{
          $dat_resep_group['updated_at'] = date('Y-m-d H:i:s');
          $dat_resep_group['updated_by'] = $this->session->userdata('sess_user_realname');
          $this->db->where('resepgroup_id', $resepgroup_id)->update('dat_resep_group', $dat_resep_group);
        }
      }else{
        if (to_date($cek_by_reg_id['tgl_catat'], '', 'date') != to_date($tgl_catat, '', 'date')) {
          $dat_resep_group['resepgroup_id'] = $resepgroup_id;
          $dat_resep_group['created_at'] = date('Y-m-d H:i:s');
          $dat_resep_group['created_by'] = $this->session->userdata('sess_user_realname');
          $this->db->insert('dat_resep_group', $dat_resep_group);
        }else{
          $dat_resep_group['updated_at'] = date('Y-m-d H:i:s');
          $dat_resep_group['updated_by'] = $this->session->userdata('sess_user_realname');
          $this->db->where('resepgroup_id', $resepgroup_id)->update('dat_resep_group', $dat_resep_group);
        }
      }
    }else{
      $dat_resep_group['updated_at'] = date('Y-m-d H:i:s');
      $dat_resep_group['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('resepgroup_id', $data['resepgroup_id'])->update('dat_resep_group', $dat_resep_group);
    }

  }

  public function get_jml($resepgroup_id='')
  {
    $sql = "SELECT 
              SUM(jml_awal) as group_jml_awal,
              SUM(jml_potongan) as group_jml_potongan,
              SUM(jml_tagihan) as group_jml_tagihan
            FROM dat_resep
            WHERE resepgroup_id=?";
    $query = $this->db->query($sql, $resepgroup_id);
    return $query->row_array();
  }

  public function cek_by_reg_id($reg_id='', $tgl_catat='')
  {
    $tgl_catat = to_date($tgl_catat, '', 'date');
    $sql = "SELECT *
            FROM dat_resep
            WHERE reg_id=? AND DATE(tgl_catat)='$tgl_catat'
            ORDER BY resepgroup_id DESC";
    $query = $this->db->query($sql, $reg_id);
    return $query->row_array();
  }

  public function pemberian_obat_delete($resep_id='', $reg_id='', $resepgroup_id='')
  {
    trash('dat_resep', array(
      'resep_id' => $resep_id,
      'reg_id' => $reg_id
    ));
    $this->db->where('resep_id', $resep_id)->where('reg_id', $reg_id)->delete('dat_resep');

    $count_dat_resep = $this->count_dat_resep($resepgroup_id);
    if ($count_dat_resep > 0) {
      // update dat_resep_group
      $get_jml = $this->get_jml($resepgroup_id);
      $dat_resep_group['group_jml_awal'] = $get_jml['group_jml_awal'];
      $dat_resep_group['group_jml_potongan'] = $get_jml['group_jml_potongan'];
      $dat_resep_group['group_jml_tagihan'] = $get_jml['group_jml_tagihan'];
      $dat_resep_group['updated_at'] = date('Y-m-d H:i:s');
      $dat_resep_group['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('resepgroup_id', $resepgroup_id)->update('dat_resep_group', $dat_resep_group);
    }else{
      $this->db->where('resepgroup_id', $resepgroup_id)->delete('dat_resep_group  ');      
    }
  }

  public function obat_row($data)
  {
    $sql = "SELECT * FROM far_stok_depo WHERE stokdepo_id=?";
    $query = $this->db->query($sql, array($data['stokdepo_id']));
    return $query->row_array();
  }

  public function obat_row_master($id)
  {
    $sql = "SELECT * FROM mst_obat WHERE obat_id=?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  public function get_no_resep($reg_id)
  {
    $sql = "SELECT * FROM dat_resep WHERE reg_id=? ORDER BY resep_id DESC LIMIT 1";
    $query = $this->db->query($sql, $reg_id);
    $row = $query->row_array();
    if ($row['resepgroup_id'] !='') {
      $row['resepgroup_id'] = $row['resepgroup_id'];
    }else{
      $row['resepgroup_id'] = 'otomatis';
    }
    return $row;
  }

  public function count_dat_resep($resepgroup_id='')
  {
    $sql = "SELECT 
              COUNT(*) AS jml_data
            FROM dat_resep
            WHERE resepgroup_id=?";
    $query = $this->db->query($sql, $resepgroup_id);
    $row = $query->row_array();
    return $row['jml_data'];
  }

  // BHP
  public function bhp_data($reg_id='', $lokasi_id='')
  {
    $sql = "SELECT 
              a.*
            FROM dat_bhp a
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.lokasi_id=? 
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $lokasi_id));
    return $query->result_array();
  }

  public function bhp_autocomplete($obat_nm=null, $map_lokasi_depo=null) {
    $sql = "SELECT 
            *
          FROM far_stok_depo a
          WHERE 
            (a.obat_nm LIKE '%$obat_nm%' OR a.obat_id LIKE '%$obat_nm%') AND
            a.stok_st = 1 AND a.stok_akhir > 0 AND a.lokasi_id = '$map_lokasi_depo' 
            AND a.jenisbarang_cd = '03'
          ORDER BY a.obat_id";                
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['obat_id']."#".$row['obat_nm']."#".$row['stokdepo_id'],
        'text' => $row['obat_id'].' - '.$row['obat_nm']
      );
    }
    return $res;
  }

  public function bhp_row($data)
  {
    $sql = "SELECT * FROM far_stok_depo WHERE stokdepo_id=?";
    $query = $this->db->query($sql, array($data['stokdepo_id']));
    return $query->row_array();
  }

  public function bhp_get($bhp_id='', $reg_id='')
  {
    $sql = "SELECT 
              a.*, DATE_FORMAT(a.tgl_catat, '%Y-%m-%d %H:%i:%s') AS tgl_catat
            FROM dat_bhp a 
            WHERE a.bhp_id=? AND a.reg_id=?";
    $query = $this->db->query($sql, array($bhp_id, $reg_id));
    return $query->row_array();
  }

  public function bhp_save()
  {
    $data = $this->input->post();
    $barang_id = explode("#",$data['barang_id']);
    $data['barang_id'] = $barang_id[0];
    $data['barang_nm'] = $barang_id[1];
    $data['stokdepo_id'] = @$barang_id[2];

    $far_stok_depo = $this->db->where('stokdepo_id', @$barang_id[2])->get('far_stok_depo')->row_array();

    $data['stokdepo_id'] = $far_stok_depo['stokdepo_id'];
    $data['harga'] = $far_stok_depo['harga_akhir'];
    $data['jml_awal'] = $data['harga'] * $data['qty'];
    $data['jml_potongan'] = null;
    $data['jml_tagihan'] = $data['jml_awal'] - $data['jml_potongan'];
    $data['tgl_catat'] = to_date($data['tgl_catat'], '', 'full_date');
    $data['user_cd'] = $this->session->userdata('sess_user_cd');

    $qty_sebelum = $data['qty_sebelum'];
    $stokdepo_id_sebelum = $data['stokdepo_id_sebelum'];
    unset($data['qty_sebelum'], $data['stokdepo_id_sebelum']);

    if ($data['bhp_id'] == null) {
      $data['bhp_id'] = get_id('dat_bhp');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_bhp', $data);
      update_id('dat_bhp', $data['bhp_id']);

      // update far_stok_depo
      $sql = "UPDATE far_stok_depo 
              SET stok_terpakai=ifnull(stok_terpakai,0) + '".$data['qty']."', 
                  stok_akhir=ifnull(stok_akhir,0) - '".$data['qty']."' 
              WHERE stokdepo_id='".$far_stok_depo['stokdepo_id']."'"; 
      $this->db->query($sql);
    }else{
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('bhp_id', $data['bhp_id'])->update('dat_bhp', $data);

      // update far_stok_depo
      if ($stokdepo_id_sebelum != $far_stok_depo['stokdepo_id']) {
        $sql1 = "UPDATE far_stok_depo 
              SET stok_terpakai=ifnull(stok_terpakai,0) - '".$qty_sebelum."', 
                  stok_akhir=ifnull(stok_akhir,0) + '".$qty_sebelum."' 
              WHERE stokdepo_id='".$stokdepo_id_sebelum."'"; 
        $this->db->query($sql1);

        $sql2 = "UPDATE far_stok_depo 
              SET stok_terpakai=ifnull(stok_terpakai,0) + '".$data['qty']."', 
                  stok_akhir=ifnull(stok_akhir,0) - '".$data['qty']."' 
              WHERE stokdepo_id='".$far_stok_depo['stokdepo_id']."'"; 
        $this->db->query($sql2);
      }else{
        $sql = "UPDATE far_stok_depo 
                SET stok_terpakai=ifnull(stok_terpakai,0) - '".$qty_sebelum."' + '".$data['qty']."', 
                    stok_akhir=ifnull(stok_akhir,0) + '".$qty_sebelum."' - '".$data['qty']."' 
                WHERE stokdepo_id='".$far_stok_depo['stokdepo_id']."'"; 
        $this->db->query($sql);
      }
    }
  }

  public function bhp_delete($bhp_id='', $reg_id='')
  {
    trash('dat_bhp', array(
      'bhp_id' => $bhp_id,
      'reg_id' => $reg_id
    ));

    $get_bhp = $this->db->where('bhp_id', $bhp_id)->where('reg_id', $reg_id)->get('dat_bhp')->row_array();

    $this->db->where('bhp_id', $bhp_id)->where('reg_id', $reg_id)->delete('dat_bhp');

    // update far_stok_depo
    $sql = "UPDATE far_stok_depo 
          SET stok_terpakai=ifnull(stok_terpakai,0) - '".$get_bhp['qty']."', 
              stok_akhir=ifnull(stok_akhir,0) + '".$get_bhp['qty']."' 
          WHERE stokdepo_id='".$get_bhp['stokdepo_id']."'"; 
    $this->db->query($sql);
  }


  // ALKES
  public function alkes_data($reg_id='', $lokasi_id='')
  {
    $sql = "SELECT 
              a.*
            FROM dat_alkes a
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.lokasi_id=? 
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $lokasi_id));
    return $query->result_array();
  }

  public function alkes_autocomplete($obat_nm=null, $map_lokasi_depo=null) {
    $sql = "SELECT 
            *
          FROM far_stok_depo a
          WHERE 
            (a.obat_nm LIKE '%$obat_nm%' OR a.obat_id LIKE '%$obat_nm%') AND
            a.stok_st = 1 AND a.stok_akhir > 0 AND a.lokasi_id = '$map_lokasi_depo' 
            AND a.jenisbarang_cd = '02'
          ORDER BY a.obat_id";
    $query = $this->db->query($sql);
    $result = $query->result_array();
    $res = array();
    foreach ($result as $row) {
      $res[] = array(
        'id' => $row['obat_id']."#".$row['obat_nm']."#".$row['stokdepo_id'],
        'text' => $row['obat_id'].' - '.$row['obat_nm']
      );
    }
    return $res;
  }

  public function alkes_row($data)
  {
    $sql = "SELECT * FROM far_stok_depo WHERE stokdepo_id=?";
    $query = $this->db->query($sql, array($data['stokdepo_id']));
    return $query->row_array();
  }

  public function alkes_get($alkes_id='', $reg_id='')
  {
    $sql = "SELECT 
              a.*, DATE_FORMAT(a.tgl_catat, '%Y-%m-%d %H:%i:%s') AS tgl_catat
            FROM dat_alkes a 
            WHERE a.alkes_id=? AND a.reg_id=?";
    $query = $this->db->query($sql, array($alkes_id, $reg_id));
    return $query->row_array();
  }

  public function alkes_save()
  {
    $data = $this->input->post();
    $barang_id = explode("#",$data['barang_id']);
    $data['barang_id'] = $barang_id[0];
    $data['barang_nm'] = $barang_id[1];
    $data['stokdepo_id'] = @$barang_id[2];

    $far_stok_depo = $this->db->where('stokdepo_id', @$barang_id[2])->get('far_stok_depo')->row_array();
    
    $data['stokdepo_id'] = $far_stok_depo['stokdepo_id'];
    $data['harga'] = $far_stok_depo['harga_akhir'];
    $data['jml_awal'] = $data['harga'] * $data['qty'];
    $data['jml_potongan'] = null;
    $data['jml_tagihan'] = $data['jml_awal'] - $data['jml_potongan'];
    $data['tgl_catat'] = to_date($data['tgl_catat'], '', 'full_date');
    $data['user_cd'] = $this->session->userdata('sess_user_cd');

    $qty_sebelum = $data['qty_sebelum'];
    $stokdepo_id_sebelum = $data['stokdepo_id_sebelum'];
    unset($data['qty_sebelum'], $data['stokdepo_id_sebelum']);

    if ($data['alkes_id'] == null) {
      $data['alkes_id'] = get_id('dat_alkes');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_alkes', $data);
      update_id('dat_alkes', $data['alkes_id']);

      // update far_stok_depo
      $sql = "UPDATE far_stok_depo 
              SET stok_terpakai=ifnull(stok_terpakai,0) + '".$data['qty']."', 
                  stok_akhir=ifnull(stok_akhir,0) - '".$data['qty']."' 
              WHERE stokdepo_id='".$far_stok_depo['stokdepo_id']."'"; 
      $this->db->query($sql);
    }else{
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('alkes_id', $data['alkes_id'])->update('dat_alkes', $data);

      // update far_stok_depo
      if ($stokdepo_id_sebelum != $far_stok_depo['stokdepo_id']) {
        $sql1 = "UPDATE far_stok_depo 
              SET stok_terpakai=ifnull(stok_terpakai,0) - '".$qty_sebelum."', 
                  stok_akhir=ifnull(stok_akhir,0) + '".$qty_sebelum."' 
              WHERE stokdepo_id='".$stokdepo_id_sebelum."'"; 
        $this->db->query($sql1);

        $sql2 = "UPDATE far_stok_depo 
              SET stok_terpakai=ifnull(stok_terpakai,0) + '".$data['qty']."', 
                  stok_akhir=ifnull(stok_akhir,0) - '".$data['qty']."' 
              WHERE stokdepo_id='".$far_stok_depo['stokdepo_id']."'"; 
        $this->db->query($sql2);
      }else{
        $sql = "UPDATE far_stok_depo 
                SET stok_terpakai=ifnull(stok_terpakai,0) - '".$qty_sebelum."' + '".$data['qty']."', 
                    stok_akhir=ifnull(stok_akhir,0) + '".$qty_sebelum."' - '".$data['qty']."' 
                WHERE stokdepo_id='".$far_stok_depo['stokdepo_id']."'"; 
        $this->db->query($sql);
      }
    }
  }

  public function alkes_delete($alkes_id='', $reg_id='')
  {
    trash('dat_alkes', array(
      'alkes_id' => $alkes_id,
      'reg_id' => $reg_id
    ));

    $get_alkes = $this->db->where('alkes_id', $alkes_id)->where('reg_id', $reg_id)->get('dat_alkes')->row_array();

    $this->db->where('alkes_id', $alkes_id)->where('reg_id', $reg_id)->delete('dat_alkes');

    // update far_stok_depo
    $sql = "UPDATE far_stok_depo 
          SET stok_terpakai=ifnull(stok_terpakai,0) - '".$get_alkes['qty']."', 
              stok_akhir=ifnull(stok_akhir,0) + '".$get_alkes['qty']."' 
          WHERE stokdepo_id='".$get_alkes['stokdepo_id']."'"; 
    $this->db->query($sql);
  }

  //Laboratorium -------------------------------------------------------------------------------------
  public function laboratorium_data($reg_id='', $pasien_id='', $lokasi_id='')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM lab_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE src_reg_id=? AND pasien_id=? AND src_lokasi_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id,$pasien_id,$lokasi_id));
    return $query->result_array();
  }

  public function laboratorium_save($reg_id = null, $id = null)
  {
    $d = $this->input->post();
    $data = $d;
    
    unset($data['checkitem']);
    unset($data['laboratorium_table_length']);

    $data['tgl_order'] = to_date($data['tgl_order'],'-','full_date');
    if ($id == null) {
      $data['pemeriksaan_id'] = get_id('lab_pemeriksaan');
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('lab_pemeriksaan', $data);
      update_id('lab_pemeriksaan', $data['pemeriksaan_id']);
    }else{
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('pemeriksaan_id', $id)->update('lab_pemeriksaan', $data);
    }

    //delete all rincian
    $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->delete('lab_pemeriksaan_rinc');
    if (isset($d['checkitem'])) {
      foreach ($d['checkitem'] as $v) {
        $item = $this->db->where('itemlab_id', $v)->get('mst_item_lab')->row_array();
        $dr = array(
          'pemeriksaanrinc_id' => get_id('lab_pemeriksaan_rinc'),
          'pemeriksaan_id' => $data['pemeriksaan_id'],
          'itemlab_id' => $item['itemlab_id'],
          'tarif_id' => $item['tarif_id'],
          'nilai_normal' => $item['nilai_normal'],
          'user_cd' => $this->session->userdata('sess_user_cd'),
          'created_at' => date('Y-m-d H:i:s'),
          'created_by' => $this->session->userdata('sess_user_realname'),
        );
        $this->db->insert('lab_pemeriksaan_rinc', $dr);
        update_id('lab_pemeriksaan_rinc', $dr['pemeriksaanrinc_id']);
      }
    }
  }

  public function laboratorium_delete($pemeriksaan_id='', $reg_id='', $pasien_id='', $lokasi_id='')
  {
    trash('lab_pemeriksaan_rinc', array('pemeriksaan_id' => $pemeriksaan_id));
    trash('lab_pemeriksaan', array(
      'pemeriksaan_id' => $pemeriksaan_id,
      'src_reg_id' => $reg_id,
      'pasien_id' => $pasien_id,
      'src_lokasi_id' => $lokasi_id,
    ));

    $this->db
      ->where('pemeriksaan_id', $pemeriksaan_id)
      ->delete('lab_pemeriksaan_rinc');
    $this->db
      ->where('pemeriksaan_id', $pemeriksaan_id)
      ->where('src_reg_id', $reg_id)
      ->where('pasien_id', $pasien_id)
      ->where('src_lokasi_id', $lokasi_id)
      ->delete('lab_pemeriksaan');
  }

  public function laboratorium_get($pemeriksaan_id='')
  {
    $sql = "SELECT 
            a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
          FROM lab_pemeriksaan a
          LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
          LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
          WHERE pemeriksaan_id=?";
    $query = $this->db->query($sql, array($pemeriksaan_id));
    return $query->row_array();
  }

  public function laboratorium_pemeriksaan_data($reg_id, $pemeriksaan_id)
  {
    $query = $this->db->query(
      "SELECT b.parent_id FROM lab_pemeriksaan_rinc a
      JOIN mst_item_lab b ON a.itemlab_id = b.itemlab_id
      GROUP BY b.parent_id
      ORDER BY b.parent_id"
    )->result_array();
    $res = null;
    if ($query != null) {
      $res = array();
      foreach ($query as $k => $v) {
        $res[$k] = $this->db->query(
          "SELECT itemlab_nm FROM mst_item_lab WHERE itemlab_id='".$v['parent_id']."'"
        )->row_array();
        $res[$k]['rinc'] = $this->db->query(
          "SELECT a.*, b.itemlab_nm FROM lab_pemeriksaan_rinc a
          JOIN mst_item_lab b ON a.itemlab_id = b.itemlab_id
          WHERE b.parent_id = '".$v['parent_id']."' AND a.pemeriksaan_id = '".$pemeriksaan_id."'
          ORDER BY a.itemlab_id ASC"
        )->result_array();
      }
    }
    return $res;
  }

  public function laboratorium_tarif_tindakan_data($reg_id='', $pemeriksaan_id='')
  {
    $sql = "SELECT a.*, b.pegawai_nm 
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            WHERE a.is_deleted=0 AND a.lokasi_id = '03.02'
            AND a.reg_id=? AND a.pemeriksaan_id=?";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function laboratorium_bhp_data($reg_id='', $pemeriksaan_id='')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_bhp a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.02'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function laboratorium_alkes_data($reg_id='', $pemeriksaan_id='')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_alkes a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.02'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  //Radiologi -------------------------------------------------------------------------------------
  public function radiologi_data($reg_id='', $pasien_id='', $lokasi_id='')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM rad_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE src_reg_id=? AND pasien_id=? AND src_lokasi_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id,$pasien_id,$lokasi_id));
    return $query->result_array();
  }

  public function radiologi_save($reg_id = null, $id = null)
  {
    $d = $this->input->post();
    $data = $d;
    
    unset($data['checkitem']);
    unset($data['radiologi_table_length']);

    $data['tgl_order'] = to_date($data['tgl_order'],'-','full_date');
    if ($id == null) {
      $data['pemeriksaan_id'] = get_id('rad_pemeriksaan');
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('rad_pemeriksaan', $data);
      update_id('rad_pemeriksaan', $data['pemeriksaan_id']);
    }else{
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('pemeriksaan_id', $id)->update('rad_pemeriksaan', $data);
    }

    //delete all rincian
    $this->db->where('pemeriksaan_id', $data['pemeriksaan_id'])->delete('rad_pemeriksaan_rinc');
    if (isset($d['checkitem'])) {
      foreach ($d['checkitem'] as $v) {
        $item = $this->db->where('itemrad_id', $v)->get('mst_item_rad')->row_array();
        $dr = array(
          'pemeriksaanrinc_id' => get_id('rad_pemeriksaan_rinc'),
          'pemeriksaan_id' => $data['pemeriksaan_id'],
          'itemrad_id' => $item['itemrad_id'],
          'tarif_id' => $item['tarif_id'],
          'user_cd' => $this->session->userdata('sess_user_cd'),
          'created_at' => date('Y-m-d H:i:s'),
          'created_by' => $this->session->userdata('sess_user_realname'),
        );
        $this->db->insert('rad_pemeriksaan_rinc', $dr);
        update_id('rad_pemeriksaan_rinc', $dr['pemeriksaanrinc_id']);
      }
    }
  }

  public function radiologi_delete($pemeriksaan_id='', $reg_id='', $pasien_id='', $lokasi_id='')
  {
    trash('rad_pemeriksaan_rinc', array('pemeriksaan_id' => $pemeriksaan_id));
    trash('rad_pemeriksaan', array(
      'pemeriksaan_id' => $pemeriksaan_id,
      'src_reg_id' => $reg_id,
      'pasien_id' => $pasien_id,
      'src_lokasi_id' => $lokasi_id,
    ));

    $this->db
      ->where('pemeriksaan_id', $pemeriksaan_id)
      ->delete('rad_pemeriksaan_rinc');
    $this->db
      ->where('pemeriksaan_id', $pemeriksaan_id)
      ->where('src_reg_id', $reg_id)
      ->where('pasien_id', $pasien_id)
      ->where('src_lokasi_id', $lokasi_id)
      ->delete('rad_pemeriksaan');
  }

  public function radiologi_get($pemeriksaan_id='')
  {
    $sql = "SELECT 
            a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
          FROM rad_pemeriksaan a
          LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
          LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
          WHERE pemeriksaan_id=?";
    $query = $this->db->query($sql, array($pemeriksaan_id));
    return $query->row_array();
  }

  public function radiologi_pemeriksaan_data($reg_id, $pemeriksaan_id)
  {
    $query = $this->db->query(
      "SELECT b.parent_id FROM rad_pemeriksaan_rinc a
      JOIN mst_item_rad b ON a.itemrad_id = b.itemrad_id
      GROUP BY b.parent_id
      ORDER BY b.parent_id"
    )->result_array();
    $res = null;
    if ($query != null) {
      $res = array();
      foreach ($query as $k => $v) {
        $res[$k] = $this->db->query(
          "SELECT itemrad_nm FROM mst_item_rad WHERE itemrad_id='".$v['parent_id']."'"
        )->row_array();
        $res[$k]['rinc'] = $this->db->query(
          "SELECT a.*, b.itemrad_nm FROM rad_pemeriksaan_rinc a
          JOIN mst_item_rad b ON a.itemrad_id = b.itemrad_id
          WHERE b.parent_id = '".$v['parent_id']."' AND a.pemeriksaan_id = '".$pemeriksaan_id."'
          ORDER BY a.itemrad_id ASC"
        )->result_array();
      }
    }
    return $res;
  }

  public function radiologi_tarif_tindakan_data($reg_id='', $pemeriksaan_id='')
  {
    $sql = "SELECT a.*, b.pegawai_nm 
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            WHERE a.is_deleted=0 AND a.lokasi_id = '03.03'
            AND a.reg_id=? AND a.pemeriksaan_id=?";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function radiologi_bhp_data($reg_id='', $pemeriksaan_id='')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_bhp a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.03'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function radiologi_alkes_data($reg_id='', $pemeriksaan_id='')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_alkes a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.03'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  //Bedah Sentral -------------------------------------------------------------------------------------
  public function bedah_sentral_data($reg_id='', $pasien_id='', $lokasi_id='')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM ibs_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE src_reg_id=? AND pasien_id=? AND src_lokasi_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id,$pasien_id,$lokasi_id));
    return $query->result_array();
  }

  public function bedah_sentral_save($reg_id = null, $id = null)
  {
    $d = $this->input->post();
    $data = $d;
    
    unset($data['checkitem']);
    unset($data['bedah_sentral_table_length']);

    $data['tgl_order'] = to_date($data['tgl_order'],'-','full_date');
    if ($id == null) {
      $data['pemeriksaan_id'] = get_id('ibs_pemeriksaan');
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('ibs_pemeriksaan', $data);
      update_id('ibs_pemeriksaan', $data['pemeriksaan_id']);
    }else{
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('pemeriksaan_id', $id)->update('ibs_pemeriksaan', $data);
    }
  }

  public function bedah_sentral_delete($pemeriksaan_id='', $reg_id='', $pasien_id='', $lokasi_id='')
  {
    trash('ibs_pemeriksaan', array(
      'pemeriksaan_id' => $pemeriksaan_id,
      'src_reg_id' => $reg_id,
      'pasien_id' => $pasien_id,
      'src_lokasi_id' => $lokasi_id,
    ));

    $this->db
      ->where('pemeriksaan_id', $pemeriksaan_id)
      ->where('src_reg_id', $reg_id)
      ->where('pasien_id', $pasien_id)
      ->where('src_lokasi_id', $lokasi_id)
      ->delete('ibs_pemeriksaan');
  }

  public function bedah_sentral_get($pemeriksaan_id='')
  {
    $sql = "SELECT 
            a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
          FROM ibs_pemeriksaan a
          LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
          LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
          WHERE pemeriksaan_id=?";
    $query = $this->db->query($sql, array($pemeriksaan_id));
    return $query->row_array();
  }

  public function bedah_sentral_tindakan_data($reg_id='', $pemeriksaan_id='')
  {
    $sql = "SELECT 
              a.*, 
              b.pegawai_nm AS pegawai_nm_1, 
              c.pegawai_nm AS pegawai_nm_2, 
              d.pegawai_nm AS pegawai_nm_3, 
              e.pegawai_nm AS pegawai_nm_4, 
              f.pegawai_nm AS pegawai_nm_5, 
              g.kelas_nm
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            LEFT JOIN mst_pegawai c ON a.petugas_id_2 = c.pegawai_id
            LEFT JOIN mst_pegawai d ON a.petugas_id_3 = d.pegawai_id
            LEFT JOIN mst_pegawai e ON a.petugas_id_4 = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_5 = f.pegawai_id
            LEFT JOIN mst_kelas g ON a.kelas_id = g.kelas_id 
            WHERE a.is_deleted = 0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id='03.04' 
            ORDER BY a.tindakan_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function bedah_sentral_bhp_data($reg_id='', $pemeriksaan_id='')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_bhp a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.04'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function bedah_sentral_alkes_data($reg_id='', $pemeriksaan_id='')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_alkes a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.04'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  //VK -------------------------------------------------------------------------------------
  public function vk_data($reg_id='', $pasien_id='', $lokasi_id='')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM vk_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE src_reg_id=? AND pasien_id=? AND src_lokasi_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id,$pasien_id,$lokasi_id));
    return $query->result_array();
  }

  public function vk_save($reg_id = null, $id = null)
  {
    $d = $this->input->post();
    $data = $d;
    
    unset($data['checkitem']);
    unset($data['vk_table_length']);

    $data['tgl_order'] = to_date($data['tgl_order'],'-','full_date');
    if ($id == null) {
      $data['pemeriksaan_id'] = get_id('vk_pemeriksaan');
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('vk_pemeriksaan', $data);
      update_id('vk_pemeriksaan', $data['pemeriksaan_id']);
    }else{
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('pemeriksaan_id', $id)->update('vk_pemeriksaan', $data);
    }
  }

  public function vk_delete($pemeriksaan_id='', $reg_id='', $pasien_id='', $lokasi_id='')
  {
    trash('vk_pemeriksaan', array(
      'pemeriksaan_id' => $pemeriksaan_id,
      'src_reg_id' => $reg_id,
      'pasien_id' => $pasien_id,
      'src_lokasi_id' => $lokasi_id,
    ));

    $this->db
      ->where('pemeriksaan_id', $pemeriksaan_id)
      ->where('src_reg_id', $reg_id)
      ->where('pasien_id', $pasien_id)
      ->where('src_lokasi_id', $lokasi_id)
      ->delete('vk_pemeriksaan');
  }

  public function vk_get($pemeriksaan_id='')
  {
    $sql = "SELECT 
            a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
          FROM vk_pemeriksaan a
          LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
          LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
          WHERE pemeriksaan_id=?";
    $query = $this->db->query($sql, array($pemeriksaan_id));
    return $query->row_array();
  }

  public function vk_tindakan_data($reg_id='', $pemeriksaan_id='')
  {
    $sql = "SELECT 
              a.*, 
              b.pegawai_nm AS pegawai_nm_1, 
              c.pegawai_nm AS pegawai_nm_2, 
              d.pegawai_nm AS pegawai_nm_3, 
              e.pegawai_nm AS pegawai_nm_4, 
              f.pegawai_nm AS pegawai_nm_5, 
              g.kelas_nm
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            LEFT JOIN mst_pegawai c ON a.petugas_id_2 = c.pegawai_id
            LEFT JOIN mst_pegawai d ON a.petugas_id_3 = d.pegawai_id
            LEFT JOIN mst_pegawai e ON a.petugas_id_4 = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_5 = f.pegawai_id
            LEFT JOIN mst_kelas g ON a.kelas_id = g.kelas_id 
            WHERE a.is_deleted = 0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.05' 
            ORDER BY a.tindakan_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function vk_bhp_data($reg_id='', $pemeriksaan_id='')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_bhp a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.05'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function vk_alkes_data($reg_id='', $pemeriksaan_id='')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_alkes a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.05'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  //ICU -------------------------------------------------------------------------------------
  public function icu_data($reg_id='', $pasien_id='', $lokasi_id='')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM icu_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE src_reg_id=? AND pasien_id=? AND src_lokasi_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id,$pasien_id,$lokasi_id));
    return $query->result_array();
  }

  public function icu_save($reg_id = null, $id = null)
  {
    $d = $this->input->post();
    $data = $d;
    
    unset($data['checkitem']);
    unset($data['icu_table_length']);

    $data['tgl_order'] = to_date($data['tgl_order'],'-','full_date');
    if ($id == null) {
      $data['pemeriksaan_id'] = get_id('icu_pemeriksaan');
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('icu_pemeriksaan', $data);
      update_id('icu_pemeriksaan', $data['pemeriksaan_id']);
    }else{
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('pemeriksaan_id', $id)->update('icu_pemeriksaan', $data);
    }
  }

  public function icu_delete($pemeriksaan_id='', $reg_id='', $pasien_id='', $lokasi_id='')
  {
    trash('icu_pemeriksaan', array(
      'pemeriksaan_id' => $pemeriksaan_id,
      'src_reg_id' => $reg_id,
      'pasien_id' => $pasien_id,
      'src_lokasi_id' => $lokasi_id,
    ));

    $this->db
      ->where('pemeriksaan_id', $pemeriksaan_id)
      ->where('src_reg_id', $reg_id)
      ->where('pasien_id', $pasien_id)
      ->where('src_lokasi_id', $lokasi_id)
      ->delete('icu_pemeriksaan');
  }

  public function icu_get($pemeriksaan_id='')
  {
    $sql = "SELECT 
            a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
          FROM icu_pemeriksaan a
          LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
          LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
          WHERE pemeriksaan_id=?";
    $query = $this->db->query($sql, array($pemeriksaan_id));
    return $query->row_array();
  }

  public function icu_tindakan_data($reg_id='', $pemeriksaan_id='')
  {
    $sql = "SELECT 
              a.*, 
              b.pegawai_nm AS pegawai_nm_1, 
              c.pegawai_nm AS pegawai_nm_2, 
              d.pegawai_nm AS pegawai_nm_3, 
              e.pegawai_nm AS pegawai_nm_4, 
              f.pegawai_nm AS pegawai_nm_5, 
              g.kelas_nm
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            LEFT JOIN mst_pegawai c ON a.petugas_id_2 = c.pegawai_id
            LEFT JOIN mst_pegawai d ON a.petugas_id_3 = d.pegawai_id
            LEFT JOIN mst_pegawai e ON a.petugas_id_4 = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_5 = f.pegawai_id
            LEFT JOIN mst_kelas g ON a.kelas_id = g.kelas_id 
            WHERE a.is_deleted = 0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.06'
            ORDER BY a.tindakan_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function icu_bhp_data($reg_id='', $pemeriksaan_id='')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_bhp a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.06'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function icu_alkes_data($reg_id='', $pemeriksaan_id='')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_alkes a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.06'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  //Hemodialisa -------------------------------------------------------------------------------------
  public function hemodialisa_data($reg_id='', $pasien_id='', $lokasi_id='')
  {
    $sql = "SELECT 
              a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
            FROM hd_pemeriksaan a
            LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
            LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            WHERE src_reg_id=? AND pasien_id=? AND src_lokasi_id=?
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id,$pasien_id,$lokasi_id));
    return $query->result_array();
  }

  public function hemodialisa_save($reg_id = null, $id = null)
  {
    $d = $this->input->post();
    $data = $d;
    
    unset($data['checkitem']);
    unset($data['hemodialisa_table_length']);

    $data['tgl_order'] = to_date($data['tgl_order'],'-','full_date');
    if ($id == null) {
      $data['pemeriksaan_id'] = get_id('hd_pemeriksaan');
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('hd_pemeriksaan', $data);
      update_id('hd_pemeriksaan', $data['pemeriksaan_id']);
    }else{
      $data['user_cd'] = $this->session->userdata('sess_user_cd');
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('pemeriksaan_id', $id)->update('hd_pemeriksaan', $data);
    }
  }

  public function hemodialisa_delete($pemeriksaan_id='', $reg_id='', $pasien_id='', $lokasi_id='')
  {
    trash('hd_pemeriksaan', array(
      'pemeriksaan_id' => $pemeriksaan_id,
      'src_reg_id' => $reg_id,
      'pasien_id' => $pasien_id,
      'src_lokasi_id' => $lokasi_id,
    ));

    $this->db
      ->where('pemeriksaan_id', $pemeriksaan_id)
      ->where('src_reg_id', $reg_id)
      ->where('pasien_id', $pasien_id)
      ->where('src_lokasi_id', $lokasi_id)
      ->delete('hd_pemeriksaan');
  }

  public function hemodialisa_get($pemeriksaan_id='')
  {
    $sql = "SELECT 
            a.*, b.pegawai_nm as dokter_nm,c.lokasi_nm
          FROM hd_pemeriksaan a
          LEFT JOIN mst_pegawai b ON a.dokterpengirim_id = b.pegawai_id 
          LEFT JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
          WHERE pemeriksaan_id=?";
    $query = $this->db->query($sql, array($pemeriksaan_id));
    return $query->row_array();
  }

  public function hemodialisa_tindakan_data($reg_id='', $pemeriksaan_id='')
  {
    $sql = "SELECT 
              a.*, 
              b.pegawai_nm AS pegawai_nm_1, 
              c.pegawai_nm AS pegawai_nm_2, 
              d.pegawai_nm AS pegawai_nm_3, 
              e.pegawai_nm AS pegawai_nm_4, 
              f.pegawai_nm AS pegawai_nm_5, 
              g.kelas_nm
            FROM dat_tindakan a
            LEFT JOIN mst_pegawai b ON a.petugas_id = b.pegawai_id
            LEFT JOIN mst_pegawai c ON a.petugas_id_2 = c.pegawai_id
            LEFT JOIN mst_pegawai d ON a.petugas_id_3 = d.pegawai_id
            LEFT JOIN mst_pegawai e ON a.petugas_id_4 = e.pegawai_id
            LEFT JOIN mst_pegawai f ON a.petugas_id_5 = f.pegawai_id
            LEFT JOIN mst_kelas g ON a.kelas_id = g.kelas_id 
            WHERE a.is_deleted = 0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.07'
            ORDER BY a.tindakan_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function hemodialisa_bhp_data($reg_id='', $pemeriksaan_id='')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_bhp a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.07'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  public function hemodialisa_alkes_data($reg_id='', $pemeriksaan_id='')
  {
    $sql = "SELECT 
              a.*, b.obat_nm 
            FROM dat_alkes a
            LEFT JOIN mst_obat b ON a.barang_id=b.obat_id 
            WHERE a.is_deleted=0 AND a.reg_id=? AND a.pemeriksaan_id=? AND a.lokasi_id = '03.07'
            ORDER BY a.reg_id";
    $query = $this->db->query($sql, array($reg_id, $pemeriksaan_id));
    return $query->result_array();
  }

  // Tindak Lanjut
  public function save_rs_rujukan()
  {
    $data = html_escape($this->input->post());
    $data['rsrujukan_id'] = $data['rsrujukan_kode'];
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['created_by'] = $this->session->userdata('sess_user_realname');
    unset($data['rsrujukan_kode']);
    $this->db->insert('mst_rs_rujukan', $data);
  }

  public function save_tindak_lanjut()
  {
    $data = $this->input->post();
    if ($data['tindaklanjut_cd'] == '01') {
      if ($data['lokasi_jenisreg_1'] !='') {
        $data_tindak_lanjut['lokasi_id'] = $data['lokasi_jenisreg_1'];
        $data_tindak_lanjut['subtindaklanjut_cd'] = $data['subtindaklanjut_cd'];
      }else if ($data['lokasi_jenisreg_2'] !='') {
        $data_tindak_lanjut['lokasi_id'] = $data['lokasi_jenisreg_2'];
        $data_tindak_lanjut['subtindaklanjut_cd'] = '';
      }
    }elseif ($data['tindaklanjut_cd'] == '02') {
      if ($data['lokasi_jenisreg_1'] !='') {
        $data_tindak_lanjut['lokasi_id'] = $data['lokasi_jenisreg_1'];
        $data_tindak_lanjut['subtindaklanjut_cd'] = $data['subtindaklanjut_cd'];
      }else if ($data['lokasi_jenisreg_2'] !='') {
        $data_tindak_lanjut['lokasi_id'] = $data['lokasi_jenisreg_2'];
        $data_tindak_lanjut['subtindaklanjut_cd'] = '';
      }
    }elseif ($data['tindaklanjut_cd'] == '03') {
      $data_tindak_lanjut['lokasi_id'] = '';
      $data_tindak_lanjut['rsrujukan_id'] = $data['rsrujukan_id'];
    }else{
      $data_tindak_lanjut['lokasi_id'] = '';
      $data_tindak_lanjut['subtindaklanjut_cd'] = '';
      $data_tindak_lanjut['rsrujukan_id'] = '';
    }
    $data_tindak_lanjut['reg_id'] = $data['reg_id'];
    $data_tindak_lanjut['pasien_id'] = $data['pasien_id'];
    $data_tindak_lanjut['tindaklanjut_cd'] = $data['tindaklanjut_cd'];
    $data_tindak_lanjut['catatan_tindaklanjut'] = $data['catatan_tindaklanjut'];
    $data_tindak_lanjut['user_cd'] = $this->session->userdata('sess_user_cd');
    $data_tindak_lanjut['tgl_tindaklanjut'] = date('Y-m-d H:i:s');
    // unset data
    unset($data['lokasi_jenisreg_1'], $data['lokasi_jenisreg_2']);
    $cek = $this->db->where('reg_id', $data['reg_id'])->where('pasien_id', $data['pasien_id'])->get('reg_pasien_tindaklanjut')->row_array();
    if ($cek == null) {
      $data_tindak_lanjut['created_at'] = date('Y-m-d H:i:s');
      $data_tindak_lanjut['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('reg_pasien_tindaklanjut', $data_tindak_lanjut);
    }else{
      $data_tindak_lanjut['updated_at'] = date('Y-m-d H:i:s');
      $data_tindak_lanjut['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('reg_id', $data['reg_id'])->where('pasien_id', $data['pasien_id'])->update('reg_pasien_tindaklanjut', $data_tindak_lanjut);
    }
  }

  public function tindak_lanjut_data($pasien_id='', $reg_id='')
  {
    $sql = "SELECT 
              a.* 
            FROM reg_pasien_tindaklanjut a
            WHERE a.is_deleted=0 AND a.pasien_id=? AND a.reg_id=?";
    $query = $this->db->query($sql, array($pasien_id, $reg_id));
    return $query->row_array();
  }

  public function save_status_pulang()
  {
    $data = $this->input->post();

    // update reg_pasien
    if ($data['pulang_st'] == '1') {
      $data_reg_pasien['periksa_st'] = 2;
      $data_reg_pasien['pulang_st'] = 1;
      $data_reg_pasien['tgl_pulang'] = to_date($data['tgl_pulang'],'-','full_date');
    }
    $data_reg_pasien['tindaklanjut_cd'] = $data['tindaklanjut_cd'];
    $data_reg_pasien['updated_at'] = date('Y-m-d H:i:s');
    $data_reg_pasien['updated_by'] = $this->session->userdata('sess_user_realname');
    $this->db->where('reg_id', $data['reg_id'])->where('pasien_id', $data['pasien_id'])->update('reg_pasien', $data_reg_pasien);

    if ($data['pulang_st'] == '1') {
      $data_status_pulang['reg_id'] = $data['reg_id'];
      $data_status_pulang['pasien_id'] = $data['pasien_id'];
      $data_status_pulang['tgl_pulang'] = to_date($data['tgl_pulang'],'-','full_date');
      $data_status_pulang['tgl_kontrol'] = to_date($data['tgl_kontrol']);
      $data_status_pulang['keadaan_cd'] = $data['keadaan_cd'];
      $data_status_pulang['carapulang_cd'] = $data['carapulang_cd'];
      $data_status_pulang['catatan_dokter'] = $data['catatan_dokter'];
      $data_status_pulang['catatan_keperawatan'] = $data['catatan_keperawatan'];
      $data_status_pulang['catatan_farmasi'] = $data['catatan_farmasi'];
      $data_status_pulang['user_cd'] = $this->session->userdata('sess_user_cd');
      //
      $cek = $this->db->where('reg_id', $data['reg_id'])->where('pasien_id', $data['pasien_id'])->get('reg_pasien_pulang')->row_array();
      if ($cek == null) {
        $data_status_pulang['created_at'] = date('Y-m-d H:i:s');
        $data_status_pulang['created_by'] = $this->session->userdata('sess_user_realname');
        $this->db->insert('reg_pasien_pulang', $data_status_pulang);
      }else{
        $data_status_pulang['updated_at'] = date('Y-m-d H:i:s');
        $data_status_pulang['updated_by'] = $this->session->userdata('sess_user_realname');
        $this->db->where('reg_id', $data['reg_id'])->where('pasien_id', $data['pasien_id'])->update('reg_pasien_pulang', $data_status_pulang);
      }
    }
  }

  public function status_pulang_data($pasien_id='', $reg_id='')
  {
    $sql = "SELECT 
              a.* 
            FROM reg_pasien_pulang a
            WHERE a.is_deleted=0 AND a.pasien_id=? AND a.reg_id=?";
    $query = $this->db->query($sql, array($pasien_id, $reg_id));
    $row = $query->row_array();
    $row['tgl_pulang'] = to_date($row['tgl_pulang'], '-', 'full_date');
    $row['tgl_kontrol'] = to_date($row['tgl_kontrol']);
    return $row;
  }

  public function reg_pasien_data_pulang_st($pasien_id='', $reg_id='')
  {
    $sql = "SELECT 
              a.pulang_st 
            FROM reg_pasien a
            WHERE a.is_deleted=0 AND a.pasien_id=? AND a.reg_id=?";
    $query = $this->db->query($sql, array($pasien_id, $reg_id));
    return $query->row_array();
  }

}