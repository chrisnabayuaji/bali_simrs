<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_reg_ranap_kunjungan extends CI_Model {

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 AND a.is_ranap = 1 ";
    // if (@$cookie['search']['tgl_order_from'] != '' && @$cookie['search']['tgl_order_to']) {
    //   $where .= "AND (DATE(a.tgl_order) BETWEEN '".to_date(@$cookie['search']['tgl_order_from'])."' AND '".to_date(@$cookie['search']['tgl_order_to'])."')";
    // }else{
    //   $where .= "AND DATE(a.tgl_order) = '".date('Y-m-d')."'";
    // }
    // if (@$cookie['search']['no_rm_nm'] != '') {
    //   $where .= "AND (b.pasien_id LIKE '%".$this->db->escape_like_str($cookie['search']['no_rm_nm'])."%' OR b.pasien_nm LIKE '%".$this->db->escape_like_str($cookie['search']['no_rm_nm'])."%' ) ";
    // }
    // if (@$cookie['search']['periksa_st_lab'] != '') {
    //   $where .= "AND a.periksa_st = '".$this->db->escape_like_str($cookie['search']['periksa_st_lab'])."' ";
    // }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
              a.*,
              b.jenispasien_nm,
              c.lokasi_nm AS lokasi_asal,
              d.lokasi_nm AS lokasi_bangsal,
              e.kamar_nm
            FROM reg_pasien a
            LEFT JOIN mst_jenis_pasien b ON a.jenispasien_id = b.jenispasien_id
            JOIN mst_lokasi c ON a.src_lokasi_id = c.lokasi_id
            JOIN mst_lokasi d ON a.lokasi_id = d.lokasi_id
            LEFT JOIN mst_kamar e ON a.kamar_id = e.kamar_id
            $where
            ORDER BY "
              .$cookie['order']['field']." ".$cookie['order']['type'].
            " LIMIT ".$cookie['cur_page'].",".$cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM reg_pasien a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT a.*
            FROM reg_pasien a
            $where";
    $query = $this->db->query($sql);
    return $query->num_rows();
  }
  
  public function total_verifikasi()
  {
    $sql = "SELECT 
              a.reg_id, a.pasien_id, a.pasien_nm, a.alamat, a.sex_cd, a.provinsi, a.kabupaten, a.kecamatan, a.kelurahan,
              b.tgl_tindaklanjut,
              c.jenispasien_nm,
              d.lokasi_nm as lokasi_asal, d.lokasi_id as lokasi_asal_id,
              e.lokasi_nm as lokasi_bangsal, e.lokasi_id as lokasi_bangsal_id
            FROM reg_pasien a
            INNER JOIN reg_pasien_tindaklanjut b ON a.reg_id = b.reg_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
            LEFT JOIN mst_lokasi d ON a.lokasi_id = d.lokasi_id
            LEFT JOIN mst_lokasi e ON b.lokasi_id = e.lokasi_id 
            WHERE b.is_deleted = 0 AND a.tindaklanjut_cd = '02' AND b.st_verif_ranap IS NULL";
    $query = $this->db->query($sql);
    return $query->num_rows();
  }

  public function get_data($id)
  {
    $sql = "SELECT 
              a.*,
              a.src_lokasi_id  AS lokasi_asal_id,
              b.lokasi_id AS lokasi_bangsal_id, b.kamar_id
            FROM reg_pasien a
            JOIN reg_pasien_kamar b ON b.reg_id = a.reg_id
            WHERE a.reg_id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    $d = $data;
    $d['provinsi'] = get_wilayah($d['wilayah_prop'], 'name');
    $d['kabupaten'] = get_wilayah($d['wilayah_kab'], 'name');
    $d['kecamatan'] = get_wilayah($d['wilayah_kec'], 'name');
    $d['kelurahan'] = get_wilayah($d['wilayah_kel'], 'name');
    $d['wilayah_id'] = get_wilayah($d['wilayah_kel'], 'id');
    $d['tgl_lahir'] = to_date($d['tgl_lahir']);
    $d['tgl_registrasi'] = to_date($d['tgl_registrasi'],'-','full_date');
    $d['kelompokumur_id'] = get_kelompokumur($d['tgl_lahir']);
    unset($d['wilayah_prop'],$d['wilayah_kab'],$d['wilayah_kec'],$d['wilayah_kel']);
    //tindak_lanjut
    $dt = array(
      'lokasi_id' => $data['lokasi_bangsal_id']
    );
    $this->db->where('reg_id', $data['src_reg_id'])->update('reg_pasien_tindaklanjut', $dt);
    //insert table reg pasien
    $d['src_lokasi_id'] = $d['lokasi_asal_id'];
    $d['lokasi_id'] = $d['lokasi_bangsal_id'];
    $d['is_ranap'] = 1;
    unset($d['lokasi_asal_id'], $d['lokasi_bangsal_id'], $d['keterangan'], $d['keterangan_kamar']);
    $this->db->where('reg_id', $id)->update('reg_pasien', $d);
    //3. insert ke table reg_pasien_kamar
    $kamar = $this->m_kamar->by_field('kamar_id', $data['kamar_id'], 'row');
    $d2 = array(
      'pasien_id' => $data['pasien_id'],
      'kelas_id' => $data['kelas_id'],
      'lokasi_id' => $data['lokasi_bangsal_id'],
      'kamar_id' => $data['kamar_id'],
      'tgl_masuk' => $d['tgl_registrasi'],
      'nom_tarif' => $kamar['nom_tarif'],
      'keterangan_kamar' => $data['keterangan_kamar']
    );
    $this->db->where('reg_id', $id)->update('reg_pasien_kamar', $d2);
  }

  public function delete($id)
  {
    $this->db->where('reg_id', $id)->delete('reg_pasien');
    $this->db->where('reg_id', $id)->delete('reg_pasien_kamar');
  }

  // Pindah Bangsal
  function get_data_pindah_bangsal($reg_id=null, $pasien_id=null, $kelas_id=null, $lokasi_id=null, $kamar_id=null) {
    $sql = "SELECT 
              a.*, b.lokasi_nm, c.kamar_nm
            FROM reg_pasien_kamar a 
            LEFT JOIN mst_lokasi b ON a.lokasi_id=b.lokasi_id
            LEFT JOIN mst_kamar c ON a.kamar_id=c.kamar_id
            WHERE a.reg_id=? AND a.pasien_id=? AND a.kelas_id=? AND a.lokasi_id=? AND a.kamar_id=?";
    $query = $this->db->query($sql, array($reg_id, $pasien_id, $kelas_id, $lokasi_id, $kamar_id));
    $row = $query->row_array();
    return $row;
  }

  public function save_pindah_bangsal($reg_id = null)
  {
    $data = html_escape($this->input->post());
    // Update reg_pasien_kamar
    $get_data = $this->db->where('reg_id', $reg_id)->where('regkamar_id', $data['regkamar_id'])->get('reg_pasien_kamar')->row_array();

    $update_reg_pasien_kamar['tgl_keluar'] = to_date($data['tgl_pindah'],'-','full_date');
    $update_reg_pasien_kamar['jml_hari'] = selisih_hari(to_date($update_reg_pasien_kamar['tgl_keluar'],'','date'), to_date($get_data['tgl_masuk'],'','date'));
    $update_reg_pasien_kamar['jml_tagihan'] = $get_data['nom_tarif'] * $update_reg_pasien_kamar['jml_hari'];
    $update_reg_pasien_kamar['user_cd'] = $this->session->userdata('sess_user_cd');
    $update_reg_pasien_kamar['updated_at'] = date('Y-m-d H:i:s');
    $update_reg_pasien_kamar['updated_by'] = $this->session->userdata('sess_user_realname');
    $this->db->where('regkamar_id', $data['regkamar_id'])->update('reg_pasien_kamar', $update_reg_pasien_kamar);

    // Insert reg_pasien_kamar
    $get_kamar = $this->m_kamar->by_field('kamar_id', $data['kamar_id'], 'row');
    $insert_reg_pasien_kamar['regkamar_id'] = get_id('reg_pasien_kamar');
    $insert_reg_pasien_kamar['reg_id'] = $data['reg_id'];
    $insert_reg_pasien_kamar['pasien_id'] = $data['pasien_id'];
    $insert_reg_pasien_kamar['kelas_id'] = $data['kelas_id'];
    $insert_reg_pasien_kamar['tarif_id'] = $get_kamar['tarif_id'];
    $insert_reg_pasien_kamar['lokasi_id'] = $data['lokasi_id'];
    $insert_reg_pasien_kamar['kamar_id'] = $data['kamar_id'];
    $insert_reg_pasien_kamar['tgl_masuk'] = to_date($data['tgl_pindah'],'-','full_date');
    $insert_reg_pasien_kamar['tgl_keluar'] = NULL;
    $insert_reg_pasien_kamar['jml_hari'] = NULL;
    $insert_reg_pasien_kamar['nom_tarif'] = $data['nom_tarif'];
    $insert_reg_pasien_kamar['jml_tagihan'] = NULL;
    $insert_reg_pasien_kamar['keterangan_kamar'] = $data['keterangan_kamar'];
    $insert_reg_pasien_kamar['user_cd'] = $this->session->userdata('sess_user_cd');
    $insert_reg_pasien_kamar['created_at'] = date('Y-m-d H:i:s');
    $insert_reg_pasien_kamar['created_by'] = $this->session->userdata('sess_user_realname');
    $this->db->insert('reg_pasien_kamar', $insert_reg_pasien_kamar);
    update_id('reg_pasien_kamar', $insert_reg_pasien_kamar['regkamar_id']);

    // Update reg_pasien
    $update_reg_pasien['lokasi_id'] = $data['lokasi_id'];
    $update_reg_pasien['kelas_id'] = $data['kelas_id'];
    $update_reg_pasien['kamar_id'] = $data['kamar_id'];
    $update_reg_pasien['updated_at'] = date('Y-m-d H:i:s');
    $update_reg_pasien['updated_by'] = $this->session->userdata('sess_user_realname');
    $this->db->where('reg_id', $data['reg_id'])->update('reg_pasien', $update_reg_pasien);

    // 4. update ke table mst_kamar
    // update mst_kamar set jml_bed_kosong=jml_bed_kosong + 1, jml_bed_terpakai=jml_bed_terpakai-1 where kamar_id=$kamar_sebelumnya
    // $kamar1 = $this->db->where('regkamar_id', $data['regkamar_id'])->get('reg_pasien_kamar')->row_array();
    // $d1 = array(
    //   'jml_bed_kosong' => $kamar1['jml_bed_kosong'] + 1,
    //   'jml_bed_terpakai' => $kamar1['jml_bed_terpakai'] - 1
    // );
    // // update mst_kamar set jml_bed_kosong=jml_bed_kosong-1, jml_bed_terpakai=jml_bed_terpakai+1 where kamar_id=$kamar_setelahnya(baru)
    // $kamar2 = $this->db->where('kamar_id', $data['kamar_id'])->get('mst_kamar')->row_array();
    // $d2 = array(
    //   'jml_bed_kosong' => $kamar2['jml_bed_kosong'] - 1,
    //   'jml_bed_terpakai' => $kamar2['jml_bed_terpakai'] + 1
    // );
    // $this->db->where('kamar_id', $kamar2['kamar_id'])->update('mst_kamar', $d1);

    $sql1 = "UPDATE mst_kamar SET jml_bed_kosong=ifnull(jml_bed_kosong,0) + 1, jml_bed_terpakai=ifnull(jml_bed_terpakai,0) - 1 
             WHERE kamar_id='".$get_data['kamar_id']."'";               
    $this->db->query($sql1);

    $sql2 = "UPDATE mst_kamar SET jml_bed_kosong=ifnull(jml_bed_kosong,0)-1, jml_bed_terpakai=ifnull(jml_bed_terpakai,0)+1 
             WHERE kamar_id='".$data['kamar_id']."'";               
    $this->db->query($sql2);
  }

  // Riwayat Bangsal
  public function list_riwayat_bangsal($reg_id=null)
  {
    $sql = "SELECT a.*, b.lokasi_nm, c.kelas_nm, d.kamar_nm  
            FROM reg_pasien_kamar a 
            LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
            LEFT JOIN mst_kelas c ON a.kelas_id = c.kelas_id
            LEFT JOIN mst_kamar d ON a.kamar_id = d.kamar_id
            WHERE a.is_deleted = 0 AND a.reg_id=?
            ORDER BY regkamar_id ASC";
    $query = $this->db->query($sql, $reg_id);
    return $query->result_array();
  }
  
}