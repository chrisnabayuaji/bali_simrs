<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_reg_ranap_verifikasi extends CI_Model {

  public function where($cookie)
  {
    $where = "WHERE b.is_deleted = 0 AND a.tindaklanjut_cd = '02' AND b.st_verif_ranap IS NULL ";
    if (@$cookie['search']['tgl_tindaklanjut'] != '' && @$cookie['search']['tgl_tindaklanjut']) {
      $where .= "AND DATE(b.tgl_tindaklanjut) = '".to_date(@$cookie['search']['tgl_tindaklanjut'])."'";
    }else{
      $where .= "AND DATE(b.tgl_tindaklanjut) = '".date('Y-m-d')."'";
    }
    if (@$cookie['search']['no_rm_nm'] != '') {
      $where .= "AND (a.pasien_id LIKE '%".$this->db->escape_like_str($cookie['search']['no_rm_nm'])."%' OR a.pasien_nm LIKE '%".$this->db->escape_like_str($cookie['search']['no_rm_nm'])."%' ) ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
              a.reg_id, a.pasien_id, a.pasien_nm, a.alamat, a.sex_cd, a.provinsi, a.kabupaten, a.kecamatan, a.kelurahan,
              b.tgl_tindaklanjut,
              c.jenispasien_nm,
              d.lokasi_nm as lokasi_asal,
              e.lokasi_nm as lokasi_bangsal
            FROM reg_pasien a
            INNER JOIN reg_pasien_tindaklanjut b ON a.reg_id = b.reg_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
            LEFT JOIN mst_lokasi d ON a.lokasi_id = d.lokasi_id
            LEFT JOIN mst_lokasi e ON b.lokasi_id = e.lokasi_id 
            $where
            ORDER BY "
              .$cookie['order']['field']." ".$cookie['order']['type'].
            " LIMIT ".$cookie['cur_page'].",".$cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT 
              a.reg_id, a.pasien_id, a.pasien_nm, a.alamat, a.sex_cd, a.provinsi, a.kabupaten, a.kecamatan, a.kelurahan,
              b.tgl_tindaklanjut,
              c.jenispasien_nm,
              d.lokasi_nm as lokasi_asal,
              e.lokasi_nm as lokasi_bangsal
            FROM reg_pasien a
            INNER JOIN reg_pasien_tindaklanjut b ON a.reg_id = b.reg_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
            LEFT JOIN mst_lokasi d ON a.lokasi_id = d.lokasi_id
            LEFT JOIN mst_lokasi e ON b.lokasi_id = e.lokasi_id 
            $where";
    $query = $this->db->query($sql);
    return $query->num_rows();
  }

  public function total_verifikasi()
  {
    $sql = "SELECT 
              a.reg_id, a.pasien_id, a.pasien_nm, a.alamat, a.sex_cd, a.provinsi, a.kabupaten, a.kecamatan, a.kelurahan,
              b.tgl_tindaklanjut,
              c.jenispasien_nm,
              d.lokasi_nm as lokasi_asal, d.lokasi_id as lokasi_asal_id,
              e.lokasi_nm as lokasi_bangsal, e.lokasi_id as lokasi_bangsal_id
            FROM reg_pasien a
            INNER JOIN reg_pasien_tindaklanjut b ON a.reg_id = b.reg_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
            LEFT JOIN mst_lokasi d ON a.lokasi_id = d.lokasi_id
            LEFT JOIN mst_lokasi e ON b.lokasi_id = e.lokasi_id 
            WHERE b.is_deleted = 0 AND a.tindaklanjut_cd = '02' AND b.st_verif_ranap IS NULL";
    $query = $this->db->query($sql);
    return $query->num_rows();
  }
  
  public function get_data($id)
  {
    $sql = "SELECT 
              a.*,
              b.tgl_tindaklanjut,
              c.jenispasien_nm,
              d.lokasi_nm as lokasi_asal, d.lokasi_id as lokasi_asal_id,
              e.lokasi_nm as lokasi_bangsal, e.lokasi_id as lokasi_bangsal_id
            FROM reg_pasien a
            INNER JOIN reg_pasien_tindaklanjut b ON a.reg_id = b.reg_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
            LEFT JOIN mst_lokasi d ON a.lokasi_id = d.lokasi_id
            LEFT JOIN mst_lokasi e ON b.lokasi_id = e.lokasi_id 
            WHERE b.reg_id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->row_array();
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    $d = $data;
    $d['provinsi'] = get_wilayah($d['wilayah_prop'], 'name');
    $d['kabupaten'] = get_wilayah($d['wilayah_kab'], 'name');
    $d['kecamatan'] = get_wilayah($d['wilayah_kec'], 'name');
    $d['kelurahan'] = get_wilayah($d['wilayah_kel'], 'name');
    $d['wilayah_id'] = get_wilayah($d['wilayah_kel'], 'id');
    $d['tgl_lahir'] = to_date($d['tgl_lahir']);
    $d['tgl_registrasi'] = to_date($d['tgl_registrasi'],'-','full_date');
    $d['kelompokumur_id'] = get_kelompokumur($d['tgl_lahir']);
    unset($d['wilayah_prop'],$d['wilayah_kab'],$d['wilayah_kec'],$d['wilayah_kel']);
    //1. update table reg_pasien_tindaklanjut
    $this->db->where('reg_id', $id)->update('reg_pasien_tindaklanjut', array('st_verif_ranap' => 1));
    //2. insert table reg pasien
    $d['src_reg_id'] = $id;
    $d['src_lokasi_id'] = $d['lokasi_asal_id'];
    $d['lokasi_id'] = $d['lokasi_bangsal_id'];
    $d['reg_id'] = get_id('reg_pasien');
    $d['is_ranap'] = 1;
    $d['created_at'] = date('Y-m-d H:i:s');
    $d['created_by'] = $this->session->userdata('sess_user_realname');
    unset($d['lokasi_asal_id'], $d['lokasi_bangsal_id'], $d['keterangan'], $d['keterangan_kamar']);
    $this->db->insert('reg_pasien', $d);
    update_id('reg_pasien', $d['reg_id']);
    //3. insert ke table reg_pasien_kamar
    $kamar = $this->m_kamar->by_field('kamar_id', $data['kamar_id'], 'row');
    $d2 = array(
      'regkamar_id' => get_id('reg_pasien_kamar'),
      'reg_id' => $d['reg_id'],
      'pasien_id' => $data['pasien_id'],
      'kelas_id' => $data['kelas_id'],
      'tarif_id' => $kamar['tarif_id'],
      'lokasi_id' => $d['lokasi_id'],
      'kamar_id' => $data['kamar_id'],
      'tgl_masuk' => $d['tgl_registrasi'],
      'nom_tarif' => $kamar['nom_tarif'],
      'keterangan_kamar' => $data['keterangan_kamar']
    );
    $this->db->insert('reg_pasien_kamar', $d2);
    update_id('reg_pasien_kamar', $d2['regkamar_id']);
    // 4. update mst_kamar
    // update mst_kamar set jml_bed_kosong=jml_bed_kosong-1, jml_bed_terpakai=jml_bed_terpakai+1 where kamar_id=$kamar_baru
    $d4 = array(
      'jml_bed_kosong' => $kamar['jml_bed_kosong'] - 1,
      'jml_bed_terpakai' => $kamar['jml_bed_terpakai'] +1
    );
    $this->db->where('kamar_id', $kamar['kamar_id'])->update('mst_kamar', $d4);
  }

}