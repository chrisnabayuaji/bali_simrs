<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_registrasi_offline extends CI_Model {

	public function where($cookie)
	{
		$where = "WHERE a.is_deleted = 0 AND b.jenisreg_st = 1 ";
    if (@$cookie['search']['tgl_registrasi_from'] != '' && @$cookie['search']['tgl_registrasi_to']) {
      $where .= "AND (DATE(a.tgl_registrasi) BETWEEN '".to_date(@$cookie['search']['tgl_registrasi_from'])."' AND '".to_date(@$cookie['search']['tgl_registrasi_to'])."')";
		}else{
      $where .= "AND DATE(a.tgl_registrasi) = '".date('Y-m-d')."'";
    }
    if (@$cookie['search']['no_rm_nm'] != '') {
      $where .= "AND (a.pasien_id LIKE '%".$this->db->escape_like_str($cookie['search']['no_rm_nm'])."%' OR a.pasien_nm LIKE '%".$this->db->escape_like_str($cookie['search']['no_rm_nm'])."%' ) ";
    }
    if (@$cookie['search']['lokasi_id'] != '') {
      $where .= "AND a.lokasi_id = '".$this->db->escape_like_str($cookie['search']['lokasi_id'])."' ";
    }
    if (@$cookie['search']['jenispasien_id'] != '') {
      $where .= "AND a.jenispasien_id = '".$this->db->escape_like_str($cookie['search']['jenispasien_id'])."' ";
    }
    if (@$cookie['search']['periksa_st'] != '') {
      $where .= "AND a.periksa_st = '".$this->db->escape_like_str($cookie['search']['periksa_st'])."' ";
    }
    if (@$cookie['search']['pulang_st'] != '') {
      $where .= "AND a.pulang_st = '".$this->db->escape_like_str($cookie['search']['pulang_st'])."' ";
    }
		return $where;
	}

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT a.*, b.lokasi_nm, c.jenispasien_nm FROM reg_pasien a
      LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
      LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
      $where
      ORDER BY "
        .$cookie['order']['field']." ".$cookie['order']['type'].
      " LIMIT ".$cookie['cur_page'].",".$cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM reg_pasien a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT a.*, b.lokasi_nm, c.jenispasien_nm FROM reg_pasien a
    LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
    LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
    $where";
    $query = $this->db->query($sql);
    return $query->num_rows();
  }

  function list_parameter_group() {
    $sql = "SELECT parameter_group FROM reg_pasien GROUP BY parameter_group ORDER BY parameter_group ASC";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function get_data($id) {
    $sql = "SELECT 
              a.*, b.pegawai_nm, c.jenispasien_nm, d.lokasi_nm 
            FROM reg_pasien a 
            LEFT JOIN mst_pegawai b ON a.dokter_id=b.pegawai_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id=c.jenispasien_id
            LEFT JOIN mst_lokasi d ON a.lokasi_id = d.lokasi_id
            WHERE a.reg_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  function check_nik($nik) {
    $sql = "SELECT 
              a.pasien_id 
            FROM mst_pasien a 
            WHERE a.nik=?";
    $query = $this->db->query($sql, $nik);
    $row = $query->row_array();
    return $row;
  }

  public function save($id = null)
  {
    $data = html_escape($this->input->post());
    $data['provinsi'] = get_wilayah($data['wilayah_prop'], 'name');
    $data['kabupaten'] = get_wilayah($data['wilayah_kab'], 'name');
    $data['kecamatan'] = get_wilayah($data['wilayah_kec'], 'name');
    $data['kelurahan'] = get_wilayah($data['wilayah_kel'], 'name');
    $data['wilayah_id'] = get_wilayah($data['wilayah_kel'], 'id');
    $data['tgl_lahir'] = to_date($data['tgl_lahir']);
    $data['tgl_registrasi'] = to_date($data['tgl_registrasi'],'-','full_date');
    $data['kelompokumur_id'] = get_kelompokumur($data['tgl_lahir']);
    unset($data['wilayah_prop'],$data['wilayah_kab'],$data['wilayah_kec'],$data['wilayah_kel']);
    if ($id == null) {
      $data['reg_id'] = get_id('reg_pasien');
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('reg_pasien', $data);
      update_id('reg_pasien', $data['reg_id']);
      // insert mst_pasien
      if ($data['statuspasien_cd'] == 'B') {
        $this->insert_mst_pasien($data);
      }
      // update reg_pasien_online
      if ($data['regonline_id'] !='') {
        $this->update_reg_pasien_online($data);
      }
      // insert dat_tindakan
      $this->insert_dat_tindakan($data);
    }else{
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('reg_id', $id)->update('reg_pasien', $data);
    }
  }

  public function insert_mst_pasien($data)
  {
    $data_mst_pasien['pasien_id'] = $data['pasien_id'];
    $data_mst_pasien['pasien_nm'] = $data['pasien_nm'];
    $data_mst_pasien['sebutan_cd'] = $data['sebutan_cd'];
    $data_mst_pasien['nik'] = $data['nik'];
    $data_mst_pasien['nama_kk'] = $data['nama_kk'];
    $data_mst_pasien['no_kk'] = $data['no_kk'];
    $data_mst_pasien['alamat'] = $data['alamat'];
    $data_mst_pasien['kode_pos'] = $data['kode_pos'];
    $data_mst_pasien['wilayah_st'] = $data['wilayah_st'];
    $data_mst_pasien['provinsi'] = $data['provinsi'];
    $data_mst_pasien['kabupaten'] = $data['kabupaten'];
    $data_mst_pasien['kecamatan'] = $data['kecamatan'];
    $data_mst_pasien['kelurahan'] = $data['kelurahan'];
    $data_mst_pasien['wilayah_id'] = $data['wilayah_id'];
    $data_mst_pasien['tmp_lahir'] = $data['tmp_lahir'];
    $data_mst_pasien['tgl_lahir'] = $data['tgl_lahir'];
    $data_mst_pasien['sex_cd'] = $data['sex_cd'];
    $data_mst_pasien['goldarah_cd'] = $data['goldarah_cd'];
    $data_mst_pasien['pendidikan_cd'] = $data['pendidikan_cd'];
    $data_mst_pasien['pekerjaan_cd'] = $data['pekerjaan_cd'];
    $data_mst_pasien['agama_cd'] = $data['agama_cd'];
    $data_mst_pasien['telp'] = $data['no_telp'];
    $data_mst_pasien['jenispasien_id'] = $data['jenispasien_id'];
    $data_mst_pasien['no_kartu'] = $data['no_kartu'];
    $data_mst_pasien['pasien_st'] = 1;
    $data_mst_pasien['tgl_catat'] = date('Y-m-d');
    $data_mst_pasien['created_at'] = date('Y-m-d H:i:s');
    $data_mst_pasien['created_by'] = $this->session->userdata('sess_user_realname');
    $this->db->insert('mst_pasien', $data_mst_pasien);
  }

  public function update_reg_pasien_online($data)
  {
    $data_reg_online['is_dilayani'] = 1;
    $data_reg_online['updated_at'] = date('Y-m-d H:i:s');
    $data_reg_online['updated_by'] = $this->session->userdata('sess_user_realname');
    $this->db->where('regonline_id',$data['regonline_id'])->update('reg_pasien_online', $data_reg_online);
  }

  public function insert_dat_tindakan($data)
  {
    $count_admisi_dokter = $this->count_admisi_dokter($data['kelas_id'], $data['lokasi_id'], $data['dokter_id']);
    if ($count_admisi_dokter > 0) {
      $result_data = $this->get_result_data($data['kelas_id'], $data['lokasi_id'], $data['dokter_id']);
    }else{
      $result_data = $this->get_result_data($data['kelas_id'], $data['lokasi_id']);
    }

    foreach ($result_data as $value) {
      $data_dat_tindakan['tindakan_id'] = get_id('dat_tindakan');
      $data_dat_tindakan['reg_id'] = $data['reg_id'];
      $data_dat_tindakan['pasien_id'] = $data['pasien_id'];
      $data_dat_tindakan['lokasi_id'] = $data['lokasi_id'];
      $data_dat_tindakan['kelas_id'] = $data['kelas_id'];
      $data_dat_tindakan['tarif_id'] = $value['tarif_id'];
      $data_dat_tindakan['tarif_nm'] = $value['tarif_nm'];
      $data_dat_tindakan['js'] = $value['js'];
      $data_dat_tindakan['jp'] = $value['jp'];
      $data_dat_tindakan['jb'] = $value['jb'];
      $data_dat_tindakan['nom_tarif'] = $value['nominal'];
      $data_dat_tindakan['qty'] = 1;
      $data_dat_tindakan['jml_awal'] = $value['nominal']*1;
      $data_dat_tindakan['jml_tagihan'] = $value['nominal']*1;
      $data_dat_tindakan['jenistindakan_cd'] = 1;
      $data_dat_tindakan['petugas_id'] = $data['dokter_id'];
      $data_dat_tindakan['tgl_catat'] = date('Y-m-d H:i:s');
      $data_dat_tindakan['created_at'] = date('Y-m-d H:i:s');
      $data_dat_tindakan['created_by'] = $this->session->userdata('sess_user_realname');
      $this->db->insert('dat_tindakan', $data_dat_tindakan);
      update_id('dat_tindakan', $data_dat_tindakan['tindakan_id']);
    }
  }

  public function count_admisi_dokter($kelas_id=null, $lokasi_id=null, $dokter_id=null) {
    $sql = "SELECT 
              COUNT(*) AS count_data
            FROM mst_tarif_admisi 
            WHERE kelas_id='$kelas_id' AND lokasi_id='$lokasi_id' AND dokter_id='$dokter_id'";
    $query = $this->db->query($sql, array($kelas_id, $lokasi_id, $dokter_id));
    $row = $query->row_array();
    return $row['count_data'];
  }

  public function get_result_data($kelas_id=null, $lokasi_id=null, $dokter_id=null) {
    $where = "";
    if ($dokter_id != null) {
      $where .= "AND a.dokter_id='$dokter_id'";
    }
    $sql = "SELECT 
              a.*, b.js, b.jp, b.jb, b.nominal, c.tarif_nm
            FROM mst_tarif_admisi a
            JOIN mst_tarif_kelas b ON a.tarif_id = b.tarif_id AND a.kelas_id = b.kelas_id
            JOIN mst_tarif c ON a.tarif_id = c.tarif_id
            WHERE a.kelas_id=? AND a.lokasi_id=? $where";
    $query = $this->db->query($sql, array($kelas_id, $lokasi_id));
    $result = $query->result_array();
    return $result;
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('sess_user_realname');
    $this->db->where('lokasi_id',$id)->update('reg_pasien', $data);
  }

  public function delete($id, $permanent = false)
  {
    trash('reg_pasien', array('reg_id' => $id));
    if ($permanent) {
      $this->db->where('reg_id', $id)->delete('reg_pasien');
    }else{
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('reg_id', $id)->update('reg_pasien', $data);
    }
  }

  public function online_data()
  {
    $sql = "SELECT * FROM reg_pasien_online";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function online_row($id)
  {
    $sql = "SELECT * FROM reg_pasien_online WHERE regonline_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    // sebutan_cd
    if ($row['sex_cd'] == 'L') {
      $row['sebutan_cd'] = 'Tn';
    }elseif ($row['sex_cd'] == 'P') {
      $row['sebutan_cd'] = 'Ny';
    }
    return $row;
  }

  public function pasien_data()
  {
    $sql = "SELECT * FROM mst_pasien";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function pasien_row($id)
  {
    $sql = "SELECT * FROM mst_pasien WHERE pasien_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    // sebutan_cd
    if (@$row['sebutan_cd'] !='') {
      $row['sebutan_cd'] = $row['sebutan_cd'];
    }else{
      if ($row['sex_cd'] == 'L') {
        $row['sebutan_cd'] = 'Tn';
      }elseif ($row['sex_cd'] == 'P') {
        $row['sebutan_cd'] = 'Ny';
      }
    }
    return $row;
  }

  public function no_rm_row($id)
  {
    $sql = "SELECT * FROM mst_pasien WHERE pasien_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    $row['status_cari'] = (@$row !='') ? '1' : '0';
    $row['statuspasien_cd'] = (@$row['statuspasien_cd'] !='') ? @$row['statuspasien_cd'] : 'B';
    $row['wilayah_st'] = (@$row['wilayah_st'] !='') ? @$row['wilayah_st'] : 'D';
    $row['wilayah_id'] = (@$row['wilayah_id'] !='') ? @$row['wilayah_id'] : '';
    $row['tgl_lahir'] = (@$row['tgl_lahir'] !='') ? @$row['tgl_lahir'] : date('Y-m-d');
    $row['sex_cd'] = (@$row['sex_cd'] !='') ? @$row['sex_cd'] : 'L';
    $row['pendidikan_cd'] = (@$row['pendidikan_cd'] !='') ? @$row['pendidikan_cd'] : '00';
    $row['pekerjaan_cd'] = (@$row['pekerjaan_cd'] !='') ? @$row['pekerjaan_cd'] : '00';
    $row['agama_cd'] = (@$row['agama_cd'] !='') ? @$row['agama_cd'] : '00';
    $row['jenispasien_id'] = (@$row['jenispasien_id'] !='') ? @$row['jenispasien_id'] : '01';
    // sebutan_cd
    if (@$row['sebutan_cd'] !='') {
      $row['sebutan_cd'] = $row['sebutan_cd'];
    }else{
      if ($row['sex_cd'] == 'L') {
        $row['sebutan_cd'] = 'Tn';
      }elseif ($row['sex_cd'] == 'P') {
        $row['sebutan_cd'] = 'Ny';
      }
    }
    return $row;
  }

  public function get_caller()
  {
    $sql = "SELECT a.* FROM lkt_antrian a WHERE a.tgl_antrian='".date('Y-m-d')."' AND a.lokasi_id=?";
    $query = $this->db->query($sql, array('01.01'));
    $row = $query->row_array();
    return $row;
  }
  
}