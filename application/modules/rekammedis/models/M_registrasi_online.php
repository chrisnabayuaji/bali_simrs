<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_registrasi_online extends CI_Model {

	public function where($cookie)
	{
		$where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['tgl_periksa_from'] != '' && @$cookie['search']['tgl_periksa_to']) {
      $where .= "AND (DATE(a.tgl_periksa) BETWEEN '".to_date(@$cookie['search']['tgl_periksa_from'])."' AND '".to_date(@$cookie['search']['tgl_periksa_to'])."')";
		}else{
      $where .= "AND DATE(a.tgl_periksa) = '".date('Y-m-d')."'";
    }
    if (@$cookie['search']['no_rm_nm'] != '') {
      $where .= "AND (a.pasien_id LIKE '%".$this->db->escape_like_str($cookie['search']['no_rm_nm'])."%' OR a.pasien_nm LIKE '%".$this->db->escape_like_str($cookie['search']['no_rm_nm'])."%' ) ";
    }
    if (@$cookie['search']['lokasi_id'] != '') {
      $where .= "AND a.lokasi_id = '".$this->db->escape_like_str($cookie['search']['lokasi_id'])."' ";
    }
    if (@$cookie['search']['jenispasien_id'] != '') {
      $where .= "AND a.jenispasien_id = '".$this->db->escape_like_str($cookie['search']['jenispasien_id'])."' ";
    }
    if (@$cookie['search']['is_have_norm'] != '') {
      if (@$cookie['search']['is_have_norm'] == '1') {
        $where .= "AND a.pasien_id != '' ";
      }elseif (@$cookie['search']['is_have_norm'] == '0') {
        $where .= "AND a.pasien_id = '' ";
      }
    }
    if (@$cookie['search']['is_have_rujukan'] != '') {
      $where .= "AND a.is_have_rujukan = '".$this->db->escape_like_str($cookie['search']['is_have_rujukan'])."' ";
    }
		return $where;
	}

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT 
              a.*, b.lokasi_nm, c.jenispasien_nm 
            FROM reg_pasien_online a
            LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
            $where
            ORDER BY "
              .$cookie['order']['field']." ".$cookie['order']['type'].
            " LIMIT ".$cookie['cur_page'].",".$cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM reg_pasien_online a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM reg_pasien_online a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  public function get_data($id) {
    $sql = "SELECT 
              a.*, b.lokasi_nm, c.jenispasien_nm 
            FROM reg_pasien_online a 
            LEFT JOIN mst_lokasi b ON a.lokasi_id = b.lokasi_id
            LEFT JOIN mst_jenis_pasien c ON a.jenispasien_id = c.jenispasien_id
            WHERE a.regonline_id=?";
    $query = $this->db->query($sql, array($id));
    $row = $query->row_array();
    return $row;
  }

  public function delete($id, $permanent = false)
  {
    trash('reg_pasien_online', array('regonline_id' => $id));
    if ($permanent) {
      $get_data = $this->get_data($id);
      $this->delete_file_process('file_ktp', @$get_data['file_ktp']);
      $this->delete_file_process('file_rujukan', @$get_data['file_rujukan']);
      $this->db->where('regonline_id', $id)->delete('reg_pasien_online');
    }else{
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('sess_user_realname');
      $this->db->where('regonline_id', $id)->update('reg_pasien_online', $data);
    }
  }

  public function delete_file_process($file_folder=null, $file_name=null) {
    $path_dir = "assets/images/pendaftaran_online/".$file_folder."/";
    $result = unlink($path_dir . $file_name);
    return $result;
  }
  
}