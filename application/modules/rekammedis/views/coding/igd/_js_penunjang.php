<script type="text/javascript">
  $(document).ready(function () {
    $('.modal-href').click(function (e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");
      var modal_tab = $(this).attr("modal-tab");

      $("#modal-size").removeClass('modal-lg').removeClass('modal-md').removeClass('modal-sm');
      
      $("#modal-title").html(modal_title);
      $("#modal-size").addClass('modal-' + modal_size);
      if(modal_custom_size){
        $("#modal-size").attr('style', 'max-width: '+modal_custom_size+'px !important');
      }
      $("#myModal").modal('show');
      $.post($(this).data('href'), function (data) {
        $("#modal-body").html(data.html);
      }, 'json');
      activaTab(modal_tab);
      get_data(modal_tab);
    });

    laboratorium_data('<?=$reg['reg_id']?>','<?=$reg['pasien_id']?>','<?=$reg['lokasi_id']?>');
  })

  function get_data($type) {
    switch ($type) {
      case 'laboratorium':
        laboratorium_data('<?=$reg['reg_id']?>','<?=$reg['pasien_id']?>','<?=$reg['lokasi_id']?>');
        break;
      case 'radiologi':
        radiologi_data('<?=$reg['reg_id']?>','<?=$reg['pasien_id']?>','<?=$reg['lokasi_id']?>');
        break;
      case 'bedah_sentral':
        bedah_sentral_data('<?=$reg['reg_id']?>','<?=$reg['pasien_id']?>','<?=$reg['lokasi_id']?>');
        break;
      case 'vk':
        vk_data('<?=$reg['reg_id']?>','<?=$reg['pasien_id']?>','<?=$reg['lokasi_id']?>');
        break;
      case 'icu':
        icu_data('<?=$reg['reg_id']?>','<?=$reg['pasien_id']?>','<?=$reg['lokasi_id']?>');
        break;
      case 'hemodialisa':
        hemodialisa_data('<?=$reg['reg_id']?>','<?=$reg['pasien_id']?>','<?=$reg['lokasi_id']?>');
        break;
    }
  }

  function activaTab(tab){
    $('.nav-tabs a[href="#' + tab + '"]').tab('show');
  };

  $("#laboratorium_refresh").on('click', function () {
    $('#laboratorium_refresh i').addClass('fa-spin');
    laboratorium_data('<?=$reg['reg_id']?>','<?=$reg['pasien_id']?>','<?=$reg['lokasi_id']?>');
  });

  $("#radiologi_refresh").on('click', function () {
    $('#radiologi_refresh i').addClass('fa-spin');
    radiologi_data('<?=$reg['reg_id']?>','<?=$reg['pasien_id']?>','<?=$reg['lokasi_id']?>');
  });

  $("#bedah_sentral_refresh").on('click', function () {
    $('#bedah_sentral_refresh i').addClass('fa-spin');
    bedah_sentral_data('<?=$reg['reg_id']?>','<?=$reg['pasien_id']?>','<?=$reg['lokasi_id']?>');
  });

  $("#vk_refresh").on('click', function () {
    $('#vk_refresh i').addClass('fa-spin');
    vk_data('<?=$reg['reg_id']?>','<?=$reg['pasien_id']?>','<?=$reg['lokasi_id']?>');
  });

  $("#icu_refresh").on('click', function () {
    $('#icu_refresh i').addClass('fa-spin');
    icu_data('<?=$reg['reg_id']?>','<?=$reg['pasien_id']?>','<?=$reg['lokasi_id']?>');
  });

  $("#hemodialisa_refresh").on('click', function () {
    $('#hemodialisa_refresh i').addClass('fa-spin');
    hemodialisa_data('<?=$reg['reg_id']?>','<?=$reg['pasien_id']?>','<?=$reg['lokasi_id']?>');
  });

  function laboratorium_data(reg_id='', pasien_id='', lokasi_id='') {
    $('#laboratorium_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_penunjang_laboratorium/data'?>', {reg_id: reg_id, pasien_id: pasien_id, lokasi_id: lokasi_id}, function (data) {
      $('#laboratorium_data').html(data.html);
      $('#laboratorium_refresh i').removeClass('fa-spin');
    }, 'json');
  }

  function radiologi_data(reg_id='', pasien_id='', lokasi_id='') {
    $('#radiologi_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_penunjang_radiologi/data'?>', {reg_id: reg_id, pasien_id: pasien_id, lokasi_id: lokasi_id}, function (data) {
      $('#radiologi_data').html(data.html);
      $('#radiologi_refresh i').removeClass('fa-spin');
    }, 'json');
  }

  function bedah_sentral_data(reg_id='', pasien_id='', lokasi_id='') {
    $('#bedah_sentral_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_penunjang_bedah_sentral/data'?>', {reg_id: reg_id, pasien_id: pasien_id, lokasi_id: lokasi_id}, function (data) {
      $('#bedah_sentral_data').html(data.html);
      $('#bedah_sentral_refresh i').removeClass('fa-spin');
    }, 'json');
  }

  function vk_data(reg_id='', pasien_id='', lokasi_id='') {
    $('#vk_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_penunjang_vk/data'?>', {reg_id: reg_id, pasien_id: pasien_id, lokasi_id: lokasi_id}, function (data) {
      $('#vk_data').html(data.html);
      $('#vk_refresh i').removeClass('fa-spin');
    }, 'json');
  }

  function icu_data(reg_id='', pasien_id='', lokasi_id='') {
    $('#icu_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_penunjang_icu/data'?>', {reg_id: reg_id, pasien_id: pasien_id, lokasi_id: lokasi_id}, function (data) {
      $('#icu_data').html(data.html);
      $('#icu_refresh i').removeClass('fa-spin');
    }, 'json');
  }

  function hemodialisa_data(reg_id='', pasien_id='', lokasi_id='') {
    $('#hemodialisa_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_penunjang_hemodialisa/data'?>', {reg_id: reg_id, pasien_id: pasien_id, lokasi_id: lokasi_id}, function (data) {
      $('#hemodialisa_data').html(data.html);
      $('#hemodialisa_refresh i').removeClass('fa-spin');
    }, 'json');
  }
</script>