<div class="js_petugas_<?=$form_name?>">
<script type="text/javascript">
  $(document).ready(function () {
    $('.modal-href').click(function (e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");
      
      $("#modal-title").html(modal_title);
      $("#modal-size").addClass('modal-' + modal_size);
      if(modal_custom_size){
        $("#modal-size").attr('style', 'max-width: '+modal_custom_size+'px !important');
      }
      $("#myModal").modal('show');
      $.post($(this).data('href'), function (data) {
        $("#modal-body").html(data.html);
      }, 'json');
    });

    <?php if (@$tindakan_id !=''): ?>
    var pegawai_id<?=$petugas_no?> = '<?=@$pegawai_id?>';
    var pegawai_nm<?=$petugas_no?> = '<?=@$pegawai_nm?>';
    var petugasData<?=$petugas_no?> = {
      id: pegawai_id<?=$petugas_no?>,
      text: pegawai_nm<?=$petugas_no?>
    };
    var petugasOpt<?=$petugas_no?> = new Option(petugasData<?=$petugas_no?>.text, petugasData<?=$petugas_no?>.id, false, false);
    $('#<?=$type?>-<?=$form_name?>').append(petugasOpt<?=$petugas_no?>).trigger('change');
    $('#<?=$type?>-<?=$form_name?>').val(pegawai_id<?=$petugas_no?>);
    $('#<?=$type?>-<?=$form_name?>').trigger('change');
    <?php endif; ?>

    // autocomplete
    $('#<?=$type?>-<?=$form_name?>').select2({
      minimumInputLength: 2,
      ajax: {
        url: "<?=site_url($nav['nav_url'])?>/ajax_tindakan/petugas_autocomplete",
        dataType: "json",
        
        data: function(params) {
          return{
            petugas_nm : params.term
          };
        },
        processResults: function(data){
          return{
            results: data
          }
        }
      }
    });
    $('.select2-container').css('width', '100%');

    // delete
    $('.delete-petugas').on('click', function (e) {
      e.preventDefault();
      Swal.fire({
        title: 'Apakah Anda yakin?',
        text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#eb3b5a',
        cancelButtonColor: '#b2bec3',
        confirmButtonText: 'Hapus',
        cancelButtonText: 'Batal',
        customClass: 'swal-wide'
      }).then((result) => {
        if (result.value) {
          var tindakan_id = $(this).attr("data-tindakan-id");
          var form_name = $(this).attr("data-form-name");
          //
          $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_tindakan/delete_petugas'?>', {tindakan_id: tindakan_id, form_name: form_name}, function (data) {
            if (data.callback == 'true') {
              $(".box_petugas_"+form_name).html('<input type="hidden" name="'+form_name+'" value="0">');
              $(".js_petugas_"+form_name).remove();
            }
          }, 'json');
        }
      })
    })

  })

  function petugas_fill(id, form_name) {
    $.ajax({
      type : 'post',
      url : '<?=site_url($nav['nav_url'].'/ajax_tindakan/petugas_fill')?>',
      dataType : 'json',
      data : 'pegawai_id='+id,
      success : function (data) {
        // autocomplete
        var petugasData = {
          id: data.pegawai_id,
          text: data.pegawai_nm
        };
        var petugasOpt = new Option(petugasData.text, petugasData.id, false, false);
        $('#<?=$type?>-'+form_name).append(petugasOpt).trigger('change');
        $('#<?=$type?>-'+form_name).val(data.pegawai_id);
        $('#<?=$type?>-'+form_name).trigger('change');
                
        $('.select2-container').css('width', '100%');
        $("#myModal").modal('hide');
      }
    })
  }
</script>
</div>
<div class="form-group row mb-2 box_petugas_<?=$form_name?>">
  <label class="col-lg-3 col-md-3 col-form-label">Petugas <span class="text-danger">*</span></label>
  <div class="col-lg-6 col-md-6">
    <select class="form-control autocomplete" name="<?=$form_name?>" id="<?=@$type?>-<?=$form_name?>">
      <option value="">- Pilih -</option>
    </select>
    <?php if ($petugas_no_post == '5' && $petugas_no == '5'): ?>
      <div class="text-danger small-text float-right mt-n1">Maksimal 5 Petugas</div>
    <?php endif; ?>
  </div>
  <div class="col-lg-1 col-md-1">
    <a href="javascript:void(0)" class="btn btn-xs btn-secondary delete-petugas" data-tindakan-id="<?=@$tindakan_id?>" data-form-name="<?=$form_name?>" title="Hapus Petugas"><i class="fas fa-times"></i></a>
  </div>
  <div class="col-lg-1 col-md-1">
    <a href="javascript:void(0)" data-href="<?=site_url().'/'.$nav['nav_url'].'/ajax_tindakan/search_petugas/'.$form_name?>" modal-title="List Data Petugas" modal-size="lg" class="btn btn-xs btn-default modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Petugas" data-original-title="Cari Petugas"><i class="fas fa-search"></i></a>
  </div>
</div>