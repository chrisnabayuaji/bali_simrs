<?php if(@$main == null):?>
  <tr>
    <th class="text-center" width="20">No</th>
    <th class="text-center" width="60">Aksi</th>
    <th class="text-center">Kode</th>
    <th class="text-center">Nama Obat</th>
    <th class="text-center">Jenis Resep</th>
    <th class="text-center">Dosis</th>
    <th class="text-center">Qty</th> 
    <th class="text-center">Cara Pakai</th>
    <th class="text-center">Keterangan</th>
  </tr>
  <tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else: ?>
  <?php foreach($main as $row):?>
  <tr bgcolor="#f5f5f5">
    <td class="text-left" colspan="99"><b>NO.RESEP : <?=$row['resepgroup_id']?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; TANGGAL : <?=to_date($row['tgl_catat'], '' , 'date')?></b></td>
  </tr>
  <tr>
    <th class="text-center" width="20">No</th>
    <th class="text-center" width="60">Aksi</th>
    <th class="text-center">Kode</th>
    <th class="text-center">Nama Obat</th>
    <th class="text-center">Jenis Resep</th>
    <th class="text-center">Dosis</th>
    <th class="text-center">Qty</th> 
    <th class="text-center">Cara Pakai</th>
    <th class="text-center">Keterangan</th>
  </tr>
    <?php $i=1;foreach ($row['list_dat_resep'] as $dat_resep): ?>
      <tr>
        <td class="text-center" width="20"><?=$i++?></td>
        <td class="text-center" width="60">
          <a href="javascript:void(0)" class="btn btn-primary btn-table btn-edit" data-resep-id="<?=$dat_resep['resep_id']?>" data-reg-id="<?=$dat_resep['reg_id']?>" title="Ubah Data"><i class="fas fa-pencil-alt"></i></a>
          <a href="javascript:void(0)" class="btn btn-danger btn-table btn-delete" data-resep-id="<?=$dat_resep['resep_id']?>" data-reg-id="<?=$dat_resep['reg_id']?>" data-lokasi-id="<?=$dat_resep['lokasi_id']?>" data-resepgroup-id="<?=$dat_resep['resepgroup_id']?>" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
        </td>
        <td class="text-center"><?=$dat_resep['obat_id']?></td>
        <td class="text-left"><?=$dat_resep['obat_nm']?></td>
        <td class="text-center"><?php echo ($dat_resep['jenisresep_cd'] == 1) ? 'Dalam' : 'Luar' ; ?></td>
        <td class="text-left"><?=$dat_resep['dosis']?></td>
        <td class="text-center"><?=$dat_resep['qty']?></td>
        <td class="text-center"><?=get_parameter_value('carapakai_cd', $dat_resep['carapakai_cd'])?></td>
        <td class="text-left"><?=$dat_resep['keterangan_resep']?></td>
      </tr>
   <?php endforeach; ?>
  <?php endforeach; ?>
<?php endif; ?>

<script type="text/javascript">
$(document).ready(function () {
  // edit
  $('.btn-edit').bind('click',function(e) {
    e.preventDefault();
    var resep_id = $(this).attr("data-resep-id");
    var reg_id = $(this).attr("data-reg-id");
    //
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_pemberian_obat/get_data'?>', {resep_id: resep_id, reg_id: reg_id}, function (data) {

      $('.datetimepicker').daterangepicker({
        startDate: moment(data.main.tgl_catat),
        endDate: moment(),
        timePicker: true,
        timePicker24Hour: true,
        timePickerSeconds: true,
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
          cancelLabel: 'Clear',
          format: 'DD-MM-YYYY H:mm:ss'
        },
        isInvalidDate: function(date) {
          return '';
        }
      })
      
      $('#resep_id').val(data.main.resep_id);

      // autocomplete
      var data2 = {
        id: data.main.obat_id+'#'+data.main.obat_nm+'#'+data.main.stokdepo_id,
        text: data.main.obat_id+' - '+data.main.obat_nm
      };
      var newOption = new Option(data2.text, data2.id, false, false);
      $('#obat_id').append(newOption).trigger('change');
      $('#obat_id').val(data.main.obat_id+'#'+data.main.obat_nm+'#'+data.main.stokdepo_id);
      $('#obat_id').trigger('change');
      $('.select2-container').css('width', '100%');

      $("#resepgroup_id").val(data.main.resepgroup_id).removeClass("is-valid").removeClass("is-invalid");
      $("#stokdepo_id").val(data.main.stokdepo_id).removeClass("is-valid").removeClass("is-invalid");
      $("#dosis").val(data.main.dosis).removeClass("is-valid").removeClass("is-invalid");
      $("#qty").val(data.main.qty).removeClass("is-valid").removeClass("is-invalid");
      $("#jenisresep_cd").val(data.main.jenisresep_cd).trigger('change').removeClass("is-valid").removeClass("is-invalid");
      $("#carapakai_cd").val(data.main.carapakai_cd).trigger('change').removeClass("is-valid").removeClass("is-invalid");
      $("#keterangan_resep").val(data.main.keterangan_resep).removeClass("is-valid").removeClass("is-invalid");
      $('#pemberian_obat_action').html('<i class="fas fa-save"></i> Ubah');
    }, 'json');
  });
  // delete
  $('.btn-delete').on('click', function (e) {
    e.preventDefault();
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#eb3b5a',
      cancelButtonColor: '#b2bec3',
      confirmButtonText: 'Hapus',
      cancelButtonText: 'Batal',
      customClass: 'swal-wide'
    }).then((result) => {
      if (result.value) {
        var resep_id = $(this).attr("data-resep-id");
		    var reg_id = $(this).attr("data-reg-id");
		    var lokasi_id = $(this).attr("data-lokasi-id");
        var resepgroup_id = $(this).attr("data-resepgroup-id");
		    //
		    $('#pemberian_obat_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
		    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_pemberian_obat/delete_data'?>', {resep_id: resep_id, reg_id: reg_id, lokasi_id: lokasi_id, resepgroup_id: resepgroup_id}, function (data) {
		    	$.toast({
            heading: 'Sukses',
            text: 'Data berhasil dihapus.',
            icon: 'success',
            position: 'top-right'
          })
		      $('#pemberian_obat_data').html(data.html);
		    }, 'json');
      }
    })
  })
});
</script>