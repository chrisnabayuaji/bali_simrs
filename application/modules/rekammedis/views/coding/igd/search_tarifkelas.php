<ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="pills-tarifkelaskhusus-tab" data-toggle="pill" href="#pills-tarifkelaskhusus" role="tab" aria-controls="pills-tarifkelaskhusus" aria-selected="false">List Data Tindakan Kelas 2</a>
  </li>
  <li class="nav-item">
    <a class="nav-link " id="pills-tarifkelas-tab" data-toggle="pill" href="#pills-tarifkelas" role="tab" aria-controls="pills-tarifkelas" aria-selected="true">List Data Tindakan Semua Kelas</a>
  </li>
</ul>
<div class="tab-content p-0" id="pills-tabContent" style="border:0px !important">
  <div class="tab-pane fade show active" id="pills-tarifkelaskhusus" role="tabpanel" aria-labelledby="pills-tarifkelaskhusus-tab">
    <div class="table-responsive">
      <table id="tarifkelaskhusus_table" class="table table-hover table-bordered table-striped table-sm w-100">
        <thead>
          <tr>
            <th class="text-center" width="20">No.</th>
            <th class="text-center" width="120">Kode</th>
            <th class="text-center">Nama Tarif</th>
            <th class="text-center">Kelas</th>
            <th class="text-center">Nominal</th>
            <th class="text-center" width="60">Aksi</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
  <div class="tab-pane fade " id="pills-tarifkelas" role="tabpanel" aria-labelledby="pills-tarifkelas-tab">
    <div class="table-responsive">
      <table id="tarifkelas_table" class="table table-hover table-bordered table-striped table-sm w-100">
        <thead>
          <tr>
            <th class="text-center" width="20">No.</th>
            <th class="text-center" width="120">Kode</th>
            <th class="text-center">Nama Tarif</th>
            <th class="text-center">Kelas</th>
            <th class="text-center">Nominal</th>
            <th class="text-center" width="60">Aksi</th>
          </tr>
        </thead>
      </table>
    </div>
  </div>
</div>

<script type="text/javascript">
  var tarifKelasTable,tarifKelasKhususTable;
  $(document).ready(function () {
    //datatables
    tarifKelasTable = $('#tarifkelas_table').DataTable({
      'retrieve': true,
      "autoWidth" : false,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
      "ajax": {
          "url": "<?php echo site_url().'/'.$nav['nav_url'].'/ajax_tindakan/search_tarifkelas_data'?>",
          "type": "POST"
      },      
      "columnDefs": [
        {"targets": 0, "className": 'text-center', "orderable" : false},
        {"targets": 1, "className": 'text-left', "orderable" : true},
        {"targets": 2, "className": 'text-left', "orderable" : false},
        {"targets": 3, "className": 'text-center', "orderable" : false},
        {"targets": 4, "className": 'text-right', "orderable" : false},
        {"targets": 5, "className": 'text-center', "orderable" : false},
      ],
    });
    tarifKelasTable.columns.adjust().draw();

    //datatables
    tarifKelasKhususTable = $('#tarifkelaskhusus_table').DataTable({
      'retrieve': true,
      "autoWidth" : false,
      "processing": true, 
      "serverSide": true, 
      "order": [], 
      "ajax": {
          "url": "<?php echo site_url().'/'.$nav['nav_url'].'/ajax_tindakan/search_tarifkelas_data/'.$kelas_id?>",
          "type": "POST"
      },      
      "columnDefs": [
        {"targets": 0, "className": 'text-center', "orderable" : false},
        {"targets": 1, "className": 'text-left', "orderable" : true},
        {"targets": 2, "className": 'text-left', "orderable" : false},
        {"targets": 3, "className": 'text-center', "orderable" : false},
        {"targets": 4, "className": 'text-right', "orderable" : false},
        {"targets": 5, "className": 'text-center', "orderable" : false},
      ],
    });
    tarifKelasKhususTable.columns.adjust().draw();
  })
</script>