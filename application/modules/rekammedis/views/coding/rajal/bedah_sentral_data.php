<?php if(@$main == null):?>
	<tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else: ?>
	<?php $i=1;foreach($main as $row):?>
	<tr>
		<td class="text-center"><?=$i++?></td>
    <td class="text-center" width="150"><?=to_date(@$row['tgl_order'],'-','full_date')?></td>
    <td class="text-left"><?=$row['keterangan_order']?></td>
    <td class="text-center" width="150"><?=to_date(@$row['tgl_diperiksa'],'-','full_date')?></td>
    <td class="text-left"><?=$row['dokter_nm']?></td>
    <td class="text-left"><?=$row['lokasi_nm']?></td>
    <td class="text-center" width="50">
      <?php if($row['periksa_st'] == 0): ?>
        <div class="badge badge-xs badge-light blink">Belum Diperiksa</div>
      <?php else: ?>
        <div class="badge badge-xs badge-success">Selesai</div>
      <?php endif;?>
    </td>
		<td class="text-center">
      <a href="javascript:void(0)" data-href="<?=site_url().'/'.$nav['nav_url'].'/ajax_penunjang_bedah_sentral/detail/'.$row['src_reg_id'].'/'.$row['pemeriksaan_id']?>" modal-tab="bedah_sentral" modal-title="Detail Data Bedah Sentral" modal-size="lg" class="btn btn-info btn-table modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-info" title="Detail Data" data-original-title="Detail Data"><i class="fas fa-list-alt"></i></a>
      <!-- <?php if($row['periksa_st'] != '1'): ?>  
        <a href="javascript:void(0)" data-href="<?=site_url().'/'.$nav['nav_url'].'/ajax_penunjang_bedah_sentral/modal/'.$row['src_reg_id'].'/'.$row['pemeriksaan_id']?>" modal-tab="bedah_sentral" modal-title="Ubah Data Bedah Sentral" modal-size="md" class="btn btn-primary btn-table modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Ubah Data" data-original-title="Ubah Data"><i class="fas fa-pencil-alt"></i></a>
        <a href="javascript:void(0)" class="btn btn-danger btn-table btn-delete" data-pemeriksaan-id="<?=$row['pemeriksaan_id']?>" data-reg-id="<?=$row['src_reg_id']?>" data-pasien-id="<?=$row['pasien_id']?>" data-lokasi-id="<?=$row['src_lokasi_id']?>" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
      <?php endif;?> -->
		</td>  
	</tr>
	<?php endforeach; ?>
<?php endif; ?>

<script type="text/javascript">
$(document).ready(function () {
  $('.modal-href').click(function (e) {
    e.preventDefault();
    var modal_title = $(this).attr("modal-title");
    var modal_size = $(this).attr("modal-size");
    var modal_custom_size = $(this).attr("modal-custom-size");
    var modal_tab = $(this).attr("modal-tab");

    $("#modal-size").removeClass('modal-lg').removeClass('modal-md').removeClass('modal-sm');
    
    $("#modal-title").html(modal_title);
    $("#modal-size").addClass('modal-' + modal_size);
    if(modal_custom_size){
      $("#modal-size").attr('style', 'max-width: '+modal_custom_size+'px !important');
    }
    $("#myModal").modal('show');
    $.post($(this).data('href'), function (data) {
      $("#modal-body").html(data.html);
    }, 'json');
  });
  // delete
  $('.btn-delete').on('click', function (e) {
    e.preventDefault();
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#eb3b5a',
      cancelButtonColor: '#b2bec3',
      confirmButtonText: 'Hapus',
      cancelButtonText: 'Batal',
      customClass: 'swal-wide'
    }).then((result) => {
      if (result.value) {
        var pemeriksaan_id = $(this).attr("data-pemeriksaan-id");
		    var reg_id = $(this).attr("data-reg-id");
		    var pasien_id = $(this).attr("data-pasien-id");
		    var lokasi_id = $(this).attr("data-lokasi-id");
		    //
		    $('#bedah_sentral_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
		    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_penunjang_bedah_sentral/delete_data'?>', {pemeriksaan_id: pemeriksaan_id, reg_id: reg_id, pasien_id: pasien_id, lokasi_id: lokasi_id}, function (data) {
		    	$.toast({
            heading: 'Sukses',
            text: 'Data berhasil dihapus.',
            icon: 'success',
            position: 'top-right'
          })
		      $('#bedah_sentral_data').html(data.html);
		    }, 'json');
      }
    })
  })
});
</script>