<div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
<div class="content-wrapper mw-100">
  <div class="row mt-n4 mb-n3">
    <div class="col-lg-4 col-md-12">
      <div class="d-lg-flex align-items-baseline col-title">
        <div class="col-back">
          <a href="<?= site_url($nav['nav_module'] . '/dashboard') ?>" class="btn btn-back btn-primary"><i class="fas fa-angle-double-left"></i> Kembali</a>
        </div>
        <div class="text-dark font-weight-semibold line-height-1 big-text border-title">
          <?= $nav['nav_nm'] ?>
          <span class="line-title"></span>
        </div>
      </div>
    </div>
    <div class="col-lg-8 col-md-12">
      <!-- Breadcrumb -->
      <nav aria-label="breadcrumb" class="d-lg-flex justify-content-xl-end justify-content-lg-end col-breadcrumb">
        <ol class="breadcrumb breadcrumb-custom">
          <li class="breadcrumb-item"><a href="<?= site_url('app/dashboard') ?>"><i class="fas fa-home"></i> Dashboard</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($module_nav['nav_url']) ?>"><i class="fas fa-folder-open"></i> <?= $module_nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item"><a href="javascript:void(0)"><i class="fas fa-ambulance"></i> Rawat Jalan</a></li>
          <li class="breadcrumb-item"><a href="<?= site_url($nav['nav_url']) ?>"><i class="<?= $nav['nav_icon'] ?>"></i> <?= $nav['nav_nm'] ?></a></li>
          <li class="breadcrumb-item active"><span>Form</span></li>
        </ol>
      </nav>
      <!-- End Breadcrumb -->
    </div>
  </div>

  <div class="row full-page mt-4">
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card border-none">
        <div class="card-body card-shadow">
          <h4 class="card-title border-bottom border-2 pb-2 mb-3"><?= $nav['nav_nm'] ?> : <?= @$main['lokasi_nm'] ?> (<?= $main['kelas_nm'] ?>)</h4>
          <div class="row mt-1">
            <input type="hidden" id="reg_id" value="<?= @$main['reg_id'] ?>">
            <input type="hidden" id="pasien_id" value="<?= @$main['pasien_id'] ?>">
            <input type="hidden" id="lokasi_id" value="<?= @$main['lokasi_id'] ?>">
            <input type="hidden" id="map_lokasi_depo" value="<?= $main['map_lokasi_depo'] ?>">
            <div class="col-md-6">
              <table class="table" style="width: 100% !important; border-bottom: 1px solid #f2f2f2;">
                <tbody>
                  <tr>
                    <td width="150" style="font-weight: 500 !important;">No.Reg</td>
                    <td width="20">:</td>
                    <td><?= @$main['reg_id'] ?></td>
                  </tr>
                  <tr>
                    <td width="150" style="font-weight: 500 !important;">NIK</td>
                    <td width="20">:</td>
                    <td><?= @$main['nik'] ?></td>
                  </tr>
                  <tr>
                    <td width="150" style="font-weight: 500 !important;">No.RM / Nama Pasien</td>
                    <td width="20">:</td>
                    <td><?= @$main['pasien_id'] ?> / <?= @$main['pasien_nm'] ?>, <?= @$main['sebutan_cd'] ?></td>
                  </tr>
                  <tr>
                    <td width="150">Alamat Pasien</td>
                    <td width="20">:</td>
                    <td>
                      <?= (@$main['alamat'] != '') ? @$main['alamat'] . ', ' : '' ?>
                      <?= (@$main['kelurahan'] != '') ? ucwords(strtolower(@$main['kelurahan'])) . ', ' : '' ?>
                      <?= (@$main['kecamatan'] != '') ? ucwords(strtolower(@$main['kecamatan'])) . ', ' : '' ?>
                      <?= (@$main['kabupaten'] != '') ? ucwords(strtolower(@$main['kabupaten'])) . ', ' : '' ?>
                      <?= (@$main['provinsi'] != '') ? ucwords(strtolower(@$main['provinsi'])) : '' ?>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="col-md-6">
              <table class="table" style="width: 100% !important; border-bottom: 1px solid #f2f2f2;">
                <tbody>
                  <tr>
                    <td width="150">Dokter PJ</td>
                    <td width="20">:</td>
                    <td><?= @$main['pegawai_nm'] ?></td>
                  </tr>
                  <tr>
                    <td width="150">Umur / JK</td>
                    <td width="20">:</td>
                    <td><?= @$main['umur_thn'] ?> Th <?= @$main['umur_bln'] ?> Bl <?= @$main['umur_hr'] ?> Hr / <?= get_parameter_value('sex_cd', @$main['sex_cd']) ?></td>
                  </tr>
                  <tr>
                    <td width="150">Jenis / Asal Pasien</td>
                    <td width="20">:</td>
                    <td><?= @$main['jenispasien_nm'] ?> / <?= get_parameter_value('asalpasien_cd', @$main['asalpasien_cd']) ?></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 mt-2">
              <ul class="nav nav-pills nav-pills-primary" id="pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link tab-menu" data-id="link" href="<?= site_url($nav['nav_url']) ?>" style="background-color: #909192; color: #fff;"><i class="fas fa-home"></i> HOME</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link tab-menu" id="diagnosis-tab" data-id="diagnosis" data-view-name="diagnosis" data-toggle="pill" href="#diagnosis" role="tab" aria-controls="diagnosis" aria-selected="false"><i class="fas fa-diagnoses"></i> DIAGNOSIS</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link tab-menu active" id="pemeriksaan-fisik-tab" data-id="pemeriksaan-fisik" data-view-name="pemeriksaan_fisik" data-toggle="pill" href="#pemeriksaan-fisik" role="tab" aria-controls="pemeriksaan-fisik" aria-selected="true"><i class="fas fa-stethoscope"></i> PEMERIKSAAN FISIK</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link tab-menu" id="tindakan-tab" data-id="tindakan" data-view-name="tindakan" data-toggle="pill" href="#tindakan" role="tab" aria-controls="tindakan" aria-selected="false"><i class="fas fa-procedures"></i> TINDAKAN</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link tab-menu" id="penunjang-tab" data-id="penunjang" data-view-name="penunjang" data-toggle="pill" href="#penunjang" role="tab" aria-controls="penunjang" aria-selected="false"><i class="fas fa-user-injured"></i> PENUNJANG</a>
                </li>
              </ul>
              <div class="tab-content p-0 no-border" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pemeriksaan-fisik" role="tabpanel" aria-labelledby="pemeriksaan-fisik-tab">
                  <div class="media">
                    <div class="media-body" id="body-pemeriksaan-fisik">
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="diagnosis" role="tabpanel" aria-labelledby="diagnosis-tab">
                  <div class="media">
                    <div class="media-body" id="body-diagnosis">
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="tindakan" role="tabpanel" aria-labelledby="tindakan-tab">
                  <div class="media">
                    <div class="media-body" id="body-tindakan">
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="pemberian-obat" role="tabpanel" aria-labelledby="pemberian-obat-tab">
                  <div class="media">
                    <div class="media-body" id="body-pemberian-obat">
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="bhp" role="tabpanel" aria-labelledby="bhp-tab">
                  <div class="media">
                    <div class="media-body" id="body-bhp">
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="alkes" role="tabpanel" aria-labelledby="alkes-tab">
                  <div class="media">
                    <div class="media-body" id="body-alkes">
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="penunjang" role="tabpanel" aria-labelledby="penunjang-tab">
                  <div class="media">
                    <div class="media-body" id="body-penunjang">
                    </div>
                  </div>
                </div>
                <div class="tab-pane fade" id="tindak-lanjut" role="tabpanel" aria-labelledby="tindak-lanjut-tab">
                  <div class="media">
                    <div class="media-body" id="body-tindak-lanjut">
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('#pills-tab a').click(function(e) {
    e.preventDefault();
    $(this).tab('show');
  });

  // store the currently selected tab in the hash value
  $("ul.nav-pills > li > a").on("shown.bs.tab", function(e) {
    var id = $(e.target).attr("href").substr(1);
    var data_id = $(e.target).attr("data-id");
    if (data_id == 'link') {
      window.location.href = $(e.target).attr("href");
    } else {
      window.location.hash = id;
    }
  });

  // on load of the page: switch to the currently selected tab
  var hash = window.location.hash;
  $('#pills-tab a[href="' + hash + '"]').tab('show');

  var data_id = $(hash + '-tab').attr("data-id");
  var view_name = $(hash + '-tab').attr("data-view-name");
  get_view_tab_menu(data_id, view_name);

  function get_view_tab_menu(data_id = '', view_name = '') {
    $.post('<?= site_url() . '/' . $nav['nav_url'] . '/ajax/view_tab_menu/' ?>', {
      view_name: view_name,
      id: '<?= $main['reg_id'] ?>'
    }, function(data) {
      $('#body-' + data_id).html(data.html);
    }, 'json');
  }

  $(document).ready(function() {
    $('.tab-menu').click(function(e) {
      e.preventDefault();
      var data_id = $(this).attr("data-id");
      var view_name = $(this).attr("data-view-name");

      get_view_tab_menu(data_id, view_name);
    });
  })
  <?= $main['reg_id'] ?>
</script>