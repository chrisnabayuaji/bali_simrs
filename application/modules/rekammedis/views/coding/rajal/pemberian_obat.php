<?php $this->load->view('_js_pemberian_obat'); ?>
<div class="row">
  <div class="col-md-7">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-list"></i> List Data Pemberian Obat</h4>
        <div class="table-responsive">
          <table class="table table-bordered table-sm">
            <tbody id="pemberian_obat_data"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-5">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-pills"></i> Pemberian Obat</h4>
        <form id="pemberian_obat_form" action="" method="post" autocomplete="off">
          <input type="hidden" name="resep_id" id="resep_id">
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Tanggal Catat <span class="text-danger">*</span></label>
            <div class="col-lg-5 col-md-6">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                </div>
                <input type="text" class="form-control datetimepicker" name="tgl_catat" id="tgl_catat" required="" aria-invalid="false">
              </div>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">No.Resep <span class="text-danger">*</span></label>
            <div class="col-lg-5 col-md-5">
              <input type="text" class="form-control" name="resepgroup_id" id="resepgroup_id" value="otomatis" readonly="">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Jenis Resep <span class="text-danger">*<span></label>
            <div class="col-lg-3 col-md-9">
              <select class="form-control chosen-select" name="jenisresep_cd" id="jenisresep_cd" required="">
                <?php foreach(get_parameter('jenisresep_cd') as $r): ?>
                  <option value="<?=$r['parameter_cd']?>" <?=(@$main['jenisresep_cd'] == $r['parameter_cd']) ? 'selected' : '';?>><?=$r['parameter_val']?></option>
                <?php endforeach;?>
              </select>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Obat <span class="text-danger">*</span></label>
            <div class="col-lg-7 col-md-3">
              <select class="form-control select2" name="obat_id" id="obat_id">
                <option value="">- Pilih -</option>
              </select>
              <input type="hidden" class="form-control" name="stokdepo_id" id="stokdepo_id">
            </div>
            <div class="col-lg-1 col-md-1">
              <a href="javascript:void(0)" data-href="<?=site_url().'/'.$nav['nav_url'].'/ajax_pemberian_obat/search_obat'?>" modal-title="List Data Obat" modal-size="lg" class="btn btn-xs btn-default modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Obat" data-original-title="Cari Obat"><i class="fas fa-search"></i></a>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Dosis <span class="text-danger">*<span></label>
            <div class="col-lg-5 col-md-5">
              <input type="text" class="form-control" name="dosis" id="dosis" required="">
            </div>
            <label class="col-lg-2 col-md-3 col-form-label">Qty <span class="text-danger">*<span></label>
            <div class="col-lg-2 col-md-2">
              <input type="number" class="form-control" name="qty" id="qty" value="1" min="1">
            </div>
          </div>
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Cara Pakai <span class="text-danger">*<span></label>
            <div class="col-lg-4 col-md-9">
              <select class="form-control chosen-select" name="carapakai_cd" id="carapakai_cd" required="">
                <?php foreach(get_parameter('carapakai_cd') as $r): ?>
                  <option value="<?=$r['parameter_cd']?>" <?=(@$main['carapakai_cd'] == $r['parameter_cd']) ? 'selected' : '';?>><?=$r['parameter_val']?></option>
                <?php endforeach;?>
              </select>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Keterangan</label>
            <div class="col-lg-9 col-md-3">
              <textarea class="form-control" name="keterangan_resep" id="keterangan_resep" value="" rows="5"></textarea>            
            </div>
          </div>
          <div class="row">
            <div class="col-lg-9 offset-lg-3 p-0">
              <button type="submit" id="pemberian_obat_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
              <button type="button" id="pemberian_obat_cancel" class="btn btn-xs btn-secondary"><i class="fas fa-times"></i> Batal</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>