<!-- js -->
<?php $this->load->view('_js_pemeriksaan_fisik'); ?>
<div class="row">
  <div class="col">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-copy"></i> Catatan Medis</h4>
        <form id="catatan_medis_form" action="" method="post" autocomplete="off">
          <input type="hidden" name="catatanmedis_id" id="catatanmedis_id">
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Tanggal Catat <span class="text-danger">*</span></label>
            <div class="col-lg-4 col-md-5">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                </div>
                <input type="text" class="form-control datetimepicker" name="tgl_catat" id="tgl_catat" required="" aria-invalid="false">
              </div>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Riwayat Penyakit <span class="text-danger">*</span></label>
            <div class="col-lg-9 col-md-3">
              <textarea class="form-control" name="riwayat_penyakit" id="riwayat_penyakit" value="" required="" rows="5"></textarea>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Alergi Obat <span class="text-danger">*</span></label>
            <div class="col-lg-9 col-md-3">
              <textarea class="form-control" name="alergi_obat" id="alergi_obat" value="" required="" rows="5"></textarea>            
            </div>
          </div>
          <div class="row">
            <div class="col-lg-9 offset-lg-3 p-0">
              <button type="submit" id="catatan_medis_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
              <button type="button" id="catatan_medis_cancel" class="btn btn-xs btn-secondary"><i class="fas fa-times"></i> Batal</button>
            </div>
          </div>
        </form>
        <hr>
        <div class="">
          <table class="table table-bordered table-striped table-sm">
            <thead>
              <tr>
                <th class="text-center" width="20">No</th>
                <th class="text-center" width="60">Aksi</th>
                <th class="text-center" width="135">Tanggal</th>
                <th class="text-center">Riwayat Penyakit</th>
                <th class="text-center">Alergi Obat</th>
              </tr>
            </thead>
            <tbody id="catatan_medis_data"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="col">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-stethoscope"></i> Pemeriksaan Fisik</h4>
        <form id="pemeriksaan_fisik_form" action="" method="post" autocomplete="off">
          <input type="hidden" name="anamnesis_id" id="anamnesis_id">
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Keluhan Utama <span class="text-danger">*</span></label>
            <div class="col-lg-9 col-md-3">
              <textarea class="form-control pemeriksaan_fisik_field" name="keluhan_utama" id="keluhan_utama" value="" required="" rows="5"></textarea>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Systole <span class="text-danger">*</span></label>
            <div class="col-lg-2 col-md-3">
              <input class="form-control pemeriksaan_fisik_field" type="number" name="systole" id="systole" value="" />
            </div>
            <label class="col-lg-2 col-md-3 col-form-label">Diastole <span class="text-danger">*</span></label>
            <div class="col-lg-2 col-md-3">
              <input class="form-control pemeriksaan_fisik_field" type="number" name="diastole" id="diastole" value="" />
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Tinggi <span class="text-danger">*</span></label>
            <div class="col-lg-2 col-md-3">
              <input class="form-control pemeriksaan_fisik_field" type="number" name="tinggi" id="tinggi" value="" />
            </div>
            <label class="col-lg-2 col-md-3 col-form-label">Berat <span class="text-danger">*</span></label>
            <div class="col-lg-2 col-md-3">
              <input class="form-control pemeriksaan_fisik_field" type="number" name="berat" id="berat" value="" />
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Suhu <span class="text-danger">*</span></label>
            <div class="col-lg-2 col-md-3">
              <input class="form-control pemeriksaan_fisik_field" type="number" name="suhu" id="suhu" value="" />
            </div>
            <label class="col-lg-2 col-md-3 col-form-label">Nadi <span class="text-danger">*</span></label>
            <div class="col-lg-2 col-md-3">
              <input class="form-control pemeriksaan_fisik_field" type="number" name="nadi" id="nadi" value="" />
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Respiration Rate <span class="text-danger">*</span></label>
            <div class="col-lg-2 col-md-3">
              <input class="form-control pemeriksaan_fisik_field" type="number" name="respiration_rate" id="respiration_rate" value="" />
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">SaO<sub>2</sub> <span class="text-danger">*</span></label>
            <div class="col-lg-2 col-md-3">
              <input class="form-control pemeriksaan_fisik_field" type="number" name="sao2" id="sao2" value="" />
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>