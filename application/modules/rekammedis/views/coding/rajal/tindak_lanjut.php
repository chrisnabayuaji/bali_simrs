<!-- js -->
<?php $this->load->view('_js_tindak_lanjut'); ?>
<form role="form" id="form-data" method="post" enctype="multipart/form-data" action="<?=site_url().'/'.$this->nav['nav_url'].'/save_tindak_lanjut'?>" autocomplete="off">
<input type="hidden" name="reg_id" id="tindak_lanjut_reg_id">
<input type="hidden" name="pasien_id" id="tindak_lanjut_pasien_id">
<div class="row">
  <div class="col">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <div class="row">
          <div class="col-lg-7">
            <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-ambulance"></i> Tindak Lanjut</h4>
            <div class="form-group row mb-2">
              <label class="col-lg-2 col-md-2 col-form-label">Tindak Lanjut <span class="text-danger">*</span></label>
              <div class="col-lg-3 col-md-3">
                <select class="form-control chosen-select" name="tindaklanjut_cd" id="tindaklanjut_cd">
                  <option value="">- Pilih -</option>
                  <?php foreach(get_parameter('tindaklanjut_cd') as $r): ?>
                    <option value="<?=$r['parameter_cd']?>"><?=$r['parameter_val']?></option>
                  <?php endforeach;?>
                </select>            
              </div>
              <label class="col-lg-2 col-md-3 col-form-label text-right d-none" id="label-hide"></label>
              <div class="col-lg-4 col-md-4 d-none" id="lokasi-jenisreg-1">
                <select class="form-control chosen-select" name="lokasi_jenisreg_1" id="lokasi_jenisreg_1">
                  <option value="">- Pilih -</option>
                  <?php foreach($lokasi_jenisreg_1 as $lok): ?>
                    <option value="<?=$lok['lokasi_id']?>"><?=$lok['lokasi_nm']?></option>
                  <?php endforeach;?>
                </select>            
              </div>
              <div class="col-lg-4 col-md-4 d-none" id="lokasi-jenisreg-2">
                <select class="form-control chosen-select" name="lokasi_jenisreg_2" id="lokasi_jenisreg_2">
                  <option value="">- Pilih -</option>
                  <?php foreach($lokasi_jenisreg_2 as $lok): ?>
                    <option value="<?=$lok['lokasi_id']?>"><?=$lok['lokasi_nm']?></option>
                  <?php endforeach;?>
                </select>            
              </div>
              <div class="col-lg-4 col-md-4 d-none" id="rs-rujukan">
                <select class="form-control chosen-select" name="rsrujukan_id" id="rsrujukan_id">
                  <option value="">- Pilih -</option>
                  <?php foreach($rujukan as $rjk): ?>
                    <option value="<?=$rjk['rsrujukan_id']?>"><?=$rjk['rsrujukan_nm']?></option>
                  <?php endforeach;?>
                </select>            
              </div>
            </div>
            <div class="form-group row mb-2 d-none" id="col-subtindaklanjut-cd">
              <label class="col-lg-2 col-md-2 col-form-label"></label>
              <?php foreach(get_parameter('subtindaklanjut_cd') as $r): ?>
              <div class="col-lg-2 col-md-2">
                <div class="form-check form-radio form-check-primary">
                  <label class="form-check-label label-radio">
                    <input type="radio" class="form-check-input subtindaklanjut_cd" name="subtindaklanjut_cd" value="<?=$r['parameter_cd']?>">
                    <?=$r['parameter_val']?>
                    <i class="input-helper"></i>
                  </label>
                </div>
              </div>
              <?php endforeach; ?>
            </div>
            <div class="form-group row mb-2 d-none" id="btn-rs-rujukan">
              <label class="col-lg-7 col-md-7 col-form-label"></label>
              <div class="col-lg-4 col-md-4 ml-n3">
                <a href="javascript:void(0)" data-href="<?=site_url().'/'.$nav['nav_url'].'/ajax_tindak_lanjut/form_rs_rujukan'?>" modal-title="Tambah RS Rujukan" modal-size="md" class="btn btn-xs btn-primary modal-href"title="Tambah RS Rujukan"><i class="fas fa-plus-circle"></i> Tambah RS Rujukan</a>
              </div>
            </div>
            <div class="form-group row mb-2">
              <label class="col-lg-2 col-md-2 col-form-label">Catatan Tindak Lanjut/Rujukan</label>
              <div class="col-lg-9 col-md-9">
                <textarea class="form-control" name="catatan_tindaklanjut" id="catatan_tindaklanjut" value="" rows="5"></textarea>            
              </div>
            </div>
          </div>
          <div class="col-lg-5">
            <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-home"></i> Status Pulang</h4>
            <div class="form-group row">
              <label class="col-lg-3 col-md-3 col-form-label">Status Pulang <span class="text-danger">*</span></label>
              <div class="col-lg-3 col-md-3">
                <div class="form-check form-radio form-check-primary">
                  <label class="form-check-label label-radio">
                    <input type="radio" class="form-check-input pulang_st" name="pulang_st" value="0">
                    Belum Pulang
                  <i class="input-helper"></i></label>
                </div>
              </div>
              <div class="col-lg-3 col-md-3">
                <div class="form-check form-radio form-check-primary">
                  <label class="form-check-label label-radio">
                    <input type="radio" class="form-check-input pulang_st" name="pulang_st" value="1">
                    Pulang
                  <i class="input-helper"></i></label>
                </div>
              </div>
            </div>
            <div class="d-none" id="col-pulang">
              <div class="form-group row">
                <label class="col-lg-3 col-md-3 col-form-label">Tgl. Pulang <span class="text-danger">*</span></label>
                <div class="col-lg-5 col-md-9">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                    </div>
                    <input type="text" class="form-control datetimepicker" name="tgl_pulang" id="tgl_pulang" required="">
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-lg-3 col-md-3 col-form-label">Tgl. Kontrol <span class="text-danger">*</span></label>
                <div class="col-lg-4 col-md-9">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                    </div>
                    <input type="text" class="form-control datepicker" name="tgl_kontrol" id="tgl_kontrol" required="">
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-lg-3 col-md-3 col-form-label">Keadaan Pasien <span class="text-danger">*</span></label>
                <div class="col-lg-4 col-md-9">
                  <select class="form-control chosen-select" name="keadaan_cd" id="keadaan_cd">
                    <option value="">- Pilih -</option>
                    <?php foreach(get_parameter('keadaan_cd') as $r): ?>
                      <option value="<?=$r['parameter_cd']?>" <?=(@$main['keadaan_cd'] == $r['parameter_cd']) ? 'selected' : '';?>><?=$r['parameter_val']?></option>
                    <?php endforeach;?>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-lg-3 col-md-3 col-form-label">Cara Pulang <span class="text-danger">*</span></label>
                <div class="col-lg-4 col-md-9">
                  <select class="form-control chosen-select" name="carapulang_cd" id="carapulang_cd">
                    <option value="">- Pilih -</option>
                    <?php foreach(get_parameter('carapulang_cd') as $r): ?>
                      <option value="<?=$r['parameter_cd']?>" <?=(@$main['carapulang_cd'] == $r['parameter_cd']) ? 'selected' : '';?>><?=$r['parameter_val']?></option>
                    <?php endforeach;?>
                  </select>
                </div>
              </div>
              <div class="form-group row mb-2">
                <label class="col-lg-3 col-md-3 col-form-label">Catatan Dokter</label>
                <div class="col-lg-9 col-md-9">
                  <textarea class="form-control" name="catatan_dokter" id="catatan_dokter" value="" rows="3"></textarea>            
                </div>
              </div>
              <div class="form-group row mb-2">
                <label class="col-lg-3 col-md-3 col-form-label">Catatan Keperawatan</label>
                <div class="col-lg-9 col-md-9">
                  <textarea class="form-control" name="catatan_keperawatan" id="catatan_keperawatan" value="" rows="3"></textarea>            
                </div>
              </div>
              <div class="form-group row mb-2">
                <label class="col-lg-3 col-md-3 col-form-label">Catatan Farmasi</label>
                <div class="col-lg-9 col-md-9">
                  <textarea class="form-control" name="catatan_farmasi" id="catatan_farmasi" value="" rows="3"></textarea>            
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-2 offset-lg-10 text-right">
            <button type="submit" class="btn btn-xs btn-primary btn-submit"><i class="fas fa-save"></i> Simpan >> Selesai</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</form>