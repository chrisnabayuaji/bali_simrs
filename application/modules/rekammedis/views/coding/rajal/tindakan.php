<?php $this->load->view('_js_tindakan'); ?>
<div class="row">
  <!-- <div class="col-md-5">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-syringe"></i> Tindakan</h4>
        <form id="tindakan_form" action="" method="post" autocomplete="off">
          <input type="hidden" name="tindakan_id" id="tindakan_id">
          <div class="form-group row">
            <label class="col-lg-3 col-md-3 col-form-label">Tanggal Catat <span class="text-danger">*</span></label>
            <div class="col-lg-5 col-md-6">
              <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                </div>
                <input type="text" class="form-control datetimepicker" name="tgl_catat" id="tgl_catat" required="" aria-invalid="false">
              </div>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Tindakan <span class="text-danger">*</span></label>
            <div class="col-lg-8 col-md-3">
              <select class="form-control autocomplete" name="tarifkelas_id" id="tarifkelas_id">
                <option value="">- Pilih -</option>
              </select>
            </div>
            <div class="col-lg-1 col-md-1">
              <a href="javascript:void(0)" data-href="<?=site_url().'/'.$nav['nav_url'].'/ajax_tindakan/search_tarifkelas/'.$reg['kelas_id']?>" modal-title="List Data Tindakan" modal-size="lg" class="btn btn-xs btn-default modal-href" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-primary" title="Cari Tindakan" data-original-title="Cari Tindakan"><i class="fas fa-search"></i></a>
            </div>
          </div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label">Qty <span class="text-danger">*</span></label>
            <div class="col-lg-3 col-md-3">
              <input type="number" class="form-control" name="qty" id="qty" value="1" min="1" required>
            </div>
            <label class="col-lg-2 col-md-2 col-form-label">Jenis <span class="text-danger">*</span></label>
            <div class="col-lg-3 col-md-3">
              <select class="form-control chosen-select" name="jenistindakan_cd" id="jenistindakan_cd">
                <?php foreach(get_parameter('jenistindakan_cd') as $r): ?>
                  <option value="<?=$r['parameter_cd']?>" <?=(@$main['jenistindakan_cd'] == $r['parameter_cd']) ? 'selected' : '';?>><?=$r['parameter_val']?></option>
                <?php endforeach;?>
              </select>
            </div>
          </div>
          <div id="box_petugas"></div>
          <div class="form-group row mb-2">
            <label class="col-lg-3 col-md-3 col-form-label"></label>
            <div class="col-lg-6 col-md-6">
              <input type="hidden" name="petugas_no" id="petugas_no" value="0">
              <a href="javascript:void(0)" class="btn btn-xs btn-default float-right" id="add_petugas" title="Tambah Petugas"><i class="fas fa-plus"></i> Tambah Petugas</a>
            </div>
          </div>
          <div class="row pt-2">
            <div class="col-lg-9 offset-lg-3 p-0">
              <button type="submit" id="tindakan_action" class="btn btn-xs btn-primary"><i class="fas fa-save"></i> Simpan</button>
              <button type="button" id="tindakan_cancel" class="btn btn-xs btn-secondary"><i class="fas fa-times"></i> Batal</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div> -->
  <div class="col-md-12">
    <div class="card border-none no-shadow">
      <div class="card-body">
        <h4 class="card-title border-bottom border-2 pb-2 mb-3"><i class="fas fa-list"></i> List Data Tindakan</h4>
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-sm">
            <thead>
              <tr>
                <th class="text-center" width="20">No</th>
                <!-- <th class="text-center" width="60">Aksi</th> -->
                <th class="text-center" width="100">Kode</th>
                <th class="text-center">Tindakan</th>
                <th class="text-center">Qty</th>
                <th class="text-center">Jenis</th>
                <th class="text-center">Biaya</th>
                <th class="text-center">Petugas</th>
              </tr>
            </thead>
            <tbody id="tindakan_data"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>