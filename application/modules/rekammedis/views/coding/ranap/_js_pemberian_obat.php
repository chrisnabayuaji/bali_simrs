<script type="text/javascript">
  var pemberian_obat_form;
  $(document).ready(function () {
    var pemberian_obat_form = $("#pemberian_obat_form").validate( {
      rules: {
        obat_id: { valueNotEquals: "" },
        qty: { valueNotEquals: "" },
        tipediagnosis_cd: { valueNotEquals: "" }
      },
      messages: {
        obat_id: { valueNotEquals: "Pilih salah satu!" },
        qty: { valueNotEquals: "Kosong!" },
        tipediagnosis_cd: { valueNotEquals: "Pilih salah satu!" }
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('select2')){
          error.insertAfter(element.next(".select2-container"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container"));
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $("#pemberian_obat_action").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#pemberian_obat_action").attr("disabled", "disabled");
        $("#pemberian_obat_cancel").attr("disabled", "disabled");
        var pasien_id = $("#pasien_id").val();
        var reg_id = $("#reg_id").val();
        var lokasi_id = $("#lokasi_id").val();
        $.ajax({
          type : 'post',
          url : '<?=site_url($nav['nav_url'])?>/ajax_pemberian_obat/save',
          data : $(form).serialize()+'&reg_id='+reg_id+'&lokasi_id='+lokasi_id,
          success : function (data) {
            pemberian_obat_reset();
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil disimpan.',
              icon: 'success',
              position: 'top-right'
            })
            $("#pemberian_obat_action").html('<i class="fas fa-save"></i> Simpan');
            $("#pemberian_obat_action").attr("disabled", false);
            $("#pemberian_obat_cancel").attr("disabled", false);
            pemberian_obat_data(reg_id, lokasi_id);
          }
        })
        return false;
      }
    });

    // select2
    $(".chosen-select").select2();
    $('.select2-container').css('width', '100%');

    // autocomplete
    $('#obat_id').select2({
      minimumInputLength: 2,
      ajax: {
        url: "<?=site_url($nav['nav_url'])?>/ajax_pemberian_obat/autocomplete",
        dataType: "json",
        data: function(params) {
          return{
            obat_nm : params.term,
            map_lokasi_depo : $('#map_lokasi_depo').val(),
          };
        },
        processResults: function(data){
          return{
            results: data
          }
        }
      }
    });
    $('.select2-container').css('width', '100%');

    var reg_id = $("#reg_id").val();
    var lokasi_id = $("#lokasi_id").val();
    pemberian_obat_data(reg_id, lokasi_id);

    $("#pemberian_obat_cancel").on('click', function () {
      pemberian_obat_reset();
    });

    $('.modal-href').click(function (e) {
      e.preventDefault();
      var modal_title = $(this).attr("modal-title");
      var modal_size = $(this).attr("modal-size");
      var modal_custom_size = $(this).attr("modal-custom-size");
      
      $("#modal-title").html(modal_title);
      $("#modal-size").addClass('modal-' + modal_size);
      if(modal_custom_size){
        $("#modal-size").attr('style', 'max-width: '+modal_custom_size+'px !important');
      }
      $("#myModal").modal('show');
      $.post($(this).data('href'), function (data) {
        $("#modal-body").html(data.html);
      }, 'json');
    });

    $('.datetimepicker').daterangepicker({
      startDate: moment("<?=date('Y-m-d H:i:s')?>"),
      endDate: moment(),
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })

  })

  function pemberian_obat_data(reg_id='', lokasi_id='') {
    $('#pemberian_obat_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_pemberian_obat/data'?>', {reg_id: reg_id, lokasi_id: lokasi_id}, function (data) {
      $('#pemberian_obat_data').html(data.html);
    }, 'json');
  }

  function pemberian_obat_reset() {
    $("#resep_id").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#stokdepo_id_sebelum").val('').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#stokdepo_id").val('').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#obat_id").val('').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#jenisresep_cd").val('1').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#dosis").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#qty").val('1').removeClass("is-valid").removeClass("is-invalid");
    $("#carapakai_cd").val('01').trigger('change').removeClass("is-valid").removeClass("is-invalid");
    $("#keterangan_resep").val('').removeClass("is-valid").removeClass("is-invalid");
    $('#pemberian_obat_action').html('<i class="fas fa-save"></i> Simpan');

    $('.datetimepicker').daterangepicker({
      startDate: moment("<?=date('Y-m-d H:i:s')?>"),
      endDate: moment(),
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })
  }

  function obat_fill(id, stokdepo_id = null) {
    $.ajax({
      type : 'post',
      url : '<?=site_url($nav['nav_url'].'/ajax_pemberian_obat/obat_fill')?>',
      dataType : 'json',
      data : 'obat_id='+id+'&stokdepo_id='+stokdepo_id,
      success : function (data) {
        // autocomplete
        var data2 = {
          id: data.obat_id+'#'+data.obat_nm+'#'+data.stokdepo_id,
          text: data.obat_id+' - '+data.obat_nm
        };
        var newOption = new Option(data2.text, data2.id, false, false);
        $('#obat_id').append(newOption).trigger('change');
        $('#obat_id').val(data.obat_id+'#'+data.obat_nm+'#'+data.stokdepo_id);
        $('#obat_id').trigger('change');
        $('.select2-container').css('width', '100%');
        //update stokdepo_id
        $("#stokdepo_id").val(data.stokdepo_id);
        $("#myModal").modal('hide');
      }
    })
  }
  
  function obat_fill_master(id) {
    $.ajax({
      type : 'post',
      url : '<?=site_url($nav['nav_url'].'/ajax_pemberian_obat/obat_fill_master')?>',
      dataType : 'json',
      data : 'obat_id='+id,
      success : function (data) {
        // autocomplete
        var data2 = {
          id: data.obat_id+'#'+data.obat_nm+'#',
          text: data.obat_id+' - '+data.obat_nm
        };
        var newOption = new Option(data2.text, data2.id, false, false);
        $('#obat_id').append(newOption).trigger('change');
        $('#obat_id').val(data.obat_id+'#'+data.obat_nm+'#');
        $('#obat_id').trigger('change');
        $('.select2-container').css('width', '100%');
        $("#jenisresep_cd").val('2').trigger('change').removeClass("is-valid").removeClass("is-invalid");
        $("#myModal").modal('hide');
      }
    })
  }
</script>