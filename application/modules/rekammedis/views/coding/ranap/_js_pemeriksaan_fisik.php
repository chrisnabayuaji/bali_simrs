<script type="text/javascript">
  var catatan_medis_form;
  $(document).ready(function () {
    var catatan_medis_form = $("#catatan_medis_form").validate( {
      rules: {
        
      },
      messages: {
        
      },
      errorElement: "em",
      errorPlacement: function (error,element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if($(element).hasClass('chosen-select')){
          error.insertAfter(element.next(".select2-container"));
        }else{
          error.insertAfter(element);
        }
      },
      highlight: function (element,errorClass,validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function (form) {
        $("#catatan_medis_action").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $("#catatan_medis_action").attr("disabled", "disabled");
        $("#catatan_medis_cancel").attr("disabled", "disabled");
        var pasien_id = $("#pasien_id").val();
        var reg_id = $("#reg_id").val();
        var lokasi_id = $("#lokasi_id").val();
        $.ajax({
          type : 'post',
          url : '<?=site_url($nav['nav_url'])?>/ajax_catatan_medis/save',
          data : $(form).serialize()+'&pasien_id='+pasien_id+'&reg_id='+reg_id+'&lokasi_id='+lokasi_id,
          success : function (data) {
            catatan_medis_reset();
            $.toast({
              heading: 'Sukses',
              text: 'Data berhasil disimpan.',
              icon: 'success',
              position: 'top-right'
            })
            $("#catatan_medis_action").html('<i class="fas fa-save"></i> Simpan');
            $("#catatan_medis_action").attr("disabled", false);
            $("#catatan_medis_cancel").attr("disabled", false);
            catatan_medis_data(pasien_id, reg_id, lokasi_id);
          }
        })
        $(form).submit(function(e){return false;});
      }
    });

    var pasien_id = $("#pasien_id").val();
    var reg_id = $("#reg_id").val();
    var lokasi_id = $("#lokasi_id").val();
    catatan_medis_data(pasien_id, reg_id, lokasi_id);

    $("#catatan_medis_cancel").on('click', function () {
      catatan_medis_reset();
    });

    pemeriksaan_fisik_data();

    $(".pemeriksaan_fisik_field").on('change', function () {
      var pasien_id = $("#pasien_id").val();
      var reg_id = $("#reg_id").val();
      var lokasi_id = $("#lokasi_id").val();
      $.ajax({
        type : 'post',
        url : '<?=site_url($nav['nav_url'])?>/ajax_pemeriksaan_fisik/save',
        data : $("#pemeriksaan_fisik_form").serialize()+'&pasien_id='+pasien_id+'&reg_id='+reg_id+'&lokasi_id='+lokasi_id,
        success : function (data) {
          pemeriksaan_fisik_data();
        }
      })
    })

    $('.datetimepicker').daterangepicker({
      startDate: moment("<?=date('Y-m-d H:i:s')?>"),
      endDate: moment(),
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })

  })

  function catatan_medis_data(pasien_id='', reg_id='', lokasi_id='') {
    $('#catatan_medis_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_catatan_medis/data'?>', {pasien_id: pasien_id, reg_id: reg_id, lokasi_id: lokasi_id}, function (data) {
      $('#catatan_medis_data').html(data.html);
    }, 'json');
  }

  function catatan_medis_reset() {
    $("#catatanmedis_id").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#riwayat_penyakit").val('').removeClass("is-valid").removeClass("is-invalid");
    $("#alergi_obat").val('').removeClass("is-valid").removeClass("is-invalid");
    $('#catatan_medis_action').html('<i class="fas fa-save"></i> Simpan');

    $('.datetimepicker').daterangepicker({
      startDate: moment("<?=date('Y-m-d H:i:s')?>"),
      endDate: moment(),
      timePicker: true,
      timePicker24Hour: true,
      timePickerSeconds: true,
      singleDatePicker: true,
      showDropdowns: true,
      locale: {
        cancelLabel: 'Clear',
        format: 'DD-MM-YYYY H:mm:ss'
      },
      isInvalidDate: function(date) {
        return '';
      }
    })
  }

  function pemeriksaan_fisik_data() {
    var pasien_id = $("#pasien_id").val();
    var reg_id = $("#reg_id").val();
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_pemeriksaan_fisik/data'?>', {reg_id: reg_id, pasien_id: pasien_id}, function (data) {
      if (data !=null) {
        $("#keluhan_utama").val(data.keluhan_utama);
        $("#anamnesis_id").val(data.anamnesis_id);
        $("#systole").val(data.systole);
        $("#diastole").val(data.diastole);
        $("#tinggi").val(data.tinggi);
        $("#berat").val(data.berat);
        $("#suhu").val(data.suhu);
        $("#nadi").val(data.nadi);
        $("#respiration_rate").val(data.respiration_rate);
        $("#sao2").val(data.sao2);
      }
    }, 'json');
  }
</script>