<?php if(@$main == null):?>
	<tr>
    <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
  </tr>
<?php else: ?>
	<?php $i=1;foreach($main as $row):?>
	<tr>
		<td class="text-center"><?=$i++?></td>
		<td class="text-center">
		  <a href="javascript:void(0)" class="btn btn-primary btn-table btn-edit" data-catatanmedis-id="<?=$row['catatanmedis_id']?>" data-reg-id="<?=$row['reg_id']?>" data-pasien-id="<?=$row['pasien_id']?>" title="Ubah Data"><i class="fas fa-pencil-alt"></i></a>
		  <a href="javascript:void(0)" class="btn btn-danger btn-table btn-delete" data-catatanmedis-id="<?=$row['catatanmedis_id']?>" data-reg-id="<?=$row['reg_id']?>" data-pasien-id="<?=$row['pasien_id']?>" data-lokasi-id="<?=$row['lokasi_id']?>" title="Hapus Data"><i class="far fa-trash-alt"></i></a>
		</td>
		<td class="text-center"><?=to_date($row['tgl_catat'],'','full_date')?></td>
		<td class="text-left"><?=$row['riwayat_penyakit']?></td>
		<td class="text-left"><?=$row['alergi_obat']?></td>
	</tr>
	<?php endforeach; ?>
<?php endif; ?>

<script type="text/javascript">
$(document).ready(function () {
  // edit
  $('.btn-edit').bind('click',function(e) {
    e.preventDefault();
    var catatanmedis_id = $(this).attr("data-catatanmedis-id");
    var reg_id = $(this).attr("data-reg-id");
    var pasien_id = $(this).attr("data-pasien-id");
    //
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_catatan_medis/get_data'?>', {catatanmedis_id: catatanmedis_id, reg_id: reg_id, pasien_id: pasien_id}, function (data) {

      $('.datetimepicker').daterangepicker({
        startDate: moment(data.main.tgl_catat),
        endDate: moment(),
        timePicker: true,
        timePicker24Hour: true,
        timePickerSeconds: true,
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
          cancelLabel: 'Clear',
          format: 'DD-MM-YYYY H:mm:ss'
        },
        isInvalidDate: function(date) {
          return '';
        }
      })

      $('#catatanmedis_id').val(data.main.catatanmedis_id);
      $('#riwayat_penyakit').val(data.main.riwayat_penyakit);
      $('#alergi_obat').val(data.main.alergi_obat);
      $('#catatan_medis_action').html('<i class="fas fa-save"></i> Ubah');
    }, 'json');
  });
  // delete
  $('.btn-delete').on('click', function (e) {
    e.preventDefault();
    Swal.fire({
      title: 'Apakah Anda yakin?',
      text: "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#eb3b5a',
      cancelButtonColor: '#b2bec3',
      confirmButtonText: 'Hapus',
      cancelButtonText: 'Batal',
      customClass: 'swal-wide'
    }).then((result) => {
      if (result.value) {
        var catatanmedis_id = $(this).attr("data-catatanmedis-id");
		    var reg_id = $(this).attr("data-reg-id");
		    var pasien_id = $(this).attr("data-pasien-id");
		    var lokasi_id = $(this).attr("data-lokasi-id");
		    //
		    $('#catatan_medis_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
		    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_catatan_medis/delete_data'?>', {catatanmedis_id: catatanmedis_id, reg_id: reg_id, pasien_id: pasien_id, lokasi_id: lokasi_id}, function (data) {
		    	$.toast({
            heading: 'Sukses',
            text: 'Data berhasil dihapus.',
            icon: 'success',
            position: 'top-right'
          })
		      $('#catatan_medis_data').html(data.html);
		    }, 'json');
      }
    })
  })
});
</script>