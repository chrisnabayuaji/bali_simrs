<form id="laboratorium_form" action="" method="post" autocomplete="off">
  <div class="row">
    <input type="hidden" name="pemeriksaan_id" id="pemeriksaan_id" value="<?=@$mainlab['pemeriksaan_id']?>">
    <input type="hidden" name="src_reg_id" id="src_reg_id" value="<?=@$reg['reg_id']?>">
    <input type="hidden" name="pasien_id" id="pasien_id" value="<?=@$reg['pasien_id']?>">
    <div class="col-6">
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Dokter Pengirim <span class="text-danger">*</span></label>
        <div class="col-lg-9 col-md-3">
          <input type="text" class="form-control" id="dokterpengirim_nm" value="<?=@$reg['pegawai_nm']?>" required="" readonly>
          <input type="hidden" name="dokterpengirim_id" value="<?=$reg['dokter_id']?>">
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Tanggal Order<span class="text-danger">*<span></label>
        <div class="col-lg-5 col-md-9">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
            </div>
            <input type="text" class="form-control datetimepicker" name="tgl_order" id="tgl_order" value="<?php if(@$mainlab){echo to_date(@$mainlab['tgl_order'],'-','full_date');}else{echo date('d-m-Y H:i:s');}?>" required readonly aria-invalid="false">
          </div>
        </div>
      </div>
      <div class="form-group row">
        <label class="col-lg-3 col-md-3 col-form-label">Lokasi Asal <span class="text-danger">*</span></label>
        <div class="col-lg-9 col-md-3">
          <input type="text" class="form-control" id="lokasi_nm" value="<?=@$reg['lokasi_nm']?>" required="" readonly disabled>
          <input type="hidden" name="src_lokasi_id" value="<?=$reg['lokasi_id']?>">
        </div>
      </div>
    </div>
    <div class="col-6">
      <div class="form-group row">
        <label class="col-lg-4 col-md-3 col-form-label">Keterangan</label>
        <div class="col-lg-8 col-md-3">
          <textarea class="form-control" name="keterangan_order" id="keterangan_order" rows="6" readonly><?=@$mainlab['keterangan_order']?></textarea>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <ul class="nav nav-pills nav-pills-primary mt-n2" id="pills-tab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" onclick="laboratorium_pemeriksaan_data('<?=@$mainlab['pemeriksaan_id']?>', '<?=@$mainlab['reg_id']?>')" id="laboratorium_pemeriksaan-tab" data-toggle="pill" href="#laboratorium_pemeriksaan" role="tab" aria-controls="laboratorium_pemeriksaan" aria-selected="true"><i class="fas fa-clipboard-check"></i> TAB PEMERIKSAAN</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" onclick="laboratorium_tarif_tindakan_data('<?=@$mainlab['pemeriksaan_id']?>', '<?=@$mainlab['reg_id']?>')" id="laboratorium_tarif_tindakan-tab" data-toggle="pill" href="#laboratorium_tarif_tindakan" role="tab" aria-controls="laboratorium_tarif_tindakan" aria-selected="false"><i class="fas fa-money-bill-alt"></i> TAB TARIF TINDAKAN</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" onclick="laboratorium_bhp_data('<?=@$mainlab['pemeriksaan_id']?>', '<?=@$mainlab['reg_id']?>')" id="laboratorium_bhp-tab" data-toggle="pill" href="#laboratorium_bhp" role="tab" aria-controls="laboratorium_bhp" aria-selected="false"><i class="fas fa-syringe"></i> TAB BHP</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" onclick="laboratorium_alkes_data('<?=@$mainlab['pemeriksaan_id']?>', '<?=@$mainlab['reg_id']?>')"id="laboratorium_alkes-tab" data-toggle="pill" href="#laboratorium_alkes" role="tab" aria-controls="laboratorium_alkes" aria-selected="false"><i class="fas fa-prescription-bottle-alt"></i> TAB ALKES</a>
    </li>
  </ul>
  <div class="tab-content p-0 no-border">
    <div class="tab-pane fade show active" id="laboratorium_pemeriksaan" role="tabpanel" aria-labelledby="laboratorium_pemeriksaan-tab">
      <div class="media">
        <div class="media-body">
          <div class="row">
            <div class="col">
              <div class="table-responsive">
                <table class="table table-hover table-bordered table-striped table-sm w-100">
                  <thead>
                    <tr>
                      <th class="text-center" width="20">No.</th>
                      <th class="text-center" width="50">Kode</th>
                      <th class="text-center">Nama </th>              
                      <th class="text-center" width="150">Tgl Hasil </th>              
                      <th class="text-center">Nilai Normal </th>              
                      <th class="text-center">Hasil Lab </th>              
                      <th class="text-center">Catatan </th>              
                    </tr>
                  </thead>
                  <tbody id="laboratorium_pemeriksaan_data"></tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="tab-pane fade" id="laboratorium_tarif_tindakan" role="tabpanel" aria-labelledby="laboratorium_tarif_tindakan-tab">
      <div class="media">
        <div class="media-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-sm">
              <thead>
                <tr>
                  <th class="text-center" width="20">No</th>
                  <th class="text-center" width="135">Kode</th>
                  <th class="text-center">Tindakan</th>
                  <th class="text-center">Qty</th>
                  <th class="text-center">Biaya</th>
                  <th class="text-center">Petugas</th>
                </tr>
              </thead>
              <tbody id="laboratorium_tarif_tindakan_data"></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="tab-pane fade" id="laboratorium_bhp" role="tabpanel" aria-labelledby="laboratorium_bhp-tab">
      <div class="media">
        <div class="media-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-sm">
              <thead>
                <tr>
                  <th class="text-center" width="20">No</th>
                  <th class="text-center">Kode</th>
                  <th class="text-center">Nama</th>
                  <th class="text-center">Qty</th>
                  <th class="text-center">Keterangan</th>
                </tr>
              </thead>
              <tbody id="laboratorium_bhp_data"></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="tab-pane fade" id="laboratorium_alkes" role="tabpanel" aria-labelledby="laboratorium_alkes-tab">
      <div class="media">
        <div class="media-body">
          <div class="table-responsive">
            <table class="table table-bordered table-striped table-sm">
              <thead>
                <tr>
                  <th class="text-center" width="20">No</th>
                  <th class="text-center">Kode</th>
                  <th class="text-center">Nama</th>
                  <th class="text-center">Qty</th>
                  <th class="text-center">Keterangan</th>
                </tr>
              </thead>
              <tbody id="laboratorium_alkes_data"></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="col-2 offset-md-10 mt-n2">
    <div class="float-right">
      <button type="button" class="btn btn-xs btn-secondary" data-dismiss="modal"><i class="fas fa-times"></i> Tutup</button>
    </div>
  </div>
</form>

<script type="text/javascript">

  laboratorium_pemeriksaan_data('<?=@$mainlab['pemeriksaan_id']?>', '<?=@$mainlab['reg_id']?>');

  function laboratorium_pemeriksaan_data(pemeriksaan_id='', reg_id='') {
    $('#laboratorium_pemeriksaan_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_penunjang_laboratorium/laboratorium_pemeriksaan_data'?>', {pemeriksaan_id: pemeriksaan_id, reg_id: reg_id}, function (data) {
      $('#laboratorium_pemeriksaan_data').html(data.html);
    }, 'json');
  }

  function laboratorium_tarif_tindakan_data(pemeriksaan_id='', reg_id='') {
    $('#laboratorium_tarif_tindakan_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_penunjang_laboratorium/laboratorium_tarif_tindakan_data'?>', {pemeriksaan_id: pemeriksaan_id, reg_id: reg_id}, function (data) {
      $('#laboratorium_tarif_tindakan_data').html(data.html);
    }, 'json');
  }

  function laboratorium_bhp_data(pemeriksaan_id='', reg_id='') {
    $('#laboratorium_bhp_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_penunjang_laboratorium/laboratorium_bhp_data'?>', {pemeriksaan_id: pemeriksaan_id, reg_id: reg_id}, function (data) {
      $('#laboratorium_bhp_data').html(data.html);
    }, 'json');
  }

  function laboratorium_alkes_data(pemeriksaan_id='', reg_id='') {
    $('#laboratorium_alkes_data').html('<tr><td class="text-center" colspan="99"><i class="fas fa-spin fa-spinner"></i><br>Loading</td></tr>');
    $.post('<?=site_url().'/'.$nav['nav_url'].'/ajax_penunjang_laboratorium/laboratorium_alkes_data'?>', {pemeriksaan_id: pemeriksaan_id, reg_id: reg_id}, function (data) {
      $('#laboratorium_alkes_data').html(data.html);
    }, 'json');
  }
</script>