<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Location extends CI_Controller{

	function __construct() {
		parent::__construct();
	}

	function index($module=null,$controller=null,$act=null,$id1=null,$id2=null,$id3=null) {
		unset_session('ses_search,success,per_page');
		//
		if($id1 != '') {
			redirect($module . '/' . $controller . '/' . $act . '/' . @$id1 . '/' . @$id2 . '/' . @$id3);	
		} else {
			redirect($module . '/' . $controller . '/' . $act);
		}		
	}
	
}