<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Service extends MX_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'master/m_kamar',
      'master/m_lokasi',
      'master/m_kelas',
      'master/m_kelas_lokasi',
      'bridging/m_konfigurasi'
    ));
  }

  public function hitung_kamar()
  {
    //reset kamar
    $this->db->query("UPDATE mst_kamar SET jml_bed_terpakai = 0, jml_bed_kosong = jml_bed;");
    //cari pasien yang masih ranap
    $reg = $this->db->query("SELECT * FROM reg_pasien WHERE is_ranap = 1 AND pulang_st = 0 AND is_billing = 1")->result_array();
    foreach ($reg as $r) {
      $kamar = $this->db->where('kamar_id', $r['kamar_id'])->get('mst_kamar')->row_array();
      if ($kamar != null) {
        $dkamar = array(
          'jml_bed_terpakai' => $kamar['jml_bed_terpakai'] + 1,
          'jml_bed_kosong' => $kamar['jml_bed_kosong'] - 1,
          'updated_at' => date('Y-m-d H:i:s'),
          'updated_by' => @$this->session->userdata('sess_user_realname'),
        );
        $this->db->where('kamar_id', $r['kamar_id'])->update('mst_kamar', $dkamar);
        echo 'Kamar ' . $kamar['kamar_nm'] . ' updated @ ' . $dkamar['updated_at'] . '</br>';
      }
    }

    //update aplicare
    $kamar_all = $this->db->query(
      "SELECT 
				a.*, b.kelas_nm, b.kelas_singkatan, c.lokasi_nm 
			FROM mst_kamar a 
			JOIN mst_kelas b ON a.kelas_id = b.kelas_id 
			JOIN mst_lokasi c ON a.lokasi_id = c.lokasi_id
			WHERE bpjs_st = 1 AND bpjs_kd IS NOT NULL AND bpjs_kd != ''"
    )->result_array();

    $conf = $this->m_konfigurasi->get_first();

    foreach ($kamar_all as $r) {
      $data_arr = array(
        "koderuang" => $r['bpjs_kd'],
        "kodekelas" => $r['kelas_singkatan'],
        "namaruang" => $r['lokasi_nm'],
        "kapasitas" => $r['jml_bed'],
        "tersedia" => $r['jml_bed_kosong'],
        "tersediapria" => 0,
        "tersediawanita" => 0,
      );
      $data_str = json_encode($data_arr);

      $response = bpjs_service('aplicare', 'POST', "rest/bed/update/" . $conf['kode_ppk'], $data_str);
      var_dump($data_arr['koderuang'] . ' ' . $response . '</br>');
    }
  }
}
