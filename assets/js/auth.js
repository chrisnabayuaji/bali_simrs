$(function() {
	$('input[name="t_username"]').focus();
	//
	$('#btn-login').bind('click',function(e) {
		e.preventDefault();
		var u = $('input[name="t_username"]');
		var p = $('input[name="t_password"]');
		if(u.val() == '') {
			u.focus();
			$('#body-message').removeClass('hide').fadeOut().fadeIn('slow');
			$('#txt-message').html('<i class="fas fa-exclamation-triangle"></i> Maaf, Username harap diisi !');
			setTimeout(function() { 
	      $('#body-message').fadeOut( 800, function() {
				    $(this).removeClass('hide');
				  });
		  }, 5000);
		} else if(p.val() == '') {
			p.focus();
			$('#body-message').removeClass('hide').fadeOut().fadeIn('slow');
			$('#txt-message').html('<i class="fas fa-exclamation-triangle"></i> Maaf, Password harap diisi !');
			setTimeout(function() { 
	      $('#body-message').fadeOut( 800, function() {
				    $(this).removeClass('hide');
				  });
		  }, 5000);
		} else {
			$.post(base_url+'app/auth/ajax/auth_login',$('#form-login').serialize(),function(data) {
				if(data.result == 'false') {
					u.focus();
					$('#body-message').removeClass('hide').fadeOut().fadeIn('slow');
					$('#txt-message').html(data.message);
					setTimeout(function() { 
			      $('#body-message').fadeOut( 800, function() {
						    $(this).removeClass('hide');
						  });
				  }, 5000);
				} else {
					location.href = data.redirect;
				}
			},'json');
		}
	});
});