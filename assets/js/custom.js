$(document).ready(function () {
  //TABLE ============================================================================================================
  // Radio and Checkbox
  $(".form-check label,.form-radio label").append(
    '<i class="input-helper"></i>'
  );
  // Highlight When Click <tr>
  if ($(".table-highlight").length) {
    $(".cb-all").change(function () {
      if ($(this).is(":checked")) {
        $(".table-highlight tbody tr").addClass("highlight-active");
      } else {
        $(".table-highlight tbody tr").removeClass("highlight-active");
      }
    });
    $(".table-highlight tbody tr").click(function (event) {
      if (event.target.type !== "checkbox") {
        $(":checkbox", this).trigger("click");
        $(this).toggleClass("highlight-active");
      }
      $(this).toggleClass("highlight-active");
    });
  }
  // Check all checkbox
  $(".cb-all").click(function (e) {
    $(".cb-item").prop("checked", this.checked);
    var a = $(".cb-item").data("id");
    console.log(a);
  });
  //Overlay Scrollbars
  $(".table-responsive tbody").overlayScrollbars({});
  //ENDTABLE ============================================================================================================

  //CHOSEN SELECT ============================================================================================================
  if ($(".chosen-select").length) {
    $(".chosen-select").select2();
    // addClass
    $(".filter-data").addClass("filter-data-chosen-select");
  }
  if ($(".chosen-select-filter").length) {
    $(".chosen-select-filter").select2();
    //
    $(".select2-container").css("margin-bottom", "-1px");
  }
  if ($(".select-search-menu").length) {
    $(".select-search-menu").select2({
      placeholder: "Pilih cari menu",
    });
  }
  if ($(".select-search-menu-module").length) {
    var title = $(".select-search-menu-module").attr("data-text");
    $(".select-search-menu-module").select2({
      placeholder: "Pilih cari menu di " + title,
    });
  }
  //END CHOSEN SELECT ============================================================================================================

  $(".modal-href").click(function (e) {
    e.preventDefault();
    var modal_title = $(this).attr("modal-title");
    var modal_size = $(this).attr("modal-size");
    var modal_custom_size = $(this).attr("modal-custom-size");
    var modal_header = $(this).attr("modal-header");
    var modal_content_top = $(this).attr("modal-content-top");

    $("#modal-size")
      .removeClass("modal-lg")
      .removeClass("modal-md")
      .removeClass("modal-sm");

    $("#modal-title").html(modal_title);
    $("#modal-size").addClass("modal-" + modal_size);
    if (modal_custom_size) {
      $("#modal-size").attr(
        "style",
        "max-width: " + modal_custom_size + "px !important"
      );
    }
    if (modal_content_top) {
      $(".modal-content-top").attr(
        "style",
        "margin-top: " + modal_content_top + " !important"
      );
    }
    if (modal_header == "hidden") {
      $("#modal-header").addClass("d-none");
    } else {
      $("#modal-header").removeClass("d-none");
    }
    $("#myModal").modal("show");
    $("#modal-body").html(
      '<div class="text-center"><i class="fas fa-spin fa-spinner fa-2x"></i><br>Loading</div>'
    );
    $.post(
      $(this).data("href"),
      function (data) {
        $("#modal-body").html(data.html);
      },
      "json"
    );
  });

  $(".modal-config-user").click(function (e) {
    e.preventDefault();
    var modal_title = $(this).attr("modal-title");
    var modal_size = $(this).attr("modal-size");
    var modal_custom_size = $(this).attr("modal-custom-size");
    var data_url = $(this).attr("data-url");

    $("#modal-title").html(modal_title);
    $("#modal-size").addClass("modal-" + modal_size);
    if (modal_custom_size) {
      $("#modal-size").attr(
        "style",
        "max-width: " + modal_custom_size + "px !important"
      );
    }
    $("#myModal").modal("show");
    $.post(
      $(this).data("href"),
      { data_url: data_url },
      function (data) {
        $("#modal-body").html(data.html);
      },
      "json"
    );
  });

  //FLASH MESSAGE ============================================================================================================
  // success message
  const flashSuccess = $(".flash-success").data("flashsuccess");
  if (flashSuccess) {
    $.toast({
      heading: "Sukses",
      text: flashSuccess,
      icon: "success",
      position: "top-right",
    });
  }
  // error message
  const flashError = $(".flash-error").data("flasherror");
  if (flashError) {
    $.toast({
      heading: "Error",
      text: flashError,
      icon: "error",
      position: "top-right",
    });
  }
  //END FLASH MESSAGE ============================================================================================================

  //DELETE CONFIRM ============================================================================================================
  $(".btn-delete").on("click", function (e) {
    e.preventDefault();

    const href = $(this).data("href");

    Swal.fire({
      title: "Apakah Anda yakin?",
      text:
        "Aksi ini tidak bisa dikembalikan. Data ini mungkin terhubung dengan data lain.",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#eb3b5a",
      cancelButtonColor: "#b2bec3",
      confirmButtonText: "Hapus",
      cancelButtonText: "Batal",
      customClass: "swal-wide",
    }).then((result) => {
      if (result.value) {
        document.location.href = href;
      }
    });
  });
  //END DELETE CONFIRM ============================================================================================================

  // MENU MODUL ===================================================================
  $("#menu_module").on("change", function () {
    var link = $(this).val();
    window.location = link;
  });
  // END MENU MODUL===============================================================

  //AUTONUMERIC ============================================================================================================
  $(".autonumeric").autoNumeric({
    aSep: ".",
    aDec: ",",
    vMax: "999999999999999",
    vMin: "0",
  });
  $(".autonumeric-float").autoNumeric({
    aSep: ".",
    aDec: ",",
    aForm: true,
    vMax: "999999999999999999.99999",
    vMin: "-999999999999999999.99999",
    mDec: "4",
    aPad: false
  });
  //END AUTONUMERIC ============================================================================================================

  //DATEPICKER ============================================================================================================
  $(".datepicker").daterangepicker({
    // maxDate: new Date(),
    singleDatePicker: true,
    showDropdowns: true,
    locale: {
      cancelLabel: "Clear",
      format: "DD-MM-YYYY",
    },
    isInvalidDate: function (date) {
      return "";
    },
  });
  $(".datepicker-top").daterangepicker({
    // maxDate: new Date(),
    singleDatePicker: true,
    showDropdowns: true,
    drops: 'up',
    locale: {
      cancelLabel: "Clear",
      format: "DD-MM-YYYY",
    },
    isInvalidDate: function (date) {
      return "";
    },
  });
  $(".datetimepicker").daterangepicker({
    timePicker: true,
    timePicker24Hour: true,
    timePickerSeconds: true,
    singleDatePicker: true,
    showDropdowns: true,
    locale: {
      cancelLabel: "Clear",
      format: "DD-MM-YYYY H:mm:ss",
    },
    isInvalidDate: function (date) {
      return "";
    },
  });
  //END DATEPICKER ============================================================================================================
});

//MULTIPLE ACTION ============================================================================================================
function multipleAction(type) {
  var desc;
  var confirmColor;
  var confirmButton;
  switch (type) {
    case "enable":
      desc = "Data ini mungkin terhubung dengan data lain.";
      confirmColor = "#5cb85c";
      confirmButton = "Aktifkan";
      break;

    case "disable":
      desc = "Data ini mungkin terhubung dengan data lain.";
      confirmColor = "#d9534f";
      confirmButton = "Non Aktifkan";
      break;

    case "delete":
      desc =
        "Aksi ini tidak bisa diurungkan. Data ini mungkin terhubung dengan data lain.";
      confirmColor = "#d9534f";
      confirmButton = "Hapus";
  }
  Swal.fire({
    title: "Apakah Anda yakin?",
    text: desc,
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: confirmColor,
    confirmButtonText: confirmButton,
    cancelButtonColor: "#b2bec3",
    cancelButtonText: "Batal",
    customClass: "swal-wide",
  }).then((result) => {
    if (result.value) {
      $("#form-multiple")
        .attr("action", nav_url + "/multiple/" + type)
        .submit();
    }
  });
}
//END MULTIPLE ACTION ============================================================================================================
