(function($) {
  'use strict';
  $(function() {
    var body = $('body');
    var contentWrapper = $('.content-wrapper');
    var scroller = $('.container-scroller');
    var footer = $('.footer');
    var sidebar = $('.sidebar');

    $('[data-toggle="minimize"]').on("click", function() {
      if ((body.hasClass('sidebar-toggle-display')) || (body.hasClass('sidebar-absolute'))) {
        body.toggleClass('sidebar-hidden');
      } else {
        body.toggleClass('sidebar-icon-only');
      }
    });

    //checkbox and radios
    $(".form-check label,.form-radio label").append('<i class="input-helper"></i>');

    //Horizontal menu in mobile
    $('[data-toggle="horizontal-menu-toggle"]').on("click", function() {
      $(".horizontal-menu .bottom-navbar").toggleClass("header-toggled");
    });
    // Horizontal menu navigation in mobile menu on click
    var navItemClicked = $('.horizontal-menu .page-navigation >.nav-item');
    navItemClicked.on("click", function(event) {
      if(window.matchMedia('(max-width: 991px)').matches) {
        if(!($(this).hasClass('show-submenu'))) {
          navItemClicked.removeClass('show-submenu');
        }
        $(this).toggleClass('show-submenu');
      }        
    })

    $(window).scroll(function() {
      if(window.matchMedia('(min-width: 992px)').matches) {
        var header = $('.horizontal-menu');
        if ($(window).scrollTop() >= 40) {
          $(header).addClass('fixed-on-scroll');
        } else {
          $(header).removeClass('fixed-on-scroll');
        }
      }
    });

    /* Code for attribute data-custom-class for adding custom class to tooltip */
    if (typeof $.fn.tooltip.Constructor === 'undefined') {
      throw new Error('Bootstrap Tooltip must be included first!');
    }

    var Tooltip = $.fn.tooltip.Constructor;

    // add customClass option to Bootstrap Tooltip
    $.extend(Tooltip.Default, {
      customClass: ''
    });

    var _show = Tooltip.prototype.show;

    Tooltip.prototype.show = function() {

      // invoke parent method
      _show.apply(this, Array.prototype.slice.apply(arguments));

      if (this.config.customClass) {
        var tip = this.getTipElement();
        $(tip).addClass(this.config.customClass);
      }

    };
    $('[data-toggle="tooltip"]').tooltip();

    if ($(".chosen-select").length) {
      $(".chosen-select").select2();
      // addClass
      $('.filter-data').addClass('filter-data-chosen-select');
    }
    if ($(".select-search-menu").length) {
      $(".select-search-menu").select2({
        placeholder: "Pilih cari menu"
      });
    }
    if ($(".select-search-menu-module").length) {
      var title = $(".select-search-menu-module").attr("data-text");
      $(".select-search-menu-module").select2({
        placeholder: "Pilih cari menu di "+title
      });
    }

    if ($(".js-example-basic-multiple").length) {
      $(".js-example-basic-multiple").select2();
    }

    if ($(".datepicker").length) {
      $('.datepicker').datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        todayHighlight: true,
      });
    }

    if ($(".table-highlight").length) {

      $('.cb-all').change(function() {
        if($(this).is(":checked")) {
          $('.table-highlight tbody tr').addClass("highlight-active");
        }else{
          $('.table-highlight tbody tr').removeClass("highlight-active");
        }
      });

      $('.table-highlight tbody tr').click(function(event) {
        if (event.target.type !== 'checkbox') {
          $(':checkbox', this).trigger('click');
          $(this).toggleClass("highlight-active");
        }
        $(this).toggleClass("highlight-active");
      });
    }

    window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
    form.addEventListener('submit', function(event) {
    if (form.checkValidity() === false) {
    event.preventDefault();
    event.stopPropagation();
    }
    form.classList.add('was-validated');
    }, false);
    });
    }, false);

    // Currency
    $(".currency").autoNumeric({ aSep: ".", aDec: ",", vMax: "999999999999999", vMin: "0" });

    $('.cb-all').click(function (e) {
      $('.cb-item').prop('checked', this.checked);
      var a = $('.cb-item').data('id');
      console.log(a);
    });
    //
    $('#delete-all').bind('click',function(e) {
      var a = $(this).attr('data-action');
      var l = $('.cb-item:checked').length;
      if(l == 0) {
        swal({
          text: "Maaf, Data harap dipilih dahulu !",
          icon: "warning",
          confirmButtonColor: '#3f51b5',
          buttons: {
            confirm: {
              text: "OK",
              value: true,
              visible: true,
              className: "btn btn-primary"
            }
          }
        });
        return false;
      } else {
        swal({
          title: "",
          text: "Apakah Anda yakin akan menghapus data terpilih ini ?",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: '#3f51b5',
          cancelButtonColor: '#ff4081',
          buttons: {
            cancel: {
              text: 'Batal',
              value: null,
              visible: true,
              className: "btn btn-danger",
              closeModal: true,
            },
            confirm: {
              text: "Hapus",
              value: true,
              visible: true,
              className: "btn btn-primary"
            }
          }
        })
        .then((willProcess) => {
          if (willProcess) {
            $('#form-index').attr('action',a).submit();
          }
        });
      }   
    });
    //
    $('#activated-all').bind('click',function(e) {
      var a = $(this).attr('data-action');
      var l = $('.cb-item:checked').length;
      if(l == 0) {
        swal({
                text: "Maaf, Data harap dipilih dahulu !",
                icon: "warning",
                button: "OK",
              });
        return false;
      } else {
        swal({
          title: "",
          text: "Apakah Anda yakin akan mengaktifkan data terpilih ini ?",
          icon: "warning",
          buttons: {
                  cancel: "Batal",
                  sucess: {
                    text: "Lanjut",
                  },
                },
        })
        .then((willProcess) => {
          if (willProcess) {
            $('#form-index').attr('action',a).submit();
          }
        });
      }   
    });
    //
    $('#non-activated-all').bind('click',function(e) {
      var a = $(this).attr('data-action');
      var l = $('.cb-item:checked').length;
      if(l == 0) {
        swal({
                text: "Maaf, Data harap dipilih dahulu !",
                icon: "warning",
                button: "OK",
              });
        return false;
      } else {
        swal({
          title: "",
          text: "Apakah Anda yakin akan menonaktifkan data terpilih ini ?",
          icon: "warning",
          buttons: {
                  cancel: "Batal",
                  sucess: {
                    text: "Lanjut",
                  },
                },
        })
        .then((willProcess) => {
          if (willProcess) {
            $('#form-index').attr('action',a).submit();
          }
        });
      }   
    });
    //
    $('#process-all').bind('click',function(e) {
      var a = $(this).attr('data-action');
      var l = $('.cb-item-usulan:checked').length;
      var no_usulan = $('#no_usulan');
      if(no_usulan.val() == '') {
        swal({
                text: "No. Usulan Belum Diisi",
                icon: "warning",
                button: "OK",
              });
              no_usulan.focus();
              return false;
      }else{
        if(l == 0) {
          swal({
                  text: "Maaf, Data harap dipilih dahulu !",
                  icon: "warning",
                  button: "OK",
                });
          return false;
        } else {
          swal({
            title: "",
            text: "Apakah Anda yakin akan memproses data terpilih ini ?",
            icon: "warning",
            buttons: {
                    cancel: "Batal",
                    sucess: {
                      text: "Lanjut",
                    },
                  },
          })
          .then((willProcess) => {
            if (willProcess) {
              $('#form-index').attr('action',a).submit();
            }
          });
        } 
      } 
    });
    //
    $('.delete-item').bind('click',function(e) {
      e.preventDefault();
      swal({
        title: "",
        text: "Apakah anda yakin akan menghapus data ini ?",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3f51b5',
        cancelButtonColor: '#ff4081',
        buttons: {
          cancel: {
            text: 'Batal',
            value: null,
            visible: true,
            className: "btn btn-danger",
            closeModal: true,
          },
          confirm: {
            text: "Hapus",
            value: true,
            visible: true,
            className: "btn btn-primary"
          }
        }
      })
      .then((willProcess) => {
        if (willProcess) {
          location.href = $(this).attr('href');
        }
      });
    }); 
    //

    $(".logout").click(function(e) {
    var location_href = document.getElementsByClassName('location-logout')[0].href;
      e.preventDefault()
        swal({
          title: "",
          text: "Apakah anda yakin akan keluar dari aplikasi ini ?",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: '#3f51b5',
          cancelButtonColor: '#ff4081',
          buttons: {
            cancel: {
              text: 'Batal',
              value: null,
              visible: true,
              className: "btn btn-danger",
              closeModal: true,
            },
            confirm: {
              text: "Logout",
              value: true,
              visible: true,
              className: "btn btn-primary"
            }
          }
        })
        .then((willDelete) => {
            if (willDelete) {
                window.location=location_href;
                    swal({
                      title: "",
                      text: "Berhasil Logout",
                      icon: "success",
                      confirmButtonColor: '#3f51b5',
                      cancelButtonColor: '#ff4081',
                      buttons: {
                        confirm: {
                          text: "Oke",
                          value: true,
                          visible: true,
                          className: "btn btn-primary"
                        }
                      }
                    })
            } else {
                
            }
        });
    });

    if ($(".color-picker").length) {
      $('.color-picker').asColorPicker();
    }

    
    
  });

})(jQuery);

(function($) {
  showSuccessToast = function(title, msg, position) {
    'use strict';
    resetToastPosition();
    $.toast({
      heading: title,
      text: msg,
      showHideTransition: 'slide',
      icon: 'success',
      loaderBg: '#13bf0d',
      position: position
    })
  };
  showInfoToast = function() {
    'use strict';
    resetToastPosition();
    $.toast({
      heading: 'Info',
      text: 'And these were just the basic demos! Scroll down to check further details on how to customize the output.',
      showHideTransition: 'slide',
      icon: 'info',
      loaderBg: '#46c35f',
      position: 'top-right'
    })
  };
  showWarningToast = function() {
    'use strict';
    resetToastPosition();
    $.toast({
      heading: 'Warning',
      text: 'And these were just the basic demos! Scroll down to check further details on how to customize the output.',
      showHideTransition: 'slide',
      icon: 'warning',
      loaderBg: '#57c7d4',
      position: 'top-right'
    })
  };
  showDangerToast = function(title, msg, position) {
    'use strict';
    resetToastPosition();
    $.toast({
      heading: title,
      text: msg,
      showHideTransition: 'slide',
      icon: 'error',
      loaderBg: '#e61b15',
      position: position
    })
  };
  showToastPosition = function(position) {
    'use strict';
    resetToastPosition();
    $.toast({
      heading: 'Positioning',
      text: 'Specify the custom position object or use one of the predefined ones',
      position: String(position),
      icon: 'info',
      stack: false,
      loaderBg: '#f96868'
    })
  }
  showToastInCustomPosition = function() {
    'use strict';
    resetToastPosition();
    $.toast({
      heading: 'Custom positioning',
      text: 'Specify the custom position object or use one of the predefined ones',
      icon: 'info',
      position: {
        left: 120,
        top: 120
      },
      stack: false,
      loaderBg: '#f96868'
    })
  }
  resetToastPosition = function() {
    $('.jq-toast-wrap').removeClass('bottom-left bottom-right top-left top-right mid-center'); // to remove previous position class
    $(".jq-toast-wrap").css({
      "top": "",
      "left": "",
      "bottom": "",
      "right": ""
    }); //to remove previous position style
  }
})(jQuery);

// non jQuery
function convertToRupiah(angka) {
  var rupiah = '';    
  var angkarev = angka.toString().split('').reverse().join('');
  for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
  return rupiah.split('',rupiah.length-1).reverse().join('');
}

// $('.modal-href').click(function(e) {
//   e.preventDefault(); //mematikan fungsi default
//   var modal_title = $(this).attr("modal-title");
//   var modal_size = $(this).attr("modal-size");
//   $("#modal-title").html(modal_title);
//   $("#modal-size").addClass('modal-'+modal_size);
//   $("#modal-body").load(this.getAttribute("href"));
//   $("#myModal").modal('show');
// });

$('.modal-href').click(function(e) {
  e.preventDefault(); //mematikan fungsi default
  var modal_title = $(this).attr("modal-title");
  var modal_size = $(this).attr("modal-size");
  $("#modal-title").html(modal_title);
  $("#modal-size").addClass('modal-'+modal_size);
  $("#myModal").modal('show');
  $.post(this, function(data) {
    $("#modal-body").html(data.html);
  },'json');
});

jQuery.extend(jQuery.expr[':'], {
    focusable: function (el, index, selector) {
        return $(el).is('a, button, :input, [tabindex]');
    }
});

$(document).on('keypress', '.next-form', function (e) {
    if (e.which == 13) {
        e.preventDefault();
        var $canfocus = $(':focusable');
        var index = $canfocus.index(document.activeElement) + 1;
        if (index >= $canfocus.length) index = 0;
        $canfocus.eq(index).focus();
    }
});

$(function(){
  var $window = $(window);
  var table = $('table');
  var windowsize = $window.width();
  if (windowsize <= 700) {
    $('.table-fixed thead').css("cssText", "width: 1300px !important;");
  }else{
    if ((table.hasClass('table-fixed'))) {
      var validation_scroll = $('.table-fixed tbody').hasScrollBar();
      if (validation_scroll == true) {
        $('.table-fixed thead').css("cssText", "width: calc( 100% - 17px ) !important;");
      }else{
        $('.table-fixed thead').css("cssText", "width: 100% !important;");
      }
    }
  }
});

(function($) {
  var table = $('table');
  if ((table.hasClass('table-fixed'))) {
    $.fn.hasScrollBar = function() {
      return this.get(0).scrollHeight > this.height();
    }
  }
})(jQuery);

$('#menu_module').on('change', function() {
  val_url = this.value;
  window.location.href = val_url;
});

// lazy load image
(function() {
  var ll = new LazyLoad({
    elements_selector: ".lazy-load",
  });
})();
